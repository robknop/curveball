import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from astropy.time import Time
import requests
import pathlib

import config
import db
from exposuresource import ExposureSource
from define_object import define_object
from get_images_for_sn import get_images_for_sn
from processing import cat_psf_sky, make_weight
from psfexreader import PSFExReader

from astropy.nddata import Cutout2D
from photutils.background import LocalBackground
from photutils.aperture import CircularAperture, ApertureStats, aperture_photometry
from astropy.wcs import WCS
from astropy.io import fits
from astropy.coordinates import SkyCoord
import astropy.units as units
from astropy.nddata.utils import NoOverlapError

from sqlalchemy import create_engine, text

# config.Config.init( 'curveball/emily_muckabout/emily_saul.yaml' )
decam = ExposureSource.get("DECam", "Reduced")

gal_file = 'graham_quasars.dat'
conn_string = 'postgresql+psycopg2://decat_ro:oe0f7cc0uaa3@decatdb.lbl.gov/decat'

_urlbase = "https://astroarchive.noirlab.edu"

def find_images( expsource, ra, dec, containing=True, dra=0.008, ddec=0.008,
                     band=None, starttime=None, endtime=None, minexptime=None,
                     logger=None ):
    print("Finding images")
    # From IRAsurvey
    if containing is False:
        raise NotImplementedError( f"{self.__class__.__name__} find_images "
                                   f"currently only works with 'containing'" )
    # Check band object
    if band is not None and not isinstance( band, db.Band ):
        bandobj = db.Band.get_by_name( band, self.id )
        if bandobj is None:
            raise RuntimeError( f'Unknown band {band}' )
        band = bandobj
    
    # Get larger of two sizes #TODO: filter this afterward?
    size = dra if dra>ddec else ddec

    # Backup start and end times
    if endtime is None:
        endtime = Time.now().mjd
    if starttime is None:
        starttime = 55197 # Start of 2010

    # Handle jd->mjd conversion
    startmjd, endmjd = [(x-2400000.5 if x>2400000 else x) for x in [starttime,endtime]]
    startdate, enddate = startmjd-1, endmjd+1
    
    # Convert to iso format for astroarchive
    startdate, enddate = Time([startdate, enddate], format='mjd').to_value('iso', subfmt='date')
    print(f"Querying dates: {startdate}, {enddate}")
    
    # Build URL
    url = f"{_urlbase}/api/sia/vohdu?POS={ra},{dec}&SIZE={size}"
    if band is not None:
        url+=f"&filter={band.filtercode}"
    url+="&format=json&instrument=decam&VERB=3&proc_type=instcal&prod_type=image&limit=0"
    
    response = requests.get(url)
    response.raise_for_status()

    # Check response and get results
    if response.status_code == 200:
        files = pd.DataFrame(response.json()[1:])
    else:
        logger.error(response.json()['errorMessage'])
        logger.error(response.json()['traceback'])  # for API developer use
        return
    
    if files.empty:
        print("No files found in search.")
        return files
    files.rename(columns={'file_dateobs': 'obsdate'}, inplace=True)
    
    # Set quantities
    files['moonillf'] = 0 # TODO: fix this later if needed
    files['filtercode'] = files['filter'].str[0]
    
    # Convert timestamps to MJD
    timestamp = pd.to_datetime(files['obsdate'], errors='coerce')
    ts = Time(timestamp)
    files['obsmjd'] = ts.mjd
    files['chiptag'] = files['hdu_idx'] # THIS IS VERY WRONG - DON'T MIX IT UP
    
    # Filter results
    if minexptime is not None:
        files = files[files.exposure>=minexptime]
    
    good = []
    for i,filename in enumerate(files.archive_filename):
        try:
            expsource.blob_image_name(files, i)
            good.append(i)
        except:
            pass
    files = files.iloc[good]
    
    # Estimate seeing from FWHM
    # noseeidx = pd.isnull(files['seeing'])
    # noseeing = files[noseeidx]
    # files.loc[noseeidx, 'seeing'] = noseeing['fwhm'] * 0.263 # arcsec / pixel
    # TODO: make sure this won't be used past the secure_references step
    # Maybe use it in guess_seeing
    #### EMILY TODO IMPORTANT: UN-HARDCODE THE PIXSCALE

    # Filter MJDs further
    files = files[(files.obsmjd > startmjd) & (files.obsmjd < endmjd)]

    files.sort_values(by='obsmjd', inplace=True, ignore_index=True)

    return files

def do_photometry(clip, clip_mask, clip_weight, sc, fwhm, radius=2.0, annulus=[5,6],
                  recentroiditerations=1):
    ### Ask Rob: should I change the radius?
    # Make noise image
    noisedata = np.sqrt(1./clip_weight.data)

    # Mask image
    bool_mask = np.array(clip_mask.data, dtype=bool)
    # masked_clip = np.ma.MaskedArray(clip.data, bool_mask)
    masked_clip = clip.data # TODO: change this if needed
    
    xy = np.array(clip.wcs.world_to_pixel(sc))
    
    for _ in range(recentroiditerations):
        
        x, y = xy
        
        # Get local background
        lb = LocalBackground(fwhm*annulus[0], fwhm*annulus[1])
        bg = lb(masked_clip, x, y, mask=clip_mask.data.astype(bool))

        bg_subtracted = masked_clip - bg

        # Do aperture photometry
        # Also, how do I handle the case where there is no mask? Happened a couple times
        # Can I use the apercor_from_psf method if we're treating like pt sources?
        # How big should the radius be for a galaxy? Although we're treating it like a pt source...
        center = np.array([y,x]) # in photutils order?
        aper = CircularAperture(center, r=radius*fwhm)
        aperstats = ApertureStats( bg_subtracted , aper, error=noisedata, mask=bool_mask )
        xy = [aperstats.xcentroid, aperstats.ycentroid]
        if np.isnan(xy).any():
            print("Got invalid value for centroids, skipping")
            return None, None, (None, None)
    
    # Calculate aperture flux
    # Ask Rob: dxy as total change or change per iteration or avg change per iteration?
    phot = aperture_photometry( bg_subtracted, aper, error=noisedata, mask=bool_mask )
    return phot['aperture_sum'][0], phot['aperture_sum_err'][0], xy

def apercor_from_psf( psf_file, x0, y0, radius ):

    """Determine the aperture correction for an aperture radius in pixels using the PSFEx file.

    x0, y0 — position on the image where the psf is evaluated
    radius — radius of aperture in pixels

    """
    psf = PSFExReader( psf_file )
    clip = psf.getclip( x0, y0, 1.0 )
    if ( radius > clip.shape[0] // 2 ):
        raise RuntimeError( f"Radius {radius} is too big for clip shape {clip.shape}" )
    xc = int( np.floor( x0 + 0.5 ) )
    yc = int( np.floor( y0 + 0.5 ) )
    aper = CircularAperture( ( clip.shape[1] // 2 + (x0-xc), clip.shape[0] // 2 + (y0-yc) ), radius )
    res = aperture_photometry( clip, aper )
    return (2.5 * np.log10( res['aperture_sum'][0] )), 0. 

def make_mask(expsource, weight_file, tiny=1e-8):
    weight = pathlib.Path(weight_file)
    with fits.open(weight_file, memmap=True) as weight_hdu:
        mask_data = np.array(weight_hdu[0].data > tiny).astype(np.uint8)
        mask_hdr = fits.Header( weight_hdu[0].header, copy=True )
    mask_hdr["COMMENT"] = f'Mask image for {weight.name}'
    
    mhdu = fits.PrimaryHDU( data=mask_data, header=mask_hdr )
    
    maskpath = weight.name
    maskpath = expsource.edit_filename(maskpath, {"imgtype": "mask"})
    print(maskpath)
    maskpath = weight.parent / maskpath
    
    print(f"Writing mask to {maskpath}")
    mhdu.writeto(maskpath, overwrite=True)
    
    return maskpath

def make_lc(expsource, basenames, sc, tiny=1e-8, recentroid=False):
    lc_data = pd.DataFrame(index=np.arange(len(basenames)), 
                           columns=['basename', 'mjd', 'phot', 'dphot', 
                                    'ra', 'dec', 'zp'])
    lc_data['basename'] = basenames
    for i,b in enumerate(basenames):
        print(f"\nProcessing file {b}\n")
        img_file = expsource.local_path_from_basename(b)
        mask_file = expsource.local_path_from_basename(b, filetype='mask')
        weight_file = expsource.local_path_from_basename(b, filetype='weight')
        
        # Make missing files
        # if weight_file is None:
        #     weight_file = make_weight(decam, img_file, mask_file)
        # if mask_file is None:
        #     mask_file = make_mask(decam, weight_file)
        
        if weight_file is None or mask_file is None:
            print(f"Could not find weight or mask file for {b}, skipping")
            continue
        
        # Make cat and PSF files
        cat_psf_sky(decam, img_file, weight_file, mask_file, 
                        pixscale_key='PIXSCAL1', nodb=True)
        psf_file = expsource.local_path_from_basename(b, filetype='psf')
        cat_file = expsource.local_path_from_basename(b, filetype='cat')
        
        # Get clips from files
        with fits.open(img_file, memmap=True) as img_hdu:
            # import pdb; pdb.set_trace()
            wcs = WCS(img_hdu[0].header)
            fwhm = img_hdu[0].header['FWHM']
            lc_data.loc[i, 'mjd'] = img_hdu[0].mjd
            try:
                clip = Cutout2D(img_hdu[0].data, sc, size=101, 
                                wcs=wcs, mode='partial', fill_value=0)
            except NoOverlapError as e:
                print(f"Error: {e} - skipping file {b}")
                continue
            lc_data.loc[i,'zp'] = img_hdu[0].header['MAGZERO']
            xy = wcs.world_to_pixel(sc)
            print(xy)
        with fits.open(weight_file, memmap=True) as weight_hdu:
            clip_weight = Cutout2D(weight_hdu[0].data, sc, size=101, 
                                 wcs=wcs, mode='partial', fill_value=tiny) #TODO: un-hardcode fill
        with fits.open(mask_file, memmap=True) as mask_hdu:
            clip_mask = Cutout2D(mask_hdu[0].data, sc, size=101, 
                                 wcs=wcs, mode='partial', fill_value=1)
            if np.sum(clip_mask.data.astype(bool)) / clip_mask.data.size > 0.4:
                print(f"Skipping file {b} - object may be fully or partially masked")
                continue
        
        # if b=='c4d_20160205_050832_z_v1.10':
        #     import pdb; pdb.set_trace()
        
        # Do aperture photometry
        rc = 5 if recentroid else 1
        phot, dphot, centroid = do_photometry(clip, clip_mask, clip_weight, sc, fwhm,
                                              recentroiditerations=rc)
        if phot is None or dphot is None or any([x is None for x in centroid]):
            raise ValueError("One or more photometry values came back as None!")
        
        print(phot)
        
        # Find aperture correction
        ac, dac = apercor_from_psf(psf_file, xy[0], xy[1], radius=fwhm)
        phot *= 10**( ac / -2.5  )
        dphot *= 10**( ac / -2.5 )
        tmp = phot * np.abs( 0.4 * np.log(10) * 10**(ac/-2.5) ) * dac
        dphot = np.sqrt( dphot*dphot + tmp*tmp ) # Ask Rob
        
        centroid = clip.wcs.pixel_to_world(centroid[0], centroid[1])
        
        print(phot, '\n')
        lc_data.loc[i, 'phot'] = phot
        lc_data.loc[i, 'dphot'] = dphot
        lc_data.loc[i, 'ra'] = centroid.ra.degree
        lc_data.loc[i, 'dec'] = centroid.dec.degree
    
    return lc_data

if __name__=='__main__':
    
    engine = create_engine(conn_string)
    
    ################
    gals = pd.read_csv(gal_file, usecols=range(6))
    gals.columns = [x.replace("#", "").strip().lower() for x in list(gals.columns)]
    gals.rename(columns={'ra':'ra_old', 'dec':'dec_old'}, inplace=True)
    gals['type'] = gals['type'].apply(lambda x: x.strip())
    gals['type2'] = None
    
    #################
    with open(gal_file, 'r') as file:
        lines = file.readlines()
        for i,line in enumerate(lines):
            line = line.split(',')
            if len(line)>6:
                gals.loc[i-1, 'type2'] = line[-1].strip()
    
    #################
    with engine.connect() as conn:
        for i,row in gals.iterrows():
            res = conn.execute(text(f"SELECT * FROM candidates WHERE id='{row['name']}'"))
            obj = pd.DataFrame(res.fetchall())
            gals.loc[i,'ra'] = obj.ra[0]
            gals.loc[i,'dec'] = obj.dec[0]
    
    ###########################
    test_gals = gals.iloc[[3]]
    test_peakt = 58719
    print(test_gals)
    
    #########################
    filenames = {}
    for i,gal in test_gals.iterrows():
        filenames[gal['name']] = []
        try:
            define_object(gal['name'], gal['ra'], gal['dec'], test_peakt)
        except:
            print(f"Object {gal['name']} exists")
        imgs = find_images(decam, gal['ra'], gal['dec'])
        for j,img in imgs.iterrows():
            filenames[gal['name']].append(decam.blob_image_name(imgs, j))
            decam.download_blob_image(imgs, j)
    
    #########################
    # gal = test_gals.iloc[2]
    gal = test_gals.iloc[0]
    test_files = [x[:-5] for x in filenames[gal['name']]]
    print(f"There are {len(test_files)} files.")
    sc = SkyCoord( gal['ra'], gal['dec'], frame='icrs', unit=units.deg )
    lc_data = make_lc(decam, test_files, sc, recentroid=True)
    
    import pdb; pdb.set_trace()
    centroids = np.array(lc_data[['ra', 'dec']].astype(float))
    sns = np.array(lc_data.phot/lc_data.dphot).astype(float)
    
    wgood = np.where( sns >= 3. )[0]
    # testra = (centroids[:,0][wgood]*(sns[wgood]**2)).sum() / (sns[wgood]**2).sum()
    # testdec = (centroids[:,1][wgood]*(sns[wgood]**2)).sum() / (sns[wgood]**2).sum()
    
    new_centroid = np.average(centroids[wgood], weights=sns[wgood]**2, axis=0)
    sc = SkyCoord(new_centroid[0], new_centroid[1], frame='icrs', unit=units.deg)
    
    lc_data = make_lc(decam, test_files, sc, recentroid=False)
    import pdb; pdb.set_trace()
    # zps = np.array(zps).astype(float)
    # phots, dphots = np.array(phots).astype(float), np.array(dphots).astype(float)
    lc_data['mag'] = -2.5 * np.log10( lc_data.phot.to_numpy(float) ) + lc_data.zp.to_numpy(float)
    lc_data['dmag'] = np.abs( 2.5 / np.log(10) * lc_data.dphot.to_numpy(float) /
                             lc_data.phot.to_numpy(float) )
    
    
#     lc = pd.DataFrame({'phot': phots, 'dphot': dphots, 
#                        'mag': mags, 'dmag': dmags, 
#                        'zp': zps, 'filename': test_files})
    lc_data.to_csv(f"{gal['name']}_lc.dat")



