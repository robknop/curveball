from mpi4py import MPI
import numpy as np
import mystuff, os, db
from processing import cat_psf_sky
from astropy.io import fits
from astropy.wcs import WCS
from exposuresource import ExposureSource
from buildltcv import process_image
import config

obj_name = 'DC21cyddn' # CHANGE
data_dir = f"/data/saved_curveball/test_scarlet/{obj_name}"
run_resamp = False # CHANGE
tiny=1e-5

decam = ExposureSource.get("DECam/Reduced")

if __name__=='__main__':
    comm = MPI.COMM_WORLD
    world_size = comm.Get_size()
    rank = comm.Get_rank()
    
    print(f"rank={rank}, world={world_size}")