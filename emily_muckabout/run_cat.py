from mpi4py import MPI
import numpy as np
import mystuff, os, db
from processing import cat_psf_sky
from astropy.io import fits
from astropy.wcs import WCS
from astropy.nddata import Cutout2D
from astropy.coordinates import SkyCoord
import astropy.units as u

from exposuresource import ExposureSource
from buildltcv import process_image
import config
import subprocess
import uuid

obj_name = 'DC21cozcn' # CHANGE
data_dir = f"/data/saved_curveball/test_scarlet/{obj_name}"
run_cat = True
run_resamp = False # CHANGE
run_swarp = False
tiny=1e-5

decam = ExposureSource.get("DECam/Reduced")

if __name__=='__main__':
    comm = MPI.COMM_WORLD
    world_size = comm.Get_size()
    rank = comm.Get_rank()
    
    # Get first part of computation done
    if rank==0:
        obj = db.Object.get_by_name(obj_name)
        img_objs = db.Image.get_including_point(obj.ra, obj.dec)
        basenames = [x.basename for x in img_objs if 'stack' not in x.basename]
        print(f"{len(basenames)} files")
        # basenames = basenames[5:10]
        
    else:
        basenames = None
        
    basenames = comm.bcast(basenames, root=0)
    
    # Get all the filenames
    obj = db.Object.get_by_name(obj_name)
    imagelist = [decam.local_path_from_basename(x) for x in basenames]
    masklist = [decam.local_path_from_basename(x, filetype='mask') for x in basenames]
    weightlist = [decam.local_path_from_basename(x, filetype='weight') for x in basenames]
    resamp = [f'{data_dir}/{x}.resamp.fits' for x in basenames]
    resamp_weights = [f"{data_dir}/{x}.resamp.weight.fits" for x in basenames]
    resamp_masks = [f"{data_dir}/{x}.resamp.mask.fits" for x in basenames]
    resamp_cats = [f"{data_dir}/{x}.resamp.cat" for x in basenames]
    clips = [f"{data_dir}/{x}.clip.fits" for x in basenames]
    clipweights = [f"{data_dir}/{x}.clip.weight.fits" for x in basenames]
    clipmasks = [f"{data_dir}/{x}.clip.mask.fits" for x in basenames]
    resamp_psfs = [f"{data_dir}/{x}.resamp.psf" for x in basenames]
    
    sc = SkyCoord( obj.ra, obj.dec, frame='icrs', unit=u.deg )
    
    iters = np.linspace(0, len(basenames), num=world_size+1).astype(int)
    
    if run_swarp==True and world_size==1:
        tmpdir = f'/tmp/{uuid.uuid4().hex}'
        hdrkeywords = decam.key_keywords
        swarpcmd = ( f'swarp -SUBTRACT_BACK N '
                     f'-COMBINE N -MEM_MAX 2048 '
                     f'-CELESTIAL_TYPE NATIVE '
                     f'-PROJECTION_TYPE TAN '
                     f'-RESAMPLING_TYPE BILINEAR '
                     f'-NTHREADS 0 -RESAMPLE_DIR {data_dir} -VMEM_DIR {tmpdir} '
                     f'-WRITE_FILEINFO Y '
                     f'-VERBOSE_TYPE LOG '
                     f'-DELETE_TMPFILES N '
                     f'-COPY_KEYWORDS {hdrkeywords},SEEING '
                     f'-WEIGHT_TYPE MAP_WEIGHT -WEIGHT_THRESH {tiny} '
                     f'-RESCALE_WEIGHTS Y '
                   )
        
        N = len(imagelist) // 100 # Run swarp in batches of ~100
        idxs = np.linspace(0, len(imagelist), N+1).astype(int)
        print(idxs)
        for i in range(N):
            imgs = imagelist[idxs[i]:idxs[i+1]]
            weights = weightlist[idxs[i]:idxs[i+1]]
            swarpcmd1 = (swarpcmd + f'-WEIGHT_IMAGE {",".join([str(s) for s in weights])} '
                        + f'{" ".join([str(x) for x in imgs])}')
            subprocess.run(swarpcmd1.split(), shell=False)
        
        print(f"Ran swarp on {len(imagelist)} images")
    
    elif run_resamp:
        for i in range(iters[rank], iters[rank+1]):
            
            with fits.open( resamp[i], memmap=True ) as hdul:
                # Get WCS from header of sub file
                imghdr = hdul[0].header
                wcs = WCS(hdul[0].header)
                zp = hdul[0].header['MAGZERO']
                data = hdul[0].data.astype(float)

            with fits.open( resamp_weights[i], memmap=True ) as hdul:
                weighthdr = hdul[0].header
                weightwcs = WCS(hdul[0].header)
                weightdata = hdul[0].data
            
            if not os.path.exists(resamp_masks[i]):
                maskdata = np.zeros_like( weightdata, dtype=np.uint8 )
                maskdata[ weightdata <= tiny ] = 1
                maskhdr = weightwcs.to_header()
                mhdu = fits.PrimaryHDU( data=maskdata, header= maskhdr)
                mhdu.writeto(resamp_masks[i])
                print(f"Wrote mask file: {resamp_masks[i]}")
            else:
                with fits.open(resamp_masks[i], memmap=True) as hdul:
                    maskdata = hdul[0].data.astype(float)
                    maskhdr = hdul[0].header
            
            if not os.path.exists(resamp_cats[i]):
                cat_psf_sky(decam, resamp[i], resamp_weights[i], resamp_masks[i],
                        catalog=resamp_cats[i], nodb=True, pixscale_key='PIXSCAL1')
                print(f"Made catalog and psf for {resamp[i]}")
            
            med = np.median(data)
            clip = Cutout2D(data, sc, size=100, wcs=wcs, mode='partial', fill_value=med)
            clipwt = Cutout2D(weightdata, sc, wcs=wcs, size=100, mode='partial', fill_value=0)
            clipmask = Cutout2D(maskdata, sc, wcs=wcs, size=100, mode='partial', fill_value=1)
            
            if not os.path.exists(clips[i]):
                imghdr.update(clip.wcs.to_header())
                cliphdu = fits.PrimaryHDU(header=imghdr, data=clip.data)
                cliphdu.writeto(clips[i])
            
            if not os.path.exists(clipweights[i]):
                weighthdr.update(clipwt.wcs.to_header())
                wthdu = fits.PrimaryHDU(header=weighthdr, data = clipwt.data)
                wthdu.writeto(clipweights[i])
            
            if not os.path.exists(clipmasks[i]):
                maskhdr.update(clipmask.wcs.to_header())
                maskhdu = fits.PrimaryHDU(header=maskhdr, data = clipmask.data)
                maskhdu.writeto(clipmasks[i])
            
    elif run_cat:
        for i in range(iters[rank],iters[rank+1]):
            process_image(decam, basenames[i], isastrom=False, isphotom=False)
            print(f"File {imagelist[i]} processed")
    
    print(f"\nRank {rank} done.\n")