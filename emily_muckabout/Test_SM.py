### A hard copy of my Scene Modeling Notebook for testing purposes

import os
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import subprocess

import config
import subtract
import db
from exposuresource import ExposureSource
from processing import cat_psf_sky
from psfexreader import PSFExReader
from sexsky import sexsky

from numpyro.distributions import constraints
import numpyro.distributions as dist
from astropy.io import fits
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord
from astropy.nddata import Cutout2D
import optax
import uuid
import glob
import sep
import astropy.units as u

import jax.numpy as jnp

import mystuff
import logging, sys
sys.path.insert(1, '/global/homes/e/eramey16/scarlet2')
# sys.path.insert(1, '/global/homes/e/eramey16/scarlet')
from scarlet.display import AsinhMapping,AsinhPercentileNorm
from scarlet.display import show_scarlet2_scene,LinearPercentileNorm
import scarlet, scarlet2
from scarlet2 import *

###############################

obj_name = 'DC21cozcn'
peakt = 59519
data_dir = os.path.expandvars(f"$SCRATCH/data/saved_curveball/test_scarlet/{obj_name}")
if not os.path.exists(data_dir):
    os.mkdir(data_dir)
print(data_dir)

logger = logging.getLogger("main")
logout = logging.StreamHandler( sys.stderr )
logger.addHandler( logout )
logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
logger.setLevel( logging.DEBUG )


config.Config.init('emily_saul.yaml', logger=logger)
decam = ExposureSource.get("DECam/Reduced")

obj = db.Object.get_by_name(obj_name)
img_objs = db.Image.get_including_point(obj.ra, obj.dec)
mjds = np.array([x.mjd for x in img_objs if 'stack' not in x.basename])
idxs = np.argsort(mjds)
basenames = np.array([x.basename for x in img_objs if 'stack' not in x.basename])
basenames = basenames[idxs]
basenames = basenames[[1, 2, 100, 150, 200, 250, 300, 350, -20, -1]] # Comment out if you want the full LC
print(basenames)
# basenames = basenames[(mjds>peakt-120)&(mjds<peakt+90)] # Comment this line if you want the full LC
# basenames = basenames[::4] # Comment this line if you want the full lightcurve

# imagelist = [decam.local_path_from_basename(x) for x in basenames]
# masklist = [decam.local_path_from_basename(x, filetype='mask') for x in basenames]
# weightlist = [decam.local_path_from_basename(x, filetype='weight') for x in basenames]
# print(f"{len(imagelist)} images")

tmpdir = f'/tmp/{uuid.uuid4().hex}'

ra = obj.ra
dec = obj.dec

sc = SkyCoord( obj.ra, obj.dec, frame='icrs', unit=u.deg )
coord_transient = SkyCoord(ra*u.deg, dec*u.deg, frame='icrs')

########################

def makeCatalog(observations, lvl=3, wave=True):
    normed_images = np.asarray([obs.data for obs in observations])
    interps = [scarlet.interpolation.interpolate_observation(obs, observations[0]) for obs in obssingle]
    interps = np.asarray(interps/np.sum(interps))
    detect_image = np.sum(interps,axis=(0,1))
    # Wavelet transform
    wave_detect = scarlet.Starlet.from_image(detect_image).coefficients

    if wave:
        # Creates detection from the first 3 wavelet levels
        detect = wave_detect[:lvl,:,:].sum(axis = 0)
    else:
        detect = detect_image

        # Runs SEP detection
    bkg = sep.Background(detect)
    catalog = sep.extract(detect-bkg.globalback, 6, err=bkg.globalrms)
    background=[]
    bg_rms=[]
    for obs in observations:
        img = obs.data
        if np.size(img.shape) == 3:
            bg_rms.append(np.array([sep.Background(band).globalrms for band in img]))
            background.append(np.array([sep.Background(band).globalback for band in img]))
        else:
            bg_rms.append(sep.Background(img).globalrms)
            background.append(sep.Background(img).globalback)
    return catalog, bg_rms, detect_image, background

############################

ra_dec = []
obssingle=[]
observations_sc2_old=[]
channels=[]
channels_sc2 =[]
times=[]
zps =[]
tiny=1e-5

# # ra_dec = [sc]
# img_file = decam.local_path_from_basename(basenames[0])
# with fits.open(img_file) as file:
#     wcs0 = WCS(file[0].header)

for i,basename in enumerate(basenames):
    
    # Get file names
    img_file = decam.local_path_from_basename(basename)
    mask_file = decam.local_path_from_basename(basename, filetype='mask')
    weight_file = decam.local_path_from_basename(basename, filetype='weight')
    psf_file = str(img_file)[:-5] + ".psf"
    print(psf_file)
    
    # Open image file
    with fits.open(img_file, memmap=True) as file:
        img_data = file[0].data
        img_hdr = file[0].header
        img_wcs = WCS(file[0].header)
        x, y = img_wcs.world_to_pixel(sc)
    # Make a clip of the image
    med = np.median(img_data)
    clip = Cutout2D(img_data, sc, size=101, wcs=img_wcs, mode='partial', fill_value=med)
    
    # Open weight file
    with fits.open(weight_file, memmap=True) as file:
        weight_data = file[0].data
    clip_weight = Cutout2D(weight_data, sc, size=101, wcs=img_wcs, mode='partial', fill_value=0)
    
    # Open mask file
    with fits.open(mask_file, memmap=True) as file:
        mask_data = file[0].data
    clip_mask = Cutout2D(mask_data, sc, size=101, wcs=img_wcs, mode='partial', fill_value=1)
    
    # Open PSF file
    try:
        reader = PSFExReader(psf_file)
        ### TODO: Figure out whether this should be shifted or not
        psf = reader.getclip(x, y, flux=1)
    except Exception as e:
        print(f"\nFile {psf_file}:\n{e}\n")
        continue
    
    ### TODO: mask the images
    
    # Get filter and maglimit
    f = decam.filter(img_file)[-1]
    try:
        maglim = img_hdr['PHOTDPTH']
    except:
        maglim = img_hdr['LMT_MG']
    
    # Get Channel names
    channel, channel_sc2 = [f"{f}{i}"], (f, str(i))
    channels.append(channel)
    channels_sc2.append(channel_sc2)
    
    # Format weight data
    Nw1, Nw2 = clip_weight.data.shape
    weight = clip_weight.data.reshape(1, Nw1, Nw2).astype(float)
    print(weight.shape)
    # weight[np.isnan(data)] = 0
    if (np.sum(weight==0)>0.01*weight.shape[-2]*weight.shape[-1] 
        or maglim<20):#0.001)*weight_ztf.shape[-2]*weight_ztf.shape[-1]:
        print(f'Poor image at index {i}, skipping')
        continue
    
    # Append relevant info
    times.append(img_hdr['MJD-OBS'])
    zps.append(img_hdr['MAGZERO'])
    
    # Format image data
    N1, N2 = clip.data.shape
    data = clip.data.reshape(1, N1, N2).astype(float)
    if np.isnan(data).any():
        print("Panic!")
    data[np.isnan(data)] = 0
    weight[np.isnan(data)] = 0
    print(data.shape)
    
    # Format PSF data
    psf_data = psf.astype(float)
    Np1, Np2 = psf_data.shape
    psf = scarlet.ImagePSF(psf_data)
    print(psf_data.shape)
    
    # Create the scarlet1 observation object and add it to our list of observations
    wcs = clip.wcs
    # wcs = wcs0
    obs = scarlet.Observation(data,
        wcs=wcs,
        psf=psf,
        channels=channel,
        weights=weight)
    obssingle.append(obs)
    

    # Create the scarlet2 observation object and add it to our list of observations
    obs_sc2 = scarlet2.Observation(jnp.asarray(data), 
                                   jnp.asarray(weight), 
                                    psf=scarlet2.ArrayPSF(jnp.asarray(psf_data)),
                                   channels=[channel_sc2], wcs=wcs)
    observations_sc2_old.append(obs_sc2)

print('There are',len(obssingle),'observations')

#############################

lvl = 3
wave = 1
#Obtain source catalog and background flux estimation from our makeCatalog function
catalog_single, bgsingle, detectsingle, globalback = makeCatalog(obssingle, lvl, wave)
obssinglearr=np.asarray(obssingle)
bgsinglearr=np.asfarray(bgsingle)
pixel = np.stack((catalog_single['y'], catalog_single['x']), axis=1)
ra_dec = [obs.get_sky_coord(pixel) for obs in obssinglearr]
observations_sc2=[]
normsingle=[]
for ind,(obs,obs2,bg,back) in enumerate(zip(obssingle,observations_sc2_old,bgsingle,globalback)):
    # obs.weights = np.ones(obs.shape) / (bg**2)[:, None, None]
    #Subtract background flux
    obs.data = obs.data-back
    obs_sc2 = scarlet2.Observation(jnp.asarray(obs.data), jnp.asarray(obs.weights),
                                   psf=scarlet2.ArrayPSF(jnp.asarray(obs.psf.get_model())),
                                   channels=[channels_sc2[ind]], wcs=obs.wcs)
    observations_sc2.append(obs_sc2)
    #Store norm based on observation data
    normsingle.append(LinearPercentileNorm(obs.data,percentiles=[0.1, 99.9]))

################################
import pdb; pdb.set_trace()

model_psf_s = scarlet.GaussianPSF(sigma=0.9)
model_frame_s = scarlet.Frame.from_observations([obssingle[0], obssingle[1]], 
                                                coverage='intersection',
                                                model_psf=model_psf_s)
print(model_frame_s)