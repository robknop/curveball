### ccd_mismatch.py - A file to run a simple query on the NOIRLab archive and 
###     compare the CCD numbers in metadata vs. image files
### Author - Emily Ramey
### Date - 2/15/23

from astropy.io import fits
from astropy.time import Time
import requests
import pandas as pd

ra, dec = 8.94908, -42.72461
starttime, endtime = 59854-.5, 59854+.5
starttime, endtime = Time([starttime, endtime], format='mjd').to_value('iso', 'date')

query_json = {
    "outfields" : [
        "archive_filename",
        "dateobs_center",
        "md5sum",
        "MJD-OBS",
        "hdu:ra_center",
        "hdu:dec_center",
        "hdu:hdu_idx",
        "hdu:CCDNUM",
    ],
    "search" : [
        ["instrument", "decam"],
        ["proc_type", "instcal"],
        ["prod_type", "image"],
        ["hdu:ra_center", ra-.151, ra+.151],
        ["hdu:dec_center", dec-.078, dec+.078],
        ["caldat", starttime, endtime],
        ["ifilter", 'g', 'startswith'],
    ]
}

# Build URL
urlbase = 'https://astroarchive.noirlab.edu'
find_url = f'{urlbase}/api/adv_search/find/'
meta_url = f'{find_url}?rectype=hdu&format=json&limit=0&count=N'

def filename_url(archive_filename):
    """ Gets the URL associated with a given archive filename """
    # Query the database for a specific filename
    params = { "limit": 0 }
    searchspec = { 
        "outfields": ["md5sum"],
        "search": [
            [ "archive_filename", archive_filename ]
        ]
    }
    response = requests.post( find_url, params=params, json=searchspec )
    response.raise_for_status()

    # Check status of request
    if response.status_code != 200:
        raise Exception( f"Query returned status {response.status_code}."+
                        f" {response.json()['errorMessage']}" )
    
    # Check request returned only one result
    files = pd.DataFrame(response.json()[1:])
    if files.empty:
        raise Exception(f"Could not find archive file matching: {archive_filename}")
    if len(files)>1:
        raise Exception(f"More than one file found for archive filename: {archive_filename}")
    
    # Get URL matching file
    md5sum = files.md5sum[0]
    data_url = f'{urlbase}/api/retrieve/{md5sum}'
    
    return data_url
    

def main():
    # Get response from astroarchive
    response = requests.post(meta_url, json=query_json)
    response.raise_for_status()
    
    # Check response and show results
    if response.status_code == 200:
        files = pd.DataFrame(response.json()[1:])
    else:
        raise Exception( f"Query returned status {response.status_code}."+
                        f" {response.json()['errorMessage']}" )
    
    files.rename(columns={"hdu:ra_center": "ra_center",
                           "hdu:dec_center": "dec_center",
                           "hdu:hdu_idx": "hdu_idx",
                              "hdu:CCDNUM": "ccdnum",
                              "MJD-OBS": 'obsmjd',
                          }, inplace=True)
    
    # Choose one file and print details
    print("\nINITIAL FILE METADATA:\n")
    example_file = files.iloc[2]
    print(f"File {example_file.archive_filename}\n HDU: {example_file.hdu_idx}\n",
         f"CCD: {example_file.ccdnum}\n md5sum: {example_file.md5sum}")
    
    # Now, find the file by archive filename
    file_url = filename_url(example_file.archive_filename)+f'/?hdus=0,{example_file.hdu_idx}'
    print(f"\nFile URL: {file_url}\n")
    
    # Download the file from the archive using the HDU number
    response = requests.get(file_url, timeout=120)
    response.raise_for_status()
    
    # Check response
    if response.status_code != 200:
        raise Exception( f"Query returned status {response.status_code}."+
                        f" {response.json()['errorMessage']}" )
    
    # Write to file
    with open('testfile.fits.fz', 'wb') as ofp:
        ofp.write(response.content)
    print("Wrote response to testfile.fits.fz")
    
    # Check file metadata
    with fits.open('testfile.fits.fz') as file:
        print(f"\nCCD # in file: {file[1].header['CCDNUM']}\n")
        print(f"Does this match the CCDNUM in metadata? " +
              f"{example_file.ccdnum == file[1].header['CCDNUM']}\n")

if __name__ == "__main__":
    main()