import sys
import config
import exposuresource
import processing
import logging
import db
import time
import numpy as np
import pandas as pd
import subprocess
from define_object import define_object
from make_reference import make_reference
from buildltcv import LtcvBuilder
from get_images_for_sn import get_images_for_sn
import sqlalchemy as sa
from injectfakes import FakeInjector

from psycopg2.extensions import register_adapter, AsIs
def addapt_numpy_float64(numpy_float64):
    return AsIs(numpy_float64)
def addapt_numpy_int64(numpy_int64):
    return AsIs(numpy_int64)
register_adapter(np.float64, addapt_numpy_float64)
register_adapter(np.int64, addapt_numpy_int64)


object0 = {'name': 'ZTF18aajivpr',
        'ra': 179.19543,
        'dec': 52.48386,
        'peakt': 59053.09,
           'source': 'ZTF',
           'band': ['ZTF_g', 'ZTF_r', 'ZTF_i'],
}
object1 = {'name': 'ZTF18aaunfqq',
        'ra': 238.57999,
        'dec': 39.45396,
        'peakt': 58268.39,
           'source': 'ZTF',
           'band': ['ZTF_g', 'ZTF_r', 'ZTF_i'],
}
object2 = {'name': 'DC21cozcn',
           'ra': 8.94907775241537,
           'dec': -42.7246132888024,
           'peakt': 59489,
           'source': ['DECam', 'Reduced'],
           'band': ['DECam_g', 'DECam_r', 'DECam_i'],
           'ref_file': ['/data/curveball/DECam_Reduced/References/ref_DC21cozcn_g.dat', 
                        '/data/curveball/DECam_Reduced/References/ref_DC21cozcn_r.dat',
                        '/data/curveball/DECam_Reduced/References/ref_DC21cozcn_i.dat'
                       ]
}
object3 = {'name': 'ZTF19abbwfgp',
           'ra': 258.323797675,
           'dec': 43.78431855625,
           'peakt': 58672.32,
           'source': 'ZTF',
           'band': ['ZTF_g', 'ZTF_r', 'ZTF_i'],
}
object4 = {'name': 'ZTF19abqhobb',
           'ra': 261.41111,
           'dec': 59.44673,
           'peakt': 58721.27,
           'source': 'ZTF',
           'band': ['ZTF_g', 'ZTF_r', 'ZTF_i'],
}
ztf_objects = pd.DataFrame({'name': ['ZTF19aanhhal',
                                     'ZTF19aapafit',
                                     'ZTF19aadnxog',
                                     'ZTF19aanovps',
                                     'ZTF19aavhblr',
                                    ],
                            'ra': [205.419793,
                                   247.725360956364,
                                   198.596151229412,
                                   237.02521,
                                   232.930764772727,
                                  ],
                            'dec': [55.669657,
                                    46.5884542418182,
                                    30.4851849882353,
                                    66.9025,
                                    16.7136958,
                            ],
                            'peakt': [58589.28,
                                      58605.39,
                                      58522.53,
                                      58584.37,
                                      58628.33,
                                     ],
                            'source': 'ZTF',
                            'bands': 'g r i',
                            'phot': 'psf ap',
                           })

decam_objects = pd.DataFrame({
    'name': ['DC21cozcn',
            'DC21djbfp',
            'DC21cyddn',
            'DC21cwcwp',
            'DC21cpcjp',
            'DC21ctkai',
            ],
    'ra': [8.94907775241537,
        7.01244, 
        8.19363,
        7.63242,
        9.56744,
        8.32920,
    ],
    'dec': [-42.7246132888024,
        -42.96940,
        -42.61276,
        -43.64061,
        -44.55215,
        -43.56573,
    ],
    'peakt': [59489,
        59510,
        59532,
        59505,
        59489,
        59492,
    ],
    'source': 'DECam',
    'secondary': 'Reduced',
    'bands': 'g r i',
    'phot': 'psf ap',
})

all_objects = np.array([object0, object1, object2, object3, object4])

def init(instrument):
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.DEBUG )
    
    config.Config.init( '/curveball/emily.yaml' , logger=logger )
    if not isinstance(instrument, str):
        es = exposuresource.ExposureSource.get(*instrument)
    else:
        es = exposuresource.ExposureSource.get(instrument)
    
    return es

def run(objects=None, bands=None):
    """ Takes a list of numerical indices and runs 
        make_reference on the corresponding objects """
    if isinstance(objects, int): objects = [objects]
    if isinstance(bands, int): bands = [bands]
    
    if objects is None:
        objects = all_objects
    else:
        objects = all_objects[objects]
    
    for i,obj in enumerate(objects):
        es = init(obj['source'])
        try:
            define_object(obj['name'], obj['ra'], obj['dec'], obj['peakt'])
        except:
            print(f"object { obj['name'] } exists")
        
        if bands is None:
            use_bands = obj['band']
        else:
            use_bands = np.array(obj['band'])[bands]
        
        for i,b in enumerate(use_bands):
            ref = None if 'ref_file' not in obj else obj['ref_file'][i]
            if ref is not None:
                make_reference(es, band=b, obj=obj['name'], imagefilename=ref)
            else:
                make_reference(es, band=b, obj=obj['name'])

def run_objects(df, expsource, idxs=None, out_folder='$HOME/curveball/emily_muckabout'):
    """ 
    Takes a DataFrame of objects (see ztf_objects, above) and runs buildltcv on them in parallel
    """
    if idxs is not None:
        df = df.iloc[idxs]
    for _,obj in df.iterrows():
        print(f"\n\nRUNNING OBJECT {obj['name']}\n\n")
        
        try:
            define_object(obj['name'], obj['ra'], obj['dec'], obj['peakt'])
        except:
            print(f"object { obj['name'] } exists")
        
        get_images_for_sn(expsource, obj['name'])
        
        bands = [f'{obj.source}_{b}' for b in obj['bands'].split(' ')]
        for b in bands:
            make_reference(expsource, band=b, obj=obj['name'])
            
            phots = [f'{phot}phot1' for phot in obj['phot'].split(' ')]
            for p in phots:
                run_buildltcv = f'mpirun -n 60 python -m mpi4py bin/buildltcv.py {obj["name"]} {obj["source"]} {b} -p {p}'
                if out_folder is not None:
                    run_buildltcv = 'nohup '+run_buildltcv
                    run_buildltcv += f' < /dev/null > {out_folder}/buildltcv_run_{obj["name"]}_{b[-1]}_{p}.out 2>&1 &'
                print(f"Running {run_buildltcv}")
                subprocess.run(run_buildltcv, shell=True, timeout=2400)
        

def undo_psf(expsource, basename):
    """ Takes a basename and exposure source and 
        removes files and database links for the PSF """
    # Filenames
    psffile = basename+'.psf'
    psfxmlfile = basename+'.psf.xml'
    catfile = basename+'.cat'
    
    # Fix database
    dbo = db.DB.get()
    img = db.Image.get_by_basename(basename, curdb=dbo)
    img.hascat = False
    dbo.db.commit()
    
    # Delete files from archive and local disk
    for file in [psffile, psfxmlfile, catfile]:
        expsource.delete_file_on_archive(file)
        p = expsource.local_path(file)
        p.unlink(missing_ok=True)

def make_psf(expsource, basename):
    img = expsource.local_path(basename+'.fits')
    weight = expsource.local_path(basename+'.weight.fits')
    mask = expsource.local_path(basename+'.mask.fits')
    
    processing.cat_psf_sky(expsource, img, weight, mask)

def run_fakes(expsource, outfile, img_names=None, method="multi-reference", photalg='both',
              band=None, nruns=1, trials=11, npsfs=50, seeing_cut=None, seed=42, **kwargs):
    # If img_names=None, it will choose at random from the database
    # Results will be saved in outfile
    # Methods:
    #    'single' will run all fakes on one image
    #    'multi-image' will run fakes on multiple images (must pass object='obj_name')
    #    'multi-reference' will run fakes on multiple images with different references
    # Alternatively, pass image names manually to use those
    dbo = db.DB.get()
    rng = np.random.default_rng( seed )
    
    # User-input values
    if img_names is not None:
        them = [db.Image.get_by_basename(basename, curdb=dbo) for basename in img_names]
        them = them * nruns
        
    elif method=='single':
        q = (dbo.db.query(db.Image).filter(db.Image.isstack==False)
            .join(db.Exposure).filter(db.Exposure.exposuresource_id==expsource.id)
            )
        if seeing_cut is not None:
            q = q.filter(db.Image.seeing < seeing_cut)
        if band is not None:
            q = q.join(db.Band).filter(db.Band.name==band)
        
        q = q.order_by(sa.func.random()).limit(1)
        
        them = q.all()
        them = them * nruns
        
    elif method=='multi-image':
        q = (dbo.db.query(db.Image).filter(db.Image.isstack==True)
             .join(db.Band).filter(db.Band.exposuresource_id==expsource.id)
            )
        if band is not None:
            q = q.filter(db.Band.name==band)
        
        q = q.order_by(sa.func.random()).limit(1)
        
        them = q.all()
        ref = db.ObjectReference.get_for_image(them[0], curdb=dbo)[0]
        images = db.Image.get_including_point(ref.object.ra, ref.object.dec, band=them[0].band,
                                              curdb=dbo)
        them = [img for img in images if not img.isstack]
        them = np.random.choice(them, nruns, replace=False)
    
    elif method=='multi-reference': # Take at random from the database
        q = (dbo.db.query(db.Image).filter(db.Image.isstack==False)
            .join(db.Exposure).filter(db.Exposure.exposuresource_id==expsource.id)
            )
        if seeing_cut is not None:
            q = q.filter(db.Image.seeing < seeing_cut)
        if band is not None:
            q = q.join(db.Band).filter(db.Band.name==band)
        
        q = q.order_by(sa.func.random()).limit(nruns)
        
        them = q.all()
    
    images = pd.DataFrame({'basename': [img.basename for img in them],
                           'band': [img.band.name for img in them],
                           'seeing': [img.seeing for img in them],
                           'ref': [db.ObjectReference.get_for_image(img, curdb=dbo)[0].image.basename 
                                   for img in them],
                          })
    
    seeds = np.random.randint(0, 100, size=len(images))
    if photalg=='both':
        photalgs = ['apphot1', 'psfphot1']
    else: photalgs = [photalg]
    
    all_data = []
    for i,row in images.iterrows():
        fakeinj = FakeInjector(expsource, row.basename, row.ref)
        
        alg_data = []
        for phot in photalgs:
            rng = np.random.default_rng(seeds[i])
            print(f"\nRunning fakinj {trials} trials, {npsfs} psfs, photalg {phot}, seed = {seeds[i]}\n")
            df = fakeinj.repeatedtrials(trials, npsfs=npsfs, photalg=phot, rng=rng, **kwargs)
            alg_data.append(df)
        
        if len(alg_data) > 1:
            df = alg_data[0].merge(alg_data[1], on=['x', 'y'], suffixes=['_ap', '_psf'])
            if len(df) != len(alg_data[0]):
                print("WARNING: lost some data in merging ap and psf")
        else:
            df = alg_data[0]
        
        df['img'] = row.basename
        df['ref'] = row.ref
        
        all_data.append(df)
    
    all_data = pd.concat(all_data, ignore_index=True)
    all_data.to_csv(outfile, index=False)
        
    
    
    
    
    
    
    
    
    
    
    