import sys
from mpi4py import MPI

def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    values = []
    ranks = []
    if rank == 0:
        status = MPI.Status()
        for i in range(1,size):
            val = comm.recv( status=status )
            values.append( val )
            ranks.append( status.Get_source() )
    else:
        comm.send( rank, 0 )

    values = comm.bcast( values, root=0 )
    ranks = comm.bcast( ranks, root=0 )

    sys.stderr.write( f'Rank {rank} : { ", ".join( [ f"{i}={j}" for i,j in zip(ranks, values) ] ) }\n' )

# ======================================================================

if __name__ == "__main__":
    main()
