import pathlib
import config

# Create a hacked config object that will have the right database fields
# for the specified tag.

def initconfig( scriptdir, tag ):
    cpath = pathlib.Path( scriptdir ) / f"webapconfig.yaml"
    if not cpath.is_file():
        raise FileNotFoundError( f"Can't find webapconfig.yaml" )
    configobj = config.Config.get( cpath, nocache=True )
    if tag is not None:
        if tag not in configobj.value( "webap.db" ):
            raise ValueError( f"Unknown tag {tag}" )
        dbdict = configobj.value( "webap.db" )[ tag ]
        for key in ( "engine", "database", "username", "host", "port", "passwordfile" ):
            if key in dbdict:
                if key not in dbdict:
                    raise RuntimeError( f"Missing config value for {tag}: {key}" )
                configobj.set_value( f"database.{key}", dbdict[ key ] )
    return configobj
