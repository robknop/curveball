var rkWebUtil = {}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Utility functions

rkWebUtil.wipeDiv = function( div )
{
    if ( div != null )
        while ( div.firstChild )
            div.removeChild( div.firstChild );
}

// **********************************************************************

rkWebUtil.elemaker = function( elemtype, parent, inprops )
{
    var props = { text: null, click: null, classes: null, attributes: null };
    Object.assign( props, inprops );
    var text = props.text
    var click = props.click;
    var classes = props.classes;
    var attributes = props.attributes;

    var attr;
    
    var elem = document.createElement( elemtype );
    if ( parent != null) parent.appendChild( elem );
    if ( text != null )
        elem.appendChild( document.createTextNode( text ) );
    if ( click != null )
        elem.addEventListener( "click", click );
    if ( classes != null ) {
        for ( let classname of classes ) {
            elem.classList.add( classname )
        }
    }
    if ( attributes != null ) {
        for ( attr in attributes ) {
            if ( attributes.hasOwnProperty( attr ) ) {
                elem.setAttribute( attr, attributes[attr] );
            }
        }
    }
    return elem;
}

// **********************************************************************

rkWebUtil.button = function( parent, title, callback, attributes={} )
{
    var button = document.createElement( "button" );
    button.appendChild( document.createTextNode( title ) );
    if ( callback != null ) {
        button.addEventListener( "click", callback );
    }
    for ( let attr in attributes ) {
        button.setAttribute( attr, attributes[attr] );
    }
    if ( parent != null ) parent.appendChild( button );
    return button;
}

// **********************************************************************
// If I ever get a date that doesn't start "2020-07-15 07:42:00" (with
// any old character in place of the space), I'm in trouble.  Alas,
// I haven't found a reliable library routine to do this, because
// Javascript insists on dates being local.

rkWebUtil.parseStandardDateString = function( datestring )
{
    // console.log( "Trying to parse date string " + datestring );
    var year = Number( datestring.substring(0, 4) );
    var month = Number( datestring.substring(5, 7) );
    var day = Number( datestring.substring( 8, 10) );
    var hour = Number( datestring.substring( 11, 13) );
    var minute = Number( datestring.substring( 14, 16) );
    var second = Number( datestring.substring( 17, 19) );
    var date;
    date = new Date( Date.UTC( year, month-1, day, hour, minute, second ) );
    // console.log( "y=" + year + " m=" + month + " d=" + day +
    //              "h=" + hour + " m=" + minute + " s=" + second );
    // console.log( "Date parsed: " + date.toString() );
    return date;
}

// **********************************************************************

rkWebUtil.dateFormat = function(date)
{
    // console.log(" Trying to format date " + date );
    var result = "";
    result += ("0000" + date.getFullYear().toString()).substr(-4, 4);
    result += "-";
    result += ("00" + (date.getMonth()+1).toString()).substr(-2, 2);
    result += "-";
    result += ("00" + date.getDate().toString()).substr(-2, 2);
    result += " ";
    result += ("00" + date.getHours().toString()).substr(-2, 2);
    result += ":";
    result += ("00" + date.getMinutes().toString()).substr(-2, 2);
    result += ":";
    result += ("00" + date.getSeconds().toString()).substr(-2, 2);
    return result;
}

rkWebUtil.dateUTCFormat = function(date)
{
    var result = "";
    result += ("0000" + date.getUTCFullYear().toString()).substr(-4, 4);
    result += "-";
    result += ("00" + (date.getUTCMonth()+1).toString()).substr(-2, 2);
    result += "-";
    result += ("00" + date.getUTCDate().toString()).substr(-2, 2);
    result += " ";
    result += ("00" + date.getUTCHours().toString()).substr(-2, 2);
    result += ":";
    result += ("00" + date.getUTCMinutes().toString()).substr(-2, 2);
    result += ":";
    result += ("00" + date.getUTCSeconds().toString()).substr(-2, 2);
    return result;
}

rkWebUtil.validateWidgetDate = function( datestr ) {
    if ( datestr.value.trim() == "" ) {
        datestr.value = ""
        return;
    }
    let num = Date.parse( datestr.value.trim() );
    if ( isNaN(num) ) {
        window.alert( "Error parsing date " + datestr.value
                      + "\nNote that Javascript's Date.parse() doesn't seem to like AM or PM at the end; "
                      + "use 24-hour time." );
        return;
    }
    let date = new Date();
    date.setTime( num );
    // TODO : verify that both Firefox and Chrome (at least) parse this right
    // (I don't just use toISOString because the T at the middle and all the precision is ugly for the user)
    datestr.value = date.getUTCFullYear() + "-"
        + String(date.getUTCMonth()+1).padStart( 2, '0') + "-"
        + String(date.getUTCDate()).padStart( 2, '0' ) + " "
        + String(date.getUTCHours()).padStart( 2, '0') + ":"
        + String(date.getUTCMinutes()).padStart(2, '0') + ":"
        + String(date.getUTCSeconds()).padStart( 2, '0') + "+00";
}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Class for connecting to a given web server

rkWebUtil.Connector = function( app )
{
    this.app = app;
}

// **********************************************************************
// Utility function used in HTTP request callbacks to make
//  sure the data is in.  Returns false if the data is not in,
//  returns true if it is.  Returns null if there
//  was an error (including non-JSON response).
//
// errorhandler is a function that takes a single argument
// that argument will have property "error" which is a string message

rkWebUtil.Connector.prototype.waitForJSONResponse = function( request, errorhandler = null )
{
    var type;

    // console.log( "request.readyState = " + request.readyState +
    //              ", request.stauts = " + request.status );
    if (request.readyState === 4 && request.status === 200) {
        type = request.getResponseHeader("Content-Type");
        if (type === "application/json")
        {
            return true;
        }
        else
        {
            if ( errorhandler != null ) {
                errorhandler( { "error": "Request didn't return JSON" }  );
            }
            else {
                window.alert("Request didn't return JSON.  Everything is broken.  Panic.");
            }
            return null;
        }
    }
    else if (request.readyState == 4) {
        if ( errorhandler != null ) {
            errorhandler( { "error": "Got back HTTP status " + request.status } );
        }
        else {
            window.alert("Woah, got back status " + request.status + ".  Everything is broken.  Panic.");
        }
        return null;
    }
    else {
        return false;
    }
}

// **********************************************************************
// Utility funciton to created and send an XMLHttpRequest, and wait
// for it to be fully finished before calling the handler.
//   appcommand -- the thing after the webapp, e.g. "/getquestionset"
//   data -- an object to be converted with JSON.stringify and sent
//   handler -- a function to be called when the request has fully returned
//              it will have one argument, the data from the request
//   errorhandler -- a function that takes a single argument that will
//                   have property "error" which is a string message

rkWebUtil.Connector.prototype.sendHttpRequest = function( appcommand, data, handler, errorhandler = null )
{
    let self = this;
    let req = new XMLHttpRequest();
    req.open( "POST", this.app + appcommand );
    req.onreadystatechange = function() { self.catchHttpResponse( req, handler, errorhandler=errorhandler ) };
    req.setRequestHeader( "Content-Type", "application/json" );
    console.log( "Sending data " + JSON.stringify(data) );
    req.send( JSON.stringify( data ) );
}

rkWebUtil.Connector.prototype.sendHttpRequestMultipartForm = function( appcommand, formdata,
                                                                       handler, errorhandler = null )
{
    let self = this;
    let req = new XMLHttpRequest();
    req.open( "POST", this.app + appcommand );
    req.onreadystatechange = function() { self.catchHttpResponse( req, handler, errorhandler ) };
    req.send( formdata );
}

rkWebUtil.Connector.prototype.catchHttpResponse = function( req, handler, errorhandler = null )
{
    if ( ! this.waitForJSONResponse( req, errorhandler ) ) return;
    var statedata = JSON.parse( req.responseText );
    if ( statedata.hasOwnProperty( "error" ) ) {
        if ( errorhandler != null ) {
            errorhandler( statedata );
        }
        else {
            window.alert( 'Error return: ' + statedata.error );
            if ( statedata.hasOwnProperty( "traceback" ) ) {
                console.log( statedata.traceback );
            }
        }
        return;
    }
    handler( statedata );
}

// **********************************************************************
// I'm honestly not sure how I want to format text, but I THINK I
// want to have paragraphs separated by \n\n.  This strips all the <p>
// and </p> tags from the text to move to that convention.

rkWebUtil.stripparagraphtags = function(text)
{
    var verystartpar = new RegExp("^\s*<p>", "g");
    var startpar = new RegExp("\n\s*<p>", "g");
    var endpar = new RegExp("</p>\s*\n", "g");
    var veryendpar = new RegExp("</p>\s*$", "g");

    var newtext = text.replace(verystartpar, "");
    newtext = newtext.replace(startpar, "\n");
    newtext = newtext.replace(endpar, "\n");
    newtext = newtext.replace(veryendpar, "");

    return newtext;
}

// **********************************************************************

rkWebUtil.mjdofdate = function( y, m, d, h, minute, s )
{
    // Trusting Wikipedia....  I didn't use Math.floor because I was
    // getting slightly wrong answers; floating-point roundoff?  Using
    // Math.floor to approximate integer division will do that.  I'm
    // gobsmacked that javascript doesn't have an explicit
    // float-to-integer coversion, nor integer division.  The ~~ is a
    // hack I found on the net that converts floating point numbers to integers.
    if ( h < 12 ) {
        d -= 1;
    }
    let jd = ( ~~( 1461 * ( y + 4800 + ~~( (m - 14) / 12 ) ) / 4 ) +
               + ~~( (367 * (m - 2 - 12 * ~~( (m - 14) / 12 ) ) ) / 12 )
               - ~~( (3 * ~~( (y + 4900 + ~~( (m - 14) / 12) ) / 100 ) ) / 4 )  + d - 32075 );
    jd += ( (h-12) + minute/60. + s/3600. ) / 24.;
    if (h < 12) jd += 1.;
    return jd - 2400000.5;
}

// **********************************************************************

rkWebUtil.ymdofmjd = function( mjd )
{
    // Again trusting Wikipedia
    // and using ~~ to convert to integers and hoping
    // that that will make the divisions and moduli right.
    // I probably did it more often than necessary,
    // but floating-point roundoff is a monster.
    //
    // There's another issue: this formula gives the date for the
    // afternoon at the beginning of the juilian day.  In other words,
    // for hours 0-12, the date is going to be one too low.
    // Fix this by taking the floor of the mjd, then adding 0.5 to
    // the jd you get from that floored mjd, and rounding

    let intmjd = ~~mjd
    let jd = ~~( intmjd + 2400000.5 + 0.5 )
    let y = 4716;
    let j = 1401;
    let m = 2;
    let n = 12;
    let r = 4;
    let p = 1461;
    let v = 3;
    let u = 5;
    let s = 153;
    let w = 2;
    let B = 274277;
    let C = -38;

    let f = jd + j + ~~( ~~( ~~( ~~(4 * jd + B) / 146097) * 3) / 4) + C;
    let e = r * f + v;
    let g = ~~( ( e % p ) / r );
    let h = u * g + w;
    let D = ~~ ( ( h % s ) / u ) + 1;
    let M = ~~( ~~( ~~( h / s ) + m ) % n ) + 1;
    let Y = ~~( e / p) - y + ~~( (n + m - M) / n );
    return [ Y, M, D ];
}

rkWebUtil.dateofmjd = function( mjd )
{
    let tmp = rkWebUtil.ymdofmjd( mjd );
    let Y = tmp[0];
    let M = tmp[1];
    let D = tmp[2];
    // Try to handle floating point roundoff
    let intmjd = ~~( rkWebUtil.mjdofdate( Y, M, D, 0, 0, 0 ) + 0.5 )
    // This next thing will only happne with a floating point
    // roundof from <something>.9999999.  In this case, just
    // round up to the next integer mjd
    if ( ( mjd - intmjd ) < 0 ) {
        intmjd -= 1;
        mjd = intmjd;
    }   
    let secs = ( mjd - Math.floor( mjd ) ) * 24 * 3600;
    let h = ~~( Math.floor( secs / 3600 ) );
    let m = ~~( Math.floor( ( secs - 3600*h ) / 60 ) );
    let s = ~~( secs - 3600*h - 60*m );
    let mus = Math.floor( 1e6 * ( secs - 3600*h - 60*m - s ) + 0.5 );
    // Another floating-point roundoff issue
    let soff = 0;
    if ( mus >= 1000000 ) {
        mus -= 1000000;
        soff = 1;
    }
    let datetime = new Date( Date.UTC( Y, M-1, D, h, m, s, mus/1000 ) );
    if ( soff != 0 ) {
        datetime.setSeconds( datetime.getSeconds() + soff );
    }
    return datetime;
}

// **********************************************************************

export { rkWebUtil }

