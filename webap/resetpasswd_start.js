import { rkAuth } from "./rkauth.js"

// ****
// Set the variable webapurl
import { Curveball } from "./curveball.js"
var webapurl = Curveball.webapurl;
// ****

rkAuth.started = false;
rkAuth.init_interval = window.setInterval(
    function()
    {
        var requestdata, renderer;
        if ( document.readyState == "complete" ) {
            if ( !rkAuth.started ) {
                rkAuth.started = true;
                window.clearInterval( rkAuth.init_interval );
                let div = document.getElementById( "authdiv" );
                let button = document.getElementById( "setnewpassword_button" );
                let tagelem = document.getElementById( "curveball_context" );
                let auther = new rkAuth( div, webapurl, tagelem.value, null );
                button.addEventListener( "click", function() { auther.getPrivKey() } );
            }
        }
    },
    100);
            
    
