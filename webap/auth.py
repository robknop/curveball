#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import uuid
import pathlib
import binascii
import traceback
import json
from datetime import datetime
import pytz
import web
from web import form
import Crypto.PublicKey.RSA
import Crypto.Cipher.PKCS1_v1_5

scriptdir = pathlib.Path( __file__ ).parent
sys.path.insert( 0, str(scriptdir) )
import curveball_initconfig
import config
import db

# This one was hacked for curveball specifically
# Some of the docs below are wrong as a result

#### HOW TO USE
#
# This isn't as modular as I'd like.  Right now, you need to have
#   a module "db" that uses SQLAlchemy and implements the following classes:
#
# User
#   properties: id UUID,
#               displayname TEXT
#               email TEXT
#               pubkey TEXT
#               privkey TEXT
#               lastlogin TEXT
# PasswordLink
#   properties: id UUID
#               userid UUID with foreign key link to AuthUser.id
#               expires TIMESTAMP WITH TIME ZONE
#
# Add the module as a submodule to your main webap; the code there needs:
#
# import auth
# ...
# urls = ( ...
#          "/auth", auth.app
#        )
# ...
# app = web.application( urls, locals() )
# ...
# initializer = { ... }
# initializer.update { auth.initializer }
# session = web.session.Session( app, web.session.DiskStore(...), initalizer=initializer )
# def session_hook(): web.ctx.session = session 
# app.add_processor( web.loadhook( session_hook ) ) 
#
# web.smtp also needs to be fully configured
#
# Access the session via web.ctx.session
# Variables in the session:
#    authenticated : True or False
#    useruuid
#    username
#    userdisplayname
#    useremail
#    ( there's also authuuid, a temporary throwaway value )
#
# (Won't work with web.py templates, see https://webpy.org/cookbook/sessions_with_subapp )
#
# CLIENT SIDE: use rkauth.js and resetpasswd_start.js

# ======================================================================

class HandlerBase:

    # This is a direct copy of HandlerBase.initconfig from curveball.py
    # I should organize things better
    def initconfig( self, tag ):
        # sys.stderr.write( f"***** In auth initconfig : tag={tag} ******" )
        global scriptdir
        self.config = curveball_initconfig.initconfig( scriptdir, tag )
    
    def GET( self, *args, **kwargs ):
        return self.do_the_things( *args, **kwargs )

    def POST( self, *args, **kwargs ):
        return self.do_the_things( *args, **kwargs )

    def jsontop( self, tag ):
        web.header('Content-Type', 'application/json')
        self.initconfig( tag )
    
# ======================================================================

class GetAuthChallenge(HandlerBase):
    def __init__( self ):
        super().__init__()

    def do_the_things( self, tag ):
        try:
            self.jsontop( tag )
            if not tag in web.ctx.session:
                setattr( web.ctx.session, tag, { 'username': None,
                                                 'useruuid': None,
                                                 'userdisplayname': None,
                                                 'useremail': None,
                                                 'authenticated': False,
                                                 'authuuid': None } )
            sesstag = getattr( web.ctx.session, tag )
            sesstag[ 'authenticated' ] = False
            inputdata = json.loads( web.data().decode(encoding="utf-8") )
            if not 'username' in inputdata:
                return json.dumps( { 'error': 'No username sent to server' } )
            sys.stderr.write( f"In GetAuthChallenge; tag={tag}; username={inputdata['username']}; "
                              f"sesstag={sesstag}; self.config.value('database')={self.config.value('database')}\n" )
            with db.DB.get( context=tag, cfg=self.config ) as dbsess:
                sys.stderr.write( f"Connected to database: {dbsess.db.get_bind()}\n" )
                users = db.User.getbyusername( inputdata['username'], cfg=self.config, curdb=dbsess )
                if len(users) > 1:
                    return json.dumps( { 'error': f'User {inputdata["username"]} is multiply defined; this is bad!' } )
                if len(users) == 0:
                    return json.dumps( { 'error': f'No such user {inputdata["username"]}' } )
                user = users[0]
            tmpuuid = str( uuid.uuid4() )
            pubkey = Crypto.PublicKey.RSA.importKey( user.pubkey )
            cipher = Crypto.Cipher.PKCS1_v1_5.new( pubkey )
            challenge = binascii.b2a_base64( cipher.encrypt( tmpuuid.encode("UTF-8") ) ).decode( "UTF-8" )
            sesstag[ 'username' ] = user.username
            sesstag[ 'useruuid' ] = user.id
            sesstag[ 'userdisplayname' ] = user.displayname
            sesstag[ 'useremail' ] = user.email
            sesstag[ 'authuuid' ] = tmpuuid
            retdata = { 'username': user.username,
                        'privkey': user.privkey,
                        'challenge': challenge }
            return json.dumps( retdata )
        except Exception as e:
            sys.stderr.write( f'{traceback.format_exc()}\n' )
            return json.dumps( { 'error': f'Exception in GetAuthChallenge: {str(e)}' } )

class RespondAuthChallenge(HandlerBase):
    def __init__( self ):
        super().__init__()

    def do_the_things( self, tag ):
        try:
            self.jsontop( tag )
            inputdata = json.loads( web.data().decode(encoding="utf-8") )
            if ( ( not 'username' in inputdata ) or
                 ( not 'response' in inputdata ) ):
                return json.dumps( { 'error': ( 'Login error: username or challenge response missing; '
                                                ' (you probably can\'t fix this, contact code maintainer) '
                                               ) } )
            if not hasattr( web.ctx.session, tag ):
                return json.dumps( { 'error': f'Tag {tag} not found in session.  Panic.' } )
            sesstag = getattr( web.ctx.session, tag )
            if inputdata['username'] != sesstag[ 'username' ]:
                return json.dumps( { 'error': ( f'username {username} didn\'t match session username '
                                                '{sesstag["username"]}; try logging out and logging back in '
                                               ) } )
            if sesstag[ "authuuid" ] != inputdata['response']:
                return json.dumps( { 'error': 'Authentication failure.' } )
            sesstag[ "authenticated" ] = True
            return json.dumps(
                { 'status': 'ok',
                  'message': f'User {sesstag["username"]} logged in.',
                  'username': sesstag["username"],
                  'useruuid': str( sesstag["useruuid"] ),
                  'useremail': sesstag["useremail"],
                  'userdisplayname': sesstag["userdisplayname"],
                 } )
        except Exception as e:
            sys.stderr.write( f'{traceback.format_exc()}\n' )
            return json.dumps( { 'error': f'Exception in RespondAuthChallenge: {str(e)}' } )
        

# ======================================================================

class GetPasswordResetLink(HandlerBase):
    def __init__( self ):
        super().__init__()

    def do_the_things( self, tag ):
        try:
            self.jsontop( tag )
            inputdata = json.loads( web.data().decode(encoding="utf-8") )
            with db.DB.get( context=tag, cfg=self.config ) as dbsess:
                if 'username' in inputdata:
                    username = inputdata['username']
                    # sys.stderr.write( f"GetPasswordResetLink: looking for username \"{username}\"\n" )
                    them = db.User.getbyusername( username, cfg=self.config, curdb=dbsess );
                    if len( them ) == 0:
                        return json.dumps( { "error": f"username {username} not known" } )
                    if len( them ) > 1:
                        return json.dumps( { "error": ( f"username {username} is multiply defined! "
                                                        "This is bad.  This is very bad." ) } )
                elif 'email' in inputdata:
                    email = inputdata['email']
                    them = db.User.getbyemail( email, cfg=self.config, curdb=dbsess )
                    if len( them ) == 0:
                        return json.dumps( { "error": f"email {email} not known" } )
                sentto = ""
                for user in them:
                    link = db.PasswordLink.new( user.id, curdb=dbsess )
                    web.sendmail( "raknop@lbl.gov", user.email, "Curveball webap password reset",
                                  f'Somebody requested a password reset for {user.username}\n' +
                                  f'for the Curveball webap.  This link will expire in 1 hour.\n'
                                  f'If you did not request this, you may ignore this message.\n' +
                                  f'Here is the link; cut and paste it into your browser:\n\n' +
                                  f'{web.ctx.home}/{tag}/resetpassword?uuid={str(link.id)}\n\n' +
                                  f'(This is an automated email.)\n' )
                    if len(sentto) > 0:
                        sentto += " "
                    sentto += user.username
            return json.dumps( { 'status': f'Password reset link(s) sent for {sentto}.' } )
        except Exception as e:
            sys.stderr.write( f'{traceback.format_exc()}\n' )
            return json.dumps( { 'error': f'Exception in GetPasswordResetLink: {str(e)}' } )

        
# ======================================================================

class ResetPassword(HandlerBase):
    def __init__( self ):
        super().__init__()

    def do_the_things( self, tag ):
        # Normally initconfig is run in jsontop, but we don't run that here
        self.initconfig( tag )
        web.header('Content-Type', 'text/html; charset="UTF-8"')
        response = "<!DOCTYPE html>\n"
        response += "<html>\n<head>\n<meta charset=\"UTF-8\">\n"
        response += f"<title>Password Reset</title>\n"

        self.initconfig( tag )
        
        webapdirurl = str( pathlib.Path( web.ctx.env['SCRIPT_NAME'] ).parent )
        if webapdirurl == "/":
            webapdirurl = ""
        # response += "<link href=\"" + webapdirurl
        # response += "photodb.css\" rel=\"stylesheet\" type=\"text/css\">\n"
        response += "<script src=\"" + webapdirurl + "/aes.js\"></src>\n"
        response += "<script src=\"" + webapdirurl + "/jsencrypt.min.js\"></script>\n"
        response += "<script src=\"" + webapdirurl + "/resetpasswd_start.js\" type=\"module\"></script>\n"
        response += "</head>\n<body>\n"
        response += f"<h1>Reset Password</h1>\n<p><b>ROB Todo: make this header better</b></p>\n";

        try:
            inputdata = web.input()
            if not hasattr( inputdata, "uuid" ):
                response += "<p>Malformed password reset URL.</p>"
                return response
            with db.DB.get( context=tag, cfg=self.config ) as dbsess:
                pwlink = db.PasswordLink.get( inputdata.uuid, cfg=self.config, curdb=dbsess )
                if pwlink is None:
                    response += "<p>Invalid password reset URL.</p>"
                    return response
                if pwlink.expires.replace( tzinfo=pytz.utc ) < datetime.now(pytz.utc):
                    response += "<p>Password reset link has expired.</p>"
                    return response
                user = db.User.get( pwlink.userid, cfg=self.config, curdb=dbsess )

            response += f"<h2>Reset password for {user.username}</h2>\n"
            response += "<div id=\"authdiv\">"
            response += "<table>\n"
            response += "<tr><td>New Password:</td><td>"
            response += form.Input( name="newpassword", id="reset_password",
                                    type="password", size=20 ).render()
            response += "</td></tr>\n"
            response += "<tr><td>Confirm:</td><td>"
            response += form.Input( name="confirmpassword", id="reset_confirm_password",
                                    type="password", size=20 ).render()
            response += "</td></tr>\n"
            response += "<tr><td colspan=\"2\">"
            response += form.Button( name="getnewpassword", id="setnewpassword_button",
                                     html="Set New Password" ).render()
            response += "</td></tr>\n</table>\n"
            response += "</div>\n"
            response += form.Input( name="linkuuid", id="resetpasswd_linkid", type="hidden",
                                    value=str(pwlink.id) ).render()
            response += form.Input( name="curveball_context", id="curveball_context", type="hidden",
                                    value=tag ).render()
            
            response += "</body>\n</html>\n"
            return response
        except Exception as e:
            sys.stderr.write( f'{traceback.format_exc()}\n' )
            response += f'Exception in ResetPassword: {str(e)}'
            return response
                
            
# ======================================================================

class GetKeys(HandlerBase):
    def __init__( self ):
        super().__init__()

    def do_the_things( self, tag ):
        try:
            self.jsontop( tag )
            inputdata = json.loads( web.data().decode(encoding="utf-8") )
            if 'passwordlinkid' not in inputdata:
                return json.dumps( { "error": "No password link id specified" } )
            with db.DB.get( context=tag, cfg=self.config ) as dbsess:
                link = db.PasswordLink.get( inputdata['passwordlinkid'], cfg=self.config, curdb=dbsess )
                if link is None:
                    return json.dumps( { "error": "Invalid password link id" } )
                linkdate = pytz.utc.localize( link.expires )
                if linkdate < datetime.now(pytz.utc):
                    return json.dumps( { "error": "Password reset link has expired" } )
            keys = Crypto.PublicKey.RSA.generate( 2048 )
            return json.dumps( { "privatekey": keys.exportKey().decode("UTF-8"),
                                 "publickey": keys.publickey().exportKey().decode("UTF-8") } )
        except Exception as e:
            sys.stderr.write( f'{traceback.format_exc()}\n' )
            return json.dumps( { "error": f"Exception in GetKeys: {str(e)}" } )
            
# ======================================================================

class ChangePassword(HandlerBase):
    def __init__( self ):
        super().__init__()

    def do_the_things( self, tag ):
        try:
            self.jsontop( tag )
            inputdata = json.loads( web.data().decode(encoding="utf-8") )
            if not "passwordlinkid" in inputdata:
                return json.dumps( { "error": "Call to changepassword without passwordlinkid" } )
            if not "publickey" in inputdata:
                return json.dumps( { "error": "Call to changepassword without publickey" } )
            if not "privatekey" in inputdata:
                return json.dumps( { "error": "Call to changepassword without privatekey" } )

            with db.DB.get( context=tag, cfg=self.config ) as dbsess:
                pwlink = db.PasswordLink.get( inputdata['passwordlinkid'], curdb=dbsess )
                user = db.User.get( pwlink.userid, curdb=dbsess )
                user.pubkey = inputdata['publickey']
                user.privkey = inputdata['privatekey']
                dbsess.db.delete( pwlink )
                dbsess.db.commit()
                return json.dumps( { "status": "Password changed" } )
        except Exception as e:
            sys.stderr.write( f'{traceback.format_exc()}\n' )
            return json.dumps( { 'error': f'Exception in ChangePassword: {str(e)}' } )
            
            
# ======================================================================

class CheckIfAuth(HandlerBase):
    def __init__( self ):
        super().__init__()

    def do_the_things( self, tag ):
        self.jsontop( tag )
        if ( not hasattr( web.ctx, 'session' ) ) or ( not hasattr( web.ctx.session, tag ) ):
            return json.dumps( { 'status': False } )
        sesstag = getattr( web.ctx.session, tag )
        if ( 'authenticated' in sesstag and sesstag['authenticated'] ):
            return json.dumps( { 'status': True,
                                 'username': sesstag['username'],
                                 'useruuid': str( sesstag['useruuid'] ),
                                 'useremail': sesstag['useremail'],
                                 'userdisplayname': sesstag['userdisplayname'],
                                } )
        return json.dumps( { 'status': False } );

# ======================================================================

class Logout(HandlerBase):
    def __init__( self ):
        super().__init__()

    def do_the_things( self, tag ):
        self.jsontop( tag )
        if hasattr( web.ctx.session, tag ):
            sys.stderr.write( f"In logout, tag={tag}, tag data={getattr(web.ctx.session, tag)}\n" )
            delattr( web.ctx.session, tag )
        sys.stderr.write( f"End of logout, hasattr(web.ctx.session,tag)={hasattr(web.ctx.session,tag)}\n" )
        return json.dumps( { 'status': 'Logged out' } )

# ======================================================================

initializer = {}
urls = ( "/(.+)/getchallenge", "GetAuthChallenge",
         "/(.+)/respondchallenge", "RespondAuthChallenge",
         "/(.+)/getpasswordresetlink", "GetPasswordResetLink",
         "/(.+)/resetpassword", "ResetPassword",
         "/(.+)/getkeys", "GetKeys",
         "/(.+)/changepassword", "ChangePassword",
         "/(.+)/isauth", "CheckIfAuth",
         "/(.+)/logout", "Logout"
)

app = web.application( urls, locals() )

