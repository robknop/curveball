import { Curveball } from "./curveball.js"

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Wait for the page to load and then do stuff

Curveball.started = false;
Curveball.init_interval = window.setInterval(
    function() {
        var requestdata, renderer;
        if ( document.readyState == "complete" ) {
            if ( !Curveball.started ) {
                Curveball.started = true;
                window.clearInterval( Curveball.init_interval );
                renderer = new Curveball.Context();
                renderer.init();
            }
        }
    },
    100);
