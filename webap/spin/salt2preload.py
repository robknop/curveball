import sncosmo

model = sncosmo.Model("salt2")
g = model.bandflux("ztfg", 0)
r = model.bandflux("ztfr", 0)
i = model.bandflux("ztfi", 0)
b = model.bandflux("bessellb", 0)
