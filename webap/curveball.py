import sys
import os
import io
import math
import base64
import traceback
import pathlib
import json
import uuid
import subprocess
import datetime
import pytz
import web
from web import form
import numpy
import sqlalchemy
import psycopg2.extras

import sncosmo

scriptdir = str( pathlib.Path( __file__ ).parent )
if scriptdir not in sys.path:
    sys.path.insert(0, scriptdir )

import curveball_initconfig
from util import sanitizeHTML
import config
import db
import auth

session = None

# ======================================================================

def secure_session( initializer ):
    global session, app
    session = web.session.Session( app, web.session.DiskStore( "/sessions" ), initializer=initializer )

# ======================================================================

class HandlerBase(object):
    def __init__(self):
        global scriptdir
        self.response = ""

    def initconfig( self, tag ):
        # sys.stderr.write( f"****** In curveball initconfig : tag={tag} *****" )
        global scriptdir
        self.config = curveball_initconfig.initconfig( scriptdir, tag )

    def verifyauth( self, tag ):
        if ( not hasattr( web.ctx.session, tag ) ):
            raise RuntimeError( "User not authenticated" )
        sesstag = getattr( web.ctx.session, tag )
        if ( ( not ( "authenticated" ) in sesstag ) or ( not sesstag["authenticated"] ) ):
            raise RuntimeError( f"User not authenticated for database {tag}" )
        
    def noauthhtmltop( self ):
        web.header('Content-Type', 'text/html; charset="UTF-8"')
        self.initconfig( None )
        self.response = "<!DOCTYPE html>\n"
        self.response += "<html lang=\"en\">\n<head>\n<meta charset=\"UTF-8\">\n"
        self.response += f"<link rel=\"stylesheet\" href=\"{self.config.value('webap.dirurl')}curveball.css\">\n"
        self.response += f"<link rel=\"stylesheet\" href=\"{self.config.value('webap.dirurl')}svgplot.css\">\n"
        self.response += ( f"<script src=\"{self.config.value('webap.dirurl')}curveball.js\" "
                           f"type=\"module\"></script>\n" )
        self.response += ( f"<script src=\"{self.config.value('webap.dirurl')}aes.js\"></script>\n" )
        self.response += ( f"<script src=\"{self.config.value('webap.dirurl')}jsencrypt.min.js\"></script>\n" )
        self.response += ( f"<script src=\"{self.config.value('webap.dirurl')}curveball_init.js\" "
                           f"type=\"module\"></script>\n" )
        self.response += "<title>Curveball Webap</title>\n"
        self.response += "</head>\n<body>\n"

    def htmltop( self, tag ):
        self.noauthhtmltop()
        self.response += f"<h1>Curveball Webap — Database {sanitizeHTML(tag,True)}</h1>\n"
        fail = True
        try:
            self.initconfig( tag )
            fail = False
        except FileNotFoundError as e:
            self.response += "<p>ERROR: Couldn't find web ap config!</p>"
        except ValueError as e:
            self.response += "<p>ERROR: unknown context {tag}</p>"
        except Exception as e:
            self.response += "<p>EXCEPTION: {sanitizeHTML(str(e))}</p>"
        if fail:
            self.htmlbottom()
            return False
        self.response += ( "<input id=\"curveball_context\" type=\"hidden\" name=\"curveball_context\" "
                           f"value=\"{tag}\">" )
        self.response += "<div id=\"rkauth_div\"></div>\n"
        try:
            self.verifyauth( tag )
        except RuntimeError as e:
            self.response += "<p>Log in to continue.</p>"
            self.htmlbottom()
            return False
        # self.response += f"<p><a href=\"/curveball.py/{tag}/\">[Back to Home]</a></p>\n"
        self.response += "<div id=\"contentdiv\"><p class=\"warn\">Loading...</p></div>"
        return True

    def jsontop( self, tag ):
        web.header( 'Content-Type', 'application/json' )
        self.initconfig( tag )
        self.verifyauth( tag )
    
    def htmlbottom(self):
        self.response += "\n</body>\n</html>\n"

    def finalize(self):
        pass

    def errordump( self, exception, message=None ):
        resp = { 'status': 'error',
                 'error': f'Exception in {self.__class__.__name__}: {str(exception)}',
                 'message': message }
        with io.StringIO() as strstream:
            traceback.print_exc( file=strstream )
            resp['traceback'] = strstream.getvalue()
        sys.stderr.write( f"Returning errordump\n{json.dumps(resp)}\n" )
        return json.dumps( resp )

    def htmlerrordump( self, exception, message=None ):
        resp = f"<pre>\nException in {self.__class__.__name__}: {str(exception)}\n"
        with io.StringIO() as strstream:
            traceback.print_exc( file=strstream )
            resp += strstream.getvalue()
        resp += "\n</pre>"
        return resp
        
    
    def GET( self, *args, **kwargs ):
        response = self.do_the_things( *args, **kwargs )
        self.finalize()
        return response

    def POST( self, *args, **kwargs ):
        response = self.do_the_things( *args, **kwargs )
        self.finalize()
        return response

    def do_the_things( self, *args, **kwargs ):
        if not self.htmltop( "" ):
            return self.response
        self.response += f"<p>do_the_things missing in class {self.__class__.__name__}</p>"
        self.htmlbottom()
        return self.response
    
# ======================================================================

class ContextSelect(HandlerBase):
    def do_the_things( self ):
        self.noauthhtmltop()
        self.response += "<h3>Select the database context you want:</h3>\n"
        self.response += "<ul>"
        for tag in self.config.value( "webap.db" ).keys():
            self.response += f"<li class=\"spaced\"><a href=\"/curveball.py/{tag}/\">{tag}</a>\n</li>\n"
        self.response += "</ul>"
        self.htmlbottom()
        return self.response

# ======================================================================

class FrontPage(HandlerBase):
    def do_the_things( self, tag ):
        if not self.htmltop( tag ):
            return self.response
        with db.DB.get( context=tag, cfg=self.config ) as curdb:
            self.htmlbottom()
            return self.response

# ======================================================================

class SaveObjectChanges(HandlerBase):
    def do_the_things( self, tag, objname ):
        try:
            self.jsontop(tag)
            data = json.loads( web.data() )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                # So much less painful to just use the psycopg2 object than try
                # to figure out how sqlalchemy wants me to use its own Connection
                # objects.
                con = curdb.db.connection().connection
                cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
                if data['classname'] != '-----not set-----':
                    query = "SELECT id,name FROM object_classification WHERE name=%(classname)s"
                    res = cursor.execute( query, { 'classname': data['classname'] } )
                    rows = cursor.fetchall()
                    if len(rows) == 0:
                        raise ValueError( f"Unknown Classification {data['classname']}" )
                    classid = rows[0]['id']
                    classname = rows[0]['name']
                else:
                    classname = None
                    classid = None
                query = ( "UPDATE object SET ra=%(ra)s,dec=%(dec)s,z=%(z)s,t0=%(t0)s,"
                          "  classification_id=%(clsid)s,confidence=%(cnf)s "
                          "WHERE name=%(objname)s" )
                # sys.stderr.write( f"Setting ra={data['ra']}, dec={data['dec']}, t0={data['t0']}, z={data['z']}, "
                #                   f"classification_id={classid}, confidence={data['confidence']}\n" )
                res = cursor.execute( query, { 'objname': objname, 'ra': data['ra'], 'dec': data['dec'],
                                               'z': data['z'], 't0': data['t0'],
                                               'clsid': classid, 'cnf': data['confidence'] } )
                con.commit()
            return json.dumps( { 'status': 'ok',
                                 'ra': float( data['ra'] ),
                                 'dec': float( data['dec'] ),
                                 'z': float( data['z'] ),
                                 't0': float( data['t0'] ),
                                 'classname': classname,
                                 'confidence': float( data['confidence'] )
                                } )
        except Exception as e:
            return self.errordump( e ) 

# ======================================================================

class DeleteImage(HandlerBase):
    def do_the_things( self, tag ):
        try:
            self.jsontop( tag )
            # sys.stderr.write( f"web.data() is {web.data()}\n" )
            data = json.loads( web.data() )
            if not "basename" in data.keys():
                # sys.stderr.write( f"basename not in {data.keys()}\n" )
                return json.dumps( { 'status': 'error', 'error': 'Must pass a basename to delete' } )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                imgs = curdb.db.query( db.Image ).filter( db.Image.basename == data['basename'] ).all()
                if len(imgs) == 0:
                    return json.dumps( { 'status': 'error', 'error': f'Unknown image {basename}' } )
                if len(imgs) > 1:
                    return json.dumps( { 'status': 'error', 'error': ( f'Multiple images with basename {basename}; '
                                                                       f'this shouldn\'t happen.' ) } )
                img = imgs[0]
                stkmbrs = curdb.db.query( db.StackMember ).filter( db.StackMember.image_id==img.id ).all()
                for stkmbr in stkmbrs:
                    curdb.db.delete( stkmbr )
                curdb.db.delete( img )
                curdb.db.commit()
            return json.dumps( { 'status': 'ok', 'deleted': data['basename'] } )
        except Exception as e:
            return self.errordump( e )

# ======================================================================

class GetObjectClassifications(HandlerBase):
    def do_the_things( self, tag ):
        global session
        try:
            self.jsontop( tag )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                con = curdb.db.bind.raw_connection()
                cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
                cursor.execute( 'SELECT id,name,description FROM object_classification ORDER BY id' );
                rows = list( cursor.fetchall() )
                resp = { 'status': 'ok',
                         'classifications': rows }
                return json.dumps( resp )
        except Exception as e:
            return self.errordump( e )
                
# ======================================================================

class GetExposureSourceList(HandlerBase):
    def do_the_things( self, tag ):
        global session
        try:
            self.jsontop( tag )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                con = curdb.db.bind.raw_connection()
                cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
                cursor.execute( 'SELECT e.id,e.name,e.secondary,e.sourcetype,e.nx,e.ny,e.pixscale,'
                                'e.preprocessed,e.skysub,e.orientation,COUNT(c.id) '
                                'FROM exposuresource e '
                                'LEFT JOIN camerachip c ON c.exposuresource_id=e.id '
                                'GROUP BY e.id,e.name,e.secondary,e.sourcetype,e.nx,e.ny,e.pixscale,'
                                'e.preprocessed,e.skysub,e.orientation '
                                'ORDER BY e.name,e.secondary' )
                rows = cursor.fetchall()
                resp = { 'status': 'ok',
                         'n': len(rows),
                         'expsources': [] }
                for row in rows:
                    resp['expsources'].append( { 'id': row['id'],
                                                 'name': row['name'],
                                                 'secondary': row['secondary'],
                                                 'sourcetype': row['sourcetype'],
                                                 'nx': row['nx'],
                                                 'ny': row['ny'],
                                                 'pixscale': row['pixscale'],
                                                 'preprocessed': row['preprocessed'],
                                                 'skysub': row['skysub'],
                                                 'orientation': row['orientation'],
                                                 'nchips': row['count']
                                                 } )
                return json.dumps( resp )
        except Exception as e:
            return self.errordump( e )

# ======================================================================

class GetBandList(HandlerBase):
    def do_the_things( self, tag ):
        global session
        try:
            self.jsontop( tag )
            data = json.loads( web.data() )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                con = curdb.db.bind.raw_connection()
                cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
                q = ( "SELECT b.name,b.filtercode,b.description,s.name AS closeststd,e.name AS expsource "
                      "FROM band b "
                      "INNER JOIN exposuresource e ON b.exposuresource_id=e.id "
                      "LEFT JOIN band s ON b.closeststd=s.id "
                     )
                subs = {}
                if 'expsource' in data.keys():
                    q += "WHERE e.name=%(expsource)s "
                    subs['expsource'] = data['expsource']
                q += "ORDER BY e.name,b.sortdex,b.name"
                cursor.execute( q, subs )
                rows = cursor.fetchall()
                resp = { 'status': 'ok',
                         'n': len(rows),
                         'bands': [ { 'name': r['name'],
                                      'filtercode': r['filtercode'],
                                      'description': r['description'],
                                      'closeststd': r['closeststd'],
                                      'expsource': r['expsource'] }
                                    for r in rows ] }
                return json.dumps( resp )
        except Exception as e:
            return self.errordump( e )

# ======================================================================

class GetAllKnownBands(HandlerBase):
    def do_the_things( self, tag ):
        global session
        try:
            self.jsontop( tag )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                con = curdb.db.bind.raw_connection()
                cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
                cursor.execute( 'SELECT b.id,b.name,b.filtercode,b.description,s.name AS std,e.name AS expsource '
                                'FROM band b '
                                'INNER JOIN exposuresource e ON b.exposuresource_id_id=e.id '
                                'LEFT JOIN band s ON b.closeststd=s.id '
                                'ORDER BY e.name,b.sortdex' )
                rows = cursor.fetchall()
                resp = { 'status': 'ok',
                         'expsources': [] }
                curexpsource = { 'name': None,
                                 'bands': [] }
                for row in rows:
                    if row['expsource'] != curexpsource['name']:
                        if row['expsource']['name'] is not None:
                            resp['expsources'].append( curexpsource )
                        curexpsource = { 'name': row['expsource'],
                                         'bands': [] }
                    curexpsource['bands'].append( { 'id': row['id'],
                                                    'name': row['name'],
                                                    'filtercode': row['filtercode'],
                                                    'description': row['description'],
                                                    'std': row['std'] } )
                if row['expsource']['name'] is not None:
                    resp['expsources'].append( curexpsource )
                return json.dumps( resp )
        except Exception as e:
            return self.errordump( e )

# ======================================================================

class GetAllKnownVersionTags(HandlerBase):
    def do_the_things( self, tag ):
        global session
        try:
            self.jsontop( tag )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                vtags = curdb.db.query( db.VersionTag ).all()
                resp = { 'status': 'ok',
                         'n': len(vtags),
                         'versiontags': [] }
                vtags.sort( key=lambda x: chr(0) if x.name=='default'
                            else chr(1) if x.name[0:8]=='default_'
                            else chr(2) if x.name=='latest'
                            else chr(3) if x.name[0:7]=='latest_'
                            else x.name )
                for vt in vtags:
                    resp['versiontags'].append( { 'id': vt.id,
                                                  'name': vt.name,
                                                  'description': vt.description } )
                return json.dumps( resp )
        except Exception as e:
            return self.errordump( e )
                
                
# ======================================================================

class GetObjectsAndReferences(HandlerBase):
    """Get a list of all defined objects, including all defined references (if any) for those objects.

    """
    def do_the_things( self, tag ):
        global session
        try:
            self.jsontop( tag )
            data = json.loads( web.data() )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                con = curdb.db.bind.raw_connection()
                cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
                cursor.execute( "SELECT o.id,o.name,o.othernames,o.ra,o.dec,o.t0,o.z,o.confidence,o.ignore, "
                                " oc.name AS classname,oc.description AS classdesc, "
                                " obref.basename AS ref,obref.name AS band "
                                " FROM object o "
                                " LEFT JOIN object_classification oc ON oc.id=o.classification_id "
                                " LEFT JOIN ( "
                                "   SELECT obr.object_id,r.basename,b.name "
                                "   FROM object_reference obr "
                                "   INNER JOIN image r ON r.id=obr.image_id "
                                "   INNER JOIN band b ON r.band_id=b.id "
                                " ) obref ON obref.object_id=o.id "
                                " ORDER BY o.name,obref.name,obref.basename " )
                rows = cursor.fetchall()
                objs = {}
                objids = []
                for row in rows:
                    if row['id'] not in objs.keys():
                        objids.append( row['id'] )
                        objs[row['id']] = { 'id': row['id'],
                                            'name': row['name'],
                                            'othernames': row['othernames'],
                                            'ra': row['ra'],
                                            'dec': row['dec'],
                                            't0': row['t0'],
                                            'z': row['z'],
                                            'confidence': row['confidence'],
                                            'ignore': row['ignore'],
                                            'classname': row['classname'],
                                            'classdesc': row['classdesc'],
                                            'bands': {}
                                           }
                    if row['band'] is not None:
                        if row['band'] not in objs[row['id']]['bands']:
                            objs[row['id']]['bands'][row['band']] = []
                        objs[row['id']]['bands'][row['band']].append( row['ref'] )

                return json.dumps( { 'status': 'ok',
                                     'n': len(rows),
                                     'objids': objids,
                                     'objects': objs,
                                    } )
        except Exception as e:
            return self.errordump( e )
            
                                          
    

# ======================================================================

class GetObjectsWithSubsAndPhot(HandlerBase):
    """Get a list of all defined objects with counts of subtractions and photometry points

    Subtractions counted are the ones tagged with the sub_vtag that have
    photometry with ANY tag of the object.

    Photometry counted are the ones of the object tagged with pho_vtag.

    """
    def do_the_things( self, tag ):
        global session
        try:
            self.jsontop( tag )
            data = json.loads( web.data() )
            subvtag = data['sub_vtag'] if 'sub_vtag' in data else 'default'
            phovtag = data['pho_vtag'] if 'pho_vtag' in data else 'default'
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                con = curdb.db.bind.raw_connection()
                cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
                cursor.execute( "SELECT o.id,o.name,o.othernames,o.ra,o.dec,o.t0,o.z,o.confidence,o.ignore, "
                                "  oc.name AS classname,oc.description AS classdesc,ssubq.subversiontag, "
                                "  ssubq.band AS subband,COUNT( ssubq.sid ) AS numsubs, "
                                "    SUM( ssubq.numpid ) AS phoinsubs "
                                "FROM object o "
                                "LEFT JOIN object_classification oc ON oc.id=o.classification_id "
                                "LEFT JOIN ( "
                                "  SELECT COUNT(p.id) AS numpid,p.object_id as poid,s.id AS sid, "
                                "    b.name AS band, vtfors.name AS subversiontag "
                                "  FROM photometry p "
                                "  INNER JOIN subtraction s ON p.subtraction_id=s.id "
                                "  INNER JOIN subtraction_versiontag svt ON svt.subtraction_id=s.id "
                                "  INNER JOIN versiontag vtfors ON svt.versiontag_id=vtfors.id "
                                "    AND vtfors.name=%(subvtag)s "
                                "  INNER JOIN image i ON s.image_id=i.id "
                                "  INNER JOIN band b ON i.band_id=b.id "
                                "  GROUP BY poid,sid,band,vtfors.name "
                                ") ssubq ON ssubq.poid=o.id "
                                "GROUP BY o.id,o.name,o.othernames,o.ra,o.dec,o.t0,o.z,o.confidence,o.ignore, "
                                "  subversiontag,classname,classdesc,subband "
                                "ORDER BY o.name,subband",
                                { 'subvtag': subvtag }
                               )
                rows = cursor.fetchall()
                objs = {}
                for row in rows:
                    # sys.stderr.write( f'row = {row}\n' )
                    if ( ( row['id'] not in objs.keys() ) and
                         ( ( ( row['numsubs'] is not None ) and ( row['numsubs'] > 0 ) )
                           or
                           ( ( row['phoinsubs'] is not None ) and ( row['phoinsubs'] > 0 ) )
                          )
                        ):
                        objs[row['id']] = { 'id': row['id'],
                                            'name': row['name'],
                                            'othernames': row['othernames'],
                                            'ra': row['ra'],
                                            'dec': row['dec'],
                                            't0': row['t0'],
                                            'z': row['z'],
                                            'confidence': row['confidence'],
                                            'ignore': row['ignore'],
                                            'classname': row['classname'],
                                            'classdesc': row['classdesc'],
                                            'subvts': [],
                                            'subs': {},
                                            'phovts': [],
                                            'phos': {}
                                           }
                    if row['subversiontag'] is not None:
                        if row['subversiontag'] not in objs[row['id']]['subvts']:
                            objs[row['id']]['subvts'].append( row['subversiontag'] )
                            objs[row['id']]['subs'][row['subversiontag']] = []
                        if row['subband'] is not None:
                            # For reasons I don't understand, phoinsubs was coming out as a Decimal object
                            objs[row['id']]['subs'][row['subversiontag']].append( { 'subband': row['subband'],
                                                                                    'numsubs': row['numsubs'],
                                                                                    'phoinsubs':
                                                                                       int( row['phoinsubs'] )
                                                                                   } )
                q = ( "SELECT o.id,o.name,o.othernames,o.ra,o.dec,o.t0,o.z,o.confidence,o.ignore, "
                                "  oc.name AS classname,oc.description AS classdesc,"
                                "  psubq.phoversiontag,psubq.band AS phoband,COUNT(psubq.pid) AS numpho "
                                "FROM object o "
                                "LEFT JOIN object_classification oc ON oc.id=o.classification_id "
                                "LEFT JOIN ( "
                                "  SELECT p.id AS pid,b.name AS band,p.object_id AS poid,vtforp.name AS phoversiontag "
                                "  FROM photometry p "
                                "  INNER JOIN photometry_versiontag pvt ON pvt.photometry_id=p.id "
                                "  INNER JOIN versiontag vtforp ON pvt.versiontag_id=vtforp.id "
                                "    AND vtforp.name=%(phovtag)s "
                                "  INNER JOIN subtraction s ON p.subtraction_id=s.id "
                                "  INNER JOIN image i ON s.image_id=i.id "
                                "  INNER JOIN band b ON i.band_id=b.id "
                                ") psubq ON psubq.poid=o.id "
                                "GROUP BY o.id,o.name,o.othernames,o.ra,o.dec,o.t0,o.z,o.confidence,o.ignore,"
                                "  classname,classdesc,phoband,phoversiontag "
                                "ORDER BY o.name,phoband " )
                subs = { 'phovtag': phovtag }
                # sys.stderr.write( f"Sending query: {cursor.mogrify( q, subs )}\n" )
                cursor.execute( q, subs )
                rows = cursor.fetchall()
                for row in rows:
                    if ( ( row['id'] not in objs.keys() )
                         and ( ( row['numpho'] is not None ) and ( row['numpho'] > 0 ) )
                        ):
                        objs[row['id']] = { 'id': row['id'],
                                            'name': row['name'],
                                            'othernames': row['othernames'],
                                            'ra': row['ra'],
                                            'dec': row['dec'],
                                            't0': row['t0'],
                                            'z': row['z'],
                                            'confidence': row['confidence'],
                                            'ignore': row['ignore'],
                                            'classname': row['classname'],
                                            'classdesc': row['classdesc'],
                                            'subvts': [],
                                            'subs': {},
                                            'phovts': [],
                                            'phos': {},
                                           }
                    if row['phoversiontag'] is not None:
                        if row['phoversiontag'] not in objs[row['id']]['phovts']:
                            objs[row['id']]['phovts'].append( row['phoversiontag'] )
                            objs[row['id']]['phos'][row['phoversiontag']] = []
                        if row['phoband'] is not None:
                            objs[row['id']]['phos'][row['phoversiontag']].append( { 'phoband': row['phoband'],
                                                                                    'numpho': row['numpho'] } )

            objs = list( objs.values() )
            objs.sort( key=lambda r: r['name'] )

            for obj in objs:
                obj['subvts'].sort( key = lambda x : chr(0) if x=='default'
                                    else chr(1) if x[0:8]=="default_"
                                    else chr(2) if x=="latest"
                                    else chr(3) if x[0:8]=="latest_"
                                    else x )
                obj['phovts'].sort( key = lambda x : chr(0) if x=='default'
                                    else chr(1) if x[0:8]=="default_"
                                    else chr(2) if x=="latest"
                                    else chr(3) if x[0:8]=="latest_"
                                    else x )
            
            # ****
            # for o in objs:
            #     for k, v in o.items():
            #         sys.stderr.write( f'obj[{k}]={v}, type {type(v)}\n' )
            # ****
            
            return json.dumps( { 'status': 'ok',
                                 'n': len(rows),
                                 'objects': objs,
                                 # 'sub_vtag': subvtag,
                                 # 'pho_vtag': phovtag
                                } )
        except Exception as e:
            return self.errordump( e )
                

# ======================================================================

class GetLtcvData(HandlerBase):
    def do_the_things( self, tag, objname ):
        global session
        try:
            self.jsontop( tag )
            data = json.loads( web.data() )
            phovtag = data['pho_vtag'] if 'pho_vtag' in data else 'default'
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                resp = {}
                # ****
                # sys.stderr.write( f"About to get object by name: {objname}\n" )
                # cfg = self.config
                # sys.stderr.write( f"database is {cfg.value('database.database')}\n" )
                # ****
                obj = db.Object.get_by_name( objname, curdb=curdb )
                if obj is None:
                    resp =  { 'status': 'error',
                              'error': f'Unknown object {objname}',
                              'traceback': '' }
                    return json.dumps( resp )
                resp['pho_vtag'] = phovtag
                resp['status'] = 'ok'
                resp['object'] = obj.name
                resp['othernames'] = obj.othernames
                resp['ra'] = obj.ra
                resp['dec'] = obj.dec
                resp['t0'] = obj.t0
                resp['z'] = obj.z
                resp['bands'] = []
                resp['band_expsources'] = {}
                resp['ltcv'] = {}
                photometry = db.Photometry.get_for_obj( obj, version=phovtag, includebad=True, curdb=curdb )
                curband = None
                for phoelem in photometry:
                    band, pholist = phoelem
                    if band.name != curband:
                        curband = band.name
                        if curband not in resp['bands']:
                            resp['bands'].append( curband )
                            resp['band_expsources'][curband] = band.exposuresource_id
                            resp['ltcv'][curband] = []
                    for pho in pholist:
                        apdata = curdb.db.query( db.ApPhotData ).filter( db.ApPhotData.photometry_id==pho.id ).all()
                        if len( apdata ) > 0:
                            apdata = apdata[0]
                            aperrad = apdata.aperrad
                            apercor = apdata.apercor
                        else:
                            apdata = None
                            aperrad = None
                            apercor = None
                        exposure = pho.subtraction.image.exposure
                        expsr = None if exposure is None else exposure.exposuresource
                        orientation = None if expsr is None else expsr.orientation
                        resp['ltcv'][curband].append( { 'id': pho.id,
                                                        'mjd': pho.mjd,
                                                        'flux': pho.flux,
                                                        'dflux': pho.dflux,
                                                        'magzp': pho.magzp,
                                                        'mag': None if math.isnan(pho.mag) else pho.mag,
                                                        'dmag': None if math.isnan(pho.dmag) else pho.dmag,
                                                        'refnorm': pho.refnorm,
                                                        'imagex': pho.imagex,
                                                        'imagey': pho.imagey,
                                                        'orientation': orientation,
                                                        'imagebasename': pho.subtraction.image.basename,
                                                        'refbasename': pho.subtraction.ref.basename,
                                                        'new_jpeg': base64.b64encode( pho.new_jpeg ).decode('utf-8'),
                                                        'ref_jpeg': base64.b64encode( pho.ref_jpeg ).decode('utf-8'),
                                                        'sub_jpeg': base64.b64encode( pho.sub_jpeg ).decode('utf-8'),
                                                        'is_bad': pho.is_bad,
                                                        'aperrad': aperrad,
                                                        'apercor': apercor
                                                       } )
                return json.dumps( resp )
        except Exception as e:
            return self.errordump( e )

# ======================================================================
        
class SetLtcvPointBad(HandlerBase):
    def do_the_things( self, tag ):
        global session
        try:
            self.jsontop( tag )
            data = json.loads( web.data() )
            phoid = data['id']
            bad = data['is_bad']
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                q = curdb.db.query( db.Photometry ).filter( db.Photometry.id==phoid )
                if q.count() == 0:
                    raise RuntimeError( f"Can't find photometry point {phoid}" )
                if q.count() > 1:
                    raise RuntimeError( f"Photometry point {phoid} is multiply defined.  This shouldn't happen." )
                pho = q.first()
                pho.is_bad = bad
                curdb.db.commit()
                return json.dumps( { 'status': 'ok',
                                     'id': phoid,
                                     'is_bad': bad } );
        except Exception as e:
            return self.errordump( e )

# ======================================================================

class GetSalt2Fits(HandlerBase):
    def do_the_things( self, tag, objname ):
        global session
        try:
            self.jsontop( tag )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                resp = {}
                obj = db.Object.get_by_name( objname, curdb=curdb )
                if obj is None:
                    resp =  { 'status': 'error',
                              'error': f'Unknown object {objname}',
                              'traceback': '' }
                    return json.dumps( resp )
                fits = db.Salt2Fit.get_for_obj( obj, version=None, curdb=curdb )
                resp['status'] = 'ok'
                resp['fits'] = []
                if fits is not None:
                    bandcache = {}
                    for fit in fits:
                        bandnamelist = []
                        for bandid in fit.bands:
                            if bandid in bandcache:
                                band = bandcache[bandid]
                            else:
                                band = db.Band.get( bandid, curdb=curdb )
                                bandcache[bandid] = band
                            bandnamelist.append( band.name )
                        versiontags = db.Salt2FitVersiontag.get_tagnames_for_fit( fit.id, cfg=self.config,
                                                                                  curdb=curdb )
                        resp['fits'].append( {
                            'id': fit.id,
                            'bands': bandnamelist,
                            'zfixed': fit.zfixed,
                            'mwebv': fit.mwebv,
                            'mbstar': fit.mbstar,
                            'dmbstar': fit.dmbstar,
                            'z': fit.z,
                            'dz': fit.dz,
                            't0': fit.t0,
                            'dt0': fit.dt0,
                            'x0': fit.x0,
                            'dx0': fit.dx0,
                            'x1': fit.x1,
                            'dx1': fit.dx1,
                            'c': fit.c,
                            'dc': fit.dc,
                            'chisq': fit.chisq,
                            'dof': fit.dof,
                            'cov': fit.cov,
                            'versiontags': versiontags } )

                    resp['fits'].sort( key=lambda x: chr(0x10ffff) if len(x['versiontags'])==0
                                       else chr(0) if x['versiontags'][0]=='default'
                                       else chr(1) if x['versiontags'][0]=='latest'
                                       else x['versiontags'][0] )
                # ****
                # strio = io.StringIO()
                # strio.write( f"Returning{len(resp['fits'])} fits: " )
                # for f in resp['fits']:
                #     strio.write( f"{f['id']} with versiontags {f['versiontags']}; " )
                # sys.stderr.write( f"{strio.getvalue()}\n" )
                # ****
                return json.dumps( resp )
        except Exception as e:
            return self.errordump( e )

# ======================================================================

class GetSalt2FitCurve(HandlerBase):
    bandmapping = {
        'ZTF_g' : ( 'ztfg', 8.9, 'ab' ),
        'ZTF_r' : ( 'ztfr', 8.9, 'ab' ),
        'ZTF_i' : ( 'ztfi', 8.9, 'ab' ),
        'DECam_g' : ( 'desg', 8.9, 'ab' ),
        'DECam_r' : ( 'desr', 8.9, 'ab' ),
        'DECam_i' : ( 'desi', 8.9, 'ab' )
    }
        
    @staticmethod
    def fit_to_curve( fit, band, dust, relt0=-19, relt1=51, dt=1, curdb=None ):
        if curdb is None:
            raise ValueError( "curdb can't be None in fit_to_curve" )
        with db.DB.get(curdb) as curdb:
            model = sncosmo.Model( "salt2",
                                   effects=[dust], effect_names=['mw'], effect_frames=['obs'] )
            model.set( mwebv=fit.mwebv )
            model.set( z=fit.z )
            model.set( t0=fit.t0 )
            model.set( x0=fit.x0 )
            model.set( x1=fit.x1 )
            model.set( c=fit.c )
            ts = numpy.arange( fit.t0+relt0, fit.t0+relt1, dt )
            bandinfo = GetSalt2FitCurve.bandmapping[band]
            # sys.stderr.write( "Calling salt2 bandflux\n" )
            fluxen = model.bandflux( bandinfo[0], ts, zp=bandinfo[1], zpsys=bandinfo[2] )
            return ts.tolist(), fluxen.tolist()


    def do_the_things( self, tag, fitid, band ):
        global session
        try:
            self.jsontop( tag )
            with db.DB.get( context=tag, cfg=self.config ) as curdb:
                resp = { 'status': 'ok',
                         't': [],
                         'flux': [] }
                data = json.loads( web.data() )
                relt0 = -19
                relt1 = 51
                dt = 1
                if 't0' in data:
                    relt0 = float( data['t0'] )
                if 't1' in data:
                    relt1 = float( data['t1'] )
                if 'dt' in data:
                    dt = float( data['dt'] )
                fit = db.Salt2Fit.get( int(fitid), curdb=curdb )
                dust = sncosmo.CCM89Dust()
                resp['t'], resp['flux'] = self.fit_to_curve( fit, band, dust,
                                                             relt0=relt0, relt1=relt1, dt=dt, curdb=curdb )
                return json.dumps( resp )
        except Exception as e:
            return self.errordump( e )

# ======================================================================

class SessionDump(HandlerBase):
    def do_the_things( self ):
        global session
        self.noauthhtmltop()
        self.response += "<h3>web.ctx.session</h3>\n"
        self.response += "<ul>\n"
        for val in dir( web.ctx.session ):
            self.response += f"<li>{val} : {getattr(web.ctx.session,val)}</li>\n"
        self.response += "</ul>\n"
        self.htmlbottom()
        return self.response

# ======================================================================

urls = (
    "/",                                          "ContextSelect",
    "/sessiondump",                               "SessionDump",
    "/(.+)/saveobjectchanges/(.+)",               "SaveObjectChanges",
    "/(.+)/deleteimage/",                         "DeleteImage",
    "/(.+)/getclassifications/",                  "GetObjectClassifications",
    "/(.+)/getexpsourcelist/",                    "GetExposureSourceList",
    "/(.+)/getbandlist/",                         "GetBandList",
    "/(.+)/getobjsandrefs/",                      "GetObjectsAndReferences",
    "/(.+)/getobjswithphot/",                     "GetObjectsWithSubsAndPhot",
    "/(.+)/getallversiontags/",                   "GetAllKnownVersionTags",
    "/(.+)/getltcvdata/(.+)",                     "GetLtcvData",
    "/(.+)/setpointbad/",                         "SetLtcvPointBad",
    "/(.+)/getsalt2fits/(.+)",                    "GetSalt2Fits",
    "/(.+)/getsalt2fitcurve_fitidband/(.+)/(.+)", "GetSalt2FitCurve",
    "/(.+)/",                                     "FrontPage",
    "/auth", auth.app
)

app = web.application( urls, globals() )

web.config.session_parameters["samesite"] = "lax"

web.config.smtp_server = 'smtp.lbl.gov';
web.config.smtp_port = 25
web.config.smtp_username = None
web.config.smtp_password = None
web.config.smtp_starttls = True

# These next three make the same session available
#   in the auth subapp as is available here
secure_session( auth.initializer )
def session_hook(): web.ctx.session = session
app.add_processor( web.loadhook( session_hook ) )

application = app.wsgifunc()

if __name__ == "__main__":
    app.run()
