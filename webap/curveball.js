import { SVGPlot } from "./svgplot.js"
import { rkWebUtil } from "./rkwebutil.js"
import { Curveball_Config } from "./curveball_config.js"
import { rkAuth } from "./rkauth.js"

// Namespace
var Curveball = {}

// Curveball.webapfullurl = "http://curveball-webap.curveball.production.svc.spin.nersc.org/curveball.py/"
// Curveball.webapfullurl = Curveball_Config.webapfullurl;
Curveball.webapurl = "/curveball.py/"

Curveball._orientation_desc = [ "North Up, East Left",
                                "North Right, East Up",
                                "North Down, East Right",
                                "North Left, East Down",
                                "North Up, East Right",
                                "North Right, East Down",
                                "North Down, East Left",
                                "North Left, East Up" ];

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Overall context

Curveball.Context = function() {}

// Curveball.bandmap = {
//     'ZTF_g' : 'ztfg',
//     'ZTF_r' : 'ztfr',
//     'ZTF_i' : 'ztfi'
// };

// **********************************************************************
// When the application is started, poke at the document to see if
// there are divs with certain ids; if so, launch handlers for them.

Curveball.Context.prototype.init = function() {
    let self = this;
    let tagelem = document.querySelector( "#curveball_context" )
    if ( tagelem == null ) {
        rkWebUtil.elemaker( "p", document.body );
        p.innerHTML = "<b>ERROR: couldn't find the tag element.</b>";
        return;
    }
    this.tag = tagelem.value;
    console.log( "Tag is " + this.tag );
    this.webapurl = Curveball.webapurl + this.tag + "/";
    let authdiv = document.querySelector('#rkauth_div');
    this.auther = new rkAuth( authdiv, Curveball.webapurl, this.tag,
                              function() { window.open( self.webapurl, "_self"); },
                              null );
    this.auther.checkAuth( function() { self.finish_init() },
                           function() { self.auther.showLoginUI() } );
    this.classifications = null;
}

Curveball.Context.prototype.finish_init = function() {
    var self = this;
    
    this.connector = new rkWebUtil.Connector( this.webapurl );

    this.contentdiv = document.querySelector( "#contentdiv" );
    if ( this.contentdiv == null ) {
        rkWebUtil.elemaker( "p", document.body );
        p.innerHTML = "<b>ERROR: couldn't find the content div.</b>"
        return;
    }

    this.connector.sendHttpRequest( "getclassifications/", {},
                                    function( datablob ) { self.loadObjectClassifications( datablob ); } );
    
    this.frontpage = new Curveball.FrontPage( this );
    this.versiontagids = [];
    this.subvtid = null;
    this.phovtid = null;
    this.versiontagnames = {};
    this.versiontagdescriptions = {};
    this.connector.sendHttpRequest( "getallversiontags/", {},
                                    function( datablob ) { self.loadVersiontagsAndRender( datablob ); } );
}
    
// **********************************************************************

Curveball.Context.prototype.loadVersiontagsAndRender = function( datablob )
{
    for ( let vtinfo of datablob.versiontags ) {
        this.versiontagids.push( vtinfo.id );
        this.versiontagnames[ vtinfo.id ] = vtinfo.name;
        this.versiontagdescriptions[ vtinfo.id ] = vtinfo.description;
    }
    this.frontpage.render();    
}

// **********************************************************************

Curveball.Context.prototype.loadObjectClassifications = function( datablob )
{
    if ( datablob.hasOwnProperty( "error" ) ) {
        window.alert( "Error return loading known object classifications: " + datablob.error );
        return;
    }
    if ( ( ! datablob.hasOwnProperty( "status" ) ) || ( datablob.status != "ok" ) ) {
        window.alert( "Did not get ok status back from loading known object classifications." );
        return;
    }
    this.classifications = datablob.classifications;
}

// **********************************************************************

Curveball.Context.prototype.loading = function() {
    rkWebUtil.wipeDiv( this.contentdiv );
    rkWebUtil.elemaker( "p", this.contentdiv, { "text": "Loading...",
                                                "classes": [ "warn" ] } );
}



// **********************************************************************
// **********************************************************************
// **********************************************************************
// Base class for all pages

Curveball.Page = function( context, last=null ) {
    this.context = context;
    this.last = last;
    this.data = null;
}

Curveball.Page.prototype.pagename = "whatever";

Curveball.Page.prototype.loadData = function( data, nullok=false ) {
    if ( data == null ) {
        if ( ! this.hasOwnProperty( 'data' ) ) {
            window.alert( "Something has gone wrong; no this.data." )
            return false;
        }
        data = this.data;
    }
    if ( ( ! nullok ) && ( data == null ) ) {
        window.alert( "Something has gone wrong; null data." );
        return false;
    }
    this.data = data;
    return true;
}

Curveball.Page.prototype.renderTop = function( nofront=false )
{
    var self = this;
    
    rkWebUtil.wipeDiv( this.context.contentdiv );
    if ( this.last != null ) {
        rkWebUtil.elemaker( "p", this.context.contentdiv,
                            { "text": "[Back to " + self.last.pagename + "]",
                              "click": function() { self.last.render(); },
                              "classes": [ "link" ]
                            } );
    }
    if ( ( ! nofront ) && ( this.last.pagename != "front page" ) ) {
        rkWebUtil.elemaker( "p", this.context.contentdiv,
                            { "text": "[Back to front page]",
                              "click": function() { self.context.frontpage.render(); },
                              "classes": [ "link" ]
                            } );
    }

    if ( this.context.subvtid == null ) this.context.subvtid = this.context.versiontagids[0];
    if ( this.context.phovtid == null ) this.context.phovtid = this.context.versiontagids[0];
    
    let div = rkWebUtil.elemaker( "p", this.context.contentdiv );
    div.appendChild( document.createTextNode( "Subtraction versiontag: " ) );
    this.subvtwidget = rkWebUtil.elemaker( "select", div );
    for ( let vtid of this.context.versiontagids ) {
        let opt = rkWebUtil.elemaker( "option", this.subvtwidget, { "text": this.context.versiontagnames[ vtid ],
                                                                    "attributes": { "value": vtid } } );
        if ( vtid == this.context.subvtid ) opt.selected = true;
    }
    this.subvtwidget.addEventListener( "change", function() { self.context.subvtid = self.subvtwidget.value; } );
    div.appendChild( document.createTextNode( "   Photometry versiontag: " ) );
    this.phovtwidget = rkWebUtil.elemaker( "select", div );
    for ( let vtid of this.context.versiontagids ) {
        let opt = rkWebUtil.elemaker( "option", this.phovtwidget, { "text": this.context.versiontagnames[ vtid ],
                                                                    "attributes": { "value": vtid } } );
        if ( vtid == this.context.phovtid ) opt.selected = true;
    }
    this.phovtwidget.addEventListener( "change", function() { self.context.phovtid = self.phovtwidget.value; } );
}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Front Page

Curveball.FrontPage = function( context, last=null ) {
    Curveball.Page.call( this, context, last );
}

Curveball.FrontPage.prototype = Object.create( Curveball.Page.prototype,
                                               { constructor: { value: Curveball.FrontPage,
                                                                enumerable: false,
                                                                writeable: true,
                                                                configurable: true } } );

Curveball.FrontPage.prototype.pagename = "front page";

// **********************************************************************

Curveball.FrontPage.prototype.render = function( data=null ) {
    let self = this;
    let ul, li, a;

    this.renderTop( true );
    
    ul = rkWebUtil.elemaker( "ul", this.context.contentdiv );
    li = rkWebUtil.elemaker( "li", ul );
    a = rkWebUtil.elemaker( "a", li, { "text": "List all defined objects (and references)",
                                       "classes": [ "link" ] } );
    a.addEventListener( "click", function() { self.listObjsAndRefs() } );
    li = rkWebUtil.elemaker( "li", ul );
    a = rkWebUtil.elemaker( "a", li, { "text": "List all defined objects w/ subtractions and photometry" ,
                                       "classes": [ "link" ] } );
    a.addEventListener( "click", function() { self.listObjsWithPhot() } );
    li = rkWebUtil.elemaker( "li", ul );
    a = rkWebUtil.elemaker( "a", li, { "text": "List all defined exposure sources" ,
                                       "classes": [ "link" ] } );
    a.addEventListener( "click", function() { self.listExpSources() } );
    li = rkWebUtil.elemaker( "li", ul );
    a = rkWebUtil.elemaker( "a", li, { "text": "List all defined bands" ,
                                       "classes": [ "link" ] } );
    a.addEventListener( "click", function() { self.listBands() } );
    
}

// **********************************************************************

Curveball.FrontPage.prototype.listObjsAndRefs = function() {
    let self = this;
    this.context.loading();
    this.context.connector.sendHttpRequest( "getobjsandrefs/", {},
                                            function(data) {
                                                let oblist = new Curveball.ObjsAndRefs( self.context, self );
                                                oblist.render( data );
                                            }
                                          );
}

Curveball.FrontPage.prototype.listObjsWithPhot = function() {
    let self = this;
    this.context.loading();
    let subvtag = this.context.versiontagnames[ this.subvtwidget.value ];
    let phovtag = this.context.versiontagnames[ this.phovtwidget.value ];
    this.context.connector.sendHttpRequest( "getobjswithphot/", { 'sub_vtag': subvtag, 'pho_vtag': phovtag },
                                            function(data) {
                                                let oblist = new Curveball.ObjsWithPhotList( self.context, self );
                                                oblist.render( data );
                                            }
                                          );
}

Curveball.FrontPage.prototype.listExpSources = function() {
    let self = this;
    this.context.loading();
    this.context.connector.sendHttpRequest( "getexpsourcelist/", {},
                                            function(data) {
                                                let explist = new Curveball.ExpSourceList( self.context, self );
                                                explist.render( data );
                                            }
                                          );
}

Curveball.FrontPage.prototype.listBands = function() {
    let self = this;
    this.context.loading();
    this.context.connector.sendHttpRequest( "getbandlist/", {},
                                            function(data) {
                                                let bandlist = new Curveball.BandList( self.context, self );
                                                bandlist.render( data );
                                            }
                                          );
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

Curveball.ExpSourceList = function( context, last=null ) {
    Curveball.Page.call( this, context, last );
}

Curveball.ExpSourceList.prototype = Object.create( Curveball.Page.prototype,
                                                   { constructor: { value: Curveball.ExpSourceList,
                                                                    enumerable: false,
                                                                    writeable: true,
                                                                    configurable: true } } );

Curveball.ExpSourceList.prototype.pagename = "exposuresource list";

// **********************************************************************

Curveball.ExpSourceList.prototype.render = function( data=null ) {
    let self = this;
    let p, table, tr;

    this.renderTop();
    if ( ! this.loadData( data ) ) return;
    data = this.data;

    p = rkWebUtil.elemaker( "p", this.context.contentdiv, { "text": "Refresh List", "classes": [ "link" ] } );
    p.addEventListener( "click", function() {
        self.context.loading();
        self.context.connector.sendHttpRequest( "getexpsourcelist/", {}, function(data) { self.render(data) } )
    } );

    rkWebUtil.elemaker( "h2", this.context.contentdiv, { "text": "Exposure Sources" } );
    
    table = rkWebUtil.elemaker( "table", this.context.contentdiv );
    tr = rkWebUtil.elemaker( "tr", table );
    rkWebUtil.elemaker( "th", tr, { "text": "Name" } );
    rkWebUtil.elemaker( "th", tr, { "text": "Secondary" } );
    rkWebUtil.elemaker( "th", tr, { "text": "Type" } );
    rkWebUtil.elemaker( "th", tr, { "text": "nx" } );
    rkWebUtil.elemaker( "th", tr, { "text": "ny" } );
    rkWebUtil.elemaker( "th", tr, { "text": "pixscale" } );
    rkWebUtil.elemaker( "th", tr, { "text": "preproc?" } );
    rkWebUtil.elemaker( "th", tr, { "text": "skysub?" } );
    rkWebUtil.elemaker( "th", tr, { "text": "orientation" } );
    rkWebUtil.elemaker( "th", tr, { "text": "N. Chips" } );
    for ( let row of data['expsources'] ) {
        tr = rkWebUtil.elemaker( "tr", table );
        rkWebUtil.elemaker( "td", tr, { "text": row['name'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['secondary'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['sourcetype'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['nx'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['ny'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['pixscale'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['preprocessed'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['skysub'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['orientation'] } );
        rkWebUtil.elemaker( "td", tr, { "text": row['nchips'] } );
    }
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

Curveball.BandList = function( context, last=null ) {
    Curveball.Page.call( this, context, last );
}

Curveball.BandList.prototype = Object.create( Curveball.Page.prototype,
                                              { constructor: { value: Curveball.BandList,
                                                               enumerable: false,
                                                               writeable: true,
                                                               configurable: true } } );

Curveball.BandList.prototype.pagename = "band list";

// **********************************************************************

Curveball.BandList.prototype.render = function( data=null ) {
    let self = this;
    let p, table, tr;

    this.renderTop();
    if ( ! this.loadData( data ) ) return;
    data = this.data;
    
    p = rkWebUtil.elemaker( "p", this.context.contentdiv, { "text": "Refresh List", "classes": [ "link" ] } );
    p.addEventListener( "click", function() {
        self.context.loading();
        self.context.connector.sendHttpRequest( "getbandlist/", {}, function(data) { self.render(data) } )
    } );

    rkWebUtil.elemaker( "h2", this.context.contentdiv, { "text": "Bands" } );
    table = rkWebUtil.elemaker( "table", this.context.contentdiv );
    tr = rkWebUtil.elemaker( "tr", table );
    rkWebUtil.elemaker( "th", tr, { "text": "Name" } );
    rkWebUtil.elemaker( "th", tr, { "text": "Filtercode" } );
    rkWebUtil.elemaker( "th", tr, { "text": "Closest Std." } );
    rkWebUtil.elemaker( "th", tr, { "text": "Description" } );
    let curexp = null;
    for ( let band of data['bands'] ) {
        if ( band['expsource'] != curexp ) {
            curexp = band['expsource'];
            tr = rkWebUtil.elemaker( "tr", table );
            rkWebUtil.elemaker( "th", tr, { "text": curexp,
                                            "attributes": { "colspan": 4 }
                                          } );
        }
        tr = rkWebUtil.elemaker( "tr", table );
        rkWebUtil.elemaker( "td", tr, { "text": band["name"] } );
        rkWebUtil.elemaker( "td", tr, { "text": band["filtercode"] } );
        rkWebUtil.elemaker( "td", tr, { "text": band["closeststd"] != null ? band["closeststd"] : "" } );
        rkWebUtil.elemaker( "td", tr, { "text": band["description"] != null ? band["description"] : "" } );
    }
}

// **********************************************************************
// **********************************************************************
// **********************************************************************
// This one is a bit odd, because it's not really a page, but a dialog
//
// Perhaps I shouldn't make it inherit from Curveball.Page?

Curveball.EditObject = function( context, last=null, obj=null ) {
    Curveball.Page.call( this, context, last );
    this.data = { 'obj': obj };
}

Curveball.EditObject.prototype = Object.create( Curveball.Page.prototype,
                                                { constructor: { value: Curveball.EditObject,
                                                                 enumerable: false,
                                                                 writeable: true,
                                                                 configurable: true } } );

Curveball.EditObject.prototype.pagename = "edit object";

// **********************************************************************

Curveball.EditObject.prototype.render = function( data=null ) {
    var self = this;

    // Do NOT call Curveball.Page.renderTop since this isn't a page, but a dialog.
    
    if ( ! this.loadData( data ) ) return;
    data = this.data;
    var obj = this.data.obj;
    
    this.dialog = rkWebUtil.elemaker( "dialog", this.context.contentdiv );
    rkWebUtil.elemaker( "p", this.dialog, { "text": "Edit " + obj['name'] } );
    var form = rkWebUtil.elemaker( "form", this.dialog, { "attributes": { "method": "dialog" } } );
    var div = rkWebUtil.elemaker( "div", form );
    let table = rkWebUtil.elemaker( "table", div );

    let tr = rkWebUtil.elemaker( "tr", table );
    let td = rkWebUtil.elemaker( "td", tr, { "text": "RA" } );
    td = rkWebUtil.elemaker( "td", tr );
    this.ra = rkWebUtil.elemaker( "input", td, { "attributes": { "type": "text",
                                                                 "value": obj['ra'] } } );

    tr = rkWebUtil.elemaker( "tr", table );
    td = rkWebUtil.elemaker( "td", tr, { "text": "dec" } );
    td = rkWebUtil.elemaker( "td", tr );
    this.dec = rkWebUtil.elemaker( "input", td, { "attributes": { "type": "text",
                                                                  "value": obj['dec'] } } );
    
    tr = rkWebUtil.elemaker( "tr", table );
    td = rkWebUtil.elemaker( "td", tr, { "text": "t0 (MJD)" } );
    td = rkWebUtil.elemaker( "td", tr );
    this.t0 = rkWebUtil.elemaker( "input", td, { "attributes": { "type": "text",
                                                                 "value": obj['t0'] } } );
    
    tr = rkWebUtil.elemaker( "tr", table );
    td = rkWebUtil.elemaker( "td", tr, { "text": "z" } );
    td = rkWebUtil.elemaker( "td", tr );
    this.z = rkWebUtil.elemaker( "input", td, { "attributes": { "type": "text",
                                                                "value": obj['z'] } } );

    let notsettext = "-----not set-----";
    let classname = notsettext;
    if ( obj.classname != null ) classname = obj.classname;
    let cification = null;
    if ( this.context.classifications != null ) {
        for ( let c of this.context.classifications ) {
            if ( c.name == obj.classname ) {
                cification = c;
            }
        }
    }
    tr = rkWebUtil.elemaker( "tr", table );
    td = rkWebUtil.elemaker( "td", tr, { "text": "classification" } );
    let clstd = rkWebUtil.elemaker( "td", tr );
    tr = rkWebUtil.elemaker( "tr", table );
    td = rkWebUtil.elemaker( "td", tr, { "text": "confidence" } );
    let cnftd = rkWebUtil.elemaker( "td", tr );

    this.classname = null;
    this.confidence = null;
    if ( ( classname == notsettext ) || ( cification != null ) ) {
        this.classname = rkWebUtil.elemaker( "select", clstd );
        let opt = rkWebUtil.elemaker( "option", this.classname,
                                      { "text": notsettext,
                                        "attributes": { "value": notsettext } } );
        if ( classname == notsettext )  opt.selected = true;
        for ( let c of this.context.classifications ) {
            opt = rkWebUtil.elemaker( "option", this.classname,
                                      { "text": c.name,
                                        "attributes": { "value": c.name } } );
            if ( c.name == classname ) opt.selected = true;
        }
        this.confidence = rkWebUtil.elemaker( "input", cnftd,
                                              { "attributes": { "value": obj.confidence,
                                                                "step": "any",
                                                                "min": 0.,
                                                                "max": 1. } } );
    }
    else {
        rkWebUtil.elemaker( "span", clstd, { "text": "(Unknown classification: " + classname + ")" } );
        rkWebUtil.elemaker( "span", cnftd, { "text": "—" } );
    }

    var p = rkWebUtil.elemaker( "div", div, { "classes": [ "hbox", "spacebox" ] } );
    var savebutton = rkWebUtil.button( p, "Save Changes", null, { "value": "save" } );
    var cancelbutton = rkWebUtil.button( p, "Cancel", null, { "value": "cancel" } );

    this.dialog.addEventListener( "close", function() { self.submitObjectEdits(); } );
    this.dialog.showModal();
}

// **********************************************************************

Curveball.EditObject.prototype.submitObjectEdits = function() {
    var self = this;
    if ( this.dialog.returnValue == "save" ) {
        var data = { 'ra': this.ra.value,
                     'dec': this.dec.value,
                     't0': this.t0.value,
                     'z': this.z.value };
        if ( this.classname == null ) {
            data.classname = '-----not set-----';
            data.confidence = 0.;
        } else {
            data.classname = this.classname.value
            data.confidence = this.confidence.value;
        }
        this.context.connector.sendHttpRequest( "saveobjectchanges/" + this.data.obj.name, data,
                                                function( datablob ) { self.updateObjectEdits( datablob ) } );
    }
}

// **********************************************************************

Curveball.EditObject.prototype.updateObjectEdits = function( data )
{
    // I'm really hoping that this.data.obj is a reference back to the right thing in whatever put up the dialog...
    this.data.obj.ra = data.ra;
    this.data.obj.dec = data.dec;
    this.data.obj.t0 = data.t0;
    this.data.obj.z = data.z;
    this.data.obj.classname = data.classname;
    this.data.obj.confidence = data.confidence;
    this.last.render();
}

    
// **********************************************************************
// **********************************************************************
// **********************************************************************

Curveball.ObjsAndRefs = function( context, last=null ) {
    Curveball.Page.call( this, context, last );
}

Curveball.ObjsAndRefs.prototype = Object.create( Curveball.Page.prototype,
                                                   { constructor: { value: Curveball.ObjsAndRefs,
                                                                    enumerable: false,
                                                                    writeable: true,
                                                                    configurable: true } } );

Curveball.ObjsAndRefs.prototype.pagename = "objects and references";

// **********************************************************************

Curveball.ObjsAndRefs.prototype.render = function( data=null ) {
    let self = this;
    let p, table, tr, td;

    this.renderTop();
    if ( ! this.loadData( data ) ) return;
    data = this.data;

    p = rkWebUtil.elemaker( "p", this.context.contentdiv, { "text": "Refresh List", "classes": [ "link" ] } );
    p.addEventListener( "click", function() {
        self.context.loading();
        self.context.connector.sendHttpRequest( "getobjsandrefs/", {}, function(data) { self.render(data) } )
    } );

    rkWebUtil.elemaker( "h2", this.context.contentdiv, { "text": "Objects" } );

    table = rkWebUtil.elemaker( "table", this.context.contentdiv );
    tr = rkWebUtil.elemaker( "tr", table );
    rkWebUtil.elemaker( "th", tr, { "text": "Object" } );
    rkWebUtil.elemaker( "th", tr, { "text": "RA" } );
    rkWebUtil.elemaker( "th", tr, { "text": "dec" } );
    rkWebUtil.elemaker( "th", tr, { "text": "t0" } );
    rkWebUtil.elemaker( "th", tr, { "text": "z" } );
    rkWebUtil.elemaker( "th", tr, { "text": "class (conf)" } );
    rkWebUtil.elemaker( "th", tr, { "text": "Refs" } );
    // Use "of objids" instead of "in objects" to preserve the order that the server sent us
    for ( let objid of data['objids'] ) {
        let obj = data['objects'][objid];
        tr = rkWebUtil.elemaker( "tr", table );
        td = rkWebUtil.elemaker( "td", tr, { "text": obj['name'],
                                             "classes": [ 'link', 'aligntop' ] } );
        td.addEventListener( 'click', function() {
            let objeditor = new Curveball.EditObject( self.context, self, obj );
            objeditor.render();
        } );
        rkWebUtil.elemaker( "td", tr, { "text": obj['ra'].toFixed(5), "classes": [ 'aligntop' ] } );
        rkWebUtil.elemaker( "td", tr, { "text": obj['dec'].toFixed(5), "classes": [ 'aligntop' ] } );
        rkWebUtil.elemaker( "td", tr, { "text": obj['t0'].toFixed(1), "classes": [ 'aligntop' ] } );
        let ztext;
        if ( obj['z'] == null ) ztext = "--"; else ztext = obj['z'].toFixed(4);
        rkWebUtil.elemaker( "td", tr, { "text": ztext, "classes": [ 'aligntop' ] } );
        let classtext = "";
        if ( obj['classname'] == null )
            classtext = "—";
        else {
            classtext = obj['classname'];
            if ( obj['confidence'] == null )
                classtext += " (—)"
            else
                classtext += " (" + obj['confidence'].toFixed(2) + ")"
        }
        rkWebUtil.elemaker( "td", tr, { "text": classtext, "classes": [ 'aligntop' ] } );
        let reftext = "";
        let firstband = true;
        td = rkWebUtil.elemaker( "td", tr, { "classes": [ 'aligntop' ] } );
        for ( let band in obj['bands'] ) {
            if ( firstband ) firstband = false;
            else rkWebUtil.elemaker( "br", td );
            rkWebUtil.elemaker( "b", td, { "text": band + ": " } );
            let first = true;
            for ( let ref of obj['bands'][band] ) {
                if ( first ) first = false;
                else td.appendChild( document.createTextNode( ", " ) );
                td.appendChild( document.createTextNode( ref + " " ) );
                let a = rkWebUtil.elemaker( "a", td, { "text": "(delete)", "classes": [ "link" ] } );
                a.addEventListener( "click", function( data ) { self.maybeDeleteReference( ref ); } );
            }
        }
    }
}

// **********************************************************************
// Needs to be moved

Curveball.ObjsAndRefs.prototype.maybeDeleteReference = function( basename ) {
    let self = this;
    var dialog = rkWebUtil.elemaker( "dialog", document.body );
    rkWebUtil.elemaker( "p", dialog, { "text": "Really delete image " + basename + " from the database? " +
                                       "This will also delete ALL aperture photometry associated with " +
                                       "this reference, and cannot be undone!" } );
    rkWebUtil.elemaker( "p", dialog, { "text": "This only deletes the image from the database.  It's up to you " +
                                       "to delete it (and its associated files) from your local data directory.  " +
                                       "The files will remain on the NERSC archive, but will be overwritten if " +
                                       "a new make_reference makes one with the same name.  (I hope.)" } );
    let cb = rkWebUtil.elemaker( "input", dialog, { 'attributes': { 'id': 'maybe_delete_reference_cb',
                                                                    'type': 'checkbox'
                                                                  }
                                                  } );
    rkWebUtil.elemaker( "label", dialog, { 'attributes': { 'for': 'maybe_delete_reference_cb' },
                                           'text': "Yes, I know what I'm doing." } );
    let form = rkWebUtil.elemaker( "form", dialog, { "attributes": { "method": "dialog" } } );
    let div = rkWebUtil.elemaker( "div", form, { "classes": [ "hbox", "spacebox" ] } );
    rkWebUtil.button( div, "Go Nuclear", null, { "value": "confirm" } );
    rkWebUtil.button( div, "Cancel", null, { "value": "cancel" } );

    dialog.addEventListener( "close", function() { self.actuallyMaybeDeleteReference( dialog, basename, cb ) } );
    dialog.showModal();
}

// **********************************************************************
// Needs to be moved

Curveball.ObjsAndRefs.prototype.actuallyMaybeDeleteReference = function( dialog, basename, cb )
{
    var self = this;
    
    if ( dialog.returnValue== "cancel" ) {
        document.body.removeChild( dialog );
        return;
    }
    if ( ! cb.checked ) {
        window.alert( "You may not know what you're doing, not deleting anything." );
        document.body.removeChild( dialog );
        return;
    }
    let data = { 'basename': basename };
    console.log( "ARGH " + JSON.stringify(data) );
    document.body.removeChild( dialog );
    this.context.connector.sendHttpRequest( "deleteimage/", data,
                                            function( datablob ) { self.context.frontpage.listObjsAndRefs(); } )
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

Curveball.ObjsWithPhotList = function( context, last=null ) {
    Curveball.Page.call( this, context, last );
    self.clickedonname = null;
}

Curveball.ObjsWithPhotList.prototype = Object.create( Curveball.Page.prototype,
                                                      { constructor: { value: Curveball.ObjsWithPhotList,
                                                                       enumerable: false,
                                                                       writeable: true,
                                                                       configurable: true } } );

Curveball.ObjsWithPhotList.prototype.pagename = "objects with subtractions & photometry";

// **********************************************************************

Curveball.ObjsWithPhotList.prototype.render = function( data=null ) {
    let self = this;
    let p, table, tr, td;

    this.renderTop();
    if ( ! this.loadData( data ) ) return;
    data = this.data;

    p = rkWebUtil.elemaker( "p", this.context.contentdiv, { "text": "Refresh List", "classes": [ "link" ] } );
    p.addEventListener( "click", function() {
        let subvtag = self.context.versiontagnames[ self.subvtwidget.value ];
        let phovtag = self.context.versiontagnames[ self.phovtwidget.value ];
        self.clickedonname = null;
        self.context.loading();
        self.context.connector.sendHttpRequest( "getobjswithphot/", { 'sub_vtag': subvtag, 'pho_vtag': phovtag },
                                                function(data) { self.render(data) } )
    } );

    rkWebUtil.elemaker( "h2", this.context.contentdiv, { "text": data["objects"].length.toString() + " Objects" } );

    // rkWebUtil.elemaker( "p", this.context.contentdiv,
    //                     { "text": ( "Subtraction counts are for subtractions tagged with '" + data['sub_vtag'] +
    //                                 "'\nPhotometry counts are for photomtery tagged with '" + data['pho_vtag' ] +
    //                                 "'" ) } );
    
    table = rkWebUtil.elemaker( "table", this.context.contentdiv );
    tr = rkWebUtil.elemaker( "tr", table );
    rkWebUtil.elemaker( "th", tr, { "text": "Object" } );
    rkWebUtil.elemaker( "th", tr, { "text": "RA" } );
    rkWebUtil.elemaker( "th", tr, { "text": "dec" } );
    rkWebUtil.elemaker( "th", tr, { "text": "t0" } );
    rkWebUtil.elemaker( "th", tr, { "text": "z" } );
    rkWebUtil.elemaker( "th", tr, { "text": "N.Subs" } );
    rkWebUtil.elemaker( "th", tr, { "text": "N.Pho on Subs" } );
    rkWebUtil.elemaker( "th", tr, { "text": ( "N.Pho ("
                                              + this.context.versiontagnames[ this.phovtwidget.value ]
                                              + ")" ) } );
    for ( let obj of data['objects'] ) {
        tr = rkWebUtil.elemaker( "tr", table );
        if ( ( self.clickedonname != null ) && ( obj['name'] == self.clickedonname ) ) {
            tr.scrollIntoView( true );
        }
        td = rkWebUtil.elemaker( "td", tr, { "text": obj['name'], "classes": [ "link", "aligntop" ] } );
        td.addEventListener( "click", function() {
            self.context.loading();
            let phovtag = self.context.versiontagnames[ self.phovtwidget.value ];
            self.context.connector.sendHttpRequest( "getltcvdata/" + obj['name'], { 'pho_vtag': phovtag },
                                                    function(data) {
                                                        self.clickedonname = obj['name'];
                                                        let showobj = new Curveball.ShowObject( self.context,
                                                                                                self, obj );
                                                        showobj.render( data );
                                                    } )
        } );
        rkWebUtil.elemaker( "td", tr, { "text": obj['ra'].toFixed(5), "classes": [ "aligntop" ] } );
        rkWebUtil.elemaker( "td", tr, { "text": obj['dec'].toFixed(5), "classes": [ "aligntop" ] } );
        rkWebUtil.elemaker( "td", tr, { "text": obj['t0'].toFixed(2), "classes": [ "aligntop" ] } );
        let ztext;
        if ( obj['z'] == null ) ztext = "--"; else ztext = obj['z'].toFixed(4);
        rkWebUtil.elemaker( "td", tr, { "text": ztext, "classes": [ "aligntop" ] } );

        td = rkWebUtil.elemaker( "td", tr, { "classes": [ "aligntop" ] } );
        let subul = rkWebUtil.elemaker( "ul", td );
        td = rkWebUtil.elemaker( "td", tr, { "classes": [ "aligntop" ] } );
        let phoinsubul = rkWebUtil.elemaker( "ul", td );
        for ( let subvt of obj['subvts'] ) {
            let subli = rkWebUtil.elemaker( "li", subul, { "text": subvt } );
            let subsubul = rkWebUtil.elemaker( "ul", subli );
            let phoinsubli = rkWebUtil.elemaker( "li", phoinsubul, { "text": subvt } );
            let subphoinsubli = rkWebUtil.elemaker( "ul", phoinsubli );
            for ( let sb of obj['subs'][subvt] ) {
                rkWebUtil.elemaker( "li", subsubul, { "text": sb['subband'] + ": " + sb['numsubs'] } );
                rkWebUtil.elemaker( "li", subphoinsubli, { "text": sb['subband'] + ": " + sb['phoinsubs'] } );
            }
        }

        td = rkWebUtil.elemaker( "td", tr, { "classes": [ "aligntop" ] } );
        let ul = rkWebUtil.elemaker( "ul", td );
        for ( let phovt of obj['phovts'] ) {
            let li = rkWebUtil.elemaker( "li", ul, { "text": phovt } );
            let subul = rkWebUtil.elemaker( "ul", li );
            for ( let pb of obj['phos'][phovt] ) {
                rkWebUtil.elemaker( "li", subul, { "text": pb['phoband'] + ": " + pb['numpho'] } );
            }
        }
    }
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

Curveball.ShowObject = function( context, last, obj ) {
    Curveball.Page.call( this, context, last );
    this.obj = obj;
    this.ltcvplots = null;
    this.bands = [];
    this.bandexpsources = {};
    this.ltcvs = {};
    this.plotters = {};
    this.ndrawn = 0;
    this.nplotters = 0;
}

Curveball.ShowObject.prototype = Object.create( Curveball.Page.prototype,
                                                { constructor: { value: Curveball.ShowObject,
                                                                 enumerable: false,
                                                                 writeable: true,
                                                                 configurable: true } } );

Curveball.ShowObject.prototype.pagename = "object ltcvs";

// **********************************************************************

Curveball.ShowObject.prototype.render = function( data ) {
    let self=this;

    this.renderTop();
    if ( ! this.loadData( data ) ) return;
    data = this.data;

    rkWebUtil.button( this.context.contentdiv, "Refresh Data",
                      function() {
                          self.context.contentdiv.removeChild( self.ltcvplots );
                          rkWebUtil.elemaker( "p", self.context.contentdiv, { "text": "Loading...",
                                                                              "classes": [ "warn" ] } );
                          self.ltcvplots = null;
                          let phovtag = self.context.versiontagnames[ self.phovtwidget.value ];
                          self.context.connector.sendHttpRequest(
                              "getltcvdata/" + self.obj.name, { 'pho_vtag': phovtag },
                              function(data) {
                                  self.render( data );
                              }
                          ) } );
    rkWebUtil.elemaker( "br", this.context.contentdiv );
    
    let div = rkWebUtil.elemaker( "div", this.context.contentdiv, { "classes": [ "hbox", "nofill" ] } )

    let table = rkWebUtil.elemaker( "table", div );
    for ( let field of [ 'pho_vtag', 'object', 'othernames', 'ra', 'dec', 't0', 'z', 'classname', 'confidence' ] ) {
        let tr = rkWebUtil.elemaker( "tr", table );
        rkWebUtil.elemaker( "td", tr, { "text": field, "classes": [ "bold" ] } );
        if ( data.hasOwnProperty( field ) && ( data[field] != null ) )
            rkWebUtil.elemaker( "td", tr, { "text": data[field] } );
        else
            rkWebUtil.elemaker( "td", tr, { "text": "—" } );
    }
    let tr = rkWebUtil.elemaker( "tr", table );
    let td = rkWebUtil.elemaker( "td", tr, { "attributes": { "colspan": 2 } } );
    rkWebUtil.button( td, "Edit Object Data",
                      function() {
                          let objeditor = new Curveball.EditObject( self.context, self, self.obj );
                          objeditor.render();
                      } );


    this.salt2fitdiv = rkWebUtil.elemaker( "div", div, { "classes": [ "vbox", "bordermarginpad" ] } );
    rkWebUtil.elemaker( "h3", this.salt2fitdiv, { "text": "Salt2 Fits" } )
    this.salt2fittable = rkWebUtil.elemaker( "table", this.salt2fitdiv );

    if ( this.ltcvplots == null ) {
        this.ltcvplots = rkWebUtil.elemaker( "div", this.context.contentdiv, { "classes": [ "ltcvplot_collection"] } );
        // rkWebUtil.elemaker( "p", this.ltcvplots, { "text": "Loading Lightcurve Data...",
        //                                            "classes": [ "warn" ] } );
        // this.context.connector.sendHttpRequest( "getltcvdata/" + this.obj.name, {},
        //                                         function( datablob ) { self.renderLtcvPlots( self.ltcvplots,
        //                                                                                      datablob ); } );
        this.renderLtcvPlots( this.ltcvplots, this.data );
    } else {
        this.context.contentdiv.appendChild( this.ltcvplots );
    }
}    

// **********************************************************************

Curveball.ShowObject.prototype.renderLtcvPlots = function( parentdiv, data )
{
    let self = this;

    rkWebUtil.wipeDiv( this.ltcvplots );
    
    for ( let band of data.bands ) {
        this.bands.push( band );
        this.bandexpsources[ band ] = data.band_expsources[band];
        this.ltcvs[band] = data.ltcv[band];
        this.plotters[band] = new Curveball.Plotter( this.context, this, parentdiv, this.obj,
                                                     band, this.bandexpsources[band], this.ltcvs[band] );
        this.nplotters += 1;
    }
    // Doing this in a separate loop because syncTimeAxes
    //  is going to compare how many times it was called
    //  to the length of this.plotters
    for ( let band of data.bands )
        this.plotters[band].render();

    // See if there are any salt2 fits
    this.loadSalt2Fits();   
}

// **********************************************************************

Curveball.ShowObject.prototype.loadSalt2Fits = function()
{
    var self = this;

    rkWebUtil.wipeDiv( this.salt2fittable );
    for ( let band of this.bands ) {
        this.plotters[band].fitset.replaceData( [], [] );
        this.plotters[band].redraw();
    }
    
    let tr = rkWebUtil.elemaker( "tr", this.salt2fittable );
    for ( let hdr of [ "Show", "mb*", "t0", "x0", "x1", "c", "χ²/ν", "bands", "tags" ] ) {
        rkWebUtil.elemaker( "th", tr, { "text": hdr } );
    }

    this.context.connector.sendHttpRequest( "getsalt2fits/" + this.obj.name, {},
                                            function( data ) { self.receivedSalt2Fits( data ); } );
}

// **********************************************************************

Curveball.ShowObject.prototype.receivedSalt2Fits = function( data )
{
    var self = this;
    
    if ( ( ! data.hasOwnProperty('status') ) || ( data['status'] != 'ok' ) ) {
        window.alert( "List of salt2 fits from server didn't return status ok." );
        return;
    }
    this.salt2fits = data['fits'];
    this.salt2fitids = [];
    this.salt2fitradios = {};
    this.shownfit = null;
    let first = true;
    for ( let fit of this.salt2fits ) {
        this.salt2fitids.push( fit.id );
        let tr = rkWebUtil.elemaker( "tr", this.salt2fittable );
        let td = rkWebUtil.elemaker( "td", tr );
        this.salt2fitradios[ fit.id ] = rkWebUtil.elemaker( "input", td,
                                                            { "attributes": { "type": "radio",
                                                                              "name": "salt2fitradios" } } );
        if ( first ) {
            this.salt2fitradios[ fit.id ].checked = true
            first = false;
        }
        this.salt2fitradios[ fit.id ].addEventListener( "change", function() { self.salt2FitRadioChanged(); } );
        rkWebUtil.elemaker( "td", tr, { "text": fit.mbstar.toFixed(3) + " ± " + fit.dmbstar.toFixed(3) } );
        rkWebUtil.elemaker( "td", tr, { "text": fit.t0.toFixed(2) + " ± " + fit.dt0.toFixed(2) } );
        rkWebUtil.elemaker( "td", tr, { "text": fit.x0.toExponential(2) + " ± " +
                                        fit.dx0.toExponential(2) } );
        rkWebUtil.elemaker( "td", tr, { "text": fit.x1.toFixed(3) + " ± " + fit.dx1.toFixed(3) } );
        rkWebUtil.elemaker( "td", tr, { "text": fit.c.toFixed(3) + " ± " + fit.dc.toFixed(3) } );
        rkWebUtil.elemaker( "td", tr, { "text": ( fit.chisq / fit.dof ).toFixed(2) } );
        rkWebUtil.elemaker( "td", tr, { "text": fit.bands.join( ", " ) } );
        rkWebUtil.elemaker( "td", tr, { "text": fit.versiontags.join( ", " ) } );
    }

    this.salt2FitRadioChanged();
}

// **********************************************************************

Curveball.ShowObject.prototype.salt2FitRadioChanged = function()
{
    var self = this;
    
    for ( let band of this.bands ) {
        this.plotters[band].fitset.replaceData( [], [] );
        this.plotters[band].redraw();
    }
    this.shownfit = null;

    for ( let id of this.salt2fitids ) {
        if ( this.salt2fitradios[ id ].checked ) {
            for ( let band of this.bands ) {
                this.context.connector.sendHttpRequest( "getsalt2fitcurve_fitidband/" + id + "/" + band, {},
                                                        function( data ) { self.plotSalt2Fit( band, data ); } );
            }
        }
    }
}

// **********************************************************************

Curveball.ShowObject.prototype.plotSalt2Fit = function( band, data ) {
    if ( ( ! data.hasOwnProperty( 'status' ) ) || ( data['status'] != 'ok' ) ) {
        window.alert( "Didn't get ok status from server with salt2 fit curve." );
        return;
    }

    for ( let i in data.flux ) data.flux[i] *= 1e6;
    this.plotters[band].fitset.replaceData( data.t, data.flux );
    this.plotters[band].redraw();
}

// **********************************************************************

Curveball.ShowObject.prototype.syncTimeAxes = function( limits=null ) {
    this.ndrawn += 1;
    if ( this.ndrawn >= this.nplotters ) {
        let xmin = 1e30;
        let xmax = -1e30;
        if ( limits == null ) {
            for ( let band of this.bands ) {
                let plotter = this.plotters[ band ];
                if ( plotter.svgplot.xmin < xmin ) xmin = plotter.svgplot.xmin;
                if ( plotter.svgplot.xmax > xmax ) xmax = plotter.svgplot.xmax;
            }
        }
        else {
            xmin = limits[0];
            xmax = limits[1];
        }
        for ( let band of this.bands ) {
            let plotter = this.plotters[ band ];
            console.log( "Synchronizing plot time axes to " + xmin + " , " + xmax );
            plotter.svgplot.defaultlimits = [ xmin, xmax, null, null ];
            // plotter.svgplot.zoomToDefault();
            plotter.svgplot.zoomTo( xmin, xmax, plotter.svgplot.ymin, plotter.svgplot.ymax );
        }
    }
}


// **********************************************************************
// **********************************************************************
// **********************************************************************
// Lightcurve plotter

Curveball.Plotter = function( context, parent, parentdiv, obj, band, expsrcid, ltcv )
{
    this.context = context;
    this.parent = parent;
    this.parentdiv = parentdiv;
    this.obj = obj;
    this.band = band;
    this.expsrcid = expsrcid;
    this.ltcv = ltcv;
}

// **********************************************************************

Curveball.Plotter.prototype.render = function()
{
    let self = this;
    var div;
    
    this.row = rkWebUtil.elemaker( "div", this.parentdiv, { "classes": [ "ltcvplot_row" ] } );
    this.plotdiv = rkWebUtil.elemaker( "div", this.row,
                                       { "classes": [ "ltcvplot" ],
                                         "attributes": { "id": "ltcv.svgdiv::" + this.obj.name + "::"
                                                         + this.band + "::" + this.expsrcid }
                                       } );
    this.sidebardiv = rkWebUtil.elemaker( "div", this.row, { "classes": [ "ltcvplot_sidebar" ] } );
    this.saltparamdiv = rkWebUtil.elemaker( "div", this.sidebardiv, { "classes": [ "saltparamdiv" ] } );
    this.pointinfodiv = rkWebUtil.elemaker( "div", this.sidebardiv, { "classes" : [ "pointinfodiv" ] } );
    this.pointinfotext = rkWebUtil.elemaker( "div", this.pointinfodiv, { "classes" : [ "pointinfotext" ] } );
    this.pointclips = rkWebUtil.elemaker( "div", this.pointinfodiv, { "classes" : [ "pointclips" ] } );
    this.belowclips = rkWebUtil.elemaker( "div", this.pointinfodiv, { "classes" : [ "belowclips" ] } );

    div = rkWebUtil.elemaker( "div", this.pointclips, { "classes": [ "oneclipmeta" ] } );
    rkWebUtil.elemaker( "h3", div, { "text": "New" } );
    div = rkWebUtil.elemaker( "div", div, { "classes" : [ "oneclip" ] } );
    this.newclip = rkWebUtil.elemaker( "div", div, { "classes" : [ "oneclipwrapper" ] } );

    div = rkWebUtil.elemaker( "div", this.pointclips, { "classes": [ "oneclipmeta" ] } );
    rkWebUtil.elemaker( "h3", div, { "text": "Ref" } );
    div = rkWebUtil.elemaker( "div", div, { "classes": [ "oneclip" ] } );
    this.refclip = rkWebUtil.elemaker( "div", div, { "classes": [ "oneclipwrapper" ] } );

    div = rkWebUtil.elemaker( "div", this.pointclips, { "classes": [ "oneclipmeta"] } );
    rkWebUtil.elemaker( "h3", div, { "text": "Sub" } );
    div = rkWebUtil.elemaker( "div", div, { "classes": [ "oneclip" ] } );
    this.subclip = rkWebUtil.elemaker( "div", div, { "classes": [ "oneclipwrapper" ] } );

    this.svgplot = new SVGPlot.Plot( { "divid": ( "ltcv::svgdiv::" + this.object + "::" + this.band
                                                  + "::" + this.expsourceid ),
                                       "svgid": ( "ltcv::svg::" + this.object + "::" + this.band
                                                  + "::" + this.expsourceid ),
                                       "title": this.band,
                                       "xtitle": "t (MJD)",
                                       "ytitle": "flux-ish (10¯⁶ Jy)",
                                     } );
    rkWebUtil.button( this.svgplot.buttonbox, "Sync Time Axes",
                      function(e) {
                          self.parent.syncTimeAxes( [ self.svgplot.xmin, self.svgplot.xmax, null, null ] );
                      } );
    this.plotdiv.appendChild( this.svgplot.topdiv );
    this.dataset = new SVGPlot.Dataset( { "name": "data", "linewid": 0,
                                          "marker": "dot", "color": "#008800", "highlightcolor": "#0088cc" } );
    this.svgplot.addDataset( this.dataset );
    this.baddataset = new SVGPlot.Dataset( { "name": "baddata", "linewid": 0,
                                             "marker": "circle", "color": "#cc0000", "highlightcolor": "#0088cc" } );
    this.svgplot.addDataset( this.baddataset );
    this.fitset = new SVGPlot.Dataset( { "name": "fit", "color": "#0000cc", "marker": null, "linewid": 2,
                                         "clickselect": false } );
    this.svgplot.addDataset( this.fitset );

    var x = [];
    var y = [];
    var dy = [];
    var pointnames = [];
    for ( let i in this.ltcv ) {
        // Javascript is dysfunctional.  This was returned as a list in
        // Python from the server.  So, I'd think that this.ltcv would
        // be the same sort of thing here, indexed by an integer.  But,
        // it seems that i is a string here, and I *think* that is
        // causing me issues later.  In any event, when I try to compare
        // the name to the lightcurve index, I'm having problems, and
        // the fact that this is a "oh, I'm so loosely typed that I'll
        // just randomly change the types of things, but, oh wait, when
        // you compare, I'm maybe not so loosely typed after all"
        // language probably isn't helping.
        i = parseInt( i );
        let pointinfo = this.ltcv[i];
        pointinfo.fluxmicrojy = pointinfo.flux * ( 10 ** ( ( 8.9 - pointinfo.magzp ) / 2.5 ) ) * 1e6;
        pointinfo.dfluxmicrojy = pointinfo.dflux * ( 10 ** ( ( 8.9 - pointinfo.magzp ) / 2.5 ) ) * 1e6;
        if ( ! pointinfo.is_bad ) {
            x.push( pointinfo.mjd );
            y.push( pointinfo.fluxmicrojy );
            dy.push( pointinfo.dfluxmicrojy );
            pointnames.push( i );
        }
    }
    this.dataset.replaceData( x, y, [], dy, pointnames );

    x = [];
    y = [];
    dy = [];
    pointnames = [];
    for ( let i in this.ltcv ) {
        i = parseInt( i );
        let pointinfo = this.ltcv[i];
        if ( pointinfo.is_bad ) {
            x.push( pointinfo.mjd );
            y.push( pointinfo.fluxmicrojy );
            dy.push( pointinfo.dfluxmicrojy );
            pointnames.push( i )
        }
    }
    this.baddataset.replaceData( x, y, [], dy, pointnames );

    this.svgplot.redraw();
    this.svgplot.addClickListener( function( info ) { self.clicked_on_point( info ) } );
    this.parent.syncTimeAxes();
}

// **********************************************************************

Curveball.Plotter.prototype.redraw = function()
{
    this.svgplot.redraw();
}

// **********************************************************************

Curveball.Plotter.prototype.find_point = function( name ) {
    let retval = [];
    for ( let setdex in this.svgplot.datasets ) {
        for ( let pointdex in this.svgplot.datasets[setdex]._pointnames ) {
            if ( name == this.svgplot.datasets[setdex]._pointnames[ pointdex ] ) {
                retval.push( { 'dataset': this.svgplot.datasets[setdex],
                               'setdex': setdex,
                               'pointdex': pointdex } );
            }
        }
    }
    return retval;
}

// **********************************************************************

Curveball.Plotter.prototype.clicked_on_point = function( info ) {
    let self = this;
    var ns = "http://www.w3.org/2000/svg";

    rkWebUtil.wipeDiv( this.pointinfotext );
    // I'm a bad person because I'm using an underscore object field
    let ltcvdex = parseInt( info.dataset._pointnames[ parseInt( info.pointdex ) ] );
    let point = this.ltcv[ ltcvdex ]
    this.point = point;
    let dt = rkWebUtil.dateofmjd( point.mjd );
    this.pointinfotext.appendChild( document.createTextNode( "t: MJD " + point.mjd.toFixed(5) + " ("
                                                             + dt.toISOString() + ")" ) );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    if ( ( Math.abs(point.fluxmicrojy) > 1e4 ) || ( Math.abs(point.fluxmicrojy) < 1 ) )
        this.pointinfotext.appendChild( document.createTextNode( "flux: " + point.fluxmicrojy.toExponential(3)
                                                                 + " ± " + point.dfluxmicrojy.toExponential(3)
                                                                 + " 10¯⁶ Jy" ) );
    else
        this.pointinfotext.appendChild( document.createTextNode( "flux: " + point.fluxmicrojy.toFixed(3)
                                                                 + " ± " + point.dfluxmicrojy.toFixed(3)
                                                                 + " 10¯⁶ Jy" ) );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    if ( point.mag == null )
        this.pointinfotext.appendChild( document.createTextNode( "mag: (null)" ) );
    else
        this.pointinfotext.appendChild( document.createTextNode( "mag: " + point.mag.toFixed(3) +
                                                                 " ± " + point.dmag.toFixed(3) ) );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    this.pointinfotext.appendChild( document.createTextNode( "image: " + point.imagebasename + ".fits" ) );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    this.pointinfotext.appendChild( document.createTextNode( "Position on image: " + point.imagex +
                                                             " , " + point.imagey ) );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    this.pointinfotext.appendChild( document.createTextNode( "magzp: " + point.magzp.toFixed(3) + 
                                                             " ; apercor: " + point.apercor.toFixed(3) +
                                                             " ; aperrad: " + point.aperrad.toFixed(2) ) );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    rkWebUtil.elemaker( "br", this.pointinfotext );
    this.badcheckbox = rkWebUtil.elemaker( "input", this.pointinfotext,
                                           { "attributes": { "type": "checkbox",
                                                             "id": "bad-check-" + this.band + "-" +
                                                                   info.setdex + "-" + info.pointdex
                                                           }
                                           }
                                         );
    if ( point.is_bad ) this.badcheckbox.setAttribute( "checked", true );
    this.badcheckbox.addEventListener( "change", function( e ) { self.changeIsBad( e, ltcvdex ); } );
    rkWebUtil.elemaker( "label", this.pointinfotext, { "text": "Photometry point is bad",
                                                       "attributes":
                                                       { "for": "bad-check-" + this.band + "-" +
                                                                 info.setdex + "-" + info.pointdex }
                                                     } );
    this.badupdatingnotice = rkWebUtil.elemaker( "span", this.pointinfotext, { "classes": [ "warn" ] } );
    
    let style;
    style = document.createElementNS( ns, "style" );
    rkWebUtil.wipeDiv( this.newclip );
    rkWebUtil.elemaker( "img", this.newclip,
                        { "attributes": {
                            "src": "data:image/jpeg;base64," + point.new_jpeg,
                            "width": 153,
                            "height": 153,
                            "alt": "New" } } )
    this.newsvg = document.createElementNS( ns, "svg" );
    this.newsvg.setAttributeNS( 'http://www.w3.org/2000/xmlns/', "xmlns", ns );
    this.newsvg.setAttribute( "viewBox", "0 0 153 153" );
    style = document.createElementNS( ns, "style" );
    this.newsvg.appendChild( style );
    this.newclip.appendChild( this.newsvg );
    this.newaper = null;
    rkWebUtil.wipeDiv( this.refclip );
    rkWebUtil.elemaker( "img", this.refclip,
                        { "attributes": {
                            "src": "data:image/jpeg;base64," + point.ref_jpeg,
                            "width": 153,
                            "height": 153,
                            "alt": "Ref" } } )
    this.refsvg = document.createElementNS( ns, "svg" );
    this.refsvg.setAttributeNS( 'http://www.w3.org/2000/xmlns/', "xmlns", ns );
    this.refsvg.setAttribute( "viewBox", "0 0 153 153" );
    style = document.createElementNS( ns, "style" );
    this.refsvg.appendChild( style );
    this.refclip.appendChild( this.refsvg );
    this.refaper = null;
    rkWebUtil.wipeDiv( this.subclip );
    rkWebUtil.elemaker( "img", this.subclip,
                        { "attributes": {
                            "src": "data:image/jpeg;base64," + point.sub_jpeg,
                            "width": 153,
                            "height": 153,
                            "alt": "Sub" } } )
    this.subsvg = document.createElementNS( ns, "svg" );
    this.subsvg.setAttributeNS( 'http://www.w3.org/2000/xmlns/', "xmlns", ns );
    this.subsvg.setAttribute( "viewBox", "0 0 153 153" );
    style = document.createElementNS( ns, "style" );
    this.subsvg.appendChild( style );
    this.subclip.appendChild( this.subsvg );
    this.subaper = null;
    style.appendChild( document.createTextNode( '.aper { stroke: #40c040; stroke-width: 1; fill: none }' ) );

    rkWebUtil.wipeDiv( this.belowclips );
    this.showapbutton = rkWebUtil.button( this.belowclips, "Show Aperture",
                                          function(e) { self.draw_aper() } );
}

// **********************************************************************

Curveball.Plotter.prototype.changeIsBad = function( e, ltcvdex )
{
    var self = this;
    rkWebUtil.wipeDiv( this.badupdatingnotice );
    e.target.disabled = true;
    this.badupdatingnotice.appendChild( document.createTextNode( "Updating bad flag on server..." ) );
    let point = this.ltcv[ ltcvdex ];
    let isbad = e.target.checked;
    this.context.connector.sendHttpRequest( "setpointbad/", { "id": point.id,
                                                              "is_bad": isbad },
                                            function( data ) { self.processChangeIsBad( e.target, ltcvdex, data ) } );
}

Curveball.Plotter.prototype.processChangeIsBad = function( checkbox, ltcvdex, data )
{
    let point = this.ltcv[ ltcvdex ];
    rkWebUtil.wipeDiv( this.badupdatingnotice );
    if ( point.id != data.id ) {
        this.badupdatingnotice.appendChild( document.createTextNode( "ERROR" ) );
        window.alert( "ERROR: tried to set a lightcurve point bad, but the point id didn't match.  Panic!" );
        return;
    }
    let flux = point.flux * ( 10 ** ( ( 8.9 - point.magzp ) / 2.5 ) ) * 1e6;
    let dflux = point.dflux * ( 10 ** ( ( 8.9 - point.magzp ) / 2.5 ) ) * 1e6;

    // Remove the point from everywhere and add it where it's needed
    let foundpoints = this.find_point( ltcvdex );
    for ( let foundpoint of foundpoints ) foundpoint[ 'dataset' ].removePoint( ltcvdex )
    if ( data.is_bad ) {
        this.baddataset.addPoint( point.mjd, flux, null, dflux, ltcvdex );
    }
    else {
        this.dataset.addPoint( point.mjd, flux, null, dflux, ltcvdex );
    }
    this.svgplot.redraw();
    point.is_bad = data.is_bad;
    checkbox.checked = data.is_bad;
    checkbox.disabled = false;
}

// **********************************************************************(

Curveball.Plotter.prototype.draw_aper = function() {
    if ( ! this.hasOwnProperty( "subsvg" ) ) return;

    var ns = "http://www.w3.org/2000/svg";

    if ( this.subaper != null ) {
        this.newaper.remove();
        this.newaper = null;
        this.refaper.remove();
        this.refaper = null;
        this.subaper.remove();
        this.subaper = null;
        // this.showapbutton.value = "Show Aperture";
        this.showapbutton.innerHTML = "Show Aperture";
        return;
    }

    // this.showapbutton.value = "Hide Aperture";
    this.showapbutton.innerHTML = "Hide Aperture";
    
    // I'm pretty sure that the way I made these clips, the nearest
    // pixel on the image to (imagex-1,imagey-1) will be made the
    // central pixel of the clip.  (The whole -1 business is to convert
    // from FITS to 0-offset coords.)  BUT, then, just to make life
    // interesting, I rotated the clip so that north is up and east
    // is left.
    //
    // So, (imagex-1, imagey-1) corresponds to the center of pixel
    // (25,25) on the clip, and since we're expanding by a factor of 3,
    // that's the center of pixel (76,76) on the image (which is
    // position 76.5, 76.5 in svg units, as svg does it RIGHT and puts
    // (0.5, 0.5) at the center of a pixel, unlike fits which does it
    // WRONG and puts (0, 0) at the center of a pixel.)
    //
    // But, we have to deal with rotation....

    let imgxctr = 25.5 + ( this.point.imagex - Math.floor( this.point.imagex + 0.5 ) )
    let imgyctr = 25.5 + ( this.point.imagey - Math.floor( this.point.imagey + 0.5 ) )
    let orient = this.point.orientation;
    let xctr, yctr;
    if ( orient == null ) {
        xctr = 3 * imgxctr;
        yctr = 3 * imgyctr;
    }
    else {
        if ( ( orient == 0 ) || ( orient == 6 ) )
            xctr = 3 * imgxctr;
        else if ( ( orient == 2 ) || ( orient == 4 ) )
            xctr = 153 - 3 * imgxctr;
        else if ( ( orient == 1 ) || ( orient == 7 ) )
            xctr = 3 * imgyctr;
        else if ( ( orient == 3 ) || ( orient == 5 ) )
            xctr = 153 - 3 * imgyctr;
        else {
            window.alert( "Unknown orientation " + orientation );
            return;
        }

        if ( ( orient == 2 ) || ( orient == 6 ) )
            yctr = 3 * imgyctr;
        else if ( ( orient == 0 ) || ( orient == 4 ) )
            yctr = 153 - 3 * imgyctr;
        else if ( ( orient == 1 ) || ( orient = 5 ) )
            yctr = 3 * imgxctr;
        else
            yctr = 153 - 3 * imgxctr;
    }
    let rad = this.point.aperrad*3;

    let circle = document.createElementNS( ns, "circle" );
    circle.setAttribute( "cx", xctr );
    circle.setAttribute( "cy", yctr );
    circle.setAttribute( "r", rad );
    circle.setAttribute( "class", "aper" );
    this.refaper = circle.cloneNode();
    this.refsvg.appendChild( this.refaper );
    this.newaper = circle.cloneNode();
    this.newsvg.appendChild( this.newaper );
    this.subaper = circle.cloneNode();
    this.subsvg.appendChild( this.subaper );
}











// **********************************************************************
// **********************************************************************
// Everything below needs to be refactored

// **********************************************************************
// **********************************************************************
// **********************************************************************
// Lightcurve plotter 0LD

// Curveball.Plotter = function( context, parent, object, band, expsource ) {
//     this.object = object;
//     this.band = band;
//     this.expsource = expsource;
//     this.parent = parent;

//     this.rowdiv = elem;
//     if ( this.rowdiv.children.length < 2 ) {
//         console.log( "Missing child of div " + this.rowdiv.id );
//         return;
//     }
//     this.plotdiv = this.rowdiv.children[0];
//     this.sidebardiv = this.rowdiv.children[1];
//     const rowmatch = this.rowdiv.id.match( /^ltcv::(.*)::(.*)::(.*)::row$/ );
//     const plotmatch = this.plotdiv.id.match( /^ltcv::(.*)::(.*)::(.*)$/ );
//     const sidebarmatch = this.sidebardiv.id.match( /^ltcv::(.*)::(.*)::(.*)::sidebar$/ );
//     let ok = true;
//     if ( rowmatch == null ) {
//         console.log( "Unexpected child div in lightcurve plots: " + this.rowdiv.id );
//         ok = false
//     }
//     else {
//         this.salt2fit = {};
//         this.object = rowmatch[1];
//         this.band = rowmatch[2];
//         this.expsourceid = rowmatch[3]
//         if ( plotmatch == null ) {
//             console.log( "Unexpected child div of row in lightcurve plots: " + this.plotdiv.id );
//             ok = false;
//         }
//         else {
//             if ( sidebarmatch == null ) {
//                 console.log( "Unexpected child div of row in lightcurve plots: " + this.sidebardiv.id );
//                 ok = false;
//             }
//             else { 
//                 if ( ( this.object != plotmatch[1] ) || ( this.band != plotmatch[2] ) ||
//                      ( this.object != sidebarmatch[1] ) || ( this.band != sidebarmatch[2] ) ) {
//                     console.log( "Row div id " + this.rowdiv.id + " inconsistent with children ids "
//                                  + this.plotdiv.id + ", " + this.sidebardiv.id );
//                     ok = false;
//                 }
//             }
//         }
//     }
//     if ( !ok ) {
//         this.object = null;
//         this.band = null;
//         this.expsourceid = null;
//         return;
//     }
//     this.connector = new rkWebUtil.Connector( parent.webapurl );
// }

// Curveball.Plotter.prototype.render = function() {
//     if ( this.object == null ) {
//         return;
//     }
//     var self = this;
//     var div;
    
//     rkWebUtil.wipeDiv( this.sidebardiv );
//     this.saltparamdiv = rkWebUtil.elemaker( "div", this.sidebardiv,
//                                             { "classes": [ "saltparamdiv" ] } );
//     this.pointinfodiv = rkWebUtil.elemaker( "div", this.sidebardiv,
//                                             { "classes" : [ "pointinfodiv" ] } );
//     this.pointinfotext = rkWebUtil.elemaker( "div", this.pointinfodiv,
//                                              { "classes" : [ "pointinfotext" ] } );
//     this.pointclips = rkWebUtil.elemaker( "div", this.pointinfodiv,
//                                           { "classes" : [ "pointclips" ] } );
//     this.belowclips = rkWebUtil.elemaker( "div", this.pointinfodiv,
//                                           { "classes" : [ "belowclips" ] } );
    
//     div = rkWebUtil.elemaker( "div", this.pointclips, { "classes": [ "oneclipmeta" ] } );
//     rkWebUtil.elemaker( "h3", div, { "text": "New" } );
//     div = rkWebUtil.elemaker( "div", div, { "classes" : [ "oneclip" ] } );
//     this.newclip = rkWebUtil.elemaker( "div", div, { "classes" : [ "oneclipwrapper" ] } );

//     div = rkWebUtil.elemaker( "div", this.pointclips, { "classes": [ "oneclipmeta" ] } );
//     rkWebUtil.elemaker( "h3", div, { "text": "Ref" } );
//     div = rkWebUtil.elemaker( "div", div, { "classes": [ "oneclip" ] } );
//     this.refclip = rkWebUtil.elemaker( "div", div, { "classes": [ "oneclipwrapper" ] } );

//     div = rkWebUtil.elemaker( "div", this.pointclips, { "classes": [ "oneclipmeta"] } );
//     rkWebUtil.elemaker( "h3", div, { "text": "Sub" } );
//     div = rkWebUtil.elemaker( "div", div, { "classes": [ "oneclip" ] } );
//     this.subclip = rkWebUtil.elemaker( "div", div, { "classes": [ "oneclipwrapper" ] } );

//     rkWebUtil.wipeDiv( this.plotdiv );
//     this.svgplot = new SVGPlot.Plot( { "divid": ( "ltcv::svgdiv::" + this.object + "::" + this.band
//                                                   + "::" + this.expsourceid ),
//                                        "svgid": ( "ltcv::svg::" + this.object + "::" + this.band
//                                                   + "::" + this.expsourceid ),
//                                        "title": this.band,
//                                        "xtitle": "t (MJD)",
//                                        "ytitle": "flux-ish (10¯⁶ Jy)",
//                                      } );
//     this.plotdiv.appendChild( this.svgplot.topdiv );
//     this.dataset = new SVGPlot.Dataset( { "name": "data", "linewid": 0 } );
//     this.svgplot.addDataset( this.dataset );
//     this.fitset = new SVGPlot.Dataset( { "name": "fit", "color": "#0000cc", "marker": null, "linewid": 2,
//                                          "clickselect": false } );
//     this.svgplot.addDataset( this.fitset );

//     this.connector.sendHttpRequest( "getltcvdata/" + this.object + "/" + this.band + "/" + this.expsourceid, {}, 
//                                     function( datablob ) { self.plot_data( datablob ) } );

//     this.connector.sendHttpRequest( "getsalt2fitcurve_objband/" + this.object + "/" + this.band, {},
//                                     function( datablob ) { self.plot_fit( datablob ) } );
// }

// Curveball.Plotter.prototype.plot_data = function( datablob ) {
//     var self = this;
//     if ( datablob.status == 'error' ) {
//         rkWebUtil.wipeDiv( this.div );
//         rkWebUtil.elemaker( "h3", this.div, { "text": "Error getting lightcurve data" } )
//         rkWebUtil.elemaker( "p", this.div, { "text": datablob.error } )
//         rkWebUtil.elemaker( "pre", this.div, { "text": datablob.traceback } )
//         return;
//     }
//     this.ltcv = datablob['ltcv'];
//     var x = [];
//     var y = [];
//     var dy = [];
//     for ( let pointinfo of this.ltcv ) {
//         x.push( pointinfo.mjd );
//         y.push( pointinfo.flux * 1e6 );
//         dy.push( pointinfo.dflux * 1e6 );
//     }
//     // console.log( "x=" + x, "y=" + y );
//     this.dataset.replaceData( x, y, [], dy );
//     this.svgplot.redraw();
//     this.svgplot.addClickListener( function( info ) { self.clicked_on_point( info ) } );
//     this.parent.syncTimeAxes();
// }
        
// Curveball.Plotter.prototype.plot_fit = function( datablob ) {
//     if ( datablob.status == 'error' ) {
//         rkWebUtil.wipeDiv( this.div );
//         rkWebUtil.elemaker( "h3", this.div, { "text": "Error getting lightcurve fit" } )
//         rkWebUtil.elemaker( "p", this.div, { "text": datablob.error } )
//         rkWebUtil.elemaker( "pre", this.div, { "text": datablob.traceback } )
//         return;
//     }
//     var x = [];
//     var y = [];
//     let usefit = null;
//     for ( let fit of datablob.fits ) {
//         if ( fit.primary ) {
//             usefit = fit;
//             if ( fit.donotuse ) {
//                 console.log( "Primary fit is marked do not use!!!!!!!" )
//             }
//             break;
//         }
//     }
//     if ( usefit == null ) {
//         for ( let fit of datablob.fits ) {
//             if ( ! fit.donotuse ) {
//                 usefit = fit;
//                 break;
//             }
//         }
//     }
//     if ( usefit != null ) {
//         x = usefit.t;
//         y = usefit.flux;
//         for ( let i in y ) {
//             y[i] *= 1e6;
//         }
//     }
//     else {
//         console.log( "No non-do-not-use fit for " + this.object + " in " + this.band )
//     }
//     this.fitset.replaceData( x, y );
//     this.svgplot.redraw();
// }




// **********************************************************************
// **********************************************************************
// **********************************************************************

export { Curveball }

