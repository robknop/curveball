import os
import math
import pathlib
import ctypes

import numpy

_rundir = pathlib.Path( __file__ ).resolve().parent

# _rundir.parent / "lib" needs to be in the LD_LIBRARY_PATH
# (The dockerfile has this.)

libcurveball = ctypes.CDLL( "libcurveball.so" )
libcurveball.fit_psf_stumbler.argtypes = [ ctypes.c_char_p,
                                           numpy.ctypeslib.ndpointer( dtype=numpy.float64, ndim=2, flags="C" ),
                                           numpy.ctypeslib.ndpointer( dtype=numpy.float64, ndim=2, flags="C" ),
                                           ctypes.c_int, ctypes.c_int, ctypes.c_int,
                                           ctypes.c_double, ctypes.c_double, ctypes.c_double,
                                           ctypes.c_double, ctypes.c_double, ctypes.c_double,
                                           ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_int,
                                           numpy.ctypeslib.ndpointer( dtype=numpy.float64, ndim=1, flags="C" ),
                                           numpy.ctypeslib.ndpointer( dtype=numpy.float64, ndim=2, flags="C" ),
                                           ctypes.c_ulong, ctypes.c_char_p ]
libcurveball.fit_psf_stumbler.restype = None

libcurveball.fit_psf_lm.argtypes = [ ctypes.c_char_p,
                                     numpy.ctypeslib.ndpointer( dtype=numpy.float64, ndim=2, flags="C" ),
                                     numpy.ctypeslib.ndpointer( dtype=numpy.float64, ndim=2, flags="C" ),
                                     ctypes.c_int, ctypes.c_int, ctypes.c_int,
                                     ctypes.c_double, ctypes.c_double, ctypes.c_double,
                                     ctypes.c_int,
                                     numpy.ctypeslib.ndpointer( dtype=numpy.float64, ndim=1, flags="C" ),
                                     numpy.ctypeslib.ndpointer( dtype=numpy.float64, ndim=2, flags="C" ) ]
libcurveball.fit_psf_stumbler.restype = None

def psffit( clip, clipweight, psffile, clip0x, clip0y, initflux, initx, inity, fixpos=False, method='lm',
            sigflux=None, sigx=0.1, sigy=0.1, steps=200, burnsteps=200, nwalkers=20,
            stumblerseed=0, filebase="" ):
    """Fit a psf (over a 0 background).

    Parameters
    ----------
    clip : numpy array
       A square array with the data to be fit, extracted from the image
       for which psffile is the PSFEx psf

    clipweight : numpy.array
       An array with the same size of clip holding 1/σ²

    psffile : Path or str
       The psfex .psf file for this image.  The full file path must be
       able to resolve to ASCII.

    clip0x : int
       The x-position on the full image of the lower-left corner of the clip (0-indexed)

    clip0y : int
       The y-position on the full image of the lower-left corner of the clip (0-indexed)

    initflux : float
       Initial flux guess

    initx : float
       Initial x-position guess (or fixed xposition if fixpos=True)

    inity : float
       Initial y-position guess (or fixed yposition if fixpos=True )

    fixpos : boolean, default False
       If True, fix the psf position at initx, inity; otherwise, fit it

    method : str default 'lm
       Must be 'lm' or 'mcmc'.  If 'lm', does a Levenberg-Marquardt fit
       using GSL.  If 'mcmc', does a Goodman & Weare MCMC using
       stumbler.

    sigflux : float, default None
       Scatter initial walker fluxes by a normal with this σ around
       initflux; default is √initflux.  (Ignored if method = 'lm' )

    sigx : float, default 0.1
       Scatter initial walker x by a normal with this σ around initx.
      (Ignored if method = 'lm' )

    sigy : float, default 0.1
       Scatter initial walker y by a normal with this σ around inity.
       (Ignored if method = 'lm' )

    steps : int, default 200
       Number of post-burn-in steps each MCMC walker should take.
       (Ignored if method = 'lm' )

    burnsteps : int, default 200
       Number of burn-in steps each MCMC walker should take.
       (Ignored if method = 'lm' )

    nwalkers : int, default 20
       Number of MCMC walkers.
       (Ignored if method = 'lm' )

    stumblerseed : long int, default 0
       A seed for the MCMC to use; default is system entropy.
       (Ignored if method = 'lm' )

    filebase : str, default ""
       Write chain files based on this name; defaut is not to write them.
       (Ignored if method = 'lm' )

    Returns
    -------
    param, covar : (3,) and (3,3) shaped numpy arrays

    """

    if isinstance( psffile, pathlib.Path ):
        psffile = str( psffile.resolve() )
    elif not isinstance( psffile, str ):
        raise TypeError( f"psffile must be a Path or str, not {type(psffile)}" )

    if ( ( not isinstance( clip, numpy.ndarray) ) or ( clip.ndim != 2 ) or ( clip.shape[0] != clip.shape[1] ) ):
        raise TypeError( f"clip must be a square numpy array" )
    if ( ( not isinstance( clipweight, numpy.ndarray ) ) or ( clipweight.shape != clip.shape ) ):
        raise TypeError( f"clipweight must be a numpy array the same shape as clip" )

    clip0x = int( clip0x )
    clip0y = int( clip0y )
    initflux = float(initflux )
    initx = float( initx )
    inity = float( inity )
    sigflux = float( sigflux ) if sigflux is not None else math.sqrt( initflux )
    sigx = float( sigx )
    sigy = float( sigy )
    steps = int( steps )
    burnsteps = int( burnsteps )
    nwalkers = int( nwalkers )
    stumblerseed = int( stumblerseed )  # long?
    filebase = str( filebase )

    param = numpy.ascontiguousarray( numpy.empty( 3, dtype=numpy.float64 ) )
    covar = numpy.ascontiguousarray( numpy.empty( (3, 3), dtype=numpy.float64 ) )

    ifixpos = 1 if fixpos else 0

    if method == 'mcmc':
        libcurveball.fit_psf_stumbler( psffile.encode('ascii'),
                                       numpy.ascontiguousarray(clip, dtype=numpy.float64),
                                       numpy.ascontiguousarray(clipweight, dtype=numpy.float64),
                                       clip.shape[0], clip0x, clip0y,
                                       initflux, initx, inity, sigflux, sigx, sigy,
                                       ifixpos, steps, burnsteps, nwalkers, param, covar,
                                       stumblerseed, filebase.encode('ascii') )
    elif method == 'lm':
        libcurveball.fit_psf_lm( psffile.encode('ascii'),
                                 numpy.ascontiguousarray( clip, dtype=numpy.float64 ),
                                 numpy.ascontiguousarray( clipweight, dtype=numpy.float64 ),
                                 clip.shape[0], clip0x, clip0y,
                                 initflux, initx, inity, ifixpos,
                                 param, covar )

    return param, covar


# def psffit_extract( image, weight, psffile, clipsize, x, y, flux, fixpos=False ):
#     """Fit a psf (over a 0 background).

#     Parameters
#     ----------
#     image : numpy.array
#        The full image data.

#     weight : numpy.array
#        The weight image (1/variance)

#     psffile : str
#        The name of the psfex .psf file for the psf for this image

#     clipsize : int
#        The size of the clip to extract from the image.  If even, 1 will be added.

#     x, y : float
#        The initial position for the fit; C-coordinates

#     flux : float
#        The initial flux for the fit

#     fixpos : bool, default False
#        If true, then force the position at (x, y)

#     Returns
#     -------
#       param, covar : numpy arrays

#     """

#     if not ( clipsize % 2 ):
#         clipsize += 1

#     ix = int( x + 0.5 )
#     iy = int( y + 0.5 )
#     ix0 = ix - clipsize // 2
#     iy0 = iy - clipsize // 2

#     fluxsig = 0.05 * flux
#     xsig = 0.1
#     ysig = 0.1

#     steps = 100
#     burnsteps = 100
#     nwalkers = 20

#     if ( ix < 0 ) or ( iy < 0 ) or ( ix0 + clipsize >= image.shape[1] ) or ( iy0 + clipsize >= image.shape[2] ):
#         raise ValueError( "Too close to edge for clipsize." )

#     imdata = image[iy0:iy0+clipsize, ix0:ix0+clipsize].copy()
#     wtdata = weight[iy0:iy0+clipsize, ix0:ix0+clipsize].copy()

#     if fixpos:
#         param = numpy.empty( 1, dtype=numpy.float64 )
#         covar = numpy.empty( (1, 1), dtype=numpy.float64 )
#         ifixpos = 1
#     else:
#         param = numpy.empty( 3, dtype=numpy.float64 )
#         covar = numpy.empty( (3, 3), dtype=numpy.float64 )
#         ifixpos = 0

#     libcurveball.fit_psf( psffile, imdata, wtdata, clipsize, ix0, iy0, flux, x, y, fluxsig, xsig, ysig,
#                           ifixpos, steps, burnsteps, nwalkers, param, covar )

#     return param, covar
