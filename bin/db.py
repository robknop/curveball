import sys
import os
import pathlib
from datetime import datetime, timedelta
import dateutil.parser
import pytz
import subprocess
import uuid
import sqlalchemy as sa
import sqlalchemy.orm
# import sqlalchemy.ext.automap
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.orm import declarative_base
from sqlalchemy.dialects import postgresql as psql
from sqlalchemy.dialects.postgresql import UUID as sqlUUID
from sqlalchemy.dialects.postgresql import array_agg
import config
import exposuresource
import psycopg2.extras
import psycopg2.sql
import astropy.wcs

# Pooling connections would be nice, but if you run things with MPI you
#  can quickly generate a ginormous number of connections that just hang
#  there doing nothing.  So, don't pool, so they'll actually be
#  released.  Hopefully the database connections aren't happening so
#  often that the overhead of reconnecting each time is bad.

from sqlalchemy.pool import NullPool

# ======================================================================
# Util function

NULLUUID = uuid.UUID( '00000000-0000-0000-0000-000000000000' )

def asUUID( val, canbenone=True ):
    if val is None:
        if canbenone:
            return None
        else:
            return NULLUUID
    if isinstance( val, uuid.UUID ):
        return val
    else:
        return uuid.UUID( val )

# ======================================================================
# ROB!!! Look at scoped session

Base = declarative_base()
        
class DB(object):
    engines = {}
    sessionfacs = {}

    @classmethod
    def DBinit( cls, context, cfg ):
        if context not in cls.engines:
            database = cfg.value('database.database')
            engine = cfg.value('database.engine')
            user = cfg.value('database.username')
            try:
                passwd = cfg.value('database.password')
            except Exception:
                passwd = None
            if passwd is None:
                with open( cfg.value('database.passwordfile') ) as ifp:
                    passwd = ifp.readline().strip()
            host = cfg.value('database.host')
            port = cfg.value('database.port')
            # sys.stderr.write( f"Creating database engine {engine}, user {user}, host {host}, port {port}, "
            #                   f"database {database}\n" )
            cls.engines[context] = sa.create_engine( f'{engine}://{user}:{passwd}@{host}:{port}/{database}',
                                                     poolclass=NullPool )
            # # I don't want to use the automatically generated relationships, as those
            # #  will have long names with fkey and such in it.  I'm going to manually
            # #  configure my relationships, which is probably overdone and me being
            # #  a control freak, but whatever.
            # def no_relationships( base, direction, return_fn, attrname, local_cls, referred_cls, **kw ):
            #     return None
            # Base.prepare( cls.engine, generate_relationship=no_relationships )
            cls.sessionfacs[context] = sa.orm.sessionmaker( bind=cls.engines[context], expire_on_commit=False )

    @staticmethod
    def collection_name( base, local_cls, referred_cls, constraint ):
        return constraint.name

    @staticmethod
    def get( db=None, context=None, cfg=None ):
        if db is not None:
            return DB( db.db, None, None )
        else:
            if context is None:
                context = "__default__"
            if cfg is None:
                cfg = config.Config.get()
            return DB( None, context, cfg )

    def __init__( self, dbsession, context, cfg ):
        """Don't call this directly, call DB.get()"""
        if dbsession is None:
            if context not in DB.engines:
                DB.DBinit( context, cfg )
            self.db = DB.sessionfacs[context]()
            self.mustclose = True
        else:
            self.db = dbsession
            self.mustclose = False

    def __enter__( self ):
        return self

    def __exit__( self, exc_type, exc_val, exc_tb):
        self.close()

    def __del__( self ):
        self.close()

    def close( self ):
        if self.mustclose and self.db is not None:
            self.db.close()
            self.db = None
            self.mustclose = False
    

# ======================================================================
# A mixin for classes with an autoincrementing primary id

class HasPrimaryID(object):
    id = sa.Column(sa.Integer, sa.Identity( start=1, cycle=True ), primary_key=True )

    @classmethod
    def get( cls, id, curdb=None, cfg=None ):
        with DB.get( curdb, cfg=cfg ) as db:
            q = db.db.query(cls).filter( cls.id==id )
            if q.count() > 1:
                raise RuntimeError( f'Error, {cls.__name__} {id} multiply defined! This shouldn\'t happen!' )
            if q.count() == 0:
                return None
            return q[0]

    @classmethod
    def get_all( cls, orderby=None, curdb=None ):
        with DB.get( curdb ) as db:
            q = db.db.query(cls)
            if orderby is not None:
                q = q.orderby( orderby )
            return q.all()
            
# ======================================================================
# A mixin for tables that have ra and dec fields indexed via q3c

class SpatiallyIndexed(object):
    ra = sa.Column(psql.DOUBLE_PRECISION)
    dec = sa.Column(psql.DOUBLE_PRECISION)

    @declared_attr
    def __table_args__(cls):
        tn = cls.__tablename__
        return ( sa.Index( f"{tn}_q3c_ang2ipix_idx", sa.func.q3c_ang2ipix(cls.ra, cls.dec) ), )

# ======================================================================
# A mixin for things with created_at and modified

class CreateMod(object):
    created_at = sa.Column( sa.DateTime, nullable=False, server_default=sa.func.now() )
    # The server_default thing is *not* working, so this field is not behaving as desired
    # Just yank it for now.
    # modified = sa.Column( sa.DateTime, nullable=False, server_default=sa.func.now(), server_onupdate=sa.func.now() )

    
# ======================================================================
# Used for tagging the versions of stuff

class VersionTag(Base,HasPrimaryID):
    __tablename__ = "versiontag"
    
    name = sa.Column( sa.Text, unique=True, index=True, nullable=False )
    description = sa.Column( sa.Text )

    @classmethod
    def get_by_name( cls, name, curdb=None ):
        with DB.get(curdb) as dbo:
            q = dbo.db.query( VersionTag ).filter( VersionTag.name==name )
            if q.count() == 0:
                return None
            else:
                return q.first()

# ======================================================================
# id is not auto-updated because I want to control it

class ExposureSource(Base):
    __tablename__ = "exposuresource"

    id = sa.Column(sa.Integer, primary_key=True)
    sourcetype = sa.Column(sa.Integer)
    nx = sa.Column(sa.Integer, default=0)
    ny = sa.Column(sa.Integer, default=0)
    orientation = sa.Column(sa.Integer, default=0)
    pixscale = sa.Column(sa.REAL)
    preprocessed = sa.Column(sa.Boolean, default=False)
    skysub = sa.Column(sa.Boolean, default=False)
    name = sa.Column(sa.Text)
    secondary = sa.Column(sa.Text)
                 
    chips = sqlalchemy.orm.relationship( "CameraChip", cascade="all", back_populates="exposuresource",
                                         order_by="CameraChip.chiptag" )

    # This is a copy of HasPrimaryID.get, but we don't make
    #  this class a subclass of HasPrimaryID... I *think*
    #  because we want to be control freaks about the ids,
    #  and not have the auto incrementing thing.
    @classmethod
    def get( cls, id, curdb=None ):
        with DB.get(curdb) as db:
            q = db.db.query(cls).filter( cls.id==id )
            if q.count() > 1:
                raise RuntimeError( f"Error, ExposureSource {id} multiply defined!  This shouldn\'t happen!" )
            if q.count() == 0:
                return None
            return q[0]

    @classmethod
    def getdbinfo( cls, name, secondary=None, curdb=None ):
        with DB.get(curdb) as db:
            q = ( db.db.query(ExposureSource)
                  .filter(ExposureSource.name==name)
                  .options( sa.orm.subqueryload( ExposureSource.chips ) ) )
            if ( secondary is not None ):
                q = q.filter(ExposureSource.secondary==secondary)
            if q.count() > 1:
                raise RuntimeError( f'Error, exposure source name {name} secondary {secondary} is multiply defined!' )
            if q.count() == 0:
                return None
            return q[0]

# ======================================================================

class CameraChip(Base,HasPrimaryID):
    __tablename__ = "camerachip"

    exposuresource_id = sa.Column(sa.Integer, sa.ForeignKey("exposuresource.id", ondelete="RESTRICT"), index=True )
    raoff = sa.Column(sa.REAL)
    decoff = sa.Column(sa.REAL)
    ra0off = sa.Column(sa.REAL)
    dec0off = sa.Column(sa.REAL)
    ra1off = sa.Column(sa.REAL)
    dec1off = sa.Column(sa.REAL)
    ra2off = sa.Column(sa.REAL)
    dec2off = sa.Column(sa.REAL)
    ra3off = sa.Column(sa.REAL)
    dec3off = sa.Column(sa.REAL)
    isgood = sa.Column(sa.Boolean, default=True)
    chiptag = sa.Column(sa.Text)
    
    exposuresource = sqlalchemy.orm.relationship( "ExposureSource", back_populates="chips" )

    @classmethod
    def get_by_tag( name, exposuresource_id, chiptag, curdb=None ):
        with DB.get(curdb) as curdb:
            q = ( curdb.db.query(CameraChip)
                  .filter(CameraChip.exposuresource_id==exposuresource_id)
                  .filter(CameraChip.chiptag==chiptag) )
            them = q.all()
            if len(them) > 1:
                raise RutimeError( f'Chip {tag} for exposure source {exposuresource_id} is multiply defined! Bad.' )
            elif len(them) == 0:
                return None
            else:
                return them[0]
    
# ======================================================================

class Band(Base,HasPrimaryID):
    """
    Description of table properties:
    
    name - Unique, no spaces, human-readable version of the primary key
    filtercode - short, goes in the filename, does not have to be unique
    description - entirely for human consumption
    sortdex - so you can sort the filters in order by color
    """
    __tablename__ = "band"

    closeststd = sa.Column(sa.Integer, sa.ForeignKey("band.id", ondelete="RESTRICT"), index=True )
    exposuresource_id = sa.Column(sa.Integer, sa.ForeignKey("exposuresource.id", ondelete="RESTRICT",
                                                            name="band_exposuresource_id_fkey"), index=True)
    name = sa.Column(sa.Text, unique=True)
    filtercode = sa.Column(sa.Text)
    description = sa.Column(sa.Text)
    sortdex = sa.Column(sa.Integer, default=2147483647)
    
    standardband = sqlalchemy.orm.relationship( "Band" )

    @staticmethod
    def get_by_name( name, expsource, curdb=None ):
        """Return the Band object of a given name for a given exposure source.

        expsource — either an ExposureSource object, or the exposure source id
        name — name of the band
        """
        with DB.get(curdb) as db:
            if not isinstance( expsource, exposuresource.ExposureSource ):
                actual_expsource = ExposureSource.get( expsource )
                if actual_expsource is None:
                    raise RuntimeError( f"Failed to find ExposureSource {expsource}" )
                expsource = actual_expsource
            q = db.db.query(Band).filter( Band.name==name ).filter( Band.exposuresource_id==expsource.id )

            them = q.all()
            if len(them) > 1:
                raise RuntimeError( f'Band {name} is muiltiply defined in the database! Fix that.' )
            elif len(them) == 0:
                return None
            else:
                return them[0]
    
# ======================================================================

class Exposure(Base,HasPrimaryID,CreateMod,SpatiallyIndexed):
    __tablename__ = "exposure"

    mjd = sa.Column(psql.DOUBLE_PRECISION)
    gallat = sa.Column(sa.REAL)
    gallong = sa.Column(sa.REAL)
    exposuresource_id = sa.Column(sa.Integer, sa.ForeignKey("exposuresource.id", ondelete="RESTRICT"), index=True )
    band_id = sa.Column(sa.Integer, sa.ForeignKey("band.id", ondelete="RESTRICT"), index=True )
    basename = sa.Column(sa.Text)
    fieldname = sa.Column(sa.Text)
    collection = sa.Column(sa.Text)
    header = sa.Column(psql.JSONB)
    
    exposuresource = sqlalchemy.orm.relationship( "ExposureSource" )
    band = sqlalchemy.orm.relationship( "Band" )
    images = sqlalchemy.orm.relationship( "Image", cascade="all", back_populates="exposure" )

    @classmethod
    def get_by_basename( cls, basename, curdb=None ):
        with DB.get(curdb) as db:
            q = db.db.query(Exposure).filter( Exposure.basename==basename )
            them = q.all()
            if len(them) > 1:
                raise RuntimeError( f'Exposure {basename} is in the database more than once! This is bad.' )
            elif len(them) == 0:
                return None
            else:
                return them[0]
        
# ======================================================================

class Image(Base,HasPrimaryID,CreateMod,SpatiallyIndexed):
    __tablename__ = "image"

    exposure_id = sa.Column(sa.Integer, sa.ForeignKey("exposure.id", ondelete="RESTRICT"), index=True )
    band_id = sa.Column(sa.Integer, sa.ForeignKey("band.id", ondelete="RESTRICT"), index=True )
    chip_id = sa.Column(sa.Integer, sa.ForeignKey("camerachip.id", ondelete="RESTRICT"), index=True )
    mjd = sa.Column(psql.DOUBLE_PRECISION)
    ra1 = sa.Column(sa.REAL)
    dec1 = sa.Column(sa.REAL)
    ra2 = sa.Column(sa.REAL)
    dec2 = sa.Column(sa.REAL)
    ra3 = sa.Column(sa.REAL)
    dec3 = sa.Column(sa.REAL)
    ra4 = sa.Column(sa.REAL)
    dec4 = sa.Column(sa.REAL)
    skysig = sa.Column(sa.REAL)
    medsky = sa.Column(sa.REAL)
    seeing = sa.Column(sa.REAL)
    magzp = sa.Column(sa.REAL)
    limiting_mag = sa.Column(sa.REAL)
    isstack = sa.Column(sa.Boolean, default=False)
    hasfakes = sa.Column(sa.Boolean, default=False)
    isastrom = sa.Column(sa.Boolean, default=False)
    isphotom = sa.Column(sa.Boolean, default=False)
    isskysub = sa.Column(sa.Boolean, default=False) 
    hascat = sa.Column(sa.Boolean, default=False)    # Also implies that seeing, skysig have been *measured*
    image_is_bad = sa.Column(sa.Boolean, default=False, server_default='f', nullable=False)
    basename = sa.Column(sa.Text)
    archive_identifier = sa.Column(sa.Text)
    header = sa.Column(psql.JSONB)
    
    exposure = sqlalchemy.orm.relationship( "Exposure", back_populates="images" )
    band = sqlalchemy.orm.relationship( "Band" )

    # ROB : look at sqlalchemy Join class and making a "stackmembers" secondary

    @classmethod
    def get_by_basename( cls, basename, curdb=None ):
        with DB.get(curdb) as db:
            q = db.db.query(Image).filter( Image.basename==basename )
            them = q.all()
            if len(them) > 1:
                raise RuntimeError( f'Image {basename} is in the database more than once! This is bad.' )
            elif len(them) == 0:
                return None
            else:
                return them[0]

    @classmethod
    def get_including_point( cls, ra, dec, band=None, expsourceid=None, mjd0=None, mjd1=None,
                             includebad=False, includestacks=False, onlystacks=False, curdb=None ):
        with DB.get(curdb) as db:
            q = ( db.db.query(Image)
                  .filter( sa.func.least(Image.ra1, Image.ra2, Image.ra3, Image.ra4) < ra )
                  .filter( sa.func.greatest(Image.ra1, Image.ra2, Image.ra3, Image.ra4) > ra )
                  .filter( sa.func.least(Image.dec1, Image.dec2, Image.dec3, Image.dec4) < dec )
                  .filter( sa.func.greatest(Image.dec1, Image.dec2, Image.dec3, Image.dec4) > dec ) )
            if band is not None:
                if not isinstance( band, Band ):
                    if expsourceid is None:
                        raise RuntimeError( f'If you pass a band name, you must also pass an exposuresource id' )
                    bandobj = Band.get_by_name( band, expsourceid, curdb=db )
                    if bandobj is None:
                        raise ValueError( f'Unknown band {band} for exposure source {expsourceid}' )
                    band = bandobj
                q = q.filter( Image.band_id==band.id )
            if mjd0 is not None:
                q = q.filter( Image.mjd >= mjd0 )
            if mjd1 is not None:
                q = q.filter( Image.mjd <= mjd1 )
            if not includebad:
                q = q.filter( Image.image_is_bad == False )
            if onlystacks:
                q = q.filter( Image.isstack == True )
            elif not includestacks:
                q = q.filter( Image.isstack == False )
            images = q.all()

            if expsourceid is not None:
                newimages = []
                for image in images:
                    if ( ( image.exposure_id is not None ) and
                         ( image.exposure.exposuresource_id == expsourceid ) ):
                        newimages.append( image )
                images = newimages
            return images
        
        @classmethod
        def get_for_object(cls, obj_name, band=None, expsourceid=None, mjd0=None, 
                           mjd1=None, includebad=False, curdb=None):
            with DB.get(curdb) as db:
                obj = Object.get_by_name(obj_name)
                return get_including_point(obj.ra, obj.dec, band=band, expsourceid=expsourceid,
                                           mjd0=mjd0, mjd1=mjd1, includebad=includebad, curdb=db)
    
# ======================================================================

class WCS(Base,HasPrimaryID):
    __tablename__ = "wcs"

    image_id = sa.Column(sa.Integer, sa.ForeignKey("image.id", ondelete="CASCADE"), index=True )
    header = sa.Column(psql.JSONB)
    
    @staticmethod
    def get_for_image( image, curdb=None ):
        with DB.get(curdb) as db:
            if not isinstance( image, Image ):
                imageobj = Image.get_by_basename( image )
                if imageobj is None:
                    raise ValueError( f"Unknown image {image}" )
                image = imageobj
            q = db.db.query( WCS ).filter( WCS.image_id==image.id )
            them = q.all()
            if len(them) == 0:
                return None 
            elif len(them) > 1:
                raise RuntimeError( f'WCS for {image.basename} is multiply defined in the database!' )
            else:
                return them[0]

    @staticmethod
    def get_astropy_wcs_for_image( image, curdb=None ):
        dbwcs = WCS.get_for_image( image, curdb=curdb )
        hdrdict = { i[0]: i[1] for i in dbwcs.header }
        return astropy.wcs.WCS( hdrdict )
            
# ======================================================================

class StackMember(Base,HasPrimaryID):
    __tablename__ = "stackmember"

    image_id = sa.Column(sa.Integer, sa.ForeignKey("image.id", ondelete="CASCADE"), index=True )
    member_id = sa.Column(sa.Integer, sa.ForeignKey("image.id", ondelete="RESTRICT"), index=True )
    weight = sa.Column(psql.DOUBLE_PRECISION)
    
    # I haven't figured out how to get these to work yet....
    # image = sqlalchemy.orm.relationship( "Image", foreign_keys=[Image.id] )
    # member = sqlalchemy.orm.relationship( "Image", foreign_keys=[Image.id] )

    @staticmethod
    def get_stack_members( imageid, curdb=None ):
        with DB.get(curdb) as db:
            q = db.db.query(StackMember).filter( StackMember.image_id==imageid )
            them = q.all()
            if len(them) == 0:
                return None
            else:
                imageids = [ sm.member_id for sm in them ]
                q = db.db.query(Image).filter( Image.id.in_(imageids) )
                return q.all()
    
# ======================================================================

class ObjectClassification(Base,HasPrimaryID):
    __tablename__ = "object_classification"

    name = sa.Column(sa.Text)
    description = sa.Column(sa.Text)
    
    @staticmethod
    def get_by_name( name, curdb=None ):
        with DB.get(curdb) as db:
            q = db.db.query(ObjectClassification).filter( ObjectClassification.name==name )
            them = q.all()
            if len(them) > 1:
                raise RuntimeError( f'Classification {name} is multiply defined!  Fix this.' )
            elif len(them) == 0:
                return None
            else:
                return them[0]
    
# ======================================================================

class Object(Base,HasPrimaryID,CreateMod,SpatiallyIndexed):
    __tablename__ = "object"

    t0 = sa.Column(psql.DOUBLE_PRECISION)
    z = sa.Column(psql.DOUBLE_PRECISION, default=-9999. )
    classification_id = sa.Column(sa.Integer, sa.ForeignKey("object_classification.id", ondelete="RESTRICT"),
                                  index=True )
    confidence = sa.Column(sa.REAL, default=0. )
    ignore = sa.Column(sa.Boolean, default=False)
    name = sa.Column(sa.Text)
    othernames = sa.Column(sa.Text)
    
    classification = sqlalchemy.orm.relationship( "ObjectClassification" )
    # I haven't yet figured out how to get this next one to work
    # references = sqlalchemy.orm.relationship( "Image", uselist=True, secondary="ObjectReference" )
    
    @staticmethod
    def get_by_name( name, curdb=None ):
        with DB.get(curdb) as db:
            q = db.db.query(Object).filter( Object.name==name )
            them = q.all()
            if len(them) > 1:
                raise RuntimeError( f'Object {name} is multiply defined in the database!  Fix that.' )
            elif len(them) == 0:
                return None
            else:
                return them[0]

    @staticmethod
    def get_by_pos( ra, dec, rad=3.0, curdb=None ):
        with DB.get(curdb) as db:
            q = db.db.query(Object).filter( sa.func.q3c_radial_query( Object.ra, Object.dec, ra, dec, rad/3600. ) )
            return q.all()
    
    @staticmethod
    def get_for_image(image, curdb=None):
        with DB.get(curdb) as db:
            if isinstance(image, str):
                image = Image.get_by_basename(image, curdb=db)
            q = (db.db.query(Object)
                 .filter(sa.func.greatest( image.ra1,image.ra2,image.ra3,image.ra4 ) > Object.ra)
                 .filter(sa.func.least(image.ra1,image.ra2,image.ra3,image.ra4 ) < Object.ra)
                 .filter(sa.func.greatest(image.dec1,image.dec2,image.dec3,image.dec4 ) > Object.dec)
                 .filter(sa.func.least(image.dec1,image.dec2,image.dec3,image.dec4 ) < Object.dec )
                )
            return q.all()
            
# ======================================================================

class OldObjectData(Base,HasPrimaryID,CreateMod,SpatiallyIndexed):
    __tablename__ = "oldobjectdata"

    object_id = sa.Column( sa.Integer, sa.ForeignKey( "object.id", ondelete="CASCADE" ), index=True, nullable=True )
    t0 = sa.Column(psql.DOUBLE_PRECISION)
    z = sa.Column(psql.DOUBLE_PRECISION, default=-9999. )
    classification_id = sa.Column(sa.Integer, sa.ForeignKey("object_classification.id", ondelete="RESTRICT"),
                                  index=True )
    confidence = sa.Column(sa.REAL, default=0. )
    othernames = sa.Column(sa.Text)

    object = sqlalchemy.orm.relationship( "Object" )
    classification = sqlalchemy.orm.relationship( "ObjectClassification" )
    
# ======================================================================

class OldObjectDataVersiontag( Base, HasPrimaryID ):
    __tablename__ = 'oldobjectdata_versiontag'
    oldobjectdata_id = sa.Column( sa.Integer, sa.ForeignKey( "oldobjectdata.id", ondelete="CASCADE" ), index=True )
    versiontag_id = sa.Column( sa.Integer, sa.ForeignKey( "versiontag.id", ondelete="CASCADE" ), index=True )

# ======================================================================

class ObjectComment(Base,HasPrimaryID,CreateMod):
    __tablename__ = "object_comment"

    object_id = sa.Column(sa.Integer, sa.ForeignKey("object.id", ondelete="CASCADE"), index=True )
    commentor = sa.Column(sqlUUID(as_uuid=True), sa.ForeignKey("authuser.id", ondelete="SET NULL"), index=True )
    comment = sa.Column(sa.Text)
    
    object = sqlalchemy.orm.relationship( "Object" )

# ======================================================================

class ObjectReference(Base,HasPrimaryID,SpatiallyIndexed) :
    __tablename__ = "object_reference"

    object_id = sa.Column(sa.Integer, sa.ForeignKey("object.id", ondelete="CASCADE"), index=True, nullable=True )
    image_id = sa.Column(sa.Integer, sa.ForeignKey("image.id", ondelete="CASCADE"), index=True )
    donotuse = sa.Column(sa.Boolean, default=False )
    notes = sa.Column(sa.Text)
    
    object = sqlalchemy.orm.relationship( "Object" )
    image = sqlalchemy.orm.relationship( "Image" )

    @staticmethod
    def get_for_obj( obj, curdb=None ):
        """Return ObjectReference objects for a given object.

        obj - a db.Object object
        curdb (optional) - a db.DB object

        """
        with DB.get(curdb) as db:
            q = ( db.db.query(ObjectReference)
                  .filter( ObjectReference.object_id==obj.id )
                  .filter( sa.not_( ObjectReference.donotuse ) ) )
            return list(q.all())
    
    @staticmethod
    def get_for_obj_and_band( obj, band, curdb=None ):
        """Return ObjectReference objects for a given object and band.

        obj - a db.Object object
        band - a db.Band object
        curdb (optional) - a db.DB object

        """
        with DB.get(curdb) as db:
            allrefs = ObjectReference.get_for_obj( obj, curdb=db )
            refs = []
            for ref in allrefs:
                if ref.image.band_id == band.id:
                    refs.append( ref )
            return refs
    
    @staticmethod
    def get_for_image(image, curdb=None):
        # Note: may need to revise this if we have multi-object images
        with DB.get(curdb) as db:
            if isinstance(image, str):
                image = Image.get_by_basename(image, curdb=db)
            objs = Object.get_for_image(image, curdb=db)
            if len(objs)==0: raise ValueError("No objects match this image")
            refs = []
            for obj in objs:
                refs+=ObjectReference.get_for_obj_and_band( obj, image.band, curdb=db)
            return refs
        
    
# ======================================================================

class SubAlgorithm(Base,HasPrimaryID):
    __tablename__ = "sub_algorithm"
  
    name = sa.Column(sa.Text, unique=True, index=True, nullable=False)
    description = sa.Column(sa.Text)
    # params = sa.Column(psql.JSONB)
    
# ======================================================================

class Subtraction(Base,HasPrimaryID,CreateMod):
    __tablename__ = "subtraction"

    image_id = sa.Column(sa.Integer, sa.ForeignKey("image.id", ondelete="CASCADE"), index=True )
    ref_id = sa.Column(sa.Integer, sa.ForeignKey("image.id", ondelete="CASCADE"), index=True )
    subalg_id = sa.Column(sa.Integer, sa.ForeignKey("sub_algorithm.id", ondelete="CASCADE"), index=True )
    refnorm = sa.Column(sa.REAL, comment="Multiply ref_fits by this to scale to new_fits")
    iscomplete = sa.Column(sa.Boolean, server_default='f', nullable=False)
    
    image = sqlalchemy.orm.relationship( "Image", foreign_keys=[ image_id ] )
    ref = sqlalchemy.orm.relationship( "Image", foreign_keys=[ ref_id ] )

    versiontag = sqlalchemy.orm.relationship( "VersionTag", secondary="subtraction_versiontag" )

class SubtractionVersiontag(Base,HasPrimaryID):
    __tablename__ = 'subtraction_versiontag'
    subtraction_id = sa.Column( sa.Integer, sa.ForeignKey( "subtraction.id", ondelete="CASCADE" ), index=True )
    versiontag_id = sa.Column( sa.Integer, sa.ForeignKey( "versiontag.id", ondelete="CASCADE" ), index=True )
    
# ======================================================================

class PhotMethod(Base,HasPrimaryID):
    __tablename__ = "photmethod"
    
    name = sa.Column( sa.Text, unique=True )
    description = sa.Column( sa.Text )
    isaper = sa.Column( sa.Boolean, default=False )
    ispsffit = sa.Column( sa.Boolean, default=False )
    isscenemodel = sa.Column( sa.Boolean, default=False )

# ======================================================================

class Photometry(Base,HasPrimaryID,CreateMod):
    __tablename__ = "photometry"

    mjd = sa.Column(psql.DOUBLE_PRECISION, index=True)
    flux = sa.Column(sa.REAL, comment="Full psf flux with zeropoint magzp")
    dflux = sa.Column(sa.REAL)
    magzp = sa.Column(sa.REAL, comment="Zeropoint for ADU")
    mag = sa.Column(sa.REAL, comment="Should be redundant with flux")
    dmag = sa.Column(sa.REAL, comment="Not including zp uncertainty?")
    refnorm = sa.Column(sa.REAL, comment="Multiply ref_fits by this to scale to new_fits")
    imagex = sa.Column(sa.REAL, comment="Position on image of measurement")
    imagey = sa.Column(sa.REAL, comment="Position on image of measurement")
    photmethod_id = sa.Column(sa.Integer, sa.ForeignKey("photmethod.id", ondelete="CASCADE"), index=True )
    object_id = sa.Column(sa.Integer, sa.ForeignKey("object.id", ondelete="CASCADE"), index=True )
    subtraction_id = sa.Column(sa.Integer, sa.ForeignKey("subtraction.id", ondelete="CASCADE"), index=True,
                               nullable=True,
                               comment="One of image_id or subtraction_id must be NULL")
    image_id = sa.Column(sa.Integer, sa.ForeignKey("image.id", ondelete="CASCADE"), index=True, nullable=True,
                         comment="One of image_id or subtraction_id must be NULL")
    new_fits = sa.Column(psql.BYTEA)
    ref_fits = sa.Column(psql.BYTEA)
    sub_fits = sa.Column(psql.BYTEA)
    new_jpeg = sa.Column(psql.BYTEA)
    ref_jpeg = sa.Column(psql.BYTEA)
    sub_jpeg = sa.Column(psql.BYTEA)

    is_bad = sa.Column( sa.Boolean, default=False, server_default='f' )
    
    photmethod = sqlalchemy.orm.relationship( "PhotMethod" )
    object = sqlalchemy.orm.relationship( "Object" )
    subtraction = sqlalchemy.orm.relationship( "Subtraction" )
    image = sqlalchemy.orm.relationship( "Image" )
    versiontag = sqlalchemy.orm.relationship( "VersionTag", secondary="photometry_versiontag" )
    
    @staticmethod
    def get_for_obj_and_image_and_ref( objid, imageid, refid, includebad=False, curdb=None ):
        """Only works for photometry tied to a subtraction"""
        with DB.get(curdb) as db:
            q = ( db.db.query(Photometry)
                  .filter( Photometry.object_id==objid )
                  .filter( Photometry.subtraction_id==Subtraction.id )
                  .filter( Subtraction.image_id==imageid )
                  .filter( Subtraction.ref_id==refid ) )
            if not includebad:
                q = q.filter( Photometry.is_bad==False )
            them = q.all()
            return them

    @staticmethod
    def get_for_obj( obj, version='default', includebad=False, curdb=None ):
        """Returns a messy structure of phometry

        Returns a list.  Each element of the list is (band, photometry).
        band is a Band object
        photometry is a list of Photometry objects
        """
        # ****
        # sys.stderr.write( f"Photometry.get_for_obj, version={version}\n" )
        # ****
        with DB.get(curdb) as db:
            if not isinstance( obj, Object ):
                objobj = Object.get_by_name( obj, curdb=db )
                if objobj is None:
                    raise ValueError( f'Unknown object {obj}' )
                obj = objobj

            SubImage = sqlalchemy.orm.aliased( Image )
            q = ( db.db.query( Band, Photometry )
                  .join( PhotometryVersiontag, PhotometryVersiontag.photometry_id==Photometry.id )
                  .join( VersionTag, sa.and_( PhotometryVersiontag.versiontag_id==VersionTag.id,
                                              VersionTag.name==version ) )
                  .outerjoin( Subtraction, Photometry.subtraction_id==Subtraction.id )
                  .outerjoin( SubImage, Subtraction.image_id==SubImage.id )
                  .outerjoin( Image, Photometry.image_id==Image.id ) )
            if not includebad:
                q = q.filter( Photometry.is_bad==False )
            q = ( q.join( Band, sa.or_( SubImage.band_id==Band.id, Image.band_id==Band.id ) )
                  .filter( Photometry.object_id==obj.id )
                  .order_by( Band.sortdex, Photometry.mjd )
                 )
            # ****
            # eng = curdb.db.get_bind()
            # sys.stderr.write( f"Query: {q.statement.compile(eng)}" )
            # ****
            rows = q.all()
            if len(rows) == 0:
                return []
            curband = rows[0][0]
            curphot = []
            results = []
            for row in rows:
                band = row[0]
                phot = row[1]
                if band.id != curband.id:
                    if len(curphot) > 0:
                        results.append( ( curband, curphot ) )
                    curband = band
                    curphot = []
                curphot.append( phot )
            if len(curphot) > 0:
                results.append( ( curband, curphot ) )
            return results
            
    @staticmethod
    def get_for_obj_and_band( obj, band, expsourceid=None, version='default', includebad=False, curdb=None ):
        with DB.get(curdb) as db:
            if not isinstance( band, Band ):
                if expsourceid is None:
                    raise RuntimeError( "If you pass a band name, you must also pass an exposure source id" )
                bandobj = Band.get_by_name( band, expsourceid, curdb=db )
                if bandobj is None:
                    raise ValueError( f'Unknown band {band}' )
                band = bandobj
            if not isinstance( obj, Object ):
                objobj = Object.get_by_name( obj, curdb=db )
                if objobj is None:
                    raise ValueError( f'Unknown object {obj}' )
                obj = objobj
            SubImage = sqlalchemy.orm.aliased( Image )
            q = ( db.db.query( Photometry )
                  .join( PhotometryVersiontag, PhotometryVersiontag.photometry_id==Photometry.id )
                  .join( VersionTag, sa.and_( PhotometryVersiontag.versiontag_id==VersionTag.id,
                                              VersionTag.name==version ) )
                  .outerjoin( Subtraction, Photometry.subtraction_id==Subtraction.id )
                  .outerjoin( SubImage, Subtraction.image_id==SubImage.id )
                  .outerjoin( Image, Photometry.image_id==Image.id )
                  .join( Band, sa.or_( SubImage.band_id==Band.id, Image.band_id==Band.id ) )
                  .filter( Photometry.object_id==obj.id )
                  .filter( Band.id==band.id )
                 )
            if not includebad:
                q = q.filter( Photometry.is_bad==False )
            q = q.order_by( Photometry.mjd )
            return q.all()

    @staticmethod
    def objs_and_filter_names_with_phot( version='default', curdb=None ):
        """Return a list of tuples; first element of tuple is an Object, second is an array of band names"""
        with DB.get(curdb) as db:
            SubImage = sqlalchemy.orm.aliased( Image )
            q = ( db.db.query( Object, Band )
                  .join( Photometry, Photometry.object_id==Object.id )
                  .join( PhotometryVersiontag, PhotometryVersiontag.photometry_id==Photometry.id )
                  .join( VersionTag, sa.and_( PhotometryVersiontag.versiontag_id==VersionTag.id,
                                              VersionTag.name==version ) )
                  .outerjoin( Subtraction, Photometry.subtraction_id==Subtraction.id )
                  .outerjoin( SubImage, Subtraction.image_id==SubImage.id )
                  .outerjoin( Image, Photometry.image_id==Image.id )
                  .join( Band, sa.or_( SubImage.band_id==Band.id, Image.band_id==Band.id ) )
                  .distinct( Object.id, Band.id )
                 )
            rows = q.all()
            if len(rows) == 0:
                  return []
            curobj = rows[0][0]
            results = []
            curbands = []
            for row in rows:
                if row[0].name != curobj.name:
                    if len(curbands) > 0:
                        curbands.sort( key=lambda x: x.sortdex )
                        results.append( ( curobj, [ c.name for c in curbands ] ) )
                        curobj = row[0]
                        curbands = []
                curbands.append( row[1] )
            if len(curbands) > 0:
                curbands.sort( key=lambda x: x.sortdex )
                results.append( ( curobj, [ c.name for c in curbands ] ) )
            results.sort( key=lambda x: x[0].name )
            return results
            
            # # ROB, WARNING, I may be relying on deprecated
            # # SQLAlchemy behavior here.
            # subq = ( db.db.query(  Object.id.label('object'),
            #                        Band.name.label('bandname') )
            #          .filter( ApPhot.object_id==Object.id )
            #          .filter( ApPhot.subtraction_id==Subtraction.id )
            #          .filter( Subtraction.image_id==Image.id )
            #          .filter( Image.band_id==Band.id )
            #          .distinct( ApPhot.object_id, Band.id )
            #          .group_by( Object, ApPhot.object_id, Band.id )
            #          .subquery( ) )
            # q = ( db.db.query( Object, sa.func.array_agg(subq.c.bandname) )
            #       .filter( Object.id==subq.c.object ).group_by( Object ).order_by( Object.name ) )
            # return q.all()

    @staticmethod
    def bands_with_phot_for_obj( obj, version='default', curdb=None ):
        with DB.get(curdb) as db:
            if not isinstance( obj, Object ):
                objobj = Object.get_by_name( obj, curdb=db )
                if objobj is None:
                    raise ValueError( f'Unknown object {obj}' )
                obj = objobj
            SubImage = sqlalchemy.orm.aliased( Image )
            q = ( db.db.query( Band )
                  .join( Photometry, Photometry.object_id==obj.id )
                  .join( PhotometryVersiontag, PhotometryVersiontag.photometry_id==Photometry.id )
                  .join( VersionTag, sa.and_( PhotometryVersiontag.versiontag_id==VersionTag.id,
                                              VersionTag.name==version ) )
                  .outerjoin( Subtraction, Photometry.subtraction_id==Subtraction.id )
                  .outerjoin( SubImage, Subtraction.image_id==SubImage.id )
                  .outerjoin( Image, Photometry.image_id==Image.id )
                  .filter( sa.or_( SubImage.band_id==Band.id, Image.band_id==Band.id ) )
                  .group_by( Band )
                  .order_by( Band.sortdex )
                  )
            return list( q.all() )
            
    @staticmethod
    def filter_names_with_phot_for_obj( obj, version='default', curdb=None ):
        """Return a list of band names for which there is aperture photometry for this object."""
        bands = Photometry.bands_with_phot_for_obj( obj, version=version, curdb=curdb )
        return [ b.name for b in bands ]


class PhotometryVersiontag(Base,HasPrimaryID):
    __tablename__ = "photometry_versiontag"
    photometry_id = sa.Column( sa.Integer, sa.ForeignKey("photometry.id", ondelete="CASCADE"), index=True )
    versiontag_id = sa.Column( sa.Integer, sa.ForeignKey("versiontag.id", ondelete="CASCADE"), index=True )
    
# ======================================================================

class ApPhotData(Base,HasPrimaryID):
    __tablename__ = "photometry_apphot_data"

    photometry_id = sa.Column( sa.Integer, sa.ForeignKey("photometry.id", ondelete="CASCADE"), index=True )
    aperrad = sa.Column(sa.REAL)
    apercor = sa.Column(sa.REAL)

    photometry = sqlalchemy.orm.relationship( "Photometry" )

class PSFPhotData(Base,HasPrimaryID):
    __tablename__ = "photometry_psfphot_data"
    
    photometry_id = sa.Column( sa.Integer, sa.ForeignKey("photometry.id", ondelete="CASCADE"), index=True )
    cov_matrix = sa.Column(sa.JSON)

    photometry = sqlalchemy.orm.relationship( "Photometry" )



# ======================================================================

class Salt2Fit(Base,HasPrimaryID,CreateMod) :
    __tablename__ = "salt2fit"

    mwebv = sa.Column(psql.DOUBLE_PRECISION)
    mbstar = sa.Column(psql.DOUBLE_PRECISION)
    dmbstar = sa.Column(psql.DOUBLE_PRECISION)
    z = sa.Column(psql.DOUBLE_PRECISION)
    dz = sa.Column(psql.DOUBLE_PRECISION)
    t0 = sa.Column(psql.DOUBLE_PRECISION)
    dt0 = sa.Column(psql.DOUBLE_PRECISION)
    x0 = sa.Column(psql.DOUBLE_PRECISION)
    dx0 = sa.Column(psql.DOUBLE_PRECISION)
    x1 = sa.Column(psql.DOUBLE_PRECISION)
    dx1 = sa.Column(psql.DOUBLE_PRECISION)
    c = sa.Column(psql.DOUBLE_PRECISION)
    dc = sa.Column(psql.DOUBLE_PRECISION)
    object_id = sa.Column(sa.Integer, sa.ForeignKey("object.id", ondelete="CASCADE"), index=True )
    chisq = sa.Column(sa.REAL)
    dof = sa.Column(sa.Integer)
    zfixed = sa.Column(sa.Boolean, default=True )
    bands = sa.Column(sa.ARRAY(sa.Integer))
    cov = sa.Column(sa.ARRAY(psql.DOUBLE_PRECISION))
    
    object = sqlalchemy.orm.relationship( "Object" )

    @staticmethod
    def get_for_obj( obj, version="default", curdb=None ):
        """Returns a list of Salt2Fit objects for the current object.

        obj: Object object, or object name
        version: either a list or a string; version tags to pull fits for, or None to get all fits
        curdb: a db.dB object

        Returns: a list of db.Salt2Fit obejcts, or None if none were found.
        """
        
        with DB.get(curdb) as db:
            if not isinstance( obj, Object ):
                objobj = Object.get_by_name( obj, curdb=db )
                if objobj is None:
                    raise ValueError( f'Unknown object {obj}' )
                obj = objobj
            q = db.db.query( Salt2Fit )
            if version is not None:
                q = q.join( Salt2FitVersiontag, Salt2FitVersiontag.salt2fit_id==Salt2Fit.id )
                if isinstance( version, list ):
                    q = q.join( VersionTag, sa.and_( Salt2FitVersiontag.versiontag_id==VersionTag.id,
                                                     VersionTag.name.in_(version) ) )
                else:
                    q = q.join( VersionTag, sa.and_( Salt2FitVersiontag.versiontag_id==VersionTag.id,
                                                     VersionTag.name==version ) )
            q = q.filter( Salt2Fit.object_id==obj.id )
            if ( q.count() == 0 ):
                return None
            return list( q.all() )

# ======================================================================

class Salt2FitVersiontag( Base, HasPrimaryID ):
    __tablename__ = "salt2fit_versiontag"
    salt2fit_id = sa.Column( sa.Integer, sa.ForeignKey( "salt2fit.id", ondelete="CASCADE" ), index=True )
    versiontag_id = sa.Column( sa.Integer, sa.ForeignKey( "versiontag.id", ondelete="CASCADE" ), index=True )

    @classmethod
    def get_tagnames_for_fit( cls, salt2fitid, curdb=None, cfg=None ):
        with DB.get(curdb, cfg=cfg) as curdb:
            vts = ( curdb.db.query( VersionTag )
                    .join( Salt2FitVersiontag, Salt2FitVersiontag.versiontag_id==VersionTag.id )
                    .filter( Salt2FitVersiontag.salt2fit_id==salt2fitid ) ).all()
            return [ vt.name for vt in vts ]
            
# ======================================================================
# ======================================================================
# Classes for rkauth

class PrimaryUUID(object):
    id = sa.Column( sqlUUID(as_uuid=True), primary_key=True, default=uuid.uuid4 )

    @classmethod
    def get( cls, id, curdb=None, cfg=None ):
        id = id if isinstance( id, uuid.UUID) else uuid.UUID( id )
        with DB.get(curdb, cfg=cfg ) as db:
            q = db.db.query(cls).filter( cls.id==id )
            if q.count() > 1:
                raise ErrorMsg( f'Error, {cls.__name__} {id} multiply defined!  This shouldn\'t happen.' )
            if q.count() == 0:
                return None
            return q[0]

class User(Base,PrimaryUUID):
    __tablename__ = "authuser"

    displayname = sa.Column(sa.Text, nullable=False)
    email = sa.Column(sa.Text, nullable=False)
    username = sa.Column(sa.Text, nullable=False, index=True)
    pubkey = sa.Column(sa.Text)
    privkey = sa.Column(sa.Text)
    lastlogin = sa.Column(sa.DateTime, default=None)
    
    @classmethod
    def getbyusername( cls, name, curdb=None, cfg=None ):
        # sys.stderr.write( f"in getbyusername, cfg={cfg._data}\n" )
        with DB.get(curdb, cfg=cfg) as db:
            # sys.stderr.write( f"in getbyusername, looking for {name}\n" )
            q = db.db.query(User).filter( User.username==name )
            return q.all()

    @classmethod
    def getbyemail( cls, email, curdb=None, cfg=None ):
        with DB.get(curdb, cfg=cfg ) as db:
            q = db.db.query(User).filter( User.email==email )
            return q.all()
    

class PasswordLink(Base,PrimaryUUID):
    __tablename__ = "passwordlink"
    userid = sa.Column( sqlUUID(as_uuid=True), sa.ForeignKey("authuser.id", ondelete="CASCADE"), index=True )
    expires = sa.Column(sa.DateTime)
    
    @classmethod
    def new( cls, userid, expires=None, curdb=None ):
        if expires is None:
            expires = datetime.now(pytz.utc) + timedelta(hours=1)
        else:
            expires = asDateTime( expires )
        with DB.get(curdb) as db:
            link = PasswordLink( userid = asUUID(userid),
                                 expires = expires )
            db.db.add( link )
            db.db.commit()
            return link

# ======================================================================

def alembic_upgrade_head( really_do=False, really_really_do=False, no_really_i_mean_it=False ):
    if not ( really_do and really_really_do and no_really_i_mean_it ):
        raise RuntimeError( "You don't really want to run alembic_upgrade_head!" )

    origdir = os.getcwd()
    os.chdir( '/curveball' )
    cfg = config.Config.get()
    # There must be a way to call this as a python function call
    subprocess.run( [ '/usr/local/bin/alembic', 'upgrade', 'head' ],
                    env={ 'CURVEBALL_CONFIG': cfg.configenv(),
                          'PYTHONPATH': os.getenv('PYTHONPATH') } )
    os.chdir( origdir )

# ======================================================================

def scorched_earth( really_do=False, really_really_do=False, no_really_i_mean_it=False ):
    if not ( really_do and really_really_do and no_really_i_mean_it ):
        raise RuntimeError( "You don't really want to run scorched_earth!" )

    with DB.get() as dbo:
        con = dbo.db.connection().engine.raw_connection()
        cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
        cursor.execute( "SELECT * FROM information_schema.tables WHERE table_schema='public'" )
        rows = cursor.fetchall()
        for row in rows:
            cursor.execute( psycopg2.sql.SQL( "DROP TABLE {table} CASCADE" )
                            .format( table=psycopg2.sql.Identifier( row['table_name'] ) ) )
        cursor.close()
        con.commit()
        
# ======================================================================

def main():
    pass

if __name__ == "__main__":
    main()
