raise RuntimeError( "Deprecated." )

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import math
import logging
import argparse
import numpy
import numpy.ma

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot

from astropy.io import fits
from photutils.aperture import CircularAperture, aperture_photometry
from photutils.background import SExtractorBackground

def apercor( image, mask, weight, cat, aperrad, infrad=[2,3,4,5],
             x0=None, y0=None, xwid=400, ywid=400, tiny=1e-10, bgsub=True,
             outplot=None, whichinfrad=None,
             logger=logging.getLogger("main") ):
    """Determines the aperture correction for an aperture of radius apperrad pixels.

    image : image file
    mask : mask file
    weight : weight (1/variance) file
    cat : Sextractor catalog file
    aperrad : radius of aperture in pixels
    infrad : multipliers of aperrad to consider infinite radius
    tiny : replace weight<=0 values with this to avoid dividing by 0 and sqrt(<0)
    bgsub : if true, we need to background subtract

    x0, y0, xwid, ywid : if (x0,y0) are non-None, only include stars that are within
                         xwid of x0 and ywid of y0.  THESE ARE FITS COORDINATES
                         (i.e. what's in the cat file, with 1-offset positions).

    Returns an array of the magnitude addition to make corresponding to the elements of infrad.
    """

    with fits.open( image ) as imhdu, fits.open( mask ) as mhdu, fits.open( weight ) as whdu:
        imhdr = imhdu[0].header
        imdata = imhdu[0].data
        maskhdr = mhdu[0].header
        maskdata = mhdu[0].data
        weighthdr = whdu[0].header
        weightdata = whdu[0].data
    with fits.open( cat ) as cathdu:
        catdata = cathdu[2].data

    goodcat = catdata[ catdata['IMAFLAGS_ISO'] == 0 ]
    stars = goodcat[ goodcat['CLASS_STAR'] > 0.95 ]
    logger.debug( f'{len(stars)} of {len(catdata)} objects are considered stars' )

    # Limit to the region of the image we care about
    if ( x0 is not None ) and ( y0 is not None):
        stars = stars[ ( numpy.fabs(stars["X_IMAGE"]-int(x0)) <= xwid ) &
                       ( numpy.fabs(stars["Y_IMAGE"]-int(y0)) <= ywid ) ]
        logger.debug( f'{len(stars)} are at {int(x0)}+-{xwid}, {int(y0)}+-{ywid}' )
    
    # Throw out stars that have another detected source within 2*max(infrad)*aperrad
    bigrad = max(infrad)*aperrad
    starmindist = numpy.zeros( stars["X_IMAGE"].shape )
    for i in range(len(stars)):
        x0 = stars["X_IMAGE"][i]
        y0 = stars["Y_IMAGE"][i]
        dist2 = ( catdata["X_IMAGE"] - x0 )**2 + ( catdata["Y_IMAGE"] - y0 )**2
        # Omit the star itself!
        dist2 = dist2[ dist2>0. ]
        starmindist[i] = math.sqrt( dist2.min() )
    stars = stars[ starmindist > 2*bigrad ]
    logger.debug( f'{len(stars)} left after throwing out things with nearby sources' )

    if len(stars) < 5:
        raise RuntimeError( "Less than 5 stars left!" )
    
    weightdata[ weightdata <= 0 ] = tiny
    noisedata = numpy.sqrt( 1. / weightdata )
    maskedimage = numpy.ma.MaskedArray( imdata, maskdata )
    if bgsub:
        logger.info( f'apercor subtracting background' )
        backgrounder = SExtractorBackground()
        bgdata = backgrounder.calc_background( maskedimage )
        maskedimage -= bgdata
    
    # Do photometry
    # ROB, MAKE SURE THESE POSITIONS ARE RIGHT
    # (Photutils doc say that what they call x and y is data[y, x], which is FITS order.
    #  but, they are 0-indexed, so I need to subtract 1 to convert FITS)
    starpos = numpy.empty( ( len(stars), 2 ) )
    starpos[:, 0] = stars["X_IMAGE"] - 1
    starpos[:, 1] = stars["Y_IMAGE"] - 1
    rads = [ aperrad ]
    rads.extend( [ aperrad * i for i in infrad ] )
    apphot = numpy.empty( ( len(stars), len(rads) ) )
    dapphot = numpy.empty( ( len(stars), len(rads) ) )
    for i in range(len(rads)):
        aps = CircularAperture( starpos, r=rads[i] )
        logger.info( f'apercor doing photometry in radius {rads[i]:5.2f} on {os.path.basename(image)}' )
        apresult = aperture_photometry( maskedimage, aps, error=noisedata, mask=maskdata )
        apphot[:, i] = apresult[:]['aperture_sum']
        dapphot[:, i] = apresult[:]['aperture_sum_err']

    apercor = apphot[ :, 1: ] / apphot[ :, 0:1 ]
    apercorvar = numpy.square(apercor) * ( numpy.square( dapphot[:, 1:] / apphot[:, 1:] ) +
                                           numpy.square( dapphot[:, 0:1] / apphot[:, 0:1] ) )
    w = ( numpy.isnan( apercor ) | ( numpy.isnan( apercorvar ) ) | ( apercorvar <= 0 ) )
    if w.sum() > 0:
        logger.debug( f'Threw out {w.sum()} NaN/0-error measurements' )
    # Probably I should use a numpy masked array, but this will have the right effect
    apercor[w] = 0.
    apercorvar[w] = 1e32
    avgapercor = (apercor/apercorvar).sum(axis=0) / (1/apercorvar).sum(axis=0)
    davgapercor = numpy.sqrt( 1 / (1/apercorvar).sum(axis=0) )

    magapercor = -2.5 * numpy.log10( avgapercor )
    dmagapercor = -2.5 / math.log(10.) * davgapercor / avgapercor
    
    if outplot is not None:
        if whichinfrad is None:
            whichinfrad = len(infrad) - 1
        fig = pyplot.figure( figsize=(6,8), tight_layout=True )
        ax = fig.add_subplot( 2, 1, 1 )
        ax.set_xlabel( "flux" )
        ax.set_ylabel( "apercor (flux)" )
        ax.set_ylim( avgapercor[whichinfrad]-0.5, avgapercor[whichinfrad]+0.5 )
        ax.set_xscale( "log" )
        ax.errorbar( apphot[:, whichinfrad+1], apercor[:, whichinfrad],
                     yerr=numpy.sqrt(apercorvar[:, -1]),
                     marker='o', linestyle="", color="blue" )
        ax.plot( ax.get_xlim(), [ avgapercor[whichinfrad], avgapercor[whichinfrad] ], color="red"  )
        ax = fig.add_subplot( 2, 1, 2 )
        ax.set_xlabel( "radius" )
        ax.set_ylabel( "apercor (mag)" )
        ax.plot( numpy.array(infrad)*aperrad, magapercor, marker="o", linestyle="-" )
        fig.savefig( outplot )

    return magapercor
        
def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    parser = argparse.ArgumentParser( "Compute an aperture coorrection on an image by measuring point sources" )
    parser.add_argument( "image", help="Image filename" )
    parser.add_argument( "aperrad", type=float, help="Radius of apertrure in pixels" )
    parser.add_argument( "-m", "--mask", default=None, help="Mask filename (def: generated from image)" )
    parser.add_argument( "-w", "--weight", default=None, help="Weight filename (def: generated from image)" )
    parser.add_argument( "-c", "--cat", default=None, help="Catalog filename (def: generated from image)" )
    parser.add_argument( "-x", "--x0", default=None, type=float,
                         help="x-Position to center measurements (default: use whole image)" )
    parser.add_argument( "-y", "--y0", default=None, type=float,
                         help="y-Position to center measurements (default: use whole image)" )
    parser.add_argument( "--xwid", default=400, type=int,
                         help=( "Half-width (in-pixels) of region to consider around x0 "
                                "(ignored if x0 is not set; default 400)" ) )
    parser.add_argument( "--ywid", default=400, type=int,
                         help=( "Half-height (in-pixels) of region to consider around x0 "
                                "(ignored if x0 is not set; default 400)" ) )
    parser.add_argument( "-o", "--outplot", default=None,
                         help="Output plot filename (default: don't write any plots)" )
    parser.add_argument( "-i", "--infrad", nargs="*", type=float, default=[2,3,4,5],
                         help=( "List of \"infinite\" radii to consider for correction, as a factor "
                                "of aperrad (default: 2 3 4 5)" ) )
    parser.add_argument( "-v", "--verbose", default=False, action="store_true", help="Show debugging information" )
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel( logging.DEBUG )
    
    if args.mask is None:
        args.mask = args.image.replace( ".fits", ".mask.fits" )
    if args.weight is None:
        args.weight = args.image.replace( ".fits", ".weight.fits" )
    if args.cat is None:
        args.cat = args.image.replace( ".fits", ".cat" )

    res = apercor( args.image, args.mask, args.weight, args.cat, args.aperrad,
                   infrad=args.infrad, x0=args.x0, y0=args.y0, xwid=args.xwid, ywid=args.ywid,
                   outplot=args.outplot, logger=logger )
    for rad, cor in zip( args.infrad, res ):
        print( f'Correction ∞={rad:4.2f} : {cor:5.2f}' )


# ======================================================================

if __name__ == "__main__":
    main()
