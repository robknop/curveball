import sys
import os
import io
import re
import logging
import argparse

from exposuresource import ExposureSource
import db
import config

_logger = logging.getLogger(__name__)
if not _logger.hasHandlers():
    _logout = logging.StreamHandler( sys.stderr )
    _logger.addHandler( _logout )
    _formatter = logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s',
                                    datefmt='%Y-%m-%d %H:%M:%S' )
    _logout.setFormatter( _formatter )
    _logger.setLevel( logging.INFO )

def main():
    config.Config.init( None, logger=_logger )

    parser = argparse.ArgumentParser( 'list_exposures',
                                      description="List defined exposure sources on the command line",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( "-c", "--chip-detail", action="store_true",
                         help="Show information on all chips" )
    parser.add_argument( "expsources", nargs='*', default=[],
                         help=( "List the exposure sources (as primary/secondary) you want to show; "
                                "by default, shows them all." ) )
    args = parser.parse_args()

    slasher = re.compile( '^(.*)/(.*)$' )
    if len( args.expsources ) > 0:
        for es in args.expsources:
            match = slasher.search( es )
            if not match:
                expsources.append( ExposureSource.get( es ) )
            else:
                expsources.append( ExpsoureSource.get( match.group(1), match.group(2) ) )
    else:
        expsources = ExposureSource.get_all()

    for es in expsources:
        print( f"\n========================================" )
        print( f"Exposure Source: Name {es.NAME} Secondary {'(None)' if es.SECONDARY is None else es.SECONDARY}" )
        print( f"  Data subdirectory: {es.topdirname}" )
        print( f"  Pixel scale: {es.pixscale}" )
        print( f"  Thumbnail size: {es.clipsize}" )
        print( f"  Orientation is (maybe) {ExposureSource._orientation_desc[es._dbinfo.orientation]}" )
        print( f"  {len(es.chips)} chips" )
        if args.chip_detail:
            print( f"    {'chiptag':7s} {'ΔRA':>8s} {'Δdec':>7s}  good  "
                   f"{'LL':^16s} {'UL':^16s} {'UR':^16s} {'LR':^16s}" )
            print( f"    ---------------------------------------------------"
                   f"----------------------------------------------------" )
            for chip in es.chips:
                good = "yes" if chip.isgood else "no"
                s = io.StringIO("")
                s.write( f"    {chip.chiptag:7s} {chip.raoff:+8.3f} {chip.decoff:+7.3f}  {good:4s}  " )
                for corner in ( 0, 1, 2, 3 ):
                    r = getattr( chip, f"ra{corner}off" )
                    d = getattr( chip, f"dec{corner}off" )
                    sp = ' ' if corner<3 else ''
                    s.write( f"{r:+8.3f},{d:+7.3f}{sp}" )
                print( s.getvalue() )

# ======================================================================

if __name__ == "__main__":
    main()
                    
                    

    
