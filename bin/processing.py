#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import re
import logging
import pathlib
import argparse
import subprocess
from mpi4py import MPI

import numpy as np
import astropy.io.votable
from astropy.io import fits

import config
import db
import util
from bijaouisky import estimate_single_sky

_scriptdir = os.path.dirname(os.path.realpath(__file__))

_fitsparse = re.compile( r'^(.*)\.fits(\.f[gz])?' )


# ======================================================================

def make_weight( expsource, image, mask=None, maskdata=None, applybadmask=False,
                 weightpath=None, overwrite=False,
                 addtosky=0., gain=None, readnoise=None, darkcurrent=None, exptime=None,
                 logger=logging.getLogger("main") ):
    """Create a weight (1/σ²) image.

    expsource — ExposureSource object
    image — image file path
    mask — mask file path, or None; ignored if maskdata is not None; 0 is good, > is bad
    maskdata — mask array (same size as image data); 0 is good, >0 is bad
    applybadmask — set to True if mask isn't yet 0 good, >0 bad, but need to apply expsource.badmask
    weightpath — output weight filepath (default: image=~s/.fits/.weight.fits/, in same dir as image)
    overwrite — set True if it's OK to overwrite an existing file
    addtosky — sky value that has been subtracted from image
    gain — gain (e-/adu) of image (default: determined from header)
    readnoise — read noise (e-) of image (default: determined from header)
    darkcurrent — dark current (e-/s) of image (default: determined from header)
    exptime — exposure time (s) of image (default: determined from header)(
    logger — logging object (default: logging.getLogger("main"))

    Returns the path of the weight image created (a pathlib.Path object).

    σ² (in ADU) will be assumed to be equal to
         ( addtosky + the pixel value  + ( readnoise² + (darcur*exptime) ) / gain ) / gain

    If the image is not sky subtracted, pass in addtosky; if it is, pass
    in the sky value in ADU.  (It assumes the sky is constant across the
    image.)

    All of gain, readnoise, and darkcurrent will be determined using
    expsource unless they are explicitly passed.

    """
    if gain is None:
        gain = expsource.gain( image )
    if readnoise is None:
        readnoise = expsource.readnoise( image )
    if darkcurrent is None:
        darkcurrent = expsource.darkcurrent( image )
    if exptime is None:
        exptime = expsource.exptime( image )

    image = pathlib.Path( image )

    with fits.open( image, memmap=False ) as ihdu:
        if maskdata is None:
            if mask is not None:
                with fits.open( mask, memmap=False ) as mhdu:
                    maskdata = mhdu[0].data
            else:
                maskdata = np.zeros_like( image, dtype=np.uint8 )
        if applybadmask:
            maskdata = maskdata & expsource.badmask
        weight = gain / ( ihdu[0].data + addtosky + ( readnoise**2 + darkcurrent*exptime ) / gain )
        weight[ maskdata != 0 ] = 0.
        weighthdr = fits.Header( ihdu[0].header, copy=True )
    weighthdr["COMMENT"] = f'Weight (inv var) image for {os.path.basename(image)}'
    whdu = fits.PrimaryHDU( data=weight, header=weighthdr )

    if weightpath is None:
        match = _fitsparse.search( image.name )
        if match is None:
            fits.warning( f'Failed to parse {os.path.basename(image)} for *.fits, using full name as base' )
            basename = image
        else:
            basename = match.group(1)
        weightpath = image.parent / f'{basename}.weight.fits'
    else:
        weightpath = pathlib.Path( weightpath )

    logger.info( f'Writing weight {weightpath}' )
    whdu.writeto( weightpath, overwrite=True )

    return weightpath

# ======================================================================
# Extract a catalog from an image with Sextractor

def extract_catalog( expsource, image, weight, catalog, mask=None,
                     bgsub=True, backsize=256, bgconstant=None,
                     savenoise=False,
                     savebg=False,
                     savebgsub=False,
                     filter_name=None,
                     nnw=None,
                     seeing=None,
                     gain_key='GAIN',
                     saturate_key='SATURATE',
                     pixscale=1.,
                     zp=0.,
                     paramset='scamp',
                     paramfile=None,
                     timeout=300,
                     logger=logging.getLogger("main") ):
    """Extract a catalog from a FITS image with Sextractor.

    expsource — an ExpsoureSource object
    image — file path of single-HDU 2d FITS image
    weight — file path of inverse-variance weight image
    catalog — file path of output catalog.
    bgsub — subtract sky? (default: True)
    backsize — BACK_SIZE; ignored if bconstant != None (default: 256)
    bgconstant — value of constant background to subtract (default: None)
    savenoise — filename to save BACKGROUND_RMS checkimage to (default: None)
    savebg — filename to save BACKGROUND checkimage to (default: None)
    filter_name — name of convolution filter (default: /usr/share/source-extractor/default.conv)
    nnw — name of NNW file (default: /usr/share/source-extractor/default.nnw)
    seeing — FWHM of seeing **in pixels** (default: just guess)
    gain_key — keyword in header that has gain; ignored if expsource is not None
    saturate_key — keyword in header that has saturation level; ignored if expsource is not None
    pixscale — arcsec/pixel of image.  Ignored if expsource is not None
    zp — Magnitude zeropoint of image (default: 0)
    paramset — set of parameters to extract [ "scamp" or "scamp_vignet" ]
               ignored if paramfile is not None
    paramfile — file with list of parameters to extract
    timeout — die after this many seconds
    logger — logging object; default is logging.getLogger("main")

    Returns the result of the subprocess.run() command to run Sextractor

    """
    
    image = pathlib.Path(image)
    weight = pathlib.Path(weight)
    catalog = pathlib.Path(catalog)
    logger.info( f'Running sextractor on {image.name}, extracting to {catalog.name}' )

    match = _fitsparse.search( image.name )
    if not match:
        logger.warning( f'Couldn\'t parse {image.name} for *.fits (or *.fits.fz or .gz). '
                        f'Using {image.name} as the base name' )
        basename = image.name
    else:
        basename = match.group(1)

    if bgsub:
        if bgconstant is not None:
            background = f'-BACK_TYPE MANUAL -BACK_VALUE {bgconstant}'
        else:
            background = f'-BACK_TYPE AUTO -BACK_SIZE {backsize}'
    else:
        background = '-BACK_TYPE MANUAL -BACK_VALUE 0'

    flagimage = f'-FLAG_IMAGE {mask}' if mask is not None else ''
    weightimage = f'-WEIGHT_TYPE MAP_WEIGHT -WEIGHT_IMAGE {weight}' if weight is not None else '-WEIGHT_TYPE NONE'
    gainkey = gain_key if expsource is None else expsource.gain_keyword
    saturkey = saturate_key if expsource is None else expsource.saturate_keyword
    gainkey = f'-GAIN_KEY {gainkey}'
    saturkey = f'-SATUR_KEY {saturkey}'
    if seeing is None:
        if expsource is None:
            raise RuntimeError( "Must give seeing if you don't give expsource." )
        seeing = expsource.guess_seeing( image, logger=logger ) / expsource.pixscale
        logger.debug( f"Used expsource subclass to guess seeing of {seeing} pixels" )
    else:
        logger.debug( f"Using passed seeing of {seeing} pixels" )
    if expsource is not None:
        pixscale = expsource.pixscale
    # The "seeing" variable is now in pixels
    logger.debug( f'extract got seeing {seeing} and pixscale {pixscale}' )
    setseeing = f'-SEEING_FWHM {seeing*pixscale}'
    apertures = f'-PHOT_APERTURES {1.346*seeing},{5*seeing}'
    pixscale = f'-PIXEL_SCALE {pixscale}'
    zp = f'-MAG_ZEROPOINT {zp}' if zp is not None else ''

    logger.info( apertures )

    checktypes = []
    checkimages = []
    if savenoise:
        checktypes.append( 'BACKGROUND_RMS' )
        checkimages.append( image.parent /  f'{basename}.rms.fits' )
    if savebg:
        checktypes.append( 'BACKGROUND' )
        checkimages.append( image.parent / f'{basename}.bg.fits' )
    if savebgsub:
        checktypes.append( '-BACKGROUND' )
        checkimages.append( image.parent / f'{basename}.bgsub.fits' )
    if len(checktypes) > 0:
        checkimages = f"-CHECKIMAGE_TYPE {','.join(checktypes)} -CHECKIMAGE_NAME {','.join(checkimages)}"
    else:
        checkimages = f"-CHECKIMAGE_TYPE NONE"

    if filter_name is None:
        filter_name = f'-FILTER_NAME /usr/share/source-extractor/default.conv'
    else:
        filter_name = f'-FILTER_NAME {filter_name}'
    if nnw is None:
        nnw = f'-STARNNW_NAME /usr/share/source-extractor/default.nnw'
    else:
        nnw = f'-STARNNW_NAME {nnw}'

    if paramfile is not None:
        params = f'-PARAMETERS_NAME {paramfile}'
    else:
        if paramset == 'scamp':
            params = f'-PARAMETERS_NAME {_scriptdir}/../configdata/sex_scamp.param'
        elif paramset == 'scamp_vignet':
            params = f'-PARAMETERS_NAME {_scriptdir}/../configdata/sex_scamp_vignet.param'
        else:
            raise ValueError( f'Unknown paramset "{paramset}"' )

    com = ( f'source-extractor -CATALOG_TYPE FITS_LDAC -CATALOG_NAME {catalog} '
            f'{background} {flagimage} {weightimage} {checkimages} {gainkey} {saturkey} {setseeing} {pixscale} {zp} '
            f'-MEMORY_OBJSTACK 12000 -MEMORY_PIXSTACK 1200000 -VERBOSE_TYPE QUIET '
            f'{apertures} {filter_name} {nnw} {params} {image}' )
    logger.debug( f'Executing command: {com}' )
    return subprocess.run( com, shell=True, timeout=timeout )

# ======================================================================

def sky_values_to_header( expsource, image, mask, seeing, imageobj=None,
                          skipsky=False, alwaysredo=False, logger=logging.getLogger("main") ):
    """Make sure SEEING, MEDSKY, and SKYSIG are in the image header.

    exposurce — db.ExposureSource object
    image — path to FITS image on disk
    mask — path to mask on disk
    seeing — the already-determined seeing of the image in arcseconds
    imageobj — db.Image object.  imageobj.seeing must already be filled and the same as seeing
    skipsky — if True, don't update the header with the sky values
    alwaysredo — update MEDSKY and SKYSIG even if they're already in the header
    logger — a logging.logger object

    Returns medsky, skysig

    """
    skysig = None
    medsky = None

    if not skipsky:
        # Figure out if we've already measured the sky background, and if
        # it's already in the image header

        # Check to see if the necessary keywords are already there, return if so
        with fits.open(image) as ihdu:
            if ( ( not alwaysredo) and
                 ( 'SEEING' in ihdu[0].header ) and
                 ( 'MEDSKY' in ihdu[0].header ) and
                 ( 'SKYSIG' in ihdu[0].header ) ):
                if imageobj is not None:
                    if ( imageobj.seeing is None ) or ( imageobj.medsky is None ) or ( imageobj.skysig is None ):
                        logger.warning( f"Database medsky and/or skysig for {imageobj.basename} was None, "
                                        f"updating sa object from header, "
                                        f"but this may not get saved back to the database." )
                        imageobj.medsky = float( ihdu[0].header['MEDSKY'] )
                        imageobj.skysig = float( ihdu[0].header['SKYSIG'] )
                return float( ihdu[0].header['MEDSKY'] ), float( ihdu[0].header['SKYSIG'] )

        # We didn't find the necessary keywords there, so we have to
        # either pull it from the databse, or measure it
        if ( ( not alwaysredo) and
             ( imageobj is not None ) and
             ( imageobj.medsky is not None ) and
             ( imageobj.skysig is not None ) ):
            medsky = imageobj.medsky
            skysig = imageobj.skysig
        else:
            # Not in the database, must measure with bijaouisky
            with fits.open( image, memmap=False ) as ihdu:
                imagedata = ihdu[0].data
            if mask is not None:
                with fits.open(mask, memmap=False) as mhdu:
                    maskdata = mhdu[0].data
                    if expsource is not None:
                        maskdata = maskdata & expsource.badmask
            else:
                maskdata = np.zeros_like( image )
            logger.info( f'Measuring sky for {image.name}' )
            # ROB!  I picked the lowsigcut and sigcut based on looking at a few ZTF images
            bija, skysig, medsky = estimate_single_sky( imagedata, maskdata, lowsigcut=3, sigcut=8,
                                                        sigconvfrac=0.01, logger=logger )
    else:
        skysig = 0
        medsky = 0

    if ( skysig is None ) or ( medsky is None ):
        raise RuntimeError( "Don't have skysig and/or medsky; this should never happen." )

    # Save results to header

    logger.info( f'Updating {os.path.basename(image)} header with SEEING, SKYSIG, MEDSKY' )
    with fits.open(image, mode="update") as ihdu:
        ihdu[0].header['SEEING'] = ( seeing, 'Seeing FWHM (") ; curveball / psfex' )
        if not skipsky:
            ihdu[0].header['SKYSIG'] = ( skysig, 'Sky noise from curveball / biajouisky' )
            ihdu[0].header['MEDSKY'] = ( medsky, 'Sky level esimated from curveball / bijaouisky' )

    return medsky, skysig

# ======================================================================

def cat_psf_sky( expsource, image, weight, mask, catalog=None, psftimeout=300,
                 seeing_key='SEEING', pixscale_key='PIXSCALE',
                 nodb=False, force_redo=False, curdb=None, extractargs=None, skipsky=False,
                 logger=logging.getLogger("main"),):

    """Extract catalog (big file, _with_ VIGNET), determine psf, and measure
       sky level and noise from astrometrically calibrated image.

    NOTE: works both with images in the database and (with nodb=True)
    images not in the database.  As such, you have to pass full paths
    for image, weight, and mask, not just names.  (I.e., manually run
    the local_path() or get_file_from_archive() method of your exposure source yourself.

    expsource — an ExposureSource object
    image — path to a single-HDU 2d FITS image
    weight — path to a weight (inverse variance) image
    mask — path to a mask image; must be 0 is good, anything in 0xff is bad (what psfex expects)
    catalog — name of output catalog file (default: image -.fits +.cat)
    psftimeout — timeout (seconds) for psfex (default: 300)
    seeing_key — keyword in header that has seeing in arcseconds.  Will be ignored if nodb=False.
    pixscale_key — keyword in header that has arcsec/pixel; ignored if expsource is nodb=False.
    logger — a logging object (default: logging.getLogger("main")
    nodb — Ignore the database
    force_redo — Force redo of catalog creation
    curdb — A db.DB object, or None
    extractargs — a dictionary of additional arguments to pass to extract_catalog

    If nodb is not True, will look at the db.Image object (searching
    basename expsource.image_basename(image)).  If that object doesn't
    exist, throws an exception.  If it does exist and hascat is True and
    force_redo is False, it will read the seeing and skysig from the
    database, and pull down the catalog from the archive (if necessary).
    Otherwise, it will calculate all the stuff, save things to the
    database, and upload the catalog file to the archive.

    At the end, the image header should have the right SEEING, SKYSIG,
    MEDSKY.  There should be a .cat file from sextractor, and a .psf and
    .psf.xml file from psfex.

    By convention, we are setting the default PSF oversampling to be 4.
    This is necessary for PSF photometry with photutils.  UPDATE
    2024-03-02 : removing this assumption, going back to natural psfex
    size.  Photometry method psfphot2 can handle this.

    Returns those seeing, skysig, medsky

    """

    if force_redo:
        raise RuntimeError( "ROB!  You have to rethink this *all the way through* to make sure it is right" )

    imgobj = None
    image = pathlib.Path( image )
    weight = pathlib.Path( weight )
    mask = pathlib.Path( mask )
    basename = None

    mustrecalculate = nodb or force_redo

    # Make sure the image is in the dabase if we're using the database
    if not nodb:
        basename = expsource.image_basename( image.name )
        dbo = db.DB.get( curdb )
        imgobj = db.Image.get_by_basename( basename, curdb=dbo )
        if imgobj is None:
            dbo.close()
            raise RuntimeError( f"Can't find image {image.name} in the database." )

    # Try to get the catalog from the archive
    if not ( nodb or force_redo ):
        basename = expsource.image_basename( image.name )
        imgobj = db.Image.get_by_basename( basename, curdb=dbo )
        if imgobj is not None and imgobj.hascat:
            for suffix in ( 'cat', 'psf', 'psf.xml' ):
                expsource.get_file_from_archive( f'{basename}.{suffix}' )
            dbo.close()
            return imgobj.seeing, imgobj.skysig, imgobj.medsky

    # Either we're not using the databse, or the image doesn't have a catalog yet,
    # so do things.
    if basename is None:
        match = _fitsparse.search( image.name )
        if match is None:
            logger.warning( f'Couldn\'t parse {image.name} for *.fits, using whole thing as base.' )
            basename = image.name
        else:
            basename = match.group(1)

    if catalog is None:
        catalog = image.parent / f'{basename}.cat'
    else:
        if not nodb:
            raise RuntimeError( "...I think you don't want to pass non-None catalog." )
        catalog = pathlib.Path( catalog )
    psfxmlname = catalog.parent / f'{basename}.psf.xml'

    # Determine seeing and pixscale

    if not nodb:
        pixscale = expsource.pixscale
        # extract_catalog() wants seeing in pixels!
        seeing = expsource.guess_seeing( image, logger=logger ) / pixscale
        logger.debug( f"cat_psf_sky used expsource to get pixscale={pixscale} and seeing={seeing} pixels" )
    else:
        with fits.open( image, memmap=False ) as hdus:
            hdr = hdus[0].header
            if pixscale_key in hdr:
                pixscale = float( hdr[pixscale_key] )
                logger.debug( f"Pixscale from header: {pixscale}" )
            else:
                pixscale = 1.
                logger.warning( f"Could not find {pixscale_key} in header, guessing pixscale=1." )
            if seeing_key in hdr:
                # extract_catalog() wants seeing in pixels!
                seeing = float( hdr[seeing_key] ) / pixscale
                logger.debug( f"Seeing from header: {seeing} pixels" )
            else:
                logger.warning( f"Could not find {seeing_key} in header, guessing seeing=1." )
                seeing = 1.

    extractargs = {} if extractargs is None else extractargs
    extractargs['seeing'] = seeing if 'seeing' not in extractargs else extractargs['seeing']
    extractargs['pixscale'] = pixscale if 'pixscale' not in extractargs else extractargs['pixscale']

    logger.debug( f"extractargs={extractargs}" )
    # Extract catalog

    if 'paramfile' in extractargs:
        logger.warning( 'cat_psf_sky: need to use scamp_vignet parameter list, ignoring your paramfile' )
        del extractargs['paramfile']
    if 'paramset' in extractargs:
        logger.warning( 'cat_psf_sky: need to use scamp_vignet parameter list, ignoring your paramset' )
        del extractargs['paramset']

    res = extract_catalog( expsource, image, weight, catalog, mask=mask,
                           paramset='scamp_vignet', logger=logger, **extractargs )

    # Measure PSF.  We eventually want 4x oversampled psfs, which can take a long time to build.
    # So, do a first pass where we let psfex do its standard sampling, measure the fwhm, and
    # then do a second pass that makes the psf the size it needs to be.
    # UPDATE 2024-03-21 : no longer doing 4x oversampled
    # TODO : cleanup the code below
    
    psfstats = None
    # for psfexpass in range(2):
    for psfexpass in [0]:
        psfexbase = ( f'psfex {catalog} -CHECKPLOT_DEV NULL -CHECKPLOT_TYPE NONE '
                      f'-CHECKIMAGE_TYPE NONE '
                      f'-XML_NAME {psfxmlname} -WRITE_XML Y -PHOTFLUX_KEY FLUX_APER(2) '
                      f'-VERBOSE_TYPE QUIET ' )
        if psfexpass == 1:
            fwhm = psfstats.array['FWHM_Mean'][0]
            # Make the PSF clip big enough that we've got at least a radius of 5*FWHM
            psfsize = int( 10 * 4 * fwhm )
            if psfsize % 2 == 0:
                psfsize += 1
            logger.debug( f'First psfex pass got FWHM {fwhm} pix, making PSF size {psfsize}' )
            psfexbase += f'-PSF_SAMPLING 0.25, -PSF_SIZE {psfsize} '
            _psftimeout = 4 * psftimeout
        else:
            _psftimeout = psftimeout
        logger.info( f'Getting PSF for {catalog.name}' )
        psfsuccess = False
        for fwhmmax in ( 10.0, 15.0, 20.0, 25.0 ):
            com = f'{psfexbase} -SAMPLE_FWHMRANGE 0.5,{fwhmmax}'
            logger.debug( f'Running {com}' )
            
            try:
                res = subprocess.run( com.split(), shell=False, timeout=_psftimeout )
                if res.returncode != 0:
                    raise Exception( f'psfex return code: {res.returncode}' )
                psfsuccess = True
                break
            except Exception as e:
                logger.exception( f'psfex failed with fwhmmax={fwhmmax}, bumping it (or quitting)' )
                continue
        if not psfsuccess:
            raise Exception( f'psfex failed for {catalog.name}' )

        psfxml = astropy.io.votable.parse( psfxmlname )
        psfstats = psfxml.get_table_by_index(1)
        seeing = psfstats.array['FWHM_Mean'][0] * pixscale
    logger.debug( f"Got seeing {seeing} from psfex (psfstats.array['FWHM_Mean']={psfstats.array['FWHM_Mean']}" )

    # Measure sky background if necessary

    medsky, skysig = sky_values_to_header( expsource, image, mask, seeing, imageobj=imgobj,
                                           skipsky=skipsky, logger=logger )

    # Save results to database and archive if relevant

    if not nodb:
        for suffix in ( "cat", "psf", "psf.xml" ):
            logger.info( f"Uploading f{basename}.{suffix} to archive" )
            expsource.upload_file_to_archive( f'{basename}.{suffix}' )
        imgobj.seeing = seeing
        imgobj.skysig = skysig
        imgobj.medsky = medsky
        imgobj.hascat = True
        dbo.db.commit()
        dbo.close()

    return seeing, skysig, medsky

# ======================================================================

class ParallelCatPsfSkyRunner:
    def __init__( self, expsource, images, weights, masks,
                  catalogs=None, psftimeout=300, seeing_key='SEEING',
                  pixscale_key='PIXSCALE', nodb=False, force_redo=False,
                  extractargs=None, skipsky=False, logger=logging.getLogger("main") ):
        self.expsource = expsource
        self.images = images
        self.weights = weights
        self.masks = masks
        if catalogs is None:
            catalogs = [ None for i in range(len(images)) ]
        self.catalogs = catalogs
        self.psftimeout = psftimeout
        self.seeing_key = seeing_key
        self.pixscale_key = pixscale_key
        self.nodb = nodb
        self.force_redo = force_redo
        self.extractargs = extractargs
        self.skipsky = skipsky
        self.logger = logger

        if ( ( len(self.images) != len(self.weights) ) or
             ( len(self.images) != len(self.masks) ) or
             ( len(self.masks) != len(self.catalogs) ) ):
            raise RuntimeError( "All of images, weights, masks, and catalogs must have the same legnth "
                                "(alternatively, just catalogs may be None)" )

        self.seeings = [ None for i in range(len(images)) ]
        self.skysigs = [ None for i in range(len(images)) ]
        self.medskys = [ None for i in range(len(images)) ]


    def controller_callback( self, result, logger ):
        seeing, skysig, medsky = result
        self.seeings.append( seeing )
        self.skysigs.append( skysig )
        self.medskys.append( medsky )

    def worker_callback( self, num, logger ):
        seeing, skysig, medsky = cat_psf_sky( self.expsource, self.images[num], self.weights[num], self.masks[num],
                                              catalog=self.catalogs[num], psftimeout=self.psftimeout,
                                              seeing_key=self.seeing_key, pixscale_key=self.pixscale_key,
                                              nodb=self.nodb, force_redo=self.force_redo,
                                              extractargs=self.extractargs, skipsky=self.skipsky,
                                              logger=self.logger )
        return ( seeing, skysig, medsky )

    def run( self ):
        runner = util.Runner( MPI.COMM_WORLD,
                              lambda res, logger: self.controller_callback( res, logger ),
                              lambda num, logger: self.worker_callback( num, logger ),
                              logger=self.logger )
        indexes = list( range( len( self.images ) ) )
        success = runner.go( indexes )
        if MPI.COMM_WORLD.Get_rank() == 0:
            for i in indexes:
                if not success[i]:
                    self.logger.error( f"Runner failed for {images[i]}" )

def parallel_cat_psf_sky( expsource, images, weights, masks,
                          catalogs=None, psftimeout=300, seeing_key='SEEING',
                          pixscale_key='PIXSCALE', nodb=False, force_redo=False,
                          extractargs=None, skipsky=False, logger=logging.getLogger("main") ):

    runner = ParallelCatPsfSkyRunner( expsource, images, weights, masks, catalogs=catalogs,
                                      psftimeout=psftimeout, seeing_key=seeing_key,
                                      pixscale_key=pixscale_key, nodb=nodb, force_redo=force_redo,
                                      extractargs=extractargs, skipsky=skipsky, logger=logger )
    runner.run()
    return runner.seeings, runner.skysigs, runner.medskys

# ======================================================================

class ArgParseFormatter(argparse.RawDescriptionHelpFormatter,argparse.ArgumentDefaultsHelpFormatter):
    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )

def main():
    size = MPI.COMM_WORLD.Get_size()
    rank = MPI.COMM_WORLD.Get_rank()

    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - {rank:02d} - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( None, logger=logger )

    parser = argparse.ArgumentParser( description=( "Build catalog, measure psf and sky, "
                                                    "optionally do astrometic & photometric calibration" ),
                                      formatter_class=ArgParseFormatter,
                                      epilog=
"""Pass one of : (-i, -w, and -m), or (--iwmlist), or (-e, -l), or (-e, -b).  (See above.)

If you are running on more than one file, you might want to run with mpi.  To do that, run

   mpirun -np 4 python -m mpi4py processing.py ...

replacing 4 with the number of process you actually want to run.

"""
                                     )
    parser.add_argument( "-i", "--image", default=[], nargs='*',
                         help="Paths to images.  Written cats will be these with .fits replaced with .cat" )
    parser.add_argument( "-w", "--weight", default=[], nargs='*', help="Paths to weights" )
    parser.add_argument( "-m", "--mask", default=[], nargs='*', help="Paths to masks" )
    parser.add_argument( "--iwmlist", default=None,
                         help="Text file with 'image weight mask' on each line" )
    parser.add_argument( "-g", "--gain-key", default="GAIN",
                         help=( "Keyword in FITS header to find the gain; will be assumed 1 if not found. "
                                "Ignored if --basename is given." ) )
    parser.add_argument( "-s", "--saturate-key", default="SATURATE",
                         help=( "Keyword in FITS header to find the saturation; will be assumed ∞ if not found. "
                                "Ignored if --basename is given." ) )
    parser.add_argument( "--seeing-key", default="SEEING",
                         help=( "Keyword in FITS header to find the seeing in arcseconds; will be assumed 1 if not "
                                "found.  Ignored if --basename is given" ) )
    parser.add_argument( "--pixscale-key", default="PIXSCALE",
                         help=( "Keyword in FITS header to find the pixle scale in arcsec/pixel; will be assumed "
                                "1 if not found.  Ignored if --basename is given" ) )
    parser.add_argument( "-b", "--basename", default=[], nargs='*',
                         help="Basenames of image in database.  Must give (-b and -e) xor (-i and -w and -m) " )
    parser.add_argument( "-l", "--basename-list", default=None,
                         help="Text file with basenames to process, one per line" )
    parser.add_argument( "-e", "--expsource", default=None, help="Name of exposure source" )
    parser.add_argument( "-a", "--astrometry", action='store_true', default=False,
                         help="Do astrometic calibration; requires --basename and --expsource" )
    parser.add_argument( "-p", "--photometry", action='store_true', default=False,
                         help="Do photometric calibration; requires --basename and --expsource" )
    parser.add_argument( "--nodb", action='store_true', default=False,
                         help=( "Normally, with --basename, will save updated information (seeing, astrometry, etc.) "
                                "to the database.  Use this flag to not save anything.  With --image, the "
                                "database is never affected." ) )
    parser.add_argument( "-v", "--verbose", action='store_true', default=False, help="Show debug log info" )
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel( logging.DEBUG )

    spacesre = re.compile( '^\s*$' )

    if ( len(args.weight) != len(args.image) ) or ( len(args.mask) != len(args.image) ):
        raise RuntimeError( "Must pass the same number of files in all of --image, --weight, --mask" )

    # First: figure out if we're doing basenames or image/weight/mask,
    # and build up the list of whichever we're doing.  If
    # image/weight/mask, set indb to False; if basenames, set indb to
    # True.
    indb = False
    images = []
    weights = []
    masks = []
    basenames = []

    if args.iwmlist is not None:
        if ( len(args.image) > 0 ) or ( len(args.weight) > 0 ) or ( len(args.mask) > 0 ):
            raise RuntimeError( "Can't mix --iwmlist with --image, --mask, and --weight" )
        if ( len(args.basename) > 0 ) or ( args.basename_list is not None ):
            raise RuntimeError( "Can't mix --iwmlist with --basename or --basename-list" )
        linen = 0
        with open( args.iwmlist ) as ifp:
            for line in ifp:
                linen += 1
                line = line.strip()
                if spacesre.search( line ) is not None:
                    continue
                files = line.split()
                if len( files ) != 3:
                    raise ValueError( f"Error on line {linen} of {args.iwlist}, there aren't 3 files listed" )
                images.append( files[0] )
                weights.append( files[1] )
                masks.append( files[2] )
    elif len(args.image) > 0:
        if ( len(args.basename) > 0 ) or ( args.basename_list is not None ):
            raise RuntimeError( "Can't mix --image with --basename or --basename-list" )
        images = args.image
        weights = args.weight
        masks = args.mask
    elif args.basename_list is not None:
        indb = True
        if len(args.basename) > 0:
            raise RuntimeError( "Can't mix --basename-list with --basename" )
        with open( args.basename_list ) as ifp:
            for line in ifp:
                line = line.strip()
                if spacere.search( line ) is not None:
                    continue
                basenames.append( line )
    else:
        if len(args.basename) == 0:
            raise RuntimeError( "Must pass one of --image, --iwmlist, --basename, or --basename-list" )
        indb = True
        basenames = args.basename

    # Actually do it

    if not indb:
        if not nodb:
            logger.warning( "Assuming --nodb ; if you want to affect the database, give -b and -e instead of -i." )
        if args.astrometry or args.photometry:
            logger.warning( "Not doing astrometry or photomtery, that only applies for images in the database "
                            "(specified with -b and -e and not setting --nodb)." )

        if len(images) == 1:
            imagepath = pathlib.Path( args.image[0] ).resolve()
            weightpath = pathlib.Path( args.weight[0] ).resolve()
            maskpath = pathlib.Path( args.mask[0] ).resolve()
            catpath = imagepath.parent / imagepath.name.replace( ".fits", ".cat" )
            if rank == 0:
                cat_psf_sky( None, imagepath, weightpath, maskpath, catalog=catpath,
                             extractargs={ 'gain_key': args.gain_key, 'saturate_key': args.saturate_key },
                             nodb=True, logger=logger )
            MPI.COMM_WORLD.Barrier()
        else:
            imagepaths = [ pathlib.Path( i ) for i in images ]
            weightpaths = [ pathlib.Path( w ) for w in weights ]
            maskpaths = [ pathlib.Path( m ) for m in masks ]
            catpaths = [ i.parent / i.name.replace( ".fits", ".cat" ) for i in imagepaths ]
            if rank == 0:
                logger.info( f"Running paralllel_cat_psf_sky on {len(imagepaths)} images" )
            parallel_cat_psf_sky( None, imagepaths, weightpaths, maskpaths, catalogs=catpaths,
                                  extractargs={ 'gain_key': args.gain_key, 'saturate_key': args.saturage_key },
                                  nodb=True, logger=logger )

    else:
        if args.expsource is None:
            raise RuntimeError( "--expsource required with --basename" )

        # Import here instead of at the top to avoid circular imports
        import exposuresource

        ok = True
        expsource = exposuresource.ExposureSource.get( args.expsource )
        if expsource is None:
            raise RuntimeError( f"Unknown exposure source {args.expsource}" )

        imagepaths = []
        weightpaths = []
        maskpaths = []

        if rank == 0:
            with db.DB.get() as dbo:
                for basename in basenames:
                    expsource.ensure_image_local( basename, logger=logger )
                    imagepath = expsource.local_path_from_basename( basename, filetype='image', logger=logger )
                    weightpath = expsource.local_path_from_basename( basename, filetype='weight', logger=logger )
                    maskpath = expsource.local_path_from_basename( basename, filetype='mask', logger=logger )
                    if ( imagepath is None ):
                        logger.error( f"Coudln't find image {basename}.fits" )
                        ok = False
                    if ( weightpath is None ):
                        logger.error( f"Couldn't find image {basename}.weight.fits" )
                        ok = False
                    if ( maskpath is None ):
                        logger.error( f"Couldn't find image {basename}.mask.fits" )
                        ok = False
                    imagepaths.append( imagepath )
                    weightpaths.append( weightpath )
                    maskpaths.append( maskpath )
        imagepaths = MPI.COMM_WORLD.bcast( imagepaths, root=0 )
        weightpaths = MPI.COMM_WORLD.bcast( weightpaths, root=0 )
        maskpaths = MPI.COMM_WORLD.bcast( maskpaths, root=0 )
        ok = MPI.COMM_WORLD.bcast( ok, root=0 )

        if not ok:
            raise FileNotFoundError( "Couldn't find some needed files" )

        if len( imagepaths ) == 0:
            raise ValueError( "No files to process!" )

        if len( imagepaths ) == 1:
            if rank == 0:
                cat_psf_sky( expsource, imagepaths[0], weightpaths[0], maskpaths[0],
                             logger=logger, nodb=args.nodb )
            MPI.COMM_WORLD.Barrier()
        else:
            if rank == 0:
                logger.info( f"Running paralllel_cat_psf_sky on {len(imagepaths)} images" )
            parallel_cat_psf_sky( expsource, imagepaths, weightpaths, maskpaths,
                                  logger=logger, nodb=args.nodb )

            # TODO : parallelize this part
        if rank == 0:
            if args.astrometry or args.photometry:
                for basename, imagepath in zip( basenames, imagepaths ):
                    imgobj = db.Image.get_by_basename( basename )
                    if imgobj is None:
                        raise RuntimeError( "WTF??" )

                    if args.astrometry:
                        # Import here instead of at the top to avoid circular imports
                        import astrometry
                        if imgobj.isastrom:
                            logger.warning( f"{basename} is already astrometrically "
                                            f"calibrated, not doing anything." )
                        else:
                            astrometry.astrom_calib( imagepath, expsource=expsource, savedb=True, logger=logger )

                    if args.photometry:
                        if imgobj.isphotom:
                            logger.warning( f"{basename} is already photometrically "
                                            f"calibrated, not doing anything." )
                        else:
                            expsource.photocalib( imagepath, savedb=not args.nodb )

# ======================================================================

if __name__ == "__main__":
    main()




