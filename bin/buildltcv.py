#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import math
import io
import logging
import pathlib
import argparse
import traceback

import numpy
import sqlalchemy as sa
import PIL
from mpi4py import MPI

import astropy.units
import astropy.coordinates
from astropy.io import fits
from astropy.visualization import ZScaleInterval

import db
import util
import config
import processing
import astrometry
from exposuresource import ExposureSource
from subtract import Subtractor
from photometry import Photomotor

# ======================================================================
# Prolly outta be a member of LtcvBuilder

def process_image( expsource, basename, isastrom, isphotom, logger=logging.getLogger("main") ):
    """Make sure image is processed -- astro/phot calibrated, has a catalog."""

    logger.info( f'Processing {basename}' )
    try:
        expsource.ensure_sourcefiles_local( basename, logger=logger )
        imagepath = expsource.local_path( f'{basename}.fits' )
        datadir = imagepath.parent
        weightpath = datadir / f'{basename}.weight.fits'
        maskpath = datadir / f'{basename}.mask.fits'
        catpath = datadir / f'{basename}.cat'
        psfpath = datadir / f'{basename}.psf'

        logger.info( f"Getting catalog and PSF for {basename}" )
        seeing, skysig, skymed = processing.cat_psf_sky( expsource, imagepath, weightpath, maskpath,
                                                         logger=logger )

        if not isastrom:
            astrometry.astrom_calib( imagepath, expsource=expsource, cat=catpath, savedb=True, logger=logger )
            # logger.debug( f'Astrometry on {basename} complete, sleeping forever.' )
            # time.sleep(3600)
            # sys.exit(20)
        else:
            logger.info( f'{basename} already astronometrically calibrated' )

        if not isphotom:
            expsource.photocalib( imagepath, catpath )
        else:
            logger.info( f'{basename} already photometrically calibrated' )

        # Need some crap from the header
        with fits.open( imagepath, memmap=False ) as hdu:
            hdr = hdu[0].header
            magzp = hdr['MAGZP']
            lmt_mg = hdr['LMT_MG']
            seeing = hdr['SEEING']
            skysig = hdr['SKYSIG']

        logger.info( f'Done processing {basename}' )
    except Exception as e:
        with io.StringIO() as strstr:
            traceback.print_exc( file=strstr )
            logger.exception( f'{basename} had an exception! {str(e)}\nf{strstr.getvalue()}' )
        raise e
        # time.sleep(3600)
        # sys.exit(20)

    return { "basename": basename,
             "seeing": seeing,
             "skysig": skysig,
             "magzp": magzp,
             "lmt_mg": lmt_mg }

# ======================================================================

class LtcvBuilder:
    def __init__( self, expsource, obj, band, secondary=None, starttoff=-20, endtoff=90, datadir=None,
                  subalg='alard1', photmethod='apphot1', recenter=False, recenter_maxmove=2.,
                  subsearchtag='default', tagsublatest=True, tagsubdefault=True, subtags=[], newsubtags=False,
                  photsearchtag=None, tagphotlatest=True, tagphotmethoddefault=True, tagphotdefault=True,
                  phottags=[], newphottags=False, only_load=False,
                  comm=None, logger=logging.getLogger("main") ):
        """Construct a class for building a lightcurve.  Just call the class after you've made it.

        expsource -- the exposuresource.ExposureSource for the images, or
          the name of the exposure source
        obj -- the db.Object for the object, or the object name

        band -- the db.Band for the filter, or the band name

        secondary -- I wish I'd never defined this in the database in the
          first place

        startoff -- Initial offset in days relative to the objects' database t0

        endoff -- Final offset in days relative to the objects' database t0

        datadir -- Don't use this

        subalg -- Subtraction algorithm to use

        photmethod -- Photometry method to use


        recenter -- if False, will force the phtometry to be at the
          position of the object (using each image WCS to move it to the
          position on the image).  If True, will start at that position,
          but will pass "recenter=True" to the photometry method,
          meaning that all the imagex and imagey positions in the saved
          photometry may (depending on the photometry method) be
          updated.

          STRONG SUGGESTION: if this is True, set:        

             tagphotmethoddefault=False
             tagphotdefault=False
             phottags=['centering']

          (and newphottags=True as necessary).  If you *don't* do this,
          then the photometry will get tagged as default, and in the
          next pass, it won't be redone with the new position.  (If run
          from the command line, those three things plus recenter=True
          are what the --recenter command-line argument does.)

          You can still use tagsubdefault=True, because this flag
          doesn't change anything about the subtraction; doing so will
          make subsequent LtcvBuilder runs faster, as it won't have to
          redo the subtradction.

        recenter_maxmove -- Recentering shouldn't move the position of
          the SN by more than this many pixels.  If it does, redo the
          photometry *without* recentering.  If the S/N of the flux
          without recentering is >= 3, then return failure (since
          the only reason we expect recentering to move to far is if
          there isn't really much SN there, and the centroid algorithm
          goes looking for a noise fluctuation).

        subsearchtag -- The tag to search the database for existing
          subtractions.  It will only search subtractions that have a
          matching subalg.

        tagsublatest -- Any subtractions created as a result of this
           lightcurve building should be tagged as the latest subtraction
           for that image (default: True)

        tagsubdefault -- Any subtractions created as a result of this
           lightcurve building should be tagged as the default subtraction
           for that image (default: True)

        subtags -- List of additional tag names to apply to all subtractions
           created or identified as part of this lightcurve; will remove
           tagging of other subtractions on the same image

        newsubtags -- if True, then some of subtags might not already be
           defined in the database; otherwise, they must be, or it will be
           an error

        photsearchtag -- The tag to search the database for existing
           photometry from a given subtraction.  It will only search
           photometry that has a matching photmethod.  If None, then it
           will search 'default_ap' if the photometry method is aperture
           photomery, 'default_psf' if the photometry method is psf
           photometry.  It will only redo photometry if existing
           photometry with that tag isn't found.  If existing photometry
           *is* found, it will be tagged with all the tags that new
           photometry would have been tagged with.

        tagphotlatest -- Tag all photometery identified or created as latest

        tagphotmethoddefault -- Tag all photometry identified or created as
           the default ofr this general photometry method (psf vs ap)

        tagphotdefault -- Tag all photometry identified or created as default

        phottags -- List of additional tag names to apply to all photometry
           created or identified; will remove those tags from earlier
           photometry on the same image / object

        newphottags -- If True, some of phottags might not yet be defined in
           the database; otherwise, they must be, or it will be an error

        only_load -- If True, then will only try to load stuff from the database,
           won't actually perform any subtractions or photometry.

        comm -- a MPI communicator object

        logger -- a logging.logger object

        Will look in the ObjectReference table to get a reference for this
        object.  Will subtract all images in the right band and time range
        that include the object, only looking at images already in the
        database.  See get_images_for_sn.py to actually get all the right
        images into the database.

        SUGGESTED USAGE:

        * define an object (with z, ra, dec, and t0), somehow

        * run make_reference.make_reference() to create a reference for that
          object; do this once for each band you care about.

        * run get_images_for_sn.get_images_for_sn() to download images from
          the exposuresource for that object

        * run LtcvBuilder with recenter=True, tagphotmethoddefault=False,
          tagphotdefault=False, photsearchtag='centering', phottags=['centering'],
          newphottags=True

        * Figure out which points are bad and should not be used in 
          recentering.  You can do this in the curveball webap by looking
          at the lightcurve, searching for photometry points tagged 'centering',
          and flagging the bad ones.

        * run recenter.recenter with photag='centering', update=True on
          the object.  This will look at all the positions on the
          photometry you just did, and update the object's position in
          the database with a S/N-weighted mean.  (Suggestion: before
          running with update=True, run with update=False and
          plotfile=<filename>.svg ; then look at <filename>.svg to make
          sure things seem sane, and deal with it if they don't.)

        * run LtcvBuilder again with recenter=False, tagphotmethoddefault=True,
          tagphotdefault=True

        The first run of LtcvBuilder will tell the photometry method to
        centroid (or fit the best position) of the object on each
        subtraction. Figure out the new center by properly combining the
        measurements of the centers (WCS transforming from image position to
        RA,dec), and update the object record in the database with the new
        center.  The second run of LtcvBuilder will use the subtractions
        already created in the first run, but will redo the photometry now
        forcing it at the position of the object set in the object record of
        the database.

        """

        self.expsource = expsource
        self.obj = obj
        self.band = band
        self.secondary = secondary
        self.starttoff = starttoff
        self.endtoff = endtoff
        self.datadir = datadir
        self.subalg = subalg
        self.photmethod = photmethod
        self.recenter = recenter
        self.recenter_maxmove = recenter_maxmove
        self.subsearchtag = subsearchtag
        self.tagsublatest = tagsublatest
        self.tagsubdefault = tagsubdefault
        self.subtags = subtags
        self.newsubtags = newsubtags
        self.photsearchtag = photsearchtag
        self.tagphotlatest = tagphotlatest
        self.tagphotmethoddefault = tagphotmethoddefault
        self.tagphotdefault = tagphotdefault
        self.phottags = phottags
        self.newphottags = newphottags
        self.only_load = only_load
        self.comm = comm
        self.logger = logger

    # **********************************************************************

    def _handleprocessresult( self, res ):
        # Note that there's database redundancy here.  I re-get the image
        # even though I did in the driver process before.  Originally, I
        # opened the datbase once at the beginning and committed at the end
        # of the whole thing.  However, this meant counting on the
        # connection being good for (potentially) a very long time.
        with db.DB.get() as curdb:
            b = res["basename"]
            img = db.Image.get_by_basename( b, curdb=curdb )
            # ROB: you have updates here that maybe you shouldn't have.
            # The following get set in processing.cat_psf_sky : seeing, skysig, medsky, hascat, isastrom
            # The following get set in astrometry.astrom_calib: isastrom
            img.seeing = res["seeing"]
            img.skysig = res["skysig"]
            img.isastrom = True
            # ...meanwhile, I should think about the fact that I still need to set these next ones manually.
            # photocalib is designed to be run on non-loaded images, though, so....
            # ROB THINK ABOUT ALL OF THIS
            img.magzp = res["magzp"]
            img.limiting_mag = res["lmt_mg"]
            img.isphotom = True
            self.imgobj[b] = img
            curdb.db.commit()



    # **********************************************************************

    def _run_subtraction( self, basename ):
        # Using variables that are defined in the function that
        #  this function is inside makes me feel a little queasy,
        #  but I should probably just get over that.
        try:
            with db.DB.get() as curdb:
                myimg = db.Image.get_by_basename( basename, curdb=curdb )
                myexposure = myimg.exposure

            stor = Subtractor.get( self.subalg, self.expsource, myimg, self.reference,
                                   datadir=self.datadir, obj=self.obj, logger=self.logger )
            subobj = stor.subtract( searchtag=self.subsearchtag, tagdefault=self.tagsubdefault,
                                    taglatest=self.tagsublatest, tags=self.subtags, newtags=self.newsubtags,
                                    only_load=self.only_load )
            if subobj is None:
                self.logger.error( "Got back a null subtraction object from subtract for {basename}" )
                return { "basename": basename, "success": False }

            # Subtraction is done.  Now we deal with photometry
            # This is long, and should probably be moved to another function...?

            subpath = stor.sub_filepath( 'sub' )
            subweightpath = stor.sub_filepath( 'subweight' )
            submaskpath = stor.sub_filepath( 'submask' )
            with db.DB.get() as curdb:
                photor = Photomotor.get( self.photmethod, self.expsource, myimg, logger=self.logger, curdb=curdb )
                photorisap = photor.isap
                photorispsf = photor.ispsf
                if photorisap and photorispsf:
                    raise ValueError( "Photometry method is both aperture and psf!  That's not possible!  Panic!" )
                if ( not photorisap ) and ( not photorispsf ):
                    raise ValueError( "Photometry method is neither aperture nor psf!  I don't now how to cope!" )

                # Get all necessary tags
                tagnames = set( self.phottags )
                if self.tagphotlatest:
                    tagnames.add( 'latest' )
                if self.tagphotdefault:
                    if not self.tagphotmethoddefault:
                        raise ValueError( "Doesn't make sense to have tagphotdefault and not tagphotmethoddefault" )
                    tagnames.add( 'default' )
                if self.tagphotmethoddefault:
                    tagnames.add( 'default_ap' if photorisap else 'default_psf' )
                tagobjs = list( curdb.db.query( db.VersionTag ).filter( db.VersionTag.name.in_( tagnames )  ).all() )
                existingtags = [ e.name for e in tagobjs ]
                addedtags = []
                for tag in tagnames:
                    if tag not in existingtags:
                        addedtags.append( tag )
                if len( addedtags ) > 0:
                    if not self.newphottags:
                        raise ValueError( f"Unknown tags {','.join(addedtags)}" )
                    for tag in addedtags:
                        newtag = db.VersionTag( name=tag )
                        curdb.db.add( newtag )
                        curdb.db.commit()
                        tagobjs.append( newtag )

                # Look for existing photometry
                methvtname = 'default_ap' if photorisap else 'default_psf'
                photsearchtag = self.photsearchtag if self.photsearchtag is not None else methvtname
                photobj = None
                photobjs = list( curdb.db.query( db.Photometry )
                                 .filter( db.Photometry.object_id==self.obj.id )
                                 .filter( db.Photometry.subtraction_id==subobj.id )
                                 .filter( db.Photometry.photmethod_id==photor.dbid )
                                 .filter( db.Photometry.image_id==myimg.id )
                                 .join( db.PhotometryVersiontag,
                                        db.Photometry.id==db.PhotometryVersiontag.photometry_id )
                                 .join( db.VersionTag,
                                        sa.and_( db.VersionTag.id==db.PhotometryVersiontag.versiontag_id,
                                                 db.VersionTag.name==photsearchtag ) ).all() )
                if len(photobjs) > 0:
                    if len(photobjs) == 1:
                        self.logger.info( f"Photometry already exists with version tag {photsearchtag} "
                                          f" using method {photor.dbname} on "
                                          f"subtraction {subobj.id} with image {myimg.basename} and "
                                          f"reference {self.reference.basename}, not redoing." )
                        photobj = photobjs[0]
                    else:
                        sio = io.StringIO()
                        sio.write( f"Multiple photometry already exists tagged as {photsearchtag} "
                                   f"using method {photor.dbname} on "
                                   f"subtraction {subobj.id} with image {myimg.basename} and "
                                   f"reference {self.reference.basename}. Fix the database.  Failing." )
                        self.logger.error( sio.getvalue() )
                        raise RuntimeError( sio.getvalue() )
                else:
                    if self.only_load:
                        self.logger.error( f"Couldn't find photometry tagged as {photsearchtag} "
                                           f"using method {photor.dbname} on "
                                           f"subtraction {subobj.id} with image {myimg.basename} and "
                                           f"reference {self.reference.basename}, but only_load was True. Failing." )
                        return { "basename": basename, "success": False }

                # Actually do the photometry if we didn't find a satisfactory existing one
                if photobj is None:
                    with fits.open(subpath) as sub, fits.open(subweightpath) as subwt, fits.open(submaskpath) as submsk:
                        if self.recenter:
                            wcs = db.WCS.get_astropy_wcs_for_image( myimg, curdb=curdb )
                            sc = astropy.coordinates.SkyCoord( self.obj.ra, self.obj.dec, frame='icrs',
                                                               unit=astropy.units.deg )
                            origx, origy = wcs.world_to_pixel( sc )

                        phot, dphot, x, y, info = photor.measure( None, None, ra=self.obj.ra, dec=self.obj.dec,
                                                                  imagedata=sub[0].data,
                                                                  weightdata=subwt[0].data,
                                                                  maskdata=submsk[0].data,
                                                                  recenter=self.recenter,
                                                                  curdb=curdb )

                        # Make sure the results are sensible
                        if ( not math.isfinite(phot) ) or ( not math.isfinite(dphot) ):
                            self.logger.error( f"Photometry gave NaN/infinite for either phot or dphot" )
                            return { "basename": basename, "success": False }
                        # Specific checks for types of methods
                        if photor.isap:
                            if not math.isfinite( info['apercor'] ):
                                self.logger.error( f"Apercor isn't finite for {basename}: {info['apercor']}" )
                                return { "baename": basename, "success": False }
                        elif photor.ispsf:
                            # Replace this pass with psf sanity checks
                            pass
                        else:
                            self.logger.warn( f"Photometry method was neither aperture nor PSF; I am surprised." )

                        if self.recenter:
                            dx = x - origx
                            dy = y - origy
                            if ( math.sqrt( dx*dx + dy*dy ) > self.recenter_maxmove ):
                                self.logger.warning( f"Recentering moved SN position by {dx:.2f}, {dy:.2f} pixels; "
                                                     f"this is too much, redoing without recentering." )

                                phot, dphot, x, y, info = photor.measure( None, None, ra=self.obj.ra, dec=self.obj.dec,
                                                                          imagedata=sub[0].data,
                                                                          weightdata=subwt[0].data,
                                                                          maskdata=submsk[0].data,
                                                                          recenter=False,
                                                                          curdb=curdb )
                                if ( phot / dphot ) >= 3:
                                    self.logger.error( "S/N was >3 on a too-far recentering point; "
                                                       "returning failure." )
                                    return { "basename": basename, "success": False }
                            else:
                                self.logger.info( f"Recentering moved SN position by {dx:.2f}, {dy:.2f} pixels" )

                    refnorm = stor.refnorm
                    newclip, refclip, subclip = stor.clips( x, y, self.expsource.clipsize )
                    zi = ZScaleInterval( contrast=0.02 )
                    il, iu = zi.get_limits( newclip )
                    sl, su = zi.get_limits( subclip )
                    newmed = numpy.median( newclip )
                    refmed = numpy.median( refclip )
                    rl = refmed + ( il - newmed ) * refnorm
                    ru = refmed + ( iu - newmed ) * refnorm
                    fitses = { "new": None, "ref": None, "sub": None }
                    jpgs = { "new": None, "ref": None, "sub": None }
                    for which, clip, l, u in zip( [ "new", "ref", "sub" ],
                                                  [ newclip, refclip, subclip ],
                                                  [ il, rl, sl ],
                                                  [ iu, ru, su ] ):
                        tmp = ( clip - l ) * 255/(u-l)
                        tmp[ tmp>255 ] = 255
                        tmp[ tmp<0 ] = 0
                        tmp = numpy.array( numpy.floor( tmp + 0.5 ), dtype=numpy.uint8 )
                        jpg = PIL.Image.fromarray( tmp )
                        bio = io.BytesIO()
                        jpg.save( bio, "JPEG", quality=90 )
                        jpgs[which] = bio.getvalue()
                        bio.close()
                        tmp = fits.PrimaryHDU( data=clip )
                        bio = io.BytesIO()
                        tmp.writeto(bio)
                        fitses[which] = bio.getvalue()
                        bio.close()                

                    if phot <= 0:
                        mag = math.nan
                        dmag = math.nan
                    else:
                        mag = -2.5 * math.log10( phot ) + myimg.magzp,
                        dmag = math.fabs( 2.5 / math.log(10) * dphot / phot ),

                    photobj = db.Photometry( mjd=myexposure.mjd,
                                             flux=phot,
                                             dflux=dphot,
                                             magzp=myimg.magzp,
                                             mag=mag,
                                             dmag=dmag,
                                             refnorm=refnorm,
                                             imagex=x,
                                             imagey=y,
                                             photmethod_id=photor.dbid,
                                             object_id=self.obj.id,
                                             subtraction_id=subobj.id,
                                             image_id=myimg.id,
                                             new_fits=fitses['new'],
                                             ref_fits=fitses['ref'],
                                             sub_fits=fitses['sub'],
                                             new_jpeg=jpgs['new'],
                                             ref_jpeg=jpgs['ref'],
                                             sub_jpeg=jpgs['sub'] )
                    curdb.db.add( photobj )
                    if photor.isap:
                        apphotobj = db.ApPhotData( photometry=photobj,
                                                   aperrad=info['radius'],
                                                   apercor=info['apercor'] )
                        curdb.db.add( apphotobj )
                    elif photor.ispsf:
                        # Add any necessary PSF data
                        pass

                    curdb.db.commit()

                self.logger.info( f"Tagging photometry point with tags: {','.join( [ t.name for t in tagobjs ] )}" )
                for tagobj in tagobjs:
                    # See if there's existing photometry for this object/image with this tag;
                    # if so, nuke it

                    existingvts = list( curdb.db.query( db.PhotometryVersiontag )
                                        .filter( db.PhotometryVersiontag.versiontag_id==tagobj.id )
                                        .join( db.Photometry, db.PhotometryVersiontag.photometry_id==db.Photometry.id )
                                        .filter( db.Photometry.object_id==self.obj.id )
                                        .filter( db.Photometry.image_id==myimg.id ).all() )
                    for evt in existingvts:
                        curdb.db.delete( evt )

                    # Creat the tag for this guy

                    newvt = db.PhotometryVersiontag( photometry_id=photobj.id,
                                                     versiontag_id=tagobj.id )
                    curdb.db.add( newvt )
                curdb.db.commit()

                return { "basename": basename, "success": True }
        except Exception as e:
            errmsg = io.StringIO()
            traceback.print_exc( file=errmsg )
            self.logger.error( f'Error subtracting & photometering image {basename}: {str(e)}:\n{errmsg.getvalue()}' )
            errmsg.close()
            return { "basename": basename, "success": False }


    # **********************************************************************

    def _handle_sub_result( self, res ):
        if res['success']:
            self.subbedimages.add( res['basename'] )
        else:
            self.failedimages.add( res['basename'] )


    # **********************************************************************

    def __call__( self ):
        if self.comm is None:
            size = 1
            rank = 0
        else:
            size = self.comm.Get_size()
            rank = self.comm.Get_rank()

        if self.datadir is None:
            self.datadir = config.Config.get().value("datadirs")[0]
        else:
            # Make sure that datadir is in the config datadirs?  Or something else?
            raise Exception( "I probably shouldn't have a datadir parameter, I am afraid of it." )
        self.datadir = pathlib.Path( self.datadir )

        # I don't seem to be able to broadcast these with comm.bcast, so
        #  spam the hell out of the database temporarily
        # (This offends me, but I'm too lazy to implement something that
        #  can be pickled....)
        if not isinstance( self.expsource, ExposureSource ):
            expsourceobj = ExposureSource.get( self.expsource, self.secondary )
            if expsourceobj is None:
                raise ValueError( f'Unknown exposure source {self.expsource}' )
            self.expsource = expsourceobj
        if not isinstance( self.obj, db.Object ):
            objobj = db.Object.get_by_name( self.obj )
            if objobj is None:
                raise ValueError( f'Unknown object {self.obj}' )
            self.obj = objobj
        if not isinstance( self.band, db.Band ):
            bandobj = db.Band.get_by_name( self.band, self.expsource.id )
            if bandobj is None:
                raise ValueError( f'Unknown band {self.band}' )
            self.band = bandobj

        # Identify a reference
        if rank == 0:
            refs = db.ObjectReference.get_for_obj_and_band( self.obj, self.band )
            if len(refs) == 0:
                raise RuntimeError( f'No reference for object {self.obj.name} in band {self.band.name}' )
            if len(refs) > 1:
                self.logger.warning( f'There are {len(refs)} refs defined for {self.obj.name} '
                                     f'in band {self.band.name}; using the first one the database gave me.' )
            refobjid = refs[0].id
            refid = refs[0].image_id
        else:
            refobjid = None
            refid = None

        if self.comm is not None:
            refobjid = self.comm.bcast( refobjid, root=0 )
            refid = self.comm.bcast( refid, root=0 )

        # Once again every rank will hit the database to get the same damn information
        self.reference = db.Image.get( refid )
        if rank == 0:
            self.logger.info( f'Using reference image {self.reference.basename}' )
            # Make sure the reference is available locally.  Note that this won't download
            #  from the archive if the files are already here.
            for suffix in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
                localfile = self.expsource.get_file_from_archive( f'{self.reference.basename}{suffix}' )
                self.logger.debug( f'  Reference {suffix} : {localfile}' )

        # Get the image list

        t0 = self.obj.t0 + self.starttoff
        t1 = self.obj.t0 + self.endtoff

        if rank == 0:
            with db.DB.get() as curdb:
                images = db.Image.get_including_point( self.obj.ra, self.obj.dec, band=self.band,
                                                       mjd0=t0, mjd1=t1,
                                                       expsourceid=self.expsource.id, curdb=curdb )
                self.logger.debug( f'Throwing out stacks (and other things without an associated exposure)...' )
                newimages = []
                for image in images:
                    if ( image.exposure_id is None ) or image.isstack:
                        continue
                    newimages.append( image.basename )
            images = newimages
            self.logger.info( f'Found {len(images)} images' )
        else:
            images = None
        if size > 1:
            images = self.comm.bcast( images, root=0 )

        # Log

        if rank==0:
            self.logger.info( f'****** Building lightcurve for object {self.obj.name} in band {self.band.name}' )
            self.logger.info( f't0 = {t0}, t1 = {t1}' )
            self.logger.info( f'Reference image = {self.reference.basename}' )
            self.logger.info( f'There are {len(images)} images.' )
            self.logger.info( f'Subtraction algorithm: {self.subalg}, photometry method: {self.photmethod}' )
            if self.recenter:
                self.logger.info( f'Allowing photometry to recenter on the residual' )
            else:
                self.logger.info( f'Forcing photometry at the object\'s position' )
            self.logger.info( f"Will look for existing subtractions tagged with {self.subsearchtag}" )
            if self.photsearchtag is not None:
                self.logger.info( f"Will look for existing photometry tagged with {self.photsearchtag}" )
            else:
                self.logger.info( f"Will look for existing phtometry tagged with default_ap xor default_psf "
                                  f"(depending on whether it's aperture of psf photometry)" )
            self.logger.info( f"Will {'NOT ' if not self.tagsublatest else ''} tag created subtractions as latest" )
            self.logger.info( f"Will {'NOT ' if not self.tagsubdefault else ''} tag created subtractions as default" )
            if len(self.subtags) > 0:
                self.logger.info( f"Additional subtraction tags: {self.subtags}" )
            self.logger.info( f"Will {'NOT ' if not self.tagphotlatest else ''} tag photometry as latest" )
            self.logger.info( f"Will {'NOT ' if not self.tagphotmethoddefault else ''} tag photometry as "
                              f"default for the general method (i.e. default_ap or default_psf" )
            self.logger.info( f"Will {'NOT ' if not self.tagphotdefault else ''} tag photometry as default" )
            if len(self.phottags) > 0:
                self.logger.info( f"Additional photometry tags: {self.phottags}" )
            if self.only_load:
                self.logger.info( f"ONLY LOADING stuff from the database, not doing any new measurements" )
            self.logger.info( "******" )


        # Extract catalog, measure PSF and sky, astrom calib, photom calib
        # Do this with util.runner so that it will multiprocess via MPI if available

        runner = util.Runner( self.comm,
                              lambda res, logger: self._handleprocessresult( res ),
                              lambda data, logger: process_image( self.expsource, data["basename"],
                                                                  data["isastrom"], data["isphotom"],
                                                                  logger=self.logger ),
                              logger=self.logger )
        if rank == 0:
            self.imgobj = {}
            datalist = []
            with db.DB.get() as curdb:
                for basename in images:
                    self.imgobj[basename] = db.Image.get_by_basename( basename, curdb=curdb )
                    datalist.append( { "basename": basename,
                                       "isastrom": self.imgobj[basename].isastrom,
                                       "isphotom": self.imgobj[basename].isphotom } )
            success = runner.go( datalist )
            if sum(success) != len(images):
                for i, (succ, basename) in enumerate( zip( success, images ) ):
                    if not succ:
                        self.logger.error( f'Catalog/PSF/Astrom/Photom failed for {basename}' )
        else:
            runner.go( None )

        # Given that runner.go ends with a bcast, this shouldn't be necessary
        if self.comm is not None:
            self.logger.debug( "About to barrier" )
            self.comm.Barrier()

        # Subtract and measure photometry
        runner = util.Runner( self.comm,
                              lambda res, logger: self._handle_sub_result( res ),
                              lambda basename, logger: self._run_subtraction( basename ),
                              logger=self.logger )
        if rank == 0:
            self.subbedimages = set()
            self.failedimages = set()
            self.logger.info( "********** RUNNING SUBTRACTIONS AND DIFFERENCE PHOTOMETRY **************" )
            success = runner.go( images )
            if len(self.failedimages) > 0:
                self.logger.error( f"Failed images: {self.failedimages}" )
            if len(self.subbedimages) + len(self.failedimages) != len(images):
                self.logger.error( f"n. of subtracted images {len(self.subbedimages)+len(self.failedimages)} != "
                                   f"n. of images {len(images)}" )
            self.logger.info( f'{len(self.subbedimages)} of {len(images)} images (probably) '
                              f'succesfully subtracted & photometered' )
        else:
            runner.go( None )

# ======================================================================

def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - {rank:02d} - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( None, logger=logger )
    if rank == 0:
        parser = argparse.ArgumentParser( description="build lightcurve from images in database",
                                          formatter_class=argparse.RawDescriptionHelpFormatter,
                                          epilog="""Builds lightcurves form images in the database for a given object,
exposure source, and band.  Does not download images from the exposure
source; assumes they're present already.

Assumes the images in the database are already reduced images
(i.e. flatfielded, etc).  Also assumes that there's a reference tagged
in the database for this band and object.

Before running this, use get_images_for_sn.py to add the relevant
lightcurve images to the databse.  Use make_reference.py to build a
reference image.  Both of those will go out to the exposure source and
pull down necessariy images.  make_reference will (probably) try to
build a reference based on a group of best-seeing and similar-zeropoint
images that are outside a time range where the lightcurve is believed to
be active; actual implementation may be exposuresource-dependent.

For every image for the given exposuresource, object, and band, whose
mjd is in the indicated time range (relative to the object's t0 field in
the database), it will look in the database for a subtraction for that
image and the object's reference (as found in the database).  If no
subtraction is found, it will perform that subtraction.  Then, it will
look for photometry on the subtraction; if none is found, it will
perform photometry and save it to the database.

SUGGESTED USAGE:

(Run each python command listed below with --help for more information on that command.)

* Define an object with

    python define_object.py <name> <ra> <dec> <t0> -z <z>

  where <name> is the object name, <ra> and <dec> is the object position
  in decimal degrees, <t0> is the MJD of the peak time of the object,
  and <z> is the object's redshift.  (Leave the -z <z> tag off if the
  object's redshift is unknown.)

  BUT FIRST try

    python list_objs.py

  to see if your object is already defined.

* (Optional) Run

    python get_images_for_sn.py -o <name> -e <expsource> -j

  where <name> is the name of the object you just defined, and
  <expsource> is the exposure source whose images you want to
  search. The flag -j tells it not to actually *get* any images, just
  list the ones that would be there.  Look at the output, see which
  bands there are images for.

* Run

    python make_reference.py <band> -o <name>

  where <band> is the band you want to make the reference for, and
  <name> is the object name.  Run this once for each band.

  NOTE: if your reference fails, try running with -v and look at the
  HUGE amount of output.  If, near the top, it says something like
  "Securing references from 1 images" (or some other number other than 1
  that's still less than, say, 10 or 20), then there just aren't enough
  images of this object to create a reference or a lightcurve.  However,
  if there are lots of possible images, it may be failing because it
  couldn't find a cluster of enough images all on similar days.  If that
  happens, then you can try relaxing the requirement that the reference
  be built from nearby days; to do this, add the argument:

    -k "cluster_outlier_days 0 cluster_img_per_day 0"

  to the python make_reference.py command.  (The quotes are important here.)

* Run

    python get_images_for_sn.py -o <name> -e <expsource>

  Notice there is no -j this time.  This will actually download the
  images from the exposure source, and add them to the database.

* Run

    python buildltcv.py <obj> <expsource> <band> --recenter

  (adding -m ... and -p ... if you want to choose a non-default
  subtraction or photometry algorithm).  This will build a lightcurve,
  tagging the *subtractions* as default, but not tagging the photometry
  as default; the photometry will only be tagged as "latest" and
  "centering".  It will not force the positions of the objects, but
  allow them to be recentroided.  The purpose of this is to tune up the
  position of the object.

  Do this once for each band.

  Note: you probably really want to do:

    mpirun -np <n> python -m mpi4py buildltcv.py ....

  where <n> is the number of processes to run at once.  If you make this 
  more than (say) 20 (or less, if you're doing a bunch at the same time),
  you *might* start to get failiures as too many processes query external
  resources at the same time.

* Go to the web ap, click on "objects w/ subtractions and photometry".
  Select "centering" as the photometry versiontag at the top.  Find your
  object in the list, and click on it.  Look at the
  lightcurves. Assuming it's not a disaster, identify any clearly bad
  points in the lightcurve and use the webap to mark them bad.

* Run 

    python recenter.py <object> -p <filename>.svg

  This will figure out a weighted mean (weighted by S/N squared) of the
  position of the object based on the measurements you just made with
  your runs of buildltcv.py.  It will tell you the new position, and how
  far off it is from the old position.  Display the plot file
  <filename>.svg and make sure things look sane.  If they don't, worry.

* Once you're sure that the updated position is reasonable, run

    python recenter.py <object> --update

  This will actually update the object with the new position.

* Run

    python buildltcv.py <obj> <expsource> <band>

  (adding -m ... and -p ... if you want to choose a non-default
  subtraction or photometry algorithm, and other command-line arguments
  if you know what you're doing).  Run this once for each band.  This
  should be faster than the last time you did it, because it doesn't have
  to rerun all of the subtractions, just the photometry.  This will force
  the photometry at the position of the object in the databse (because
  you are NOT including the --recenter tag this time around), and will
  tag the photometry as "default" and "default_ap" (for aperture
  photometry) or "default_psf" (for psf fitting photometry).

  Once again, you probably really want to do

    mpirun -np <n> python -m mpi4py buildltcv.py ....

  with the same caveats as above.

* Look at the lightcurve in the webap; you now want to look at the
  "default" photometry versiontag.  Flag any bad points as bad.  (The
  ones you flagged before will no longer be flagged, because you flagged
  the photometry that was done when you were recentering.)

* Run a SALT2 fit, or do whatever else it is you want to do with this stuff.

"""                                          )
        parser.add_argument( "object", help="Name of object")
        parser.add_argument( "expsource", help="Name of exposure source" )
        parser.add_argument( "band", help="Name of band" )
        parser.add_argument( "--secondary", type=str, default=None,
                             help="Secondary for the exposure source" )
        parser.add_argument( "-s", "--starttoff", type=float, default=-20,
                             help="Start this many days after peak (default: -20)" )
        parser.add_argument( "-f", "--endtoff", type=float, default=90,
                             help="End this may days after peak (default: 90)" )
        parser.add_argument( "-m", "--sub-method", default='alard1',
                             help="Name of subtraction algorithm to use (default: alard1)" )
        parser.add_argument( "--sub-search-tag", default="default",
                             help=( "Use this versiontag when searching the database for subtractions. "
                                    "(default: \"default\")" ) )
        parser.add_argument( "--no-tag-sub-default", action='store_true', default=False,
                             help=( "Normally, if a subtraction doesn't exist for a given image and "
                                    "ref and sub method, the new subtraction will be tagged as 'default'. "
                                    "Specify this flag to not tag it." ) )
        parser.add_argument( "--subtags", nargs="+", default=[],
                             help=( "Additional tags (beyond default, unless --no-tag-sub-default is given) "
                                    "to tag subtractions with." ) )
        parser.add_argument( "-p", "--phot-method", default='apphot1',
                             help="Name of photometry method to use (default: apphot1)" )
        parser.add_argument( "--pho-search-tag", default=None,
                             help=( "Use this versiontag when searching the database for photometry. "
                                    "By default, searches default_ap for aperture photomery methods "
                                    "or default_psf for psf-fitting photometry methods.  Set this to "
                                    "a tag that does not exist to force it to always redo all measurements.") )
        parser.add_argument( "-n", "--no-tag-pho-default", action='store_true', default=False,
                             help=( "Normally, if photometry point doesn't already exist for "
                                    "a given subtraction, the new things will be "
                                    "version tagged as both 'default' and ('default_ap' (for "
                                    "aperture photometry) or 'default_psf' (for psf photometry)). "
                                    "Specify this flag to *not* do that tagging." ) )
        parser.add_argument( "--photags", nargs="+", default=[],
                             help=( "Additional tags (beyond default and default_(ap|psf), unless "
                                    "--no-tag-pho-default is given) to tag photometry points with." ) )
        parser.add_argument( "-r", "--recenter", action='store_true', default=False,
                             help=( "Normally, forces photometry at the position of the object in the "
                                    "object table in the database (moved to each image via its WCS). "
                                    "If you specify this, it will allow the photometry to find a better center. "
                                    "Giving this option implies --no-tag-pho-default, "
                                    "--pho-search-tag centering, and --photags centering" ) )
        parser.add_argument( "-v", "--verbose", action="store_true", default=False,
                             help="Show debug logging messages" )
        args = parser.parse_args()
    else:
        args = None
    args = comm.bcast( args, root=0 )

    if args.verbose:
        logger.setLevel( logging.DEBUG )

    addedphotags = set( args.photags )
    if args.recenter:
        args.pho_search_tag = 'centering'
        args.no_tag_pho_default = True
        addedphotags.add( 'centering' )
    addedphotags = list( addedphotags )

    phodef = not args.no_tag_pho_default
    subdef = not args.no_tag_sub_default

    builder = LtcvBuilder( args.expsource, args.object, args.band, secondary=args.secondary,
                           starttoff=args.starttoff, endtoff=args.endtoff,
                           subalg=args.sub_method, photmethod=args.phot_method, recenter=args.recenter,
                           subsearchtag=args.sub_search_tag, tagsublatest=True, tagsubdefault=subdef,
                           subtags=args.subtags, newsubtags=True,
                           photsearchtag=args.pho_search_tag, tagphotlatest=True,
                           tagphotmethoddefault=phodef, tagphotdefault=phodef, phottags=addedphotags, newphottags=True,
                           comm=comm, logger=logger )
    builder()

# ======================================================================

if __name__ == "__main__":
    main()
