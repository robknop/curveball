import sys
import argparse
import math
import logging
import numpy
import pandas
from matplotlib import pyplot

import astropy.units as units

import db

colors = [ 'green', 'red', 'brown' ]

def recenter( obj, photag='centering', update=False, curdb=None, plotfile=None, logger=logging.getLogger("main") ):
    """Determine a new RA/Dec for a supernova from photometry.

    obj -- a db.Object object, or the name of the object
    
    photag -- the version tag of the photometry for the object to consider

    update -- If true, save the new RA and Dec of the object to the database

    curdb (optional) -- a db.DB object

    plotfile -- if not None, the name of a file to which a diagnostic plot will be written

    logger (optional) -- a logging.logger object

    Will pull all photometry for the object tagged with the given
    photag.  Will look at the imagex and imagey fields in the
    photometry, and will use image WCS objects to transform them to RA
    and Dec.  Will do a weight average of each coordinate, weighting by
    the SN**2 of the photometry.

    Returns meanra, meandec

    """
    with db.DB.get( curdb ) as dbo:
        if not isinstance( obj, db.Object ):
            objobj = db.Object.get_by_name( obj, curdb=dbo )
            if objobj is None:
                raise ValueError( f"Unknown object {obj}" )
            obj = objobj

        phos = db.Photometry.get_for_obj( obj, version=photag, curdb=dbo )
        bands = []
        ras = []
        decs = []
        sns = []

        for photuple in phos:
            band, phos = photuple
            for pho in phos:
                sn = pho.flux / pho.dflux
                img = db.Image.get( pho.image_id, curdb=dbo )
                wcs = db.WCS.get_astropy_wcs_for_image( img, curdb=dbo )
                sc = wcs.pixel_to_world( pho.imagex, pho.imagey )
                ras.append( sc.ra.to( units.deg ).value )
                decs.append( sc.dec.to( units.deg ).value )
                sns.append( sn )
                bands.append( band )

    sns = numpy.array( sns )
    ras = numpy.array( ras )
    decs = numpy.array( decs )
    bands = numpy.array( [ b.name for b in bands ] )

    wgood = numpy.where( sns >= 3. )[0]

    meanra = ( ras[wgood] * ( sns[wgood] **2 ) ).sum() / ( (sns[wgood] ** 2).sum() )
    meandec = ( decs[wgood] * ( sns[wgood] ** 2 ) ).sum() / ( (sns[wgood] ** 2).sum() )

    if plotfile is not None:
        fig = pyplot.figure( figsize=(6, 6), tight_layout=True )
        ax = fig.add_subplot( 1, 1, 1 )
        ax.set_title( f"Position update for {obj.name}" )
        ax.set_xlabel( "Δra (arcsec)" )
        ax.set_ylabel( "Δdec (arcsec)" )
        df = pandas.DataFrame( { 'band': bands[wgood], 'ra': ras[wgood], 'dec': decs[wgood], 'sn': sns[wgood] } )
        for i, band in enumerate( df['band'].unique() ):
            subdf = df[ df['band'] == band ]
            dra = ( subdf['ra'] - obj.ra ) * 3600. / math.cos( obj.dec * math.pi / 180. )
            ddec = ( subdf['dec'] - obj.dec ) * 3600.
            size = 2 + ( subdf['sn'] - 3 ) * 2
            size[ size>50 ] = 50
            size[ size<2 ] = 2
            ax.scatter( dra, ddec, s=size, marker='o', color=colors[i], label=band )
        ax.plot( 0., 0., linestyle='None', marker='x', markersize=12, mew=3, color='cyan', label='Original Position' )
        dra = ( meanra - obj.ra ) * 3600. / math.cos( obj.dec * math.pi / 180. )
        ddec = ( meandec - obj.dec ) * 3600.
        ax.plot( dra, ddec, linestyle='None', marker='+', markersize=12, mew=3,
                 color='magenta', label='Mean Position' )
        ax.legend()
        fig.savefig( plotfile )
        pyplot.close( fig )

    if update:
        with db.DB.get( curdb ) as dbo:
            obj = db.Object.get( obj.id, curdb=dbo )
            oldobj = db.OldObjectData( object_id=obj.id,
                                       ra=obj.ra,
                                       dec=obj.dec,
                                       t0=obj.t0,
                                       z=obj.z,
                                       classification_id=obj.classification_id,
                                       confidence=obj.confidence,
                                       othernames=obj.othernames )
            dbo.db.add( oldobj )
            obj.ra = meanra
            obj.dec = meandec
            dbo.db.commit()
            logger.info( f"Position updated from [{oldobj.ra:.6f}, {oldobj.dec:.6f}] "
                         f"to [{obj.ra:.6f}, {obj.dec:.6f}]" )
        
    return meanra, meandec

# ======================================================================

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    parser = argparse.ArgumentParser( description="Find a new mean RA/Dec for a supernova from database photometry",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( "object", help="Name of object" )
    parser.add_argument( "-p", "--plotfile", default=None,
                         help=( "Name of file to write a diagnostic plot to.  (Extension must be something that "
                                "matplotlib knows how to handle with Figure.save().)" ) )
    parser.add_argument( "--update-position", default=False, action='store_true',
                         help="Update object in database with new RA and Dec" )
    args = parser.parse_args()

    obj = db.Object.get_by_name( args.object )
    if obj is None:
        raise ValueError( f"Unknown object {args.object}" )
    meanra, meandec = recenter( obj, update=args.update_position, plotfile=args.plotfile, logger=logger )
    
    print( f"Original position: {obj.ra:.6f} , {obj.dec:.6f} degrees" )
    print( f"Mean photometry position: {meanra:.6f} , {meandec:.6f} degrees" )
    ddec = ( meandec - obj.dec ) * 3600.
    dra = ( meanra - obj.ra ) * 3600 / math.cos( obj.dec * math.pi / 180. )
    print( f"Difference: {dra:.3f} , {ddec:.3f} arcsec" )

# ======================================================================

if __name__ == "__main__":
    main()
