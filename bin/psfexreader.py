import sys
import math
import logging
import argparse
import pathlib
import numpy
import numpy.random

from astropy.io import fits
import astropy.io.votable

class PSFExReader:
    
    def __init__( self, psfexfile, logger=logging.getLogger("main") ):
        self.logger = logger
        psfexfile = pathlib.Path( psfexfile )
        
        with fits.open( psfexfile, memmap=False ) as psf:
            if ( psf[1].header['POLNAXIS'] != 2 ):
                raise ValueError( f'Unexpected value of POLNAXIS = {psf[1].header["POLNAXIS"]}' )
            if ( psf[1].header['POLNGRP'] != 1 ):
                raise ValueError( f'Unexpected value of POLNGRP = {psf[1].header["POLNGRP"]}' )
            if ( psf[1].header['POLNAME1'] != "X_IMAGE" ) or ( psf[1].header['POLNAME2'] != "Y_IMAGE" ):
                raise ValueError( f'Unexpected values of POLNAME1 ("{psf[1].header["POLNAME1"]}") '
                                  f'and/or POLNAME2 ("{psf[1].header["POLNAME2"]}")' )
            self.x0 = float( psf[1].header['POLZERO1'] ) - 1
            self.xsc = float( psf[1].header['POLSCAL1'] )
            self.y0 = float( psf[1].header['POLZERO2'] ) - 1
            self.ysc = float( psf[1].header['POLSCAL2'] )

            self.psfsamp = float( psf[1].header['PSF_SAMP'] )

            self.psforder = int( psf[1].header['POLDEG1'] )
            nparam = ( self.psforder - numpy.arange( 0, self.psforder+1, dtype=int ) + 1 ).sum()
            if psf[1].data[0][0].shape[0] != nparam:
                raise ValueError( f'Unexpected shape {psf[1].data[0][0].shape} of psf basis for '
                                  f'order={self.psforder}; expected ({nparam}, :, :)' )

            self.psfdata = psf[1].data[0][0]

        self.psfwid = self.psfdata.shape[1]
        if ( self.psfwid % 2 ) == 0:
            raise ValueError( f'Even psf width {self.psfwid} surprises me.' )
        if self.psfdata.shape[2] != self.psfwid:
            raise ValueError( f'Non-square psf ( {self.psfwid} × {self.psfdata.shape[2]} ) surprises me.' )
        # self.psfoff = int( math.floor( self.psfwid/2 ) + 0.5 )

        self.psfdex1d = numpy.arange( -(self.psfwid//2), self.psfwid//2+1, dtype=int )
        # self.psfdex = numpy.meshgrid( psfdex1d, psfdex1d )

        self.stampwid = int( math.floor( self.psfsamp * self.psfwid ) + 0.5 )
        if ( self.stampwid % 2 ) == 0:
            self.logger.warning( f'Stampwid came out even ({self.stampwid}), subtracting ' )
            self.stampwid -= 1

        # Read the .psf.xml file

        psfxmlfile = psfexfile.parent / f"{psfexfile.name}.xml"
        if not psfxmlfile.is_file():
            logger.warning( f"Can't find file {psfxmlfile.name}, not reading it" )
            self.psfstats = None
        else:
            psfxml = astropy.io.votable.parse( psfxmlfile )
            self.psfstats = psfxml.get_table_by_index(1)
            
    def get_resampled_psf( self, x, y, dtype=numpy.float64 ):
        """Return an image fragment with the PSF at the sampling PSFEx made it at.

        x and y are the positions on the image the PSF was extracted from where the
        PSF should be evaluated (since it's spatially variable).  They are
        photutils coordinates

        If norm is True, normalize the array so the sum is 1.

        """
        xc = int( math.floor(x + 0.5) )
        yc = int( math.floor(y + 0.5) )

        psfbase = numpy.zeros_like( self.psfdata[0,:,:] )
        off = 0
        # THINK ABOUT THE ORDER
        #  I think psfex does it in the order 1, x, x², y, yx, y²
        # I do NOT add or subtract 1 to the position to deal
        #  with the 1-offset vs 0-offset issue, since I took
        #  care of that in the constructor when setting
        #  self.x0- and self.y0
        # numpy vs. FITS coordinate ording doesn't come into it
        #  here, since we're not indexing into the image
        #  Did I do this right?  I think so.
        for j in range( self.psforder+1 ) :
            for i in range( self.psforder+1-j ):
                psfbase += self.psfdata[off] * (
                    ( (x - self.x0) / self.xsc )**i *
                    ( (y - self.y0) / self.ysc )**j
                )
                off += 1

        # # But wait... I think psfex is ordering the basis
        # #   states opposite from how FITS does.  I am
        # #  confused.
        # psfbase = psfbase.transpose().copy()
        # No, that was wrong
        return psfbase
                

    def getclip( self, x, y, flux, clipx0=None, clipy0=None, clipwid=None,
                 norm=True, noisy=False, gain=1., rng=None, dtype=numpy.float64 ):
        """Get a image clip with the psf.

        x and y are the postion on the image this psf came from where
        the psf should be evaluated (since it varies with position).
        They are photutils coordinates; that is, they are backwards from
        numpy coordinates (so the pixel at x, y is image[y,x]), but are
        0-offset (so the lower-left pixel is 0, 0).

        clipwid is the width of the clip to return.  By default, this is
        the stampwid of the PSF (and that's usually what you want).
        
        clipx0, clipy0 are the (0-offset) coordinates of the pixel on
        the image that corresponds to pixel (0, 0) of the stamp.  By
        default, these are floor( (x|y) + 0.5) - clipwid // 2
        
        If norm is True, normalize the clip so that it sums to flux
        (before adding noise if any).

        If noisy is True, will also scatter the pixel values using
        Poisson statistics, assuming gain e-/adu.  (Get this from the
        image header of the exposure source.)  If you want to pre-seed
        the rng (for reproducability), make a numpy.random.Generator
        (e.g. with numpy.default_rng) and give it the seed you want;
        pass that rng in as rng.

        dtype is the type of the returned clip.  (It will be a 2d array.)

        Returns a 2d numpy array whose size is determined by the psfex
        parameters.  It is sampled at the pixel scale of the image the
        psf came from.

        """

        psfbase = self.get_resampled_psf( x, y )

        xc = int( math.floor(x + 0.5) )
        yc = int( math.floor(y + 0.5) )

        clipwid = self.stampwid if clipwid is None else clipwid
        
        clip = numpy.empty( ( clipwid, clipwid ), dtype=dtype )
        if clipx0 is None:
            xmin = xc - clipwid // 2
            xmax = xc + clipwid // 2 + 1
        else:
            xmin = clipx0
            xmax = clipx0 + clipwid
        if clipy0 is None:
            ymin = yc - clipwid // 2
            ymax = yc + clipwid // 2 + 1
        else:
            ymin = clipy0
            ymax = clipy0 + clipwid
        for xi in range( xmin, xmax ):
            for yi in range( ymin, ymax ):
                xsincarg = self.psfdex1d - (xi-x) / self.psfsamp
                xsincvals = numpy.sinc( xsincarg ) * numpy.sinc( xsincarg/4. )
                xsincvals[ ( xsincarg > 4 ) | ( xsincarg < -4 ) ] = 0
                ysincarg = self.psfdex1d - (yi-y) / self.psfsamp
                ysincvals = numpy.sinc( ysincarg ) * numpy.sinc( ysincarg/4. )
                ysincvals[ ( ysincarg > 4 ) | ( ysincarg < -4 ) ] = 0
                clip[ yi-ymin, xi-xmin ] = ( xsincvals[numpy.newaxis, :]
                                             * ysincvals[:, numpy.newaxis]
                                             * psfbase ).sum()

        if norm:
            clip /= clip.sum()

        clip *= flux
            
        if noisy:
            if rng is None:
                rng = numpy.random.default_rng()
            sig = numpy.zeros_like( clip )
            sig[ clip > 0 ] = numpy.sqrt( clip[ clip > 0 ] / gain )
            clip = rng.normal( clip, sig )

        return clip
            
    def add_psf_to_image( self, image, x, y, flux, norm=True, noisy=False, weight=None, gain=1., rng=None ):
        """Add a psf with indicated flux to the 2d image.

        image : a 2d numpy array

        x, y : position of the PSF

        flux : flux of the PSF in ADU

        weight : a 2d numpy array with inverse variances.  If
           noisy=True, then shot noise will be added to this image,
           assuming that if adding flux f to one pixel, the uncertainty
           is sqrt(f/gain)

        shape of image should match the shape of image the psf was extracted from

        For documentation on x, y, noisy, gain, and rng see PSFExReader.clip

        """

        if ( x < 0 ) or ( x >= image.shape[1] ) or ( y < 0 ) or ( y >= image.shape[0] ):
            self.logger.warn( "Center of psf to be added to image is off of edge of image" )
        
        xc = int( math.floor(x + 0.5) )
        yc = int( math.floor(y + 0.5) )
        clip = self.getclip( x, y, flux, norm=norm, noisy=noisy, gain=gain, rng=rng )

        xmin = xc - self.stampwid // 2
        x0 = 0
        if xmin < 0:
            x0 = -xmin
            xmin = 0
        xmax = xc + self.stampwid // 2 + 1
        x1 = self.stampwid
        if xmax > image.shape[1]:
            x1 -= xmax - image.shape[1]
            xmax = image.shape[1]
        ymin = yc - self.stampwid // 2
        y0 = 0
        if ymin < 0:
            y0 = -ymin
            ymin = 0
        ymax = yc + self.stampwid //2 + 1
        y1 = self.stampwid
        if ymax > image.shape[0]:
            y1 -= ymax - image.shape[0]
            ymax = image.shape[0]

        image[ ymin:ymax, xmin:xmax ] += clip[ y0:y1, x0:x1 ]
        if noisy and weight is not None:
            weight[ ymin:ymax, xmin:xmax ] = ( 1. / ( ( 1. / weight[ ymin:ymax, xmin:xmax  ] ) +
                                                      ( clip[ y0:y1, x0:x1 ] / gain )
                                                     )
                                              )

    def get_psf_palette( self, spacing=200, xsize=1024, ysize=1024, x0=None, y0=None, image=None ):
        """Return a 2d array with a grid of PSFs rendered at specified spacing.

        spacing - pixels between PSF centeres in each of x and y
        xsize - width (second numpy index) of the image; ignored if image is not None
        ysize - height (first numpy index) of the image; ignored if image is not None
        image - image file to read to get x and y; pass the path to the image that the PSF was
          originally extracted from
        
        Returns a 2d numpy array.

        """
        if image is not None:
            with fits.open( image, memmap="False" ) as hdul:
                nx = hdul[0].header['NAXIS1']
                ny = hdul[0].header['NAXIS2']
        else:
            nx = xsize
            ny = ysize

        palette = numpy.zeros( ( ny, nx ), dtype=numpy.float32 )
        x0 = ( nx - ( spacing * ( nx // spacing ) ) ) / 2. if x0 is None else x0
        origy0 = y0
        while x0 < nx:
            self.logger.debug( f"Rendering psfs at x={x0:.2f}..." )
            y0 = ( ny - ( spacing * ( ny // spacing ) ) ) / 2. if origy0 is None else origy0
            while y0 < ny:
                # self.logger.debug( f"Rendering psf at ( {x0:.2f}, {y0:.2f} )" )
                self.add_psf_to_image( palette, x0, y0, 1., True, False )
                y0 += spacing
            x0 += spacing

        return palette
        
        
# ======================================================================

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    parser = argparse.ArgumentParser( description="Render one or more psfs",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )

    parser.add_argument( "psfexfile", help="Name of the .psf file produced by PSFEx" )
    parser.add_argument( "-r", "--resampled-psf", default=None,
                         help=( "Write a FITS file with this name that has the resampled PSF at the "
                                "sampling PSFEx chose." ) )
    parser.add_argument( "-c", "--clip", default=None,
                         help=( "Write a thumbnail of the PSF at (x, y) at image resolution" ) )
    parser.add_argument( "-x", type=float, default=1024,
                         help=( "Position on FITS image the PSF was extracted on to evalute the resampled PSF "
                                "(for spatially variable PSFs).  This is in photutils coordinates, so "
                                "the center of the lower-left pixel is (0,0) and 2d numpy arrays are "
                                "indexed [y,x]." ) )
    parser.add_argument( "-y", type=float, default=1024 )
    parser.add_argument( "-p", "--palette", default=None,
                         help=( "Write a \"palette\" file with this name with a PSF rendered at regular intervals on "
                                "the image, at the pixel scale of the image the PSF was extracted from." ) )
    parser.add_argument( "-i", "--imagefile", default=None,
                         help=( "The name of the FITS file the PSF was extracted from; used only to determine "
                                "the size of the PSF palette written." ) )
    parser.add_argument( "--xsize", type=int, default=2048,
                         help=( "The width of the PSF palette written; ignored if --imagefile is given" ) )
    parser.add_argument( "--ysize", type=int, default=2048,
                         help=( "The height of hte PSF palette written; ignored if --imagefile is given" ) )
    parser.add_argument( "-s", "--spacing", type=float, default=200,
                         help=( "The spacing between the rendered PSF on the palette image" ) )
    parser.add_argument( "-v", "--verbose", action='store_true', default=False,
                         help="Show debug logging information." )
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel( logging.DEBUG )

    psfex = PSFExReader( args.psfexfile, logger=logger )

    if args.resampled_psf is not None:
        psfbase = psfex.get_resampled_psf( args.x, args.y, dtype=numpy.float32 )
        fits.writeto( args.resampled_psf, psfbase, overwrite=True )
        logger.info( f"Resampled PSF image evaluted at image coordinates ({args.x},{args.y}) "
                     f"written to {args.resampled_psf}" )

    if args.clip is not None:
        clip = psfex.getclip( args.x, args.y, 1., norm=False, noisy=False, dtype=numpy.float32 )
        fits.writeto( args.clip, clip, overwrite=True )
        logger.info( f"PSF thumbnail centered on image coordinates ({args.x},{args.y}) written to {args.clip}" )
        
    if args.palette is not None:
        palette = psfex.get_psf_palette( spacing=args.spacing, xsize=args.x, ysize=args.y, image=args.imagefile )
        fits.writeto( args.palette, palette, overwrite=True )
        logger.info( f"PSF palette written to {args.resampled_psf}" )

    print( f"The PSF was sampled at {psfex.psfsamp:.5f}" )
    print( f"The resampled PSF image size is {psfex.psfdata.shape[2]} × {psfex.psfdata.shape[1]}" )
    print( f"Image-resolution stamp width is {psfex.stampwid}" )
    print( f"(x0, y0) = {psfex.x0:.2f}, {psfex.y0:.2f}" )
    print( f"(xsc, ysc) = {psfex.xsc:.2f}, {psfex.ysc:.2f}" )
    print( f"Spatial polynomial degree: {psfex.psforder}" )

# ======================================================================

if __name__ == "__main__":
    main()
