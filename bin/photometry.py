import pathlib
import logging
import numpy

import astropy.coordinates
import astropy.units
from astropy.io import fits

import config
import db
import exposuresource
import bijaouisky
import sexsky

class Photomotor:
    """Photometry doer thingy.

    Sub classes must implement _measure; see template below.  Do not confuse _measure with measure.

    """

    # ======================================================================-

    @classmethod
    def get( cls, dbname, expsource, image, curdb=None, logger=logging.getLogger("main") ):
        """Get a Photomtor object.

        dbname -- type of photometry.  Currently defined are:
          apphot1 : aperture photometry in an aperture of r=1FWHM, calculating aperture corrections in 5FWHM
          psfphot1 : PSF-fitting using photutils.psf, defined in psfphot.py
          psfphot2 : PSF-fitting using cc_code/libsrc/psffit.cc, defined in psfphot2.py
        expsource -- either the name, or the exposuresource.ExposureSource object for the exposure source for the image
        image -- either the basename, or the db.Image object for the image we'll do photometry on
        curdb -- a db.DB object (optional)
        logging -- a logging.logger object (optional)

        """
        if dbname == 'apphot1':
            from apphot import ApPhot
            return ApPhot( dbname, expsource, image, default_radius=1.0, default_radius_unit='fwhm', logger=logger )
        elif dbname == 'psfphot1':
            raise RuntimeError( "psfphot1 is currently broken and needs to be fixed for updated _measure return" )
            from psfphot import PSFPhot
            return PSFPhot(dbname, expsource, image, logger=logger)
        elif dbname == 'psfphot2':
            from psfphot_psfex import PSFPhotPSFEx
            return PSFPhotPSFEx( dbname, expsource, image, logger=logger )
        else:
            raise ValueError( f"Unknown photometry method {dbname}" )


    # ======================================================================

    def __init__( self, dbname, expsource, image, skyalg='sextractor',
                  datadir=None, curdb=None, logger=logging.getLogger("main") ):
        """Don't call this directly, call Photometor.get()"""
        self.logger = logger
        self.dbname = dbname
        self.expsource = expsource
        self.image = image
        self.dbid = None

        if datadir is None:
            self.datadir = config.Config.get().value("datadirs")[0]
            self.datadir = pathlib.Path( self.datadir )
        else:
            raise RuntimeError( "Don't use datadir, I'm afraid of it." )

        self.skyalg = skyalg

        if ( self.expsource is not None ) and ( not isinstance( self.expsource, exposuresource.ExposureSource ) ):
            self.expsource = ExposureSoruce.get( self.expsource )

        with db.DB.get(curdb) as curdb:
            pms = curdb.db.query( db.PhotMethod ).filter( db.PhotMethod.name==dbname ).all()
            if pms is None:
                raise RuntimeError( f"Unknown photometry method {dbname}" )
            if len(pms) > 1:
                raise RuntimeError( f"Photometry method {dbname} is multiply defined in the database; "
                                    f"this should never happen.  Panic." )
            self.dbid = pms[0].id

            if isinstance( self.image, db.Image ):
                # SQL Alchemy drives me a little nuts.  It assumes that
                # (a) you will never interact with the database except through it, nad
                # (b) that you're happy to just keep a database connection open constantly
                # Both of these assumptions are wrong for me.
                self.image = curdb.db.merge( self.image )
                curdb.db.refresh( self.image )
            else:
                imgobj = db.Image.get_by_basename( self.image, curdb=curdb )
                if imgobj is None:
                    raise ValueError( f'Unknown image {self.image}' )
                self.image = imgobj
                if self.expsource is None:
                    if self.image.exposure_id is not None:
                        dbexps = self.image.exposure.exposurcesource
                        self.expsource = exposuresource.ExposureSource.get( dbexps.name, dbexps.secondary )

            if ( self.expsource is not None ) and ( self.image.exposure is not None ):
                if self.expsource.id != self.image.exposure.exposuresource.id:
                    raise RuntimeError( "Image exposure source doesn't match given exposure source." )

    # ======================================================================

    @property
    def ispsf( self ):
        """True if this is a psf-fitting or psf-weighting photometry method"""
        return self._ispsf

    @property
    def isap( self ):
        """True if this is an aperture photometry method"""
        return self._isap

    # ======================================================================

    def measure( self, x, y, ra=None, dec=None, recenter=False, imagedata=None, weightdata=None, maskdata=None,
                  tiny=1e-10, curdb=None, **kwargs ):
        """Measure photometry on the image at either 0-offset pixel (x,y), or at sky coordinate (ra,dec)

        x, y -- position on the image *in photutils coordinates*.  That
          is, the center of the lower-left pixel is (0,0) (so offset by
          1 from sextractor/FITS coordinates), and 2d numpy arrays are
          indexed by y, x
        ra, dec -- *ignored if x and y are not None*  Use image WCS to convert these to x and y
        recenter -- If True, the photometry method *might* recenter the aperture position; if False,
          force photometry at x,y
        imagedata -- The image data on which to actually do the photometry.  By default, uses the image
          passed to the object constructor.  Use this to actually measure a different image that's aligned
          with the constructor image (e.g. a subtraction, something with fakes added).  **This image
          must be sky subtracted.**
        weightdata, maskdata -- must pass these if you pass imagedata
        tiny -- weights that are <=0 will be set to this value before calculating
          noises from 1/sqrt(weight) to avoid inf and nan
        curdb -- a db.DB object, or None
        **kwargs -- additional keyword arguments are passed on to the subclass

        Returns phot, x, y, covar, info

        phot, x, and y are numpy arrays if x and y were numpy arrays,
        otherwise they are floats.  The measured photometry in ADU is
        phot±dphot (and is ideally a full-PSF value if photometry
        subclasses are defined properly).  x and y are the positions
        where the apertures were actually measured.

        covar is either a (3,3) numpy array (if input x and y were
        scalars), or a (x.,shape[0], 3, 3) numpy array, holding the
        covariances phot, x, and y.

        info is a dictionary of additional informaton that is method-specific

        """
        with db.DB.get(curdb) as curdb:
            if ( x is None ) != ( y is None ):
                raise ValueError( "Either both x and y must be None, or both must be specified" )
            if x is None:
                if ( ra is None ) or ( dec is None ):
                    raise ValueError( "Must give either (x,y) or (ra,dec)" )
                wcs = db.WCS.get_astropy_wcs_for_image( self.image, curdb=curdb )

                sc = astropy.coordinates.SkyCoord( ra, dec, frame='icrs', unit=astropy.units.deg )
                x, y = wcs.world_to_pixel( sc )

            maskedim, noisedata, boolmask = self._load_data( imagedata, weightdata, maskdata, tiny )

            return self._measure( x, y, maskedim, noisedata, boolmask, recenter=recenter, tiny=tiny, **kwargs )


    # ======================================================================
    # Returns imagedata, weightdata, maskdata

    def _measure( self, x, y, maskedimage, noisedata, boolmask, recenter=False, tiny=1e-10 ):
        """The method in all subclasses that does an actual measurement.

        x, y -- position on the image *in photutils coordinates*.  That
          is, the center of the lower-left pixel is (0,0) (so offset by
          1 from sextractor/FITS coordinates), and 2d numpy arrays are
          indexed by y, x
        maskedimage -- A numpy.ma.MaskedArray of the image data on which to do the measurement
        noisedata -- A σ image (so, sqrt(1/weight)) corresponding to imagedata
        boolmask -- A boolean image where True = masked (bad) pixel
        recenter -- If True, the photometry method *might* recenter the aperture position; if False,
          force photometry at x,y
        tiny -- weights that are <=0 will be set to this value before calculating
          noises from 1/sqrt(weight) to avoid inf and nan
        curdb -- a db.DB object, or None

        Subclasses may define additional keyword arguments.

        Returns phot, x, y, covar, info

        These are the same return values as from Phtomotor.measure

        """
        raise NotImplementedError( "Subclasses of Photomotor must implement _measure" )

    # ======================================================================

    def _load_data( self, imagedata, weightdata, maskdata, tiny ):
        """Makes sure we have the data we need

        If any of imagedata, weightdata, or maskdata is None, will read
        them from the filesystem or archive for the image object given
        to the ApPhot constructor.

        imagedata — If None, will read the image from the database.  If not None, must be the
           same size and aligned with the image in the databse, and ***must be sky subtracted***.
        weightdata — (same)
        maskdata — (same)

        If images are read from the databasem, the image will be sky
        subtracted if necessary, so that the returned maskedimage is sky
        subtractded.

        Pixels <= in weightdata will be replaced with tiny (to avoid inf and nan).

        Returns maskedimage, noisedata, boolmask.

        """

        if imagedata is not None:
            self.logger.warning( "Assuming passed image data is sky subtracted." )

        mustsubsky = False

        if ( imagedata is None ) or ( weightdata is None ) or ( maskdata is None ):
            if self.expsource is None:
                raise RuntimeError( "ExposureSource required if you don't give all of (image|mask|weight)data" )

            self.expsource.ensure_image_local( self.image.basename, logger=self.logger )
            impath = self.datadir / self.expsource.relpath( f"{self.image.basename}.fits" )
            wtpath = self.datadir / self.expsource.relpath( f"{self.image.basename}.weight.fits" )
            mspath = self.datadir / self.expsource.relpath( f"{self.image.basename}.mask.fits" )

            if imagedata is None:
                with fits.open( impath ) as hdu:
                    imagedata = hdu[0].data
                if not self.image.isskysub:
                    mustsubsky = True
            if weightdata is None:
                with fits.open( wtpath ) as hdu:
                    weightdata = hdu[0].data
            if maskdata is None:
                with fits.open( mspath ) as hdu:
                    maskdata = hdu[0].data

        if ( maskdata.shape != imagedata.shape ) or ( weightdata.shape != imagedata.shape ):
            raise RuntimeError( "weight and mask data shape must match image data shape" )

        if mustsubsky:
            if self.skyalg == 'sextractor':
                skyim, σ = sexsky.sexsky( imagedata, maskdata, logger=self.logger )
                self.logger.info( f"{self.image.basename} sexsky gave |s|={numpy.median(skyim)}, σ={σ}" )
                imagedata -= skyim
            elif self.skyalg == 'bijaoui':
                sky, a, σ, s = bijaouisky.estimate_smooth_sky( imagedata, maskdata, logger=self.logger )
                self.logger.info( f"{self.image.basename} bijaouisky gave a={a}, σ={σ}, s={σ}" )
                imagedata -= sky
            else:
                raise ValueError( f"Unknown sky subtraction method {self.skyalg}" )

        boolmask = numpy.array( maskdata, dtype=bool )
        maskdata = None
        weightdata[ weightdata <= 0 ] = tiny
        noisedata = numpy.sqrt( 1. / weightdata )
        weightdata = None
        maskedimage = numpy.ma.MaskedArray( imagedata, boolmask )
        imagedata = None

        return maskedimage, noisedata, boolmask


