import sys
import math
import pathlib
import logging
import numbers
import argparse
import numpy
import numpy.ma

import astropy.units
from astropy.io import fits
from astropy.wcs import WCS
from photutils.psf import EPSFModel, EPSFFitter, EPSFStar, EPSFStars
import matplotlib.pyplot as plt

from matplotlib import pyplot
from PIL import Image

import db
import config
import bijaouisky
import sexsky
from psfexreader import PSFExReader
from exposuresource import ExposureSource
from photometry import Photomotor


class PSFPhot(Photomotor):
    """PSF photometry using photutils; requires a sufficiently oversampled PSF."""

    _ispsf = True
    _isap = False
    default_oversamp = 4
    default_clipsize = 25
    RETURNS = 4 # Number of returns for the _measure function
    
    # ======================================================================
    def __init__( self, dbname, expsource, image,
                  skyalg='sextractor', datadir=None, curdb=None,
                 logger=logging.getLogger("main"), *args, **kwargs ):
        """Normally, get a PSFPhot object from photometry.Photomotor.get() 
        rather than constructing PSFPhot directly.

        expsource -- An ExposureSource object (from exposuresource.py),
           or the name of one.  (Can be None for some uses of ApPhot, but not all.)
        image -- A db.Image object, or the basename of one
        skyalg -- sky subtraction algorithm to use if db image isn't sky subtracted.
           'sextractor' or 'bijaoui'
        datadir -- (Optional) the base data dir to read/write from
          (default: datadirs[0] from config)
        curdb -- a db.DB object, used only during the constructor (not saved)
        logging -- (Optional) A logging.logger object

        """

        super().__init__( dbname, expsource, image, *args, **kwargs )
        logger.info("Initializing PSFPhot object")
        
        ### TODO: check the oversampling and if it's not at least 4 then we should re-run
        self.skyalg = skyalg
        self.datadir = datadir
        self.catdata = None
        
        psf_file = expsource.local_path(image.basename + ".psf")
        
        self.psf_reader = PSFExReader(psf_file)
        self.logger = logger
        
        if self.datadir is None:
            self.datadir = config.Config.get().value("datadirs")[0]
            self.datadir = pathlib.Path( self.datadir )
        else:
            raise RuntimeError( "Don't use datadir, I'm afraid of it." )
    
    def _measure( self, x, y, maskedimage, noisedata, boolmask, recenter=False, tiny=1e-10,
                 clipsize=None, oversamp=None, resids_out=False ):
        """ PSF Photometry """
        # Wrangle input variables
        if clipsize is None: clipsize = self.default_clipsize
        if oversamp is None: oversamp = self.default_oversamp
        if clipsize % 2 == 0:
            raise ValueError(f"The clip size cannot be even! clipsize: {clipsize}")
        
        self.logger.info("Fitting PSF, estimating source flux")
        
        # Check for passed arrays of center locations
        if isinstance(x, list) or isinstance(y, list):
            # print(f'x: {x}')
            # print(f'y: {y}')
            if not isinstance(x, list) or not isinstance(y, list):
                raise ValueError("x and y must either both be lists or both be floats.")
            if len(x) != len(y):
                raise ValueError("Shapes of x and y arrays do not match.")
            
            # TODO: find a way to not hardcode the number of returns
            N = len(x)
            results = numpy.empty([N,self.RETURNS])
            cov = []
            
            # Recursively calculate the fake values
            for i in range(N):
                # print(f'xi, yi: {x[i]}, {y[i]}')
                result = self._measure(x[i], y[i], maskedimage, noisedata, 
                                       boolmask, recenter=recenter, tiny=tiny)
                results[i] = result[:self.RETURNS]
                cov.append(result[self.RETURNS])
            
            # Split them up and return
            phot, dphot, newx, newy = results.T
            
            return phot, dphot, newx, newy, cov
        
        # Get PSF at location x,y from input file
        psf_data = self.psf_reader.get_resampled_psf(x, y)
        psf_data = psf_data * (oversamp**2) / numpy.sum(psf_data)
         
        ### TODO Emily: Remove hardcoded size values
        # Clip the image and mask around the point source
        hclip = int(clipsize / 2)
        int_x, int_y = int(x+.5), int(y+.5)
        clip = maskedimage[int_y-hclip:int_y+hclip+1, int_x-hclip:int_x+hclip+1]
        clipwt = 1/(noisedata[int_y-hclip:int_y+hclip+1, int_x-hclip:int_x+hclip+1])
        clipwt[clip.mask] = 0
        
        cutout_center = [x-int_x+hclip, y-int_y+hclip]
        origin = [int_x-hclip, int_y-hclip]
        
        # Get a guess for the flux
        flux_guess = 2*numpy.pi * clip[hclip, hclip] # TODO: Supposed to multiply by sigma squared
        
        # self.logger.info(f"x: {int_x}, y: {int_y}")
        # if int_x==25 and int_y==2087:
        #     import pdb; pdb.set_trace()
        
        if clip.mask.all():
            self.logger.warning(f"The clip is fully masked for position {int_x}, {int_y}. No fit possible.")
            return numpy.nan, numpy.nan, numpy.nan, numpy.nan, {'covariance': None}
        
        # Make EPSFStar object
        star = EPSFStar(clip, weights=clipwt, cutout_center=cutout_center, origin=origin)
        
        ### TODO Emily: Fix hardcoded oversampling here too
        # Make a photutils PSF model with the PSFEx data
        psfmodel = EPSFModel(psf_data, x_0=x, y_0=y, flux=1.0, oversampling=oversamp, normalize=False)
        if not recenter:
            psfmodel.x_0.fixed = True
            psfmodel.y_0.fixed = True
        
        # Make a fitter to take the model and point source to fit
        if clip.mask.any():
            self.logger.warning(f"The clip passed to the fitter is partially masked for position {int_x}, {int_y}! " \
                                "Possible edge-case shenanigans will ensue.")
        try:
            fitter = EPSFFitter()
            fitted = fitter(psfmodel, EPSFStars(star))
        except:
            self.logger.error(f"EPSF fitting failed for position {int_x}, {int_y}!!!")
            return numpy.nan, numpy.nan, numpy.nan, numpy.nan, {'covariance': None}
        
        # Get the fitted parameters
        phot = fitted.flux
        cov_matrix = fitter.fitter.fit_info.get('param_cov', None)
        
        if cov_matrix is None:
            raise ValueError("Fitter did not return a covariance matrix")
        
        flux_var = cov_matrix[0,0]
        
        if flux_var is None:
            raise ValueError("Invalid value for flux uncertainty")
        
        if resids_out:
            # Get an estimated image
            ### TODO: un-hardcode this
            xx, yy = numpy.arange(int_x-hclip, int_x+hclip+1), numpy.arange(int_y-hclip, int_y+hclip+1)
            xv, yv = numpy.meshgrid(xx, yy)
            est = psfmodel.evaluate(xv,yv,flux=fitted.flux, x_0=x, y_0=y)
            
            fig, ax = plt.subplots()
            
            # Write resid out to image
            resid = clip - est
            ax.imshow(resid, origin='lower', cmap='viridis')
            
            # Reduced chisq
            rchisq = numpy.sum((clipwt**2)*(resid**2))/(clipsize*clipsize)
            ax.annotate(f"Sum: {numpy.sum(resid):.2f}\nrchisq: {rchisq:.2f}", (3,3), 
                         color='w', fontsize=16)
            
            resid_dir = self.datadir.joinpath(self.expsource.topdirname+'/residuals')
            resid_dir.mkdir(parents=True, exist_ok=True)
            save_file = resid_dir.joinpath(f'{self.image.basename}.{int_x}_{int_y}_resid.png')
            fig.savefig(save_file, bbox_inches='tight')
            
            fig, ax = plt.subplots()
            ax.imshow(clip, origin='lower', cmap='viridis')
            save_file = resid_dir.joinpath(f'{self.image.basename}.{int_x}_{int_y}_clip.png')
            ax.annotate(f"Sum: {numpy.sum(clip):.2f}", (3,3), color='w', fontsize=16)
            fig.savefig(save_file, bbox_inches='tight')
            
#             clip_away = maskedimage[int_y-clipsize*2:int_y-clipsize, int_x-clipsize*2:int_x-clipsize]
#             noise_away = noisedata[int_y-clipsize*2:int_y-clipsize, int_x-clipsize*2:int_x-clipsize]
#             stddev = numpy.sqrt(numpy.sum(noise_away**2))
            
#             fig, ax = plt.subplots()
#             ax.imshow(clip_away, origin='lower', cmap='viridis')
#             save_file = resid_dir.joinpath(f'{self.image.basename}.{int_x}_{int_y}_box.png')
#             ax.annotate(f"Sum: {numpy.sum(clip_away):.2f}\nSum_unc: {stddev:.2f}",
#                         (3,3), color='w', fontsize=16)
#             fig.savefig(save_file, bbox_inches='tight')
            
            plt.close('all')
            
        
        dphot = numpy.sqrt(flux_var)
        
        newx, newy = fitted.center
        
        return phot, dphot, newx, newy, {'covariance': cov_matrix}
        
        
