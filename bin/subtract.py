#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import math
import logging
import pathlib
import uuid
import argparse
import shutil
import subprocess

import numpy

import sqlalchemy as sa

import astropy.io
from astropy.io import fits

import db
import config
from exposuresource import ExposureSource
import processing

class Subtractor:
    """Superclass of all subtraction algorithms.

    To use this, call sub=Subtractor.get(subalg, ...), where subalg is
    the name of the subtraction algorithm.  Then call sub.subtract() to
    actuall perform the subtraction.

    Subclasses must implement the following methods (see documentation below)
       _run_subtraction()
       refnorm (property) -- what to multiply the ref by in order to make it have the same zp as new/sub

    The list of files that subclasses must produces can be found under the
    documentation for the subtract() method; subclasses must add all
    other created files to the self._tmpfiles variable (unless the
    subclass cleans them up itself).

    To generate filenames, subclasses should call _outdir_filename(ext),
    where ext is the extention that shows up after the imagebasename.
    The *actual* filename will be {basename}.{subid}.{ext}, where
    {subid} is the database subtraction id (so that these things will be
    unique if a new subtraction is run on the same image).

    """

    # ======================================================================
    
    @classmethod
    def get( cls, subalg_name, expsource, image, reference, datadir=None, tmpdir=None,
             tiny=1e-10, logger=logging.getLogger("main"), curdb=None, **kwargs ):
        """Get a Subtractor object of the desired subclass; then call that object's subtraction method.

        subalg_name -- name of the subtraction algorithm desired.  Currently only 'alard1' is defined
        expsource -- the relevant exposuresource.ExposureSource object
        image -- either the db.Image object, or the basename of the image that is the search image
        reference -- etiher the db.Image object, or the basename of the image that is the reference
           *** the reference is assumed to be sky-subtracted *** (but the new may not be)
        datadir -- root of data directory; defaults to the first one in Config.get().value("datadirs")
        tmpdir -- a place to dump temporary files; defaults to an automatically defined directory under /tmp
        tiny -- weights that are <= 0 will be set to this so that 1/sqrt(weight) doesn't go nan or inf
        logger -- a logging.logger object
        curdb (optional) -- a db.DB object

        Additional keywords are specific to those algorithms, and will
        be passed along.  This is probably not a good idea, because the
        subalg_name should fully define the parameters of the
        subtraction.  So, probably, don't use any additional keyword
        arguments.

        """
        if subalg_name == 'alard1':
            from alard_hotpants import Alard1
            return Alard1( expsource, image, reference, datadir=datadir, tmpdir=tmpdir,
                           tiny=tiny, logger=logger, curdb=curdb, **kwargs )
        else:
            raise ValueError( f"Unknown subtraction algorithm {subalg_name}" )

        
    # ======================================================================
    
    def __init__( self, expsource, image, reference, datadir=None,
                  tmpdir=None, tiny=1e-10, obj=None, logger=logging.getLogger("main"), curdb=None ):
        """Don't call this directly, call Subtractor.get()

        expsource -- the relevant exposuresource.ExposureSource object
        image -- either the db.Image object, or the basename of the image that is the search image
        reference -- etiher the db.Image object, or the basename of the image that is the reference
        datadir -- root of data directory; defaults to the first one in Config.get().value("datadirs")
        tmpdir -- a place to dump temporary files; defaults to an automatically defined directory under /tmp
        tiny -- weights that are <= 0 will be set to this so that 1/sqrt(weight) doesn't go nan or inf
        obj -- an optional db.Object object; if here, will use this to choose the right ObjectReference
        logger -- a logging.logger object

        In addition to loading internal objects, will make sure that the
        image, weight, mask, and catalog exist on the local disk for both the image and the
        reference.  Sets internal variables (for use by subclasses) with these paths:
          self._imagepath
          self._weightpath
          self._maskpath
          self._catpath
          self._refimagepath
          self._refweightpath
          self._refmaskpath
          self._refcatpath
          self._remapreffile
          self._remaprefweight
          self._remaprefmask
          self._remaprefcat
          self._subfile
          self._subweight
          self._submask

        Also sets the variable self._outdir which is the path to the
        place where all files should be written.

        """
        
        self.expsource = expsource
        self.image = image
        self.refimg = reference
        self.datadir = datadir
        self.tmpdir = tmpdir
        self.tiny = tiny
        self.logger = logger
        self.subobj = None
        self.warpedrefmade = False
        
        self.subalg = None
        with db.DB.get(curdb) as curdb:
            q = curdb.db.query( db.SubAlgorithm ).filter( db.SubAlgorithm.name==self._subalg_name )
            if q.count() == 0:
                raise RuntimeError( f"Failed to find subtraction algorithm {self._subalg_name} in the database" )
            if q.count() > 1:
                raise RuntimeError( f"Subalg {self._subalg_name} is multiply defined in the database. "
                                    f"This really shouldn't happen.  Panic." )
            self.subalg = q.first()
            if self.subalg is None:
                raise RuntimeError( "This should never happen." )

            if self.datadir is None:
                self.datadir = config.Config.get().value("datadirs")[0]
                self.datadir = pathlib.Path( self.datadir )

            if self.tmpdir is None:
                self.tmpdir = f'/tmp/{str(uuid.uuid4())}'
                self.tmpdir = pathlib.Path( self.tmpdir )
            self.tmpdir.mkdir( exist_ok=True, parents=True )

            if not isinstance( self.image, db.Image ):
                imgobj = db.Image.get_by_basename( self.image, curdb=curdb )
                if imgobj is None:
                    raise ValueError( f'Unknown image {self.image}' )
                self.image = imgobj
            self.expsource.ensure_image_local( self.image.basename )
            self._imagepath = self.expsource.local_path( f'{self.image.basename}.fits' )
            self._weightpath = self.expsource.local_path( f'{self.image.basename}.weight.fits' )
            self._maskpath = self.expsource.local_path( f'{self.image.basename}.mask.fits' )
            self._catpath = self.expsource.get_file_from_archive( f'{self.image.basename}.cat' )
            
            if not isinstance( self.refimg, db.Image ):
                imgobj = db.Image.get_by_basename( self.refimg, curdb=curdb )
                if imgobj is None:
                    raise ValueError( f'Unknown reference image {self.refimg}' )
                self.refimg = imgobj
            self.expsource.ensure_image_local( self.refimg.basename )
            self._refimagepath = self.expsource.local_path( f'{self.refimg.basename}.fits' )
            self._refweightpath = self.expsource.local_path( f'{self.refimg.basename}.weight.fits' )
            self._refmaskpath = self.expsource.local_path( f'{self.refimg.basename}.mask.fits' )
            self._refcatpath = self.expsource.get_file_from_archive( f'{self.refimg.basename}.cat' )
            
            self._outdir = self.datadir / self.expsource.archivesubdir( self.image.basename )
                
            q = ( curdb.db.query( db.ObjectReference )
                 .filter( db.ObjectReference.image_id==self.refimg.id ) )
            if q.count() > 1:
                if obj is not None:
                    objrefs = []
                    for objref in q.all():
                        if objref.object_id == obj.id:
                            objrefs.append( objref )
                    if len( objrefs ) > 1:
                        raise RuntimeError( "Multiple ObjectReferences for ref image {self.refimage.basename} "
                                            "and object {obj.name}.  Panic." )
                    self.objref = objrefs[0]
                else:
                    raise RuntimeError( f"Multiple ObjectReferences found for your ref image {self.refimg.basename}; "
                                        "try passing an object with the obj keyword." )
            elif q.count() == 0:
                raise RuntimeError( f"Couldn't find ObjectReference for reference image {self.refimg.basename}" )
            else:
                self.objref = q[0]
                if ( obj is not None ) and ( self.objref.object_id != obj.id ):
                    logger.warning( f"Found ObjectReference for ref image {self.refimg.basename}, but it's for "
                                    f"object {self.objeref.object.name}, not your object {obj.name}; "
                                    f"using it anyway." )

        self._tmpfiles = []

    # =====================================================================

    def _set_associated_filenames( self ):
        self._remapreffile = self._outdir_filename( "remapref.fits" )
        self._remaprefweight = self._outdir_filename( "remapref.weight.fits" )
        self._remaprefmask = self._outdir_filename( "remapref.mask.fits" )
        self._remaprefcat = self._outdir_filename( "remapref.cat" )
        self._subfile = self._outdir_filename( "sub.fits" )
        self._subweight = self._outdir_filename( "sub.weight.fits" )
        self._submask = self._outdir_filename( "sub.mask.fits" )

    
    # =====================================================================

    def _outdir_filename( self, ext ):
        if self.subobj is None:
            raise RuntimeError( "Can't call _outdir_filename until self.subobj is not None." )
        return self._outdir / f"{self.image.basename}.{self.subobj.id}.{ext}"
    
    # =====================================================================
        
    def _get_existing_subtraction( self, searchtag='default', curdb=None ):
        with db.DB.get(curdb) as dbo:
            q = ( dbo.db.query( db.Subtraction )
                  .filter( db.Subtraction.image_id==self.image.id )
                  .filter( db.Subtraction.ref_id==self.refimg.id )
                  .filter( db.Subtraction.subalg_id==self.subalg.id )
                  .filter( db.Subtraction.iscomplete==True )
                  .join( db.SubtractionVersiontag, db.SubtractionVersiontag.subtraction_id==db.Subtraction.id )
                  .join( db.VersionTag, sa.and_( db.SubtractionVersiontag.versiontag_id==db.VersionTag.id,
                                                 db.VersionTag.name==searchtag ) )
                 )
            if q.count() == 0:
                return None
            self.subobj = q[0]
        self._set_associated_filenames()
            
        filesexist = True
        self.logger.info( f'Making sure image, mask, and weight for pre-existing subtraction are local' )
        self.expsource.ensure_image_local( self.image.basename, curdb=curdb, logger=self.logger )
        self.logger.info( 'Making sure image catalog file and subtraction files '
                          'for pre-existing subtraction are local' )
        for f in [ 'catpath', 'remapreffile', 'remaprefweight', 'remaprefmask', 'remaprefcat',
                   'subfile', 'subweight', 'submask' ]:
            try:
                newf = self.expsource.get_file_from_archive( getattr( self, f'_{f}' ).name )
                if not newf.is_file():
                    raise FileNotFoundError( "Didn't find {newf}" )
            except Exception as e:
                filesexist = False
                self.logger.error( f'Failed to find {f} [{getattr(self,f"_{f}").name}] on local disk, '
                                   f'or to pull down from archive.' )
                break
        if not filesexist:
            raise RuntimeError( "Subtraction exists but some files aren't there; I don't know how to cope." )
    
    # ======================================================================
    
    def _nuke_tempfiles( self ):
        for f in self._tmpfiles:
            if f.exists():
                f.unlink()
        self._tmpfiles = []

    # ======================================================================

    def sub_filepath( self, which ):
        """Return the file path that was or will be produced by this subtraction.

        which -- one of "remapref", "remaprefweight", "remaprefmask", "remaprefcat", "sub", "subweight", "submask"

        """
        if which == "remapref":
            return self._remapreffile
        elif which =="sub":
            return self._subfile
        else:
            return getattr( self, f"_{which}" )

    # ======================================================================
        
    def sub_filename( self, which ):
        """Return the filename (not path) that was or will be produced by this subtraction.

        which -- one of "remapref", "remaprefweight", "remaprefmask", "remaprefcat", "sub", "subweight", "submask"

        """
        return self.sub_filepath( which ).name
        
    # ======================================================================

    def all_archived_filenames( self ):
        """Return a list of all filenames (not paths) that might get pushed up 
        to the archive for this subtraction
    
        """
        return [ self.sub_filepath(i).name for i in [ "remapref", "remaprefweight", "remaprefmask", "remaprefcat",
                                                      "sub", "subweight", "submask" ] ]
        
        
    # ======================================================================

    def get_or_make_subtraction_object( self, searchtag='default', redo=False, only_load=False, curdb=None,
                                        logifexisting=False ):
        """Don't normally need to call this from the outside, it'll be called in subtract()"""
        
        with db.DB.get(curdb) as curdb:
            self._get_existing_subtraction( searchtag=searchtag, curdb=curdb )
            if self.subobj is not None:
                if redo:
                    raise RuntimeError( "A subtraction exists with the same algorithm, new, and ref. "
                                        "Currently, you have to manually and painfully delete it before "
                                        "redoing.  Good luck." )
                if logifexisting:
                    self.logger.info( f"Using existing subtraction for algorithm {self.subalg.name}, "
                                      f"image {self.image.basename}, reference {self.refimg.basename}, "
                                      f"versiontag {searchtag}" )
            else:
                if only_load:
                    self.logger.error( f"Could not find subtraction in the database for subalg {self.subalg.name}, "
                                       f"image {self.image.basename}, reference {self.refimg.basename}, "
                                       f"versiontag {searchtag}, and only_load was True; returning None." )
                    return None
                self.subobj = db.Subtraction( image_id=self.image.id, ref_id=self.refimg.id,
                                          subalg_id=self.subalg.id, refnorm=None )
                curdb.db.add( self.subobj )
                curdb.db.commit()
                self._set_associated_filenames()

        return self.subobj
    
    # ======================================================================
        
    def subtract( self, redo=False, searchtag='default',
                  tagdefault=False, tagdefaultifnodefault=True, taglatest=True, tags=[], newtags=False,
                  noarchive=False, only_load=False ):
        """Read the subtraction from the database/archive, or build it if it doesn't exist.

        redo -- If true, always rebuild the subtraction.  Maybe.  Currently broken.

        searchtag -- Search for this tag when trying to load an existing
          subtraction.  SUGGESTION: if searchtag is 'default', you probably
          want tagdefaultifnodefault=True.  If it's something else, you
          probably want that tag in the list you give as tags.

        tagdefault -- After running the subtraction, tag it in the
          database as the "default", removing that tag if any from
          existing subtractions for this search image; implies
          tagdefaultifnodefault (default: False)

        tagdefaultifnodefault -- After running the subtraction, if there
          are no subtractions for this search image taggaed as
          "default", then tag this as default.  If there already are
          subtractions tagged as the default, then *don't* tag this one
          as the default. (default: True)

        taglatest -- After running the subtraction, tag this as the
        latest for this search image (default: True)

        tags -- List of tag names to tag this subtraction as, removing
          those tags on any previous subtractions for this search image.
          Will ignore 'default' and 'latest' (use the other arguments
          for those).

        newtags -- Must be True if any "tags" don't already exist as
          tags.

        noarchive -- DO NOT USE THIS UNLESS YOU REALLY KNOW WHAT YOU'RE
          DOING.  Don't save the subtraction files to the archive.  The
          only reason you'd do this is you're testing, or if you know
          you're going to delete the subtraction database entry and
          files right away.

        only_load -- Only try to load the subtraction from the databse, don't
          actually perform a subtraction.

        NOTE that tags will be applied even if the subtraction already
        exists!  (So, this is the method you call to apply tags on
        existing subtractions.)

        Returns the db.Subtraction object.  Produces the followng files on disk in the relevant data directory,
        where "basename" is the basename of the new image, "subid" is the database id field for this
        subtraction:
          {basename}.{subid}.remapref.fits        -- reference image aligned with new, but *not* convolved
          {basename}.{subid}.remapref.weight.fits -- weight for remapref
          {basename}.{subid}.remapref.mask.fits   -- mask for remapref
          {basename}.{subid}.remapref.cat         -- sextractor catalog for remapref
          {basename}.{subid}.sub.fits             -- new minus convolved ref.  Assumed to be aligned with,
                                                     have same zeropoint, and have same psf as new.
          {basename}.{subid}.sub.weight.fits      -- weight for sub
          {basename}.{subid}.sub.mask.fits        -- mask for sub

        The sub image is scaled to the new/search image (so the new
        zeropoint can be used for all objects found on the subtraction;
        the ref/template image is not, but the "refnorm" field in the
        subtraction database table has the factor that the ref image
        must be multiplied by to scale it to the new.

        """

        with db.DB.get() as curdb:

            # Check for an existing subtraction with the same new, ref, and algorithm
            # ROB TODO : think about handling cases where iscomplete = False.
            # (Right now, that has to be handled manually.)

            self.get_or_make_subtraction_object( searchtag=searchtag, redo=redo, only_load=only_load, curdb=curdb,
                                                 logifexisting=True )

            # Make sure that we have all database tags that
            #  we are going to tag this subtraction with

            tags = set( tags )
            try :
                tags.remove( 'default' )
            except KeyError as ex:
                pass
            try:
                tags.remove( 'latest' )
            except KeyError as ex:
                pass
            if tagdefault or tagdefaultifnodefault:
                tags.add( 'default' )
            if taglatest:
                tags.add( 'latest' )
            tagobjs = list( curdb.db.query( db.VersionTag ).filter( db.VersionTag.name.in_( tags ) ).all() )
            existingtagnames = [ e.name for e in tagobjs ]
            addedtags = []
            for tag in tags:
                if tag not in existingtagnames:
                    addedtags.append( tag )
            if len( addedtags ) > 0:
                if not newtags:
                    raise ValueError( f"Unknown tags {','.join(addedtags)}" )
                for tag in addedtags:
                    newtag = db.VersionTag( name=tag )
                    curdb.db.add( newtag )
                    # I do lots of commits here rather than one at the
                    # end so I can be sure that the automatic newtag.id
                    # field is loaded (which I THINK happens with
                    # SQLAlchemy).  I'm not sure if this is actually
                    # necessary; SQLAlchmey is always a bit (at least!)
                    # mysterious if you really want to think about the
                    # actual tables and the SQL interface, which I do.
                    curdb.db.commit()
                    tagobjs.append( newtag )

            # Actually run the subtraction if necessary
            if not self.subobj.iscomplete:
                if not self.warpedrefmade:
                    self.align_ref_to_new( curdb=curdb )
                self._run_subtraction()
                mustarchive = True
            else:
                mustarchive = False
            
            # Push all resultant files up to the archive
            if mustarchive and not noarchive:
                self.logger.info( f"Uploading subtraction files to the archive..." )
                for f in [ 'remapreffile', 'remaprefweight', 'remaprefmask', 'remaprefcat',
                           'subfile', 'subweight', 'submask' ]:
                    localp = self.expsource.local_path( getattr( self, f'_{f}' ).name )
                    if localp is None:
                        raise FileNotFoundError( f"Failed to find the {f} for the subtraction!" )
                    if not self.expsource.upload_file_to_archive( localp.name, overwrite=True, logger=self.logger ):
                        raise RuntimeError( f"Failed to upload the {f} to the archive" )
                self.logger.info( f"...done uploading subtraction files to the archive." )
            else:
                self.logger.warning( "Not uploading subtraction files to archive." )

            # Mark the subtraction as complete in the database
            self.subobj.iscomplete = True
            curdb.db.commit()
            self._nuke_tempfiles()

        if self.subobj is None:
            raise RuntimeError( 'subobj is none after running subtraction.  This is bad.' )


        # Tag this subtraction, first removing the tags on existing
        # subtractions with the same search image (with special
        # handling for default)

        with db.DB.get() as curdb:
            yankdefault = None
            vtdels = set()
            for tag in tagobjs:
                curvt = ( curdb.db.query( db.SubtractionVersiontag )
                          .join( db.Subtraction, db.Subtraction.id==db.SubtractionVersiontag.subtraction_id )
                          .filter( db.Subtraction.image_id==self.image.id )
                          .filter( db.SubtractionVersiontag.versiontag_id==tag.id ) ).all()
                if len(curvt) == 0:
                    continue
                if len(curvt) > 1:
                    raise RuntimeError( "I am surprised." )
                curvt = curvt[0]
                if ( tag.name == 'default' ) and ( not tagdefault ):
                    yankdefault = tag
                else:
                    curdb.db.delete( curvt )
            if yankdefault is not None:
                tagobjs.remove( yankdefault )
            for tag in tagobjs:
                newsubvt = db.SubtractionVersiontag( subtraction_id=self.subobj.id, versiontag_id=tag.id )
                curdb.db.add( newsubvt )
            curdb.db.commit()

        return self.subobj

    # ======================================================================

    @property
    def refnorm( self ):
        """A factor that you multiply the reference image by in order to get it on the same zp as new.

        In general, will not be defined before subtract() is run.

        """
        raise NotImplementedError( "Subclasses of Subtractor must define refnorm" )

    # ======================================================================

    def clips( self, x, y, width ):
        """Get image clips at x, y

        x and y are photutils coordiantes, so are backwards from numpy coordinates

        returns newclip, refclip, subclip

        Each clip is a  width×width numpy array, centered around
        int(x+0.5), int(y+0.5).

        """
        if width % 2 == 0:
            raise ValueError( "Width must be odd" )
        x0 = int( math.floor( x + 0.5 ) )
        y0 = int( math.floor( y + 0.5 ) )

        newclip = numpy.zeros( ( width, width ), dtype=numpy.float32 )
        refclip = numpy.zeros( ( width, width ), dtype=numpy.float32 )
        subclip = numpy.zeros( ( width, width ), dtype=numpy.float32 )

        def ranges( data, x0, y0, width ):
            offx0 = 0
            offx1 = width
            offy0 = 0
            offy1 = width
            minx = x0 - width // 2
            maxx = x0 + width // 2 + 1
            miny = y0 - width // 2
            maxy = y0 + width // 2 + 1
            if ( minx < 0 ):
                offx0 = -minx
                minx = 0
            if ( maxx > data.shape[1] ):
                offx1 -= maxx - data.shape[1]
                maxx = data.shape[1]
            if ( miny < 0 ):
                offy0 = -miny
                miny = 0
            if ( maxy > data.shape[0] ):
                offy1 -= maxy - data.shape[0]
                maxy = data.shape[0]

            return offx0, offx1, offy0, offy1, minx, maxx, miny, maxy

        # This is a little gratuitous, since new, remapref, sub should all have the same size,
        #   so we don't have to check the limits every time, but, whatevs.
        for path, clip in zip( [ self.expsource.local_path_from_basename( self.image.basename ),
                                  self.sub_filepath( 'remapref' ),
                                  self.sub_filepath( 'sub' ) ],
                                [ newclip, refclip, subclip ] ):
            with fits.open( path ) as hdu:
                offx0, offx1, offy0, offy1, minx, maxx, miny, maxy = ranges( hdu[0].data, x0, y0, width )
                clip[ offy0:offy1, offx0:offx1 ] = hdu[0].data[ miny:maxy, minx:maxx ]

        newclip = self.expsource.north_up_east_left( newclip )
        refclip = self.expsource.north_up_east_left( refclip )
        subclip = self.expsource.north_up_east_left( subclip )

        return newclip, refclip, subclip
        
    # ======================================================================

    def align_ref_to_new( self, curdb=None ):
        """Align the reference image ot the search image using swarp.

        Don't normally call this, it'll be called by subtract()

        Will produce files in self._outdir:
           {basename}.{subid}.remapref.fits
           {basename}.{subid}.remapref.weight.fits
           {basename}.{subid}.remapref.mask.fits
           {basename}.{subid}.remapref.cat

        curdb -- the db.DB object (can't be None)

        This is a utility method that multiple subtraction methods might
        need.  Subraction methods may supply their own way of doing this if
        they wish, and do not have to call this method.

        """
        basename = self.image.basename

        # We need to make a version of the reference whose WCS is based on the WCS of the image
        # I'm using a sleazy trick with swarp and scamp here I learned from Danny Goldstein
        # First get a WCS solution for the new image that uses the ref's catalog as its
        # astrometric standard.  This will write a header new.head.
        #
        # Next, make a headerfile remapref.head that has the NAXIS keywords
        # from the new image, plus the stuff from new.head.
        #
        # Then, swarp on the reference image to remapref.head, and voila, the ref image is
        # now aligned with the new image.

        imagefile = self.expsource.local_path_from_basename( basename, filetype='image', logger=self.logger )
        if imagefile is None:
            raise RuntimeError( f"Can't find {basename}.fits" )
        imagehead = imagefile.parent / f'{self.image.basename}.head'
        reffile = self._outdir_filename( "tmpref.fits" )
        refcatfile = self._outdir_filename( "tmpref.cat" )
        scampxml = self._outdir_filename( "tmpscamp.xml" )
        remaprefhead = self._outdir_filename( "remapref.head" )
        
        shutil.copy2( self._refimagepath, reffile )
        shutil.copy2( self._refcatpath, refcatfile )
        self._tmpfiles.extend( [ reffile, refcatfile, scampxml, imagehead, remaprefhead ] )
        
        scamp = ( f'scamp -ASTREF_CATALOG FILE -ASTREFCAT_NAME {self._refcatpath} '
                  f'-ASTREFMAG_KEY MAG_AUTO -SOLVE_PHOTOM N '
                  f'-CHECKPLOT_DEV NULL -CHECKIMAGE_TYPE NONE '
                  f'-PROJECTION_TYPE TPV -WRITE_XML Y -XML_NAME {scampxml} -VERBOSE_TYPE LOG '
                  f'-CROSSID_RADIUS 2.0 -ASTR_ACCURACY 0.2 {self._catpath}' )
        self.logger.debug( f'Running {scamp}' )
        res = subprocess.run( scamp, shell=True, capture_output=True, text=True, check=True,
                              timeout=self.scamptimeout )
        errstr = "Not enough matched detections"
        if ( errstr in res.stdout ) or ( errstr in res.stderr ):
            raise RuntimeError( f'Scamp had not enough matched detections.' )

        scampxmldata = astropy.io.votable.parse( scampxml )
        scampstat = scampxmldata.get_table_by_index(1)
        nmatch = scampstat.array["AstromNDets_Reference"][0]
        sig0 = scampstat.array["AstromSigma_Reference"][0][0]
        sig1 = scampstat.array["AstromSigma_Reference"][0][1]
        self.logger.info( f'Scamp of ref to new yielded {nmatch} matches with σnew = {sig0:.3f}", {sig1:.3f}"' )
        
        # ROB VERIFY THAT THESE NUMBERS CORRESPOND TO A GOOD MATCH

        # Create the remapref header so that the ref will remap to what we want
        remaprefhdr = []
        with fits.open( self._imagepath, memmap=False ) as ifp:
            for card in ifp[0].header.cards:
                if "NAXIS" in card.keyword:
                    remaprefhdr.append(card.image)
        remaprefhdr = "\n".join(remaprefhdr) + "\n"
        with open( remaprefhead, "w" ) as ofp:
            ofp.write( remaprefhdr )
            with open( imagehead, "r" ) as ifp:
                ofp.write(ifp.read())
                
        # swarp the reference
        # ASSUMING reference is background subtracted!
        keepkw = f'SKYSIG,SEEING,MEDSKY,{self.expsource.saturate_keyword}'
        swarp = ( f'swarp {self._refimagepath} '
                  f'-IMAGEOUT_NAME {self._remapreffile} -WEIGHTOUT_NAME {self._remaprefweight} '
                  f'-SUBTRACT_BACK N -RESAMPLE_DIR {self.tmpdir} -VMEM_DIR {self.tmpdir} -VERBOSE_TYPE LOG ' 
                  f'-MAP_TYPE MAP_WEIGHT -WEIGHT_IMAGE {self._refweightpath} '
                  f'-VERBOSE_TYPE LOG -COPY_KEYWORDS {keepkw}' )
        self.logger.info( f'Running swarp to remap ref to new' )
        self.logger.debug( f'Swarp command: {swarp}' )
        subpresult = subprocess.run( swarp, shell=True, timeout=self.swarptimeout, capture_output=True )
        if subpresult.returncode != 0:
            raise RuntimeError( f"swarp failed!  stderr:\n{subpresult.stderr.decode('utf-8')}" )
        self.logger.info( f'swarp complete, wrote {self._remapreffile}' )

        # Create a mask for the remapped ref.  (Can swarp "just do" this?)
        with fits.open( self._remaprefweight ) as hdu:
            image_nx = hdu[0].header['NAXIS1']
            image_ny = hdu[0].header['NAXIS2']
            weightdata = hdu[0].data
            maskdata = numpy.zeros_like( weightdata, dtype=numpy.uint8 )
            maskdata[ weightdata <= self.tiny ] = 1
            mhdu = fits.PrimaryHDU( data=maskdata )
            mhdu.writeto( self._remaprefmask, overwrite=True )
            
        # Get a catalog of the swarped image
        self.logger.info( f'Getting catalog of objects on warped ref' )
        processing.extract_catalog( self.expsource, self._remapreffile, self._remaprefweight, self._remaprefcat,
                                    mask=self._remaprefmask, bgsub=False, logger=self.logger )

        self.warpedrefmade = True
        
    # ======================================================================
        
    def _run_subtraction( self, curdb ):
        """Actually perform the subtraction.  Don't call this, call subtract().

        curdb  -- An existing db.DB object; only use this to access the database.

        This method must be implemented by subclasses of Subtractor.  It
        must write the files described under subtract().  All otherfiles
        must be put in the self._tmpfiles variable.

        It can assume that the remapped ref already exists; ref warping
        is performed before calling this method.

        Doesn't have to worry about version tags, that is handled in
        subtract().  Also doesn't have to worry about pushign files up
        to the archive, that's also handled in subtract().

        """
        raise RuntimeError( "Subclasses of Subractor must implement _run_subtraction." )

    
# ======================================================================
    
def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( None, logger=logger )
    
    parser = argparse.ArgumentParser( description="Subtract ref from new, make lots of files",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( "image",
                         help=( "Basename of image in database.  Image must have a catalog, must be photometrically "
                                "and astrometrically calibrated.  (Do processing.py --help)" ) )
    parser.add_argument( "reference", help="Basename of reference in database" )
    parser.add_argument( "-s", "--subtraction-algorithm", default='alard1',
                         help="Name of subtraction algorithm to use" )
    parser.add_argument( "-v", "--verbose", action="store_true", default=False, help="Show debugging log messages" )
    parser.add_argument( "-n", "--no-tag-latest", action="store_true", default=False,
                         help="Do NOT tag this subtraction as the latest for this image (default is to do so)." )
    parser.add_argument( "--searchtag", default="default", help="Search for an existing subtraction with this tag" )
    parser.add_argument( "--never-default", action="store_true", default=False,
                         help=( "Never tag this subtraction as the default for this image; normally, this subtraction "
                                "will be tagged as default if there isn't already a default." ) )
    parser.add_argument( "--always-default", action="store_true", default=False,
                         help=( "Tag this subtraction as the default for this image, replacing the old default "
                                "if there is one (default is to do so)" ) )
    parser.add_argument( "-t", "--tags", nargs="+", default=[],
                         help=( "Additional tags to apply to this subtraction, removing these tags from "
                                "any existing subtractions of the same image" ) )
    parser.add_argument( "--no-newtags", action="store_true", default=False,
                         help="Indicates all --tags should already be defined in the database" )
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel( logging.DEBUG )
    
    with db.DB.get() as curdb:
        image = db.Image.get_by_basename( args.image, curdb=curdb )
        if image is None:
            raise ValueError( f'Image {image} not found in database' )
        expsource = ExposureSource.get( image.exposure.exposuresource.name, image.exposure.exposuresource.secondary )
        ref = db.Image.get_by_basename( args.reference, curdb=curdb )
        if ref is None:
            raise ValueError( f'Reference image {ref} not found in database' )

    subtractor = Subtractor.get( args.subtraction_algorithm, expsource, image, ref, logger=logger )
    subtractor.subtract( searchtag=args.searchtag, tagdefault=args.always_default, tagdefaultifnodefault=not args.never_default,
                         taglatest=not args.no_tag_latest, tags=args.tags, newtags=not args.no_newtags  )
    
    
# ======================================================================

if __name__ == "__main__":
    main()
