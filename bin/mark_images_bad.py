import sys
import io
import logging
import argparse

import psycopg2.extras

import db
import config
from exposuresource import ExposureSource

_logger = logging.getLogger("main")
_logout = logging.StreamHandler( sys.stderr )
_logger.addHandler( _logout )
_logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
_logger.setLevel( logging.INFO )

def _actually_mark_images_bad( imgobjs, good=False, untag_photometry=True, curdb=None ):
    """Don't call this from the outside, internal use only."""

    if curdb is None:
        raise RuntimeError( "curdb can't be None" )

    for img in imgobjs:
        img.image_is_bad = not good

    curdb.db.commit()

    if ( not good ) and untag_photometry:
        con = curdb.db.connection().engine.raw_connection()
        cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
        q = ( "DELETE FROM photometry_versiontag WHERE photometry_id IN "
              "( SELECT id FROM photometry WHERE image_id IN %s )" )
        imgids = tuple( [ img.id for img in imgobjs ] )
        cursor.execute( q, ( imgids, ) )
        con.commit()
        
    
def mark_images_bad( basenames=[], obj=None, expsource=None, band=None, mjd0=None, mjd1=None,
                     really_do=False, good=False, untag_photometry=True, curdb=None ):
    """Mark images in the database as bad, and thus not to be used furhter.

    basename : List of image basenames.  Either specify this, *or*
      specify all of obj, expsourece, mjd0, mjd1, and (optionally) band
    obj : Either a db.Object object, or the name of an object we'll find images that contain
    expsource : either an exposuresource.ExposureSource object, or the name of an exposure source
    band : Either a db.Band object, or the name of a band we'll use to search for images
    mjd0 : The earliest mjd of images to find
    mjd1 : The latest mjd of images to find
    really_do : If False, just returns the db.Image objects that would have been marked bad.
    good : Actually make images as good rather than bad
    untag_photometry : Remove *all* versiontags from photometry from the images that are marked bad,
       so that photometry won't be found later
    curdb : A db.DB object

    """

    if ( ( ( band is not None) or ( mjd0 is not None ) or ( mjd1 is not None ) or ( expsource is not None ) )
         and
         ( obj is None ) ):
        raise RuntimeError( "Only give expsource, band, mjd0, mjd1 if also giving obj" )
    if ( ( obj is not None ) and ( ( mjd0 is None ) or ( mjd1 is None ) or ( expsource is None ) ) ):
        raise RuntimeError( "obj requires mjd0, mjd1" ) 
    if ( len(basenames) > 0 ) and ( obj is not None ):
         raise RuntimeError( "Either give a list of basenames, or specify an object, not both" )

    with db.DB.get( curdb ) as dbo:
        if len(basenames) > 0:
            imgobjs = dbo.db.query( db.Image ).filter( db.Image.basename.in_( basenames ) ).all()
            if len(imgobjs) != len(basenames):
                raise RuntimeError( f"You gave {len(basename)} basenames, but I found {len(imgobjs)} images" )
        else:
            if not isinstance( obj, db.Object ):
                objobj = db.Object.get_by_name( obj, curdb=dbo )
                if objobj is None:
                    raise RuntimeError( f"Unknown object {obj}" )
                obj = objobj
            if not isinstance( expsource, ExposureSource ):
                expsource = ExposureSource.get( expsource )
            imgobjs = db.Image.get_including_point( obj.ra, obj.dec, band=band, expsourceid=expsource.id,
                                                    mjd0=mjd0, mjd1=mjd1, includebad=True, curdb=dbo )
        imgobjs.sort( key=lambda x: x.mjd )

        if really_do:
            _actually_mark_images_bad( imgobjs, good=good, untag_photometry=True, curdb=dbo )

        return imgobjs
     
         
def main():
    config.Config.init( None, logger=_logger )

    parser = argparse.ArgumentParser( description="Mark images in the database bad" )

    parser.add_argument( "-b", "--basenames", default=[], nargs='+',
                         help="Basenames of image to mark bad" )
    parser.add_argument( "-o", "--object", default=None,
                         help="Mark images that have this object in them bad" )
    parser.add_argument( "-e", "--expsource", default=None,
                         help="Exposure source of images to makr bad" )
    parser.add_argument( "--band", default=None,
                         help="Only mark images for object in this band bad (default: all bands)" )
    parser.add_argument( "--mjd0", default=None,
                         help="Mark images starting at this mjd as bad" )
    parser.add_argument( "--mjd1", default=None,
                         help="Mark images stopping at this mjd as bad" )
    parser.add_argument( "-g", "--good", action='store_true', default=False,
                         help="Mark images *good* rather than bad" )
    parser.add_argument( "-d", "--dont-untag-photometry", action='store_true', default=False,
                         help=( "Don't untag photometry on images marked bad: "
                                "(default: all photometry versiontags stripped)" ) )
    parser.add_argument( "--really-do", action='store_true', default=False,
                         help="Really do it!  (Otherwise, just list what will be done.)" )
    args = parser.parse_args()

    with db.DB.get() as curdb:
        imgobjs = mark_images_bad( basenames=args.basenames, obj=args.object, expsource=args.expsource,
                                   band=args.band, mjd0=args.mjd0, mjd1=args.mjd1, good=args.good,
                                   untag_photometry=(not args.dont_untag_photometry), curdb=curdb )
        sio = io.StringIO()
        if args.really_do:
            sio.write( f"Marking the following {len(imgobjs)} images as {'good' if args.good else 'bad'}:\n" )
        else:
            sio.write( f"Would mark the following {len(imgobjs)} images as {'good' if args.good else 'bad'}:\n" )
        sio.write( f"    {'basename':<40s} {'mjd':9s} {'band':8s}\n" )
        for img in imgobjs:
            sio.write( f"    {img.basename:<40s} {img.mjd:9.3f} {img.band.name:8s}\n" )

        _logger.info( sio.getvalue() )

        if not args.good:
            if args.dont_untag_photometry:
                _logger.warning( f"{'Will' if args.really_do else 'Would'} "
                                 f"not untag photometry from images marked bad." )
            else:
                _logger.info( f"{'Will' if args.really_do else 'Would'} "
                              f"strip all version tags from photometry on images marked bad." )

        if args.really_do:
            sys.stderr.write( f"Really mark these {len(imgobjs)} images as {'good' if args.good else 'bad'}? (y/n) " )
            yn = input()
            if yn.lower() == 'y':
                _actually_mark_images_bad( imgobjs, good=args.good,
                                           untag_photometry=(not args.dont_untag_photometry), curdb=curdb )
            else:
                _logger.info( "Didn't mark anything." )
            
# ======================================================================

if __name__ == "__main__":
    main()
