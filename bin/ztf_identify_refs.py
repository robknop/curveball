#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import logging
import argparse
import pathlib
import numpy

from exposuresource import ExposureSource
import config
import processing
import db

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.DEBUG )

    config.Config.init( None, logger=logger )
    
    argparser = argparse.ArgumentParser( description="Download ZTF for RA/Dec and load into DB" )
    argparser.add_argument( "ra", type=float, help="RA decimal degrees" )
    argparser.add_argument( "dec", type=float, help="Dec decimal degrees" )
    argparser.add_argument( "peakt", type=float, help="Peak time (MJD)" )
    args = argparser.parse_args()

    ztf = ExposureSource.get( "ZTF" )
    refs = ztf.secure_references( args.ra, args.dec, args.peakt, logger=logger )

    for fc in refs.keys():
        print( f'\n\nRefs for {fc}:' )
        for ref in refs[fc]:
            print( ref )

    
# ======================================================================

if __name__ == "__main__":
    main()
