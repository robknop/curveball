import sys
import logging
import argparse

import pandas

import db
import config

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( None, logger=logger )

    parser = argparse.ArgumentParser( description="List database objects" )
    parser.add_argument( "-o", "--object", default=None, help="Name of object in database (default: show all)" )
    parser.add_argument( "-i", "--include-ignore", action='store_true', help="Include ignored objects" )
    args = parser.parse_args()
    
    with db.DB.get() as curdb:
        objs = curdb.db.query( db.Object )
        if args.object is not None:
            objs = objs.filter( db.Object.name==args.object )
        if not args.include_ignore:
            objs = objs.filter( db.Object.ignore!=True )
        objs = objs.all()
        data = { 'id': [],
                 'name': [],
                 'ra': [],
                 'dec': [],
                 't0': [],
                 'z': [] }
        for o in objs:
            for field in [ 'id', 'name', 'ra', 'dec', 't0', 'z' ]:
                data[field].append( getattr( o, field ) )
        df = pandas.DataFrame( data )
        pandas.set_option( 'display.max_columns', None )
        pandas.set_option( 'display.max_rows', None )
        print( df.to_string(index=False) )

# ======================================================================

if __name__ == "__main__":
    main()
