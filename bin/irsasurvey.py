import io
import requests
import logging
import urllib
import pandas

from exposuresource import *
import util
import db
import processing
import astrometry

class IRSASurvey(ExposureSource):
    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )

    # ======================================================================

    def find_images( self, ra, dec, containing=True, dra=0.008, ddec=0.008,
                     band=None, starttime=None, endtime=None, minexptime=None,
                     logger=logging.getLogger("main") ):
        if containing is False:
            raise NotImplementedError( f"{self.__class__.__name__} find_images "
                                       f"currently only works with 'containing'" )

        url = self._searchurlbase + f"?POS={ra:.4f},{dec:.4f}"

        query = ""
        if starttime is not None:
            query += " AND " if len(query) > 0 else ""
            if isinstance( starttime, float ) or isinstance( starttime, int ):
                if starttime < 2400000:
                    starttime += 2400000.5
                query += f"obsjd>={starttime:.5f}"
            else:
                query += f"obsjd>={util.julday(starttime):.5f}"
        if endtime is not None:
            query += " AND " if len(query) > 0 else ""
            if isinstance( endtime, float ) or isinstance( endtime, int ):
                if endtime < 2400000:
                    endtime += 2400000.5
                query += f"obsjd<={endtime:.5f}"
            else:
                query += f"obsjd<={util.julday(endtime):.5f}"
        if minexptime is not None:
            query += " AND " if len(query) > 0 else ""
            query += f"exptime>{minexptime}"
        if band is not None:
            if not isinstance( band, db.Band ):
                band = db.Band.get_by_name( band, self.id )
                if band is None:
                    raise RuntimeError( f'Unknown band {band}' )
            query += " AND " if len(query) > 0 else ""
            query += self.filter_find_query( band )

        if len(query) > 0:
            url += f"&WHERE={urllib.parse.quote_plus(query)}"

        if self._irsa_query_columns is not None:
            url += f"&COLUMNS={self._irsa_query_columns}"
        url += "&CT=csv"
        logger.debug( f'Querying url {url}' )
        res = requests.get( url )
        if res.status_code != 200:
            raise Exception( f"{self.__class__.__name__} query returned status {res.status_code}" )
        if ( not 'Content-Type' in res.headers) or ( res.headers['Content-Type'] != 'text/csv' ):
            import pdb; pdb.set_trace()
            raise Exception( f"{self.__class__.__name__} query didn't return text/csv" )
        

        strio = io.StringIO( res.text )
        tab = pandas.read_csv( strio )

        # import pdb; pdb.set_trace()
        
        # Change jd to mjd if necessary
        if 'obsmjd' not in tab.columns:
            tab['obsmjd'] = tab['obsjd'] - 2400000.5

        # Make sure that there is a filtercode
        if 'filtercode' not in tab.columns:
            fcs = []
            for i in range(len(tab)):
                fcs.append( self.blob_image_filtercode( tab, i ) )
            tab['filtercode'] = fcs

        # Sort by obsmjd
        tab.sort_values( 'obsmjd', inplace=True )

        # Further processing that's exposuresource-specific

        tab = self.further_process_blob( tab, logger=logger )
        
        return tab

    # ======================================================================
    # ZTF doesn't need this, PTF does.  Default doing nothing here.
    
    def further_process_blob( self, blob, logger=logging.getLogger("main") ):
        return blob
    
    # ======================================================================
    # Basic information about exposures from this Exposure Source

    @property
    def badmask(self):
        # From http://web.ipac.caltech.edu/staff/fmasci/ztf/ztf_pipelines_deliverables.pdf
        # NOTE -- that documentation is for ZTF.  I'm currently rashly assumign
        # it's going to be true for PTF and other IRSA things too.  THAT MAY BE WRONG.
        # I want to mask thing with bits set: 0, 5, 6, 7, 8, 9, 10 (12?)
        return 2**0 + 2**5 + 2**6 + 2**7 + 2**8 + 2**9 + 2**10
    
    # ======================================================================
    # ======================================================================
    # Downloading images from IRSA server

    def wrangle_mask( self, databytes, outfile, imagebase ):
        fstream = io.BytesIO( databytes )
        with fits.open( fstream, memmap=False ) as hdu:
            maskraw = hdu[0].data
        maskdata = numpy.zeros_like( maskraw, dtype=numpy.uint8 )
        w = numpy.where( ( maskraw & self.badmask ) != 0 )
        maskdata[w] = 1
        maskhdr = fits.Header()
        maskhdr['IMAGE'] = ( f'{imagebase}.fits', 'Bad-pixel mask for image' )
        maskhdr['COMMENT'] = '1=bad, 0=not known to be bad'
        hdu = fits.PrimaryHDU( header=maskhdr, data=maskdata )
        hdu.writeto( outfile )
    
    def ensure_sourcefiles_local( self, basename, retries=5, curdb=None, logger=logging.getLogger("main") ):
        cfg = config.Config.get()
        with db.DB.get(curdb) as curdb:
            imgobj = db.Image.get_by_basename( basename, curdb=curdb )
            if imgobj is None:
                raise RuntimeError( f'No image {basename} in the databse' )
            outroot = cfg.value('datadirs')[0]
            imagepath = self.local_path( f'{basename}.fits' )
            maskpath = self.local_path( f'{basename}.mask.fits' )
            weightpath = self.local_path( f'{basename}.weight.fits' )
            for path, tag in zip( [ imagepath, maskpath ], [ '', '.mask' ] ):
                if path is None:
                    fname = f'{basename}{tag}.fits'
                    url = self.source_url( fname, logger=logger )
                    countdown = retries
                    while countdown >= 0:
                        try:
                            logger.info( f'Getting {fname} from URL {url}' )
                            res = requests.get( url )
                            if res.status_code != 200:
                                raise RuntimeError( f'IRSA query returned status {res.status_code}' )
                            path = outroot / self.relpath( f'{basename}{tag}.fits' )
                            path.parent.mkdir( parents=True, exist_ok=True )
                            if tag == "":
                                imagepath = path
                                with open( path, "wb" ) as ofp:
                                    ofp.write( res.content )
                            elif tag == ".mask":
                                maskpath = path
                                self.wrangle_mask( res.content, path, basename )
                            else:
                                raise Exception( "This really should never happen." )
                            countdown = -1
                        except Exception as e:
                            if countdown > 0:
                                logger.error( f'Exception {str(e)} trying to download from {url} in '
                                              f'ensure_sourcfiles_local; will sleep 2s and try again' )
                                time.sleep(2)
                                countdown -= 1
                            else:
                                logger.error( f'Exception {str(e)} trying to download from {url} '
                                              f'in ensure_sourcefiles_local' )
                                raise RuntimeError( f'Repeated exceptions trying to download {url} '
                                                    f'in ensure_sourcefiles_local' )
            if imgobj.isastrom:
                # Update the downloaded image header with astrometry from database
                astrometry.update_image_header_from_db( imagepath.name, self, curdb=curdb, logger=logger )
            if imgobj.isphotom:
                # Make sure image header has right photometry keywords
                if ( ( imgobj.magzp is None ) or ( imgobj.seeing is None ) or ( imgobj.medsky is None ) or
                     ( imgobj.limiting_mag is None ) ):
                    raise RuntimeError( f"One of (magzp, seeing, medsky, limiting_mag) is None in database "
                                        f"for {imgobj.basename}" )
                with fits.open( imagepath, mode='update', memmap=False ) as hdu:
                    hdu[0].header['MAGZP'] = imgobj.magzp
                    hdu[0].header['MAGZPERR'] = hdu[0].header[ self._magzpunc_header_keyword ]
                    hdu[0].header['SEEING'] = imgobj.seeing
                    hdu[0].header['MEDSKY'] = imgobj.medsky
                    hdu[0].header['SKYSIG'] = imgobj.skysig
                    hdu[0].header['LMT_MG'] = imgobj.limiting_mag

            if weightpath is None:
                weightpath = imagepath.parent / f'{basename}.weight.fits'
                logger.info( f'Creating {weightpath.name}' )
                processing.make_weight( self, imagepath, mask=maskpath, weightpath=weightpath, logger=logger )

    # ======================================================================
    # Getting stuff from the find_images blob

    def blob_image_mjd( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns MJD of image for index in the blob returned by find_images"""
        return imageblob.iloc[index].obsmjd

    def blob_image_exptime( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns exposure time (seconds) of image for index in the blob returned by find_images"""
        return imageblob.iloc[index].exptime

    
