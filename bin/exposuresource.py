#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import io
import re
import time
import json
import pathlib
import logging
import requests
import subprocess
import hashlib

import numpy
import pandas
from sklearn.neighbors import KernelDensity
from scipy.signal import argrelextrema

import astropy.units as units
from astropy.coordinates import SkyCoord
from astropy.wcs import WCS
from astropy.io import fits

import config
import db
import processing
from archive import Archive


# ======================================================================

class ExposureSource:
    """A subclass of ExposureSource defines a place we can get images from.

    There are some underlying assumptions.  There are implicit
    assumptions that instrumental zeropoints, color terms, etc. will be
    the same for all images from the same ExposureSource.  (Meaning: if
    you build a lightcurve all from one ExposureSource, then you don't
    have to worry about cross-camera systematics, etc.  Within-camera
    systematics and correlations, of course, are all still there.)
    Filenames will all follow the same pattern for a given exposure
    source.  Image orientation is at least approximately the same every
    time for a given exposure source.  Probably others.

    (More documentation needed.)

    FILENAME ASSUMPTIONS: All filenames of images within the curveball
    universe follow the pattern `*.fits` or `*.{type}.fits`. There may
    also be a .fz or a .gz after the .fits.  Something that is just
    ".fits" is assumed to be just a sky image.  {type} will be something
    like "weight", "mask", etc.  Other than that, there are methods within the
    appropriate ExposureSource subclass to pull out the right filename
    for the right type of image.

    The software assumes that the date of the image is somehow encoded
    into the filename.  Exactly how that is done is defined by the
    subclass for a given ExposureSource.  If the native files from the
    ExposureSource *don't* encode the date, then ... well, we'll have to
    think about how to propertly deal with that.

    """

    TYPE_UNKNOWN = 0
    TYPE_CAMERA = 1
    TYPE_SURVEY = 2
    TYPE_CAMERA_REDUCED = 3
    NAME = None
    SECONDARY = None

    _orientation_desc = [ "North Up, East Left",
                          "North Right, East Up",
                          "North Down, East Right",
                          "North Left, East Down",
                          "North Up, East Right",
                          "North Right, East Down",
                          "North Down, East Left",
                          "North Left, East Up" ]

    _parseoutfits = re.compile( r"^(.*)\.fits(\.[fg]z)?$" )
    _parseouttypeandfits = re.compile( r"^(.*)\.([^\.]+)\.fits(\.[fg]z)?$" )

    # Subclasses need to define these as compiled regular expressions; see ptf.py as an example

    _generalparse = None
    _imageparse = None

    # Subclasses need to define these as indexes into the match array returned by re.search
    # for the patterns above

    _match_basename = None
    _match_image_ccd = None
    _match_yyyymmdd = None
    _match_hhmmss = None
    _match_fake = None
    _match_stack = None
    _match_filetype = None
    _match_image_filetype = None

    archive_singlet = None

    # I do the importing inside this function to
    #  avoid issues of circular imports
    @classmethod
    def get( cls, name, secondary=None ):
        """Call this to get an ExposureSource object."""

        # If secondary is not None, then it's possible name has a slash in it
        #   and has both primary and secondary
        if secondary is None:
            name, secondary = cls.how_annoying_split_name_secondary_at_slash( name )

        if ( name == "ZTF" ) and ( secondary is None ):
            import ztf
            return ztf.ZTF()
        elif ( name == "PTF" ) and ( secondary is None ):
            import ptf
            return ptf.PTF()
        elif ( name == "DECam" ) and ( secondary == "Reduced" ):
            import decam_reduced
            return decam_reduced.DECam_Reduced()
        elif ( name == "manual_expsource" ) and ( secondary is None ):
            import manual_expsource
            return manual_expsource.Manual_Expsource()
        else:
            raise ValueError( f'Unknown exposure source {name}{" "+secondary if secondary is not None else ""}' )

    @classmethod
    def how_annoying_split_name_secondary_at_slash( cls, fullname ):
        primary = fullname
        secondary = None
        try:
            slashdex = fullname.index( "/" )
            primary = fullname[ 0:slashdex ]
            secondary = fullname[ slashdex+1: ]
        except ValueError as ex:
            pass
        return primary, secondary

    # There is some redundant database grabbing here, but oh well
    # (This method queries the database, and the same exact data will be pulled from
    # the databse in get() when the actual class is instantiated.)
    @classmethod
    def get_by_database_id( cls, dbid ):
        dbexpsrc = db.ExposureSource.get( dbid )
        if dbexpsrc is None:
            raise ValueError( f"Unknown exposure source id {dbid}" )
        return cls.get( name=dbexpsrc.name, secondary=dbexpsrc.secondary )

    @classmethod
    def get_all( cls ):
        """Return a list of ExposureSource objects."""
        expsources = []
        expsources.append( cls.get( 'ZTF' ) )
        return expsources

    def __init__( self, archive=None ):
        """Don't call this directly; call ExposureSource.get()"""
        self._dbinfo = db.ExposureSource.getdbinfo( self.NAME, self.SECONDARY )
        if self._dbinfo is None:
            raise ValueError( f'Couldn\'t load database info for exposure source '
                              f'{self.NAME}{" "+self.SECONDARY if self.SECONDARY is not None else ""}' )

        if archive is not None:
            self.archive = archive
        else:
            # ...not thread safe...
            if ExposureSource.archive_singlet is None:
                cfg = config.Config.get()
                archiveurl = cfg.value( 'archive.uploadurl' )
                prefix = cfg.value( 'archive.prefix' )
                token = cfg.value( 'archive.token' )
                verify_cert = cfg.value( 'archive.verify_cert', default=False )
                local_read_dir = cfg.value( 'archive.read_dir', default=None )
                local_write_dir = cfg.value( 'archive.write_dir', default=None )
                ExposureSource.archive_singlet = Archive( archive_url=archiveurl, path_base=prefix,
                                                          token=token, verify_cert=verify_cert,
                                                          local_read_dir=local_read_dir,
                                                          local_write_dir=local_write_dir )
            self.archive = ExposureSource.archive_singlet

    # ======================================================================
    # Basic information about exposures from this Exposure Source

    @property
    def id(self):
        return self._dbinfo.id

    @property
    def chips(self):
        return self._dbinfo.chips

    @property
    def pixscale(self):
        """Nominal pixel scale of camera"""
        return self._dbinfo.pixscale

    @property
    def clipsize(self):
        """Size of thumbnail to clip out for subtraction preview"""
        return 51

    @property
    def badmask(self):
        """Which bits in source mask are "bad"

        For images straight from the source, AND this value with the
        mask image to identify bad pixels.  The rest of the pipeline is
        going to assume that mask!=0 is bad, mask=0 is good, so the
        subclass that acquires images needs to mangle the mask
        accordingly.

        (I wish I could just work with original masks and use IMAFLAGS
        and such in sextractor, but sadly psfex seems to assume that
        that mask is a single byte, and at least ZTF has a 16-bit mask.
        So... simplify it is.)

        """
        return 0xffff

    @property
    def ra_degrees_keyword(self):
        """Keyword for RA of *exposure*"""
        return "TELRAD"

    @property
    def dec_degrees_keyword(self):
        """Keywrod for dec of *exposure*"""
        return "TELDECD"

    @property
    def filter_keyword(self):
        """ Keyword in FITS header for filter letter """
        raise NotImplementedError(f'filter keyword unknown for {self.__class__.__name__}')

    @property
    def mjd_keyword(self):
        """ Keyword in FITS header for filter letter """
        raise NotImplementedError(f'MJD keyword unknown for {self.__class__.__name__}')

    @property
    def saturate_keyword(self):
        """Keyword in FITS Header with saturation level in adu"""
        return "SATURATE"

    @property
    def gain_keyword(self):
        """Keyword in FITS header with e-/adu gain"""
        return "GAIN"

    @property
    def readnoise_keyword(self):
        """Keyword in FITS header with readnoise in e-/adu"""
        raise NotImplementedError( f'readnoise keyword unknown for {self.__class__.__name__}' )

    @property
    def darkcurrent_keyword(self):
        """Keyword in FITS header with dark current in e-/sec"""
        raise NotImplementedError( f'dark current keyword unknown for {self.__class__.__name__}' )

    @property
    def exptime_keyword(self):
        """Keyword in FITS header with exptime in seconds"""
        raise NotImplementedError( f'exptime keyword unknown for {self.__class__.__name__}' )

    @property
    def key_keywords(self):
        """String with comma-separated list of keywords to keep in swarp"""
        raise NotImplementedError( f'key_keywords unknown for {self.__class__.__name__}' )

    # ======================================================================
    # Information about image names, exposure names, and paths

    @property
    def topdirname(self):
        """Top directory name (below root data directory) for archiving images."""
        name = self.NAME.replace( " ", "_" )
        if self.SECONDARY is not None:
            name += "_" + self.SECONDARY.replace( " ", "_" )
        return name

    def archivesubdir( self, imagename ):
        """Returns a relative pathlib.Path object with subdirectory of image"""
        imagename = pathlib.Path( imagename ).name
        if self.isstack( imagename ):
            return pathlib.Path( self.topdirname ) / "stack" / self.yyyymmdd( imagename )
        else:
            return pathlib.Path( self.topdirname ) / self.yyyymmdd( imagename )

    def relpath( self, imagename ):
        """Retruns a relative pathlib.Path object for the image relative to data root dir"""
        imagename = pathlib.Path( imagename ).name
        return self.archivesubdir( imagename ) / imagename

    def exposure_basename(self, imagename):
        """Return base name of exposure (without chip, without .fits, with .* just before fits)

        image — name (or path) of image.
        """
        match = self._generalparse.search( imagename )
        if match is None:
            raise ValueError( f'Error parsing exposure filename {image}' )
        return match.group(self._match_basename)

    def image_basename(self, imagename):
        """Return base name of image (including chip information, without .fits, without .* just before fits)

        image — name (or path) of image.
        """
        match = self._imageparse.search( imagename )
        if match is None:
            raise ValueError( f'Error parsing image filename {imagename}' )
        return f'{match.group(self._match_basename)}.{match.group(self._match_image_ccd)}'

    def make_stackpath_from_image_name( self, imagename ):
        imagename = pathlib.Path( imagename )
        stackname = imagename.name
        if stackname[0:6] != 'stack_':
            stackname = f'stack_{stackname}'
        return pathlib.Path( self.topdirname ) / "stack" / self.yyyymmdd( imagename.name ) / stackname

    def get_path_for_stack( self, stackbasename ):
        """Returns the path *realtive to data root* of a stack with basename stackbasename"""
        stackbasename = pathlib.Path( stackbasename ).name
        stackname = f'{stackbasename}.fits'
        return pathlib.Path( self.topdirname ) / "stack" / self.yyyymmdd( stackname ) / stackname

    # ======================================================================
    # Information parsed from image names

    def chiptag(self, imagename):
        """Return chip tag from image filenaem (or maybe header?)"""
        match = self._imageparse.search( imagename )
        if match is None:
            raise ValueError( f'Error parsing image filename for chiptag: {image}' )
        return match.group(self._match_image_ccd)

    def yyyymmdd( self, image ):
        """Get string YYYY-MM-DD from image name or header"""
        match = self._generalparse.search( image )
        if match is None:
            raise ValueError( f'Error parsing date from {os.path.basename(image)}' )
        datestr = match.group(self._match_yyyymmdd)
        return f'{datestr[0:4]}-{datestr[4:6]}-{datestr[6:8]}'

    def isstack( self, imagename ):
        """Parse filename; is this a stack, or a non-stack image?"""
        match = self._generalparse.search( imagename )
        if match is None:
            raise ValueError( f'Error parsing image filename {imagename}' )
        return match.group(self._match_stack) == "stack_"

    def issfake( self, imagename ):
        """Parse filename; is this an image with fakes"""
        match = self._generalparse.search( imagename )
        if match is None:
            raise ValueError( f'Error parsing image filename {imagename}' )
        return match.group(self._match_fake) == "fake_"

    def filetype( self, filename ):
        """Returns the type of FITS file this is supposed to be (image, mask, weight, ... )

        """

        # known_types = [ "mask", "weight" ]
        known_types = [ "mask", "weight" ]
        filepath = pathlib.Path(filename)
        filename = filepath.name
        match = self._imagetypeparse.search( filename )
        if match is not None:
            if match.group(self._match_image_filetype) in known_types:
                return match.group(self._match_image_filetype)
            else:
                raise ValueError( f'Unknown file type {match.group(self._match_image_filetype)} for {filename}' )
        match = self._imageparse.search( filename )
        if match is not None:
            return "image"
        match = self._exposuretypeparse.search( filename )
        if match is not None:
            if match.group(self._match_exposure_filetype) in known_types:
                return match.group(self._match_exposure_filetype)
            else:
                raise ValueError( f'Unknown file type {match.group(self._match_exposure_filetype)} for {filename}' )
        match = self._exposureparse.search( filename )
        if match is not None:
            return "image"
        else:
            raise ValueError( f'Failed to parse {filename}' )

    # ======================================================================
    # Information parsed from image headers

    def gain(self, image):
        """Return gain in e-/adu for the image, determined somehow from the header (or image)"""
        with fits.open( image, memmap=False ) as hdu:
            return float(hdu[0].header[self.gain_keyword])

    def readnoise(self, image):
        """Return readnoise in e- for the image, determined somehow from the header"""
        with fits.open( image, memmap=False ) as hdu:
            return float(hdu[0].header[self.readnoise_keyword])

    def darkcurrent(self, image):
        """Return dark current of image in e-/s, determined somehow from the header"""
        ### Overload if needed
        with fits.open( image, memmap=False ) as hdu:
            return float(hdu[0].header[self.darkcurrent_keyword])

    def exptime(self, image):
        """Return exptime of image in seconds, determined somehow from the header"""
        with fits.open( image, memmap=False) as hdu:
            return float(hdu[0].header[self.exptime_keyword])

    def filter(self, image):
        """Return filter from header of image"""
        with fits.open( image, memmap=False ) as hdu:
            return self.filtername_from_header( hdu[0].header )

    def filtername_from_header( self, hdr ):
        """
        Return filter from passed FITS header

        if the filter header keyword doesn't match the database name, 
            you need to override this function
        """
        return hdr[self.filter_keyword].strip()

    def fieldname(self, image):
        """Field name for this image"""
        with fits.open( image, memmap=False ) as hdu:
            return self.fieldname_from_header(hdu[0].header)

    def fieldname_from_header(self, header):
        """Field name parsed from this FITS Header"""
        return "Unknown"

    def mjd( self, image):
        """MJD for this image"""
        with fits.open( image, memmap=False) as hdu:
            return self.mjd_from_header( hdu[0].header )

    def mjd_from_header(self, header):
        """MJD parsed from this FITS header"""
        return float(header[self.mjd_keyword])

    def guess_seeing(self, image, logger=logging.getLogger("main")):
        """Guess the seeing FWHM in arcseconds.

        Based on the header, if anything; may be a blind guess.  Use
        this only for a very general guideline, not for anything that
        requires actual accuracy or precision

        This default method returns the value of the SEEING header card.
        It's assuming that's in arcseconds..  At least with ZTF, the
        donwloaded images have it in pixels, but the scale is
        1.0"/pixel, so there it doesn't matter.

        """
        with fits.open( image ) as hdu:
            try:
                return float(hdu[0].header["SEEING"])
            except KeyError as e:
                logger.warning( 'SEEING not found in header, just randomly guessing 1.2"' )
                return 1.2

    # ======================================================================

    def north_up_east_left( self, arr, orientation=None ):
        """Return a new numpy array that is oriented N-up E-left assuming it starts with expsource orientation.

        This one is very scary because matplotlib thinks that higher
        values of y are lower on the image, whereas astronomres think
        that higher values of y are higher on the image.

        It's also scary because of storage order and what you think of as x and y.

        Yikers.

        """
        if orientation is None:
            orientation = self._dbinfo.orientation

        newarr = numpy.array( arr )
        if orientation == 0:
            pass
        elif orientation == 1:
            numpy.rot90( newarr, k=3 )
        elif orientation == 2:
            numpy.rot90( newarr, k=2 )
        elif orientation == 3:
            numpy.rot90( newarr, k=1 )
        elif orientation == 4:
            numpy.fliplr( newarr )
        elif orientation == 5:
            numpy.fliplr( newarr )
            numpy.rot90( newarr, k=1 )
        elif orientation == 6:
            numpy.fliplr( newarr )
            numpy.flipud( newarr )
        elif orientation == 7:
            numpy.rot90( newarr, k=1 )
            numpy.fliplr( newarr )
        else:
            raise ValueError( f'Unknown orientation {orientation} '
                              f'for exposure source {self._dbinfo.name}' )
        return newarr

    # ======================================================================

    def local_path( self, image, logger=logging.getLogger("main") ):
        """Return local absolute path of image (etc.), or None if not found."""
        cfg = config.Config.get()
        relpath = self.relpath( image )
        localfile = None
        for datadir in cfg.value( 'datadirs' ):
            testfile = pathlib.Path(datadir) / relpath
            if testfile.exists():
                if testfile.is_file():
                    localfile = testfile
                    break
                else:
                    raise RuntimeError( f'{testfile} exists but is not a file!' )
        return localfile

    def local_path_from_basename(self, basename, filetype=None, ext=None,
                                 logger=logging.getLogger("main")):
        """ Returns the local path to a file from its basename """
        imgpath = basename
        if ext is None:
            ext = '.fits'

        if filetype==None or filetype=='image':
            imgpath+= ext
        elif filetype=='mask':
            imgpath+= f'.mask{ext}'
        elif filetype=='weight':
            imgpath+= f'.weight{ext}'
        elif filetype=='cat':
            imgpath+='.cat'
        elif filetype=='psf':
            imgpath+='.psf'

        localfile = self.local_path( imgpath )
        # if localfile is None:
        #     localfile = self.local_path( imgpath + '.fz')

        return localfile

    # ======================================================================

    def all_archived_filenames( self, basename ):
        """Returns a list of all filenames (not paths) that might get copied
        up to the archive for an image with this basename.  Does NOT include
        files generated by a subtraction, just files generated by original
        image loading and cat_psf_sky.

        Some of the ones passed back might not be copied, e.g. .fits,
        .weight.fits, and .mask.fits are *not* stored on the archive for
        exposuresources that give reduced images (as they can be
        recovered from the original exposuresource).

        """
        them = []
        for ext in [ ".fits", ".weight.fits", ".mask.fits", ".cat", ".psf", ".psf.xml" ]:
            them.append( f"{basename}{ext}" )
        return them

    # ======================================================================

    def photocalib_headeronly( self, image, cat=None ):
        """The exposuresource-specific routine that actually does photometric calbration.

        image - Path to image file
        cat - Path to catalog file (not used by all exposuresources) (currently used by none of them)

        Updates the header, does NOT update the database.

        Calibration is for PSF-fitting photometry, i.e. that includes
        all of the flux of a star; there is no aperture correction built
        into the zeropoint.

        Header that must be written are:

        MAGZP — zeropoint in the filter of this image (no color correction applied)
        MAGZPERR — uncertainty on the zeropoint (or 0 if unknown)
        LMT_MG — 5σ point-source limiting magnitude
        Maybe SEEING and SKYSIG should be set too (they are for ZTF) but not sure if it's correct

        Returns magzp
        """
        raise NotImplementeError( f'photocalib_headeronly not implemented for {self.__class__.__name__}' )

    # ======================================================================

    def photocalib( self, image, cat=None, savedb=False, force_redo=True, curdb=None,
                    logger=logging.getLogger("main") ):

        """Photometrically calibrate the image, updating the header and maybe the database.

        image - one of
           * a db.Image object
           * an image basename
           * an image filename
           * path to image object
        cat - Path to catalog file (not used by all exposuresources) (currently used by none of them)
        savedb - Update the image's record in the database.  If True, and the image isn't in the
           database, will still update the header, but then thrown an error.

        Calibration is for PSF-fitting photometry, i.e. that includes
        all of the flux of a star; there is no aperture correction built
        into the zeropoint.

        Header keywords are:

        MAGZP — zeropoint in the filter of this image (no color correction applied)
        MAGZPERR — uncertainty on the zeropoint (or 0 if unknown)
        LMT_MG — 5σ point-source limiting magnitude
        Maybe SEEING and SKYSIG should be set too (they are for ZTF) but not sure if it's correct

        Returns magzp

        """

        if cat is not None:
            logger.warn( "Don't know how to deal with cat not equal None, setting cat to None!!!!!!!" )
            cat = None

        if not force_redo:
            raise NotImplementedError( "Need to implement force_redo=False" )

        if isinstance( image, db.Image ):
            self.ensure_image_local( image.basename )
            path = self.local_path_from_basename( image.basename, filetype='image' )
            if not path.is_file():
                raise FileNotFoundError( f"Couldn't get local path for image with basename {image.basename}" )
        else:
            path = pathlib.Path( image )
            origpath = path
            if not path.is_file():
                path = self.local_path( origpath.name )
                if path is None:
                    path = self.local_path_from_basename( origpath.name, filetype='image' )
                    if path is None:
                        raise FileNotFoundError( "Failed to find image {image}" )

        magzp = self.photocalib_headeronly( path, cat=cat )
        if not savedb:
            return magzp

        basename = self.image_basename( path.name )

        with db.DB.get(curdb) as curdb:
            imgobj = db.Image.get_by_basename( basename, curdb=curdb )
            if imgobj is None:
                raise RuntimeError( f"Failed to find image {basename} in the database" )

            with fits.open( path, memmap=False ) as hdu:
                magzp = hdu[0].header['MAGZP']
                magzperr = hdu[0].header['MAGZPERR']
                lmt_mg = hdu[0].header['LMT_MG']
                seeing = hdu[0].header['SEEING']

            imgobj.seeing = seeing
            imgobj.magzp = magzp
            imgobj.limiting_mag = lmt_mg
            imgobj.isphotom = True
            curdb.db.commit()

        return magzp

    # ======================================================================
    # This one is a tool used by create_or_load_stack and create_or_load_image

    def _parse_header_info( self, image ):
        with fits.open( image ) as ifp:
            info = {}
            header = ifp[0].header
            info['nx'] = int( header['naxis1'] )
            info['ny'] = int( header['naxis2' ] )
            wcs = WCS( header )
            coord = wcs.pixel_to_world( info['nx']//2, info['ny']//2 )
            info['ra'] = coord.ra.to(units.deg).value
            info['dec'] = coord.dec.to(units.deg).value
            info['racorners'] = []
            info['deccorners'] = []
            for x, y in zip( [ 0, 0, info['nx']-1, info['nx']-1 ], [ 0, info['ny']-1, info['ny']-1, 0] ):
                coord = wcs.pixel_to_world( x, y )
                info['racorners'].append( coord.ra.to(units.deg).value )
                info['deccorners'].append( coord.dec.to(units.deg).value )
            hdrdict = dict(header)
            # Strip COMMENT and HISTORY out of header (so it can be JSONified)
            tostrip = []
            for key in hdrdict.keys():
                if isinstance( hdrdict[key], fits.header._HeaderCommentaryCards ):
                    tostrip.append( key )
            for key in tostrip:
                del hdrdict[key]
            info['hdrdict'] = hdrdict
            # Check if it's a stack
            if self.isstack(str(image.name)):
                info['mjd'] = None
            else:
                info['mjd'] = self.mjd_from_header( header )
            # TODO ##### Ask Rob if this is an issue (Hardcodes seeing, skysig, and magzp values)
            #... I think so?  They are all curveball-defined header keywords.  Thought required.
            # Rob response: SEEING, SKYSIG, MEDSKY are all written to the header as aprt of
            #  processing.  The others are in photocalib.
            info['seeing'] = float( header['SEEING'] ) if 'SEEING' in header else None
            info['skysig'] = float( header['SKYSIG'] ) if 'SKYSIG' in header else None
            info['medsky'] = float( header['MEDSKY'] ) if 'MEDSKY' in header else None
            info['magzp'] = float( header['MAGZP'] ) if 'MAGZP' in header else None
            info['limiting_mag'] = float( header['LMT_MG'] ) if 'LMT_MG' in header else None
            info['arch_id'] = str(header['ARCHID']).strip() if 'ARCHID' in header else None

        return info

    # ======================================================================

    def create_or_load_stack( self, stack, memberbasenames, band, collection=None,
                              isskysub=True, isastrom=True, isphotom=True, 
                              curdb=None, logger=logging.getLogger('main') ):
        """Either create or load the datbase entry for a stack.  Stack image must already exist.

        """

        stack = pathlib.Path( stack )
        match = self._parseoutfits.search( stack.name )
        if match is None:
            logger.warning( f'Couldn\'t parse {stack.name} for *.fits, using whole thing as basename' )
            stackbase = stack.name
        else:
            stackbase = match.group(1)

        memberbasenamesset = set( memberbasenames )
        with db.DB.get(curdb) as curdb:
            if not isinstance( band, db.Band ):
                raise RuntimeError( "OMG stack loading is broken, figure out what to do!" )
                # ( Band.get_by_name requires an exposuresource, but we don't have one here. )
                bandobj = db.Band.get_by_name( band, self.id )
                if bandobj is none:
                    raise ValueError( f'Unknown band {band}' )
                band = bandobj

            # Make sure all images are in the databse, and pull their ids

            q = curdb.db.query( db.Image.basename, db.Image.id ).filter( db.Image.basename.in_( memberbasenames ) )
            things = q.all()
            dbbasenames = set( [ i[0] for i in things ] )
            memberids = set( [ i[1] for i in things ] )
            if dbbasenames != memberbasenamesset:
                raise ValueError( f"The following images aren't in the database: {memberbasenamesset-dbbasenames}" )

            # Check to see if this stack is already in the database, and just return it if so

            stackobj = db.Image.get_by_basename( stackbase, curdb=curdb )
            if stackobj is not None:
                dbmembers = curdb.db.query( db.StackMember.member_id ).filter( db.StackMember.image_id==stackobj.id )
                dbmemberids = set( [ m[0] for m in dbmembers.all() ] )
                if len( dbmemberids ) == 0:
                    raise RuntimeError( f'Stack {stackbase} is in the database but has no stackmembers!' )
                if dbmembers != memberids: # TODO: fix this
                    raise RuntimeError( f'Stack {stackbase} is already in '+
                                       'the database with different members!' )
                else:
                    return stackobj

            # Check to see if this stack is in the database with a different set of members
            # If there is and it has the same basename, we'll have already found it above

            oneimid = next( iter( memberids ) )
            stackswithfirst = curdb.db.query( db.StackMember ).filter( db.StackMember.member_id==oneimid )
            for stack in stackswithfirst:
                q = curdb.db.query( db.StackMember.member_id ).filter( db.StackMember.image_id==stack.image_id )
                existingstackmembers = set( [ m[0] for m in q ] )
                if existingstackmembers == memberids:
                    raise RuntimeError( f"Stack {stackim.basename} is built from the same set of images as the "
                                        f"one you're trying to build; just use that!" )

            # If we get here, the stack really is new, so create the db object and return it

            hdrinfo = self._parse_header_info( stack )
            ras = hdrinfo['racorners']
            decs = hdrinfo['deccorners']
            stackobj = db.Image( basename=stackbase, band_id=band.id, isstack=True,
                                 isskysub=isskysub, isastrom=isastrom, isphotom=isphotom,
                                 ra=hdrinfo['ra'], dec=hdrinfo['dec'],
                                 ra1=ras[0], ra2=ras[1], ra3=ras[2], ra4=ras[3],
                                 dec1=decs[0], dec2=decs[1], dec3=decs[2], dec4=decs[3],
                                 header=hdrinfo['hdrdict'],
                                 seeing=hdrinfo['seeing'], skysig=hdrinfo['skysig'], magzp=hdrinfo['magzp'],
                                 limiting_mag=hdrinfo['limiting_mag'] )
            curdb.db.add( stackobj )
            curdb.db.commit()
            for member in memberids:
                stackmemobj = db.StackMember( image_id=stackobj.id, member_id=member )
                curdb.db.add( stackmemobj )
            curdb.db.commit()

            return stackobj

    # ======================================================================

    def create_or_load_image( self, image, collection=None, curdb=None, isskysub=None, hasfakes=False,
                              logger=logging.getLogger('main') ):
        """Image must have the filename the database expects!!!!

        Only alters the database.  Does not move the image.  So, put it
        in the right place before calling this.  Image must be
        preprocessed (bias/flat/etc.).

        """
        image = pathlib.Path( image )
        basename = self.image_basename( image.name )
        with db.DB.get(curdb) as curdb:
            imageobj = db.Image.get_by_basename( basename, curdb=curdb )
            if imageobj is not None:
                if imageobj.exposure.exposuresource_id != self._dbinfo.id:
                    raise ValueError( f'Exposure of image {basename} has the wrong exposure source '
                                      f'(is {imageobj.exposure.exposuresource_id}, expected {self._dbinfo.id}' )
            else:
                exposure = self.create_or_load_exposure_for_image( image, collection=collection, curdb=curdb )
                chiptag = self.chiptag( image.name )
                chip = db.CameraChip.get_by_tag( exposure.exposuresource_id, chiptag )
                if chip is None:
                    raise ValueError( f'Unknown chip {chiptag}' )
                info = self._parse_header_info( image )
                nx = info['nx']
                ny = info['ny']
                if ( nx != self._dbinfo.nx ) or ( ny != self._dbinfo.ny ):
                    logger.warning( f'Header dimensions ({nx} × {ny}) '
                                    f'don\'t match expected ({self._dbinfo.nx} × {self._dbinfo.ny})' )
                if isskysub is None:
                    isskysub = self._dbinfo.skysub
                racorners = info['racorners']
                deccorners = info['deccorners']

                # If this is not a preprocessed exposure source, then we know we've made changes
                # to the image since it was downloaded from the original source, so we have to
                # archive it.
                if not self._dbinfo.preprocessed:
                    logger.info( "Images don't come from source preprocessed, "
                                 "so archiving them to curveball archive." )
                    for ext in [ "", ".weight", ".mask" ]:
                        self.upload_file_to_archive( f"{basename}{ext}.fits" )

                imageobj = db.Image( exposure_id=exposure.id, band_id=exposure.band_id, chip_id=chip.id,
                                     basename=basename, isskysub=isskysub, hasfakes=hasfakes,
                                     header=info['hdrdict'],
                                     ra=info['ra'], dec=info['dec'], mjd=info['mjd'],
                                     ra1=racorners[0], dec1=deccorners[0], ra2=racorners[1], dec2=deccorners[1],
                                     ra3=racorners[2], dec3=deccorners[2], ra4=racorners[3], dec4=deccorners[3],
                                     seeing=info['seeing'], medsky=info['medsky'], skysig=info['skysig'], magzp=info['magzp'],
                                     limiting_mag=info['limiting_mag'], archive_identifier=info['arch_id'] )
                curdb.db.add(imageobj)
                curdb.db.commit()
            return imageobj

    # ======================================================================

    def create_or_load_exposure_for_image( self, image, collection=None, curdb=None ):
        """Image must have the filename the database expects!!!!"""
        image = pathlib.Path( image )
        basename = self.exposure_basename( image.name )
        with db.DB.get(curdb) as curdb:
            exposure = db.Exposure.get_by_basename( basename, curdb=curdb )
            if exposure is not None:
                if exposure.exposuresource_id != self._dbinfo.id:
                    raise ValueError( f'Exposure {basename} has the wrong exposure source '
                                      f'(is {exposure.exposuresource_id}, expected {self._dbinfo.id})' )
            else:
                with fits.open( image ) as ifp:
                    header = ifp[0].header
                    ra = float( header[self.ra_degrees_keyword] )
                    dec = float( header[self.dec_degrees_keyword] )
                    filtername = self.filtername_from_header( header )
                    band = db.Band.get_by_name( filtername, self.id, curdb=curdb )
                    if band is None:
                        raise ValueError( f'Filter {filtername} (from header of {image}) is unknown.' )
                    field = self.fieldname_from_header( header )
                    mjd = self.mjd_from_header( header )
                    coord = SkyCoord( ra, dec, unit=units.deg )
                    gallat = coord.galactic.b.to(units.deg).value
                    gallong = coord.galactic.l.to(units.deg).value
                    hdrdict = dict(header)
                    # Strip COMMENT and HISTORY out of header (so it can be JSONified)
                    tostrip = []
                    for key in hdrdict.keys():
                        if isinstance( hdrdict[key], fits.header._HeaderCommentaryCards ):
                            tostrip.append(key)
                    for key in tostrip:
                        del hdrdict[key]

                    exposure = db.Exposure( mjd=mjd, ra=ra, dec=dec, gallat=gallat, gallong=gallong,
                                            exposuresource_id=self._dbinfo.id, band_id=band.id,
                                            basename=basename, fieldname=field, collection=collection,
                                            header=hdrdict )
                    curdb.db.add( exposure )
                    curdb.db.commit()
        return exposure

    # ======================================================================
    # ======================================================================
    # Downloading images from the curveball cache at nersc

    def upload_file_to_archive( self, image, overwrite=True, logger=logging.getLogger("main") ):
        """Upload an archive file to nersc.  image is a filename (not path) of an image or other file."""
        relpath = self.relpath( image )
        localfile = self.local_path( image, logger=logger )
        return self.archive.upload( localfile, relpath.parent, relpath.name, overwrite=overwrite )


    def get_file_from_archive( self, image, outdir=None, overwrite=False, verifymd5=False,
                               logger=logging.getLogger("main") ):
        """Make sure there is a local copy of the file.  Return its absolute path.

        image -- the name of the file to return.  Doesn't have to be an actual image, just something that's on
          the archive.  This is just the name; will use ExposureSource.relpath to figure out where it is
          relative to the base of the storage hierarchy.

        outdir -- Base directory to write this file to.  If None, will try to find the file underneath all of
          the directories spoecified in the 'datadirs' directory in config.  If it doesn't find it, will use
          the first value from the 'datadirs' array in config.  (You almost always want to use None here.)

        overwrite -- If True and the local file already exists, will verify its md5sum with the server and
          download it only if the md5sum doesn't exist.  (If verifymd5 is False, will always redownload.)

        verifymd5 -- If True, will verify that the md5sum of the image matches the md5sum of the file on
          the server.  If it does, and overwrite=True, will redownload the file; otherwise, will raise an
          exception.

        logger -- a logging.logger object (default: gets the logger named "main")

        """
        relpath = self.relpath( image )
        localfile = self.local_path( image, logger=logger ) if outdir is None else pathlib.Path( outdir ) / relpath
        if localfile is None:
            cfg = config.Config.get()
            outdir = cfg.value('datadirs')[0]
            localfile = pathlib.Path( outdir ) / relpath

        self.archive.download( relpath, localfile, verifymd5=verifymd5, clobbermismatch=overwrite )

        return localfile


    def ensure_image_local( self, basename, curdb=None, logger=logging.getLogger("main"), **kwargs ):
        """Ensure that the image, mask, and weight file are all local.

        basename - the basename of the image (matching what's in the database).

        If the exposure source has preprocessed images, then the images
        will be pulled (if needed) from the original exposure source.
        Otherwise, it will be pulled from curveball's own archive.

        For stacks, always pull from curveball's own archive.

        Doesn't return anything.

        """

        with db.DB.get(curdb) as dbo:
            imgobj = db.Image.get_by_basename( basename, curdb=dbo )
            if imgobj is None:
                raise RuntimeError( f"Image {basename} isn't in the database" )

            if self._dbinfo.preprocessed and ( not self.isstack( basename ) ):
                self.ensure_sourcefiles_local( basename, curdb=dbo, logger=logger, **kwargs )

            else:
                for ext in [ "", ".weight", ".mask" ]:
                    self.get_file_from_archive( f"{basename}{ext}.fits", logger=logger )


    def delete_file_on_archive( self, image, okifmissing=True ):
        """Deletes a file from the archive.  image is a filename (not path) of an image or other file."""
        relpath = self.relpath( image )
        return self.archive.delete( relpath, okifmissing=okifmissing )

    # ======================================================================
    # ======================================================================
    # Downloading images from the actual source represented by this ExposureSource

    def ensure_sourcefiles_local( self, basename, retries=5, curdb=None, logger=logging.getLogger("main") ):
        """Ensure image, mask, and weight are in local datadirs.

        If they aren't, go to the online archive for the ExposureSource
        and pull the three images down again (or regenerate them as
        appropriate).  Make sure the mask file is 0=good, 1=bad.  Make
        sure that headers are updated for WCS and photometric
        calibration if isastrom and isphotom respectively in the
        database are set to true.

        """
        raise NotImplementedError( f'ensure_sourcefiles_local not implemented for {self.__class__.__name__}' )

    def find_images( self, ra, dec, containing=True, dra=0.008, ddec=0.008,
                     band=None, starttime=None, endtime=None, minexptime=None,
                     logger=logging.getLogger("main") ):

        """Find images from this image source.

        Not implemented for all sources.

        NOTE : starttime and endtime are input in MJD, not JD!

        Returns an opaque object that you pass to futher methods of the
        ExposureSource.  (You can use len() on it to figure out the
        number of images, though there's no guarantee that there aren't
        duplicates.)

        This iterable object needs columns:
        - obsmjd
        - filtercode
        - maglimit
        - exptime
        - seeing
        - obsdate
        - airmass
        - moonillf

        """
        raise NotImplementedError( f'find_images not implemented for {self.__class__.__name__}' )

    # ======================================================================

    def blob_image_name( self, imageblob, index, filetype="image", logger=logging.getLogger("main") ):
        """Returns curveball name of image for index in the blob returned by find_images"""
        raise NotImplementedError( f'image_name not implemented for {self.__class__.__name__}' )

    # ======================================================================

    def blob_image_path( self, imageblob, index, rootdir=None, filetype="image",
                    logger=logging.getLogger("main") ):
        """Returns an absolute pathlib.Path for image index in the blob returned by find_images

        rootdir : data root directory (defaults to first directory in config datadirs)
        """
        cfg = config.Config.get()
        if rootdir is None:
            rootdir = pathlib.Path( cfg.value('datadirs')[0] )
        else:
            rootdir = pathlib.Path( rootdir )
        filename = self.blob_image_name( imageblob, index, filetype=filetype, logger=logger )
        return rootdir / self.archivesubdir( filename ) / filename

    # ======================================================================

    def blob_image_mjd( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns MJD of image for index in the blob returned by find_images"""
        raise NotImplementedError( f'blob_image_mjd not implemented for {self.__class__.__name__}' )

    # ======================================================================

    def blob_image_exptime( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns exposure time (seconds) of image for index in the blob returned by find_images"""
        raise NotImplementedError( f'blob_image_exptime not implemented for {self.__class__.__name__}' )

    # ======================================================================

    def blob_image_filter( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns some sort of filter name for index in the blob returned by find_images

        This needs to be better defined.
        """
        raise NotImplementedError( f'blob_image_filter not implemented for {self.__class__.__name__}' )

    # ======================================================================

    def blob_image_maglim( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns a limiting mag (ideally 5σ, but *something* for image image in the blob from find_images"""
        # This assumes that blob is a pandas dataframe, which it is so far for everything
        # I've implemented.  Subclass this if this code doesn't work for your class.
        return( imageblob.iloc[index]['maglimit'] )

    # ======================================================================

    def download_blob_image( self, imageblob, index, rootdir=None, filename=None,
                             files=['image','mask'], logger=logging.getLogger("main") ):
        """Download an image found by find_images.

        imageblob — what was returned by find_images
        index — the index into imageblob of which image to download
        root — root data directory; image will go to archivesubdir below this
                  (default: config value datadirs[0] )
        filename — name of the file; if None, will use a default for the source.
        files — which files 

        NOTE: Built-in assumptions in the software requires this to download FITS files
            instead of .fits.fz files (must unpack files somehow)

        Returns a list of files written (pathlib.Path objects).

        """
        raise NotImplementedError( f'download_image not implemented for {self.__class__.__name__}' )


    # ======================================================================

    # def ensure_image_local( self, image, outdir=None, retries=5, logger=logging.getLogger("main") ):
    #     """Make sure that the indicated image is local.  Does NOT check if the
    #     image is in the database.  Returns path.

    #     """
    #     raise NotImplementedError( f'ensure_image_local not implemented for {self.__class__.__name__}' )

    # ======================================================================
    def _filter_refs(self, images, fcode, days_before, days_after, seeing_below,
                     maglimit_above, airmass_below, bw=10, logger=logging.getLogger("main")):
        """ Filter data and create clusters of similar timeframes """
        # good_data = images[(images.seeing<seeing_below) & (images.maglimit > maglimit_above) & 
        #                    ((images.tdiff<-days_before) | (images.tdiff>days_after))
        #                   ].sort_values('tdiff')
        good_data = images[ (images.tdiff<-days_before) | (images.tdiff>days_after) ].sort_values('tdiff')
        ngood_trange = len(good_data)
        good_data = good_data[ good_data.seeing < seeing_below ]
        ngood_seeing = len(good_data )
        good_data = good_data[ good_data.maglimit > maglimit_above ]
        ngood_maglimit = len( good_data )

        # Check for data
        if good_data.empty: return good_data

        # Cluster data by timeframe
        kde = KernelDensity(kernel='linear', bandwidth=bw)
        data = good_data['tdiff'].to_numpy().reshape(-1,1)
        kde.fit(data)
        # Score according to cluster strength
        scores = kde.score_samples(data)
        # Minima in cluster strength are breaks between clusters
        mi, ma = argrelextrema(scores, numpy.less)[0], argrelextrema(scores, numpy.greater)[0]

        # Group cluster indices
        x = good_data.index.to_numpy()
        start = 0
        clusters = []
        for m in mi: # Each minimum is a break
            clusters.append(x[start:m])
            start = m
        # Last cluster
        clusters.append(x[start:len(x)])

        # Put cluster indices in dataframe
        for i in range(len(clusters)):
            good_data.loc[clusters[i], 'cluster'] = i

        # Filter for airmass and band
        good_data = good_data[(good_data.filtercode==fcode) & (good_data.airmass<airmass_below)]

        logger.debug( f"{ngood_trange} images in tranage; {ngood_seeing} w/ seeing<{seeing_below:.2f}, "
                      f"{ngood_maglimit} w/ maglimit>{maglimit_above:.2f}, "
                      f"{len(good_data)} w/ airmass<{airmass_below:.2f}" )

        # Ensure clusters param is integer
        good_data['cluster'] = good_data['cluster'].astype(int)

        return good_data

    # ======================================================================

    def _bestcut(self, good_data, outlier_days=75, min_count=11, img_per_day=0.2, logger=logging.getLogger("main")):
        """Tries to find the best cluster of images for making a reference.

        Determines if the filter parameters are too tight based on
        whether suitable reference images can be found.

        good_data - input dataframe, blob from the image finding
          thing. (TODO: document which fields are assume dpresent?)

        outlier_days - images further from the median of their cluster
          than this many days will be dropped from consideration

        min_count - clusters must have at least this many images to be
          considered

        img_per_day - clusters must have at least this many images per
          day to be considered

        """
        # Force datatypes because of the handling of the "-k" option to /bin/make_reference.py
        outlier_days = int( outlier_days )
        img_per_day = float( img_per_day )

        # Check for data
        if good_data.empty: return None

        # Take out outliers in each cluster
        for i, group in good_data.groupby('cluster'):
            # Get median of days
            med = group.tdiff.median()
            if outlier_days > 0:
                # Drop rows too far from median
                for j, row in group.iterrows():
                    if numpy.abs(row.tdiff-med) > outlier_days:
                        good_data.drop(index=j, inplace=True)
        nleft_after_outliers = len(good_data)

        ### Estimate the best cluster
        clusters = good_data.groupby('cluster')
        scores = pandas.DataFrame()
        scores['num'] = clusters.size()
        scores['imgpday'] = clusters.size() / (clusters.tdiff.max() 
                                                      - clusters.tdiff.min()) # avg images / day
        scores['seeing'] = clusters.seeing.mean()
        totnclusters = len( scores )

        # Drop infinities
        scores.replace([numpy.inf, -numpy.inf], numpy.nan, inplace=True)
        scores.dropna(inplace=True)
        nleft_after_dropna = len( scores )

        # Make sure there are enough images
        scores = scores[scores.num>=min_count]
        nleft_after_mincount = len( scores )
        if img_per_day > 0:
            scores = scores[scores.imgpday>img_per_day]
        # Cast, just in case
        scores['seeing'] = scores['seeing'].astype(float)

        logger.debug( f"{nleft_after_outliers} images after outlier rejection; "
                      f"{totnclusters} clusters, {nleft_after_dropna} w/o NaN, "
                      f"{nleft_after_mincount} w/ num>={min_count}, "
                      f"{len(scores)} w/ img_per_day>{img_per_day}" )

        # Return cluster with highest seeing, or None if nothing makes the cut
        if scores.empty: return None
        else: return scores.seeing.idxmin()


    # ======================================================================

    def secure_references( self, ra, dec, peakt, maxseeing=2.2, maxairmass=2.0, maglimit=18.5,
                           band=None, searchlocal=False, outdir=None, random_seed=42,
                           cluster_outlier_days=75, cluster_img_per_day=0.2, min_count=11,
                           curdb=None, overwrite=False, logger=logging.getLogger("main") ):
        """Identify and (if necessary) download refs for SN at ra/dec with peak time peakt (in JD)

        Returns a dictionary whose keys are the filter codes for the
        corresponding band, and whose values are lists of image
        basenames (*not* paths).

        ra - RA of SN
        dec - Dec of SN
        peakt - Peak time of supernova in MJD
        band - db.Band object, or band name.  Default: will get everything on the source
        searchlocal - If True, only search the local database; default: search exposure source
        outdir - root data directory (default: first one in the config)
        curdb - a db.DB object (optional)
        logger - a logging.Logger object (optional)

        """
        # Set random seed
        numpy.random.seed(random_seed)

        # Open database or return existing one
        with db.DB.get(curdb) as curdb:
            if band is not None:
                if not isinstance( band, db.Band ):
                    bandobj = db.Band.get_by_name( band, self.id, curdb=curdb )
                    if bandobj is None:
                        raise ValueError( f'Unknown band {band}' )
                    band = bandobj

            if outdir is None:
                outdir = pathlib.Path( config.Config.get().value("datadirs")[0] )
            else:
                outdir = pathlib.Path( outdir )

            if searchlocal:
                images = db.Image.get_including_point( ra, dec, band=band, expsourceid=self._dbinfo.id,
                                                       curdb=curdb )
                imgdict = {}
                imgdict['filtercode'] = [ image.band.filtercode for image in images ]
                imgdict['obsjd'] = [ image.exposure.mjd + 2400000.5 for image in images ]
                imgdict['basename'] = [ image.basename for image in images ]
                imgdict['maglimit'] = [ image.limiting_mag for image in images ]
                imgdict['exptime'] = [ image.exposure.header['EXPTIME'] for image in images ]
                imgdict['seeing'] = [ image.seeing for image in images ]
                imgdict['obsdate'] = [ util.julday_to_datetime( image.exposure.mjd + 2400000.5 ) 
                                      for image in images ]
                imgdict['airmass'] = [ image.exposure.header['AIRMASS'] for image in images ]
                imgdict['moonillf'] = [ 0 for image in images ] # What is this?

                images = pandas.DataFrame( imgdict )
            else:
                images = self.find_images( ra, dec, band=band, logger=logger )
                basenames = [ self.image_basename( self.blob_image_name( images, i ) )
                             for i in range(len(images)) ]
                images['basename'] = basenames

            startnimages = len(images)

            ############ Emily's code #########################################
            logger.info(f"Securing references from {startnimages} images.")
            images['tdiff'] = images.obsmjd - peakt # Calculate time from peak

            filtercodes = images['filtercode'].unique()
            filtersubset = {fc: None for fc in filtercodes}
            # TODO: enforce a similar zero point for all reference images
            for fc in filtercodes:
                # Start at low seeing/maglimit/airmass and gradually allow higher ones
                best = None
                for b in numpy.arange(10, 60, 10): # bandwidth loop
                    for a in numpy.arange(1.05, maxairmass+.05, .05): # airmass loop
                        for m in numpy.arange(maglimit+2, maglimit-.1, -.1): # maglimit loop (counts down)
                            for s in numpy.arange(1.6, maxseeing+.05, .05): # seeing loop
                                # Get filtered and clustered data
                                good_data = self._filter_refs(images, fcode=fc, days_before=30, 
                                                  days_after=365, seeing_below=s, maglimit_above=m,
                                                  airmass_below=a, bw=b, logger=logger) # TODO: less hardcoded stuff
                                if good_data.empty: continue

                                # Get the best cluster
                                best = self._bestcut(good_data, min_count=min_count, outlier_days=cluster_outlier_days,
                                                     img_per_day=cluster_img_per_day, logger=logger)
                                if best is not None:
                                    logger.info(f"Found good cluster: {best}")
                                    break # Found good cluster
                            else: continue
                            break # end maglimit loop
                        else: continue
                        break # end airmass loop
                    else: continue
                    break # end bandwidth loop

                if best is None: # Nothing found
                    logger.warning(f"No images found for filter {fc}")
                    continue

                # Get image locations
                ref_idxs = good_data[good_data.cluster==best].index

                # Cut if too many images
                if len(ref_idxs) > 20:
                    cluster = good_data.loc[ref_idxs].sort_values('seeing')
                    ref_idxs = cluster.index[:20]

                # Get full filtered dataset
                filtered_data = good_data.loc[ref_idxs]
                filtersubset[fc] = filtered_data # Put in Rob's structure

                # Log reference images
                info = io.StringIO()
                info.write( f"Images for {fc} : {len(filtered_data)} out of {startnimages}...\n" )
                info.write( f'{"Obs. Date":26s}  t_exp airmass moon  m_lim  seeing\n' )
                for i, row in filtered_data.iterrows():
                    info.write( f'{row["obsdate"]:26s}  {row["exptime"]:5.1f}  {row["airmass"]:4.1f} '
                                f'{row["moonillf"]:5.2f}   {row["maglimit"]:3.1f} {row["seeing"]:5.2f}\n' )
                logger.info( info.getvalue() )
                info.close()
            ###########################################################
            # Download images

            refs = {}
            for fc in filtercodes:
                logger.info( f'Securing references for {fc}' )
                refs[fc] = []
                if filtersubset[fc] is None:
                    logger.warning(f"No references found for filter {fc}")
                    continue
                for i in range(len(filtersubset[fc])):
                    basename = filtersubset[fc].iloc[i]['basename']
                    logger.debug( f'Searching database for image basename {basename}' )
                    imgobj = db.Image.get_by_basename( basename, curdb=curdb )
                    if imgobj is not None:
                        logger.info( f'Image {basename} is already in the database' )
                        self.ensure_sourcefiles_local( basename, curdb=curdb, logger=logger )

                    # imagename = f'{basename}.fits'
                    # maskname = f'{basename}.mask.fits'
                    # weightname = f'{basename}.weight.fits'

                    if overwrite:
                        imagefile = None
                        maskfile = None
                        weightfile = None
                        # ROB!!!!!!!  Broken if there are multiple dataidrs
                        # You need to search and destroy.
                    else:
                        imagefile = self.local_path_from_basename( basename, logger=logger )
                        maskfile = self.local_path_from_basename( basename, filetype='mask',
                                                                 logger=logger )
                        weightfile = self.local_path_from_basename( basename, filetype='weight',
                                                                   logger=logger )
                    needed = []
                    if ( imagefile is None ): needed.append( 'image' )
                    if ( maskfile is None ): needed.append( 'mask' )
                    if len(needed) > 0:
                        countdown = 5
                        success = False
                        while countdown > 0:
                            try:
                                downloaded = self.download_blob_image( filtersubset[fc], i, files=needed,
                                                                       rootdir=outdir, overwrite=overwrite,
                                                                       logger=logger )
                                countdown = 0
                                success = True
                            except Exception as e:
                                countdown -= 1
                                if countdown > 0:
                                    logger.warning( f'Exception trying to download image {basename}, '
                                                    f'retrying: {str(e)}' )
                                    time.sleep(1)
                                else:
                                    logger.error( f'Failed to download image {basename}, skipping it' )
                        if not success:
                            continue

                        for dlimage in downloaded:
                            ftype = self.filetype( dlimage )
                            if ftype == "image":
                                imagefile = outdir / dlimage
                                logger.info( f'Downloaded image str{imagefile}' )
                            elif ftype == "mask":
                                maskfile = outdir / dlimage
                                logger.info( f'Downloaded mask str{maskfile}' )
                            else:
                                logger.warning( f'Unknown file type {ftype} for {dlimage}' )

                    if weightfile is None:
                        logger.info( f'Building weight for {imagefile.name}' )
                        processing.make_weight( self, imagefile, mask=maskfile, logger=logger )

                    if not imgobj:
                        self.create_or_load_image( imagefile, curdb=curdb, logger=logger )

                    refs[fc].append( self.image_basename( imagefile.name ) )

        return( refs )

# ======================================================================

# def main():
#     pass

# if __name__ == "__main__":
#     main()
