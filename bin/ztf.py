import sys
import os
import io
import re
import math
import requests
import pathlib
import urllib.parse
import numpy
import pandas
import logging

from astropy.io import fits
from sklearn.neighbors import KernelDensity
from scipy.signal import argrelextrema

from exposuresource import *
from irsasurvey import *
import config
import db
import util
import processing
import astrometry
from util import cls_init

random_seed = 42

class ZTF(IRSASurvey):
    NAME = "ZTF"
    SECONDARY = None

    _irsa_query_columns = None
    _searchurlbase = "https://irsa.ipac.caltech.edu/ibe/search/ztf/products/sci"
    _dataurlbase   = "https://irsa.ipac.caltech.edu/ibe/data/ztf/products/sci"
    # Images come from ZTF as: ztf_{yyyymmdd}{dayfrc}_{fieldn}_{fc}_c{ccd}_i{imgtyp}_q{q}_sciimg.fits
    # I rewire them to:
    # ztf_{yyyymmdd}{dayfrc}_{fieldn}_{fc}_i{imgtyp}.c{ccd}{q}.fits
    #  dayfrc = fraction of day (6 digits)
    #  fieldn = field id (6 digits)
    #  fc = filter code (1 or 2 letters: zg, zr, zi, g, r, i)
    #  imgtyp = o (on-sky), f (flat), b (bias)
    #  ccd = ccdid (2 digits)
    #  q = quadrant id (1 digit)
    #  group 1: basename (up to .ccdq.fits)
    #  group 2: "stack_" or ""
    #  group 3: yyyymmdd
    #  group 4: dayfrac
    #  group 5: fieldn
    #  group 6: fc
    #  group 7: mgtyp
    # For exposures:
    #  group 8: file type (_exposuretypeparse only)
    # For images:
    #  group 8: {ccd}{q} (What the database just calls chiptag)
    #  group 9: file type (_imagetypeparse only)
    # _generalparse =      re.compile( r'^((stack_)?ztf_([0-9]{8})([0-9]{6})_([0-9]{6})_(..?)_i(.))' )
    # _exposureparse =     re.compile( r'^((stack_)?ztf_([0-9]{8})([0-9]{6})_([0-9]{6})_(..?)_i(.))\.fits(\.[fg]z)?$' )
    # _imageparse =        re.compile( r'^((stack_)?ztf_([0-9]{8})([0-9]{6})_([0-9]{6})_(..?)_i(.))\.([0-9]{3})\.fits(\.[fg]z)?$' )
    # _exposuretypeparse = re.compile( r'^((stack_)?ztf_([0-9]{8})([0-9]{6})_([0-9]{6})_(..?)_i(.))\.([^\.]+)\.fits(\.[fg]z)?$' )
    # _imagetypeparse =    re.compile( r'^((stack_)?ztf_([0-9]{8})([0-9]{6})_([0-9]{6})_(..?)_i(.))\.([0-9]{3})\.([^\.]+)\.fits(\.[fg]z)?$' )

    # _match_basename = 1
    # _match_stack = 2
    # _match_yyyymmdd = 3
    # _match_dayfrac = 4
    # _match_fieldn = 5
    # _match_filter = 6
    # _match_imagetype = 7
    # _match_exposure_filetype = 8
    # _match_image_ccd = 8
    # _match_image_filetype = 9

    _generalparse =  re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?ztf_(?P<yyyymmdd>[0-9]{8})'
                                 r'(?P<dayfrac>[0-9]{6})_(?P<fieldn>[0-9]{6})_(?P<filter>..?)_i(?P<imgtype>.))' )
    _exposureparse = re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?ztf_(?P<yyyymmdd>[0-9]{8})'
                                 r'(?P<dayfrac>[0-9]{6})_(?P<fieldn>[0-9]{6})_(?P<filter>..?)_i(?P<imgtype>.))'
                                 r'\.fits(?P<fz>\.[fg]z)?$' )
    _imageparse =    re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?ztf_(?P<yyyymmdd>[0-9]{8})'
                                 r'(?P<dayfrac>[0-9]{6})_(?P<fieldn>[0-9]{6})_(?P<filter>..?)_i(?P<imgtype>.))'
                                 r'\.(?P<chiptag>[0-9]{3})\.fits(?P<fz>\.[fg]z)?$' )
    _exposuretypeparse = re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?ztf_(?P<yyyymmdd>[0-9]{8})'
                                     r'(?P<dayfrac>[0-9]{6})_(?P<fieldn>[0-9]{6})_(?P<filter>..?)_i(?P<imgtype>.))'
                                     r'(?P<filetype>[^\.]+)\.fits(?P<fz>\.[fg]z)?$' )
    _imagetypeparse =    re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?ztf_(?P<yyyymmdd>[0-9]{8})'
                                     r'(?P<dayfrac>[0-9]{6})_(?P<fieldn>[0-9]{6})_(?P<filter>..?)_i(?P<imgtype>.))'
                                     r'\.(?P<chiptag>[0-9]{3})\.(?P<filetype>[^\.]+)\.fits(?P<fz>\.[fg]z)?$' )

    _match_basename = 'basename'
    _match_fake = 'fake'
    _match_stack = 'stack'
    _match_yyyymmdd = 'yyyymmdd'
    _match_dayfrac = 'dayfrac'
    _match_fieldn = 'fieldn'
    _match_filter = 'filter'
    _match_imagetype = 'imgtype'
    _match_exposure_filetype = 'imagetype'
    _match_image_ccd = 'chiptag'
    _match_image_filetype = 'filetype'


    _magzpunc_header_keyword = 'MAGZPUNC'
    
    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )

    # ======================================================================
    # IRSA query thing to add a filter

    def filter_find_query( self, band ):
        return f"filtercode='{band.filtercode}'"
        
    # ======================================================================
    # Basic information about exposures from this Exposure Source
    
    @property
    def readnoise_keyword(self):
        return "READNOI"

    @property
    def darkcurrent_keyword(self):
        return "DARKCUR"

    @property
    def exptime_keyword(self):
        return "EXPOSURE"
    
    @property
    def filter_keyword(self):
        return "FILTER"
    
    @property
    def mjd_keyword(self):
        return "OBSMJD"

    @property
    def key_keywords(self):
        return ( 'ORIGIN,OBSERVER,INSTRUME,IMGTYPE,FILTERID,FILTER,FILTPOS,RA,DEC,OBSERVAT,TELESCOPE,'
                 'PIXSCALX,PIXSCALY,GAIN,SATURATE,DBFIELD,MAGZP,MAGZPUNC' )

    # ======================================================================
    # Information parsed from image headers
    
    def fieldname_from_header(self, header):
        return str( int( header['FIELDID'] ) )

    # ZTF isn't always consistent about their filters,
    #   so parse a pattern and turn it into a standard
    def filtername_from_header( self, header ):
        filt = header['FILTER'].strip()
        match = re.search( '^ZTF[_ ]*(.)$', filt )
        if match is None:
            raise RuntimeError( f"Can't parse filter from {filt}" )
        return f"ZTF_{match.group(1)}"
    
    # ======================================================================
    # ZTF images come photometrically calibrated, so just make sure the
    #  header has the right stuff
    #
    # Longer term, I probably want to do my own
    #
    # ZTF downloaded images come with MAGZP, MAGZPUNC, MAGZPRMS, and MAGLIM.
    #
    # Of the few images I've looked at, MAGZPUNC is absurdly small (10^-5),
    #  but MAGZPRMS is bigger than I'd expect a real uncertainty to be
    #  (0.03).  Go with MAGZPUNC for now, but feel queasy about it.
    
    def photocalib_headeronly( self, image, cat=None ):
        with fits.open( image, mode="update" ) as hdu:
            hdu[0].header['MAGZPERR'] = hdu[0].header['MAGZPUNC']
            magzp = float( hdu[0].header['MAGZP'] )
            seeing = float( hdu[0].header['SEEING'] )
            skysig = float( hdu[0].header['SKYSIG'] )
            # Assume aperture photometry with an aperture of radius 1 FWHM
            hdu[0].header['LMT_MG'] = -2.5 * math.log10( 5 * math.sqrt(math.pi) * skysig * seeing ) + magzp
            return magzp
            
    
    # ======================================================================
    # ======================================================================
    # Downloading images from IRSA server

    def source_url( self, image, logger=logging.getLogger("main") ):
        match = self._imagetypeparse.search( image )
        if match is None:
            match = self._imageparse.search( image )
            image_filetype = ''
        else:
            image_filetype = match.group( self._match_image_filetype )
        basename = match.group(self._match_basename)
        yyyymmdd = match.group(self._match_yyyymmdd)
        yyyy = yyyymmdd[0:4]
        mmdd = yyyymmdd[4:8]
        dayfrac = match.group(self._match_dayfrac)
        fieldn = match.group(self._match_fieldn)
        filter = match.group(self._match_filter)
        imagetype = match.group(self._match_imagetype)
        ccdquad = match.group(self._match_image_ccd)
        ccd = ccdquad[0:2]
        quad = ccdquad[2]
        filebase = f'ztf_{yyyymmdd}{dayfrac}_{fieldn}_{filter}_c{ccd}_{imagetype}_q{quad}'
        urlbase = f'{self._dataurlbase}/{yyyy}/{mmdd}/{dayfrac}/{filebase}'
        if image_filetype == '':
            url = f'{urlbase}_sciimg.fits'
        elif image_filetype == 'mask':
            url = f'{urlbase}_mskimg.fits'
        else:
            logger.error( f'ZTF archive doesn\'t have image filetype {image_filetype}' )
            url = None
        return url

    def blob_image_name( self, blob, index, filetype="image", logger=logging.getLogger("main") ):
        if ( index >= len(blob) ):
            raise IndexError( f'Index {index} into imageblob is greater than length {len(blob)}' )
        row = blob.iloc[index]
        filefracday = str( row.filefracday )
        yyyy = filefracday[0:4]
        mmdd = filefracday[4:8]
        fracday = filefracday[8:14]
        filename = ( f"ztf_{filefracday}_{row.field:06d}_{row.filtercode}_"
                     f"i{row.imgtypecode}.{row.ccdid:02d}{row.qid:1d}" )
        if filetype != "image":
            filename += f".{filetype}"
        filename += ".fits"
        return filename

    # ======================================================================

    def blob_image_filter( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns some sort of filter name for index in the blob returned by find_images

        This needs to be better defined.
        """
        return imageblob.iloc[index].filtercode

    # ======================================================================
    # Has some redundant code with blob_image_name
    
    def download_blob_image( self, tab, index, rootdir=None, files=['image', 'mask'],
                             overwrite=False, logger=logging.getLogger("main") ):
        if not isinstance( tab, pandas.DataFrame ):
            raise ValueError( 'download_image: Passed image blob isn\'t the right sort of thing.' )
        if index > len(tab):
            raise IndexError( f'index {index} is greater than number of images {len(tab)} in image blob.' )

        cfg = config.Config.get()
        
        row = tab.iloc[index]

        if rootdir is None:
            rootdir = pathlib.Path( cfg.value('datadirs')[0] )
        else:
            rootdir = pathlib.Path( rootdir )
        filefracday = str( row.filefracday )
        yyyy = filefracday[0:4]
        mmdd = filefracday[4:8]
        fracday = filefracday[8:14]

        filebase = ( f"ztf_{filefracday}_{row.field:06d}_{row.filtercode}_c{row.ccdid:02d}_"
                     f"{row.imgtypecode}_q{row.qid}" )
        defoutfilestem = ( f"ztf_{filefracday}_{row.field:06d}_{row.filtercode}_"
                           f"i{row.imgtypecode}.{row.ccdid:02d}{row.qid:1d}" )
        
        filename = f"{defoutfilestem}.fits"
        match = self._parseoutfits.search( str(filename) )
        if match is None:
            raise ValueError( f'Error parsing {filename} for *.fits, *.fits.fz, or *.fits.gz' )
        outfilebase = match.group(1)

        filedir = rootdir / self.archivesubdir( filename )
        filedir.mkdir( parents=True, exist_ok=True )
        urlbase = f"{self._dataurlbase}/{yyyy}/{mmdd}/{fracday}/{filebase}"

        dlfiles = []
        for filetype in files:
            if filetype == "image":
                url = f"{urlbase}_sciimg.fits"
                outfile = f"{outfilebase}.fits"
            elif filetype == "mask":
                url = f"{urlbase}_mskimg.fits"
                outfile = f"{outfilebase}.mask.fits"
            else:
                logger.warning( f'ZTF archive doesn\'t have file type "{filetype}", not downloading.' )
                continue
            if (filedir/outfile).exists():
                if not (filedir/outfile).is_file():
                    raise RuntimeError( f'File {str(filedir/outfile)} exists and is not a file!' )
                if not overwrite:
                    raise RuntimeError( f'File {str(filedir/outfile)} already exists, won\'t overwrite' )
                logger.warning( f'Deleting existing file {str(filedir/outfile)}' )
                (filedir/outfile).unlink()
            logger.debug( f'Querying url {url}' )
            res = requests.get( f"{url}" )
            if res.status_code != 200:
                raise Exception( f'ZTF query returned status {res.status_code}' )
            logger.debug( f'ZTF query returned type {res.headers["Content-Type"]}' )
            if filetype != "mask":
                with open( filedir / outfile, "wb" ) as ofp:
                    ofp.write( res.content )
            else:
                self.wrangle_mask( res.content, filedir / outfile, outfilebase )
            dlfiles.append( filedir / outfile )

        return dlfiles

    # ======================================================================

    # def ensure_image_local( self, image, outdir=None, retries=5, logger=logging.getLogger("main") ):
    #     cfg = config.Config.get()
    #     match = self._imageparse.search( image )
    #     if match is None:
    #         match = self._exposuretypeparse.search( image )
    #         if match is None:
    #             raise ValueError( f'Error parsing filename {image}' )
    #         exptype = match.group(self._match_exposure_filetype)
    #     else:
    #         exptype = "fits"
    #     relpath = self.relpath( image )
        
    #     for datadir in cfg.value('datadirs'):
    #         testfile = pathlib.Path( datadir ) / relpath
    #         if testfile.exists():
    #             if testfile.is_file():
    #                 return testfile
    #             else:
    #                 raise RuntimeError( f'{testfile} exists but is not a regular file!' )
    #     if outdir is None:
    #         outdir = cfg.value('datadirs')[0]
    #     outdir = pathlib.Path( outdir )
    #     localfile = outdir / relpath
    #     countdown = retries
    #     while countdown >= 0:
    #         try:
                
            
        

    
    # ======================================================================
#     def _filter_refs(self, images, fcode, days_before, days_after, seeing_below,
#                      maglimit_above, airmass_below, bw=10):
#         """ Filter data and create clusters of similar timeframes """
#         good_data = images[(images.seeing<seeing_below) & (images.maglimit > maglimit_above) & 
#                            ((images.tdiff<-days_before) | (images.tdiff>days_after))
#                           ].sort_values('tdiff')
        
#         # Check for data
#         if good_data.empty: return good_data
        
#         # Cluster data by timeframe
#         kde = KernelDensity(kernel='linear', bandwidth=bw)
#         data = good_data['tdiff'].to_numpy().reshape(-1,1)
#         kde.fit(data)
#         # Score according to cluster strength
#         scores = kde.score_samples(data)
#         # Minima in cluster strength are breaks between clusters
#         mi, ma = argrelextrema(scores, numpy.less)[0], argrelextrema(scores, numpy.greater)[0]

#         # Group cluster indices
#         x = good_data.index.to_numpy()
#         start = 0
#         clusters = []
#         for m in mi: # Each minimum is a break
#             clusters.append(x[start:m])
#             start = m
#         # Last cluster
#         clusters.append(x[start:len(x)])

#         # Put cluster indices in dataframe
#         for i in range(len(clusters)):
#             good_data.loc[clusters[i], 'cluster'] = i
        
#         # Filter for airmass and band
#         good_data = good_data[(good_data.filtercode==fcode) & (good_data.airmass<airmass_below)]
        
#         # Ensure clusters param is integer
#         good_data['cluster'] = good_data['cluster'].astype(int)

#         return good_data
    
#     # ======================================================================
    
#     def _bestcut(self, good_data, outlier_days=75, min_count=11, img_per_day=0.2):
#         """ 
#         Determines if the filter parameters are too tight based on whether 
#         suitable reference images can be found.
#         """
#         # Check for data
#         if good_data.empty: return None
    
#         # Take out outliers in each cluster
#         for i, group in good_data.groupby('cluster'):
#             # Get median of days
#             med = group.tdiff.median()
#             for j, row in group.iterrows():
#                 # Drop rows too far from median
#                 if numpy.abs(row.tdiff-med) > outlier_days:
#                     good_data.drop(index=j, inplace=True)
        
#         ### Estimate the best cluster
#         clusters = good_data.groupby('cluster')
#         scores = pandas.DataFrame()
#         scores['num'] = clusters.size()
#         scores['imgpday'] = clusters.size() / (clusters.tdiff.max() 
#                                                       - clusters.tdiff.min()) # avg images / day
#         scores['seeing'] = clusters.seeing.mean()
        
#         # Drop infinities
#         scores.replace([numpy.inf, -numpy.inf], numpy.nan, inplace=True)
#         scores.dropna(inplace=True)
        
#         # Make sure there are enough images
#         scores = scores[scores.num>=min_count]
#         scores = scores[scores.imgpday>img_per_day]
#         # Cast, just in case
#         scores['seeing'] = scores['seeing'].astype(float)
        
#         # Return cluster with highest seeing, or None if nothing makes the cut
#         if scores.empty: return None
#         else: return scores.seeing.idxmin()
    
#     # ======================================================================
    
#     def secure_references( self, ra, dec, peakt, maxseeing=2.2, maxairmass=1.5, maglimit=18.5,
#                           band=None, searchlocal=False, outdir=None, curdb=None, 
#                           overwrite=False, logger=logging.getLogger("main") ):
#         # Set random seed
#         numpy.random.seed(random_seed)
        
#         # Open database or return existing one
#         with db.DB.get(curdb) as curdb:
#             if band is not None:
#                 if not isinstance( band, db.Band ):
#                     bandobj = db.Band.get_by_name( band, curdb=curdb )
#                     if bandobj is None:
#                         raise ValueError( f'Unknown band {band}' )
#                     band = bandobj

#             if outdir is None:
#                 outdir = pathlib.Path( config.Config.get().value("datadirs")[0] )
#             else:
#                 outdir = pathlib.Path( outdir )

#             if searchlocal:
#                 images = db.Image.get_including_point( ra, dec, band=band, expsourceid=self._dbinfo.id,
#                                                        curdb=curdb )
#                 imgdict = {}
#                 imgdict['filtercode'] = [ image.band.filtercode for image in images ]
#                 imgdict['obsjd'] = [ image.exposure.mjd + 2400000.5 for image in images ]
#                 imgdict['basename'] = [ image.basename for image in images ]
#                 imgdict['maglimit'] = [ image.limiting_mag for image in images ]
#                 imgdict['exptime'] = [ image.exposure.header['EXPTIME'] for image in images ]
#                 imgdict['seeing'] = [ image.seeing for image in images ]
#                 imgdict['obsdate'] = [ util.julday_to_datetime( image.exposure.mjd + 2400000.5 ) 
#                                       for image in images ]
#                 imgdict['airmass'] = [ image.exposure.header['AIRMASS'] for image in images ]
#                 imgdict['moonillf'] = [ 0 for image in images ]

#                 images = pandas.DataFrame( imgdict )
#             else:
#                 images = self.find_images( ra, dec, band=band, logger=logger )
#                 basenames = [ self.image_basename( self.blob_image_name( images, i ) ) 
#                              for i in range(len(images)) ]
#                 images['basename'] = basenames

#             startnimages = len(images)
            
#             ############ Emily's code #########################################
#             images['obsmjd'] = images['obsjd'] - 2400000.5 # Convert mjds
#             images['tdiff'] = images.obsmjd - peakt # Calculate time from peak
            
#             filtercodes = images['filtercode'].unique()
#             filtersubset = {fc: None for fc in filtercodes}
            
#             for fc in filtercodes:
#                 # Start at low seeing/maglimit/airmass and gradually allow higher ones
#                 best = None
#                 for b in numpy.arange(10, 60, 10): # bandwidth loop
#                     for a in numpy.arange(1.05, maxairmass+.05, .05): # airmass loop
#                         for m in numpy.arange(maglimit+2, maglimit-.1, -.1): # maglimit loop (counts down)
#                             for s in numpy.arange(1.6, maxseeing+.05, .05): # seeing loop
#                                 # Get filtered and clustered data
#                                 good_data = self._filter_refs(images, fcode=fc, days_before=30, 
#                                                   days_after=365, seeing_below=s, maglimit_above=m,
#                                                   airmass_below=a, bw=b) # TODO: less hardcoded stuff
#                                 if good_data.empty: continue

#                                 # Get the best cluster
#                                 best = self._bestcut(good_data)
#                                 if best is not None:
#                                     logger.info(f"Found good cluster: {best}")
#                                     break # Found good cluster
#                             else: continue
#                             break # end maglimit loop
#                         else: continue
#                         break # end airmass loop
#                     else: continue
#                     break # end bandwidth loop
                
#                 if best is None: # Nothing found
#                     logger.warning(f"No images found for filter {fc}")
#                     continue
                
#                 # Get image locations
#                 ref_idxs = good_data[good_data.cluster==best].index

#                 # Cut if too many images
#                 if len(ref_idxs) > 20:
#                     cluster = good_data.loc[ref_idxs].sort_values('seeing')
#                     ref_idxs = cluster.index[:20]
                
#                 # Get full filtered dataset
#                 filtered_data = good_data.loc[ref_idxs]
#                 filtersubset[fc] = filtered_data # Put in Rob's structure
                
#                 # Log reference images
#                 info = io.StringIO()
#                 info.write( f"Images for {fc} : {len(filtered_data)} out of {startnimages}...\n" )
#                 info.write( f'{"Obs. Date":26s}  t_exp airmass moon  m_lim  seeing\n' )
#                 for i, row in filtered_data.iterrows():
#                     info.write( f'{row["obsdate"]:26s}  {row["exptime"]:5.1f}  {row["airmass"]:4.1f} '
#                                 f'{row["moonillf"]:5.2f}   {row["maglimit"]:3.1f} {row["seeing"]:5.2f}\n' )
#                 logger.info( info.getvalue() )
#                 info.close()
            
#             #####################################################################
                
# #             # Throw out anything that's between 30 days before peak and
# #             # 365 days after peak
# #             # (ZTF database has JD, not MJD)
# #             images = images[ ( images['obsjd'] > 2400000.5+peakt-30 ) & 
# #                             ( images['obsjd'] < 2400000.5+peakt+365 ) ]
# #             images.sort_values( 'obsjd', inplace=True )
    
# #             # Subset based on filtercode
# #             # NOTE.  If band was passed, there should only be a single filter code in the images list
            
# #             filtercodes = images['filtercode'].unique()
# #             filtersubset = {}
# #             for fc in filtercodes:
# #                 filtersubset[fc] = images[ images['filtercode'] == fc ]

# #             for fc in filtercodes:
# #                 # Get the median of the 9 best seeings; keep everything that's
# #                 #  better than 0.2 bigger than that
# #                 nseeings = min( 9, len(filtersubset[fc]) )
# #                 see = numpy.median( filtersubset[fc].sort_values(["seeing"])["seeing"].values[0:nseeings] )
# #                 sub = filtersubset[fc][ filtersubset[fc]["seeing"] < see+0.2 ]

# #                 # Keep things whose magnitude limit is better than the best-0.5
# #                 maglim = sub["maglimit"].values.max()
# #                 sub = sub[ sub["maglimit"] > maglim-0.5 ]

# #                 info = io.StringIO()
# #                 info.write( f"Images for {fc} : {len(sub)} out of {startnimages}...\n" )
# #                 info.write( f'{"Obs. Date":26s}  t_exp airmass moon  m_lim  seeing\n' )
# #                 for i, row in sub.iterrows():
# #                     info.write( f'{row["obsdate"]:26s}  {row["exptime"]:5.1f}  {row["airmass"]:4.1f} '
# #                                 f'{row["moonillf"]:5.2f}   {row["maglimit"]:3.1f} {row["seeing"]:5.2f}\n' )
# #                 logger.info( info.getvalue() )
# #                 info.close()

# #                 filtersubset[fc] = sub

#             # Download images

#             refs = {}
#             for fc in filtercodes:
#                 logger.info( f'Securing references for {fc}' )
#                 refs[fc] = []
#                 if filtersubset[fc] is None:
#                     logger.warning(f"No references found for filter {fc}")
#                     continue
#                 for i in range(len(filtersubset[fc])):
#                     basename = filtersubset[fc].iloc[i]['basename']
#                     logger.debug( f'Searching database for image basename {basename}' )
#                     imgobj = db.Image.get_by_basename( basename, curdb=curdb )
#                     if imgobj is not None:
#                         logger.info( f'Image {basename} is already in the database' )
#                         self.ensure_sourcefiles_local( basename, curdb=curdb, logger=logger )
#                     imagename = f'{basename}.fits'
#                     maskname = f'{basename}.mask.fits'
#                     weightname = f'{basename}.weight.fits'
#                     if overwrite:
#                         imagefile = None
#                         maskfile = None
#                         weightfile = None
#                         # ROB!!!!!!!  Broken if there are multiple dataidrs
#                         # You need to search and destroy.
#                     else:
#                         imagefile = self.local_path( imagename, logger=logger )
#                         maskfile = self.local_path( maskname, logger=logger )
#                         weightfile = self.local_path( weightname, logger=logger )
#                     needed = []
#                     if ( imagefile is None ): needed.append( 'image' )
#                     if ( maskfile is None ): needed.append( 'mask' )
#                     if len(needed) > 0:
#                         countdown = 5
#                         success = False
#                         while countdown > 0:
#                             try:
#                                 downloaded = self.download_blob_image( filtersubset[fc], i, files=needed,
#                                                                        rootdir=outdir, overwrite=overwrite,
#                                                                        logger=logger )
#                                 countdown = 0
#                                 success = True
#                             except Exception as e:
#                                 countdown -= 1
#                                 if countdown > 0:
#                                     logger.warning( f'Exception trying to download image {basename}, '
#                                                     f'retrying: {str(e)}' )
#                                     time.sleep(1)
#                                 else:
#                                     logger.error( f'Failed to download image {basename}, skipping it' )
#                         if not success:
#                             continue

#                         for dlimage in downloaded:
#                             ftype = self.filetype( dlimage )
#                             if ftype == "image":
#                                 imagefile = outdir / dlimage
#                                 logger.info( f'Downloaded image str{imagefile}' )
#                             elif ftype == "mask":
#                                 maskfile = outdir / dlimage
#                                 logger.info( f'Downloaded mask str{maskfile}' )
#                             else:
#                                 logger.warning( f'Unknown file type {ftype} for {dlimage}' )

#                     if weightfile is None:
#                         logger.info( f'Building weight for {imagefile.name}' )
#                         processing.make_weight( self, imagefile, mask=maskfile, logger=logger )

#                     if not imgobj:
#                         self.create_or_load_image( imagefile, curdb=curdb, logger=logger )

#                     refs[fc].append( self.image_basename( imagefile.name ) )

#         return( refs )        
        

