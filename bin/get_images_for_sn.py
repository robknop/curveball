#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import io
import logging
import time
import argparse

from exposuresource import ExposureSource
import config
import db
import processing

def get_images_for_sn( expsource, obj=None, ra=None, dec=None, peakt=None,
                       startoff=-30, endoff=90, mjd0=None, mjd1=None,
                       datadir=None, curdb=None, just_list=False,
                       clobber=True, num_images=None, logger=logging.getLogger("main") ):
    """Get images for a supernova from the exposure source, and load them into the database

    exposource - an object returned by exposuresource.ExposureSource.get()

    obj - Either a db.Object or the name of the object, or None

    ra, dec - ignored if obj is not None; the position images must include

    peakt - ignored if obj is not None; the peak time of the supernova

    startoff, endoff - time relative to peakt to search; igored if mjd0 and mjd1 are not none

    just_list - If true, just lists what's found, doesn't actually download or load the database

    clobber -

    num_images - For debugging, only download this many images (all if this is None)
    
    datadir

    curdb

    logger
    
    """
    if ( mjd0 is None ) != ( mjd1 is None ):
        raise ValueError( "Either specify both of or neither of mjd0 and mjd1" )
    
    if datadir is None:
        datadir = config.Config.get().value("datadirs")[0]

    if obj is None:
        if ( ra is None ) or ( dec is None ) or ( peakt is None ):
            raise ValueError( "Must specify either obj or all of (ra, dec, peakt)" )
    else:
        if not isinstance(obj, db.Object):
            objobj = db.Object.get_by_name( obj )
            if objobj is None:
                raise ValueError( f'Unknown object {obj}' )
            obj = objobj
        ra = obj.ra
        dec = obj.dec
        peakt = obj.t0

    logger.debug( f"original peakt={peakt}" )
        
    # Make sure peakt is JD
    if peakt < 2400000:
        peakt += 2400000.5

    if mjd0 is not None:
        jd0 = mjd0 + 2400000.5
        jd1 = mjd1 + 2400000.5
    else:
        jd0 = peakt + startoff
        jd1 = peakt + endoff
        
    logger.debug( f"corrected peakt={peakt}" )

    images = expsource.find_images( ra, dec, starttime=jd0, endtime=jd1, logger=logger )
    if ( num_images is not None ) and ( len(images) > num_images ):
        logger.debug( f'Reducing from {len(images)} to {num_images} images for testing purposes' )
        images = images.iloc[0:num_images]

    strio = io.StringIO()
    strio.write( "Images found:\n" )
    strio.write( f"{'MJD':11s} {'Filter':6s} {'Exptime':7s} {'Maglimit':8s} {'Curveball Name'}\n" )
    for i in range( len(images) ):
        maglim = -99 if expsource.blob_image_maglim(images,i) is None else expsource.blob_image_maglim(images,i)
        strio.write( f'{expsource.blob_image_mjd(images, i):11.5f} '
                     f'{expsource.blob_image_filter(images, i):6s} '
                     f'{expsource.blob_image_exptime(images, i):7.1f} '
                     f'{maglim:8.2f} '
                     f'{expsource.blob_image_name(images, i)}\n' )
    logger.info( strio.getvalue() )

    # import pdb; pdb.set_trace()
    
    if just_list:
        logger.info( "Not downloading anything, just_list was True." )
        return images
    else:
        logger.info( "Starting to look at and download these...." )
    
    for i in range( len(images) ):
        logger.info( f'Considering {i+1} of {len(images)} images' )
        imagefile = expsource.blob_image_path( images, i, datadir )
        maskfile = expsource.blob_image_path( images, i, datadir, "mask" )
        weightfile = expsource.blob_image_path( images, i, datadir, "weight" )
        basename = expsource.image_basename( imagefile.name )

        imgobj = db.Image.get_by_basename( basename )
        if imgobj is not None:
            logger.info( f'{basename} is already in the database' )
            # buildltcv will do an ensure_sourcefiles_local, so don't do that there
        else:
            filesneeded = []
            if imagefile.exists():
                if not imagefile.is_file():
                    raise RuntimeError( f'File {imagefile} exists but is not a normal file!' )
                elif clobber:
                    imagefile.unlink()
                    imagefile = None
                    filesneeded.append( 'image' )
                else:
                    logger.info( f'File {imagefile} already present' )
            else:
                imagefile = None
                filesneeded.append( 'image' )

            if maskfile.exists():
                if not maskfile.is_file():
                    raise RuntimeError( f'File {maskfile} exists but is not a normal file!' )
                elif clobber:
                    maskfile.unlink()
                    maskfile = None
                    filesneeded.append( 'mask' )
                else:
                    logger.info( f'File {maskfile} already present' )
            else:
                maskfile = None
                filesneeded.append( 'mask' )

            if weightfile.exists():
                if not weightfile.is_file():
                    raise RuntimeError( f'File {weightfile} exists but is not a normal file!' )
                elif clobber:
                    weightfile.unlink()
                    weightfile = None
                    filesneeded.append( 'weight' )
                else:
                    logger.info( f'File {weightfile} already present' )
            else:
                weightfile = None
                filesneeded.append( 'weight' )

            countdown = 5
            success = False
            while countdown > 0:
                try:
                    downloaded = expsource.download_blob_image( images, i, files=filesneeded,
                                                                rootdir=datadir, logger=logger )
                    success = True
                    countdown = 0
                except Exception as e:
                    countdown -= 1
                    if countdown > 0:
                        logger.warning( f'Exception trying to download image {i}, retrying: {str(e)}' )
                        time.sleep(1)
                    else:
                        logger.error( f'Failed to download image{i}, skipping it' )
            if not success:
                continue

            for dlimage in downloaded:
                ftype = expsource.filetype( dlimage )
                if ftype == "image":
                    imagefile = dlimage
                elif ftype == "mask":
                    maskfile = dlimage
                elif ftype == "weight":
                    weightfile = dlimage
                else:
                    logger.error( f'Unknown file type {ftype} for downloaded image {dlimage}, ignoring it!' )
            logger.debug( f'Have image file {imagefile}' )
            logger.debug( f'Have mask file {maskfile}' )
            logger.debug( f'Have weight file {weightfile}' )

            if ( imagefile is None ) or ( maskfile is None ):
                logger.error( f'Image or mask missing for image {i}!  Not continuing (some stuff may be downloaded).' )

            if weightfile is None:
                logger.info( f'Building weight for {imagefile}...' )
                weightfile = processing.make_weight( expsource, imagefile, mask=maskfile, logger=logger )

            logger.info( f'Loading {imagefile.name} into database' )
            expsource.create_or_load_image( imagefile, logger=logger )

    return images

# ======================================================================

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s',
                                            datefmt='%Y-%m-%d %H:%M:%S' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( None, logger=logger )
    
    argparser = argparse.ArgumentParser( description="Load into DB images for SN",
                                         epilog="""Go to the exposure source and find images that include the RA/Dec of
this supernova.  If the image is already in the database, do nothing.
(Doesn't insure that the image is local!  That happens inside
buildltcv.)  Otherwise, download the image, make sure .fits, .mask.fits,
and .weight.fits exist, and load the image into the database.
                                      
"""
                                        )
    argparser.add_argument( "-e", "--expsource", required=True,
                            help="Name of exposure source for images" )
    argparser.add_argument( "-o", "--object", default=None,
                            help="Name of object; specify either this or ra/dec/peakt" )
    argparser.add_argument( "-r", "--ra", default=None, type=float,
                            help="RA of object (decimal degrees)" )
    argparser.add_argument( "-d", "--dec", default=None, type=float,
                            help="Dec of object (decimal degrees)" )
    argparser.add_argument( "-t", "--peakt", default=None, type=float,
                            help="Peak time of SN (JD or MJD)" )
    argparser.add_argument( "-s", "--startoff", default=-30, type=float,
                            help=( "Start with images this many days after peakt (default: -30); "
                                   "ignored if mjd[01] is not None" ) )
    argparser.add_argument( "-f", "--endoff", default=90, type=float,
                            help=( "Finish with images this many days after peakt (default: 90); "
                                   "ignored if mjd[01] is not None" ) )
    argparser.add_argument( "--mjd0", default=None, type=float, help="First mjd to search" )
    argparser.add_argument( "--mjd1", default=None, type=float, help="Last mjd to search" )
    argparser.add_argument( "-c", "--clobber", default=False, action='store_true',
                            help="Blow away exiting files?" )
    argparser.add_argument( "-n", "--num-images", default=None, type=int,
                            help="Only grab this many images (for testing purposes)" )
    argparser.add_argument( "-j", "--just-list", action='store_true', default=False,
                            help="Don't actually download anything, just list what was found" )
    argparser.add_argument( "-v", "--verbose", action="store_true", default=False,
                            help="Show debugging information" )
    args = argparser.parse_args()

    if args.verbose:
        logger.setLevel( logging.DEBUG )

    expsource = ExposureSource.get( args.expsource )

    get_images_for_sn( expsource, obj=args.object, ra=args.ra, dec=args.dec, peakt=args.peakt,
                       startoff=args.startoff, endoff=args.endoff, mjd0=args.mjd0, mjd1=args.mjd1,
                       just_list=args.just_list, clobber=args.clobber, num_images=args.num_images, logger=logger )
    
# ======================================================================

if __name__ == "__main__":
    main()
