#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
import random
from mpi4py import MPI

from util import Runner

def worker( data, logger=logging.getLogger("main") ):
    number = random.randint( 0, 100 )
    logger.info( f'Generated number {number} for dex {data["dex"]}' )
    if data["dex"] == 2:
        raise Exception( "I don't like 2" )
    return { "dex": data["dex"],
             "number": number }

def controller( data, logger=logging.getLogger("main") ):
    logger.info( f'Dex {data["dex"]} gave number {data["number"]}' )

def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - {rank:02d} - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    njobs = 5 
    
    datalist = []
    if rank == 0:
        for i in range( 1, njobs+1 ):
            datalist.append( { "dex": i } )
    runner = Runner( comm, controller, worker, logger=logger )
    success = runner.go( datalist )

    if rank == 0:
        print( f'{sum(success)} succesful, {njobs-sum(success)} failed' )
            

# ======================================================================

if __name__ == "__main__":
    main()
