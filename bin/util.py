#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import re
import math
import datetime
import dateutil.parser
import pytz
import logging
import traceback
from mpi4py import MPI

# ======================================================================
# A decorator that allows me to run a class init function of a parent
#   class when subclasses are created

def cls_init( cls ):
    if getattr( cls, "_cls_init", None):
        cls._cls_init()
    return cls

# ======================================================================
# Full julian day from python datetime

def julday( val, mjd=False ):
    if not isinstance( val, datetime.datetime ):
        if isinstance( val, str ):
            try:
                val = dateutil.parser.parse( val )
            except Exception as e:
                raise ValueError( f'Error parsing string {val} for date/time' )
        else:
            raise ValueError( f'Don\'t know how to make {val} (type({val})) into a datetime object' )

    if val.tzinfo is not None:
        val = val.astimezone( pytz.utc )

    Y = val.year
    M = val.month
    D = val.day
    H = val.hour
    MIN = val.minute
    S = val.second + val.microsecond / 1e6

    # https://quasar.as.utexas.edu/BillInfo/JulianDatesG.html
    if ( M == 1 ) or ( M == 2 ):
        M += 12
        Y -= 1
    
    A = Y // 100
    B = A // 4
    C = 2 - A + B
    E = int( 365.25 * ( Y + 4716 ) )
    F = int( 30.6001 * ( M + 1 ) )
    JDN = C + D + E + F - 1524.5

    JD = JDN + H/24 + MIN/1440 + S/86400
    
    if mjd:
        JD -= 2400000.5 
    
    return JD

# ======================================================================
# Convert full julian day to python datetime

def julday_to_datetime( jd, mjd=False ):
    if mjd:
        jd += 2400000.5

    # https://quasar.as.utexas.edu/BillInfo/JulianDatesG.html

    dayfrac = jd - ( math.floor(jd) + 0.5 )
    if dayfrac < 0:
        dayfrac += 1

    Q = jd + 0.5
    Z = int(Q)
    W = int( (Z - 1867216.25) / 36524.25 )
    X = W // 4
    A = Z + 1 + W - X
    B = A + 1524
    C = int( (B - 122.1) / 365.25 )
    D = int( 365.25 * C )
    E = int( (B-D) / 30.6001 )
    F = int( 30.6001 * E )

    day = int( B - D - F + (Q-Z) )
    month = int( E - 1 if E < 13 else E - 13 )
    year = int( C - ( 4715 if month<=2 else 4716 ) )

    hour = int( 24 * dayfrac )
    minute = int( 1440 * ( dayfrac - hour/24 ) )
    second = int( 60*1440 * ( dayfrac - hour/24 - minute/1440 ) )
    μs = int( 60*1440*1000000 * ( dayfrac - hour/24 - (minute + second/60)/1440 ) + 0.5 )

    return datetime.datetime( year, month, day, hour, minute, second, μs, tzinfo=pytz.utc )

# ======================================================================
# Parse RA h:m:s to digital degrees

_hmsre = re.compile( '^ *([0-9]+) *: *([0-9]+) *: *([0-9]*\.?[0-9]*) *$' )

def hmstodeg( hms ):
    global _hmsre
    match = _hmsre.search( hms )
    if match is None:
        raise ValueError( f'Error parsing {hms} for h:m:s' )
    return 15. * ( float(match.group(1)) + float(match.group(2))/60. + float(match.group(3))/3600. )

# ======================================================================
# Parse Dec ±d:m:s to digital degrees

_dmsre = re.compile( '^ *([+-−])? *([0-9]+) *: *([0-9]+) *: *([0-9]*\.?[0-9]*) *$' )

def dmstodeg( dms ):
    global _dmsre
    match = _dmsre.search( dms )
    if match is None:
        raise ValueError( f'Error parsing {dms} for ±d:m:s' )
    if ( match.group(1) == "-" ) or ( match.group(1) == "−" ):
        decsgn = -1
    else:
        decsgn = 1
    return decsgn * ( float(match.group(2)) + float(match.group(3))/60. + float(match.group(4))/3600. )
    
# =====================================================================

def write_regions( catalog, filename="regions.reg", world=False, color="blue", width=2, radius=None ):
    if world:
        xvals = catalog['X_WORLD']
        yvals = catalog['Y_WORLD']
        radius = 2 if radius is None else radius
        unit1 = 'd'
        unit2 = '"'
        frame = "icrs"
        prec = 6
    else:
        xvals = catalog['X_IMAGE']
        yvals = catalog['Y_IMAGE']
        radius = 10 if radius is None else radius
        unit1 = ""
        unit2 = ""
        frame = "image"
        prec = 2
    with open( filename, "w") as ofp:
        for x, y in zip( xvals, yvals ):
            ofp.write( f'{frame};circle({x:.{prec}f}{unit1},{y:.{prec}f}{unit1},{radius}{unit2}) '
                       f'# color={color} width={width}\n' )
            

# =====================================================================

class Runner:
    def __init__( self, comm, controller_callback, worker_callback,
                  dieonerror=False,  logger=logging.getLogger("main") ):
        """A framework to run a series of tasks in parallel.

        Of the MPI ranks, the first one is a controller rank, and just
        sends out jobs to the other ranks.  The other ranks actually do
        the work.

        comm : MPI communicator, or None of if you're using this entirely gratuitously
        worker_callback : Callback called once for each element of datalist passed to go.  This one is
                          run in parallel, and should have the majority of the meat of the work.
        controller_callback : Callback called for each return value of worker_callback.  This one is run
                              in the controller thread every time, so it should be lightweight.
        dieonerror : If True, and any process throws an exception or otherwise has an error, throw a RuntimeError
                     otherwise, just move on.  If True, you probably want to run with python -m mpi4py
        logger : A logging.Logger object

        """
        self.comm = comm
        if self.comm is None:
            self.size = 1
            self.rank = 0
        else:
            self.size = comm.Get_size()
            self.rank = comm.Get_rank()
        self.logger = logger
        self._controller_callback = controller_callback
        self._worker_callback = worker_callback
        self.dieonerror = dieonerror
        if self.size == 2 and self.rank == 0:
            logger.warning( 'Runner won\'t give any improvement over single-threaded for 2 processes.' )

    def go( self, datalist ):
        """Run the processes for a list of data

        datalist : a list of objects that will be passed to the worker callback

        Returns an array of booleans corresponding to datalist.  Where
        True, Runner thinks the process worked.

        """
        if self.rank == 0:
            if self.size == 1:
                success = self._justdoit( datalist )
            else:
                success = self._controller( datalist )
        else:
            self._worker()
            success = []
        if self.size > 1:
            success = self.comm.bcast( success, root=0 )
        return success
        
    def _justdoit( self, datalist ):
        success = [ False ] * len(datalist)
        for i, data in enumerate(datalist):
            try:
                self.logger.info( f'Runner running task {i}' )
                result = self._worker_callback( data, logger=self.logger )
                self._controller_callback( result, logger=self.logger )
                success[i] = True
            except Exception as e:
                if self.dieonerror:
                    raise(e)
                else:
                    self.logger.exception( f'Error running task {i}; results are missing or incomplete!' )
        return success

    def _controller( self, datalist ):
        workers = [ False ] * self.size
        success = [ False ] * len(datalist)
        avail = []
        ncheckedin = 0
        sentdex = 0
        ndone = 0

        # Wait for all workers to check in
        status = MPI.Status()
        while ncheckedin < self.size-1:
            self.logger.debug( f'Waiting for a worker to check in' )
            msg = self.comm.recv( source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status )
            rank = status.Get_source()
            self.logger.debug( f'Rank {rank} just checked in' )

            if msg["message"] == "Hello":
                if workers[rank]:
                    raise RuntimeError( f'Rank {rank} checked in more than once!' )
                workers[rank] = True
                avail.append( rank )
                ncheckedin += 1
            else:
                raise RuntimeError( f'Got message {msg["message"]} while trying to initialize' )

        # Run the things
        while ndone < len(datalist):
            # Send out jobs to all available workers
            while ( sentdex < len(datalist) ) and ( len( avail ) > 0 ):
                rank = avail.pop()
                msg = { "message": "do", "dex": sentdex, "data": datalist[sentdex] }
                self.logger.info( f'Sending job {sentdex} of {len(datalist)} to rank {rank}' )
                self.comm.send( msg, dest=rank )
                sentdex += 1

            # Wait for responses
            self.logger.debug( f'Waiting for responses from workers' )
            msg = self.comm.recv( source=MPI.ANY_SOURCE, tag=MPI.ANY_TAG, status=status )
            rank = status.Get_source()

            if msg["message"] == "Finished":
                self.logger.debug( f'Got complete from rank {rank}' )
                self._controller_callback( msg["data"], logger=self.logger )
                success[ msg["dex"] ] = True
                ndone += 1
                avail.append( rank )
            elif msg["message"] == "Error":
                if self.dieonerror:
                    raise RuntimeError( f'Error from rank {rank}: {msg["error"]}' )
                else:
                    self.logger.error( f'ERROR: rank {rank} returned error {msg["error"]}. '
                                       'Task is incomplete or only partially complete!' )
                    ndone += 1
                    avail.append( rank )
            else:
                raise RuntimeError( f'Unexpected message {msg["message"]}' )
            self.logger.info( f'{ndone} of {len(datalist)} jobs finished' )

        # Tell everybody to die
        self.logger.info( f'All jobs complete, closing down' )
        for rank in range(1, self.size):
            self.logger.debug( f'Sending "die" to rank {rank}' )
            self.comm.send( { "message": "die" }, dest=rank )
        self.logger.debug( f'Done telling workers to die.' )
            
        return success
            
    def _worker( self ):
        # Check in with home base
        self.logger.debug( f'Checking in with controller' )
        self.comm.send( { "message": "Hello" }, dest=0 )

        # Wait to be commanded
        while True:
            msg = self.comm.recv( source=0, tag=MPI.ANY_TAG )
            if msg["message"] == "die":
                break
            elif msg["message"] == "do":
                result = None
                try:
                    self.logger.debug( f'Got data: {msg["data"]}' )
                    result = self._worker_callback( msg["data"], logger=self.logger )
                    self.logger.debug( f'Worker callback got result: {result}' )
                except Exception as e:
                    self.logger.exception( f'Rank {self.rank} got an exception' )
                    self.comm.send( { "message": "Error",
                                      "dex": msg["dex"],
                                      "error": str(e),
                                      "exception": e }, dest=0 )
                else:
                    self.comm.send( { "message": "Finished", "dex": msg["dex"], "data": result }, dest=0 )
            else:
                self.comm.send( { "message": "Error",
                                  "dex": msg["dex"],
                                  "error": f'Rank {self.rank} got unexpected command {msg["message"]}' } )
        self.logger.debug( "Runner worker closing down." )
