import sys
import time
import math
import argparse
import logging
import numpy
import types

import sqlalchemy as sa
from astropy.table import Table
import sncosmo
import sfdmap

import db
import util
import config

# This is the mapping of database band names to sncosmo SALT2 band names
bandmapping = {
    'ZTF_g' : 'ztfg',
    'ZTF_r' : 'ztfr',
    'ZTF_i' : 'ztfi',
    'DECam_g': 'desg',
    'DECam_r': 'desr',
    'DECam_i': 'desi',
    }


def _update_s2fit_version_tags( s2fitobj, always_tag_default=False, tag_default=True,
                                add_new_tags=True, versiontags=[], curdb=None, logger=logging.getLogger("main") ):
    obj = db.Object.get( s2fitobj.object_id, curdb=curdb )

    # Figure out the tags we're going to tag this with;
    #  this requires special handling for default
    if isinstance( versiontags, str ):
        tags = set( [ versiontags ] )
    else:
        tags = set( versiontags )

    if tag_default or always_tag_default:
        curdefault = db.Salt2Fit.get_for_obj( obj, version='default', curdb=curdb )
        if curdefault is not None:
            if always_tag_default:
                tags.add( 'default' )
            else:
                logger.warning( f'{obj.name} already has a default SALT2 fit, not tagging this fit as default.' )
                try:
                    tags.remove( 'default' )
                except KeyError as ex:
                    pass
        else:
            tags.add( 'default' )
    else:
        try:
            tags.remove( 'default' )
        except KeyError as ex:
            pass

    # Remove version tags on existing fits for this object and these versiont ags
    q = ( curdb.db.query( db.Salt2FitVersiontag )
          .join( db.VersionTag, sa.and_( db.Salt2FitVersiontag.versiontag_id==db.VersionTag.id,
                                         db.VersionTag.name.in_( tags ) ) )
          .join( db.Salt2Fit, db.Salt2Fit.id==db.Salt2FitVersiontag.salt2fit_id )
          .filter( db.Salt2Fit.object_id==obj.id ) )
    for s2fittag in q.all():
        curdb.db.delete( s2fittag )
        # I have this vague memory that a bunch of deletes
        #  followed by a single commit didn't work.  It ought to,
        #  but I'm being paranoid and putting this inside the loop.
        curdb.db.commit()

    # Add version tags for this fit
    for tag in tags:
        vt = db.VersionTag.get_by_name( tag, curdb=curdb )
        if vt is None:
            vt = db.VersionTag( name=tag )
            curdb.db.add( vt )
            curdb.db.commit()
        sf2vt = db.Salt2FitVersiontag( salt2fit_id=s2fitobj.id, versiontag_id=vt.id )
        curdb.db.add( sf2vt )
        curdb.db.commit()


def get_or_run_salt2fit( obj, version="default", versiontags=[], force_rerun=False, save=False,
                         curdb=None, logger=logging.getLogger("main"), **kwargs ):
    """Load the SALT2 fit for the object with the indicated version tag, or run and save it if it doesn't exist.

    obj: Either a db.Object object, or the name of the object
    version: Try to load this verson from the database
    fitbands: Bands to fit to.  Ignored if it loads an existing fit (which is suboptimal, but, oh well)
    save: Save this fit to the database?  Will also update tags even if the fit was already found.
    boundt0: If True, will tell SALT2 to bound the peak of the lightcurve to the object's database t0±10 days
    photversion: The version tag of the photometry to use to get the lightcurve
       (will not get photometry that has is_bad=True)
    always_tag_default: Tag this fit as the default fit, untagging as default any existing fit for this object
    tag_default: Tag this fit as the default fit for this object ONLY IF there already isn't a default
    versiontags: Additional version tags to add to this object in addition to version and (maybe) default
    add_new_tags: If version tags aren't already defined, define them.
    curdb (optional): a db.DB object
    logger (optional): a logging.logger objectn


    Returns a db.Salt2Fit object, or maybe a namespace with some of the same fields if save was False,
       or maybe None if nothing worked, or, hell, I don't know.

    Tries to load the fit from the databse.  If it exists, update its tags as necessary in the 
    database.  If it doesn't exist, run the fit, save it, tag it.

    If you JUST want to load the fit, and not run it if it doesn't exist, use db.Salt2Fit.get_for_obj()

    """

    versiontags = set( versiontags )
    versiontags.add( version )

    with db.DB.get(curdb) as curdb:
        if not isinstance( obj, db.Object ):
            obj = db.Object.get_by_name( obj, curdb=curdb )

        s2fit = None

        if not force_rerun:
            s2fit = db.Salt2Fit.get_for_obj( obj, version=version, curdb=curdb )
            if s2fit is not None:
                if len(s2fit) > 1:
                    logger.warning( f"Multiple fits found for object {obj.name}, fit version {version}. "
                                    f"This is surprising." )
                s2fit = s2fit[0]

        didrun = False

        if s2fit is not None:
            logger.info( "Fit loaded from database." )
        else:
            logger.info( "Running the fit..." )
            s2fit = run_salt2fit( obj, curdb=curdb, logger=logger, save=save, versiontags=versiontags, **kwargs )
            didrun = True

        if s2fit is None:
            raise RuntimeError( "Can't find or failed to run fit." )

        if save and ( not didrun ):
            logger.info( "Ensuring tags are up to date." )
            _update_s2fit_version_tags( s2fit, always_tag_default=kwargs['always_tag_default'],
                                        tag_default=kwargs['tag_default'], add_new_tags=kwargs['add_new_tags'],
                                        versiontags=versiontags, curdb=curdb, logger=logger )
        return s2fit


def run_salt2fit( obj, fitbands=[],
                  iterate_sigreject=False, relsigcut=3., abssigcut=5., markrejbad=False, markofffitrejbad=False,
                  save=False, boundt0=False, z=None, fit_z=False, z_bounds=None, update_z=False,
                  photversion='default', always_tag_default=False, tag_default=True, versiontags=[],
                  add_new_tags=True, errorpuff=False, curdb=None, logger=logging.getLogger("main") ):
    """Run a SALT2 fit to the non-bad photometry for an object.


    Parameters
    ----------
    obj: Either a db.Object, or the name of an object

    fitbands: A list of the band names to fit to. By default, uses photometry of all bands.

    iterate_sigreject: Iterative sigma rejection of points.  For a point
      to be rejected, it must be *both* more than relsigcut times its
      error bar away from the fit, and abssigcut times mr away from
      the fit, where mr is the median |residual| of all points from the
      fit.

    relsigcut: individual point relative cut for iterative sigma rejection

    abssigcut: individual point absolute cut for iterative sigma rejection

    markrejbad: if True, mark all points that get rejected from the fit as "bad" in the photometry database.

    markofffitrejbad: Normally, only those points that are within the
      time range where SALT2 actually fits are run through the outlier
      cut.  If this is True, then run all points through the outlier
      cut, coutning the ones outside the time range fit by SALT2 as
      having a model value of 0.  You usually don't want to use this!
      The one use case is for automatically rejecting outliers for
      purposes of coming up with a set of photometry points to use to
      measure recentering.  There, you care about more than just the
      fit, and we need a way of throwing out points that (say) have a CR
      but that are outside the range of times fit by SALT2.  But, there
      will likely be collatoral damage!  But, specifically for centering,
      the collatoral damage will be points that would have had low
      weight on the recentering anyway.

    save: Save this fit to the database?  (Default: don't.)_

    boundt0: If True, will tell SALT2 to bound the peak of the lightcurve to the
      t0 in the database ± 10 days

    photversion: The version tag of the photometry to pull

    always_tag_default: If saving, tag this fit as the default fit, overriding any
      previous fit tagged as default

    tag_default: If saving, tag this fit as the default fit if there isn't 
      already one that is tagged as default

    versiontags: List, additional version tags to apply to this fit.

    add_new_tags: If one of the version tags on the list isn't already defined, define it

    logger (optional): a logging.logger object

    Returns a namespace with the either a db.Salt2Fit object, or a namespace with some of the same fields

    """
    if z is not None and fit_z:
        raise ValueError("Z value should not be set when fit_z is set.")
    if fit_z and (z_bounds is None):
        raise ValueError("You must pass redshift bounds when fit_z is set. Try [0.01,1].")

    with db.DB.get(curdb) as curdb:
        if not isinstance( obj, db.Object ):
            obj = db.Object.get_by_name( obj, curdb=curdb )
        photometry = db.Photometry.get_for_obj( obj, version=photversion, curdb=curdb )

        t = []
        bands = []
        flux = []
        fluxerr = []
        zp = []
        zpsys = []
        bandnamelist = []
        bandidlist = []
        phoobjs = []
        bandwarned = set()

        for pho in photometry:
            band = pho[0]
            ltcv = pho[1]
            if ( len(fitbands) > 0 ) and ( band.name not in fitbands ):
                if band.name not in bandwarned:
                    logger.warning( f'Found photometry for {band.name}, but not using it as requested' )
                    bandwarned.add( band.name )
                continue
            if band.name not in bandmapping.keys():
                raise ValueError( f'Don\'t know SALT2 equivalent of {band.name}' )
            bandidlist.append( band.id )
            bandnamelist.append( band.name )

            bands += [ bandmapping[band.name] ] * len(ltcv)
            zp += [ 8.9 ] * len(ltcv)
            zpsys += [ 'ab' ] * len(ltcv)
            t += [ p.mjd for p in ltcv ]
            flux += [ p.flux * 10**( ( 8.9 - p.magzp ) / 2.5 ) for p in ltcv ]
            fluxerr += [ p.dflux * 10**( ( 8.9 - p.magzp ) / 2.5 ) for p in ltcv ]
            phoobjs.extend( ltcv )
        bands = numpy.array( bands )
        t = numpy.array( t )
        flux = numpy.array( flux )
        fluxerr = numpy.array( fluxerr )
        zp = numpy.array( zp )
        zpsys = numpy.array( zpsys )

        logger.info( f'Found photometry for {bandnamelist}' )

        done = False
        while not done:
            result = actually_run_salt2fit( obj, bands, t, flux, fluxerr, zp, zpsys,
                                            boundt0=boundt0, z=z, fit_z=fit_z, z_bounds=z_bounds,
                                            logger=logger )

            if not iterate_sigreject:
                done = True
                break

            abssig = {}
            fluxen = result.fit_model.bandflux( bands, t, zp, zpsys )
            # Where fluxen are strictly 0, it means that the model didn't
            # actually fit to those points.
            fitpoints = fluxen != 0
            resid = flux - fluxen
            # residrms = resid.std()
            residmed = numpy.median( numpy.abs( resid ) )
            wbad = ( ( numpy.abs( resid ) > abssigcut * residmed )
                     &
                     ( numpy.abs( resid ) > relsigcut * fluxerr ) )
            if not markofffitrejbad:
                wbad = wbad & fitpoints
                fracbad = wbad.sum() / len(flux)
            else:
                fracbad = wbad.sum() / len(fitpoints)

            if markrejbad and ( fracbad > 0.1 ):
                errmsg = f"More than 10% ({fracbad:.2f} of the points would be marked bad!  Aborting."
                logger.error( errmsg )
                raise RuntimeError( errmsg )

            if wbad.sum() == 0:
                logger.info( f"Fit with χ²/ν = {result.chisq:.0f}/{result.dof} = {result.chisq/result.dof:.1f} "
                             f"had no outliers, finishing." )
                done = True
            else:
                logger.info( f"Fit with χ²/ν = {result.chisq:.0f}/{result.dof} = {result.chisq/result.dof:.1f} "
                             f"had {wbad.sum()} outliers, iterating." )
                if markrejbad:
                    logger.info( "...marking outlier points as bad." )
                    for phot, bad in zip( phoobjs, wbad ):
                        if bad:
                            phot.is_bad = True
                    curdb.db.commit()
                bands = bands[ ~wbad ]
                t = t[ ~wbad ]
                flux = flux[ ~wbad ]
                fluxerr = fluxerr[ ~wbad ]
                zp = zp[ ~wbad ]
                zpsys = zpsys[ ~wbad ]
                phoobjs = [ p for p, b in zip( phoobjs, wbad ) if not b ]

        # HACK ALERT : puff error bars
        if errorpuff and ( ( result.chisq / result.dof ) > 1. ):
            chifac = math.sqrt( result.chisq / result.dof )
            result = actually_run_salt2fit( obj, bands, t, flux, fluxerr*chifac, zp, zpsys,
                                            boundt0=boundt0, z=z, fit_z=fit_z, z_bounds=z_bounds,
                                            logger=logger )


        if update_z:
            obj.z = z
            curdb.db.commit()
            print( f"\nObject {obj.name} updated to z={z}" )

        if save:
            sfobj = db.Salt2Fit( object_id=obj.id,
                                 bands=bandidlist,
                                 z=result.z,
                                 dz=result.dz,
                                 zfixed=result.zfixed,
                                 mwebv=result.mwebv,
                                 mbstar=result.mbstar,
                                 dmbstar=result.dmbstar,
                                 t0=result.t0,
                                 dt0=result.dt0,
                                 x0=result.x0,
                                 dx0=result.dx0,
                                 x1=result.x1,
                                 dx1=result.dx1,
                                 c=result.c,
                                 dc=result.dc,
                                 cov=result.cov,
                                 chisq=result.chisq,
                                 dof=result.dof )
            curdb.db.add( sfobj )
            curdb.db.commit()

            _update_s2fit_version_tags( sfobj, always_tag_default=always_tag_default, tag_default=tag_default,
                                        add_new_tags=add_new_tags, versiontags=versiontags,
                                        curdb=curdb, logger=logger )
            logger.info( "Fit added to database" )

            return sfobj
        else:
            result.bandidlist = bandidlist
            return result

def actually_run_salt2fit( obj, bands, t, flux, fluxerr, zp, zpsys,
                           boundt0=False, z=None, fit_z=False, z_bounds=None,
                           logger=logging.getLogger("main") ):
    data = Table( [ t, bands, flux, fluxerr, zp, zpsys ],
                  names=["time", "band", "flux", "fluxerr", "zp", "zpsys"] )
    mwdust = sfdmap.SFDMap( "/sfddata" )
    mwebv = mwdust.ebv( obj.ra, obj.dec )

    dust = sncosmo.CCM89Dust()
    model = sncosmo.Model( "salt2", effects=[dust], effect_names=['mw'], effect_frames=['obs'] )

    if z is None and not fit_z:
        z = obj.z
    if z is not None and z < -0.1:
        raise ValueError(f"The redshift is absurd. Object {obj.name}, z: {z} ."
                         "Run program with --fitz to fit the redshift.")
    if not fit_z:
        model.set( z = z )
    model.set( mwebv = mwebv )

    bounds = {}
    if boundt0:
        bounds['t0'] = ( obj.t0-10, obj.t0+10 )
    if z_bounds is not None:
        bounds['z'] = z_bounds

    free_params = ['t0', 'x0', 'x1', 'c']
    if fit_z:
        free_params = ['z'] + free_params

    logger.info( 'Running fit...' )
    result, fit_model = sncosmo.fit_lc( data, model, free_params, bounds=bounds )

    # What's the right band and magnitude system to use here???
    ix0 = result.param_names.index('x0')
    mbstar = fit_model.source.peakmag( 'bessellb', 'vega' )
    dmbstar = 2.5/math.log(10) * result.errors['x0'] / result.parameters[ix0]

    covparams = [ 'z', 't0', 'x0', 'x1', 'c' ]
    covparamdex = [-1, -1, -1, -1, -1 ]
    paramdex = [-1, -1, -1, -1, -1 ]
    for i in range(len(covparams)):
        try:
            dex = result.vparam_names.index( covparams[i] )
            covparamdex[i] = dex
        except ValueError as ve:
            covparamdex[i] = -1
        try:
            dex = result.param_names.index( covparams[i] )
            paramdex[i] = dex
        except ValueError as ve:
            paramdex[i] = -1

    cov = numpy.zeros( ( len(covparams), len(covparams) ) )
    for i in range(cov.shape[0]):
        for j in range(cov.shape[1]):
            if ( covparamdex[i] >=0 ) and ( covparamdex[j] >= 0 ):
                cov[i, j] = result.covariance[ covparamdex[i], covparamdex[j] ]

    if fit_z:
        z = result.parameters[paramdex[0]]
        dz = result.errors['z']
        zfixed = False
    else:
        zfixed = True
        dz = 0.


    sfresult = types.SimpleNamespace()
    sfresult.result = result
    sfresult.fit_model = fit_model
    sfresult.object_id=obj.id
    sfresult.z=z
    sfresult.dz=dz
    sfresult.zfixed=zfixed
    sfresult.mwebv=mwebv
    sfresult.mbstar=mbstar
    sfresult.dmbstar=dmbstar
    sfresult.t0=result.parameters[paramdex[1]]
    sfresult.dt0=result.errors['t0']
    sfresult.x0=result.parameters[paramdex[2]]
    sfresult.dx0=result.errors['x0']
    sfresult.x1=result.parameters[paramdex[3]]
    sfresult.dx1=result.errors['x1']
    sfresult.c=result.parameters[paramdex[4]]
    sfresult.dc=result.errors['c']
    sfresult.cov=cov.tolist()
    sfresult.chisq=result.chisq
    sfresult.dof=result.ndof

    return sfresult


# ======================================================================

def main():
    global bandmapping

    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( None, logger=logger )

    parser = argparse.ArgumentParser( description="SALT2 fit to lightcurve with SNCOSMO" )
    parser.add_argument( "object", help="Name of object" )
    parser.add_argument( "-b", "--bands", nargs='*', default=[],
                         help="List of bands to include in fit (def: all in database)" )
    parser.add_argument( "-v", "--versiontag", default='default',
                         help=( "Version tag of the fit to search the database for.  If --save is given, "
                                "the fit will be tagged with this version tag." ) )
    parser.add_argument( "--errorpuff", default=False, action='store_true',
                         help=( "Puff out errorbars to bring χ²/ν down to 1 if it starts >1. "
                                "(Done after iterative rejection of outliers.)" ) )
    parser.add_argument( "-i", "--iterate-sigreject", default=False, action='store_true',
                         help="Do iterative outlier rejection of things that pass both relsigcut and abssigcut" )
    parser.add_argument( "-r", "--relsigcut", default=3., type=float,
                         help="Reject points this many sigma away from the current iteration's fit" )
    parser.add_argument( "-a", "--abssigcut", default=5., type=float,
                         help="Reject points with residual this many times the median |residual|" )
    parser.add_argument( "-m", "--mark-rejected-bad", default=False, action='store_true',
                         help="Mark photometry rejected from the fit as bad" )
    parser.add_argument( "-e", "--mark-bad-off-edge-of-fit", default=False, action='store_true',
                         help=( "If mark-rejected-bad, normally only points within where sncomso fits a model "
                                "will be considered for outlier status.  If this is set, then points outside "
                                "the usual SALT2 range will be considered bad if they're outside the sigma "
                                "ranges from 0.  You usually don't want to use this; see source code docstrings." ) )
    parser.add_argument( "-s", "--save", action='store_true', default=False,
                         help="Save fit to database?" )
    parser.add_argument( "-f", "--force", action='store_true', default=False,
                         help="Redo the fit even if one was found.  Ignored if --save is not given." )
    parser.add_argument( "-t", "--t0-bound", action='store_true', default=False,
                         help="Limit t0 of the fit to the object's database t0 ± 10 days" )
    parser.add_argument( "-z", "--redshift", type=float, default=None,
                         help="Redshift of the object (default is database value)" )
    parser.add_argument( "--fitz", action='store_true', default=False,
                         help="Fit the redshift. Must set zbounds." )
    parser.add_argument( "--zbounds", type=float, nargs=2, default=None,
                         help="Bounds to fit redshift. Must be given if fitz is true." )
    parser.add_argument( "-u", "--update", action='store_true', default=False,
                         help="Update object in database with fitted redshift." )
    parser.add_argument( "-p", "--photometry-version", default="default",
                         help="Version tag of photometry to pull (defaut: \"default\")" )
    parser.add_argument( "-d", "--always-tag-default", action='store_true', default=False,
                         help="Tag this fit as the default fit, overriding any previous one." )
    parser.add_argument( "-n", "--no-tag-default", action='store_true', default=False,
                         help=( "Do NOT tag this fit as the default.  By default, will tag the fit as the "
                                "default if if there isn't already another tagged as default for this object." ) )
    parser.add_argument( "--versiontags", nargs='+', default=[],
                         help="Additional version tags to tag this fit as.  Ignored if --save isn't given." )
    args = parser.parse_args()


    s2fit = get_or_run_salt2fit( args.object, args.versiontag, fitbands=args.bands,
                                 save=args.save, force_rerun=args.force,
                                 iterate_sigreject=args.iterate_sigreject,
                                 markrejbad=args.mark_rejected_bad, markofffitrejbad=args.mark_bad_off_edge_of_fit,
                                 relsigcut=args.relsigcut, abssigcut=args.abssigcut, 
                                 boundt0=args.t0_bound, z=args.redshift, fit_z=args.fitz, z_bounds=args.zbounds,
                                 update_z=args.update, photversion=args.photometry_version,
                                 always_tag_default=args.always_tag_default,
                                 tag_default=( not args.no_tag_default ), versiontags=args.versiontags,
                                 add_new_tags=True, errorpuff=args.errorpuff, logger=logger )
    if s2fit is None:
        logger.error( f"Couldn't find fit tagged {args.versiontag} for {args.object}" )
        return

    print( f'χ min = {s2fit.chisq}, DoF = {s2fit.dof}' )
    print( f'Parameters:' )
    for param in [ 'z', 't0', 'x0', 'x1', 'c', 'mbstar' ]:
        print( f"  {param:>6s} : {getattr(s2fit,param)} ± {getattr(s2fit,f'd{param}')}" )

# ======================================================================

if __name__ == "__main__":
    main()
