#!/usr/bin/env python3
# -*- coding: utf-8 -*-

### astropsf.py : Defines a class for reading a psfex file into a Python object
### Author : Emily Ramey, Rob Knop
### Date : 3/21/23

import sys
import math
import logging
from exposuresource import ExposureSource
from photutils.psf import EPSFModel

import numpy
import pandas
from astropy.io import fits

class AstroPSF:
    
    """ 
    Contains data from a psfex file that can be used to generate a PSF from 
    any point in an image and convert it to an astropy model.
    """
    
    def __init__(self, psffilename,
                 logger=logging.getLogger("main")):
        self.psffilename = psffilename
        
        with fits.open( psffilename, memmap=False ) as psf:
            if ( psf[1].header['POLNAXIS'] != 2 ):
                raise ValueError( f'Unexpected value of POLNAXIS = {psf[1].header["POLNAXIS"]}' )
            if ( psf[1].header['POLNGRP'] != 1 ):
                raise ValueError( f'Unexpected value of POLNGRP = {psf[1].header["POLNGRP"]}' )
            if ( psf[1].header['POLNAME1'] != "X_IMAGE" ) or ( psf[1].header['POLNAME2'] != "Y_IMAGE" ):
                raise ValueError( f'Unexpected values of POLNAME1 ("{psf[1].header["POLNAME1"]}") '
                                  f'and/or POLNAME2 ("{psf[1].header["POLNAME2"]}")' )
            # The -1 is to deal with FITS 1-offset BS
            self.x0 = float( psf[1].header['POLZERO2'] ) - 1
            self.xsc = float( psf[1].header['POLSCAL2'] )
            self.y0 = float( psf[1].header['POLZERO1'] ) - 1
            self.ysc = float( psf[1].header['POLSCAL1'] )

            self.psfsamp = float( psf[1].header['PSF_SAMP'] )
            print(self.psfsamp)

            psforder = int( psf[1].header['POLDEG1'] )
            self.psforder = psforder
            nparam = ( psforder - numpy.arange( 0, psforder+1, dtype=int ) + 1 ).sum()
            if psf[1].data[0][0].shape[0] != nparam:
                raise ValueError( f'Unexpected shape {psf[1].data[0][0].shape} of psf basis '
                                 f'for order={psforder}; '
                                  f'expected ({nparam}, :, :)' )

            psfdata = psf[1].data[0][0]
        
        self.psfdata = psfdata
        
        psfwid = psfdata.shape[1]
        if ( psfwid % 2 ) == 0:
            raise ValueError( f'Even psf width {psfwid} surprises me.' )
        if psfdata.shape[2] != psfwid:
            raise ValueError( f'Non-square psf ( {psfwid} × {psfdata.shape[2]} ) surprises me.' )
        psfoff = int( math.floor( psfwid/2 ) + 0.5 )

        self.psfdex1d = numpy.arange( -(psfwid//2), psfwid//2+1, dtype=int )
        self.psfdex = numpy.meshgrid( self.psfdex1d, self.psfdex1d )

        stampwid = int( math.floor( self.psfsamp * psfwid ) + 0.5 )
        if ( stampwid % 2 ) == 0:
            logger.warning( f'Stampwid came out even ({stampwid}), subtracting ' )
            stampwid -= 1
        
        self.stampwid = stampwid
        
    def generate_psf(self, x, y):
        xc = int( math.floor(x + 0.5) )
        yc = int( math.floor(y + 0.5) )
        half = self.stampwid//2
        origin = numpy.array([xc - half, yc - half])
        psfpalette = numpy.zeros([self.stampwid, self.stampwid])

        psfbase = numpy.zeros_like( self.psfdata[0,:,:] )
        off = 0
        # THINK ABOUT THE ORDER
        #  I think psfex does it in the order 1, x, x², y, yx, y²
        #  But x and y are backwards in numpy compared to FITS,
        #    so the outer loop is x here
        #  Did I do this right?
        for i in range( self.psforder+1 ):
            for j in range( self.psforder+1-i ) :
                psfbase += self.psfdata[off] * ( (x - self.x0) / self.xsc )**i * \
                            ( (y - self.y0) / self.ysc )**j
                off += 1

        # Be clever and find a way to avoid for loop here?  (Uf-da)
        for i in range(self.stampwid): #numpy.arange( xc - stampwid//2, xc + stampwid//2, dtype=int ):
            for j in range(self.stampwid): #numpy.arange( yc - stampwid//2 , yc + stampwid//2, dtype=int ):
                xi = i+origin[0]
                yi = j + origin[1]
                
                xsincarg = self.psfdex1d - (xi-x) / self.psfsamp
                xsincvals = numpy.sinc( xsincarg ) * numpy.sinc( xsincarg/4. )
                xsincvals[ ( xsincarg > 4 ) | ( xsincarg < -4 ) ] = 0
                ysincarg = self.psfdex1d - (yi-y) / self.psfsamp
                ysincvals = numpy.sinc( ysincarg ) * numpy.sinc( ysincarg/4. )
                ysincvals[ ( ysincarg > 4 ) | ( ysincarg < -4 ) ] = 0
                # import pdb; pdb.set_trace()
                psfpalette[ i, j ] = ( xsincvals[:, numpy.newaxis] * 
                                        ysincvals[numpy.newaxis, :] *
                                        psfbase ).sum()
        
        # self.psfpalette = psfpalette
        ### Make an EPSFModel object
        model = EPSFModel(psfpalette, x_0=0.0, y_0=0.0, flux=1.0, oversamp=1)
        return model, psfbase




#################### Code that used to be in subtract.py
### TODO Emily: Check that this function runs when Rob is done with his changes
def psf_photometer(self, x, y, oversamp=4, crit_separation=8, fwhm=4.5, threshold=None, aprad=None, world=True):
    """ Performs PSF photometry at position x,y using PSFex and photutils.psf """
    basename = self.image.basename

    if not self.get_existing_subtraction(): # No subtraction has been done yet
        raise FileNotFoundError( f'Photometry can\'t proceed.' )

    # Open relevant FITS files
    with ( fits.open( self.subfile ) as subhdu,
           fits.open( self.submask ) as submaskhdu,
           fits.open( self.subnoise ) as subnoisehdu,
           fits.open( self.imagefile ) as imagehdu ):

        # Get a masked, subtracted file ready for PSF photometry
        maskedsub = numpy.ma.MaskedArray( subhdu[0].data, submaskhdu[0].data )

        if world: # Get the correct pixel values from RA and DEC
            ra = x
            dec = y
            skypos = SkyCoord( ra=ra, dec=dec, unit=units.deg )
            wcs = WCS( imagehdu[0].header )
            # I *really hope* that this gives me 0-offset pixels
            x, y = wcs.world_to_pixel( skypos )
            # wcs.world_to_pixel seems to return 0-dimensional arrays (?)
            x = float(x)
            y = float(y)
            ### TODO Emily: check that x,y is actually on the image

        # Make an ePSFModel object from the PSFex file at the correct location
        ### TODO Emily: fix the oversamp parameter / understand how it works
        psffilename = pathlib.Path(self.imagefile).with_suffix('.psf')
        epsf = AstroPSF(psffilename, oversamp=oversamp)
        psf = epsf.generate_psf(x, y)

        # Run photometry
        if threshold is None:
            # Set up background
            sigma_clip = SigmaClip(sigma=3.0)
            bkg = MMMBackground(sigma_clip=sigma_clip)
            threshold = 2.5*bkg(imgdata)

        if aprad is None:
            aprad = 6.5 ### TODO Emily: fix this

        dao = DAOPhotPSFPhotometry(crit_separation=crit_separation, threshold=threshold, 
                       fwhm=fwhm, psf_model=psfdata, aperture_radius=aprad,
                       fitshape=psfdata.data.shape)
        ### TODO: what should the input radius be? Also make sure this is the right xy order
        cutout = maskedsub[y-25:y+25,x-25:x+25]
        photresult = dao(cutout)

        ### TODO: do we still need an aperture correction? I'm guessing not?
        ### But then is the flux calculated the same with the magzp?
        return photresult