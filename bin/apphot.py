import sys
import math
import pathlib
import logging
import numbers
import argparse
import numpy
import numpy.ma

import astropy.units
from astropy.io import fits
from photutils.aperture import CircularAperture, ApertureStats, aperture_photometry

from matplotlib import pyplot

import db
import bijaouisky
import sexsky
import psfexreader
from exposuresource import ExposureSource
from photometry import Photomotor

class ApPhot(Photomotor):
    """Aperture photometry on image loaded into database.  Call measure() and apercor()."""

    _ispsf = False
    _isap = True
    
    # ======================================================================
    
    def __init__( self, dbname, expsource, image, default_radius=1.0, default_radius_unit='fwhm',
                  curdb=None, *args, **kwargs ):
        """Normally, get an ApPhot object from photometry.Photomotor.get() rather than constructing ApPhot directly.

        expsource -- An ExposureSource object (from exposuresource.py),
           or the name of one.  (Can be None for some uses of ApPhot, but not all.)
        image -- A db.Image object, or the basename of one
        default_radius -- the radius to use if not specified in furthyer calls (default 1.0)
        default_radius_unit -- 'pixel', 'arcsec', or 'fwhm'
        curdb -- a db.DB object, used only during the constructor (not saved)
        logging -- (Optional) A logging.logger object

        """

        super().__init__( dbname, expsource, image, curdb=curdb, *args, **kwargs )
        
        self.default_radius = default_radius
        self.default_radius_unit = default_radius_unit
        self.catdata = None
        self.psf = None
        
    # ======================================================================
    
    def _radius_to_pixels( self, x, y, radius, radiusunit, seeing ):
        """Convert radius to pixels.

        x -- numpy array of x positions on image (C units)
        y -- numpy array of y positions on image (C units)
        radius -- single value or numpy array of radii
        radiusunit -- 'fwhm', 'arcsec', or 'pixel'

        Returns a numpy array (that may be shape (1,)) of radii in pixels

        """
        radius = numpy.asarray( radius, dtype=numpy.float64 )
        radius = numpy.atleast_1d( radius )
        if radius.shape != x.shape:
            if radius.shape != (1,):
                raise ValueError( "Radius must be a single value or must have the same shape as x and y" )
        if ( radiusunit == 'fwhm' ) or ( radiusunit == 'arcsec' ):
            if ( not self.image.isastrom ) or ( ( radiusunit == 'fwhm' ) and ( not self.image.hascat ) ):
                raise RuntimeError( "Image must have WCS (and also catalog for fwhm units)" )
            if radius.shape == (1, ):
                radius = numpy.full_like( x, radius[0], dtype=numpy.float64 )
            wcs = db.WCS.get_astropy_wcs_for_image( self.image )
            # Figure out pixel scales at all of the positions.  I'm kind
            # of surprised there's not a WCS method for this.
            ctr = wcs.pixel_to_world( x, y )
            ctrdecrad = ctr.dec.to(astropy.units.radian).value
            xl = wcs.pixel_to_world( x-0.5, y )
            xlra = xl.ra.to(astropy.units.arcsec).value * numpy.cos(ctrdecrad)
            xldec = xl.dec.to(astropy.units.arcsec).value
            xh = wcs.pixel_to_world( x+0.5, y )
            xhra = xh.ra.to(astropy.units.arcsec).value * numpy.cos(ctrdecrad)
            xhdec = xh.dec.to(astropy.units.arcsec).value
            yl = wcs.pixel_to_world( x, y-0.5 )
            ylra = yl.ra.to(astropy.units.arcsec).value * numpy.cos(ctrdecrad)
            yldec = yl.dec.to(astropy.units.arcsec).value
            yh = wcs.pixel_to_world( x, y+0.5 )
            yhra = yh.ra.to(astropy.units.arcsec).value * numpy.cos(ctrdecrad)
            yhdec = yh.dec.to(astropy.units.arcsec).value
            # pixel scale = sqrt of |determinant of jacobian| (I think)
            # (or is it the determinant of the jacobean?  Is Shakespeare involved?)
            pixscale = numpy.sqrt( numpy.fabs( ( xhra - xlra ) * ( yhdec - yldec ) -
                                               ( xhdec - xldec ) * ( yhra - ylra ) ) )
            radius /= pixscale
            if radiusunit == 'fwhm': radius *= seeing
        return radius
        
    # ======================================================================

    def _actually_measure( self, x, y, maskedim, boolmask, noisedata, radius, recentroiditerations ):
        """Internal, does the work of measure

        x, y: Must be numpy arrays with C coordinates (in photutils order, i.e. backwards from numpy indexing)
        maskedim: a 2d numpy.MaskedArray
        boolmask: a 2d numpy array of true/false
        noisedata: a 2d numpy array
        radius: radius of aperture in pixels
        recentroiditerations: (see measure)

        """

        def _do_aper( xy, radius, recentroiditerations ):
            aper = CircularAperture( xy, radius )
            centroidsleft = recentroiditerations
            if centroidsleft > 0:
                newxy = numpy.empty_like( xy )
            newx = xy[:,0]
            newy = xy[:,1]
            dx = numpy.zeros_like( newx )
            dy = numpy.zeros_like( newx )
            dxy = numpy.zeros_like( newx )
            while centroidsleft > 0:
                aperstats = ApertureStats( maskedim, aper, error=noisedata, mask=boolmask )
                newx = aperstats.xcentroid
                newy = aperstats.ycentroid
                if isinstance( aperstats.covar_sigx2, astropy.units.quantity.Quantity ):
                    dx = numpy.sqrt( aperstats.covar_sigx2.to_value( astropy.units.pix**2 ) )
                    dy = numpy.sqrt( aperstats.covar_sigy2.to_value( astropy.units.pix**2 ) )
                    dxy = aperstats.covar_sigxy.to_value( astropy.units.pix**2 )
                else:
                    dx = numpy.sqrt( aperstats.covar_sigx2 )
                    dy = numpy.sqrt( aperstats.covar_sigy2 )
                    dxy = aperstats.covar_sigxy
                centroidsleft -= 1
                if centroidsleft > 0:
                    newxy[:,0] = newx
                    newxy[:,1] = newy
                    aper = CircularAperture( newxy, radius )
            return aperture_photometry( maskedim, aper, error=noisedata, mask=boolmask ), newx, newy, dx, dy, dxy

        xy = numpy.empty( ( x.size, 2 ) )
        xy[:,0] = x
        xy[:,1] = y
        if radius.shape == (1,):
            photresult, newx, newy, dx, dy, dxy = _do_aper( xy, radius[0], recentroiditerations )
            phot = photresult['aperture_sum']
            dphot = photresult['aperture_sum_err']
        else:
            phot = []
            dphot = []
            newx = []
            newy = []
            dx = []
            dy = []
            dxy = []
            for i in range( x.size ):
                photresult, newxi, newyi, dxi, dyi, dxyi = _do_aper( xy[i:i+1,:], radius[i], recentroiditerations )
                phot.append( photresult[0]['aperture_sum'] )
                dphot.append( photresult[0]['aperture_sum_err'] )
                newx.append( newxi[0] )
                newy.append( newyi[0] )
                dx.append( dxi[0] )
                dy.append( dyi[0] )
                dxy.append( dxyi[0] )
            phot = numpy.array( phot )
            dphot = numpy.array( dphot )
            newx = numpy.array( newx )
            newy = numpy.array( newy )
            dx = numpy.array( dx )
            dy = numpy.array( dy )
            dxy = numpy.array( dxy )

        if phot.shape == (1, ):
            phot = phot[0]
            dphot = dphot[0]
            newx = newx[0]
            newy = newy[0]
            dx = dx[0]
            dy = dy[0]
            dxy = dxy[0]
            radius = radius[0]
            
        return ( phot, dphot, newx, newy, dx, dy, dxy, radius )
        
    
    # ======================================================================
            
    def _measure( self, x, y, maskedim, noisedata, boolmask,
                  radius=None, radiusunit=None, recenter=False, recentroiditerations=5, tiny=1e-10,
                  noapercor=False ):
        """Perform aperture photometry.

        NOTE documentation all out of date.  See Photomotor._measure and fix what's below
        
        x (float) -- pixel position (or list / numpy array of same) on image
        y (float) -- pixel position (or list / numpy array of same) on image
        
        NEXT THREE ARE WRONG, todo : fix documentation
        imagedata -- A numpy array of the same dimensionas as the image.  (E.g., pass a subtraction aligned
                     with the image here.)  Defaults to reading the image from disk.
        weightdata -- required if imagedata is passed
        maskdata -- required if maskdata is passed

        radius (float) -- radius of aperture (defaults to self.default_radius)
          Can also pass a list/numpy array of same length as x/y if each object gets its own radius.
        radiusunit (string) -- 'fwhm', 'arcsec', or 'pixel' (defaults to self.default_radius_unit)
          if 'fwhm', will use the seeing field of the image object passed to ApPhot constructor
        recenter -- If True, will iterate centroiding in the aperture recentroiditrations times.
           If False, will just do photometry at the specified position.
        recentroiditerations (int) -- How many times to iterate recentroiding
        tiny -- replace weight<=0 values with this to avoid dividing by 0 and sqrt(<0)
        noapercur -- if True, don't do an aperture correction

        Note: if unit is fwhm or arcsec, and recentroiditerations > 0,
        it does *not* recalculate the pixel scale at each recentroid

        iteration, but assumes it's the same as at the beginning.

        Returns a tuple: ( phot, dphot, x, y, { 'radius': radius,
                                                'apercor': apercor,
                                                'dapercor': dapercor )

        Each element is either a float or a 1-d numpy array.  phot and
        dphot are in ADU and are *not* aperture corrected.  radius is in
        pixels.  If a single radius was passed in pixels, then radius
        will be a single float; otherwise, radius will be an array whose
        length matches the other arrays.  apercor and dapercor are in
        magnitudes; dapercor has been propagated into dphot.

        """
        x = numpy.atleast_1d( numpy.asarray( x, dtype=numpy.float64 ) )
        y = numpy.atleast_1d( numpy.asarray( y, dtype=numpy.float64 ) )
        if x.shape != y.shape:
            raise ValueError( "Shapes of x and y must match." )
        # if x.ndim != 1:
        #     raise ValueError( "x and y must either be a single value or a one-dimensional list/array" )
        if radius is None:
            radius = self.default_radius
        if radiusunit is None:
            radiusunit = self.default_radius_unit
        radius = self._radius_to_pixels( x, y, radius, radiusunit, self.image.seeing )

        if not recenter:
            recentroiditerations = 0
        
        phot, dphot, newx, newy, dx, dy, dxy, radius =  self._actually_measure( x, y, maskedim, boolmask, noisedata,
                                                                                radius, recentroiditerations )
        if isinstance( phot, numbers.Number ):
            covar = numpy.array( [ [ dphot**2, 0., 0. ],
                                   [ 0., dx**2, dxy ],
                                   [ 0., dxy, dy**2 ] ] )
        else:
            covarshape = list( x.shape )
            covarshape.extend( [ 3, 3 ] )
            covar = numpy.empty( covarshape )
            covar[ ..., 0, 0 ] = dphot ** 2
            covar[ ..., 1, 1 ] = dx ** 2
            covar[ ..., 2, 2 ] = dy ** 2
            covar[ ..., 0, 1 ] = 0.
            covar[ ..., 0, 2 ] = 0.
            covar[ ..., 1, 0 ] = 0.
            covar[ ..., 2, 0 ] = 0.
            covar[ ..., 1, 2 ] = dxy
            covar[ ..., 2, 1 ] = dxy

        if not noapercor:
            # Aperture correction

            def findac( _rad, _x0, _y0 ):
                ac, dac = self.apercor( _x0, _y0, radius=_rad )
                return ac[-1], dac[-1]

            if isinstance( phot, numbers.Number ):
                ac, dac = findac( radius, newx, newy )
                phot *= 10**( ac / -2.5  )
                dphot *= 10**( ac / -2.5 )
                tmp = phot * math.fabs( 0.4 * math.log(10) * 10**(ac/-2.5) ) * dac
                dphot = math.sqrt( dphot*dphot + tmp*tmp )
                covar[ ..., 0, 0 ] = dphot ** 2
                return phot, newx, newy, covar, { 'radius': radius, 'apercor': ac, 'dapercor': dac }

            apercors = numpy.empty_like( phot )
            dapercors = numpy.empty_like( dphot )
            for i, x0 in numpy.ndenumerate( newx ):
                y0 = newy[i]
                if isinstance( radius, numbers.Number ):
                    rad = radius
                else:
                    rad = radius[i]
                ac, dac = findac( rad, x0, y0 )
                apercors[i] = ac
                dapercors[i] = dac
                phot[i] *= 10**( ac / -2.5 )
                dphot[i] *= 10**( ac / -2.5 )
                tmp = phot[i] * math.fabs( 0.4 * math.log(10) * 10**(ac/-2.5) ) * dac
                dphot[i] = math.sqrt( dphot[i] * dphot[i] + tmp * tmp )
                covar[ i, 0, 0 ] = dphot[i] ** 2

        else:
            apercors = None
            dapercors = None
                
        return phot, newx, newy, covar, { 'radius': radius, 'apercor': apercors, 'dapercor': dapercors }
            
    # ======================================================================

    def apercor( self, x0, y0, radius=None, radiusunit='pixels', method='psf', **kwargs ):
        """Determine aperture correction

        x0, y0 - position on image to determine ac
        radius, radiusunit - Radius of aperture; defaults to the default one passed to object constructor
        method - 'psf' (to use the PSFEx file) or 'stars' (to use stars identified on the image)
        
        additional arguments are passed to the function that implements the method.

        Returns ac, dac, two lists.  For the default ('psf') method,
        these are always single-element lists, and dac is always [0.].
        For the 'stars' method, the length of the lists is the same as
        the length of the infrad array (which is the multiplier to apply
        to the aperture radius for the big aperture that approximates
        infinity, default [2,3,4,5]).

        """

        if radius is None:
            radius = self.default_radius
            radiusunit = self.default_radius_unit

        radius = self._radius_to_pixels( numpy.array( [x0] ), numpy.array( [y0] ),
                                         radius, radiusunit, self.image.seeing )[0]

        if method == 'psf':
            return self.apercor_from_psf( x0, y0, radius, **kwargs )
        elif method == 'stars':
            return self.apercor_from_stars( radius=radius, x0=x0, y0=y0, radiusunit='pixels', **kwargs )
        else:
            raise ValueError( f'Unknown apercor method {method}' )
    
    
    # ======================================================================

    def apercor_from_psf( self, x0, y0, radius ):

        """Determine the aperture correction for an aperture radius in pixels using the PSFEx file.

        x0, y0 — position on the image where the psf is evaluated
        radius — radius of aperture in pixels

        """
        if self.psf is None:
            psffile = self.expsource.get_file_from_archive( f"{self.image.basename}.psf" )
            self.psf = psfexreader.PSFExReader( psffile, logger=self.logger )
        clip = self.psf.getclip( x0, y0, 1.0 )
        if ( radius > clip.shape[0] // 2 ):
            raise RuntimeError( f"Radius {radius} is too big for clip shape {clip.shape}" )
        xc = int( math.floor( x0 + 0.5 ) )
        yc = int( math.floor( y0 + 0.5 ) )
        aper = CircularAperture( ( clip.shape[1] // 2 + (x0-xc), clip.shape[0] // 2 + (y0-yc) ), radius )
        res = aperture_photometry( clip, aper )
        # ****
        # self.logger.info( f"xc={xc}, yc={yc}, x0={x0}, y0={y0}, clip.shape={clip.shape}; "
        #                   f"aperture at {clip.shape[1]//2+(x0-xc):.2f},{clip.shape[0]//2+(y0-yc):.2f} "
        #                   f"radius {radius:.2f}" )
        # fits.writeto( "testpsf.fits", clip, overwrite=True )
        # ****
        return [ 2.5 * math.log10( res['aperture_sum'][0] ) ], [ 0. ]
        
    # ======================================================================

    def apercor_from_stars( self, radius=None, infrad=[2,3,4,5], radiusunit='pixels',
                            x0=None, y0=None, xwid=400, ywid=400,
                            cat=None,
                            tiny=1e-10, recentroiditerations=0,
                            outplot=None, whichinfrad=None,
                            imagedata=None, weightdata=None, maskdata=None,
                            maskedim=None, noisedata=None, boolmask=None ):
        """Determines the aperture correction for an aperture of radius apperrad by measuring "stars" on the image.

        This method is probably not as reliable as apercor_from_psf, as it depends on the
        SEXTractor identification of stars as being good.  (Of course, the other method depends
        on the psfex PSF being good.)

        radius -- radius of aperture; if None, will use self.default_radius, and
          radunit will be set to self.default_radius_unit
        infrad -- multipliers of aperrad to consider infinite radius.
        radunit -- 'pixel', 'fwhm', or 'arcsec'
        x0, y0, xwid, ywid : if (xwid,ywid) are non-None, only include
          stars that are within xwid of x0 and ywid of y0.  THESE ARE C
          COORDINATES (i.e. 0-offset).  They are in photutils order
          (i.e. backwards from numpy indexing, but matching what's in
          the cat file).  If x0, y0 are None, they will be set to the
          center of the image.
        cat -- Sextractor catalog file path.  If None, figures it out from self.image
        tiny : replace weight<=0 values with this to avoid dividing by 0 and sqrt(<0)
        recentroiditerations : number of times to iterate centroid on primary aperture.
          (Won't bother on bigger apertures.)
        outplot : filename to plot a diagnostic plot to
        whichinfrad : Something to do with outplot....
        imagedata : 2d numpy array.  If None, reads from the database.
          If not None, must be sky subracted.
        weightdata : 2d numpy array; if None, reads from the database.
        maskdata : 2d numpy array; if None, reads from the databse
        maskedim :
        noisedata :
        boolmask :

        Returns two arrays: the magnitude addition to make corresponding
        to the elements of infrad, and the uncertainties on those
        values.

        """

        if radius is None:
            radius = self.default_radius
            radiusunit = self.default_radius_unit
        radius = float( radius )
            
        if ( xwid is None ) != ( ywid is None ):
            raise ValueError( "ApPhot.apercor: either both of xwid and ywid must be specified, or both must be None." )

        if ( not isinstance( radius, numbers.Number ) ) and ( radius is not None ):
            raise ValueError( "ApPhot.apercor: pass a single value for radius." )
        
        if cat is None:
            if self.catdata is None:
                if self.expsource is None:
                    raise RuntimeError( "Error, unknown exposure source" )
                catpath = self.expsource.get_file_from_archive( f"{self.image.basename}.cat" )
                with fits.open( catpath ) as cathdu:
                    self.catdata = cathdu[2].data
            catdata = self.catdata
        else:
            with fits.open( cat ) as cathdu:
                catdata = cathdu[2].data

        # Get the image data
        if ( maskedim is not None ):
            if ( noisedata is None ) or ( boolmask is None ):
                raise RuntimeError( "noisedata and boolmask required with maskedim" )
            if ( imagedata is not None ) or ( weightdata is not None ) or ( maskdata is not None ):
                raise RuntimeError( "Must specify at most one of (maskedim,noisedata,boolmask) or "
                                    "(imagedata,weightdata,maskdata)" )
        else:
            if ( noisedata is not None ) or ( boolmask is not None ):
                raise RuntimeError( "Can't give noisedata or boolmask without maskedim" )
            maskedim, noisedata, boolmask = self._load_data( imagedata, weightdata, maskdata, tiny )

        if radius is None:
            radius = self.default_radius
        if radiusunit is None:
            radiusunit = self.default_radius_unit
        xctr = x0
        yctr = y0
        # photutils/FITS ordering is backwards from numpy array ordering
        if xctr is None:
            xctr = maskedim.shape[1] / 2.
        if yctr is None:
            yctr = maskedim.shape[0] / 2.
            
        radius = self._radius_to_pixels( numpy.array( [xctr] ), numpy.array( [yctr] ),
                                         radius, radiusunit, self.image.seeing )[0]

        goodcat = catdata[ catdata['IMAFLAGS_ISO'] == 0 ]
        stars = goodcat[ goodcat['CLASS_STAR'] > 0.95 ]
        self.logger.debug( f'{len(stars)} of {len(catdata)} objects are considered stars' )

        # Limit to the region of the image we care about
        if xwid is not None:
            stars = stars[ ( numpy.fabs(stars["X_IMAGE"]-(int(xctr)+1)) <= xwid ) &
                           ( numpy.fabs(stars["Y_IMAGE"]-(int(yctr)+1)) <= ywid ) ]
            self.logger.debug( f'{len(stars)} are at {int(xctr)}+-{xwid}, {int(yctr)}+-{ywid}' )

        # Throw out stars that have another detected source within 2*max(infrad)*aperrad
        # Don't worry about masked out stuff, because photutils should (I REALLY HOPE)
        # also mask out those pixels
        bigrad = max(infrad) * radius
        starmindist = numpy.zeros( stars["X_IMAGE"].shape )
        for i in range(len(stars)):
            x0 = stars["X_IMAGE"][i]
            y0 = stars["Y_IMAGE"][i]
            dist2 = ( goodcat["X_IMAGE"] - x0 )**2 + ( goodcat["Y_IMAGE"] - y0 )**2
            # Omit the star itself!
            dist2 = dist2[ dist2>0. ]
            starmindist[i] = math.sqrt( dist2.min() )
        stars = stars[ starmindist > 2*bigrad ]
        self.logger.debug( f'{len(stars)} left after throwing out things with nearby sources' )

        if len(stars) < 5:
            raise RuntimeError( "Less than 5 stars left for aperture correction!" )

        # Do photometry; -1 because the cat file has 1-offset positions
        starx = stars["X_IMAGE"] - 1
        stary = stars["Y_IMAGE"] - 1
        rads = [ radius ]
        rads.extend( [ radius * i for i in infrad ] )
        apphot = numpy.empty( ( len(stars), len(rads) ) )
        dapphot = numpy.empty( ( len(stars), len(rads) ) )
        for i in range(len(rads)):
            # For the main aperture, recentroid, but not for the large apertures (where it won't matter)
            recen = recentroiditerations if i == 0 else 0
            ( phot, dphot, newx, newy,
              dx, dy, dxy, radius ) = self._actually_measure( starx, stary, maskedim, boolmask, noisedata,
                                                              numpy.array( [rads[i]] ), recen )
            apphot[:, i] = phot[:]
            dapphot[:, i] = dphot[:]

        maksedim = None
        boolmask = None
        noisedata = None
            
        apercor = apphot[ :, 1: ] / apphot[ :, 0:1 ]
        apercorvar = numpy.square(apercor) * ( numpy.square( dapphot[:, 1:] / apphot[:, 1:] ) +
                                               numpy.square( dapphot[:, 0:1] / apphot[:, 0:1] ) )
        w = ( numpy.isnan( apercor ) | ( numpy.isnan( apercorvar ) ) | ( apercorvar <= 0 ) )
        if w.sum() > 0:
            self.logger.debug( f'Threw out {w.sum()} NaN/0-error measurements' )
        # Probably I should use a numpy masked array, but this will have the right effect
        apercor[w] = 0.
        apercorvar[w] = 1e32
        avgapercor = (apercor/apercorvar).sum(axis=0) / (1/apercorvar).sum(axis=0)
        davgapercor = numpy.sqrt( 1 / (1/apercorvar).sum(axis=0) )

        magapercor = -2.5 * numpy.log10( avgapercor )
        dmagapercor = numpy.fabs( -2.5 / math.log(10.) * davgapercor / avgapercor )

        if outplot is not None:
            if whichinfrad is None:
                whichinfrad = len(infrad) - 1
            fig = pyplot.figure( figsize=(6,8), tight_layout=True )
            ax = fig.add_subplot( 2, 1, 1 )
            ax.set_xlabel( "flux" )
            ax.set_ylabel( "apercor (flux)" )
            ax.set_ylim( avgapercor[whichinfrad]-0.5, avgapercor[whichinfrad]+0.5 )
            ax.set_xscale( "log" )
            ax.errorbar( apphot[:, whichinfrad+1], apercor[:, whichinfrad],
                         yerr=numpy.sqrt(apercorvar[:, -1]),
                         marker='o', linestyle="", color="blue" )
            ax.plot( ax.get_xlim(), [ avgapercor[whichinfrad], avgapercor[whichinfrad] ], color="red"  )
            ax = fig.add_subplot( 2, 1, 2 )
            ax.set_xlabel( "radius" )
            ax.set_ylabel( "apercor (mag)" )
            ax.plot( numpy.array(infrad)*radius, magapercor, marker="o", linestyle="-" )
            fig.savefig( outplot )

        return magapercor, dmagapercor

# ======================================================================

def main():
    parser = argparse.ArgumentParser( 'apphot', description="Do aperture photometry at position on image",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( "-b", "--basename", required=True, help="Basename of image in database" )
    parser.add_argument( "-x", required=True,
                         help=("x position on image.  This is a 0-offset position, in photutils/FITS ordering."
                                "I.e., x,y matches what you'd see in ds9, but the lower left pixel is 0,0 not 1,1") )
    parser.add_argument( "-y", required=True, help="y position on image" )
    parser.add_argument( "-i", "--image", default=None,
                         help=("Image filename.  If specified, must also give --weight and --mask. "
                               "if you also give --basename, that image's databsae info will be used, "
                               "but the specified image data will be used in place of the database image's data" ) )
    parser.add_argument( "-e", "--expsource", default=None,
                         help="Name of exposure source.  Usually not needed, will figure it out automatically." )
    parser.add_argument( "-w", "--weight", default=None, help="Weight (invar) filename" )
    parser.add_argument( "-m", "--mask", default=None, help="Mask (int16, 0=good, non-0=bad) filename" )
    parser.add_argument( "-r", "--radius", type=float, default=1.0, help="Radius of aperture" )
    parser.add_argument( "-u", "--radunit", default='fwhm', help="Unit of radius: 'pixel', 'arcsec', or 'fwhm'." )
    parser.add_argument( "-c", "--centroid-iterations", default=5, type=int,
                         help="Iterate recentroid this many times; set to 0 to force passed position." )
    parser.add_argument( "-a", "--apercor", default=False, action="store_true",
                         help="Do aperture correction.  Requires image in database with catalog and expsource." )
    parser.add_argument( "--apercor-radii", type=float, default=[5], nargs='+',
                         help=( "Use big apertures these factors of main aperture for aperture correction." ) )
    parser.add_argument( "--width", default=None, type=float,
                         help=( "Width of box around x, y to include stars for aperture correction; "
                                "default: whole image" ) )
    parser.add_argument( "-p", "--plotfile", default=None,
                         help="Do a diagnostic plot of aperture correction to this file." )
    parser.add_argument( "-v", "--verbose", action="store_true", default=False,
                         help="Show debug log info (default, just info and below)" )
    
    args = parser.parse_args()

    logger = logging.getLogger( __name__ )
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s',
                                            datefmt='%Y-%m-%d %H:%M:%S' ) )
    logger.setLevel( logging.DEBUG if args.verbose else logging.INFO )
    
    if args.image is not None:
        if ( args.weight is None ) or ( args.mask is None ):
            raise RuntimeError( "If you give image, must also give weight and mask" )

    with db.DB.get() as curdb:
        imgobj = db.Image.get_by_basename( args.basename, curdb=curdb )
        if imgobj is None:
            raise RuntimeError( f"Failed to find image with basename {args.basename} in database." )
        if ( imgobj.exposure_id is None ) and ( args.expsource is None ):
            raise RuntimeError( f"Image in databsae doesn't have an exposoure source, and you didn't pass one." )
        if args.expsource is not None:
            expsource = ExposureSource.get( args.expsource )
        else:
            # This is kinda bass-ackwards
            expsourcedb = imgobj.exposure.exposuresource
            expsource = ExposureSource.get( expsourcedb.name, expsourcedb.secondary )
        catpath = None
        if args.apercor:
            if not imgobj.hascat:
                raise RuntimeError( "Image must have a catalog in the database for apercor" )
            
        imagedata = None
        weightdata = None
        maskdata = None
        if args.image is not None:
            with fits.open( args.image ) as hdul:
                imagedata = hdul[0].data
        if args.weight is not None:
            with fits.open( args.weight ) as hdul:
                weightdata = hdul[0].data
        if args.mask is not None:
            with fits.open( args.mask ) as hdul:
                maskdata = hdul[0].data

        photor = ApPhot( expsource, imgobj, args.radius, args.radunit, curdb=curdb, logger=logger )
        phot, dphot, x, y, info = photor.measure( args.x, args.y, imagedata, weightdata, maskdata,
                                                  recentroiditerations=args.centroid_iterations )
        radius = info['radius']
        ndecs = 0
        if math.log10( dphot ) < 1:
            ndecs = -( math.log10( dphot ) - 1 )
        if args.apercor:
            magapercor, dmagapercor = photor.apercor( args.x, args.y, args.radius, args.apercor_radii, args.radunit,
                                                      xwid=args.width, ywid=args.width,
                                                      recentroiditerations=args.centroid_iterations,
                                                      outplot=args.plotfile,
                                                      imagedata=imagedata, weightdata=weightdata, maskdata=maskdata )
            if args.plotfile is not None:
                logger.info( f"Aperture correction diagnostic plot written to {args.plotfile}" )

        print( f"\nAperture photometry at {args.x},{args.y} in radius of {args.radius} {args.radunit}" )
        print( f"\nUncorrected flux: {phot:.{ndecs}f} ± {dphot:.{ndecs}f} ; "
               f"position is ({x:.02f}, {y:.02f}), radius is {radius:.02f}" )

        if args.apercor:
            print( "\nAperture corrections:" )
            for radfac, ac, dac in zip( args.apercor_radii, magapercor, dmagapercor ):
                print( f"{radfac:4.1f} × radius : {ac:7.3f} +- {dac:7.3f}" )
                
            phot *= 10**( magapercor[-1] / -2.5 )
            dphot *= 10**( magapercor[-1] / -2.5 )
            tmp = phot * math.fabs( 0.4 * math.log(10) * 10**(magapercor[-1]/-2.5) ) * dmagapercor[-1]
            dphot = math.sqrt( dphot * dphot + tmp*tmp )

            print( f"\nAperture corrected flux: {phot:.{ndecs}f} ± {dphot:.{ndecs}f}" )

# ======================================================================

if __name__ == "__main__":
    main()
