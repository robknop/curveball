import collections.abc
import logging
import numbers

import numpy

from psfexreader import PSFExReader
from photometry import Photomotor
import psffit

class PSFPhotPSFEx(Photomotor):
    """PSF Photometry using cc_code/libsrc/psffit.cc.  Works directly with psfex PSFs."""

    _ispsf = True
    _isap = False

    def __init__( self, dbname, expsource, image, curdb=None, logger=logging.getLogger("main"), *args, **kwargs ):
        """Normally, get a PSFPhotPSFEx object from photometry.Photomotor, get()
        rather than constructing directly.

        expsource -- An ExposureSource object (from exposuresource.py),
           or the name of one.  (Can be None for some uses of ApPhot, but not all.)
        image -- A db.Image object, or the basename of one.  Will look for
           both the image and psf under this name.
        curdb -- a db.DB object, used only during the constructor (not saved)
        logging -- (Optional) A logging.logger object
        
        """

        super().__init__( dbname, expsource, image, *args, **kwargs )

        expsource.ensure_image_local( image.basename, logger=logger )
        self.image_relpath = expsource.relpath( f'{image.basename}.fits' )
        self.image_file = expsource.local_path( f'{image.basename}.fits' )
        self.psf_relpath = self.image_relpath.parent / f'{image.basename}.psf'
        self.psf_file = expsource.local_path( self.psf_relpath.name )
        expsource.archive.download( self.psf_relpath, self.psf_file )

        if not self.psf_file.is_file():
            raise RuntimeError( f"Can't find psfex file {self.psf_file.name}" )


        self.psf = PSFExReader( self.psf_file, logger=logger )

    def _measure( self, x, y, maskedimage, noisedata, boolmask, recenter=False, tiny=1e-10, clipsize=None ):
        scalar = False

        xarr = numpy.atleast_1d( numpy.asarray( x, dtype=numpy.float64 ) )
        yarr = numpy.atleast_1d( numpy.asarray( y, dtype=numpy.float64 ) )

        if xarr.shape != yarr.shape:
            raise TypeError( "Shapes of x and y must match" )
            
        weight = numpy.empty( maskedimage.shape )
        wbad = ( maskedimage.mask )
        weight[ wbad ] = 0.
        weight[ ~wbad ] = 1 / ( noisedata[ ~wbad ] **2 )

        if clipsize is None:
            clipsize = self.psf.stampwid

        phot = numpy.empty_like( xarr, dtype=numpy.float64 )
        dphot = numpy.empty_like( xarr, dtype=numpy.float64 )
        xpos = numpy.empty_like( xarr, dtype=numpy.float64 )
        ypos = numpy.empty_like( xarr, dtype=numpy.float64 )
        covarshape = list( phot.shape )
        covarshape.extend( [ 3, 3 ] )
        covars = numpy.empty( covarshape )
            
        # So, yeah, I probably outta vectorize psffit.psffit, and the C version too.
        # Eh, whatevs.
        for dex, x0 in numpy.ndenumerate( xarr ):
            y0 = yarr[ dex ]
            ix0 = int( x0 + 0.5 )
            iy0 = int( y0 + 0.5 )

            clip = numpy.zeros( ( clipsize, clipsize ) )
            clipwt = numpy.zeros( ( clipsize, clipsize ) )

            xclipmin = 0
            xclipmax = clipsize
            clipx0 = ix0 - clipsize // 2
            xmin = clipx0
            xmax = xmin + clipsize
            yclipmin = 0
            yclipmax = clipsize
            clipy0 = iy0 - clipsize // 2
            ymin = clipy0
            ymax = ymin + clipsize

            if xmin < 0:
                xclipmin = -xmin
                xmin = 0
            if ymin < 0:
                ycipmin = -ymin
                ymin = 0
            if xmax > maskedimage.shape[1]:
                xclipmax -= ( xmax - maskedimage.shape[1] )
                xmax = maskedimage.shape[1]
            if ymax > maskedimage.shape[0]:
                yclipmax -= ( ymax - maskedimage.shape[0] )
                ymax = maskedimage.shape[0]

            clip[yclipmin:yclipmax, xclipmin:xclipmax] = maskedimage.data[ymin:ymax, xmin:xmax]
            clipwt[yclipmin:yclipmax, xclipmin:xclipmax] = weight[ymin:ymax, xmin:xmax]

            initflux = maskedimage.sum()
            initx = x0
            inity = y0

            param, covar = psffit.psffit( clip, clipwt, self.psf_file, clipx0, clipy0,
                                          initflux, initx, inity,
                                          fixpos=not recenter, method='lm' )


            phot[dex] = param[0]
            xpos[dex] = param[1]
            ypos[dex] = param[2]
            covars[dex] = covar

        if isinstance( x, numbers.Number ):
            phot = phot[0]
            xpos = xpos[0]
            ypos = ypos[0]
            covars = covars[0]
            
        return phot, xpos, ypos, covars, {}
