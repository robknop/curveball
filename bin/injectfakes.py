import sys
import math
import shutil
import logging
import argparse
import numbers
import glob

import numpy
import numpy.random
import scipy.stats
import pandas
from astropy.io import fits

import db
import exposuresource
import psfexreader
import subtract
import photometry
import apphot

#NOTE.  There is something scarily circular about using the PSFEx file
# to inject PSFs, and then that same file to do aperture corrections or
# PSF fitting.

class FakeInjector:
    """A class for injecting fake transients into an image, subtracting, and measuring them.

    To use:

    * Instantiate the class with the exposure source, image, and
      reference.  Ideally, this image and reference have already been
      used in a real subtraction, but really waht's needed is that both
      must be astrometrically and photometrically calibrated, and the
      image must have a catalog and a psf.

    * Call repeatedtrials() to inject multiple PSFs mutiple time and collect statistics.

    * Alternatively, instead of repeatedtrials(), do it manually by calling:
       * either inject_psfs() to inject psfs at specfiic positions with specifix fluxes
         or scatterpsfs() to distribute psfs around the image
       * write_and_save_image() to save the image with the injected psfs
       * subtract() to perform the subtraction
       * measurefakes() to get the photometry of the fakes you injected
       * cleanup() to remove the temporary lines added to the database and the temporary
         files

    Can be run from the command-line, in which case it's just a wrapper
    around instantiating an object and calling repeatedtrials().  Run
    with "--help" for more information.

    """


    def __init__( self, expsource, image, reference, subalg='alard1',
                  curdb=None, logger=logging.getLogger("main") ):
        """Create a fake injector

        expsource — either an exposuresource.ExposureSource object, or the name of the exposure source
        image — either a db.Image object, or the basename of the image
        reference — either a db.Image object, or the basename of the reference
        subalg — name of the subtraction algorithm (default: 'alard1')
        curdb (optional) — a db.DB Object
        logger (optional) — a logger.logging object

        Will read the image data, and instantiate a psfexreader.PSFExReader from the image psf.

        """
        self.logger =logger
        self.expsource = expsource
        self.image = image
        self.reference = reference
        self.subalg = subalg

        self.imagedata = None
        self.fakeimgobj = None
        self.fakeimagedata = None
        self.fakeweightdata = None
        self.fakex = None
        self.fakey = None
        self.fakeflux = None
        self.subobj = None

        with db.DB.get(curdb) as curdb:
            if not isinstance( expsource, exposuresource.ExposureSource ):
                self.expsource = exposuresource.ExposureSource.get( expsource )
            if not isinstance( image, db.Image ):
                self.image = db.Image.get_by_basename( image, curdb=curdb )
                if self.image is None:
                    raise RuntimeError( f"Unknown image {image}" )
            if not isinstance( reference, db.Image ):
                self.reference = db.Image.get_by_basename( reference, curdb=curdb )
                if self.reference is None:
                    raise RuntimeError( f"Unknown reference image {image}" )

            if not ( self.image.isastrom and self.image.isphotom and self.image.hascat ):
                raise RuntimeError( "Image must be astro/photo calibrated and must have catalog." )

            self.expsource.ensure_image_local( self.image.basename, curdb=curdb, logger=self.logger )
            self.imagepath = self.expsource.local_path_from_basename( self.image.basename, logger=self.logger )
            if self.imagepath is None:
                raise RuntimeError( f"Can't find {self.image.basename}.fits on local filesystem; "
                                    f"this shouldn't happen." )
            self.weightpath = self.expsource.local_path_from_basename( self.image.basename, filetype='weight',
                                                                       logger=self.logger )
            if self.weightpath is None:
                raise RuntimeError( f"Can't find {self.imagepath.basename}.weight.fits on local filesystem; "
                                    f"this shouldn't happen." )

            self.imagegain = self.expsource.gain( self.imagepath )

            self.catpath = self.expsource.get_file_from_archive( f"{self.image.basename}.cat", logger=self.logger )
            if self.catpath is None:
                raise RuntimeError( f"Can't find {self.image.basename}.cat, which is required." )
            self.psfpath = self.expsource.get_file_from_archive( f"{self.image.basename}.psf", logger=self.logger )
            if self.psfpath is None:
                raise RuntimeError( f"Can't find {self.image.basename}.psf, which is required." )

            with fits.open( self.imagepath, memmap=False ) as hdul, fits.open( self.weightpath, memmap=False) as wgthdul:
                self.imagedata = hdul[0].data
                self.imageheader = hdul[0].header
                self.weightdata = wgthdul[0].data
                self.weightheader = wgthdul[0].header

            self.psfex = psfexreader.PSFExReader( self.psfpath, logger=self.logger )


    # ======================================================================

    def inject_psfs( self, x, y, flux, addtoexisting=False, noisy=True, curdb=None, rng=None):
        """Inject PSFs on to the image

        x, y — Position(s) of the PSF to inject.  Each is either a
          single number, or a numpy array.  Coordinates are photutils
          coordaintes: that is, the ctner lower-left pixel is (0,0), and
          2d numpy arrays (using the standard numpy ordering) are indexed by [y,x].

        flux — Flux(es) of the PSF to inject in ADU.  Must either be a
          single number (in which case psfs of the same fluxes are
          injected at all (x,y)), or must have the same length as x and
          y.

        addtoexisting — if True, adds more injected psfs to what's
          already been injected.  If False, removes all previously
          injected PSFs (if any) and reinitilzeds the with-fakes image
          data to the original image dataz

        noisy — Add shot noise to the added pixel values.  Will use the
          image gain as determined from the database / image header to
          figure out the right scaling.

        curdb (optional) — a db.DB object

        rng (optional) — a numpy.random.Generator object.  If nothing is
          passed here, this method will call numpy.random.default_rng(),
          which seeds from system entropy.  Pass an already-initialized
          generator here if you want to control the seed.

        PSFs will be injected on to self.fakeimagedata; if noisy=True,
        then self.fakeweightdata will also be modified.  If
        addtoexisting=False, then those two images will be recopied from
        self.imagedata and self.weightdata.

        """
        if not addtoexisting:
            self.fakeimgobj = None
            self.fakeimagedata = None
            self.fakieweightdata = None
            self.fakex = None
            self.fakey = None
            self.fakeflux = None

        if self.fakeimagedata is None:
            self.fakeimagedata = self.imagedata.copy()
            self.fakeweightdata = self.weightdata.copy()
            self.fakex = []
            self.fakey = []
            self.fakeflux = []

        x = numpy.atleast_1d( x )
        y = numpy.atleast_1d( y )
        flux = numpy.atleast_1d( flux )

        if x.shape != y.shape:
            raise ValueError( f"Size of x and y don't match." )
        if flux.shape == (1,):
            flux = numpy.full( x.shape, flux, dtype=numpy.float64 )
        if flux.shape != x.shape:
            raise ValueError( f"Flux must either be a single number, or same shape as x and y." )

        for curx, cury, curflux in zip( x, y, flux ):
            self.psfex.add_psf_to_image( self.fakeimagedata, curx, cury, curflux, gain=self.imagegain,
                                         noisy=noisy, weight=self.fakeweightdata, rng=rng )

        self.fakex.extend( x )
        self.fakey.extend( y )
        self.fakeflux.extend( flux )

    # ======================================================================

    def _flatfluxen( self, n, minmag, maxmag, rng ):
        m = rng.random( n ) * ( maxmag-minmag ) + minmag
        flux = 10 ** ( 0.4* ( self.image.magzp - m ) )
        return flux

    def _uniformpos( self, n, rng ):
        return ( rng.random( n ) * self.imagedata.shape[1],
                 rng.random( n ) * self.imagedata.shape[0] )

    def scatter_psfs( self, n, xs=None, ys=None, fluxen=None,
                      minmag=17, maxmag=22, fluxdistrib='flat', distribparam=[],
                      nearobj=False, objdistmean=5., objdistsigma=3.,
                      noisy=True, rng=None, curdb=None ):
        """Inject multiple psfs on to the image

        n — number of psfs to inject

        xs, ys — positions of psfs.  If xs and ys are None, will
          randomly inject psfs around the image.  Coordinates are
          photutils coordinates.

        fluxen — fluxes of psfs.  If None, will use the method specified
          in fluxdistrib (using minmag, maxmag, and the parameters in
          distribparam) to randomly generate fluxes

        fluxdistrib — Currently only "flat" is implemented, which
          generates fluxes unitformly between minmag and maxmag.  Uses
          the image zeropoint from the database to turn magnitudes into
          fluxes.

        minmag, maxmag — Range of magnitudes for random flux generation.

        distribuparam — Additional parameters for the fluxdistrib
          method.  Currently unused, as "flat" has no additional
          parmaeters.

        nearobj — NOT IMPLEMENTED
        objdistmean
        objdistsigma

        noisy — If True (default), will add random shot noise to injected psfs

        rng (optional) — a numpy.random.Generator object.  If not
          specified, will create one with numpy.random.default_rng(),
          which seeds with system entropy.  Pass something here if you
          want to control the seed.

        curdb (optional) — a db.DB object

        Returns xs, ys, fluxen of the added psfs, all as numpy arrays
        (potentially of length 1).  This will just be a copy of the
        parameters (with fluxen expanded to a numpy array with the same
        length as xs and ys) if they were passed; otherwise, returns the
        randomly generated ones.  The latter allows subsequent calls to
        scatter_psfs() to use the same set of randomly generated psfs
        (though the realization of the noise will be different each
        time, unless you pass a fresh rng with the same seed each time).

        Modifies self.fakeimagedata (starting by copying it from
        self.imagedata, so that only the PSFs injected by this call will
        be on it); if noisy=True, also modifies self.fakeweightdata.

        """

        if ( ( xs is None ) != ( ys is None ) ) or ( ( xs is None ) != ( fluxen is None ) ):
            raise ValueError( "If you give any of (xs, ys, fluxen), you must give all three." )

        if xs is None:
            if rng is None:
                rng = numpy.random.default_rng()

            if nearobj:
                raise NotImplementedError( "Near objects not yet implemented" )
            else:
                xs, ys = self._uniformpos( n, rng )

                # In practice, often the reference image won't cover the
                # entire search image, so a bunch of the positions we
                # generated may be entirely masked out.  Try to limit
                # how bad this is by slowly going through all of the
                # positions, seeing if they're masked on either the
                # image or the reference, and if so, throwing them out
                # and regenerating them.
                self.logger.info( "Rerandomizing positions where things are masked..." )
                nmoved = 0

                # To do this, though, we need the warped ref, so we have to make a
                # Subtractor object to create one.  Go ahead and do the full subtraction,
                # which is a little profligate.
                # I gotta think about this.  Ideally, I should be able to just do
                # the remap, but the handling of existing subtractions and the archive
                # and all of that is complicated.  Probably this means
                # refactoring subtraction....
                self.logger.info( "...Performing subtraction for base image to get remapped ref mask..." )
                tmpstor = subtract.Subtractor.get( self.subalg, self.expsource, self.image, self.reference,
                                                   logger=self.logger )
                subobj = tmpstor.subtract( searchtag='fakeinject', tags=['fakeinject'], newtags=True )
                self.logger.info( "...done with subtraction of base image...." )
                refmaskpath = self.expsource.get_file_from_archive( tmpstor.sub_filename( "remaprefmask" ) )

                imgmaskpath = self.expsource.local_path_from_basename( self.image.basename, filetype='mask' )
                with fits.open( imgmaskpath ) as imghdul, fits.open( refmaskpath ) as refhdul:
                    imgmaskdata = imghdul[0].data
                    refmaskdata = refhdul[0].data

                def _limits( x, y ):
                    x0 = int(x+0.5) - 3
                    x1 = int(x+0.5) + 3
                    y0 = int(y+0.5) - 3
                    y1 = int(y+0.5) + 3
                    x0 = x0 if x0 >= 0 else 0
                    x1 = x1 if x1 < imgmaskdata.shape[1] else imgmaskdata.shape[1]-1
                    y0 = y0 if y0 >= 0 else 0
                    y1 = y1 if y1 < imgmaskdata.shape[0] else imgmaskdata.shape[0]-1
                    return x0, x1, y0, y1

                for i in range(len(xs)):
                    x0, x1, y0, y1 = _limits( xs[i], ys[i] )
                    thismoved = False
                    while ( imgmaskdata[y0:y1,x0:x1].max() == 1 ) or ( refmaskdata[y0:y1,x0:x1].max() == 1 ):
                        # self.logger.info(f'x={xs[i]}, y={ys[i]}')
                        thismoved = True
                        xrnd, yrnd = self._uniformpos( 1, rng )
                        xs[i] = xrnd[0]
                        ys[i] = yrnd[0]
                        x0, x1, y0, y1 = _limits( xs[i], ys[i] )
                    if thismoved:
                        # self.logger.info("OMG Moved")
                        nmoved += 1

                self.logger.info( f"...done rerandomizing positions, had to reset {nmoved}/{len(xs)} objects." )

            if fluxdistrib == 'flat':
                fluxen = self._flatfluxen( n, minmag, maxmag, rng )
            else:
                raise NotImplementedError( "Only flat flux distribution is currently implemented." )

        self.logger.debug( f"About to inject psfs; xs.shape={xs.shape}, ys.shape={ys.shape}, "
                           f"fluxen.shape={fluxen.shape}" )
        self.inject_psfs( xs, ys, fluxen, noisy=noisy, curdb=curdb, rng=rng )

        return xs, ys, fluxen

    # ======================================================================

    def write_and_save_image( self ):
        """Create an image named fake_* in the database and on the filesystem using imagedata.

        Return the db.Image object.

        """
        if self.fakeimagedata is None:
            raise RuntimeError( "You haven't injected any fakes yet; fakeimagedata=None" )
        if self.fakeimgobj is not None:
            raise RuntimeError( "You've already saved the fakeimageobj" )

        newbase = f'fake_{self.image.basename}'
        existingfakeimage = db.Image.get_by_basename( newbase )
        if existingfakeimage is not None:
            raise RuntimeError( f"There's already a {newbase} in the database." )
        self.fakepath = self.imagepath.parent / f"{newbase}.fits"
        self.fakeweightpath = self.imagepath.parent / f"{newbase}.weight.fits"

        fakehdr = self.imageheader.copy()
        fakehdr['COMMENT'] = "Fakes added by curveball"
        fits.writeto( self.fakepath, self.fakeimagedata, fakehdr )
        fakeweighthdr = self.weightheader.copy()
        fakehdr['COMMENT'] = "Fakes added by curveball"
        fits.writeto( self.fakeweightpath, self.fakeweightdata, fakeweighthdr )

        with fits.open( self.expsource.local_path_from_basename( self.image.basename, filetype='mask' ) ) as hdul:
            fits.writeto( self.fakepath.parent / f"{newbase}.mask.fits", hdul[0].data, hdul[0].header )
        # ROB THINK ABOUT THIS -- the catalog won't have the fakes in it!
        # I *think* this is what I want, because I don't want later code
        # using the fakes for things like matching to other images
        shutil.copy2( self.catpath, self.fakepath.parent / f"{newbase}.cat" )
        shutil.copy2( self.psfpath, self.fakepath.parent / f"{newbase}.psf" )

        with db.DB.get() as curdb:
            self.fakeimgobj = self.expsource.create_or_load_image( self.fakepath, hasfakes=True, curdb=curdb )
            self.fakeimgobj.isastrom = self.image.isastrom
            self.fakeimgobj.isphotom = self.image.isphotom
            self.fakeimgobj.hascat = self.image.hascat
            curdb.db.commit()

            wcs = db.WCS.get_for_image( self.image, curdb=curdb )
            fakewcs = db.WCS( image_id=self.fakeimgobj.id, header=wcs.header )
            curdb.db.add( fakewcs )
            curdb.db.commit()

    # ======================================================================

    def subtract( self ):
        """Subtract the reference from the faked image.

        Must call write_and_save_image() before calling this.

        The subtact.Subtractor object goes into the stor property of the
        FakeInjector object, and the db.Subtraction object goes into the
        subobj property.

        ...it's too bad that hotpants (at least) doesn't allow us to
        separate out the step of psf matching the ref to the new, as
        that's going to be exactly the same every time.  We could cache
        that result and make repeated subtractions faster.  (Or does it?  I should look into this.)

        """
        self.stor = subtract.Subtractor.get( self.subalg, self.expsource, self.fakeimgobj, self.reference,
                                        logger=self.logger )
        self.subobj = self.stor.subtract( tagdefault=False, tagdefaultifnodefault=False, taglatest=False,
                                          noarchive=True )

    # ======================================================================

    def measurefakes( self, photalg, recenter=True ):
        """Measure the injected fakes.

        photalg — The photometry algorithm to use (e.g. 'apphot1')

        Must call subtract() before running this.

        Loads up the following fields in self:
          fakemeasuredflux : float or numpy.array[...]
          fakemeasuredfluxerr : float or numpy.array[...]
          fakemeasuredx : float or numpy.array[...]
          fakemeasuredy : float or numpy.array[...]
          fakemeasuredcovar : numpy.array[3,3] or numpy.array[...,3,3]

        """

        if self.subobj is None:
            raise RuntimeError( "Subtraction hasn't been run yet." )

        subpath = self.stor.sub_filepath( "sub" )
        subweightpath = self.stor.sub_filepath( "subweight" )
        submaskpath = self.stor.sub_filepath( "submask" )

        with fits.open( subpath, memmap=False ) as hdul:
            subdata = hdul[0].data
        with fits.open( subweightpath, memmap=False ) as hdul:
            subweightdata = hdul[0].data
        with fits.open( submaskpath, memmap=False ) as hdul:
            submaskdata = hdul[0].data

        photor = photometry.Photomotor.get( photalg, self.expsource, self.fakeimgobj, logger=self.logger )
        phot, x, y, covar, info = photor.measure( self.fakex, self.fakey, recenter=recenter,
                                                  imagedata=subdata, weightdata=subweightdata, maskdata=submaskdata )

        if isinstance( phot, numbers.Number ):
            self.fakemeasuredflux = numpy.array( [ phot ] )
            self.fakemeasuredfluxerr = numpy.array( [ math.sqrt(covar[0,0]) ] )
            self.fakemeasuredx = numpy.array( [ x ] )
            self.fakemeasuredy = numpy.array( [ y ] )
            self.fakemeasuredcovar = covar
        else:
            self.fakemeasuredflux = phot
            self.fakemeasuredfluxerr = numpy.sqrt( covar[ ..., 0, 0 ] )
            self.fakemeasuredx = x
            self.fakemeasuredy = y
            self.fakemeasuredcovar = covar

    # ======================================================================

    def cleanup( self, curdb=None ):
        """Clean up temporary files and database entries.

        curdb (optional) — a db.DB object

        A number of fake_* files will have been created on the
        filesystem (and, for some of them, on the archive).
        Additionally, there will be an entry in the databse for
        db.Image, db.WCS, and db.Subtraction.  This method gets rid of
        all of them.

        """
        with db.DB.get(curdb) as curdb:
            tonuke = []
            if self.subobj is not None:
                # Do this to not worry about SA session connection
                actualsubobj = db.Subtraction.get( self.subobj.id, curdb=curdb )
                curdb.db.delete( actualsubobj )
                tonuke.extend( self.stor.all_archived_filenames() )
            if self.fakeimgobj is not None:
                actualfakeimgobj = db.Image.get( self.fakeimgobj.id, curdb=curdb )
                curdb.db.delete( actualfakeimgobj )
                tonuke.extend( self.expsource.all_archived_filenames( self.fakeimgobj.basename ) )
            curdb.db.commit()
        self.subobj = None
        self.fakeimgobj = None
        self.fakex = None
        self.fakey = None
        self.fakeflux = None

        for f in tonuke:
            p = self.expsource.local_path( f, logger=self.logger )
            if p is not None:
                p.unlink( missing_ok=True )
            try:
                self.expsource.delete_file_on_archive( f )
            except Exception as ex:
                self.logger.error( r"Failed to delete file on archive ¯\_(ツ)_/¯" )

    # ======================================================================

    def repeatedtrials( self, trials, x=None, y=None, flux=None, npsfs=1,
                        photalg='apphot1', curdb=None, outfile=None,
                        dontcleanuplast=False, **kwargs ):
        """Inject multiple PSFs multiple times and return statistics

        trials — number of tials to do.  Each trial, each PSF is
          injected with a different realization; same flux, but differnet
          random shot noise in the added pixel values.

        x, y — positions of the injected psfs.  If None, will randomly generate the positions using scatter_psfs().

        flux — fluxes of injected psfs.  If None, will randomly generate the fluxes using scatter_psfs().

        npsfs — Number of psfs to scatter about the image.  Ignored if
          x, y, and flux are non-None

        photalg — the name of the photometry algorithm to pass to measure() (e.g. 'apphot1', the default)

        curdb (optional) — a db.DB object
        
        outfile (optional) - a file name in which to save the resulting data array

        dontcleanuplast — if True, don't delete the last fake_* image from the database

        ADDITIONAL KEYWORDS : any additional keywords are passed to
          scatter_psfs(); see documentation on that method for the
          parameters necessary to specify how psf positions and fluxes
          are chosen when they aren't explicitly passed.

        Will inject all opf the psfs trials times, measure them, and
        collect statistics.  Returns a Pandas dataframe with columns:

            x: x positions (photutils coords) of the injected psfs

            y: y positions (photutils coords) of the injected psfs

            flux: the true (pre-shot-noise) flux of the injected psfs

            meanflux: the average of measurements of each psf over all trials

            stdflux: the standard deviation of flux of each psf over all trials

            fluxbar: a (attempt at a) variance-weighted mean flux; the weighting
              is done by only the error from the photon shot noise of the injected
              psfs, not the pre-existing noise in the image, because the latter is
              100% correlated between all trials.  See warning below.

            dfluxbar : uncertainty on fluxbar.  Includes the uncertainty
              on the variance weigthed mean, added in quadrature to the
              flux error from the pre-injection image (i.e. the part
              that's correlated between all trials).

            avgdflux : the mean of the flux errors returned by the
               photometry method for each psf, averaged over all trials

            bkvar : The part of dfluxbar that comes from the pre-injection image.

            meanx: the average of measurements of x positions over all trails
              NOTE : meanings of positions and errors is a bit fraught.
              Especially for dimmer sources, remember that the
              background noise pattern is the same for every injection.
              As such, anything in the background that randmoly moves
              either the centroid, or the center of the PSF fit, will be
              correlated between all trials.

            meany: the average of measurements of y positions over all trials

            meanxerr: undertainty in meanx (which is the mean x uncertainty divided by sqrt(ntrials)

            meanerr :   "         "  meany  " ...

            stdx: the standard deviation of x positions over all trials

            stdy: the standard deviation of x positions over all trials

        NOTE : cacluation of uncertainties and weightings and such make
        the assumption that the ref image is much deeper than the new
        image, so the noise in the ref image can be ignored in
        comparison to the noise in the new image.

        """

        if ( x is None ) or ( y is None ) or ( flux is None ):
            if not ( ( y is None ) and ( flux is None ) and ( flux is None ) ):
                raise ValueError( "If any of x, y, and flux are None, all must be None" )
        else:
            x = numpy.atleast_1d( x )
            y = numpy.atleast_1d( y )
            flux = numpy.atleast_1d( flux )
            if ( x.shape != y.shape ):
                raise ValueError( "Shapes of x and y must match." )
            if ( x.shape != flux.shape ):
                if ( flux.shape == (1,) ):
                    flux = numpy.full_like( x, flux[0] )
                else:
                    raise ValueError( "Flux must either be a single value, or must match shape of x and y" )

        measuredfluxen = []
        measureddfluxen = []
        measuredfluxen2 = []
        measuredx = []
        measureddx = []
        measuredx2 = []
        measuredy = []
        measureddy = []
        measuredy2 = []

        bkvar = None
        for n in range(trials):
            try:
                self.logger.info( f"Trial {n}: injecting psfs" )
                x, y, flux = self.scatter_psfs( npsfs, x, y, flux, curdb=curdb, **kwargs )
                self.logger.info( f"Trial {n}: writing image" )
                self.write_and_save_image()
                self.logger.info( f"Trial {n}: subtracting" )
                self.subtract()
                self.logger.info( f"Trial {n}: measuring" )
                self.measurefakes( photalg )
                measuredfluxen.append( self.fakemeasuredflux )
                measureddfluxen.append( self.fakemeasuredfluxerr )
                measuredfluxen2.append( self.fakemeasuredflux**2 )
                measuredx.append( self.fakemeasuredx )
                measuredx2.append( self.fakemeasuredx ** 2 )
                measureddx.append( numpy.sqrt( self.fakemeasuredcovar[ ..., 1, 1 ] ) )
                measuredy.append( self.fakemeasuredy )
                measuredy2.append( self.fakemeasuredy ** 2 )
                measureddy.append( numpy.sqrt( self.fakemeasuredcovar[ ..., 2, 2 ] ) )


                # The uncertainties that will be in the measured fluxes
                # above will include a contribution from the shot noise in
                # the injected fakes, and a contribution from the noise in
                # the underlying image.  Because for a given position, the
                # underlying image is the same every time, the noise from
                # the underlying image is 100% correlated between the
                # trials.  As such, when we do statistics later like χ² or
                # residuals, we need to take this correlated noise into
                # account.  To do that, we need to measure the noise on the
                # underlying image.  We only have to do this once, of course.
                # However, the procedure for doing that depends on the photometry
                # method.

                if bkvar is None:
                    imwpath = self.expsource.local_path_from_basename( self.image.basename, "weight" )
                    with fits.open( imwpath, memmap="false" ) as hdul:
                        imvar = 1. / hdul[0].data
                        # For our purposes here, we want to ignore masked pixels,
                        # and if all is well (yeah, right), weight <= 0. and masked
                        # are the same thing.
                        imvar[ hdul[0].data <= 0. ] = 0.
                    photor = photometry.Photomotor.get( photalg, self.expsource, self.fakeimgobj, logger=self.logger )

                    if photor.isap:
                        # For aperture photometry, we can stick 1/weight into the
                        # photometry code and the "flux" we get back will be the variance
                        # in the background.
                        imones = numpy.full_like( imvar, 1. )
                        imzeros = numpy.full_like( imvar, 0, dtype=numpy.int16 )
                        phot, newx, newy, covar, info = photor.measure( x, y, imagedata=imvar, recenter=False,
                                                                        weightdata=imones, maskdata=imzeros )
                        bkvar = phot
                        imvar = None
                        imzeros = None
                        imones = None
                    elif photor.ispsf:
                        # For PSF photometry, it's not clear that this is the right thing to do,
                        # but let's go with it and hope that it's close enough.  We're going to
                        # get a normalized PSF, use that as the weights for a weighted sum of
                        # the pixels in 1/(the original weight image), and call that the background
                        # variance.
                        bkvar = numpy.empty( x.size )
                        for i in range(len(x)):
                            ix = int( math.floor( x[i] + 0.5 ) )
                            iy = int( math.floor( y[i] + 0.5 ) )
                            clip = self.psfex.getclip( x[i], y[i], 1., norm=True )
                            # All this (literal) edge case business... blah
                            clipx0 = 0
                            clipx1 = clip.shape[1]
                            clipy0 = 0
                            clipy1 = clip.shape[0]
                            imgx0 = ix - clip.shape[1] // 2
                            imgx1 = ix + clip.shape[1] // 2 + 1
                            imgy0 = iy - clip.shape[0] // 2
                            imgy1 = iy + clip.shape[0] // 2 + 1
                            if imgx0 < 0:
                                clipx0 -= imgx0
                                imgx0 = 0
                            if imgx1 > imvar.shape[1]:
                                clipx1 -= ( imgx1 - imvar.shape[1] )
                                imgx1 = imvar.shape[1]
                            if imgy0 < 0:
                                clipy0 -= imgy0
                                imgy0 = 0
                            if imgy1 > imvar.shape[0]:
                                clipy1 -= ( imgy1 - imvar.shape[0] )
                                imgy1 = imvar.shape[0]
                            bkvar[i] = ( imvar[ imgy0:imgy1 , imgx0:imgx1 ] *
                                         clip[ clipy0:clipy1, clipx0:clipx1 ] ).sum()

                    else:
                        raise RuntimeError( f"Photometry method {photor.dbname} is neither "
                                            f"aperture nor psf; I don't know how to cope." )

                    self.logger.info( f"********* Finished {n+1} of {trials} trials ************" )
            finally:
                if ( n < trials-1 ) or ( not dontcleanuplast ):
                    self.cleanup()

        measuredfluxen = numpy.array( measuredfluxen )
        measureddfluxen = numpy.array( measureddfluxen )
        measuredfluxen2 = numpy.array( measuredfluxen2 )
        measuredx = numpy.array( measuredx )
        measuredx2 = numpy.array( measuredx2 )
        measureddx = numpy.array( measureddx )
        measuredy = numpy.array( measuredy )
        measuredy2 = numpy.array( measuredy2 )
        measureddy = numpy.array( measureddy )

        meanflux = measuredfluxen.mean( axis=0 )
        stdflux = numpy.sqrt( measuredfluxen2.mean( axis=0 ) - meanflux**2 )
        avgdflux = measureddfluxen.mean( axis=0 )
        meanx = measuredx.mean( axis=0 )
        stdx = numpy.sqrt( measuredx2.mean( axis=0 ) - meanx**2 )
        dx = measureddx.mean( axis=0 ) / math.sqrt( trials )
        meany = measuredy.mean( axis=0 )
        stdy = numpy.sqrt( measuredy2.mean( axis=0 ) - meany**2 )
        dy = measureddy.mean( axis=0 ) / math.sqrt( trials )


        # Try to take into account the fact that the background is the same
        # in all trials, and thus is a correlated error.
        # ASSUME that the background noise is dominated by the image
        # (We could measure the remap ref, but it will have correlated
        # pixels, and we HOPE that it's lots deeper than the new image.)
        measureddfluxen = numpy.sqrt( measureddfluxen**2 - bkvar )

        # Variance weight by just the shot noise in the object, not the background,
        # since the background is correlated with everything.  Not sure this
        # is the right weighting to do, or that I've done it right.
        fluxbar = ( ( measuredfluxen / measureddfluxen**2 ).sum( axis=0 )
                    /  ( 1 / measureddfluxen**2 ).sum( axis=0 ) )
        dfluxbar = numpy.sqrt( 1. / ( 1 / measureddfluxen**2 ).sum( axis=0 ) + bkvar )

        df = pandas.DataFrame( { 'x': x, 'y': y, 'flux': flux,
                                 'meanflux': meanflux, 'stdflux': stdflux,
                                 'fluxbar': fluxbar, 'dfluxbar': dfluxbar,
                                 'avgdflux': avgdflux, 'bkvar': bkvar,
                                 'meanx': meanx, 'meany': meany,
                                 'meanxerr': dx, 'meanyerr': dy,
                                 'stdx': stdx, 'stdy': stdy } )
        return df

    # ======================================================================

    @classmethod
    def repeatedtrialsstats( cls, df ):
        """Return some statistics on the result of a call to repeatedtrials

        df : the Dataframe returned by repeatedtrials()

        returns : ngood, chisq, chisqmargin, meanreducedresid, meanfractionalresid

        ngood : the number of non-NaN flux measurements

        chisq : sum( ( ( fluxbar - injected flux ) / dfluxbar )^2 ) / ngood
           --> This is a reduced chisquare where the model is "all measured fluxes
               are the injected flux" (so it is a 0-parameter model).  It uses
               fluxbar, which is the non-correlated-variance-weighted average
               of the fluxes over all trials, and dfluxbar which is hopefully
               the rightr uncertainty on fluxbar given the correlated errors

        chisqmargin : CDF of the χ² distribution up to ngood*chisq for
          ngood degrees of freedom.  Want this to be around 0.5.  If
          it's really high, then the chisquare is too high and the fit
          is bad.  If it's really low, then the chisq is too small and
          errors are overestimated, or the model is overfitting or
          something.

        meanreducedresid : The mean of the residual (fluxbar -
          injectedflux) divided by the uncertainty (dfluxbar).  This
          ought to be in the range ± 1/sqrt(ngood) (I think).

        meanfractionalresid : The mean of the residual divded by the
          injected flux.  Should be close to 0.  How close is a harder
          question, so meanreducedresid is probably more useful.  This
          does give you a sense as to the relative error in the
          measurement (relative to the value of the flux).
        """

        good = df[ ~numpy.isnan( df.fluxbar ) ]
        ngood = len(good)
        chisq = ( ( ( good['fluxbar'] - good['flux'] ) / good['dfluxbar'] )**2 ).sum() / ngood
        chisqmargin = scipy.stats.chi2.cdf( chisq*ngood, ngood )
        meanreducedresid = ( ( good['fluxbar'] - good['flux'] ) / good['dfluxbar'] ).mean()
        meanfractionalresid = ( ( good['fluxbar'] - good['flux'] ) / good['flux'] ).mean()

        # print(f"Good flux measurements: {ngood}\n",
        #       f"\u03C7\u00b2: {chisq}\n",
        #       r"\u03C7\u00b2 margin: {chisqmargin}\n",
        #       f"Mean reduced resid: {meanreducedresid}\n",
        #       f"Mean fractional resid: {meanfractionalresid}"
        #      )

        return ngood, chisq, chisqmargin, meanreducedresid, meanfractionalresid

    def printdf( self, df, posprecision=2, posfmt='f', fluxprecision=1, fluxfmt='f', outfile=sys.stdout ):
        origmaxrows = pandas.options.display.max_rows
        origmaxcols = pandas.options.display.max_columns
        origwidth = pandas.options.display.width
        pandas.options.display.max_rows = None
        pandas.options.display.max_columns = None
        pandas.options.display.width = 9999

        posfmt = f'{{:,.{posprecision}{posfmt}}}'
        fluxfmt = f'{{:,.{fluxprecision}{fluxfmt}}}'

        printdf = df.copy()
        printdf['x'] = printdf['x'].map( posfmt.format )
        printdf['y'] = printdf['y'].map( posfmt.format )
        printdf['flux'] = printdf['flux'].map( fluxfmt.format )
        printdf['meanflux'] = printdf['meanflux'].map( fluxfmt.format )
        printdf['stdflux'] = printdf['stdflux'].map( fluxfmt.format )
        printdf['fluxbar'] = printdf['fluxbar'].map( fluxfmt.format )
        printdf['resid'] = ( df['fluxbar'] - df['flux'] ).map( fluxfmt.format )
        printdf['reduced'] = ( ( df['fluxbar'] - df['flux'] ) / df['dfluxbar'] ).map( '{:,.2f}'.format )
        printdf['dfluxbar'] = printdf['dfluxbar'].map( fluxfmt.format )
        printdf['avgdflux'] = printdf['avgdflux'].map( fluxfmt.format )
        printdf['bkerr'] = numpy.sqrt( printdf['bkvar'] )
        printdf['bkerr'] = printdf['bkerr'].map( fluxfmt.format )
        printdf['meanx'] = printdf['meanx'].map( posfmt.format )
        printdf['dmeanx'] = printdf['meanxerr'].map( posfmt.format )
        printdf['sigx'] = printdf['stdx'].map( posfmt.format )
        printdf['meany'] = printdf['meany'].map( posfmt.format )
        printdf['dmeany'] = printdf['meanyerr'].map( posfmt.format )
        printdf['sigy'] = printdf['stdy'].map( posfmt.format )
        printdf = printdf[ [ 'x', 'y', 'flux', 'fluxbar', 'dfluxbar', 'resid', 'reduced',
                             'meanflux', 'stdflux', 'avgdflux', 'bkerr',
                             'meanx', 'dmeanx', 'sigx', 'meany', 'dmeany', 'sigy' ] ]
        outfile.write( f"{printdf}\n" )

        ngood, chisq, chisqmargin, meanreducedresid, meanfractionalresid = self.repeatedtrialsstats( df )
        outfile.write( f"\nInfo on residuals( throwing out NaNs):\n" )
        outfile.write( f"\nMean fractional residual ( (fluxbar-flux)/flux ): {meanfractionalresid:.3g}\n" )
        outfile.write( f"Mean reduced residual ( (fluxbar-flux)/dfluxbar ): {meanreducedresid:.3f}  "
                       f"(expect ± {1./math.sqrt(ngood):.3f})\n" )
        outfile.write( f"Median reduced residual ( (fluxbar-flux)/dfluxbar ): "
                       f"{numpy.median(( df['fluxbar'] - df['flux'] ) / df['dfluxbar'] ):.3f}\n" )
        outfile.write( f"χ²/n ( mean of ((fluxbar-flux)/dfluxbar)^2 ): {chisq:.2f}  "
                       f"(prob. of bigger: {1.-chisqmargin:.2f})\n" )

        pandas.options.display.max_rows = origmaxrows
        pandas.options.display.max_columns = origmaxcols
        pandas.options.display.width = origwidth


# ======================================================================

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    formatter = logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s', datefmt='%Y-%m-%D %H:%M:%S' )
    logout.setFormatter( formatter )
    logger.setLevel( logging.INFO )

    parser = argparse.ArgumentParser( 'injectfakes',
                                      description="Inject fakes on an image, run subtraction, measure",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )

    parser.add_argument( "image", help="Basename of the search image" )
    parser.add_argument( "ref", help="Basename of the ref image" )
    parser.add_argument( "-e", "--expsource", required=True, help="Name of exposure source" )
    parser.add_argument( "-x", default=None, type=float, help="X position for a single fake" )
    parser.add_argument( "-y", default=None, type=float, help="Y position for a single fake" )
    parser.add_argument( "-f", "--flux", default=None, type=float, help="Flux (in ADU) of psf to inject" )
    parser.add_argument( "-m", "--magnitude", default=None, type=float,
                         help="Magnitude of psf to inject.  Give at most one of --flux or --magnitude" )
    parser.add_argument( "-n", "--ntrials", default=1, type=int,
                         help="Number of times to place the fake (with different random noise each time)" )
    parser.add_argument( "--numpsfs", default=1, type=int,
                         help="Number of psfs to randomly scatter around the image." )
    parser.add_argument( "--m0", default=17, type=float,
                         help=( "Minimum magnitude of psfs to randomly scatter around the image. "
                                "Ignored if ntrials=1, or if --flux or --magnitude are given" ) )
    parser.add_argument( "--m1", default=22, type=float,
                         help=( "Maximum magnitude of psfs to randomly scatter around the image. "
                                "Ignored if ntrials=1, or if --flux or --magnitude are given" ) )
    parser.add_argument( "-s", "--subtraction-method", default='alard1', help="Subtraction method" )
    parser.add_argument( "-p", "--photometry-method", default='apphot1', help="Photometry method" )
    parser.add_argument( "--seed", default=None, type=int, help="Random seed (default: system entropy)" )
    parser.add_argument( "-d", "--dontcleanuplast", default=False, action='store_true',
                         help=( "Don't cleanup last created fake_* images from the database.  (By default, "
                                "removes all created fake images." ) )
    parser.add_argument( "-o", "--outfile", default=None,
                         help="Write the result table to this file in addition to stdout" )
    parser.add_argument( "-v", "--verbose", default=False, action='store_true', help="Show debug log messages" )

    args = parser.parse_args()

    if args.verbose:
        logger.setLevel( logging.DEBUG )

    with db.DB.get() as curdb:
        img = db.Image.get_by_basename( args.image, curdb=curdb )
        if img is None:
            raise RuntimeError( f"Failed to find {args.image} in the database." )
        zp = img.magzp

    if args.ntrials <=0 :
        raise ValueError( "ntrials must be a positive integer" )
    if args.ntrials == 1:
        if args.flux is None:
            if args.magnitude is not None:
                raise RuntimeError( "Specify exactly one of -f or -m for --ntrials=1" )
            flux = args.flux
        else:
            if args.magnitude is None:
                raise RuntimeError( "Specify exactly one of -f or -m for --ntrials=1" )
            flux = 10 ** ( 0.4 * ( zp - args.magnitude ) )
    else:
        if ( args.m0 is None ) or ( args.m1 is None ):
            raise RuntimeError( "Must give both --m0 and --m1 if ntrials>1" )

    rng = numpy.random.default_rng( args.seed ) if args.seed is not None else None

    logger.info( f"Making faker for {args.image} with ref {args.ref}" )
    faker = FakeInjector( args.expsource, img, args.ref, subalg=args.subtraction_method, logger=logger )
    df = faker.repeatedtrials( args.ntrials, npsfs=args.numpsfs, x=args.x, y=args.y, flux=args.flux,
                               photalg=args.photometry_method,
                               minmag=args.m0, maxmag=args.m1, fluxdistrib='flat', noisy=True, rng=rng,
                               outfile=args.outfile, dontcleanuplast=args.dontcleanuplast )
    print( f"\nSummary of results for {args.numpsfs} psfs and {args.ntrials} trials:" )
    faker.printdf( df )
    if args.outfile is not None:
        with open( args.outfile, "w" ) as ofp:
            ofp.write( f"\nSummary of results for {args.numpsfs} psfs and {args.ntrials} trials:\n" )
            faker.printdf( df, outfile=ofp )

    # I've moved this printing to printdf; not sure if that's what I really want
    # ngood, chisq, chisqmargin, meanreducedresid, meanfractionalresid = faker.repeatedtrialsstats( df )
    # print( f"\nInfo on residuals( throwing out NaNs):" )
    # print( f"\nMean fractional residual ( (fluxbar-flux)/flux ): {meanfractionalresid:.3g}" )
    # print( f"Mean reduced residual ( (fluxbar-flux)/dfluxbar ): {meanreducedresid:.3f}  "
    #        f"(expect ± {1./math.sqrt(ngood):.3f})" )
    # print( f"χ²/n ( mean of ((fluxbar-flux)/dfluxbar)^2): {chisq:.2f}  "
    #        f"(prob. of bigger: {1.-chisqmargin:.2f})" )

# ======================================================================

if __name__ == "__main__":
    main()
