#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import io
import logging
import math
import pathlib
import shutil
import subprocess
import uuid
import argparse

import numpy
import numpy.ma

import PIL.Image

import astropy.io
import astropy.coordinates
import astropy.units as units
from astropy.io import fits
from astropy.coordinates import SkyCoord
from astropy.wcs import WCS
from astropy.visualization import ZScaleInterval
from photutils.aperture import CircularAperture, aperture_photometry

import config
import util
import processing
from subtract import Subtractor
from exposuresource import ExposureSource

# This one is brobdingnagian

class Alard1(Subtractor):
    """Subtract (Alard/Lupton) a reference from a new."""

    _subalg_name = 'alard1'

    # ======================================================================
    
    def __init__( self, expsource, image, reference, datadir=None,
                  scamptimeout=300, swarptimeout=300, hotpantstimeout=600,
                  tmpdir=None, tiny=1e-10, obj=None,
                  logger=logging.getLogger("main"), curdb=None ):
        super().__init__( expsource, image, reference, datadir=datadir, tmpdir=tmpdir, tiny=tiny,
                          logger=logger, obj=obj, curdb=curdb )
        self.scamptimeout = scamptimeout
        self.swarptimeout = swarptimeout
        self.hotpantstimeout = hotpantstimeout

    # ======================================================================

    @property
    def refnorm( self ):
        with fits.open( self.sub_filepath( "sub" ) ) as hdu:
            return 1. / float( hdu[0].header['KSUM00'] )
    
    # ======================================================================

    def _run_subtraction( self ):
        remaprefconv = self._outdir_filename( "remapref.conv.fits" )
        self._tmpfiles.extend( [ remaprefconv ] )
        
        subnoise = self._outdir_filename( "sub.noise.fits" )
        self._tmpfiles.extend( [ subnoise ] )
        
        remaprefnoise = self._outdir_filename( "remapref.noise.fits" )
        imagenoise = self._outdir_filename( "noise.fits" )
        ssf = self._outdir_filename( "tmp.ssf" )
        self._tmpfiles.extend( [ remaprefnoise, imagenoise, ssf ] )
        
        
        # Figure out hotpants parameters.
        # This involves determining substamps from the catalog,
        # as well as figuring out convoltion kernels from the seeing.
        # Some other stuff too.

        hotgaussorders = [2, 0, 0]
        hotgaussfactors = [0.5, 1.0, 1.5]

        # Get some info from the image and reference headers
        with fits.open( self._imagepath ) as hdu:
            hdr = hdu[0].header
            if ( self.image.medsky is not None ) and ( self.image.skysig is not None ):
                il = self.image.medsky - 10.0 * self.image.skysig
            elif ( 'MEDSKY' in hdr ) and ( 'SKYSIG' in hdr ):
                il = hdr['MEDSKY'] - 10.0 * hdr['SKYSIG']
            else:
                raise RuntimeError( f"Can't find mesky and skysig, it's not in the database nor the image header "
                                    f"for {self.image.basename}" )
            iu = hdr[self.expsource.saturate_keyword]
            image_nx = hdr['NAXIS1']
            image_ny = hdr['NAXIS2']
        with fits.open( self._remapreffile ) as hdu:
            # Assuming ref is background subtracted.  Also,
            # with the keyword copies, assuming that the pixel
            # scale and normalization was not appreciably affected
            # by swarp.
            hdr = hdu[0].header
            tl = -10.0 * hdr['SKYSIG']
            tu = hdr[self.expsource.saturate_keyword]

        kernelrad = int( 2.5 * self.image.seeing / self.expsource.pixscale )
        rss = int( 3.75 * self.image.seeing / self.expsource.pixscale )

        trtlt = 2 * math.sqrt( 2 * math.log(2) )
        if self.image.seeing < self.refimg.seeing:
            self.logger.warning( f'New has better seeing {self.image.seeing:.2f} '
                                 f'than ref {self.refimg.seeing:.2f}! '
                                 f'Going to use higher orders so there can be sharpening scariness (brrr....)' )
            hotgaussorders = [6, 4, 2]
            hotgaussfactors = [0.5, 1., 2. ]
            nominalfwhm = trtlt * self.expsource.pixscale
        else:
            nominalfwhm = math.sqrt( self.image.seeing**2 - self.refimg.seeing**2 )
        self.logger.info( f'Nominal reference convolution kernel fwhm: {nominalfwhm: .3f}"' )

        # hotpants seems to have issues if the gaussian sigmas are too
        # small This could also be a case where the seeings are too
        # close.  I've seen cases where the ref was *slightly* lower
        # seeing than the new (as measured), but it was actually the new
        # that probably needed to be convolved.  Convolving the ref was
        # a disaster, until we used higher orders.  So, in this case,
        # use higher orders, and worry and fret a lot.
        nominalsigpix = nominalfwhm / trtlt / self.expsource.pixscale
        if nominalsigpix < 0.6:
            self.logger.warning( f'Seeings were close enough '
                                 f'(new={self.image.seeing:.2f}, ref={self.refimg.seeing:.2f}) '
                                 f'that nominalsigpix was < 0.6.  Going to use higher orders in case there needs '
                                 f'to be sharpening scariness (brrr...)' )
            nominalsigpix = 1.0
            hotgaussorders = [6, 4, 2]
            hotgaussfactors = [0.5, 1., 2]

        gaussparam = f'-ng {len(hotgaussorders)} '
        tmptxt = 'Convolution σ and orders: '
        for order, factor in zip( hotgaussorders, hotgaussfactors ):
            gaussparam += f'{order} {factor*nominalsigpix} '
            tmptxt += f'{factor*nominalsigpix:.3f} ({order}), '
        self.logger.info( tmptxt )

        # hotpants needs noise images not 1/σ² weight images, so do that:
        self.logger.info( f'Making noise images for hotpants' )
        for wt, msk, outfile in zip( [ self._weightpath, self._remaprefweight ],
                                     [ self._maskpath, self._remaprefmask ],
                                     [ imagenoise, remaprefnoise ] ):
            with fits.open( wt, memmap=False) as whdu, fits.open( msk, memmap=False ) as mhdu:
                whdu[0].data[ ( mhdu[0].data != 0 ) |
                              ( whdu[0].data <= 0 ) ] = self.tiny
                noisedata = numpy.sqrt( 1 / whdu[0].data )
                nhdu = fits.PrimaryHDU( header=whdu[0].header, data=noisedata )
                nhdu.writeto( outfile, overwrite=True )
                
        # Match stars on the new and the remapped ref
        self.logger.info( f'Selecting stars for hotpants ' )
        with fits.open( self._catpath, memmap=False ) as hdu:
            imagecat = hdu[2].data
        with fits.open( self._remaprefcat, memmap=False ) as hdu:
            refcat = hdu[2].data

        # Filter out to unsaturated stars on the search image
        #  (Don't do this on the ref, in case the swarping mucked
        #   up sextractor's star identification.)
        # import pdb; pdb.set_trace()
        origlen = len(imagecat)
        imagecat = imagecat[ ( imagecat['CLASS_STAR'] > 0.9 ) &
                             ( imagecat['IMAFLAGS_ISO'] == 0 ) ]
        self.logger.debug( f'{len(imagecat)} of {origlen} objects on the new are unsaturated stars' )
        origlen = len(refcat)
        refcat = refcat[ ( refcat['IMAFLAGS_ISO'] == 0 ) ]
        self.logger.debug( f'{len(refcat)} of {origlen} objects in the ref have IMAFLAGS_ISO=0' )

        imagecoords = SkyCoord( ra=imagecat['X_WORLD'], dec=imagecat['Y_WORLD'], unit=units.deg )
        refcoords = SkyCoord( ra=refcat['X_WORLD'], dec=refcat['Y_WORLD'], unit=units.deg )
        idx, sep2d, junk = astropy.coordinates.match_coordinates_sky( refcoords, imagecoords )
        w = numpy.where( sep2d < 1*units.arcsecond )[0]

        refcat = refcat[w]
        imagecat = imagecat[idx[w]]

        # What's the number of stars we want for hotpants?
        # Think about dividing the image into regions of size 256; we
        # want one star per region.
        nsx = image_nx // 256
        nsy = image_ny // 256
        nstars_ideal = nsx * nsy

        self.logger.debug( f'{len(imagecat)} stars matched between new and ref' )
        if len(imagecat) < nstars_ideal // 8:
            self.logger.error( f'{len(imagecat)} stars for PSF matching isn\'t enough.' )
            raise RuntimeError( f'Not enough stars for PSF matching.' ) # TODO: check how many stars are filtered out from imagecat compared to normal subtraction
        if len(imagecat) < nstars_ideal // 4:
            self.logger.warning( f'{len(imagecat)} stars for PSF matching is low, reducing kernel orders' )
            hotgaussorders = [1, 0]
            hotgaussfactors = [0.6, 1.2]
        if len(imagecat) > nstars_ideal * 2:
            dexes = numpy.argsort( imagecat['MAG_AUTO'] )
            dexes = dexes[ 0 : 2*nstars_ideal ]
            imagecat = imagecat[dexes]
            refcat = refcat[dexes]
        self.logger.info( f'Using {len(refcat)} stars for hotpants kernel determination' )

        # Write these positions to the substamp file
        with open( ssf, "w" ) as ofp:
            for x, y in zip( imagecat["X_IMAGE"], imagecat["Y_IMAGE"] ):
                ofp.write( f'{x} {y}\n' )
                
        self.logger.info( f'Running hotpants' )
        hotpants = ( f'hotpants -inim {self._imagepath} -hki -n i -c t -tmplim {self._remapreffile} '
                     f'-outim {self._subfile} -omi {self._submask} -oni {subnoise} '
                     f'-tl {tl} -tu {tu} -il {il} -iu {iu} '
                     f'-r {kernelrad} -rss {rss} -ssf {ssf} '
                     f'-tni {remaprefnoise} -ini {imagenoise} '
                     f'-imi {self._maskpath} -tmi {self._remaprefmask} '
                     f'-oci {remaprefconv} -v 0 '
                     f'-nrx 1 -nry 1 -ko 1 -bgo 1 {gaussparam}' )
        self.logger.debug( f'hotpants command: {hotpants}' )
        subpresult = subprocess.run( hotpants, shell=True, timeout=self.hotpantstimeout, check=True,
                                     capture_output=True )
        self.logger.debug( f'hotpants complete' )

        # Hotpants gave us a noise file, but we need a weight file

        with fits.open( subnoise, memmap=False ) as noi, fits.open( self._submask, memmap=False ) as msk:
            wt = numpy.empty_like( noi[0].data, dtype=numpy.float32 )
            wbad = ( noi[0].data <= 0 )
            wgood = ( ~ wbad )
            wt[ wgood ] = 1. / ( noi[0].data[ wgood ] ** 2 )
            wt[ wbad ] = 0.
            wt[ msk[0].data != 0 ] = 0.
            fits.writeto( self._subweight, wt, noi[0].header )
        
    
# ======================================================================
    
def main():
    raise RuntimeError( "Don't run this, run subtract.py with the right subalg argument" )
    
    
# ======================================================================

if __name__ == "__main__":
    main()
