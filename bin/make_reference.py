#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import math
import os
import time
import argparse
import logging
import pathlib

from astropy.io import fits

import util
import db
import config
import buildstack
import processing
import astrometry
from exposuresource import ExposureSource

def make_reference( expsource, band, imagelis=None, imagefilename=None,
                    secure_references_kwargs={},
                    stackname=None, obj=None, ra=None, dec=None, peakt=None,
                    datadir=None, outdir=None, workdir=None, searchlocal=False, bgsub=True, 
                    min_count=11, curdb=None, logger=logging.getLogger("main") ):
    """Builds a reference image for a supernova.

    expsource : the ExposureSource for the images going into the stack
                  (assume no stacks from different telescopes)
    imagelis : list image filenames to build into the stack ; USE WITH CAUTION.
       Images must already be in the database.
    band : a db.Band object, or the name of the band
    stackname : filename for stack.  If given, will be put in the "stack" subdirectory
                under outdir.  If not given, a name will be built from the first
                image in the list
    obj : Either a db.Object object, or a name to be looked up in the database, or None
          if you just want to give ra/dec/peakt
    ra : H:M:S or decimal degrees ; ignored if obj is not None
    dec : ±d:m:s or decimal degrees ; ignored if obj is not None
    peakt : MJD or JD; ignored if obj is not None.  Will exclude images between -30 and 365 days after this peak.
    band : Either a db.Band object, or the name of the band.
    datadir : Root data directory.  Default: first one on the list from the config file
    outdir : Root output data directory.  Default: datadir.
    workdir : swarp working area.  Default: makes a dir under /tmp
    searchlocal : Look only in the local database for template sources.
                  Default: goes out to the survey and pulls in images if they
                  aren't already in the database.
    bgsub : Do we need to background subtract the images before summing?
    min_count : at least this many images must be summed into a reference
    curdb : a db.DB object (optional)
    logger : a logging.Logger object
    
    Uses ExposureSource.secure_references to figure out which reference images it needs.

    Builds the reference and loads it into the database.  Pushes the
    .fits, .weight.fits, .mask.fits, .cat, .psf, and .psf.xml files up
    to the NERSC archive.

    Returns ( stackobj, objrefobj ) where stackobj is the db.Image
    object for the stac, and objrefobj is the db.ObjectREference object
    created

    """
    stackbobj = None
    objrefobj = None
    ra = None           # Python scoping is sensible, but then the fact that it lets
    dec = None          #   you not declare variables means that there are traps built in
    with db.DB.get(curdb) as curdb:

        if not isinstance( band, db.Band ):
            bandobj = db.Band.get_by_name( band, expsource.id, curdb=curdb )
            if bandobj is None:
                raise ValueError( f'Unknown band {band}' )
            band = bandobj

        if obj is None:
            raise NotImplementedError( 'make_reference currently only works if you give it an object' )
            if ( ra is None ) or ( dec is None ) or ( peakt is None ):
                raise ValueError( 'Must specify either obj, or ( ra and dec and peakt )' )
            if not ( ( type(ra) == int ) or ( type(ra) == float ) ):
                ra = util.hmstodeg( ra )
            if not ( ( type(dec) == int ) or ( type(dec) == float ) ):
                dec = util.dmstodeg( dec )
            if not ( ( type(peakt) == int ) or ( type(peakt) == float ) ):
                raise ValueError( 'peakt must be a number' )
        else:
            if not isinstance( obj, db.Object ):
                objobj = db.Object.get_by_name( obj, curdb=curdb )
                if objobj is None:
                    raise ValueError( f'Unknown object {obj}' )
                obj = objobj
            ra = obj.ra
            dec = obj.dec
            peakt = obj.t0
            if peakt is None:
                raise RuntimeError( f'Can\'t build reference for object {obj.name}, t0 is unknown.' )

        # Check to see if a reference already exists

        objrefs = db.ObjectReference.get_for_obj_and_band( obj, band, curdb=curdb )
        if len(objrefs) > 0:
            strs = [ obr.image.basename for obr in objrefs ]
            logger.info( f'References already exist for {obj.name} in band {band.name}:\n' + "\n".join(strs) )
            return ( None, objrefs[0] )
            
        # Make sure peakt is MJD
        if peakt > 2400000:
            peakt -= 2400000.5

        if imagelis is not None:
            refs = imagelis
            for ref in refs:
                expsource.ensure_sourcefiles_local( ref )
            
        elif imagefilename is not None:
            with open( imagefilename ) as ifp:
                refs = ifp.read()
                refs = refs.split('\n')
                refs = [ i.strip() for i in refs ]
                for ref in refs:
                    expsource.ensure_sourcefiles_local(ref)
                # TODO : handle blank lines better

        else:
            refdict = expsource.secure_references( ra, dec, peakt, band=band, searchlocal=searchlocal,
                                                   min_count=min_count, curdb=curdb, logger=logger,
                                                   **secure_references_kwargs )
            if not band.filtercode in refdict:
                raise RuntimeError( f'No references found for {band.name}' )
            refs = refdict[band.filtercode]

        logger.info( f'Found {len(refs)} references.' )
        
        if datadir is None:
            datadir = pathlib.Path( config.Config.get().value("datadirs")[0] )
        if outdir is None:
            outdir = datadir

        # ROB TODO
        # Check stacks that exist and see if there already exists one with all these refs in it

        refimgs = [ expsource.local_path_from_basename( ref ) for ref in refs ]
        refmasks = [ expsource.local_path_from_basename( ref, filetype='mask' ) for ref in refs ]
        refweights = [ expsource.local_path_from_basename( ref ) for ref in refs ]
        refcats = []
    
        # Make sure we have catalogs and seeings and limiting magnitudes and zeropoints for everything

        refimgobjs = []
        for i in range(len(refs)):
            ref = refs[i]
            imgobj = db.Image.get_by_basename( ref, curdb=curdb )
            if imgobj is None:
                raise Exception( f"Didn't find {ref} in the database; this shouldn't happen." )
            refimgobjs.append( imgobj )
            mustcat = False
            catpath = datadir / expsource.relpath( f'{ref}.cat' )
            if ( not catpath.is_file() ) or ( not imgobj.isastrom ):
                logger.info( f'Must build catalog {catpath}' )
                mustcat = True
                if catpath.is_file():
                    catpath.unlink()

            if mustcat:
                logger.info( f'Making catalog, measuring sky and psf for {ref}' )
                seeing, skysig, skymed = processing.cat_psf_sky( expsource, refimgs[i], refweights[i],
                                                                 refmasks[i], logger=logger, curdb=curdb )
                logger.info( f'r{ref} has seeing {seeing:.2f}, skysig {skysig:.1f}, skymed {skymed:.1f}' )
                imgobj.skysig = skysig
                imgobj.seeing = seeing
                curdb.db.commit()
            refcats.append( catpath )

            if not imgobj.isastrom:
                logger.info( f'Doing astrometry for {ref}' )
                astrometry.astrom_calib( refimgs[i], catpath, expsource=expsource,
                                         savedb=True, logger=logger, curdb=curdb )
                imgobj.isastrom = True
                curdb.db.commit()

            if not imgobj.isphotom:
                logger.info( f'Doing photometry for {ref}' )
                imgobj.magzp = expsource.photocalib( refimgs[i], catpath )
                # For limiting magnitude, assume sky limited in an aperture
                #  whose radius is seeing.  (This is conservative.)
                # Do a 5σ limits
                rad = imgobj.seeing / expsource.pixscale
                flux = 5 * math.sqrt(math.pi) * rad * imgobj.skysig
                imgobj.limiting_mag = -2.5*math.log10(flux) + imgobj.magzp
                imgobj.isphotom = True
                curdb.db.commit()

        # Determine image size
        with fits.open( refimgs[0], memmap=False ) as hdu:
            nx = hdu[0].header['NAXIS1']
            ny = hdu[0].header['NAXIS2']

        if stackname is not None:
            stackname = pathlib.Path( stackname )
            outstack = outdir / "stack" / stackname
        else:
            outstack = outdir / expsource.make_stackpath_from_image_name( refimgs[0] )
        logger.info( f'Building stack {outstack}' )
        buildstack.buildstack( expsource, outstack, refimgs, refweights, refmasks, refcats,
                               trim=(nx,ny), center='MOST', bgsub=bgsub, logger=logger )

        # Get a catalog, measure psf, measure sky noise

        logger.info( f'Extracting catalog, measuring PSF and sky noise' )
        stackdir = outstack.parent
        stackbase = expsource.image_basename( outstack.name )
        outweight = stackdir / f'{stackbase}.weight.fits'
        outmask = stackdir / f'{stackbase}.mask.fits'
        outcat = stackdir / f'{stackbase}.cat'
        outpsf = stackdir / f'{stackbase}.psf'
        outpsfxml = stackdir / f'{stackbase}.psf.xml'
        
        # Load stack into database
        stackobj = expsource.create_or_load_stack( outstack, [ r.basename for r in refimgobjs ],
                                                   band=band, isskysub=True, isastrom=False, 
                                                   isphotom=False,
                                                   curdb=curdb, logger=logger )

        # Make a catalog
        seeing, skysig, medsky = processing.cat_psf_sky( expsource, outstack, outweight, outmask,
                                                         logger=logger, curdb=curdb,
                                                         extractargs={ 'bgsub': False } )

        # Astrometrically and photometrically calibrate
        # TODO: One of the previous functions is meant to make a .cat file?
        logger.info( f'Astrometric and photometric calibration' )
        astrometry.astrom_calib( outstack, outcat, expsource=expsource, savedb=True, curdb=curdb, logger=logger )

        magzp = expsource.photocalib( outstack, outcat )
        # Get the limiting magnitude from the header
        relstackpath = expsource.get_path_for_stack( stackobj.basename )
        stackpath = expsource.local_path( relstackpath.name )
        logger.debug( f"Reading limiting magnitude from header of {stackpath}" )
        with fits.open( stackpath ) as hdul:
            lmt_mg = float( hdul[0].header[ 'LMT_MG' ] )

        # Refresh the stack object from the database before updating
        # I'm never really sure with SQLAlchemy when this kind of thing is necessary
        curdb.db.refresh( stackobj )
        stackobj.magzp = magzp
        stackobj.isphotom = True
        stackobj.limiting_mag = lmt_mg
        curdb.db.commit()

        # Tag this as an object reference if object was given
        
        objrefobj = db.ObjectReference( object_id=obj.id if obj is not None else None,
                                        ra=ra, dec=dec, image_id=stackobj.id )
        curdb.db.add( objrefobj )
        curdb.db.commit()

        # Will I regret keeping the database open this long?

    # Push stack and associated files up to archive
        
    expsource.upload_file_to_archive( outstack.name, overwrite=True )
    expsource.upload_file_to_archive( outweight.name, overwrite=True )
    expsource.upload_file_to_archive( outmask.name, overwrite=True )
    expsource.upload_file_to_archive( outcat.name, overwrite=True )
    expsource.upload_file_to_archive( outpsf.name, overwrite=True )
    expsource.upload_file_to_archive( outpsfxml.name, overwrite=True )

    return ( stackobj, objrefobj )

# ======================================================================

def main():
    # ROB TODO : add the ability to add a new reference if one already exists,
    #  or clobber the existing reference.
    
    argparser = argparse.ArgumentParser( description="Build a reference stack and load into DB" )
    argparser.add_argument( "band", help="Name of filter (e.g. ZTF_g, ZTF_r, or ZTF_i)" )
    argparser.add_argument( "-e", "--expsource", default="ZTF",
                            help="Name of exposure source (default ZTF)" )
    argparser.add_argument( "-o", "--object", default=None, help="Name of database object to build reference for" )
    argparser.add_argument( "-r", "--ra", type=float, default=None,
                            help="RA decimal degrees (ignored if -o specified)" )
    argparser.add_argument( "-d", "--dec", type=float, default=None,
                            help="Dec decimal degrees (ignored if -o specified)" )
    argparser.add_argument( "-t", "--peakt", type=float, default=None,
                            help="Peak time (MJD or JD) (ignored if -o specified)" )
    argparser.add_argument( "-l", "--local", default=False, action='store_true',
                            help="Search only local database (default: query exposure source)" )
    argparser.add_argument( "-f", "--image-filename", default=None,
                            help=( "Text file with one-per-line names of images to use; if specified, "
                                   "then does not do a database search" ) )
    argparser.add_argument( "-m", "--min-count", default=11, type=int,
                            help="Must have at least this many images coadded into refernce (default: 11)" )
    argparser.add_argument( "-k", "--secure-references-kwargs", default=None,
                            help="Additional arguments to specify to secure_references; put in quotes" )
    argparser.add_argument( "--name", default=None,
                            help="Name of reference image" )
    argparser.add_argument( "-n", "--nobgsub", default=False, action='store_true',
                            help="Do NOT background-subtract images before summing (inside swarp)" )
    argparser.add_argument( "-v", "--verbose", default=False, action='store_true', help="So debug logs" );
    args = argparser.parse_args()

    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.DEBUG if args.verbose else logging.INFO )

    config.Config.init( None, logger=logger )

    expsource = ExposureSource.get( args.expsource )

    kwargs = {}
    if args.secure_references_kwargs is not None:
        srargs = args.secure_references_kwargs.split()
        i = 0
        while i < len(srargs):
            kwargs[ srargs[i] ] = srargs[i+1]
            i += 2
    
    make_reference( expsource, args.band, imagefilename=args.image_filename,
                    secure_references_kwargs=kwargs,
                    stackname=args.name, obj=args.object,
                    ra=args.ra, dec=args.dec, peakt=args.peakt,
                    searchlocal=args.local, bgsub=not args.nobgsub,
                    min_count=args.min_count, logger=logger )

    
# ======================================================================

if __name__ == "__main__":
    main()
