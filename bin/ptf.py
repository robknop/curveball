import sys
import os
import io
import re
import math
import requests
import pathlib
import urllib.parse
import numpy
import pandas
import db
import psycopg2
import psycopg2.extras
from exposuresource import *
from irsasurvey import *
import util
import logging
from util import cls_init
from astropy.io import fits

# WARNING -- will not work anywhere other than NERSC due to connection to nerscdb03.nersc.gov
#  in further_process_blob()

# See https://irsa.ipac.caltech.edu/ibe/docs/ptf/images/level1/

class PTF(IRSASurvey):
    NAME = "PTF"
    SECONDARY = None

    _irsa_query_columns = ( 'obsmjd,moonillf,ccdid,ra1,dec1,ra2,dec2,ra3,dec3,ra4,dec4,'
                            'seeing,obsdate,airmass,exptime,filter,'
                            'pfilename,afilename1,afilename2,afilename3,afilename4' )
    _searchurlbase = 'https://irsa.ipac.caltech.edu/ibe/search/ptf/images/level1'
    _dataurlbase   = 'https://irsa.ipac.caltech.edu/ibe/data/ptf/images/level1'
    # Images come from PTF as: {dir}/PTF_{yyyymmdd}{dayfrac:04d}_i_p_scie_t{hhmmss}_u{dbpiid}_f{filterid:02d}_p{ptffield}_c{ccd:02d}.fits
    # where masks replace "scie" with "mask".  There don't seem to be weight images.
    #
    # db = "database processed image id", and I bet it's something I can ignore
    #
    # I rewire them to
    # ptf_{yymmdd}_{hhmmss}_{fc}.{ccd:02d}.fits
    # with weights and masks as ptf_....{ccd:02d}.weight.fits etc.
    #
    # To store the other information (needed for regetting a file), I stick
    # (last two subdirs of dir),dayfrac,dbpiid,filterid,ptffield into the ARCH_ID
    # header field, and into the archive_identifier field of the database
    _generalparse =      re.compile( r'^(?P<basename>(?P<stack>stack_)?ptf_(?P<yyyymmdd>[0-9]{8})_'
                                     r'(?P<hhmmss>[0-9]{6})_(?P<filtercode>[^_\.]+))' )
    _exposureparse =     re.compile( r'^(?P<basename>(?P<stack>stack_)?ptf_(?P<yyyymmdd>[0-9]{8})_'
                                     r'(?P<hhmmss>[0-9]{6})_(?P<filtercode>[^_\.]+))\.fits(\.[fg]z)?' )
    _imageparse =        re.compile( r'^(?P<basename>(?P<stack>stack_)?ptf_(?P<yyyymmdd>[0-9]{8})_'
                                     r'(?P<hhmmss>[0-9]{6})_(?P<filtercode>[^_\.]+))\.'
                                     r'(?P<chiptag>[0-9]+)\.fits(\.[fg]z)?' )
    _exposuretypeparse = re.compile( r'^(?P<basename>(?P<stack>stack_)?ptf_(?P<yyyymmdd>[0-9]{8})_'
                                     r'(?P<hhmmss>[0-9]{6})_(?P<filtercode>[^_\.]+))\.'
                                     r'(?P<filetype>[^\.]+)\.fits(\.[fg]z)?' )
    _imagetypeparse =    re.compile( r'^(?P<basename>(?P<stack>stack_)?ptf_(?P<yyyymmdd>[0-9]{8})_'
                                     r'(?P<hhmmss>[0-9]{6})_(?P<filtercode>[^_\.]+))\.(?P<chiptag>[0-9]+)\.'
                                     r'(?P<filetype>[^\.]+)\.fits(\.[fg]z)?' )
    _match_basename = 'basename'
    _match_stack = 'stack'
    _match_yyyymmdd = 'yyyymmdd'
    _match_hhmmss = 'hhmmss'
    _match_filtercode = 'filtercode'
    _match_image_ccd = 'chiptag'
    _match_exposure_type = 'filetype'
    _match_image_filetype = 'filetype'
    
    _magzpunc_header_keyword = 'ZPTSIGMA'
    
    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )


    # ======================================================================
    # IRSA query thing to add a filter

    def filter_find_query( self, band ):
        filternamemap = { 'PTF_G': 'G', 'PTF_R': 'R' }
        return f"filter='{filternamemap[band.name]}'"
        
    # ======================================================================
    # Basic information about exposures from PTF

    @property
    def ap_radius( self ):
        return 1.0

    @property
    def ra_degrees_keyword(self):
        return "TELRA"

    @property
    def dec_degrees_keyword(self):
        return "TELDEC"

    @property
    def filter_keyword(self):
        return "FILTER"

    @property
    def mjd_keyword(self):
        return "OBSMJD"

    @property
    def saturate_keyword(self):
        return "SATURVAL"
    
    @property
    def readnoise_keyword(self):
        return "READNOI"

    @property
    def darkcurrent_keyword(self):
        return "DARKCUR"

    @property
    def exptime_keyword(self):
        return "AEXPTIME"

    @property
    def key_keywords(self):
        return ( 'ORIGIN,OBSERVER,INSTRUME,IMGTYP,FILTERID,FILTER,FILTERSL,OBJRAD,OBJDECD,TELESCOPE,'
                 'PIXSCALE,GAIN,SATURVAL,DBFIELD,IMAGEZPT,COLORTRM,ZPTSIGMA' )

    # ======================================================================
    # Information parsed from iamge filenames

    def hhmmss( self, image ):
        match = self._generalparse.search( image )
        if match is None:
            raise ValueError( f'Error parsing hhmmss from {os.path.basename(image)}' )
        return match.group(self._match_hhmmss)

    # ======================================================================
    # Information parsed from image headers
    
    def fieldname_from_header(self, header):
        return str(int(header['DBFIELD']))

    def filtername_from_header( self, header ):
        filt = header['FILTER'].strip()
        return f"PTF_{filt}"

    # ======================================================================
    # Photometric calibration.  I'm foolishly ignoring color terms right now.

    def photocalib_headeronly( self, image, cat=None ):
        with fits.open( image, mode="update" ) as hdu:
            hdu[0].header['MAGZP'] = ( hdu[0].header['IMAGEZPT'], "curveball ZP, just a copy of IMAGEZPT" )
            magzp = float( hdu[0].header['MAGZP'] )
            hdu[0].header['MAGZPERR'] = ( hdu[0].header['ZPTSIGMA'], "curveball sigZP, just a copy of ZPTSIGMA" )
            seeing = float( hdu[0].header['SEEING'] )
            skysig = float( hdu[0].header['SKYSIG'] )
            # Assume aperture photometry with an aperture of radius 1 FWHM
            hdu[0].header['LMT_MG'] = -2.5 * math.log10( 5 * math.sqrt(math.pi) * skysig * seeing ) + magzp
            return magzp

    # ======================================================================
    # ======================================================================
    # Downloading images from IRSA server

    def further_process_blob( self, blob, logger=logging.getLogger("main") ):
        logger.debug( "Querying secondary table to get limting magnitudes for PTF images" )
        cfg = config.Config.get()
        dbhost = cfg.value( 'ptf.seconddbhost' )
        dbport = cfg.value( 'ptf.seconddbport' )
        dbname = cfg.value( 'ptf.seconddbdb' )
        dbuser = cfg.value( 'ptf.seconddbuser' )
        dbpasswd = cfg.value( 'ptf.seconddbpasswd' )

        dbcon = psycopg2.connect( host=dbhost, port=dbport, dbname=dbname, user=dbuser, password=dbpasswd,
                                  cursor_factory=psycopg2.extras.RealDictCursor )
        cursor = dbcon.cursor()
        cursor.execute( "CREATE TEMP TABLE jdid(pfilename text, obsmjd double precision, ccdid int)" )
        strdata = io.StringIO( blob.loc[ :, ['pfilename', 'obsmjd','ccdid'] ]
                               .to_csv( header=False, index=False, sep=',' ) )
        # import pdb; pdb.set_trace()
        # cursor.execute( "SELECT table_name, column_name, data_type "
        #                 "FROM information_schema.columns "
        #                 "WHERE table_name='jdid'" )
        # junk = cursor.fetchall()
        # import pdb; pdb.set_trace()
        cursor.copy_from( strdata, 'jdid', columns=( 'pfilename', 'obsmjd', 'ccdid' ), sep=',' )
        cursor.execute( "SELECT p.filename,j.pfilename,j.obsmjd,j.ccdid,p.lmt_mg "
                        "FROM proc_image p "
                        "INNER JOIN jdid j ON "
                        " ( p.ccdid=j.ccdid ) AND  "
                        "   abs( j.obsmjd+2400000.5-p.ujd  ) < 0.0001 " )
        rows = cursor.fetchall()
        blob['maglimit'] = 0.
        for row in rows:
            blob.loc[ blob['pfilename']==row['pfilename'], 'maglimit' ] = float( row['lmt_mg' ] )
        if len( blob[ blob['maglimit'] < 1 ] ) > 0:
            import pdb; pdb.set_trace()
            logger.error( f"{len( blob[ blob['maglimit'] ] )} images didn't get lmt_mg" )
            raise RuntimeError( f"Didn't get maglimit for some images" )
        dbcon.rollback()
        dbcon.close()
        return blob
        
    
    def source_url( self, image, logger=logging.getLogger("main") ):
        """Get the source_url for an image that's in the database.
        
        image can be either a image filename/path or a db.Image object
        """
        if not isinstance( image, db.Image ):
            image = pathlib.Path(image).name
            match = self._imagetypeparse.search( image )
            if match is None:
                match = self._imageparse.search( image )
                if match is None:
                    raise ValueError( f"Error parsing PTF filename {image}" )
                image_filetype = ''
            else:
                image_filetype = match.group( self._match_image_filetype )
            if ( image_filetype != '' ) and ( image_filetype != 'mask' ):
                logger.error( f"PTF doesn't have filetype {image_filetype}" )
                return None
            image = db.Image.get_by_basename( f"{match.group(self._match_basename)}."
                                              f"{match.group(self._match_image_ccd)}" )
        else:
            image_filetype = ''

        extradir, dayfrac, dbpiid, filterid, ptffield = image.archive_identifier.split( "," )
        yyyymmdd = self.yyyymmdd( image.basename )
        match = re.search ('^([0-9]{4})-([0-9]{2})-([0-9]{2})', yyyymmdd )
        yyyy = match.group(1)
        mm = match.group(2)
        dd = match.group(3)
        hhmmss = self.hhmmss( image.basename )
        ccd = self.chiptag( f"{image.basename}.fits" )
        
        if image_filetype == "mask":
            return ( f"{self._dataurlbase}/proc/{yyyy}/{mm}/{dd}/f{int(filterid)}/c{int(ccd)}/{extradir}/"
                     f"PTF_{yyyy}{mm}{dd}{dayfrac}_i_p_mask_t{hhmmss}_u{dbpiid}_f{filterid}_p{ptffield}_c{ccd}.fits" )
        elif image_filetype == "":
            return ( f"{self._dataurlbase}/proc/{yyyy}/{mm}/{dd}/f{int(filterid)}/c{int(ccd)}/{extradir}/"
                     f"PTF_{yyyy}{mm}{dd}{dayfrac}_i_p_scie_t{hhmmss}_u{dbpiid}_f{filterid}_p{ptffield}_c{ccd}.fits" )
        else:
            raise RuntimeError( "This should never happen") 


    def blob_image_name( self, blob, index, filetype='image', logger=logging.getLogger("main") ):
        if ( index >= len(blob) ):
            raise IndexError( f'Index {index} into imageblob is greater than length {len(blob)}' )
        if filetype == 'image':
            tag = ''
        elif filetype == 'mask':
            tag = '.mask'
        else:
            logger.error( f"PTF archive doesn't have images of type {filetype}" )
            return None
        row = blob.iloc[index]
        match = re.search( '^.*/PTF_(?P<yyyymmdd>[0-9]{8})(?P<dayfrac>[0-9]{4})_i_p_scie_'
                           't(?P<hhmmss>[0-9]{6})_u(?P<dbpiid>[0-9]+)_f(?P<filterid>[0-9]{2})_'
                           'p(?P<ptffieldid>[0-9]+)_c(?P<ccd>[0-9]{2}).fits$',
                           row.pfilename )
        if match is None:
            logger.error( f"PTF blob_image_name: failed to parse {row.pfilename}" )
            raise ValueError( f"PTF blob_image_name: failed to parse {row.pfilename}" )
        filtercodemap = { '01': 'pg', '02': 'pr' }
        filename = ( f"ptf_{match.group('yyyymmdd')}_{match.group('hhmmss')}_"
                     f"{filtercodemap[match.group('filterid')]}.{match.group('ccd')}{tag}.fits" )
        return filename

    def blob_ptf_image_name_and_archid( self, blob, index, logger=logging.getLogger("main") ):
        if ( index >= len(blob) ):
            raise IndexError( f'Index {index} into imageblob is greater than length {len(blob)}' )
        row = blob.iloc[index]
        match = re.search( '^.*/(?P<dirs>[^/]+/[^/]+)/PTF_(?P<yyyymmdd>[0-9]{8})(?P<dayfrac>[0-9]{4})_i_p_scie_'
                           't(?P<hhmmss>[0-9]{6})_u(?P<dbpiid>[0-9]+)_f(?P<filterid>[0-9]{2})_'
                           'p(?P<ptffieldid>[0-9]+)_c(?P<ccd>[0-9]{2}).fits$',
                           row.pfilename )
        if match is None:
            logger.error( f"PTF blob_ptf_image_name_and_archid: failed to parse {row.pfilename}" )
            raise ValueError( f"PTF ptf_blob_image_name_and_archid: failed to parse {row.pfilename}" )
        filtercodemap = { '01': 'pg', '02': 'pr' }
        archid = ( f"{match.group('dirs')},{match.group('dayfrac')},{match.group('dbpiid')},"
                   f"{match.group('filterid')},{match.group('ptffieldid')}" )
        filename = ( f"ptf_{match.group('yyyymmdd')}_{match.group('hhmmss')}_"
                     f"{filtercodemap[match.group('filterid')]}.{match.group('ccd')}.fits" )
        return filename, archid
        
    def blob_image_filter( self, imageblob, index, logger=logging.getLogger("main") ):
        # Can't use .filter because that's interpreted as something.
        # This probably says something about loosey goosey langauges
        # like python that let you define object properties
        # at runtime....
        return imageblob.iloc[index]['filter']

    def blob_image_filtercode( self, imageblob, index, logger=logging.getLogger("main") ):
        filtercodemap = { 'R': 'pr', 'G': 'pg' }
        return filtercodemap[ imageblob.iloc[index]['filter'] ]
    
    # ======================================================================

    def download_blob_image( self, tab, index, rootdir=None, files=['image', 'mask'],
                             overwrite=False, logger=logging.getLogger('main') ):
        if not isinstance( tab, pandas.DataFrame ):
            raise ValueError( 'download_image: Passed image blob isn\'t the right sort of thing.' )
        if index > len(tab):
            raise IndexError( f'index {index} is greater than number of images {len(tab)} in image blob.' )

        cfg = config.Config.get()
        
        row = tab.iloc[index]

        if rootdir is None:
            rootdir = pathlib.Path( cfg.value('datadirs')[0] )
        else:
            rootdir = pathlib.Path( rootdir )

        oname, archid = self.blob_ptf_image_name_and_archid( tab, index )
        extradir = archid[0]
        obase = self.image_basename( oname )
        yyyymmdd = self.yyyymmdd( oname )
        match = re.search( '^([0-9]{4})-([0-9]{2})-([0-9]{2})', yyyymmdd )
        if match is None:
            raise RuntimeError( f'Failed to parse {yyyymmdd} for yyyy-mm-dd' )
        yyyy = match.group(1)
        mm = match.group(2)
        dd = match.group(3)
        filteridmap = { 'G': 1, 'R': 2 }
        filterid = filteridmap[ row['filter'] ]
        ccdnum = row.ccdid

        if rootdir is None:
            rootdir = pathlib.Path( cfg.value('datadirs')[0] )
        else:
            rootdir = pathlib.Path( rootdir )

        filedir = rootdir / self.archivesubdir( oname )
        filedir.mkdir( parents=True, exist_ok=True )

        dlfiles = []
        for filetype in files:
            if filetype == "image":
                url = f'{self._dataurlbase}/{row.pfilename}'
                outfile = oname
            elif filetype == "mask":
                url = f'{self._dataurlbase}/{row.afilename1}'
                outfile = f'{obase}.mask.fits'
            else:
                logger.warning( f'PTF doesn\'t have file type "{filetype}", not dowloading.' )
                continue

            if ( filedir / outfile ).exists():
                if not ( filedir / outfile ).is_file():
                    raise RuntimeError( f'File {str(filedir/outfile)} exists and is not a file!' )
                if not overwrite:
                    raise RuntimeError( f'File {str(filedir/outfile)} already exists, won\'t overwrite' )
                logger.warning( f'Deleting existing file {str(filedir/outfile)}' )
                (filedir/outfile).unlink()
            logger.debug( f'Querying url {url}' )
            res = requests.get( f"{url}" )
            if res.status_code != 200:
                raise Exception( f'PTF query returned status {res.status_code}' )
            logger.debug( f'PTF query returned type {res.headers["Content-Type"]}' )
            if filetype != "mask":
                with open( filedir / outfile, "wb" ) as ofp:
                    ofp.write( res.content )
                if filetype == "image":
                    # Add the "ARCHID" header field so create_or_load_image will
                    # stick the right thing in archive_identifier
                    with fits.open( filedir / outfile, mode="update" ) as hdu:
                        hdu[0].header['ARCHID'] = archid
            else:
                self.wrangle_mask( res.content, filedir / outfile, obase )
            dlfiles.append( filedir / outfile )

        return dlfiles
                
        
        
            
