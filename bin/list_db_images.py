#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
import argparse

import pandas

import db
import config
import exposuresource

from astropy.io import fits
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( None, logger=logger )

    parser = argparse.ArgumentParser( description="List images in database for object (not incl. stacks)" )
    parser.add_argument( "object", help="Name of object in database" )
    parser.add_argument( "-e", "--expsource", default=None, help="Exposure Source name" )
    parser.add_argument( "-b", "--band", default=None, help="Name of band; requires expsource (default: all)" )
    parser.add_argument( "-i", "--include-bad", default=False, action='store_true',
                         help="Include images marked as bad?" )
    parser.add_argument( "-s", "--include-stacks", default=False, action='store_true',
                         help="Include image stacks?  (Only works if you odn't give an expsource)" )
    parser.add_argument( "-p", "--positions", default=False, action='store_true',
                         help="Show SN positions (slows thinhgs down)" )
    parser.add_argument( "-n", "--num-images", default=None, type=int,
                         help="Number of images to show (default: all)" )
    args = parser.parse_args()

    with db.DB.get() as curdb:
        obj = db.Object.get_by_name( args.object, curdb=curdb )
        if obj is None:
            raise ValueError( f'Unknown object {args.object}' )
        scobj = SkyCoord( obj.ra, obj.dec, unit='degree' )
        bandobj = None
        expsid = None
        expsource = None
        if args.band is not None or args.positions:
            if args.expsource is None:
                raise RuntimeError( "If you specify --band or --position, you must also specify --expsource" )
            expsource = exposuresource.ExposureSource.get( args.expsource )
        if args.band is not None:
            if expsource is None:
                raise RuntimeError( f"Unknown exposure source {args.expsource}" )
            expsid = expsource.id
            bandobj = db.Band.get_by_name( args.band, expsource, curdb=curdb )
            if bandobj is None:
                raise ValueError( f'Unknown band {args.band}' )

        images = db.Image.get_including_point( obj.ra, obj.dec, band=bandobj, expsourceid=expsid,
                                               includebad=args.include_bad, curdb=curdb )
        if not args.include_stacks:
            images = [ i for i in images if not i.isstack ]

        if ( args.num_images is not None ) and ( len(images) > args.num_images ):
            images = images[ 0 : args.num_images ]

        df = pandas.DataFrame( { "basename": [ i.basename for i in images ],
                                 "mjd": [ i.mjd for i in images ],
                                 "band": [ i.band.name for i in images ],
                                 "magzp": [ i.magzp for i in images ],
                                 "seeing": [ i.seeing for i in images ],
                                 "lmt": [ i.limiting_mag for i in images ],
                                 "stack": [ i.isstack for i in images ],
                                 "bad": [ i.image_is_bad for i in images ] } )
        df.sort_values( 'mjd', inplace=True )

        if args.positions:
            print( f'{"image":48s}  {"t-t0":>8s}  {"band":6s} seeing  magzp  lmt_mg   (x,y)              bad?' )
        else:
            print( f'{"image":48s}  {"t-t0":>8s}  {"band":6s} seeing  magzp  lmt_mg bad?' )

        for i, row in df.iterrows():

            # Ugly hack
            for key in row.keys():
                if row[key] is None:
                    row[key] = -99

            if args.positions:
                logger.info( f"Getting image {row['basename']} so we can find position" )
                expsource.ensure_image_local( row['basename'], curdb=curdb, logger=logger )
                path = expsource.local_path_from_basename( row['basename'], filetype='image' )
                with fits.open( path ) as hdul:
                    wcs = WCS( hdul[0].header )
                    x, y = wcs.world_to_pixel( scobj )

                print( f'{row["basename"]:48s}  {row["mjd"]-obj.t0:8.1f}  {row["band"]:6s}  '
                       f'{row["seeing"]:5.2f}  {row["magzp"]:5.2f}  {row["lmt"]:5.2f}   '
                       f'{x:.1f},{y:.1f} '
                       f'       {"BAD" if row["bad"] else ""}' )

            else:
                print( f'{row["basename"]:48s}  {row["mjd"]-obj.t0:8.1f}  {row["band"]:6s}  '
                       f'{row["seeing"]:5.2f}  {row["magzp"]:5.2f}  {row["lmt"]:5.2f}'
                       f'       {"BAD" if row["bad"] else ""}' )



# ======================================================================

if __name__ == "__main__":
    main()
