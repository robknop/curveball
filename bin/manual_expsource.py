import re
import math
import datetime

import numpy

import astropy.time
import astropy.wcs
from astropy.io import fits

import config
from exposuresource import *

class Manual_Expsource(ExposureSource):
    """An exposure source for creating manual images for testing purposes.

    There is only one chip, chip 0.

    Filenames are manual_yyyymmdd_hhmmss.00.fits

    """
    NAME = "manual_expsource"
    SECONDARY = None

    _generalparse = re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?manual_(?P<yyyymmdd>[0-9]{8})_'
                                r'(?P<hhmmss>[0-9]{6}))' )
    _exposureparse = re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?manual_(?P<yyyymmdd>[0-9]{8})_'
                                 r'(?P<hhmmss>[0-9]{6}))\.fits(?P<fz>\.[fg]z)?' )
    _imageparse = re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?manual_(?P<yyyymmdd>[0-9]{8})_'
                                 r'(?P<hhmmss>[0-9]{6}))\.(?P<chiptag>00)\.fits(?P<fz>\.[fg]z)?' )
    _exposuretypeparse = re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?manual_(?P<yyyymmdd>[0-9]{8})_'
                                     r'(?P<hhmmss>[0-9]{6}))\.(?P<filetype>[^\.]+)\.fits(?P<fz>\.[fg]z)?' )
    _imagetypeparse = re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?manual_(?P<yyyymmdd>[0-9]{8})_'
                                  r'(?P<hhmmss>[0-9]{6}))\.(?P<chiptag>00)\.'
                                  r'(?P<filetype>[^\.]+)\.fits(?P<fz>\.[fg]z)?' )

    _match_basename = 'basename'
    _match_fake = 'fake'
    _match_stack = 'stack'
    _match_yyyymmdd = 'yyyymmdd'
    _match_hhmmss = 'hhmmss'
    _match_image_ccd = 'chiptag'
    _match_image_filetype = 'filetype'

    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )

    @property
    def ap_radius(self):
        return 1.0

    @property
    def filter_keyword(self):
        return "FILTER"

    @property
    def mjd_keyword(self):
        return "MJD"

    @property
    def readnoise_keyword(self):
        return "READNOI"

    @property
    def darkcurrent_keyword(self):
        return "DARKCUR"

    @property
    def exptime_keyword(self):
        return "EXPTIME"

    @property
    def key_keywords(self):
        return 'FILTER,TELRAD,TELDECD,GAIN,SATURATE,MAGZP,MAGZPUNC'

    # ======================================================================

    def photocalib( self, image, cat=None ):
        with fits.open( image, mode="update" ) as hdu:
            magzp = float( hdu[0].header['MAGZP'] )
            seeing = float( hdu[0].header['SEEING'] ) / self._dbinfo.pixscale
            skysig = float( hdu[0].header['SKYSIG'] )
            hdu[0].header['LMT_MG'] = -2.5 * math.log10( 5 * math.sqrt(math.pi) * skysig * seeing ) + magzp
            return magzp

    # ======================================================================

    def ensure_sourcefiles_local( self, basename, retries=5, curdb=None, logger=logging.getLogger("main" ) ):
        """For manual exposure source, there is no original archive; pull from our archive."""
        for ext in [ "", ".weight", ".mask" ]:
            self.get_file_from_archive( f"{basename}{ext}.fits", logger=logger )

    # ======================================================================

    def find_images( self, ra, dec, containing=True, dra=0.008, ddec=0.008,
                     band=None, starttime=None, endtime=None, minexptime=None,
                     logger=logging.getLogger("main") ):
        raise NotImplementedError( f"Can't find_images for manual exposure source" )

    # ======================================================================

    def create_image( self, imdata, weightdata=None, maskdata=None,
                      y=2023, m=4, d=1, h=6, minute=6, s=6, exptime=60.,
                      ra=120, dec=30., outdir=None, ignore_if_already_in_db=False ):
        """Create a fake image, not yet loaded into the database

        imdata -- a 1024x2048 numpy array of floats
        weightdata -- weight for imdata; if None, will be a bunch of 1s
        maskdata -- mask for imdata; if None, will be a bunch of 0.  Should be numpy.int16
        y -- year
        m -- month
        d -- day
        h -- hour
        minute -- minute
        s -- second
        exptime -- exposure time
        outdir -- base output directory (default : config datadirs[0] )
        ignore_if_already_in_db -- if True, merrily make the image even
           if it already exists in the database

        If an image with the basename that this image would have is
        already in the datbase, fails.  Overwrites any relevasnt
        existing files.

        """

        basename = f"manual_{y:04d}{m:02d}{d:02d}_{h:02d}{minute:02d}{s:02d}.00"
        if not ignore_if_already_in_db:
            oldimg = db.Image.get_by_basename( basename )
            if oldimg is not None:
                raise RuntimeError( f"{basename} already exists in the database" )

        if imdata.shape != ( 1024, 2048 ):
            raise RuntimeError( "imdata must be 1024x2048" )
        if weightdata is None:
            weightdata = numpy.full_like( imdata, 1., dtype=numpy.float32 )
        if maskdata is None:
            maskdata = numpy.full_like( imdata, 0, dtype=numpy.int16 )

        if outdir is None:
            outdir = config.Config.get().value( 'datadirs' )[0]
        outdir = pathlib.Path( outdir )

        t = astropy.time.Time( datetime.datetime( y, m, d, h, minute, s, tzinfo=datetime.timezone.utc ) )
        mjd = t.mjd

        wcs = astropy.wcs.WCS( naxis=2 )
        wcs.wcs.crpix = [ 1024, 512 ]
        wcs.wcs.crval = [ ra, dec ]
        wcs.wcs.ctype = [ 'RA---TAN', 'DEC--TAN' ]
        cosdec = math.cos( dec / 180 / math.pi )
        wcs.wcs.cdelt = [ -self._dbinfo.pixscale / 3600. / cosdec, self._dbinfo.pixscale / 3600. ]
        imghdr = wcs.to_header()
        imghdr.update( { 'DATE-OBS': f'{y:04d}-{m:02d}-{d:02d}',
                         'TIME-OBS': f'{h:02d}:{minute:02d}:{s:02d}',
                         'MJD': mjd,
                         'FILTER': 'open',
                         'READNOI': 0.,
                         'DARKCUR': 0.,
                         'EXPTIME': exptime,
                         'TELRAD': ra,
                         'TELDECD': dec,
                         'GAIN': 2.5,
                         'PIXSCALE': 0.37,
                         'SEEING': 1.3,
                         'MAGZP': 27.,
                         'MAGZPERR': 0.01,
                         'LMT_MG': 24.,
                         'SATURATE': 50000.,
                        } )

        imgpath = outdir / self.relpath( f'{basename}.fits' )
        imgpath.parent.mkdir( exist_ok=True, parents=True )
        fits.writeto( imgpath, imdata, imghdr, overwrite=True )
        fits.writeto( outdir / self.relpath( f'{basename}.weight.fits' ), weightdata, overwrite=True )
        fits.writeto( outdir / self.relpath( f'{basename}.mask.fits' ), maskdata, overwrite=True )

        return basename


