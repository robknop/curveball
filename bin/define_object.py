#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
import argparse

import db
import config

def define_object( name, ra, dec, peakt, z=None, classification=None, confidence=1.0, othernames=None, curdb=None ):
    with db.DB.get(curdb) as curdb:
        curobjs = db.Object.get_by_pos( ra, dec, curdb=curdb )
        if len(curobjs) > 0:
            raise RuntimeError( f'There\'s already object(s) within 3" of specified RA/Dec' )

        if othernames is None:
            othernames = ""
        else:
            if not isinstance( othernames, str ):
                othernames = ",".join( [ str(i).strip() for i in othernames ] )

        if classification is not None:
            if not isinstance( classification, db.ObjectClassification ):
                classobj = db.ObjectClassification.get_by_name( classification )
                if classobj is None:
                    raise ValueError( f'Unknown classification {classification}' )
                classification = classobj
            
        obj = db.Object( name=name, ra=ra, dec=dec, t0=peakt, z=z, confidence=confidence, othernames=othernames,
                         classification_id=None if classification is None else classification.id )
        curdb.db.add(obj)
        curdb.db.commit()

        return obj
        
def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( None, logger=logger )
    parser = argparse.ArgumentParser( description="Add object to database" )
    parser.add_argument( "name", help="Name of object" )
    parser.add_argument( "ra", type=float, help="RA (decimal degrees)" )
    parser.add_argument( "dec", type=float, help="Dec (decimal degrees)" )
    parser.add_argument( "t0", type=float, help="Time of peak (MJD)" )
    parser.add_argument( "-z", "--redshift", type=float, default=None,
                         help="Redshift of object" )
    parser.add_argument( "-c", "--classification", default=None,
                         help="Classification of object (must match name in database)" )
    parser.add_argument( "--confidence", default=1.0,
                         help="How confident (0-1) are you in classification (default: 1)" )
    parser.add_argument( "-o", "--othernames", default=None,
                         help="Other names for object (comma-separated, no spaces)" )
    args = parser.parse_args()

    obj = define_object( args.name, args.ra, args.dec, args.t0, z=args.redshift,
                         classification=args.classification, confidence=args.confidence,
                         othernames=args.othernames )

    print( f"Object {obj.name} has id {obj.id}, classification id {obj.classification_id}, redshift {obj.z}" )
    print( f"RA: {obj.ra}, Dec: {obj.dec}, t0: {obj.t0}" )
    print( f"Other names: {obj.othernames}" )
    
# ======================================================================

if __name__ == "__main__":
    main()
