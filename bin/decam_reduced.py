import pathlib
from exposuresource import ExposureSource
import config
from astropy.time import Time
from astropy.io import fits
from astropy.utils.data import download_file
import io
import os
import requests
import logging
import urllib
import pandas as pd
import util
import db
import re
import time
import math
import astrometry

from urllib.error import HTTPError

class DECam_Reduced(ExposureSource):
    NAME = "DECam"
    SECONDARY = "Reduced"
    
    _urlbase = "https://astroarchive.noirlab.edu"
    _search_keywords = ["archive_filename", "dateobs_min", "url", "ifilter", "exposure", "md5sum",
                          "ra_center", "dec_center"]
    _proposal_ids = ["2022A-724693", "2022A-388025", "2021A-0113", "2021B-0149"]
    _fov_2 = 1.5 # half the FOV, in degrees
    _timeout = 120 # seconds
    
    _imgtypecodes = {
        "image": "ooi",
        "mask": "ood",
        "weight": "oow",
        "raw": "ori",
    }

    def __init__( self, *args, **kwargs ):
        super().__init__( *args, **kwargs )
        # We are using the HDU number in filenames instead of the chip number
        self._generalparse = re.compile("(?P<fake>fake_)?(?P<stack>stack_)?c4d_(?P<date>\d{6,8})_(?P<time>\d{6})_"
                                        "(?P<typecode>o[a-z]{2})?_?"
                                         "(?P<filter>[^_]+)_(?P<v>v\d+)\.?(?P<chiptag>\d{2})"
                                        "?\.?(?P<imgtype>weight|mask)?"
                                        "(?P<ext>\..*)?$"
                                       )
        self._imageparse = re.compile( r'^(?P<basename>(?P<fake>fake_)?(?P<stack>stack_)?c4d_(?P<yyyymmdd>[0-9]{8})_'
                                       r'(?P<hhmmss>[0-9]{6})_(?P<filter>[^_]+)_(?P<v>v\d+))\.(?P<chiptag>\d{2})'
                                       r'\.fits(?P<fz>\.[fg]z)?$' )
        self._match_basename = "basename"
        self._match_image_ccd = "chiptag"
    
    # ======================================================================
    # NOTE TO ROB : you are going to have to process some of the downloaded
    #   images in order to get the header keywords right.
    # In particular, images are downlaoded as *stacks*, so need to be
    #   split.  The keyword CENRA and CENDEC have to be copied from
    #   HDU 0 to all the individual image headers as
    #   EXPRA and EXPDEC
    
    @property
    def ra_degrees_keyword(self):
        return "CENRA1"
    @property
    def dec_degrees_keyword(self):
        return "CENDEC1"

    @property
    def saturate_keyword(self):
        return "SATURATA"

    @property
    def gain_keyword(self):
        return "GAINA"

    @property
    def readnoise_keyword(self):
        return "RDNOISEA"
    
    @property
    def exptime_keyword(self):
        return "EXPTIME"
    
    # def darkcurrent_keyword(self):
    #     return None
    
    # @property
    # def filter_keyword(self):
    #     return "FILTER"
    
    @property
    def maglim_keyword(self):
        return "PHOTDPTH"
    
    def zpt_keyword(self):
        return "MAGZERO"
    
    @property
    def mjd_keyword(self):
        return "MJD-OBS"

    @property
    def key_keywords(self):
        return ( 'ORIGIN,OBSERVER,INSTRUME,PROCTYPE,PRODTYPE,BAND,FILTER,FILTPOS,RA,DEC,OBSERVAT,TELESCOP,'
                 'PIXSCAL1,PIXSCAL2,GAINA,GAINB,SATURATA,SATURATB,MAGZERO' )

    # ======================================================================
    
    def  _decam_parse_image( self, imagename ):
        imagename = pathlib.Path( imagename ).name
        match = self._generalparse.search( imagename )
        if match is None:
            raise RuntimeError( f"Failed to parse DECam Reduced filename {imagename}" )
        return match
    
    def exposure_basename( self, imagename ):
        match = self._decam_parse_image( imagename )
        
        basename = f"c4d_{match['date']}_{match['time']}_"+\
                 f"{match['filter']}_{match['v']}."
        if match['stack'] is not None:
            basename = 'stack_' + basename
        
        return ( basename )
    
    # def image_basename( self, imagename ):
    #     match = self._decam_parse_image( imagename )
    #     
    #      basename = f"c4d_{match['date']}_{match['time']}_"+\
    #              f"{match['filter']}_{match['v']}."+\
    #              f"{match['chiptag']}"
    #     if match['stack'] is not None:
    #         basename = "stack_" + basename
    #     
    #     return ( basename )
    
    def chiptag( self, imagename ):
        match = self._decam_parse_image( imagename )
        return match['chiptag']

    def yyyymmdd( self, imagename ):
        match = self._decam_parse_image( imagename )
        datestr = match['date']
        # in case it gets called on an archive file
        if len(datestr)==6: datestr = '20'+datestr
        return f'{datestr[0:4]}-{datestr[4:6]}-{datestr[6:8]}'
    
    def isstack( self, imagename ):
        match = self._decam_parse_image( imagename )
        return bool(match['stack'])

    def issfake( self, imagename ):
        match = self._decam_parse_image( imagename )
        return bool(match['fake'])

    def gain( self, image ):
        with fits.open( image, memmap=False ) as hdu:
            return float( hdu[0].header["GAINA"] + hdu[0].header["GAINB"] ) / 2.0
        
    def imgtypecode(self, imgtype):
        if imgtype in self._imgtypecodes: return self._imgtypecodes[imgtype]
        raise ValueError(f"The image type {imgtype} is not recognized.")
    
    def darkcurrent(self, image):
        # Assuming it's negligible based on:
        # https://noirlab.edu/science/programs/ctio/instruments/Dark-Energy-Camera/characteristics
        # Guess it's '~' whatever that means
        return 0
    
    def filtername_from_header(self, hdr):
        b = hdr['FILTER'].strip()[0]
        return 'DECam_'+b
    
    def filetype(self, filename):
        """ Returns the file type from the file name """
        match = self._decam_parse_image(filename)
        
        if match['imgtype'] is None:
            if match['typecode'] is None:
                return "image"
            elif match['typecode'] in self._imgtypecodes.values():
                # Reverse dictionary lookup in case it's called on archive file
                return list(self._imgtypecodes.keys())[
                    list(self._imgtypecodes.values()).index(match['typecode'])]
            else: raise ValueError(f"Invalid type code {match['typecode']} for {filename}.")
        elif match['imgtype'] in self._imgtypecodes.keys():
            return match['imgtype']
        else:
            raise ValueError(f"Unknown file type {match['imgtype']} for {filename}.")
    
    def blob_image_mjd( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns MJD of image for index in the blob returned by find_images"""
        return imageblob.iloc[index].obsmjd
    
    def blob_image_filter( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns some sort of filter name for index in the blob returned by find_images"""
        return imageblob.iloc[index].filtercode
    
    def blob_image_exptime( self, imageblob, index, logger=logging.getLogger("main") ):
        """Returns exposure time (seconds) of image for index in the blob returned by find_images"""
        return imageblob.iloc[index].exptime
    
    # def guess_seeing(self, image, logger=logging.getLogger("main")):
    #     with fits.open( image ) as hdu:
    #         try: ### TODO: Check this with Rob and graph data
    #             return float(hdu[0].header["SEEING"])
    #         except KeyError as e:
    #             logger.warning( f'SEEING not found in header for file {image}, just guessing the FWHM' )
    #             return float(hdu[0].header['FWHM']) * 0.263 # arcsec/pixel
        

    # ======================================================================
    # This is where the old define_dbentry was
    
    # ------------------------------------------------------------------------------------------
    def photocalib_headeronly( self, image, cat=None ):
        with fits.open( image, mode="update" ) as hdu:
            hdr = hdu[0].header
            # hdu[0].header['MAGZPERR'] = hdu[0].header['MAGZPUNC'] # Doesn't exist for DECam?
            magzperr = hdr['MAGZPERR'] if 'MAGZPERR' in hdr else 0.2 # Totally fake 
            ### TODO: FIXME: actual value?
            hdu[0].header['MAGZPERR'] = magzperr
            
            magzp = float(hdr['MAGZP']) if 'MAGZP' in hdr else float( hdr['MAGZERO'] )
            hdu[0].header['MAGZP'] = magzp
            
            seeing = float( hdr['SEEING'] )
            
            skysig = float(hdr['SKYSIG']) if 'SKYSIG' in hdr else float( hdr['SKYNOISE'] )
            # TODO: check if the images are sky subtracted
            hdu[0].header['SKYSIG'] = skysig
            
            # Assume aperture photometry with an aperture of radius 1 FWHM
            hdu[0].header['LMT_MG'] = -2.5 * math.log10( 5 * math.sqrt(math.pi) * skysig * seeing ) + magzp
            return magzp
            
    
    # ------------------------------------------------------------------------------------------
    def url_from_filename(self, archive_filename, md5sum=None, hdu_idx=None, 
                             logger=logging.getLogger("main")):
        if md5sum is None:
            url = f'{self._urlbase}/api/adv_search/find/'
            params = { "limit": 10 }
            searchspec = { 
                "outfields": ["md5sum"],
                "search": [
                    [ "archive_filename", archive_filename ]
                ]
            }
            res = requests.post( url, params=params, json=searchspec )
            res.raise_for_status()

            # Check status
            if res.status_code != 200:
                raise Exception( f"{self.__class__.__name__} query returned status {res.status_code}."+
                                f" {res.json()['errorMessage']}" )

            # return files
            files = pd.DataFrame(res.json()[1:])
            if files.empty:
                raise Exception(f"Could not find archive file matching: {archive_filename}")
            if len(files)>1:
                raise Exception(f"More than one file found for archive filename: {archive_filename}")
            
            md5sum = files.md5sum[0]
        
        ### FIXME: This needs a +1 while the NOIRLab ppl fix the archive, 
        ###      but will need to be deleted later
        data_url = f'{self._urlbase}/api/retrieve/{md5sum}'
        if hdu_idx is not None:
            data_url += f'/?hdus=0,{hdu_idx+1}'
        
        return data_url
    
    # ------------------------------------------------------------------------------------------ 
    def find_images( self, ra, dec, containing=True, dra=.151, ddec=.078,
                     band=None, starttime=None, endtime=None, minexptime=None,
                     logger=logging.getLogger("main") ):
        logger.info("Finding images")
        # From IRAsurvey
        if containing is False:
            raise NotImplementedError( f"{self.__class__.__name__} find_images "
                                       f"currently only works with 'containing'" )
        
        # Check band object
        if band is not None and not isinstance( band, db.Band ):
            bandobj = db.Band.get_by_name( band, self.id )
            if bandobj is None:
                raise RuntimeError( f'Unknown band {band}' )
            band = bandobj
        
        # Backup start and end times
        if endtime is None:
            endtime = Time.now().mjd
        if starttime is None:
            starttime = 55197 # Start of 2010
        
        # Handle jd->mjd conversion
        startmjd, endmjd = [(x-2400000.5 if x>2400000 else x) for x in [starttime,endtime]]
        startdate, enddate = startmjd-1, endmjd+1
        
        # Convert to iso format for astroarchive
        startdate, enddate = Time([startdate, enddate], format='mjd').to_value('iso', subfmt='date')
        print(startdate, enddate)
        
        jj = {
            "outfields" : [
                "archive_filename",
                "instrument",
                "telescope",
                "proposal",
                "proc_type",
                "prod_type",
                "caldat",
                "dateobs_center",
                "ifilter",
                "exposure",
                "md5sum",
                "MJD-OBS",
                "DATE-OBS",
                "PHOTDPTH",
                "SEEING",
                "AIRMASS",
                "hdu:ra_center",
                "hdu:dec_center",
                "hdu:FWHM",
                "hdu:hdu_idx",
                "hdu:CCDNUM",
            ],
            "search" : [
                ["instrument", "decam"],
                ["proc_type", "instcal"],
                ["prod_type", "image"],
                ["hdu:ra_center", ra-dra, ra+dra],
                ["hdu:dec_center", dec-ddec, dec+ddec],
                ["caldat", startdate, enddate],
            ]
        }
        
        # Add a search for the band
        if band is not None:
            jj['search'].append(['ifilter', band.filtercode, 'startswith'])
        
        # Build URL
        apiurl=f'{self._urlbase}/api/adv_search/find/?rectype=hdu&format=json&limit=0&count=N'
        print('\njj: \n',jj,'\nURL: ',apiurl)
        # Get response from astroarchive
        response = requests.post(apiurl,json=jj)
        response.raise_for_status()
        
        # Check response and return results
        if response.status_code == 200:
            files = pd.DataFrame(response.json()[1:])
        else:
            logger.error(response.json()['errorMessage'])
            logger.error(response.json()['traceback'])  # for API developer use
            return
        
        
        if files.empty:
            logger.warning("No files found in search.")
            return files
        
        files.rename(columns={"hdu:ra_center": "ra_center",
                           "hdu:dec_center": "dec_center",
                            "hdu:FWHM": "fwhm",
                           "hdu:hdu_idx": "hdu_idx",
                              "hdu:CCDNUM": "chiptag",
                              "MJD-OBS": 'obsmjd',
                              "DATE-OBS": 'obsdate',
                              "PHOTDPTH": "maglimit",
                              "exposure": "exptime",
                              "SEEING": "seeing",
                              "AIRMASS": "airmass",
                          }, inplace=True)
        
        files['moonillf'] = 0 # TODO: fix this later if needed
        
        files['filtercode'] = files.ifilter.str[0]
        
        if minexptime is not None:
            files = files[files.exposure>=minexptime]
        
        good = []
        for i,filename in enumerate(files.archive_filename):
            try:
                self.blob_image_name(files, i)
                good.append(i)
            except:
                pass
        
        files = files.iloc[good]
        
        noseeidx = pd.isnull(files['seeing'])
        noseeing = files[noseeidx]
        files.loc[noseeidx, 'seeing'] = noseeing['fwhm'] * 0.263 # arcsec / pixel
        # TODO: make sure this won't be used past the secure_references step
        # Maybe use it in guess_seeing
        
        # Filter MJDs further
        files = files[(files.obsmjd > startmjd) & (files.obsmjd < endmjd)]
        
        files.sort_values(by='obsmjd', inplace=True, ignore_index=True)
        
        return files
    
    # ------------------------------------------------------------------------------------------ 
    def edit_filename(self, filename, edits):
        """ Changes fields in an existing filename """
        match = self._generalparse.search(filename)
        if match is None: raise ValueError(f"The filename provided is not as expected: {filename}")
        
        fields = match.groupdict()
        fields.update(edits)
        
        fields['stack'] = 'stack_' if fields['stack'] else ''

        filename = f"{fields['stack']}c4d_{fields['date']}_{fields['time']}"

        if 'typecode' in fields and fields['typecode'] is not None:
            filename += f"_{fields['typecode']}"

        filename += f"_{fields['filter']}_{fields['v']}"

        if 'chiptag' in fields and fields['chiptag'] is not None:
            chiptag = int(fields['chiptag'])
            filename += f".{chiptag:{0}{2}}" # pad with 0s
        
        # No .image.fits - image is the default type
        if 'imgtype' in fields and (fields['imgtype'] is not None 
                                    and fields['imgtype']!='image'):
            filename += f".{fields['imgtype']}"

        if 'ext' in fields and fields['ext'] is not None:
            filename += fields['ext']
        
        return filename
    
    # ------------------------------------------------------------------------------------------ 
    def _combine_headers(self, header1, header2):
        """ Combines primary and secondary header for a DECam FITS image """
        # Set up new FITS header
        header = fits.Header()
        noprop = ["", "COMMENT", "HISTORY"]
        
        # Add a better filtercode to the header
        # TODO: make this obsolete by using the instrument in lookups
        # if "BAND" in header1:
        #     header1["FCODE"] = f"DECam_{header1['BAND'].strip()}"
        
        # Transfer keywords from header 1
        for keyword in header1:
            if keyword not in noprop:
                header[keyword] = header1[keyword]
        header["ENDPRIMR"] = "HERE"
        
        # Transfer keywords from header 2
        for keyword in header2:
            if keyword not in noprop:
                header[keyword] = header2[keyword]
        
        return header
    
    # ------------------------------------------------------------------------------------------ 
    def _try_image(self, url, outfile, retries=5, logger=logging.getLogger("main")):
        countdown = retries
        while countdown>0:
            countdown -=1
            
            # Download file from archive
            ### NOTE: This actually gets a .fits.fz
            try:
                # Make web request
                res = requests.get(url, timeout=self._timeout)
                res.raise_for_status()
                
                # Check status of web request
                if res.status_code != 200:
                    raise Exception(f'{res.status_code} ({res.reason}) ')
                
                # Write to file if successful
                with open(outfile, 'wb') as ofp:
                    ofp.write(res.content)
                
                return
            except Exception as e: # Raise exception if anything failed
                logger.error( f'Exception: {str(e)} trying to download from {url}'
                                              f'; will sleep 2s and try again' )
                time.sleep(2.5)
        
        # Couldn't find file (would have returned otherwise)
        raise RuntimeError( f'Repeated exceptions trying to download {url}. ' )
    
    # ------------------------------------------------------------------------------------------ 
    def blob_image_name( self, blob, index, filetype="image", logger=logging.getLogger("main") ):
        """
        Takes output from find_images and gets the (assumed) local image name
        NOTE: This will always return the .fits extension, even for the initial placeholder
                for the .fits.fz archive files
        """
        
        if ( index >= len(blob) ):
            raise IndexError( f'Index {index} into imageblob is greater than length {len(blob)}' )
        
        # Get image name and type code
        row = blob.iloc[index]
        image_filename = pathlib.Path(row.archive_filename)
        
        match = self._decam_parse_image(image_filename.name)
        if len(match['date'])<8:
            newdate = f'{row.obsdate[:4]}{row.obsdate[5:7]}{row.obsdate[8:10]}'
        else: newdate = match['date']
        
        # Change the filename according to inputs
        filename = self.edit_filename(image_filename.name, edits={'typecode': None, 
                                                       'imgtype': filetype,
                                                       'chiptag': row.chiptag,
                                                        'date': newdate,
                                                        'ext': '.fits',
                                                      })
        
        return filename
    
    # ------------------------------------------------------------------------------------------
    def ensure_sourcefiles_local( self, basename, retries=5, curdb=None, logger=logging.getLogger("main") ):
        # Get config info
        cfg = config.Config.get()
        rootdir = pathlib.Path( cfg.value('datadirs')[0] )
        
        # Set up image path name
        rootdir = rootdir.joinpath(f"{self.NAME}_{self.SECONDARY}") # DECAM folder
        datestring = self.yyyymmdd(basename)
        rootdir = rootdir.joinpath(datestring)
        
        # Make path, if necessary
        rootdir.mkdir(parents=True, exist_ok=True)
        
        # Open database
        with db.DB.get(curdb) as curdbg:
            
            # Get image info from database
            imgobj = db.Image.get_by_basename( basename, curdb=curdb )
            if imgobj is None:
                raise RuntimeError( f'No image {basename} in the databse' )
            basename = imgobj.basename
            
            # Retrieve and parse decam-specific archive info
            arch_id = imgobj.archive_identifier.split(',')
            if len(arch_id) != 2:
                raise ValueError(f"No hdu_idx provided in header for {basename}")
            archive_filename = pathlib.Path(arch_id[0].strip())
            hdu_idx = int(arch_id[1])
            
            # Download image and secondaries, if necessary
            dlfiles = []
            for imgtype in ['image', 'mask', 'weight']:
                # Get path to file on disk
                local_filename = self.edit_filename(basename+'.fits', 
                                              edits={'imgtype': imgtype})
                try_path = self.local_path( local_filename )
                # if try_path is None:
                #     local_filename += '.fz'
                #     try_path = self.local_path(local_filename)
                
                local_filename = rootdir / local_filename
                
                # Download file from archive
                if try_path is None:
                    
                    # Get image file type
                    if imgtype in self._imgtypecodes:
                        typecode = self._imgtypecodes[imgtype]
                    else: raise ValueError(f"Invalid file type: {imgtype}")
                    
                    # Change the typecode to match the file type
                    file_base = self.edit_filename(archive_filename.name, 
                                                   edits={'typecode': typecode})
                    filename = archive_filename.parent.joinpath(file_base)

                    new_path = self._save_image(outfile=local_filename, archive_filename=filename,
                                     hdu_idx=hdu_idx)
                    dlfiles.append(new_path)
                    
                else: dlfiles.append(try_path)
            
            imagepath, maskpath, weightpath = dlfiles
            
            # Update astrometry, if needed
            if imgobj.isastrom:
                # Update the downloaded image header with astrometry from database
                astrometry.update_image_header_from_db( imagepath.name, self, curdb=curdb, logger=logger )
            
            # Update photometry, if needed
            if imgobj.isphotom:
                # Make sure image header has right photometry keywords
                if ( ( imgobj.magzp is None ) or ( imgobj.seeing is None ) or ( imgobj.medsky is None ) or
                     ( imgobj.limiting_mag is None ) ):
                    raise RuntimeError( f"One of (magzp, seeing, medsky, limiting_mag) is None in database "
                                        f"for {imgobj.basename}" )
                with fits.open( imagepath, mode='update', memmap=False ) as hdu:
                    hdu[0].header['MAGZP'] = imgobj.magzp
                    hdu[0].header['MAGZPERR'] = 0.2  ## **** ### TODO: FIXME
                    hdu[0].header['SEEING'] = imgobj.seeing
                    hdu[0].header['MEDSKY'] = imgobj.medsky
                    hdu[0].header['SKYSIG'] = imgobj.skysig
                    hdu[0].header['LMT_MG'] = imgobj.limiting_mag
            
            # Make weightfile, if needed
            if weightpath is None:
                weightpath = imagepath.parent / f'{basename}.weight.fits'
                logger.info( f'Creating {weightpath.name}' )
                processing.make_weight( self, imagepath, mask=maskpath, weightpath=weightpath,
                                       logger=logger )
            return dlfiles
    
    # ------------------------------------------------------------------------------------------
    def _save_image(self, outfile, archive_filename, hdu_idx, 
                    md5sum=None, logger=logging.getLogger("main")):
        """
        Saves an image locally given an archive filename
        
        NOTE: The initial placeholder file from _try_image will have the extension .fits, but it
                will actually be a compressed .fits.fz file until _combine_headers is run
        """
        archive_filename = pathlib.Path(archive_filename)
        
        # Get type of output file
        outfile = pathlib.Path(outfile)
        filetype = self.filetype(outfile.name)
        
        # Retrieve file from archive
        try:
            # Get url from filename
            file_url = self.url_from_filename(str(archive_filename), hdu_idx=hdu_idx, md5sum=md5sum)
            
            # Try to download image
            self._try_image(url=file_url, outfile=outfile)
            logger.info(f"Successfully retrieved {filetype} file for image: {archive_filename.name}")
            
        except Exception as e:
            logger.warning(str(e))
            logger.error(f"{filetype} not downloaded for image: {archive_filename}.")
            return None

        # Combine header of downloaded file
        try:
            with fits.open(outfile) as file:
                # Check if new header needed
                if len(file)==2:
                    new_header = self._combine_headers(file[0].header, file[1].header)
                    new_data = file[1].data
                else: # Remove file if there is an issue saving over temporary
                    outfile.unlink() # TODO: is this a memory leak?
                    raise RuntimeError(f"Header for {archive_filename} ",
                                       f"should have two HDUs, not {len(file)}.")

            # Add filename to header
            new_header['ARCHID'] = f"{archive_filename},{hdu_idx}"

            # Write to output file
            new_file = fits.PrimaryHDU(header=new_header, data=new_data)
            new_file.writeto(outfile, overwrite=True)
                
        except Exception as e:
            logger.error(f"Error saving image {archive_filename} to {outfile}: {e}")
            return None
        
        return outfile
    
    # ------------------------------------------------------------------------------------------ 
    def download_blob_image( self, tab, index, rootdir=None, files=['image', 'mask', 'weight'],
                             overwrite=False, logger=logging.getLogger("main") ):
        """
        Downloads an image from a blob (tab) from the archive.
        """
        # Check input
        if not isinstance( tab, pd.DataFrame ):
            raise ValueError( 'download_blob_image: Passed image blob isn\'t the right sort of thing.' )
        if index > len(tab):
            raise IndexError( f'index {index} is greater than number of images {len(tab)} in image blob.' )
        
        # Get configuration details
        cfg = config.Config.get()
        if rootdir is None:
            rootdir = pathlib.Path( cfg.value('datadirs')[0] )
        else:
            rootdir = pathlib.Path( rootdir )
        
        # Get image details from table
        row = tab.iloc[index]
        image_filename = pathlib.Path(row.archive_filename)
        
        # Set up image path name
        rootdir = rootdir.joinpath(f"{self.NAME}_{self.SECONDARY}") # DECAM folder
        datestring = self.yyyymmdd(image_filename.name)
        rootdir = rootdir.joinpath(datestring)
        
        # Make path, if necessary
        rootdir.mkdir(parents=True, exist_ok=True)
        
        # Loop through and download each image/secondary type
        dlfiles = []
        ccdnums = []
        for filetype in files:
            # Get image file type
            if filetype in self._imgtypecodes:
                typecode = self._imgtypecodes[filetype]
            else: raise ValueError(f"Invalid file type: {filetype}")
            
            md5sum = row.md5sum if filetype=='image' else None
            
            # Change the typecode to match the file type
            file_base = self.edit_filename(image_filename.name, edits={'typecode': typecode})
            filename = image_filename.parent.joinpath(file_base)
            
            # Get the output filename (edited from archive)
            outfile = self.blob_image_name(tab, index, filetype)
            outfile = rootdir/outfile
            
            # Check CCDNUM
            # ccdnums.append(row.chiptag)
            
            # Check if file exists already
            if os.path.isfile(outfile) and not overwrite:
                logger.warning(f"{outfile} exists, not redownloading.")
                dlfiles.append(outfile)
                continue # Skip if there
            
            dlfile = self._save_image(outfile, archive_filename=str(filename), 
                                      hdu_idx=row.hdu_idx, md5sum=md5sum)
            if dlfile is not None: dlfiles.append(dlfile)
            
#             # Retrieve file from archive
#             try:
#                 file_url = self.url_from_filename(str(filename), hdu_idx=row.hdu_idx)
#                 self._try_image(url=file_url, outfile=outfile)
#                 logger.info(f"Successfully retrieved {filetype} file.")
#                 dlfiles.append(outfile)
#             except Exception as e:
#                 logger.warning(str(e))
#                 logger.error(f"{filetype} not downloaded for input file: {row.archive_filename}.")
            
#             # Combine header of downloaded file
#             with fits.open(outfile) as file:
#                 # Check if new header needed
#                 if len(file)==2:
#                     new_header = self._combine_headers(file[0].header, file[1].header)
#                     new_data = file[1].data
                    
#                     # Add URL to header
#                     new_header["URL"] = file_url
                    
#                     # Write to output file
#                     new_file = fits.PrimaryHDU(header=new_header, data=new_data)
#                     new_file.writeto(outfile, overwrite=True)
        
        ### TODO: make an actual check for this
        # If all CCD numbers aren't the same, delete all the files and error
        # if len(ccdnums)>0 and not all([c==ccdnums[0] for c in ccdnums]):
        #     for filename in dlfiles:
        #         filename.unlink()
        #     raise Exception("NOIRLab Archive error: The CCD numbers do not match for \n"
        #                     ',\n'.join([str(d) for d in dlfiles]))
            
        return dlfiles
        
