#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
import time
import math
import argparse
import logging
import pathlib
import numpy as np
from scipy.signal import convolve2d
from scipy.special import erfc
from scipy.optimize import differential_evolution
from scipy.optimize import minimize
from scipy.optimize import curve_fit
from numpy.polynomial.chebyshev import chebval2d
from astropy.io import fits

def grow_bpm( bpm, bpmgrow ):
    bpmgrow = int( math.floor( bpmgrow + 0.5 ) )
    bpmkernel = np.ones( (2*bpmgrow+1, 2*bpmgrow+1) )
    bpmconv = convolve2d( bpm, bpmkernel )
    bpmconv = bpmconv[ bpmgrow:-bpmgrow, bpmgrow:-bpmgrow ]
    w = np.where( bpmconv > 0 )
    # ROB!  What's the right value to use?
    bpm[w] = 1
    return bpm

def calc_pI( params, I ):
    a = params[0]
    σ = params[1]
    s = params[2]
    sqrt2 = math.sqrt(2)
    # The 0.5 and sqrt2 come from Bijaoui's erfc being different from scipy's
    pI = 1/a * np.exp(σ**2 / (2*a**2) ) * np.exp(-(I-s)/a) * 0.5 * erfc( ( σ/a - (I-s)/σ ) / sqrt2 )
    pI /= pI.sum()
    return pI

def fitfunc( params, I, nI ):
    pI = calc_pI( params, I )
    val = -( nI * np.log( pI ) ).sum()
    if math.isnan(val):
        val = 1e32
    return val
    
def fitfunc_jac( params, I, nI ):
    # NOTE!  Doesn't handle the hard edge for I<0
    a = params[0]
    σ = params[1]
    s = params[2]
    sqrt2 = math.sqrt(2)
    sqrtπ = math.sqrt(math.pi)
    # The 0.5 and sqrt2 come from Bijaoui's erfc being different from scipy's
    # pI = 1/a * np.exp(σ**2 / (2*a**2) ) * np.exp(-(I-s)/a) * 0.5*erfc( (σ/a - (I-s) / σ ) / sqrt2 )

    expσaterm = np.exp(σ**2 / (2*a**2) )
    dexpσatermda = expσaterm * ( -σ**2 / a**3 )
    dexpσatermdσ = expσaterm * ( σ / a**2 )
    expIsaterm = np.exp(-(I-s)/a)
    dexpIsatermda = (I-s)/(a**2) * expIsaterm
    dexpIsatermds = 1/a * expIsaterm
    erfarg =  (σ/a - (I-s) / σ ) / sqrt2
    erfintegralarg = np.exp( -erfarg**2 )
    derfargda = -σ/a**2 / sqrt2
    derfargdσ = 1/sqrt2 * ( 1/a + (I-s)/σ**2 )
    derfargds = 1/σ / sqrt2
    erfcterm = 0.5*erfc( erfarg )
    derfctermda = -1/sqrtπ * erfintegralarg * derfargda
    derfctermdσ = -1/sqrtπ * erfintegralarg * derfargdσ
    derfctermds = -1/sqrtπ * erfintegralarg * derfargds
    
    pI = 1/a * expσaterm * expIsaterm * erfcterm
    dpIda = pI * ( -1/a + dexpσatermda/expσaterm + dexpIsatermda/expIsaterm + derfctermda/erfcterm )
    dpIdσ = pI * ( dexpσatermdσ/expσaterm + derfctermdσ/erfcterm )
    dpIds = pI * ( dexpIsatermds/expIsaterm + derfctermds/erfcterm )

    pIsum = pI.sum()
    dpIda = dpIda / pIsum - pI / pIsum**2 * dpIda.sum()
    dpIdσ = dpIdσ / pIsum - pI / pIsum**2 * dpIdσ.sum()
    dpIds = dpIds / pIsum - pI / pIsum**2 * dpIds.sum()
    pI /= pIsum
    
    val = -( nI * np.log( pI ) ).sum()
    dvalda = -( nI/pI * dpIda ).sum()
    dvaldσ = -( nI/pI * dpIdσ ).sum()
    dvalds = -( nI/pI * dpIds ).sum()
    
    if math.isnan( val ):
        val = 1e32
        dvalda = -1e32
        dvaldσ = -1e32
        dvalds = -1e32
    # print( f'Returning {val}' )
    return ( val, (dvalda, dvaldσ, dvalds) )
    
def estimate_single_sky( image, bpm, sigcut=5.0, lowsigcut=None,
                         bpmgrow=3, nbins=None, logger=logging.getLogger("run"),
                         workers=1, iterations=5, sigconvfrac=None, maxiterations=20, figname=None ):
    """Estimate the sky level of an image using Bijaoui, 1980, A&A, 84, 81

    Parameters:
       image   : 2d numpy array with image data
       bpm     : 2d numpy array that's =0 for good pixels, >0 for bad pixels
       bpmgrow : Expand bad pixel mask around bad pixels by this much
       sigcut  : Fit to a histogram this many σ within the median of the image.
       workers : Doesn't do a damn thing.  Ideally, though, it paralellizes the fit.
                 Pass None to have it read the env variable OMP_NUM_THREADS.
       iterations : Iterate this many times in hopes of getting an optimal histogram
       sigconfvrac : if not None, instead of iterating a fixed number of times, iterate
                     until σ doesn't change by more than this from one iteration to the next
       maxiteratoins : Stop after this many iterations even if sigconvfrac isn't none

    Returns:
       a, σ, s

       These are the three parameters from Bijaoui 1980.  s is the sky
       level estimate.  σ is the estimation of sky noise (more or less).
       a is a parameters indicating what fraction of pixels are brighter
       and how bright they are— really a nuisance paramaeter

       Empirically, at least in extragalactic fields, this algorithm
       seems to *underestimate* the sky value by a little bit (>0.1
       times the sigma).
    """

    if lowsigcut is None:
        lowsigcut = sigcut
    
    if bpm is not None:
        if bpmgrow > 0:
            bpm = grow_bpm( bpm, bpmgrow )
        w = np.where( bpm == 0 )
        newimage = image[w]
        if newimage.size < 0.1*image.size:
            logger.warning( f'Masked more than 90% of {image.size} pixels in image segment; not masking '
                            f'this segment for sky subtraction' )
            newimage = None
        else:
            image = newimage

    iteration = 0
    done = False
    while not done:
        if iteration == 0:
            sguess = np.median( image )
            σguess = np.std( image )
            logger.debug( f'Image has median {sguess} and stdev {σguess}' )
            # ROB!  What's a good guess for a?  It's the exponential falloff of the histogram
            #  of starlight after sky subtraction.
            aguess = 3 * σguess
            logger.debug( f'Initial guess: a={aguess:.3f}, σ={σguess:.3f}, s={sguess:.3f}' )
        else:
            sguess = s
            σguess = σ
            aguess = a
        iteration += 1
            
        minrange = sguess - lowsigcut*σguess
        maxrange = sguess + sigcut*max(aguess, σguess)
        if nbins is None:
            nbins = max( 40, image.size // 8000 )
        hist, bins = np.histogram( image, bins=nbins, range=( minrange, maxrange ) )
        pixvals = (bins[:-1] + bins[1:]) / 2.
        logger.debug( f'Binsize: {pixvals[1]-pixvals[0]}; nbins: {nbins}' )
        
        # ****
        # if iteration == 0:
        #     np.save( "pixvals0.npy", pixvals )
        #     np.save( "hist0.npy", hist )
        # elif iteration == iterations-1:
        #     np.save( "pixvalsn.npy", pixvals )
        #     np.save( "histn.npy", hist )
        # ****
        
        paramguess = ( aguess, σguess, sguess )

        bounds = [ (0.01*aguess, 100.*aguess),
                   (0.1*σguess, 10.*σguess),
                   (sguess - 10*σguess, sguess + 10*σguess) ]

        # ****
        # logger.debug( f'In bijaouisky; initial guess: a={aguess}, σ={σguess}, s={sguess}' )
        # f, g = fitfunc_jac(paramguess, pixvals, hist)
        # fa, junk = fitfunc_jac( ( aguess*1.001, σguess, sguess ), pixvals, hist )
        # fσ, junk = fitfunc_jac( ( aguess, σguess*1.001, sguess ), pixvals, hist )
        # fs, junk = fitfunc_jac( ( aguess, σguess, sguess*1.001 ), pixvals, hist )
        # dfda = ( fa - f ) / ( 0.001*aguess )
        # dfdσ = ( fσ - f ) / ( 0.001*σguess )
        # dfds = ( fs - f ) / ( 0.001*sguess )
        # logger.debug( f'fitfunc_jac of initial guess : {f}' )
        # logger.debug( f'g[0] = {g[0]}, cheesy dfda = {dfda}' )
        # logger.debug( f'g[1] = {g[1]}, cheesy dfdσ = {dfdσ}' )
        # logger.debug( f'g[2] = {g[2]}, cheesy dfds = {dfds}' )
        # logger.debug( f'Sleeping for a long time.' )
        # time.sleep( 36000 )
        # ****

        # logger.debug( f'bijaouisky: starting minimization...' )
        # res = minimize( fitfunc_jac, paramguess, bounds=bounds, args=(pixvals, hist),
        #                 jac=True, tol=1e-6, options={ 'maxiter': 100 } )
        if workers is None:
            workers = os.getenv('OMP_NUM_THREADS')
        if workers is None:
            workers = 1
        # workers does spawn additional processes, but they don't seem to use extra cpus....
        # Probably a Python GIL thing or some such; or, maybe it's because I tried it inside MPI.
        res = differential_evolution( fitfunc, bounds, args=(pixvals, hist), tol=1e-6,
                                      workers=workers )
        # logger.debug( f'bijaouisky: ...done with minimization' )

        if not res.success:
            raise Exception( f'Minimization failiure: {res.message}' )
        a, σ, s = res.x
        logger.debug( f'Iteration {iteration}: a={a:.3f}, σ={σ:.3f}, s={s:.3f}' )

        if sigconvfrac is None:
            if iteration >= iterations:
                done = True
        else:
            if math.fabs( σguess - σ ) / σguess < sigconvfrac:
                done = True
            if iteration >= maxiterations:
                # logger.warning( f'bijaouisky stopping after {maxiterations} iterations even though σ hasn\'t '
                #                 f'converted to with {sigconvfrac}' )
                logger.warning( f'bijaouisky failing after {maxiterations} iterations: σ hasn\'t '
                                f'converted to with {sigconvfrac}' )
                raise RuntimeError( "Bijaouisky didn't converge" )
        
    if figname is not None:
        import matplotlib
        matplotlib.use('Agg')
        from matplotlib import pyplot
        fig = pyplot.figure( figsize=(8,6), tight_layout=True )
        ax = fig.add_subplot()
        ax.set_xlabel('Pixel value')
        ax.set_ylabel('N')
        ax.plot( pixvals, hist, color='blue' )
        fitvals = calc_pI( (a,σ,s), pixvals )
        norm = hist.sum() / fitvals.sum()
        ax.plot( pixvals, norm*fitvals, color='red' )
        fig.savefig(figname)
        
    return a, σ, s

# ======================================================================

def cheb( coords, order, shape, *params ):
    if len(params) == 1:
        params = params[0]
    x = ( coords[0] - shape[0]/2. ) / (shape[0]/2.)
    y = ( coords[1] - shape[1]/2. ) / (shape[1]/2.)
    return chebval2d( x, y, np.asarray(params).reshape( order+1, order+1 ) )

def estimate_smooth_sky( image, bpm, comm=None, order=2, cutshort=10, sigcut=4.0, lowsigcut=None,
                         iterations=5, bpmgrow=3, nbins=None, logger=logging.getLogger("run"), figname=None ):
    """Fit a 2-dimensional smooth sky.

    Cuts image into roughly square boxes such that the short side of the
    image is divded into cutshort boxes.  Calls estimate_single_sky on
    each of those boxes, and then fits a 2d chebyshev polynomial of the
    specified order to the results.  Uses MPI communicator comm, with
    rank 0 as a driver process and all the other ranks as a worker
    process.  (Pass comm=None to do it single-threaded.)

    Returns: sky, a, σ, s
    sky is the resultant sky in comm rank 0 ONLY (otherwise None)
    a, σ, s are the means of the estimate_single_sky returned values

    Empirically, at least in extragalactic fields, this algorithm seems
    to *underestimate* the sky value by a little bit (>0.1 times the
    sigma).
    """

    figpath = pathlib.Path( figname ) if figname is not None else None
    
    if comm is None:
        rank = 0
        size = 1
    else:
        rank = comm.Get_rank()
        size = comm.Get_size()

    # Do bpm growing single-threaded to avoid seam issues
    if bpm is not None and bpmgrow > 0:
        if rank == 0:
            bpm = grow_bpm( bpm, bpmgrow )
        if size > 1:
            bpm = comm.bcast( bpm, root=0 )
            
    # First, determine the coordinates of the boxes on the image
    if rank == 0:
        if image.shape[0] < image.shape[1]:
            cut0 = cutshort
            cut1 = cut0 * ( image.shape[1] // image.shape[0] )
        else:
            cut1 = cutshort
            cut0 = cut1 * ( image.shape[0] // image.shape[1] )

        xstart = np.arange( 0, image.shape[0], image.shape[0] // cut0, dtype=int )
        if image.shape[0] - xstart[-1] < image.shape[0] // (cut0 * 2):
            xstart = xstart[:-1]
        xstop = np.empty_like( xstart )
        xstop[:-1] = xstart[1:]
        xstop[-1] = image.shape[0]
        ystart = np.arange( 0, image.shape[1], image.shape[1] // cut1, dtype=int )
        if image.shape[1] - ystart[-1] < image.shape[1] // (cut1 * 2):
            ystart = ystart[:-1]
        ystop = np.empty_like( ystart )
        ystop[:-1] = ystart[1:]
        ystop[-1] = image.shape[1]

        xstartgrid, ystartgrid = np.meshgrid( xstart, ystart )
        xstopgrid, ystopgrid = np.meshgrid( xstop, ystop )
        agrid = np.zeros_like( xstartgrid, dtype=float )
        σgrid = np.zeros_like( xstartgrid, dtype=float )
        sgrid = np.zeros_like( xstartgrid, dtype=float )

    # Next, run estimate_single_sky on each one of these boxes,
    #  collecting the results in agrid, σgrid, and sgrid

    if rank == 0:
        logger.info( f'Estimating sky in {agrid.size} boxes with {size} processe{"s" if size>1 else ""}' )
    
    if size == 1:
        for i in range( agrid.shape[0] ):
            for j in range( agrid.shape[1] ):
                if figpath is not None:
                    thisfigname = figpath.parent / f'{figpath.stem}_{i:02d}_{j:02d}{figpath.suffix}'
                else:
                    thisfigname = None
                if bpm is not None:
                    a, σ, s = estimate_single_sky( image[ xstartgrid[i,j]:xstopgrid[i,j],
                                                          ystartgrid[i,j]:ystopgrid[i,j] ],
                                                   bpm[ xstartgrid[i,j]:xstopgrid[i,j],
                                                        ystartgrid[i,j]:ystopgrid[i,j] ],
                                                   sigcut=sigcut, lowsigcut=lowsigcut,
                                                   iterations=iterations,
                                                   bpmgrow=0, nbins=nbins, logger=logger,
                                                   workers=None, figname=thisfigname )
                else:
                    a, σ, s = estimate_single_sky( image[ xstartgrid[i,j]:xstopgrid[i,j],
                                                          ystartgrid[i,j]:ystopgrid[i,j] ],
                                                   None,
                                                   sigcut=sigcut, lowsigcut=lowsigcut,
                                                   iterations=iterations,
                                                   bpmgrow=0, nbins=nbins, logger=logger,
                                                   workers=None, figname=thisfigname )
                agrid[i, j] = a
                σgrid[i, j] = σ
                sgrid[i, j] = s
    else:
        DIE = 0
        HELLO = 1
        DO_WORK = 2
        RESULT_OF_WORK = 3

        # MPI messages come as lists msg
        # Based on msg[0], the rest of the list is:
        #   DIE            : empty
        #   HELLO          : hello; msg[1] = my rank
        #   DO_WORK        : a[1]=i, a[2]=j, a[3:7] = xstart, xstop, ystart, ystop
        #   RESULT_OF_WORK : result of work, msg[1]=rank, msg[2]=i, msg[3]=j, msg[4]=a, msg[5]=σ, msg[6]=s
    
        if rank == 0:
            did = np.full_like( xstartgrid, False )
            toldtostop = 0
            numdead = 0
            numdid = 0
            i = 0
            j = -1
            while ( numdid < did.size ) or ( numdead < size-1 ):
                msg = comm.recv()
                if msg[0] == HELLO:
                    workerrank = msg[1]
                elif msg[0] == DIE:
                    if i < agrid.shape[0]:
                        raise Exception( f'Rank {msg[1]} died prematurely.' )
                    logger.debug( f'Rank {msg[1]} has declared itself done.' )
                    numdead += 1
                    logger.debug( f'{numdead} workers have stopped.' )
                    continue
                elif msg[0] == RESULT_OF_WORK:
                    workerrank, worki, workj, a, σ, s = msg[1:]
                    if did[worki, workj]:
                        raise ValueError( f'Rank {workerrank} returned data for '
                                          f'({worki},{workj}), which was already done!' )
                    agrid[worki, workj] = a
                    σgrid[worki, workj] = σ
                    sgrid[worki, workj] = s
                    logger.debug( f'For [{worki:02d},{workj:02d}], a={a:.3f}, σ={σ:.3f}, s={s:.3f}' )
                    did[worki, workj] = True
                    numdid += 1
                    logger.debug( f'Did {numdid} of {did.size} tiles' )
                else:
                    raise ValueError( f"estimate_smooth_sky driver rank got unexpected message {msg[0]}" )

                if i >= agrid.shape[0]:
                    comm.send( [DIE], dest=workerrank )
                    toldtostop += 1
                    logger.debug( f'{toldtostop} told to stop' )
                else:
                    j += 1
                    if j >= agrid.shape[1]:
                        j = 0
                        i += 1
                    if i >= agrid.shape[0]:
                        comm.send( [DIE], dest=workerrank )
                        toldtostop += 1
                        logger.debug( f'{toldtostop} told to stop' )
                    else:
                        logger.debug( f'Sending {i},{j} to {workerrank} : '
                                      f'x=[{xstartgrid[i,j]}:{xstopgrid[i,j]}], '
                                      f'y=[{ystartgrid[i,j]}:{ystopgrid[i,j]}]' )
                        comm.send( [ DO_WORK, i, j, xstartgrid[i,j], xstopgrid[i,j], ystartgrid[i,j], ystopgrid[i,j] ],
                                   dest=workerrank )
        else:
            # Other ranks
            comm.send( [HELLO, rank], dest=0 )
            done = False
            while not done:
                msg = comm.recv()
                if msg[0] == DIE:
                    comm.send( [ DIE, rank ], dest=0 )
                    done = True
                elif msg[0] == DO_WORK:
                    i, j, xstart, xstop, ystart, ystop = msg[1:]
                    if figpath is not None:
                        thisfigname = figpath.parent / f'{figpath.stem}_{i:02d}_{j:02d}{figpath.suffix}'
                    else:
                        thisfigname = None
                    if bpm is not None:
                        a, σ, s = estimate_single_sky( image[xstart:xstop, ystart:ystop],
                                                       bpm[xstart:xstop, ystart:ystop],
                                                       sigcut=sigcut, lowsigcut=lowsigcut,
                                                       iterations=iterations,
                                                       bpmgrow=0, nbins=nbins, logger=logger,
                                                       workers=1, figname=thisfigname )
                    else:
                        a, σ, s = estimate_single_sky( image[xstart:xstop, ystart:ystop], None,
                                                       sigcut=sigcut, lowsigcut=lowsigcut,
                                                       iterations=iterations,
                                                       bpmgrow=0, nbins=nbins, logger=logger,
                                                       workers=1, figname=thisfigname )
                    comm.send( [ RESULT_OF_WORK, rank, i, j, a, σ, s ], dest=0 )
                else:
                    raise ValeuError( f'Rank {rank} got unknown command {msg[0]}' )

    comm.Barrier()
    # Now that we've estimated the sky in the various grids, we need to fit a smooth sky to it

    if rank == 0:
        logger.debug( f'Done getting sky in tiles, fitting order {order} polynomial to the tiles.' )
        meansky = sgrid.mean()
        σsky = sgrid.std()
        meanσ = σgrid.mean()
        σσ = σgrid.std()
        meana = agrid.mean()
        σa = agrid.std()
        xcoord = ( xstartgrid + xstopgrid ) / 2.
        ycoord = ( ystartgrid + ystopgrid ) / 2.
        smoothfitfunc = lambda coords, *params : cheb( coords, order, image.shape, *params )
        params = np.zeros( (order+1)**2 )
        params[0] = meansky
        params, pcov = curve_fit( smoothfitfunc, ( xcoord.reshape( [xcoord.size] ),
                                                   ycoord.reshape( [ycoord.size] ) ),
                                  sgrid.reshape( [sgrid.size] ), p0=params )
        logger.info( f'fit 0-term: {params[0]}; sky grid means: a={meana:.2f}±{σa:.2f}, '
                     f'σ={meanσ:.2f}±{σσ:.2f}, s={meansky:.2f}±{σsky:.2f}' )
        allyvals, allxvals = np.meshgrid( np.arange(image.shape[1]), np.arange(image.shape[0]) )
        allxvals = allxvals.reshape( [allxvals.size] )
        allyvals = allyvals.reshape( [allyvals.size] )
        sky = np.empty( [ image.size ] )
        sky = smoothfitfunc( (allxvals,allyvals), params )
        sky = sky.reshape( image.shape )
    else:
        sky = None
        meana = None
        meanσ = None
        meansky = None
    if size > 1:
        meana = comm.bcast( meana, root=0 )
        meanσ = comm.bcast( meanσ, root=0 )
        meansky = comm.bcast( meansky, root=0 )
    if rank == 0:
        logger.info( f'Done with estimate_smooth_sky' )
    return sky, meana, meanσ, meansky
        
# ======================================================================

def main():
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    
    if rank == 0:
        parser = argparse.ArgumentParser( description="Estimate image sky" )
        parser.add_argument( "image", help="Image filename" )
        parser.add_argument( "bpm", help="Bad Pixel Mask filename" )
        parser.add_argument( "-g", "--growbpm", type=int, default=3,
                             help="Grow bad pixel mask by this many pixels (def: 3)" )
        parser.add_argument( "-s", "--sigcut", type=float, default=5.0,
                             help="Histogram within this many sigma of median (default: 5.0)" )
        parser.add_argument( "-l", "--low-sigcut", type=float, default=None,
                             help="Histogram cutoff on low side (default: same as sigcut)" )
        parser.add_argument( "-o", "--order", type=int, default=0,
                             help="Order of fit (default 0, single-value sky)" )
        parser.add_argument( "-i", "--iterations", type=int, default=5,
                             help="Number of iterations of histogram-building (default: 5)" )
        parser.add_argument( "-w", "--write-sky", default=None,
                             help="Write sky to this file (default: don't write)" )
        parser.add_argument( "-c", "--converge", default=None, type=float,
                             help="Fractional change in σ to stop iterating (default: use -i)" )
        parser.add_argument( "--save-figure", default=None,
                             help="Matplotlib figure of histogram and fit" )
        args = parser.parse_args()
    else:
        args = None
    args = comm.bcast( args, root=0 )
        
    logger = logging.getLogger( "run" )
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - {rank:2d} - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.DEBUG )
    
    if rank == 0:
        with fits.open( args.image, memmap=False ) as imfp, fits.open( args.bpm, memmap=False ) as bpmfp:
            im = imfp[0].data
            bpm = bpmfp[0].data
    else:
        im = None
        bpm = None
    if size > 1:
        im = comm.bcast( im, root=0 )
        bpm = comm.bcast( bpm, root=0 )

    if args.order == 0:
        if rank == 0:
            a, σ, s = estimate_single_sky( im, bpm, sigcut=args.sigcut, lowsigcut=args.low_sigcut,
                                           iterations=args.iterations, sigconvfrac=args.converge,
                                           bpmgrow=args.growbpm, figname=args.save_figure  )
            skyim = np.full_like( im, s )
    else:
        if sigconvfrac is not None:
            logger.warning( f'--converge not supported for smooth sky, using {args.iterations} iterations' )
        skyim, a, σ, s = estimate_smooth_sky( im, bpm, comm, order=args.order,
                                              sigcut=args.sigcut, lowsigcut=args.low_sigcut,
                                              iterations=args.iterations,
                                              bpmgrow=args.growbpm, figname=args.save_figure )
    if ( args.write_sky is not None ) and ( rank == 0 ):
        hdu = fits.PrimaryHDU( data=skyim )
        hdu.writeto( args.write_sky, overwrite=True )
        logger.info( f'Wrote {args.write_sky}' )
        # ****
        # hdu.data = im - skyim
        # hdu.writeto( "testsub.fits", overwrite=True )
        # logger.info( f'Wrote testsub.fits' )
        # ****
            
    if rank == 0:
        print( f'a={a}; σ={σ}; s={s}' )
    


# ======================================================================

if __name__ == "__main__":
    main()
 
