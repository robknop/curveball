#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import time
import logging
import argparse
import pathlib

from exposuresource import ExposureSource
import config
import processing

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.DEBUG )

    config.Config.init( None, logger=logger )
    
    argparser = argparse.ArgumentParser( description="Download ZTF for RA/Dec and load into DB" )
    argparser.add_argument( "ra", type=float, help="RA decimal degrees" )
    argparser.add_argument( "dec", type=float, help="Dec decimal degrees" )
    argparser.add_argument( "peakt", type=float, help="Peak time (JD)" )
    argparser.add_argument( "-n", "--num-images", type=int, default=0,
                            help="Only get this many images (for testing). (Default: 0=all)" )
    args = argparser.parse_args()

    outdir = config.Config.get().value("datadirs")[0]
    ztf = ExposureSource.get( "ZTF" )
    
    images = ztf.find_images( args.ra, args.dec, logger=logger,
                              starttime=args.peakt-30, endtime=args.peakt+90 )

    if ( args.num_images > 0 ) and ( len(images) > args.num_images ):
        logger.debug( f"For testing purposes, reducinhg to {args.num_images} images" )
        images = images.iloc[0:args.num_images]
    
    for i in range(len(images)):
        logger.info( f'Downloading {i+1} of {len(images)} ZTF images...' )

        imagefile = ztf.blob_image_path( images, i, outdir )
        maskfile = ztf.blob_image_path( images, i, outdir, "mask" )
        weightfile = None
        if imagefile.is_file() and maskfile.is_file():
            logger.info( f'Image and mask for {imagefile.name} already exist, not downloading.' )
            weightfile = imagefile.parent / imagefile.name.replace(".fits", ".weight.fits")
            if not weightfile.is_file():
                weightfile = None
        else:
            countdown = 5
            success = False
            while countdown > 0:
                try:
                    downloaded = ztf.download_blob_image( images, i, rootdir=outdir, logger=logger )
                    success = True
                    countdown = 0
                except Exception as e:
                    countdown -= 1
                    if countdown > 0:
                        logger.warning( f'Exception trying to download image {i}, retrying: {str(e)}' )
                        time.sleep(1)
                    else:
                        logger.error( f'Failed to download image {i}, skipping it' )
            if not success:
                continue

            # I know I'm not going to get a weight file for ZTF...
            imagefile = None
            maskfile = None
            weightfile = None
            for dlimage in downloaded:
                ftype = ztf.filetype( dlimage )
                if ftype == "image":
                    imagefile = dlimage
                elif ftype == "mask":
                    maskfile = dlimage
                elif ftype == "weight":
                    weightfile = dlimage
                else:
                    raise RuntimeError( f'Unknown file type {ftype} for downloaded image {dlimage}' )
        logger.debug( f'Got image file {imagefile}' )
        logger.debug( f'Got mask file {maskfile}' )
        logger.debug( f'Got weight file {weightfile}' )

        if imagefile is None or maskfile is None:
            logger.error( f'Image or mask missing for image {i}!  Stuff may be downloaded, but not loading it to DB.' )
            continue

        if weightfile is None:
            logger.info( f'Building weight for {imagefile}...' )
            weightfile = processing.make_weight( ztf, outdir/imagefile, mask=outdir/maskfile, logger=logger )

        logger.info( f'Loading {imagefile} into database' )
        ztf.create_or_load_image( outdir/imagefile, logger=logger )
        
# ======================================================================

if __name__ == "__main__":
    main()
