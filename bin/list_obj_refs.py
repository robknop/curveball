import sys
import io
import logging
import argparse

import db

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    parser = argparse.ArgumentParser( description="List references defined for an object" )
    parser.add_argument( "object", help="Name of object in database" )
    args = parser.parse_args()

    with db.DB.get() as curdb:
        obj = db.Object.get_by_name( args.object, curdb=curdb )
        objrefs = db.ObjectReference.get_for_obj( obj, curdb=curdb )
        objrefs.sort( key=lambda x : x.image.mjd )
        
        sio = io.StringIO()
        sio.write( f"{len(objrefs)} references known for {obj.name}:\n" )
        sio.write( f"  {'basename':<50s}  {'band':<8s}  {'seeing':6s}  "
                   f"{'zp':5s}  {'lmt_mg':6s}  {'mjd0':8s}  {'mjdn':8s}\n")
        sio.write( f"  ----------------------------------------------------"
                   f"---------------------------------------------------\n" )
        for objref in objrefs:
            sio.write( f"  {objref.image.basename:<50s}  {objref.image.band.name:<8s}  "
                       f"{objref.image.seeing:6.2f}  {objref.image.magzp:5.2f}  {objref.image.limiting_mag:6.2f}  "
                       f"{objref.image.mjd:8.2f}  " )
            if objref.image.isstack:
                members = db.StackMember.get_stack_members( objref.image.id, curdb=curdb )
                members.sort( key=lambda x: x.mjd )
                sio.write( f"{members[-1].mjd:8.2f}" )
            sio.write( "\n" )
        logger.info( sio.getvalue() )
            
# ======================================================================
if __name__ == "__main__":
    main()
    
