#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import re
import time
import math
import logging
import pathlib
import subprocess
import argparse
import json
import requests

import numpy as np
import pandas

from astropy.io import fits
import astropy.io.votable
import astropy.table
from astropy.wcs import WCS

import ldac
import processing
import db
import config
from exposuresource import ExposureSource

_fitsparse = re.compile( '^(.*)\.fits(\.f[gz])?' )

# ======================================================================

def update_image_header_from_db( image, expsource, curdb=None, logger=logging.getLogger("main") ):
    with db.DB.get(curdb) as curdb:
        localfile = expsource.local_path( image )
        if localfile is None:
            raise FileNotFoundError( f'Can\'t find image {image}' )
        basename = expsource.image_basename( image )
        wcs = db.WCS.get_for_image( basename, curdb=curdb )
        if wcs is None:
            logger.warning( f'No WCS in database for {basename}, not updating file' )
            return
        # json seems to have saved my tuples as lists... but fits wants lists
        hdrtuples = [ tuple(card) for card in wcs.header ]
        with fits.open( localfile, mode='update', memmap=False ) as hdu:
            hdu[0].header.extend( hdrtuples, update=True )

# ======================================================================

def dump_gaia_regs( df, fname ):
    with open( fname, "w" ) as regfile:
        for i, row in df.iterrows():
            regfile.write( f'icrs;circle({row["X_WORLD"]:.5f}d,{row["Y_WORLD"]:.5f}d,2") # color=blue width=2\n' )

def download_gaia_stars( ra=None, dec=None, dra=None, ddec=None,
                         wcs=None, xsize=None, ysize=None, buffer=0.2,
                         cat="gaia.cat", forcenew=False, regs=None, limmag=25.,
                         logger=logging.getLogger("main") ):
    """Download Giaia stars from https://ls4-gaia-dr3.lbl.gov

    Specify EITHER ra, dec OR wcs, xsize, ysize
    If ra, dec, specify EITHER dra, dec OR radius

    ra, dec — coordinates of center of field (decimal degrees)
    dra, ddec — size of field, in decimal degrees.  This is solid-angle
         borders, NOT coordinates (i.e. dra will be divided by cos(dec) )

    wcs — The WCS of the image we're getting gaia stars for
    xsize — horizontal size of the FITS image (which is second numpy index)
    ysize — vertical size of the FITS image (first numpy index)

    buffer — Find stars that are bigger than the specified field by
        this fraction along each axis (default: 0.2)

    cat — output catalog filename (default "gaia.cat")
    regs — output ds9 region filename (default: nothing written)

    limmag — only get stars of this magnitude or brighter
    logger — log here

    retries — retry the query this many times before giving up (default: 5)
    sleeptime — sleep this many seconds before a retry (default: 5)

    If cat is not None, writes a FITS table to cat with fields:
      X_WORLD         (from database field ra)
      Y_WORLD         (from database field dec)
      MAG             (from database field phot_g_mean_mag)
      MAGERR          ( = 1.086 / database field phot_g_mean_flux_over_error )
      OBSDATE         (fixed to 2015.5)
      ERRA_WORLD      (max of database field ra_error, dec_error)
      ERRB_WORLD      (same as ERRA_WORLD)
      ERRTHETA_WORLD  (hardcoded to 10.0 for no adequately explained reason)

    Returns a Pandas dataframe with those columns (and a couple of other intermediate ones)

    """

    if cat is not None:
        logger.debug( f'Looking for gaia stars at {cat}' )
        cat = pathlib.Path( cat )
        if cat.exists():
            if not cat.is_file():
                raise RuntimeError( f'{cat} exists but is not a file!' )
            if forcenew:
                logger.debug( f'Deleteing {cat}' )
                cat.unlink()
            else:
                logger.info( f'Reading gaia stars from {cat}' )
                tbl = ldac.get_table_from_ldac( cat )
                df = tbl.to_pandas()
                if regs is not None:
                    dump_gaia_regs( df, regs )
                return df
    
    if ( ra is not None ) or ( dec is not None ):
        if ( wcs is not None ):
            return RuntimeError( f'Specify either ra/dec or pass a wcs' )
        if ( ra is None ) or ( dec is None ) or ( dra is None ) or ( ddec is None ):
            return RuntimeError( f'Must specify all of ra, dec, dra, ddec' )
        dra /= math.cos(dec)
        ra0 = ra - dra / 2.
        ra1 = ra + dra / 2.
        dec0 = dec - ddec / 2.
        dec1 = dec + ddec / 2.
    else:
        if wcs is None:
            return RuntimeError( f'Specify either ra/dec or pass a wcs' )
        if ( xsize is None ) or ( ysize is None ):
            return runtimeError( f'Must specify xsize and ysize with wcs' )
        ra, dec = wcs.all_pix2world( [ [xsize/2., ysize/2.] ], 0 )[0]
        corners = wcs.all_pix2world( [ [0, 0], [0, ysize-1], [xsize-1, 0], [xsize-1, ysize-1] ], 0 )
        ra0 = corners[:, 0].min()
        ra1 = corners[:, 0].max()
        dec0 = corners[:, 1].min()
        dec1 = corners[:, 1].max()
        
    dra = ra1 - ra0
    ddec = dec1 - dec0

    if buffer > 1.:
        ra0 -= (buffer/2.) * dra
        ra1 += (buffer/2.) * dra
        dec0 -= (buffer/2.) * ddec
        dec1 += (buffer/2.) * ddec
        dra *= buffer
        ddec *= buffer
        
    logger.info( f'Querying GAIA stars at RA={ra:.4f}°, DEC={dec:.4f}° with width={dra:.4f}°, height={ddec:.4f}°' )

    cfg = config.Config.get()
    url = f"{cfg.value('gaia.rectserver')}/{ra0}/{ra1}/{dec0}/{dec1}/{limmag}"
    res = requests.post( url, timeout=cfg.value('gaia.rectserver_timeout_sec') )
    if res.status_code != 200:
        raise RuntimeError( f"Request to {cfg.value('gaia.rectserver')} returned {res.status_code}" )

    df = pandas.DataFrame( res.json() )

    # Remove unexpected columns, rename columns to what is expected
    rename_columns = { "phot_g_mean_mag": "MAG",
                       "ra": "X_WORLD",
                       "dec": "Y_WORLD",
                       "phot_g_mean_flux_over_error": "FOVERFE",
                       "ra_error": "ERRA_WORLD",
                       "dec_error": "ERRB_WORLD",
                      }
    for col in df.columns:
        if col not in rename_columns.keys():
            df.drop( col, axis=1, inplace=True )
    df.rename( columns=rename_columns, inplace=True )

    # Add and calculate columns that scamp needs

    df["OBSDATE"] = 2015.5

    # calculate parameters that scamp needs
    # ERRATHETA_WORLD is arbitrary, but we are approximating the errors as equal                              
    df["MAGERR"] = 1.086 / df["FOVERFE"]
    df["ERRTHETA_WORLD"] = 10.0
    df["ERRA_WORLD"] = df["ERRB_WORLD"] = np.max( list(zip(df["ERRA_WORLD"], df["ERRB_WORLD"])), axis=1 )

    if cat is not None:
        tab = astropy.table.Table.from_pandas( df[ [ "X_WORLD", "Y_WORLD", "MAG", "MAGERR", "OBSDATE",
                                                     "ERRA_WORLD", "ERRB_WORLD", "ERRTHETA_WORLD" ] ] )
        logger.info( f'Writing {len(tab)} gaia objects to {os.path.basename(cat)}' )
        ldac.save_table_as_ldac( tab, cat, overwrite=True )

    if regs is not None:
        dump_gaia_regs( df, regs )

    return df

# ======================================================================

def save_image_header_to_db_wcs( expsource, basename, loadastromhead=False, curdb=None,
                                 logger=logging.getLogger("main") ):
    """Update / create a WCS database object based on the image header.  Also set Image.isastrom to True.

    expsource -- exposure source of image
    basename -- basename of image
    loadastromhead -- If True, instead of image header, look at basename.astrom.head (from scamp)
    curdb -- (optional) a db.DB object
    logger -- (optional) a logging.logger object

    """
    with db.DB.get( curdb ) as curdb:
        imgobj = db.Image.get_by_basename( basename, curdb=curdb )
        imagepath = expsource.local_path( f"{basename}.fits" )
        imagedir = imagepath.parent
        if imgobj is None:
            logger.error( f"Can't find image {basename} in the database!  Can't save WCS." )
            raise RuntimeError( "Tried to save WCS to db with not-in-database image {basename}" )
        if loadastromhead:
            with open( imagedir / f'{basename}.astrom.head', "r" ) as ifp:
                hdrtext = ifp.read()
                # Fix Scamp output for FITS standard, which is based on straight-up ASCII
            hdrtext = hdrtext.replace( 'é', 'e' )
            hdr = fits.Header.fromstring( hdrtext, sep='\n' )
        else:
            hdrmatches = [ 'PV', 'CRPIX', 'CRVAL', 'CDELT', 'CUNIT', 'CTYPE', 'CD1_1', 'CD1_2', 'CD2_1', 'CD2_2',
                           'LONPOLE', 'LATPOLE', 'RADESYS', 'WCSAXES' ]
            imagepath = expsource.local_path( f'{basename}.fits' )
            with fits.open( imagepath, memmap="False" ) as hdul:
                kws = {}
                for k, v in hdul[0].header.items():
                    match = False
                    for comp in hdrmatches:
                        if k[0:len(comp)] == comp:
                            match = True
                            break
                    if match:
                        kws[k] = v
            hdr = fits.Header( kws )
        hdrjson = []
        for card in hdr.items():
            hdrjson.append( tuple(card) )
        wcsobj = db.WCS.get_for_image( imgobj, curdb=curdb )
        if wcsobj is None:
            wcsobj = db.WCS( image_id=imgobj.id )
            curdb.db.add( wcsobj )
        wcsobj.header = hdrjson
        imgobj.isastrom = True
        curdb.db.commit()
    

# ======================================================================

def astrom_calib( image, cat=None, expsource=None,
                  maximgobj=2000, limmag=25.,
                  poserrs=[1.0, 2.0, 0.5, 0.25, 5.0, 10.0],
                  brightlimits=[14., 16., 18.],
                  dimlimit=23.,
                  crossidrad=2.0,
                  maxsig=0.15,
                  fracmatch=0.1,
                  minnummatch=8,
                  imaflags_iso_mask=0xffff,
                  forcenewgaia=False,
                  imagereg=None, gaiareg=None,
                  timeout=600,
                  noupdateheader=False,
                  savedb=False,
                  logger=logging.getLogger("main"),
                  curdb=None ):
    """Update image WCS based on match to Gaia.

    image — FITS image of the image we're calibrating.  A single-HDU image.
    cat — Path of catalog extracted from the image.  If None, one will
       be generated named <imagename>.cat (where <imagename> is the
       image name with .fits stripped).  Assumes (I think) LDAC format,
       or at least whatever sextractor gives.  (The table is in the
       third HDU, i.e. index 2.)
    maximgobj — reduce number of objects in the image to this many (for runtime);
           will only included unflagged images.  Pass None to do no cut.
    limmag — limiting magnitude of gaia objects to grab (default: 25)
    poserrs — list of position errors to try in scamp (default: 1, 2, .5, .25, 5, 10)
    brightlimits — bright-side limit to try in scamp (default: 14, 16, 18)
    dimlimit — dim-side limit for scamp (default: 23)
    crossidrad — CROSSID_RADIUS to pass to samp (default: 2.0)
    maxsig — average of uncertainties in x and y match must be ≤ this (default: 0.15)
    fracmatch — scamp must match at least this fraction of the min(image catalog, gaia catalog)
           for us to accept the solution (default: 0.1)
    minnummatch — scamp must match at least this many objects for us to accept
           the solution (default: 8)
    imaflags_iso_mask — will bitwise AND IMAFLAGS_ISO with this and consider
           anything with a non-zero result of the AND as "bad".  (Default: 0xffff)
    forcenewgaia — If False, will look for a .gaia.cat file and use that if it's there
    imagereg — ds9 region file of image objects fed to scamp (default: not written)
    gaiareg — ds9 gaia region file (default: not written)
    timeout — How many seconds to let scamp try to do its thing (default: 600)
    noupdateheader — Don't actually update the .fits image header, just write a .astrom.head file
    savedb — save the solution to the wcs table in the database?
    logger — a logging object

    Will create a templefile <basename>.astrom.cat that has the contents
    of cat, omitting things with IMAFLAGS_ISO non-0.  (It *doesn't* omit
    non-0 FLAGS, because I've found I need to include some saturated
    stars to get a match, but more importantly in crowded fields you
    have to include deblended things.  IMAFLAGS_ISO may well have
    saturated stars matched, though; match them there if you don't want
    them used.)

    Generates a file named <basename>.gaia.cat.  Updates image WCS.

    Returns True if it thinks the solution worked, False otherwise

    TODO : if savedb is true, try loading from the db!  Put in an option to force redo

    """

    imagepath = pathlib.Path( image )
    imagedir = imagepath.parent
    imagename = imagepath.name

    match = _fitsparse.search( imagename )
    if match is None:
        logger.error( f'Failed to parse {imagename} for *.fits (with maybe .fz or .gz); that must work here' )
        raise ValueError( f'Failed to parse {imagename} for *.fits (with maybe .fz or .gz)' )
    else:
        basename = match.group(1)
    
    ### FIXME
    weight = imagedir / f'{basename}.weight.fits' # should exist?
    mask = imagedir / f'{basename}.mask.fits' # should exist?
    gaiacat = imagedir / f'{basename}.gaia.cat' # doesn't exist
    tmpcat = imagedir / f'{basename}.astrom.cat' # doesn't exist
    xmlname = imagedir / f'{basename}.scamp.xml' # doesn't exist
    
    # Make image catalog if necessary
    
    if cat is None:
        if expsource is None:
            raise RuntimeError( f'Must pass an exposure source if you don\'t pass a catalog' )
        logger.info( f'Extracting catalog from {imagename} for astrometry' )
        cat = imagedir / f'{basename}.cat'
        processing.extract_catalog( expsource, image, weight, cat, mask=mask, logger=logger )

    # Filter the image catalog
        
    with fits.open( cat, memmap=False ) as cathdu:
        w = np.where( ( cathdu[2].data["IMAFLAGS_ISO"] & imaflags_iso_mask ) == 0 )
        origlength = len( cathdu[2].data )
        cathdu[2].data = cathdu[2].data[w]
        logger.debug( f'{len(cathdu[2].data)} of {origlength} detections left after removing flagged ones' )
        if ( maximgobj is not None ) and ( len(cathdu[2].data) > maximgobj ):
            logger.info( f'Reducing number of detections on {imagename} to {maximgobj} to help '
                         'scamp succeed in a reasonable time.' )
            dexes = np.argsort( -cathdu[2].data["FLUX_AUTO"] / cathdu[2].data["FLUXERR_AUTO"] )
            cathdu[2].data = cathdu[2].data[dexes[0:maximgobj]]
        cathdu.writeto( tmpcat, overwrite=True )
        ndata = len( cathdu[2].data )
        
        if imagereg is not None:
            with open( imagereg, "w" ) as regfile:
                for row in cathdu[2].data:
                    color = "red" if ( row['FLAGS'] != 0 ) else "green"
                    regfile.write( f'image;circle({row["X_IMAGE"]:.2f},{row["Y_IMAGE"]:.2f},10) '
                                   f'# color={color} width=2\n' )
            
    # Get the gaia catalog

    with fits.open( image, memmap=False ) as hdu:
        wcs = WCS( hdu[0].header )
        xsize = hdu[0].header['NAXIS1']
        ysize = hdu[0].header['NAXIS2']
    gaiadf = download_gaia_stars( wcs=wcs, xsize=xsize, ysize=ysize, cat=gaiacat, limmag=limmag,
                                  regs=gaiareg, forcenew=forcenewgaia, logger=logger )
    ngaia = len(gaiadf)
    
    # Scamp it up
    
    scampbase = ( f'scamp {tmpcat} -ASTREF_CATALOG FILE -ASTREFCAT_NAME {gaiacat} -ASTRINSTRU_KEY FILTER '
                  f'-SOLVE_PHOTOM N -CHECKPLOT_DEV NULL -CHECKIMAGE_TYPE NONE '
                  f'-PROJECTION_TYPE TPV '
                  f'-WRITE_XML Y -XML_NAME {xmlname} -VERBOSE_TYPE QUIET -CROSSID_RADIUS {crossidrad} ' )
    scampsuccess = False
    for j, brightlim in enumerate( brightlimits ):
        for i, poserr in enumerate( poserrs ):
            syscall = f'{scampbase} -ASTREFMAG_LIMITS {brightlim},{dimlimit} -POSITION_MAXERR {poserr:.2f}'
            try:
                logger.debug( f'Running scamp: {syscall}' )
                res = subprocess.run( syscall, shell=True, capture_output=True, text=True, check=True, timeout=timeout)
                errstr = "Not enough matched detections"
                if ( errstr in res.stdout ) or ( errstr in res.stderr ):
                    logger.info( f'Not enough matched detections for {imagename} '
                                 f'with POSITION_MAXERR={poserr:.2f} and '
                                 f'ASTREFMAG_LIMITS={brightlim},{dimlimit}' )
                else:
                    scampxml = astropy.io.votable.parse( xmlname )
                    scampstat = scampxml.get_table_by_index(1)
                    nmatch = scampstat.array["AstromNDets_Reference"][0]
                    sig0 = scampstat.array["AstromSigma_Reference"][0][0]
                    sig1 = scampstat.array["AstromSigma_Reference"][0][1]
                    infostr = ( f'Scamp with ASTREFMAG_LIMITS {brightlim},23 and POSITION_MAXERR {poserr:.1f} '
                                f'yielded {nmatch} out of {ndata}/{len(gaiadf)} matches with '
                                f'σref = {sig0:.3f}", {sig1:.3f}"' )
                    if (  ( nmatch > fracmatch * min(ndata, ngaia) )
                          and ( nmatch > minnummatch )
                          and ( (sig0 + sig1) / 2. <= maxsig ) ):
                        scampsuccess = True
                    else:
                        infostr += ", which isn't good enough"
                    logger.info(infostr)
            except subprocess.CalledProcessError:
                logger.info( f"Scamp had non-0 exit for {imagename} with POSITION_MAXERR={poserr:.2f} "
                             f"and ASTREFMAG_LIMITS={brightlim},{dimlimt}" )
            except TimeoutError:
                logger.info( f"Scamp timed out for {imagename} with POSITION_MAXERR={poserr:.2f} and "
                             f"ASTREFMAG_LIMITS={brightlim},{dimlimit}" )
            if scampsuccess:
                break
            else:
                if i < len(poserrs)-1:
                    logger.info( f'Retrying scamp for {imagename} with different POSITION_MAXERR' )
        if scampsuccess:
            break
        else:
            if j < len(poserrs)-1:
                logger.info( f'Retrying scamp for {imagename} with different ASTERFMAG_LIMITS' )


    if scampsuccess:
        if savedb:
            save_image_header_to_db_wcs( expsource, basename, loadastromhead=True, curdb=curdb, logger=logger )
            logger.info( f'WCS saved to database for {imagename}' )
            
        if not noupdateheader:
            # Use missfits to copy the header from the .head (or whatever scamp wrote) into image
            # I'm not fond of the misfits interface; "missfits <image>.fits"
            #   by itself will copy <image>.head into the header of
            #   <image>.fits, which to me seems like weird default behavior.
            #   I haven't figured out a set of parameters to tell missfits
            #   to copy <othername>.head into the <image>.fits header... so
            #   I have to do a rename here to make missfits do what I want.
            #   (There's a HEADER_SUFFIX parameter but not a HEADER_FILE or
            #   similar.)
            logger.debug( f"Running missfits to update header of {imagename}" )
            oldhead = imagedir / f'{basename}.astrom.head'
            newhead = imagedir / f'{basename}.head'
            os.rename( oldhead, newhead )
            ####################################### FIXME FIXME FIXME PLEASEEEEE
            # This changes the basename.fits.fz file into a .fits.fz.back file and a .fits.fz.fits file
            # Which SHOULD NOT HAPPEN and I don't know why it does
            #
            # --> missfits doesn't seem to undestand .fz?  There are
            # assumptions throughout the pipeline that fits files end in
            # .fits and not .fits.fz, so we need to do a lot of
            # refactoring if we want to fully support .fz files other
            # than at initial import.
            subprocess.run( f"missfits {imagepath} -WRITE_XML N", shell=True, timeout=60, check=True )
            # Cleanup backup file
            ( imagedir / f'{basename}.fits.back' ).unlink( missing_ok=True )
            ( imagedir / f'{basename}.head' ).unlink( missing_ok=True )
            logger.info( f'Astrometry complete for {imagename}, FITS header updated.' )

        # Cleanup astrom.head file
        ( imagedir / f'{basename}.astrom.head' ).unlink( missing_ok=True )
            
        return True
    else:
        logger.error( f"Failed to solve astrometry adequately with scamp for {imagename}" )        
        return False
                        
# ======================================================================

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )
    
    parser = argparse.ArgumentParser()
    parser.add_argument( "image", help="Path to image" )
    parser.add_argument( "-j", "--just-filename", default=False, action='store_true',
                         help="Actually, image is just the filename; find it.  Requires -e" )
    parser.add_argument( "-s", "--save-wcs", default=False, action='store_true',
                         help="Save WCS to database.  Requires -e" )
    parser.add_argument( "-e", "--exposure-source", default=None,
                         help="Exposure Source (e.g. ZTF)" )
    parser.add_argument( "-v", "--verbose", default=False, action='store_true',
                         help="Show debugging messages" )
    parser.add_argument( "-n", "--no-header-update", default=False, action='store_true',
                         help="Don't update image header (just write .head file)" )
    parser.add_argument( "-c", "--catalog-redo", default=False, action='store_true',
                         help="Rebuild .cat file if it exists" )
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel( logging.DEBUG )

    if ( args.just_filename or args.save_wcs ) and ( args.exposure_source == None ):
        raise RuntimeError( f'-s and -j require -e' )

    if args.just_filename or args.save_wcs or ( args.exposure_source is not None ):
        config.Config.init( None )
        curdb = db.DB.get()
    else:
        curdb = None
    if args.exposure_source is None:
        expsource = None
    else:
        expsource = ExposureSource.get( args.exposure_source )
    if args.just_filename:
        imagepath = expsource.local_path( args.image )
    else:
        imagepath = pathlib.Path( args.image ).resolve()
    imagename = imagepath.name

    cat = None
    if not args.catalog_redo:
        match = re.search( '^(.*)\.fits$', imagename )
        if match is None:
            raise ValueError( f'Failed to parse {imagename} for *.fits' )
        basename = match.group(1)
        catpath = imagepath.parent / f'{basename}.cat'
        if catpath.exists():
            if not catpath.is_file():
                raise RuntimeError( f'{catpath} exists but is not a file!' )
            cat = catpath
    logger.debug( f'cat is {cat}' )

    astrom_calib( imagepath, expsource=expsource, cat=cat, noupdateheader=args.no_header_update,
                  savedb=args.save_wcs, logger=logger, curdb=curdb )
        
    

# ======================================================================

if __name__ == "__main__":
    main()
