import sys
import argparse

from astropy.io import fits

def main():
    parser = argparse.ArgumentParser( description="Simple arithmetic on FITS images",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( "firstfile", help="FITS image on left side of operation" )
    parser.add_argument( "operation", help="One of +, *, -, or /; you probably have to use shell escapes for *" )
    parser.add_argument( "secondfile", help="FITS image on right side of operation" )
    parser.add_argument( "outfile", help="Output FITS image" )
    args = parser.parse_args()

    if args.operation not in [ '+', '-', '*', '/' ]:
        raise ValueError( "Operation must be one of + - * /" )
    
    with fits.open( args.firstfile, memmap=False ) as left, fits.open( args.secondfile, memmap=False ) as right:
        if left[0].data.shape != right[0].data.shape:
            raise RuntimeError( "Shape of input images must match." )
        data = left[0].data
        header = left[0].header
        if args.operation == '+':
            data += right[0].data
        elif args.operation == '-':
            data -= right[0].data
        elif args.operation == '*':
            data *= right[0].data
        elif args.operation == '/':
            data *= right[0].data
        else:
            raise RuntimeError( "This should never happen" )

    header['COMMENT'] = f'{args.firstfile} {args.operation} {args.secondfile}'
    fits.writeto( args.outfile, data, header )

# ======================================================================

if __name__ == "__main__":
    main()
