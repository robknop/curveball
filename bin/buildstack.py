#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os
import time
import argparse
import logging
import re
import uuid
import shutil
import pathlib
import subprocess
import numpy as np
from mpi4py import MPI
from astropy.io import fits
from astropy.wcs import WCS
from bijaouisky import estimate_single_sky, estimate_smooth_sky
from processing import make_weight,extract_catalog
from exposuresource import ExposureSource

_fitsparse = re.compile( '^(.*)\.fits$' )

# ======================================================================

def buildstack( expsource, stack, stackimages, stackweights, stackbpms, stackcats, tiny=1e-10,
                bgsub=False, bgorder=2, bgcutshort=10, bgsigcut=3, bgbpmgrow=3, bgnbins=None,
                swarpbg=True, swarpbgsize=256, center=None, trim=None, timeout=1200, tmpdir=None,
                hdrkeywords=None,
                logger=logging.getLogger("main"), comm=None ):
    """Build an image stack
    
       expsource : an ExposureSource object
       stack : path to output stack image
       stackimages : list of paths of input stack images; can't be .fz, must be .fits
       stackweights : list of paths of input 1/variance images
       stackbpms : list of paths of input bad pixel masks (0 good, >0 bad)
       stackcats : list of paths of SCAMP-ready stack catalog files, or [] or None
       tiny : cutoff in weight image to mask pixel (things below this get masked)
   
       bgsub : False if the images are already background subtrated, True if we still need to subtract
       swarpbg : If bgsub is True, and this is True, let swarp bg subtract, else use bijaouisky
       swarpbgsize : BACK_SIZE swarp background mesh size parameter
    Parameters for bijaouisky:
       bgorder : order of polynomial fit to bg in bijaouisky.estimate_smooth_sky (default: 2)
       bgcutshort : Divide the shorter axis into this many tiles (default: 10)
       bigsigcut : iteratively fit a histogram that has bins within this many currently-estimated σ
                    of the mean pixel value (default: 4)
       bgbpmgrow : expand bad pixel mask around bad pixels by this for purposes of sky estimation
       bgnbins : 

       center : either ALL, MOST, or a 2-element sequence.  If ALL or
                MOST, passes that value to swarp.  If a 2-elements
                sequence, uses them as ra and dec (both degrees) and
                passes MANUAL as centering to swarp.  Defaults to MOST.

       trim : 2-elements sequence; trim image size x, y (FITS
              coordinates, which are backwards from numpy coordinates).
              Default: passes IMAGE_SIZE 0 to swarp, which does something automatically
    
       timeout : timeout (sec) for scamp and swarp

       tmpdir : A tmpdir for swarp to work in (default: will make one under /tmp)

       hdrkeywords : String: comma-separated (no spaces!) list of header
                     keywords to copy from the first (?) image to the
                     stack image.  (Default: uses expsource to figure
                     that out.)

       comm : MPI communicator that includes all the process that may
               work together on this.  (currently only used in
               bijaouisky subtraction).  None means singled-process.
               NOTE: there is a built-in assumption that all of these
               are on the same node.  Some parts of the process will
               (try to) use OpenMP in a single rank with a number of
               threads equal to the size of this communicator.

    

    Builds the stack.  Outputs to file named in stack.

    ROB TODO:
    [[[ NOTE: If I understand swarp correctly (doing a clipped mean), it ]]]
    [[[ scales the output image so that it should have the same zeropoint as ]]]
    [[[ the first input image.  (Indeed, it implicitly assumes the zeropoint ]]]
    [[[ is the same for all input images.)  For this reason, I am just ]]]
    [[[ copying the SATURATEA and SATURATEB keywords from the first input to ]]]
    [[[ the output.  They aren't reliable!  It's easy to imagine a case ]]]
    [[[ where a pixel is lower than this value but still bad (saturated in ]]]
    [[[ some images, not in others).  However, makesub.py reads in those ]]]
    [[[ header values to give hotpants a "maximum valid pixel value", so ]]]
    [[[ something has to be there.  hotpants also reads mask images, and the ]]]
    [[[ mask images are what should really be used to determine if a pixel ]]]
    [[[ is good or not. ]]]

    """

    stack = pathlib.Path( stack )
    stackimages = [ pathlib.Path(i) for i in stackimages ]
    if stackweights is not None:
        stackweights = [ pathlib.Path(w) for w in stackweights ]
    if stackbpms is not None:
        stackbpms = [ pathlib.Path(b) for b in stackbpms ]
    if stackcats is not None:
        stackcats = [ pathlib.Path(c) for c in stackcats ]
        
    if comm is None:
        rank = 0
        size = 1
    else:
        rank = comm.Get_rank()
        size = comm.Get_size()

    basenamelist = []
    zps = []
    for image in stackimages:
        if rank == 0:
            match = _fitsparse.search( image.name )
            if match is None:
                logger.warning( f'Failed to parse {image.name} for *.fits; '
                                f'using full name as base for other filenames.' )
                basenamelist.append( image.name )
            else:
                basenamelist.append( match.group(1) )
            with fits.open( image, memmap=False ) as hdu:
               zp = hdu[0].header['MAGZP']
               zps.append( zp )
        if size > 1:
            basenamelist = comm.bcast( basenamelist, root=0 )
            zps = comm.bcast( zps, root=0 )

    if ( stackweights is None ) or ( len(stackweights) == 0 ):
        if rank == 0:
            # Really ought to parallelize this, huh.
            if not bgsub:
                addtoskys = []
                for image in stackimages:
                    with fits.open( image, memmap=False ) as hdu:
                        addtoskys.append( float( hdu[0].header['MEDSKY'] ) )
            stackweights = []
            for image, mask, addtosky in zip(stackimages, stackbpms, addtoskys):
                weight = make_weight( expsource, image, mask, overwrite=True, addtosky=addtosky, logger=logger )
                stackweights.append( weight )
        if size > 1:
            stackweights = comm.bcast( stackweights, root=0 )

    if ( stackcats is None ) or ( len(stackcats) == 0 ):
        if rank == 0:
            # Another one I should run in multiple processes
            stackcats = []
            for basename, image, weight, mask, zp in zip( basenamelist, stackimages, stackweights, stackbpms, zps ):
                catname = image.parent / f'{basename}.cat'
                extract_catalog( expsource, image, weight, catname, mask,
                                 bgsub=bgsub, backsize=swarpbgsize, zp=zp, logger=logger )
                stackcats.append( catname )
        if size > 1:
            stackcats = comm.bcast( stackcats, root=0 )

    if ( ( len(stackimages) != len(stackweights) )
         or ( len(stackimages) != len(stackbpms) )
         or ( len(stackimages) != len(stackcats) ) ):
        logger.error( f'len(stackimages)={len(stackimages)}, '
                      f'len(stackweights)={len(stackweights)}, '
                      f'len(stackbpms)={len(stackbpms)}, '
                      f'len(stackcats)={len(stackcats)}' )
        raise ValueError( "stackimages, stackweights, stackbpms, and stackcats must all have the same length" )
    
    match = _fitsparse.search( stack.name )
    if match is None:
        raise ValueError( f'Error parsing output stack name {stack}' )
    stackbasename = match.group(1)
    outputweight = stack.parent / f'{stackbasename}.weight.fits'
    outputmask = stack.parent / f'{stackbasename}.mask.fits'
    
    # Run scamp to align all images with the first one in the list.  This will create a bunch
    #  of .head files
    
    if rank == 0:
        scampcmd = ( f'scamp -ASTREF_CATALOG FILE -ASTREFCAT_NAME {stackcats[0]} -ASTR_ACCURACY 0.2 '
                     f' -SOLVE_PHOTOM N -CHECKPLOT_DEV NULL -WRITE_XML N -VERBOSE_TYPE NORMAL '
                     f'{" ".join([ str(i) for i in stackcats[1:]])}' )
        logger.info( f'Running scamp on to align {len(stackcats)} images for stacking.' )
        subprocess.run( scampcmd, shell=True, timeout=timeout )
        logger.info( f'Scamp complete.' )
    if size > 1:
        comm.Barrier()

    # Exceptions in MPI are troublesome.
    # Should I raise it in all ranks?  That dumps LOTS
    #   of error messages, but does avoid deadlocks.
    # Running with -m mpi4py is a quick way to deal with this.

    if rank == 0:
        if center is None:
            center = "MOST"
        elif len(center) == 2:
            centerra = center[0]
            centderdec = center[1]
            center = "MANUAL"
        elif ( center != 'MOST' ) and ( center != 'ALL' ):
            raise ValueError( f'Unknown value of center: {center}' )
        if size > 1:
            center = comm.bcast( center, root=0 )
            centerra = comm.bcast( centerra, root=0 )
            centerdec = comm.bcast( centerdec, root=0 )

    if trim is not None:
        if rank == 0:
            if len(trim) != 2:
                raise ValueError( f'trim needs 2 values' )
        
        # If trimming, figure out the average RA/Dec to use as the center
        # if rank == 0:
        #     logger.debug( f'Gonna read header from {stackimages[0]}' )
        #     hdr = fits.Header.fromfile( stackimages[0] )
        #     wcs = WCS(hdr)
        #     radecs = [ wcs.calc_footprint().mean(0) ]
        #     for cat in stackcats[1:]:
        #         logger.debug( f'Gonna read header from {str(cat).replace(".cat",".head")}' )
        #         # Scamp writes some non-ascii latin-1 characers (é) to the .head file;
        #         # fits.Header.fromfile barfs at this.  So, wrangle around this awkwardly.
        #         # (the docs say fits.Header.fromstring can read latin-1 characters.)
        #         with open( str(cat).replace(".cat", ".head"), "rb" ) as ifp:
        #             hdrstr = ifp.read()
        #         hdr = fits.Header.fromstring( hdrstr, sep='\n' )
        #         logger.debug( f'About to make WCS' )
        #         # # ***** HACK ALERT
        #         # # WCS is barfing on the header that I just read because it has non-ascii
        #         # # characters.  (Scamp, sigh, I realize that not supporting é is American
        #         # # anti-French imperialism, but could you have just obeyed the standard?)
        #         # # Gotta remove the first two lines of the header.
        #         # # Also have to hack in the axis size for our purposes.
        #         # hdr = hdr[2:]
        #         # hdr['NAXIS'] = 2
        #         # hdr['NAXIS1'] = ROB FIGURE IT OUT
        #         # hdr['NAXIS2'] = ROB FIGURE IT OUT
        #         wcs = WCS( hdr )
        #         radecs.append( wcs.calc_footprint().mean(0) )
        #     meanradec = np.array( radecs ).mean(0)
        #     centerra = meanradec[0]
        #     centerdec = meanradec[1]
        # else:
        #     centerra = None
        #     centerdec = None
        # if size > 1:
        #     centerra = comm.bcast( centerra, root=0 )
        #     centerdec = comm.bcast( centerdec, root=0 )
        
    if bgsub and not swarpbg:
        logger.exception( f'bijaouisky is broken right now.  ROB: deal with paths and such!' )
        logger.warning( f'bijaouisky subtraction isn\'t tested right now!!!!' )
        imagelist = []
        for basename, image, weight, bpm in zip( basenamelist, stackimages, stackweights, stackbpms ):
            bgname = pathlib.Path( f'{basename}.stackbgsub.fits' )
            headname = pathlib.Path( f'{basename}.head' )
            bgheadname = pathlib.Path( f'{basename}.stackbgsub.head' )
            imagelist.append( str(bgname) )
            
            if rank == 0:
                logger.info( f'Subtracting background from {image}' )
                with fits.open( image, memmap=False ) as imhdu:
                    imhdr = imhdu[0].header
                    imdata = imhdu[0].data
                with fits.open( bpm, memmap=False ) as bpmhdu:
                    bpmdata = bpmhdu[0].data
            else:
                imhdr = None
                imdata = None
                bpmdata = None
            if bgorder == 0:
                if rank == 0:
                    a, σ, s = estimate_single_sky( imdata, bpmdata, sigcut=bgsigcut,
                                                   bpmgrow=bgbpmgrow, nbins=bpnbins,
                                                   sigconvfrac=0.01, logger=logger )
                    logger.info( f'Constant sky for {os.path.basename(image)}: s={s:.1f}, σ={σ:.2f}, a={a:.2f}' )
                    imdata -= s
            else:
                if size > 0:
                    imdata = comm.bcast( imdata, root=0 )
                    bpmdata = comm.bcast( bpmdata, root=0 )
                sky, a, σ, s = estimate_smooth_sky( imdata, bpmdata, comm=comm,
                                                    order=bgorder, cutshort=bgcutshort,
                                                    sigcut=bgsigcut, bpmgrow=bgbpmgrow,
                                                    nbins=bgnbins, logger=logger )
                if rank == 0:
                    logger.info( f'Sky order {bgorder} for {os.path.basename(image)}: '
                                 f's={s:.1f}, σ={σ:.2f}, a={a:.2f}' )
                    imdata -= sky
            if rank == 0:
                imhdr.add_history( f'buildstack sky order {bgorder} s=~{s:.1f}' )    
                hdu = fits.PrimaryHDU( data=imdata, header=imhdr )
                hdu.writeto( bgname, overwrite=True )
                if headname.is_file():
                    if bgheadname.is_file():
                        bgheadname.unlink()
                    logger.debug( f'Copying {headname} to {bgheadname}' )
                    shutil.copyfile( headname, bgheadname )
    else:
        imagelist = stackimages
                
    # The scamp run should have created a whole bunch of .head files that swarp will "just read".

    if rank == 0:
        if tmpdir is None:
            tmpdir = f'/tmp/{uuid.uuid4().hex}'
        pathlib.Path( tmpdir ).mkdir( parents=True, exist_ok=True )
        pathlib.Path( stack.parent ).mkdir( parents=True, exist_ok=True )
        centerstr = f"-CENTER_TYPE {center} "
        if center == "MANUAL":
            centerstr += "-CENTER {centerra},{centerdec} "
        trimstr = ""
        if trim:
            trimstr = f"-IMAGE_SIZE {trim[0]},{trim[1]} "
        if bgsub and swarpbg:
            bgparam = f"-SUBTRACT_BACK y -BACK_TYPE AUTO -BACK_SIZE {swarpbgsize} "
        else:
            bgparam = f"-SUBTRACT_BACK N -BACK_TYPE MANUAL -BACK_DEFAULT 0. "
        if hdrkeywords is None:
            hdrkeywords = expsource.key_keywords
        swarpcmd = ( f'swarp {bgparam} '
                     f'-COMBINE_TYPE CLIPPED -IMAGEOUT_NAME {stack} -MEM_MAX 2048 '
                     f'-PROJECTION_TYPE TAN {centerstr} {trimstr} '
                     f'-NTHREADS {size} -RESAMPLE_DIR {tmpdir} -VMEM_DIR {tmpdir} '
                     f'-WEIGHT_IMAGE {",".join([str(s) for s in stackweights])} '
                     f'-WEIGHT_TYPE MAP_WEIGHT -WEIGHT_THRESH {tiny} '
                     f'-WEIGHTOUT_NAME {outputweight} -WRITE_FILEINFO Y '
                     f'-VERBOSE_TYPE LOG -COPY_KEYWORDS {hdrkeywords} '
                     f'{" ".join([str(i) for i in imagelist])}' )
        logger.info( f'Running swarp to coadd {len(imagelist)} images.' )
        logger.info( f'Swarp command: {swarpcmd}' )
        subprocess.run( swarpcmd, shell=True, timeout=timeout )
        logger.info( 'swarp complete' )

        # Create mask based on the weight image
        with fits.open( outputweight, memmap=False ) as weighthdu:
            # Use np.int16, *not* np.uint16.  The latter seems more reasonable
            #  for a mask, but interacts poorly with the FITS datatype system
            #  and with assumptions made by sextractor.
            # (Maybe I should be using int8?)
            maskdata = np.zeros_like( weighthdu[0].data, dtype=np.int16 )
            w = np.where( weighthdu[0].data < tiny )
            maskdata[w] = 1
            maskhdr = fits.Header( weighthdu[0].header, copy=True )
            maskhdr.add_history( f'Stack mask=1 for all weight=0 pixels' )
            maskhdu = fits.PrimaryHDU( data=maskdata, header=maskhdr )
            maskhdu.writeto( outputmask, overwrite=True )

            # noisedata = np.array( weighthdu[0].data )
            # noisedata[ noisedata <= tiny ] = tiny
            # noisedata = np.sqrt( 1./noisedata )
            # noisehdr = fits.Header( weighthdu[0].header, copy=True )
            # noisehdr.add_history( f'noise built from sqrt(1/swarp weight)' )
            # noisehdu = fits.PrimaryHDU( data=noisedata, header=noisehdr )
            # noisehdu.writeto( outputnoise, overwrite=True )
            
    if size > 1:
        comm.Barrier()
    
# ======================================================================

def _list_or_listfile( arg ):
    if len(arg) == 1 and arg[0] == '@':
        with open( arg[1:], "r" ) as ifp:
            vals = ifp.readlines()
        vals = [ strip(i) for i in vals ]
        vals = [ i for i in vals if len(i) > 0 ]
    else:
        vals = arg
    return vals

# ======================================================================        

def main():
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()

    if rank != 0:
        args = None
        images = None
        weights = None
        masks = None
        cats = None
    else:
        parser = argparse.ArgumentParser( description="Coadd a bunch of images into a stack; "
                                          "note that it assumes exposure times and depths are all the same!!!" )
        parser.add_argument( 'images', nargs='*', default=[],
                             help="image filenames to coadd; @filename for filename with list of images" )
        parser.add_argument( '-e', '--expsource', requred=True,
                             help='Primary name of exposure source' )
        parser.add_argument( '--expsource-sec', default=None,
                             help='Secondary name of exposure source if relevant' )
        parser.add_argument( '-w', '--weights', nargs='*', default=[],
                             help="weight (1/variance) images (or @filename)" )
        parser.add_argument( '-m', '--masks', nargs='*', default=[],
                             help="mask images (or @filename)" )
        parser.add_argument( '-c', '--cats', nargs='*', default=[],
                             help="SCAMP-ready catalog files (or @filename); default: will run sextractor." )
        parser.add_argument( '-b', '--bgsub', action='store_true', default=False,
                             help="Should we background subtract?" )
        parser.add_argument( '-s', '--swarp-bgsub', action='store_true', default=False,
                             help="Should we let swarp background subtract? (Default: bijaouisky)" )
        parser.add_argument( '-o', '--output-stack', default=None,
                             help="Name of output stack file; must end in .fits" )
        parser.add_argument( '-t', '--trim', nargs='2', type=int, default=None,
                             help="Trim image to x y (FITS coords) (default: don't)." )
        parser.add_argument( '-v', '--verbose', action='store_true', default=False,
                             help="Set log level to INFO (default: WARNING)" )
        parser.add_argument( '-vv', '--debug', action='store_true', default=False,
                             help="Set log level to DEBUG, supercedes --verbose" )
        args = parser.parse_args()

        images = _list_or_listfile( args.images )
        weights = _list_or_listfile( args.weights )
        masks = _list_or_listfile( args.masks )
        cats = _list_or_listfile( args.cats )

        if len(images)<2:
            raise ValueError( "Must have at least 2 images to coadd!" )
        if ( len(weights) != len(images) ) or ( len(masks) != len(images) ):
            raise ValueError( "Must specify same number of images, weights, masks." )
        if ( len(cats) != 0 ) and ( len(cats) != len(images) ):
            raise ValueError( "Must either specify no catalogs, or same number as images" )

    args = comm.bcast( args, root=0 )
    images = comm.bcast( images, root=0 )
    weights = comm.bcast( weights, root=0 )
    masks = comm.bcast( masks, root=0 )
    cats = comm.bcast( cats, root=0 )

    logger = logging.getLogger( "main" )
    logout = logging.StreamHandler( sys.stdout )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - {rank:03d} - %(levelname)s] - %(message)s' ) )
    if args.debug:
        logger.setLevel( logging.DEBUG )
    elif args.verbose:
        logger.setLevel( logging.INFO )
    else:
        logger.setLevel( logging.WARNING )

    expsource = ExposureSource.get( args.expsource, args.expsource_sec )

    buildstack( expsource, args.output_stack, images, weights, masks, cats,
                bgsub=args.bgsub, swarpbg=args.swarpbg, trim=args.trim,
                comm=comm, logger=logger )

# ======================================================================

if __name__ == "__main__":
    main()
