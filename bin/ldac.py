# Copyright 2015 Fred Moolekamp
# BSD 3-clause license
# Modified by RKNOP in 2021 to add imghdr
"""
Functions to convert FITS files or astropy Tables to FITS_LDAC files and
vice versa.
"""


def convert_hdu_to_ldac(hdu, imghdr=None):
    """Convert an hdu table to a fits_ldac table (format used by astromatic suite)

    Parameters
    ----------
    hdu: `astropy.io.fits.BinTableHDU` or `astropy.io.fits.TableHDU`
        HDUList to convert to fits_ldac HDUList

    Returns
    -------
    tbl1: `astropy.io.fits.BinTableHDU`
        Header info for fits table (LDAC_IMHEAD)
    tbl2: `astropy.io.fits.BinTableHDU`
        Data table (LDAC_OBJECTS)

    NOTE!  Because of how it constructs tbl1, calling writeto on an
    HDULIST constructed from these tables will fail if you don't also
    pass output_verify='ignore'
    """
    from astropy.io import fits
    import numpy as np

    if imghdr is None:
        imghdr = hdu.header
    imghdrstr = imghdr.tostring()
    imghdrstrarr = np.array( [ imghdrstr[i:i+80] for i in range(0, len(imghdrstr), 80) ] )
    # import pdb; pdb.set_trace()
    # col = fits.Column(name="Field Header Card", format=f'{len(imghdrstr)}A',
    #                   dim=f'(80, {len(imghdrstrarr)})', array=np.array([imghdrstrarr]))
    # cols = fits.ColDefs([col])
    # tbl1 = fits.BinTableHDU.from_columns(cols)
    # tbl1.header["EXTNAME"] = "LDAC_IMHEAD"

    # ****
    # I failed trying to do it right.  The problem was that the "Column"
    # definition above was stripping blank spaces from the end of each
    # line in the hdrstrarr.  They got replaced with 0 bytes in the
    # table data, which was *NOT* what I wanted.  I haven't figured out
    # how to tell Column *not* to strip training spaces from its
    # strings.  (Among other things, it made the FITS reader in scamp
    # not work.  One might blame scamp for using a FITS header routine
    # to read binary table data, but I need this to work.)  Looking at
    # the astropy code, it looks like fits.Column will convert it to a
    # numpy chararay, which strips whitespace, irritatingly.  I don't
    # think there's a way to work around this.  So, just do it manually;
    # this also requires adding output_verify='ignore' to the .writeto
    # call in save_table_as_ldac, because astropy FITS gets all pissy
    # when I did the thing that it documented that I could do.
    hdrfields = [ ( 'XTENSION', 'BINTABLE', True ),
                  ( 'BITPIX', 8, False ),
                  ( 'NAXIS', 2, False ),
                  ( 'NAXIS1', len(imghdrstr), False ),
                  ( 'NAXIS2', 1, False ),
                  ( 'PCOUNT', 0, False ),
                  ( 'GCOUNT', 1, False ),
                  ( 'TFIELDS', 1, False ),
                  ( 'TTYPE1', 'Field Header Card', True ),
                  ( 'TFORM1', f'{len(imghdrstr)}A  ', True ),
                  ( 'TDIM1', f'(80, {len(imghdrstrarr)})', True ),
                  ( 'EXTNAME', 'LDAC_IMHEAD', True ),
    ]
    hdr = ""
    for field in hdrfields:
        hdrline = f'{field[0]:<8s}= '
        if field[2]:
            hdrline += f'\'{field[1]}\''
        else:
            hdrline += f'{field[1]:>20n}'
        hdrline += " "*80
        hdrline = hdrline[0:80]
        hdr += hdrline
    hdr += f'{"END":<80s}'
    hdr += " " * ( 2880 - ( len(hdr) % 2880 ) )
    imghdrstr += " " * ( 2880 - ( len(imghdrstr) % 2880 ) )
    # Lord help us if there are non-ASCII characters somewhere in the
    #  image header.  (I bet the FITS standard doesn't allow that....
    #  The FITS standard predates unicode by a millenium or two.  The
    #  very fact that they call header records "cards" tells you
    #  something.)
    tbl1 = fits.BinTableHDU.fromstring( hdr.encode('ascii') + imghdrstr.encode('ascii') )
    # ****

    tbl2 = fits.BinTableHDU(hdu.data)
    tbl2.header["EXTNAME"] = "LDAC_OBJECTS"
    return (tbl1, tbl2)


def convert_table_to_ldac(tbl, imghdr=None):
    """
    Convert an astropy table to a fits_ldac

    Parameters
    ----------
    tbl: `astropy.table.Table`
        Table to convert to ldac format
    imghdr: `astropy.io.fits.Header`
        Optional: image header data to store in the LDAC
    Returns
    -------
    hdulist: `astropy.io.fits.HDUList`
        FITS_LDAC hdulist that can be read by astromatic software
    """
    from astropy.io import fits
    import tempfile

    f = tempfile.NamedTemporaryFile(suffix=".fits", mode="rb+")
    tbl.write(f, format="fits")
    f.seek(0)
    hdulist = fits.open(f, mode="update", memmap=False)
    tbl1, tbl2 = convert_hdu_to_ldac(hdulist[1], imghdr=imghdr)
    new_hdulist = [hdulist[0], tbl1, tbl2]
    new_hdulist = fits.HDUList(new_hdulist)
    return new_hdulist


def save_table_as_ldac(tbl, filename, imghdr=None, **kwargs):
    """
    Save a table as a fits LDAC file

    Parameters
    ----------
    tbl: `astropy.table.Table`
        Table to save
    filename: str
        Filename to save table
    imghdr: `astropy.io.fits.Header`
        Optional: image header data to store in the LDAC
    kwargs:
        Keyword arguments to pass to hdulist.writeto
    """
    hdulist = convert_table_to_ldac(tbl, imghdr=imghdr)
    hdulist.writeto(filename, output_verify='ignore', **kwargs)


def get_table_from_ldac(filename, frame=1):
    """
    Load an astropy table from a fits_ldac by frame (Since the ldac format has column
    info for odd tables, giving it twce as many tables as a regular fits BinTableHDU,
    match the frame of a table to its corresponding frame in the ldac file).

    Parameters
    ----------
    filename: str
        Name of the file to open
    frame: int
        Number of the frame in a regular fits file
    """
    from astropy.table import Table

    if frame > 0:
        frame = frame * 2
    tbl = Table.read(filename, hdu=frame)
    return tbl
