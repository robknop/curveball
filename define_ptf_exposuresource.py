import db
import config

def main( curdb=None ):
    with db.DB.get(curdb) as dbo:
        ptf = db.ExposureSource( sourcetype=2, nx=2048, ny=4096, orientation=2,
                                 pixscale=1.01, preprocessed=True, skysub=False, name='PTF' )
        dbo.db.add( ptf )
        dbo.db.commit()

        g = db.Band( exposuresource_id=ptf.id, name='PTF_G', filtercode='pg', description='PTF G', sortdex=1000 )
        r = db.Band( exposuresource_id=ptf.id, name='PTF_R', filtercode='pr', description='PTF R', sortdex=2000 )
        dbo.db.add( g )
        dbo.db.add( r )

        # TODO -- figure out real offsets
        ptfchips = [ { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '00'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '01'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '02'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": False,
                       "chiptag": '03'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '04'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '05'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '06'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '07'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '08'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '09'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '10'
                      },
                     { "raoff": 0.,
                       "decoff": 0.,
                       "ra0off": 0.,
                       "dec0off": 0.,
                       "ra1off": 0.,
                       "dec1off": 0.,
                       "ra2off": 0.,
                       "dec2off": 0.,
                       "ra3off": 0.,
                       "dec3off": 0.,
                       "isgood": True,
                       "chiptag": '11'
                      },
                    ]

        for chip in ptfchips:
            chip[ 'exposuresource_id' ] = ptf.id
            chipobj = db.CameraChip( **chip )
            dbo.db.add( chipobj )

        dbo.db.commit()

# ======================================================================

if __name__ == "__main__":
    config.Config.init()
    main()
