CREATE EXTENSION IF NOT EXISTS q3c WITH SCHEMA public;

--- ======================================================================

CREATE TABLE exposuresource (
   id INT PRIMARY KEY,
   sourcetype INT,
   nx INT DEFAULT 0,
   ny INT DEFAULT 0,
   orientation INT DEFAULT 0,
   pixscale REAL,
   preprocessed BOOLEAN DEFAULT FALSE,
   skysub BOOLEAN DEFAULT FALSE,
   name TEXT DEFAULT NULL,
   secondary TEXT DEFAULT NULL
   );

COMMENT ON COLUMN exposuresource.sourcetype IS
  '0: unknown, 1: telescope (sec)/camera (pri), 2: survey download';
COMMENT ON COLUMN exposuresource.orientation IS
  '0=Nup Eleft, 1=Nright Eup, 2=Ndown Eright, 3=Nleft Edown, 4=Nup Eright, 5=Nright Edown, 6=Ndown Eleft, 7=Nleft Eup';
COMMENT ON COLUMN exposuresource.preprocessed IS
  'Do exposures come from the source already preprocessed?';
COMMENT ON COLUMN exposuresource.skysub IS
  'Do exposures come from the source already sky-subtracted?';
COMMENT ON COLUMN exposuresource.pixscale IS
  'Arcseconds/pixel, or 0 if unknown / not consistent';

CREATE SEQUENCE exposuresource_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE exposuresource_id_seq OWNED BY exposuresource.id;
ALTER TABLE ONLY exposuresource ALTER COLUMN id SET DEFAULT nextval('exposuresource_id_seq'::regclass);

--- ======================================================================

CREATE TABLE camerachip (
  id INT PRIMARY KEY,
  exposuresource_id INT,
  raoff REAL,
  decoff REAL,
  ra0off REAL,
  dec0off REAL,
  ra1off REAL,
  dec1off REAL,
  ra2off REAL,
  dec2off REAL,
  ra3off REAL,
  dec3off REAL,
  isgood BOOLEAN DEFAULT TRUE,
  chiptag TEXT
);

COMMENT ON COLUMN camerachip.raoff IS 'RA offset from telescope of chip center';
COMMENT ON COLUMN camerachip.decoff IS 'Dec offset from telescope of chip center';
COMMENT ON COLUMN camerachip.ra0off IS 'RA offset from telescope RA of SE corner';
COMMENT ON COLUMN camerachip.dec0off IS 'Dec offset from telescope Dec of SE corner';
COMMENT ON COLUMN camerachip.ra1off IS 'RA offset from telescope RA of NE corner';
COMMENT ON COLUMN camerachip.dec1off IS 'Dec offset from telescope Dec of NE corner';
COMMENT ON COLUMN camerachip.ra2off IS 'RA offset from telescope RA of NW corner';
COMMENT ON COLUMN camerachip.dec2off IS 'Dec offset from telescope Dec of NW corner';
COMMENT ON COLUMN camerachip.ra3off IS 'RA offset from telescope RA of SW corner';
COMMENT ON COLUMN camerachip.dec3off IS 'Dec offset from telescope Dec of SW corner';

CREATE INDEX ix_camerachip_exposuresource ON camerachip USING btree (exposuresource_id);
ALTER TABLE ONLY camerachip
  ADD CONSTRAINT camerachip_exposuersource_fkey
  FOREIGN KEY (exposuresource_id)
  REFERENCES exposuresource(id)
  ON DELETE CASCADE;

CREATE SEQUENCE camerachip_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE camerachip_id_seq OWNED BY camerachip.id;
ALTER TABLE ONLY camerachip ALTER COLUMN id SET DEFAULT nextval('camerachip_id_seq'::regclass);

--- ======================================================================

CREATE TABLE band (
  id INT PRIMARY KEY NOT NULL,
  closeststd INT,
  sortdex INT DEFAULT 2147483647;
  name TEXT UNIQUE,
  filtercode TEXT,
  description TEXT
);

COMMENT ON COLUMN band.name IS 'Standard name of filter (as in FITS header)';
COMMENT ON COLUMN band.filtercode IS 'Abbreviated filter code used in survey datbase searches';

CREATE INDEX ix_band_closeststd ON band USING btree (closeststd);
ALTER TABLE ONLY band
  ADD CONSTRAINT band_closeststd_fkey
  FOREIGN KEY (closeststd)
  REFERENCES band(id);

CREATE SEQUENCE band_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE band_id_seq OWNED BY band.id;
ALTER TABLE ONLY band ALTER COLUMN id SET DEFAULT nextval('band_id_seq'::regclass);

--- ======================================================================

CREATE TABLE exposure (
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  modified TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  mjd DOUBLE PRECISION,
  ra REAL,
  dec REAL,
  gallat REAL,
  gallong REAL,
  id INT PRIMARY KEY,
  exposuresource_id INT,
  band_id INT,
  basename TEXT UNIQUE,
  fieldname TEXT,
  collection TEXT,
  header JSONB
);

COMMENT ON COLUMN exposure.collection IS 'Proposal ID / Project / Survey / etc. to which exposure belongs';
COMMENT ON COLUMN exposure.fieldname IS 'Filename without directory, chip number, or .fits';
COMMENT ON COLUMN exposure.header IS 'Original exposure header without COMMENT or HISTORY';

CREATE INDEX ix_exposure_exposuresource ON exposure USING btree (exposuresource_id);
ALTER TABLE ONLY exposure
  ADD CONSTRAINT exposure_exposuresource_fkey
  FOREIGN KEY (exposuresource_id)
  REFERENCES exposuresource(id);
CREATE INDEX ix_exposure_band ON exposure USING btree (band_id);
ALTER TABLE ONLY exposure
  ADD CONSTRAINT exposure_band_fkey
  FOREIGN KEY (band_id)
  REFERENCES band(id);

CREATE SEQUENCE exposure_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE exposure_id_seq OWNED BY exposure.id;
ALTER TABLE ONLY exposure ALTER COLUMN id SET DEFAULT nextval('exposure_id_seq'::regclass);

--- ======================================================================

CREATE TABLE image (
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  modified TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  id INT PRIMARY KEY,
  exposure_id INT,
  band_id INT,
  chip_id INT,
  ra REAL,
  dec REAL,
  ra1 REAL,
  dec1 REAL,
  ra2 REAL,
  dec2 REAL,
  ra3 REAL,
  dec3 REAL,
  ra4 REAL,
  dec4 REAL,
  skysig REAL,
  seeing REAL,
  magzp REAL,
  limiting_mag REAL,
  isstack BOOLEAN DEFAULT FALSE,
  isastrom BOOLEAN DEFAULT FALSE,
  isphotom BOOLEAN DEFAULT FALSE,
  isskysub BOOLEAN DEFAULT FALSE,
  basename TEXT UNIQUE,
  header JSONB
);

COMMENT ON TABLE image IS 'Images which have been preprocessed (overscan/bias/flatfield/linearity/etc)';
COMMENT ON COLUMN image.basename IS 'Name of image without directory and without .fits at the end';
COMMENT ON COLUMN image.exposure_id IS 'Can be NULL for stacks';
COMMENT ON COLUMN image.basename IS 'Name with chip but without .fits; can build .weight.fits, etc. from this';
COMMENT ON COLUMN image.seeing IS 'FWHM seeing estimate in arcseconds';
COMMENT ON COLUMN image.limiting_mag IS 'Vague, but probably something like 5-sigma point source';
COMMENT ON COLUMN image.isstack IS 'Is this a stack of other images in the database?';
COMMENT ON COLUMN image.isastrom IS 'True if we believe header WCS is good; also means skysig & seeing should be good';
COMMENT ON COLUMN image.isphotom IS 'True if we believe magzp and limiting_mag are good';
COMMENT ON COLUMN image.seeing IS 'Seeing FWHM in arcseconds';
COMMENT ON COLUMN image.magzp IS 'Full-PFS (or infinite aperture) zeropoint';
COMMENT ON COLUMN image.limiting_mag IS 'Maybe dubious; ideally 5σ sky-limited magnitude limit';
COMMENT ON COLUMN image.header IS
   'Image FITS Header (maybe not with later things from processing) with COMMENT and HISTORY stripped out';

CREATE INDEX ix_image_exposure ON image USING btree (exposure_id);
ALTER TABLE ONLY image
  ADD CONSTRAINT image_exposure_fkey
  FOREIGN KEY (exposure_id)
  REFERENCES exposure(id)
  ON DELETE CASCADE;
CREATE INDEX ix_image_band ON image USING btree (band_id);
ALTER TABLE ONLY image
  ADD CONSTRAINT image_band_fkey
  FOREIGN KEY (band_id)
  REFERENCES band(id);
CREATE INDEX ix_image_chipid ON image USING btree (chip_id);
ALTER TABLE ONLY image
  ADD CONSTRAINT image_chipid_fkey
  FOREIGN KEY (chip_id)
  REFERENCES camerachip(id);

CREATE SEQUENCE image_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE image_id_seq OWNED BY image.id;
ALTER TABLE ONLY image ALTER COLUMN id SET DEFAULT nextval('image_id_seq'::regclass);

--- ======================================================================

CREATE TABLE wcs (
  id INT PRIMARY KEY,
  image_id INT,
  header JSONB
);

CREATE SEQUENCE wcs_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE wcs_id_seq OWNED BY wcs.id;
ALTER TABLE ONLY wcs ALTER COLUMN id SET DEFAULT nextval('wcs_id_seq'::regclass);

CREATE INDEX ix_wcs_image ON wcs USING btree (image_id);
ALTER TABLE ONLY wcs
  ADD CONSTRAINT wcs_image_fkey
  FOREIGN KEY (image_id)
  REFERENCES image(id)
  ON DELETE CASCADE;

--- ======================================================================

CREATE TABLE stackmember (
  weight DOUBLE PRECISION,
  id INT PRIMARY KEY,
  image_id INT,
  member_id INT
);

COMMENT ON COLUMN stackmember.image_id IS 'image id of the stack';
COMMENT ON COLUMN stackmember.member_id is 'image id of image that went into the stack';

CREATE INDEX ix_stackmember_image ON stackmember USING btree (image_id);
ALTER TABLE ONLY stackmember
  ADD CONSTRAINT stackmember_image_fkey
  FOREIGN KEY (image_id)
  REFERENCES image(id)
  ON DELETE CASCADE;
CREATE INDEX ix_stackmember_member ON stackmember USING btree (member_id);
ALTER TABLE ONLY stackmember
  ADD CONSTRAINT stackmember_member_fkey
  FOREIGN KEY (member_id)
  REFERENCES image(id);

CREATE SEQUENCE stackmember_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE stackmember_id_seq OWNED BY stackmember.id;
ALTER TABLE ONLY stackmember ALTER COLUMN id SET DEFAULT nextval('stackmember_id_seq'::regclass);

--- ======================================================================

CREATE TABLE object_classification (
  id INT PRIMARY KEY,
  name TEXT,
  description TEXT
);

COMMENT ON TABLE object_classification IS 'Definitions of classifications to tag to objects';

CREATE SEQUENCE object_classification_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE object_classification_id_seq OWNED BY object_classification.id;
ALTER TABLE ONLY object_classification ALTER COLUMN id SET DEFAULT nextval('object_classification_id_seq'::regclass);

--- ======================================================================

CREATE TABLE object (
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  modified TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  ra DOUBLE PRECISION,
  dec DOUBLE PRECISION,
  t0 DOUBLE PRECISION,
  z DOUBLE PRECISION DEFAULT -9999,
  id INT PRIMARY KEY,
  classification_id INT DEFAULT NULL,
  confidence REAL DEFAULT 0.,
  ignore BOOLEAN DEFAULT FALSE,
  name TEXT,
  othernames TEXT
);

COMMENT ON COLUMN object.confidence IS 'Confidence (0..1) that classification is correct (higher=better)';
COMMENT ON COLUMN object.t0 IS 'Reference time in MJD ; e.g. peak time of SN.';
COMMENT ON COLUMN object.z IS 'Redshift of object, -9999 if unknown';

CREATE INDEX object_q3c_ang2ipix_idx ON object USING btree (public.q3c_ang2ipix(ra,dec));

CREATE SEQUENCE object_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE object_id_seq OWNED BY object.id;
ALTER TABLE ONLY object ALTER COLUMN id SET DEFAULT nextval('object_id_seq'::regclass);

CREATE INDEX ix_object_classification ON object USING btree (classification_id);
ALTER TABLE ONLY object
  ADD CONSTRAINT object_classification_fkey
  FOREIGN KEY (classification_id)
  REFERENCES object_classification(id);

--- ======================================================================

CREATE TABLE object_comment (
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  modified TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  id INT PRIMARY KEY,
  object_id INT,
  commentor TEXT,
  comment TEXT
);

CREATE SEQUENCE object_comment_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE object_comment_id_seq OWNED BY object_comment.id;
ALTER TABLE ONLY object_comment ALTER COLUMN id SET DEFAULT nextval('object_comment_id_seq'::regclass);

CREATE INDEX ix_object_comment_object ON object_comment USING btree(object_id);
ALTER TABLE object_comment
  ADD CONSTRAINT object_comment_object_fkey
  FOREIGN KEY (object_id)
  REFERENCES object(id)
  ON DELETE CASCADE;

--- ======================================================================

CREATE TABLE object_reference (
  id INT PRIMARY KEY,
  object_id INT,
  ra REAL,
  dec REAL,
  image_id INT,
  donotuse BOOLEAN DEFAULT FALSE,
  notes TEXT
);

COMMENT ON TABLE object_reference IS 'A Reference to use for indicated object, or if NULL, ra/dec';

CREATE SEQUENCE object_reference_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE object_reference_id_seq OWNED BY object_reference.id;
ALTER TABLE ONLY object_reference ALTER COLUMN id SET DEFAULT nextval('object_reference_id_seq'::regclass);

CREATE INDEX ix_object_reference_object ON object_reference USING btree(object_id);
ALTER TABLE ONLY object_reference
  ADD CONSTRAINT object_reference_object_fkey
  FOREIGN KEY (object_id)
  REFERENCES object(id)
  ON DELETE CASCADE;
CREATE INDEX ix_object_reference_image ON object_reference USING btree(image_id);
ALTER TABLE ONLY object_reference
  ADD CONSTRAINT object_reference_image_fkey
  FOREIGN KEY (image_id)
  REFERENCES image(id)
  ON DELETE CASCADE;

--- ======================================================================

CREATE TABLE apphot (
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  modified TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  mjd DOUBLE PRECISION,
  fluxadu REAL,
  dfluxadu REAL,
  flux REAL,
  dflux REAL,
  magzp REAL,
  mag REAL,
  dmag REAL,
  aperrad REAL,
  apercor REAL,
  refnorm REAL,
  imagex REAL,
  imagey REAL,
  id INT PRIMARY KEY,
  object_id INT,
  image_id INT,
  reference_id INT,
  new_fits BYTEA,
  ref_fits BYTEA,
  sub_fits BYTEA,
  new_jpeg BYTEA,
  ref_jpeg BYTEA,
  sub_jpeg BYTEA );

COMMENT ON COLUMN apphot.fluxadu IS 'ADU in aperture (not aperture corrected)';
COMMENT ON COLUMN apphot.flux IS 'Jy (for flat f_ν assumption), aperture corrected, i.e. 8.90 zeropoint';
COMMENT ON COLUMN apphot.apercor IS 'Total PSF magnitude minus magnitude in aperture = -2.5log(f_tot/f_ap)';
COMMENT ON COLUMN apphot.magzp IS '-2.5log(flux) + magzp + apercor = AB magnitude';
COMMENT ON COLUMN apphot.aperrad IS 'Aperture radius in pixels';
COMMENT ON COLUMN apphot.imagex IS 'FITS coordinates (1-offset, fast index)';
COMMENT ON COLUMN apphot.imagey IS 'FITS coordinates (1-offset, slow index)';

CREATE SEQUENCE apphot_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE apphot_id_seq OWNED BY apphot.id;
ALTER TABLE ONLY apphot ALTER COLUMN id SET DEFAULT nextval('apphot_id_seq'::regclass);

CREATE INDEX ix_apphot_object ON apphot USING btree(object_id);
ALTER TABLE ONLY apphot
  ADD CONSTRAINT apphot_object_fkey
  FOREIGN KEY (object_id)
  REFERENCES object(id)
  ON DELETE CASCADE;
CREATE INDEX ix_apphot_image ON apphot USING btree(image_id);
ALTER TABLE ONLY apphot
  ADD CONSTRAINT apphot_image_fkey
  FOREIGN KEY (image_id)
  REFERENCES image(id);
CREATE INDEX ix_apphot_reference ON apphot USING btree(reference_id);
ALTER TABLE ONLY apphot
  ADD CONSTRAINT apphot_reference_fkey
  FOREIGN KEY (reference_id)
  REFERENCES object_reference(id);

--- ======================================================================

CREATE TABLE apphottag_def (
  id INT PRIMARY ID,
  def TEXT
);

INSERT INTO apphottag_def VALUES(0, 'Comment');
INSERT INTO apphottag_def VALUES(1, 'Bad ref');
INSERT INTO apphottag_def VALUES(2, 'Bad pixel/column on new');
INSERT INTO apphottag_def VALUES(3, 'Subtraction artifact');
INSERT INTO apphottag_def VALUES(4, 'New/ref misaligned')
INSERT INTO apphottag_def VALUES(5, 'Badly centered aperture');
INSERT INTO apphottag_def VALUES(99, 'Something else bad');

--- ======================================================================

CREATE TABLE apphottag (
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  modified TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  user_id UUID,
  id INT PRIMARY KEY,
  apphot_id INT,
  tag_id INT,
  donotuse BOOLEAN DEFAULT FALSE,
  comment TEXT
  );

CREATE SEQUENCE apphottag_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE apphottag_id_seq OWNED BY apphottag.id;
ALTER TABLE ONLY apphottag ALTER COLUMN id SET DEFAULT nextval('apphottag_id_seq'::regclass);

CREATE INDEX ix_apphottag_apphot ON apphottag USING btree(apphot_id);
ALTER TABLE ONLY apphottag
  ADD CONSTRAINT apphottag_apphot_fkey
  FOREIGN KEY (apphot_id)
  REFERENCES apphot(id)
  ON DELETE CASCADE;

CREATE INDEX ix_apphottag_tag ON apphottag USING btree(tag_id);
ALTER TABLE ONLY apphottag
  ADD CONSTRAINT apphottag_tag_fkey
  FOREIGN KEY (tag_id)
  REFERENCES apphottag(id)
  ON DELETE CASCADE;

CREATE INDEX ix_apphottag_user ON apphottag USING btree(user_id);
ALTER TABLE ONLY apphottag
  ADD CONSTRAINT apphottag_user_fkey
  FOREIGN KEY (user_id)
  REFERENCES authuser(id)
  ON DELETE CASCADE;
  
--- ======================================================================
--- cov is a 5×5 covariance matrix for z t0 x0 x1 c
--- dz, dt0, etc. are redundant with it, which violates DRY, but, oh well.

CREATE TABLE salt2fit (
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  modified TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  mwebv DOUBLE PRECISION DEFAULT 0.,
  mbstar DOUBLE PRECISION,
  dmbstar DOUBLE PRECISION,
  z DOUBLE PRECISION,
  dz DOUBLE PRECISION,
  t0 DOUBLE PRECISION,
  dt0 DOUBLE PRECISION,
  x0 DOUBLE PRECISION,
  dx0 DOUBLE PRECISION,
  x1 DOUBLE PRECISION,
  dx1 DOUBLE PRECISION,
  c DOUBLE PRECISION,
  dc DOUBLE PRECISION,
  id INT PRIMARY KEY,
  object_id INT,
  chisq REAL,
  dof INT,
  primary_fit BOOLEAN DEFAULT FALSE,
  donotuse BOOLEAN DEFAULT FALSE,
  zfixed BOOLEAN DEFAULT TRUE,
  bands INT[],
  cov DOUBLE PRECISION[][]
  );
  
COMMENT ON COLUMN salt2fit.cov IS '5×5 covariance for z t0 x0 x1 c';

CREATE SEQUENCE salt2fit_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE salt2fit_id_seq OWNED BY salt2fit.id;
ALTER TABLE ONLY salt2fit ALTER COLUMN id SET DEFAULT nextval('salt2fit_id_seq'::regclass);

CREATE INDEX ix_salt2fit_object ON salt2fit USING btree(object_id);
ALTER TABLE ONLY salt2fit
  ADD CONSTRAINT salt2fit_object_fkey
  FOREIGN KEY (object_id)
  REFERENCES object(id)
  ON DELETE CASCADE;

-- ======================================================================

CREATE TABLE salt2fittag_def (
  id INT PRIMARY KEY,
  description TEXT
);

INSERT INTO salt2fittag_def VALUES(0, 'Comment');
INSERT INTO salt2fittag_def VALUES(1, 'Bad overall lightcurve');
INSERT INTO salt2fittag_def VALUES(2, 'Data should be able to be fit better');
INSERT INTO salt2fittag_def VALUES(3, 'Rob look at this');

CREATE TABLE salt2fittag (
  created_at TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  modified TIMESTAMP WITH TIME ZONE DEFAULT NOW() NOT NULL,
  user_id UUID,
  id INT PRIMARY KEY,
  salt2fit_id INT,
  tag_id INT,
  comment TEXT
);

CREATE SEQUENCE salt2fittag_id_seq
  START WITH 1 INCREMENT BY 1
  NO MINVALUE NO MAXVALUE CACHE 1;
ALTER SEQUENCE salt2fittag_id_seq OWNED BY salt2fittag.id;
ALTER TABLE ONLY salt2fittag ALTER COLUMN id SET DEFAULT nextval('apphottag_id_seq'::regclass);

CREATE INDEX ix_salt2fittag_salt2fit ON salt2fittag USING btree(salt2fit_id);
ALTER TABLE ONLY salt2fittag
  ADD CONSTRAINT salt2fittag_salt2fit_fkey
  FOREIGN KEY (salt2fit_id)
  REFERENCES salt2fit(id)
  ON DELETE CASCADE;

CREATE INDEX ix_salt2fittag_tag ON salt2fittag USING btree(tag_id);
ALTER TABLE ONLY salt2fittag
  ADD CONSTRAINT salt2fittag_tag_fkey
  FOREIGN KEY (tag_id)
  REFERENCES salt2fittag_def(id)
  ON DELETE CASCADE;

CREATE INDEX ix_salt2fittag_user ON salt2fittag USING btree(user_id);
ALTER TABLE ONLY salt2fittag
  ADD CONSTRAINT salt2fittag_user_fkey
  FOREIGN KEY (user_id)
  REFERENCES authuser(id)
  ON DELETE CASCADE;

-- ======================================================================
-- rkauth tables

CREATE TABLE authuser (
  id           UUID PRIMARY KEY NOT NULL,
  displayname  TEXT NOT NULL,
  email        TEXT NOT NULL,
  username     TEXT UNIQUE NOT NULL,
  pubkey       TEXT DEFAULT '',
  privkey      TEXT DEFAULT '',
  lastlogin    TIMESTAMP WITH TIME ZONE
);

CREATE TABLE passwordlink (
  id           UUID PRIMARY KEY NOT NULL,
  userid       UUID,
  expires      TIMESTAMP WITH TIME ZONE
);

ALTER TABLE ONLY passwordlink
  ADD CONSTRAINT passwordlink_userid_fkey
  FOREIGN KEY (userid)
  REFERENCES authuser(id)
ON DELETE CASCADE;
CREATE INDEX passwordlink_userid_idx ON passwordlink USING btree(userid);
