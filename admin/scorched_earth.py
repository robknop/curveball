import sys
import os
import argparse
import psycopg2
import psycopg2.extras
import psycopg2.sql

import config
import db

def main():
    parser = argparse.ArgumentParser( description="Blow away curveball database",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( "-c", "--curveball_config", default=None,
                         help="curveball config name (default is CURVEBALL_CONFIG env var)" )
    parser.add_argument( "--database-name", required=True, help="Name of database from the curveball config" )
    parser.add_argument( "--do", action="store_true", default=False )
    parser.add_argument( "--really-do", action="store_true", default=False )
    parser.add_argument( "--no-really-i-want-to-delete-everything-and-i-know-what-i-am-doing", action="store_true",
                         default=False )
    args = parser.parse_args()

    cfgname = None
    if args.curveball_config is not None:
        cfgname = f"/home/curveball/{args.curveball_config}.yaml"
    cfg = config.Config.get( cfgname )
    if cfg.value( "database.database" ) != args.database_name:
        raise ValueError( f"Database in config ({cfg.value('database.database')}) does not match "
                          f"what you specified ({args.database_name})" )

    if not ( args.do and args.really_do and args.no_really_i_want_to_delete_everything_and_i_know_what_i_am_doing ):
        raise RuntimeError( "I don't believe you really want to blow away the database." )

    dbo = db.DB.get( cfg=cfg )
    con = dbo.db.connection().engine.raw_connection()
    cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
    cursor.execute( "SELECT * FROM information_schema.tables WHERE table_schema='public'" )
    rows = cursor.fetchall()
    for row in rows:
        cursor.execute( psycopg2.sql.SQL( "DROP TABLE {table} CASCADE" )
                        .format( table=psycopg2.sql.Identifier( row['table_name'] ) ) )
    cursor.close()
    con.commit()

    sys.stderr.write( f"Database {cfg.value('database.database')} on {cfg.value('database.host')} "
                      "completely destroyed.  This can not be undone.  Hope you wanted to do that." )
    

# ======================================================================

if __name__ == "__main__":
    main()
