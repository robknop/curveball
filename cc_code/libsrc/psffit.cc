#include <sys/random.h>
#include <iostream>
#include <random>
#include <sstream>
#include <mpi.h>
#include <psffit.hh>
#include <gsl/gsl_multifit_nlinear.h>
#include <gsl/gsl_vector_double.h>

//**********************************************************************


double PSFFitter::lnL( const std::vector<double> &param, const MPI_Comm &comm ) const {
  auto stamp = psfex.psf_stamp<double>( param[1], param[2], clip0x, clip0y, clipsize, true );

  double chisq = 0;
  for ( int i = 0 ; i < clipsize*clipsize ; ++i ) {
    double delta = clip[i] - param[0] * stamp[i];
    chisq += delta * delta * weight[i];
  }

  return -0.5 * chisq;
}

// **********************************************************************

int lmfuncwrapper( const gsl_vector *params, void *vself, gsl_vector *f ) {
  PSFFitter* self = (PSFFitter*)vself;
  return self->lmfunc( params, f );
}

int PSFFitter::lmfunc( const gsl_vector *params, gsl_vector *f ) {
  std::shared_ptr<double[]> stamp;
  if ( fix_position ) {
    // TODO : this can be cached!  It's the same every time
    stamp = psfex.psf_stamp<double>( fixx0, fixy0, clip0x, clip0y, clipsize, true );
  } else {
    stamp = psfex.psf_stamp<double>( gsl_vector_get(params, 1), gsl_vector_get(params, 2),
                                     clip0x, clip0y, clipsize, true );
  }
  auto flux = gsl_vector_get( params, 0 );
  for ( int i = 0 ; i < clipsize*clipsize ; ++i ) {
    gsl_vector_set( f, i, flux * stamp[i] - clip[i] );
  }

  return 0;
}

// **********************************************************************

PSFFitter::PSFFitter( std::string &psffilename, const double *clip, const double *weight,
                      int clipsize, int clip0x, int clip0y ) :
  psffilename( psffilename ),
  psfex( psffilename ),
  clip( clip ),
  weight( weight ),
  clipsize( clipsize ),
  clip0x( clip0x ),
  clip0y( clip0y )
{
}

// **********************************************************************

void PSFFitter::run_stumbler_psf_fit( std::vector<double> &param, std::vector<std::vector<double>> &covar,
                                      const std::vector<double> &initparam, const std::vector<double> &initsigma,
                                      unsigned long int stumblerseed, std::string filebase,
                                      bool fix_position, int nwalkers, int burnsteps, int steps )
{
  if ( stumblerseed == 0 ) {
    auto junk = getrandom( &stumblerseed, sizeof(unsigned long int), 0 );
  }
  std::string chainname{ filebase + ".dat" };
  std::string burnchainname{ filebase + "_burn.dat" };
  std::string debugchainname{ filebase + "_debug.dat" };
  if ( filebase.size() == 0 ) {
    chainname = "";
    burnchainname = "";
    debugchainname = "";
  }

  MPI_Comm globalcomm, stumblercomm, subcomm;
  globalcomm = MPI_COMM_WORLD;
  int mpirank;
  MPI_Comm_rank( globalcomm, &mpirank );
  MPI_Comm_split( globalcomm, 1, mpirank, &stumblercomm );
  MPI_Comm_split( globalcomm, mpirank, mpirank, &subcomm );

  auto lnLfunc = std::bind( &PSFFitter::lnL, this, std::placeholders::_1, std::placeholders::_2 );
  stumbler = std::make_unique<Stumbler<double>>( nwalkers, burnsteps, steps, 2.0, initparam, initsigma, paramnames,
                                                 lnLfunc, globalcomm, stumblercomm, subcomm,
                                                 chainname, burnchainname, debugchainname, false, 100, stumblerseed );
  this->fix_position = fix_position;
  if ( fix_position ) {
    stumbler->set_param_fixed( { false, true, true } );
  }
  stumbler->go();

  std::vector<std::vector<double>> mean_var;
  stumbler->get_mean_var( mean_var );
  stumbler->get_covar( covar );
  param.resize( mean_var[0].size() );
  for ( int i = 0 ; i < mean_var[0].size() ; ++i ) param[i] = mean_var[0][i];
}

// **********************************************************************

double PSFFitter::stumbler_reduced_chisq() const
{
  std::vector<std::vector<double>> mean_var;
  std::vector<double> param;
  stumbler->get_mean_var( mean_var );
  param.resize( mean_var[0].size() );
  for ( int i = 0 ;i < mean_var[0].size() ; ++i ) param[i] = mean_var[0][i];
  return ( -2 * lnL( param, MPI_COMM_WORLD ) ) / ( clipsize*clipsize - param.size() );
}

// **********************************************************************

void PSFFitter::run_lm_psf_fit( std::vector<double> &param, std::vector<std::vector<double>> &covar,
                               const std::vector<double> &initparam, bool fix_position )
{
  this->fix_position = fix_position;

  gsl_multifit_nlinear_parameters gslparam = gsl_multifit_nlinear_default_parameters();
  auto workspace = gsl_multifit_nlinear_alloc( gsl_multifit_nlinear_trust, &gslparam,
                                               clipsize * clipsize, fix_position ? 1 : 3 );
  gsl_vector* vparam;
  if ( fix_position ) {
    vparam = gsl_vector_alloc( 1 );
    gsl_vector_set( vparam, 0, initparam[0] );
    fixx0 = initparam[1];
    fixy0 = initparam[2];
  } else {
    vparam = gsl_vector_alloc( 3 );
    gsl_vector_set( vparam, 0, initparam[0] );
    gsl_vector_set( vparam, 1, initparam[1] );
    gsl_vector_set( vparam, 2, initparam[2] );
  }
  auto vweight = gsl_vector_alloc( clipsize * clipsize );
  for ( int i = 0 ; i < clipsize * clipsize ; ++i ) gsl_vector_set( vweight, i, weight[i] );

  gsl_multifit_nlinear_fdf gslfunc;
  gslfunc.f = lmfuncwrapper;
  gslfunc.df = nullptr;
  gslfunc.fvv = nullptr;
  gslfunc.n = clipsize * clipsize;
  gslfunc.p = fix_position ? 1 : 3;
  gslfunc.params = this;

  gsl_multifit_nlinear_winit( vparam, vweight, &gslfunc, workspace );

  int done = 0;
  int nsteps = 0;
  while ( ! done ) {
    gsl_multifit_nlinear_iterate( workspace );
    gsl_multifit_nlinear_test( 1e-5, 0, 0, &done, workspace );
  }

  param.resize( 3 );
  if ( fix_position ) {
    param[0] = gsl_vector_get( workspace->x, 0 );
    param[1] = fixx0;
    param[2] = fixy0;
  } else {
    for ( int i = 0 ; i < 3 ; ++i ) param[i] = gsl_vector_get( workspace->x, i );
  }

  gsl_matrix* mcovar;
  if ( fix_position )
    mcovar = gsl_matrix_alloc( 1, 1 );
  else
    mcovar = gsl_matrix_alloc( 3, 3 );
  gsl_multifit_nlinear_covar( workspace->J, 1e-10, mcovar );
  covar.resize(3);
  for ( int i = 0 ; i < 3 ; ++i ) {
    covar[i].resize(3);
    for ( int j = 0 ; j < 3 ; ++j ) {
      if ( ( !fix_position ) || ( ( i == 0 ) && ( j == 0 ) ) )
          covar[i][j] = gsl_matrix_get( mcovar, i, j );
        else
          covar[i][j] = 0.;
    }
  }

  gsl_matrix_free( mcovar );
  gsl_vector_free( vweight );
  gsl_vector_free( vparam );
  gsl_multifit_nlinear_free( workspace );
}

// **********************************************************************

extern "C"
{
  void fit_psf_stumbler( const char *psffilename, const double *clip, const double *weight,
                         int clipsize, int clip0x, int clip0y,
                         double initflux, double initx, double inity, double sigflux, double sigx, double sigy,
                         int fixpos, int steps, int burnsteps, int nwalkers, double *param, double *covar,
                         unsigned long int stumblerseed, const char *filebasecstr )
  {
    std::string psffile{ psffilename };
    std::string filebase{ filebasecstr };
    std::vector<double> initparam{ initflux, initx, inity };
    std::vector<double> initsig{ sigflux, sigx, sigy };
    bool fix_position = ( fixpos == 0 ) ? false : true;

    // std::cerr << "filebase = \"" << filebase << "\" (size " << filebase.size() << ")" << std::endl;
    // std::cerr << "stumblerseed = " << stumblerseed << std::endl;
    // std::cerr << "psfffile = " << psffile << std::endl;
    // std::cerr << "clip = " << clip << std::endl;
    // std::cerr << "weight = " << weight << std::endl;
    // std::cerr << "clipsize = " << clipsize << std::endl;
    // std::cerr << "clip0x = " << clip0x << std::endl;
    // std::cerr << "clip0y = " << clip0y << std::endl;
    // std::cerr << "initparam = ";
    // for ( auto vi = initparam.begin() ; vi != initparam.end() ; ++vi ) std::cerr << *vi << " ";
    // std::cerr << std::endl;
    // std::cerr << "initsig = ";
    // for ( auto vi = initsig.begin() ; vi != initsig.end() ; ++vi ) std::cerr << *vi << " ";
    // std::cerr << std::endl;
    // std::cerr << "nwalkers = " << nwalkers << std::endl;
    // std::cerr << "burnsteps = " << burnsteps << std::endl;
    // std::cerr << "steps = " << steps << std::endl;
    // std::cerr << "fix_position = " << fix_position << std::endl;

    auto fitter = PSFFitter( psffile, clip, weight, clipsize, clip0x, clip0y );

    std::vector<double> vparam;
    std::vector<std::vector<double>> vcovar;

    // std::cerr << "vparam.size() = " << vparam.size() << std::endl;
    // std::cerr << "vcovar.size() == " << vcovar.size() << std::endl;
    // std::cerr << "Calling fitter.run_psf_fit" << std::endl << std::flush;

    fitter.run_stumbler_psf_fit( vparam, vcovar, initparam, initsig, stumblerseed, filebase,
                                 fix_position, nwalkers, burnsteps, steps );

    // std::cerr << "vparam.size() = " << vparam.size() << std::endl;
    // std::cerr << "vcovar.size() = " << vcovar.size() << std::endl;
    // for ( int i = 0 ; i < vcovar.size() ; ++i ) {
    //   std::cerr << "vcovar[" << i << "].size() = " << vcovar[i].size() << std::endl;
    // }
    // std::cerr << "param = " << param << std::endl;
    // std::cerr << "covar = " << covar << std::endl;

    for ( int i = 0 ; i < 3 ; ++i ) {
      param[i] = vparam[i];
      for ( int j = 0 ; j < 3 ; ++j ) {
        covar[ j * 3 + i ] = vcovar[i][j];
      }
    }
  }

  // **********************************************************************

  void fit_psf_lm( const char *psffilename, const double *clip, const double *weight,
                   int clipsize, int clip0x, int clip0y,
                   double initflux, double initx, double inity,
                   int fixpos, double *param, double *covar )
  {
    std::string psffile( psffilename );
    std::vector<double> initparam { initflux, initx, inity };
    bool fix_position = ( fixpos == 0 ) ? false : true;

    auto fitter = PSFFitter( psffile, clip, weight, clipsize, clip0x, clip0y );
    std::vector<double> vparam;
    std::vector<std::vector<double>> vcovar;
    fitter.run_lm_psf_fit( vparam, vcovar, initparam, fix_position );

    for ( int i = 0 ; i < 3 ; ++i ) {
      param[i] = vparam[i];
      for ( int j = 0 ; j < 3 ; ++j ) {
        covar[ j * 3 + i ] = vcovar[i][j];
      }
    }
  }

}


