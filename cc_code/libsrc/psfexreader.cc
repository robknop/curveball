#include "psfexreader.hh"
#include <exception>
#include <sstream>
#include <cmath>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <fitsio.h>
#include <gsl/gsl_sf_trig.h>

// **********************************************************************

PSFExReader::PSFExReader() {
  make_logger();
}

PSFExReader::PSFExReader( std::shared_ptr<spdlog::logger> inlogger ) {
  logger = inlogger;
}

PSFExReader::PSFExReader( const std::string &filepath ) {
  make_logger();
  read_psf( filepath );
}

PSFExReader::PSFExReader( const std::string &filepath, std::shared_ptr<spdlog::logger> inlogger ) {
  logger = inlogger;
  read_psf( filepath );
}

// **********************************************************************

PSFExReader::~PSFExReader() {
}

// **********************************************************************

void PSFExReader::make_logger() {
  if ( logger == nullptr ) {
    logger = spdlog::get( "PSFExReader" );
    if ( logger == nullptr ) {
      logger = spdlog::stderr_color_mt( "PSFExReader" );
      logger->set_level( spdlog::level::info );
    }
  }
}

// **********************************************************************

void PSFExReader::read_psf( const std::string &filepath ) {
  if ( initialized ) throw std::runtime_error( "PSFExReader is already initialized, can't reread" );
  initialized = true;
  std::stringstream str("");
  int status = 0;

  fitsfile *fptr;
  fits_open_file( &fptr, filepath.c_str(), READONLY, &status );
  if ( status != 0 ) {
    str.str();
    str << "Error " << status << " in read_psf reading file " << filepath;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  int hdutype = 0;
  fits_movabs_hdu( fptr, 2, &hdutype, &status );
  if ( status != 0 ) {
    str.str();
    str << "Error " << status << " in read_psfd moving to HDU 2 in " << filepath;
    logger->error( str.str() );
    status = 0;
    fits_close_file( fptr, &status );
    throw std::runtime_error( str.str() );
  }
  if ( hdutype != BINARY_TBL ) {
    str.str("");
    str << "read_psf: hdutype is " << hdutype << ", expected BINARY_TBL (" << BINARY_TBL << ")";
    logger->error( str.str() );
    status = 0;
    fits_close_file( fptr, &status );
    throw std::runtime_error( "read_psf: didn't find a binary table in HDU 2" );
  }

  char comment[80];
  char polname1[80], polname2[80];
  int polnaxis, polngrp;
  fits_read_key( fptr, TINT, "POLNAXIS", &polnaxis, comment, &status );
  fits_read_key( fptr, TINT, "POLNGRP", &polngrp, comment, &status );
  fits_read_keyword( fptr, "POLNAME1", polname1, comment, &status );
  fits_read_keyword( fptr, "POLNAME2", polname2, comment, &status );
  fits_read_key( fptr, TDOUBLE, "POLZERO1", &polzero1, comment, &status );
  fits_read_key( fptr, TDOUBLE, "POLSCAL1", &polscal1, comment, &status );
  fits_read_key( fptr, TDOUBLE, "POLZERO2", &polzero2, comment, &status );
  fits_read_key( fptr, TDOUBLE, "POLSCAL2", &polscal2, comment, &status );
  fits_read_key( fptr, TDOUBLE, "PSF_SAMP", &_psfsamp, comment, &status );
  fits_read_key( fptr, TDOUBLE, "POLDEG1", &poldeg1, comment, &status );
  if ( status != 0 ) {
    str.str("");
    str << "read_psf: status=" << status << " after extracting header keywords.";
    logger->error( str.str() );
    status = 0;
    fits_close_file( fptr, &status );
    throw std::runtime_error( str.str() );
  }
  bool ok = ( ( polnaxis == 2 ) &&
              ( polngrp == 1 ) &&
              ( strcmp( polname1, "'X_IMAGE '" ) == 0 ) &&
              ( strcmp( polname2, "'Y_IMAGE '" ) == 0 ) );
  if ( ! ok ) {
    str.str("");
    str << "read_psf: Unexpected header keywords:\n";
    str << "POLNAXIS = " << polnaxis << " (expected 2) " << std::endl;
    str << "POLNGRP = " << polngrp << " (expected 1) " << std::endl;
    str << "POLNAME1 = " << polname1 << " (expected 'X_IMAGE ') " << std::endl;
    str << "POLNAME2 = " << polname2 << " (expected 'Y_IMAGE ') " << std::endl;
    logger->error( str.str() );
    status = 0;
    fits_close_file( fptr, &status );
    throw std::runtime_error( str.str() );
  }

  str.str( "" );
  str << "read_psf: Some values from header:" << std::endl;
  str << "POLZERO1 = " << polzero1 << std::endl;
  str << "POLSCAL1 = " << polscal1 << std::endl;
  str << "POLZERO2 = " << polzero2 << std::endl;
  str << "POLSCAL2 = " << polscal2 << std::endl;
  str << "PSF_SAMP = " << _psfsamp << std::endl;
  str << "POLDEG1 = " << poldeg1 << std::endl;
  logger->debug( str.str() );

  long nrows;
  int ncols;
  fits_get_num_rows( fptr, &nrows, &status );
  fits_get_num_cols( fptr, &ncols, &status );
  if ( ( nrows != 1 ) || ( ncols != 1 ) ) {
    str.str("");
    str << "read_psf: Binary table has " << nrows << " rows and " << ncols << " columns; expected 1×1.";
    logger->error( str.str() );
    status = 0;
    fits_close_file( fptr, &status );
    throw std::runtime_error( str.str() );
  }
  int naxis, typecode;
  long naxes[3] = { 0, 0, 0 };
  long repeat, width;
  fits_read_tdim( fptr, 1, 3, &naxis, naxes, &status );
  fits_get_coltype( fptr, 1, &typecode, &repeat, &width, &status );
  if ( naxis != 3 ) {
    str.str("");
    str << "read_psf: Got naxis= " << naxis << ", expected 3.";
    logger->error( str.str() );
    status = 0;
    fits_close_file( fptr, &status );
    throw std::runtime_error( str.str() );
  }
  if ( typecode != TFLOAT ) {
    str.str("");
    str << "read_psf: Got typecode=" << typecode << ", expected " << TFLOAT << " (TFLOAT)";
    logger->error( str.str() );
    status = 0;
    fits_close_file( fptr, &status );
    throw std::runtime_error( str.str() );
  }
  str.str("");
  str << "PSF is " << naxes[0] << " × " << naxes[1] << " × " << naxes[2]
      << " ; typecode=" << typecode << ", repeat=" << repeat << ", width=" << width;
  logger->debug( str.str() );

  psfdata = std::move( std::make_unique<float[]>( naxes[0] * naxes[1] * naxes[2] ) );
  float nullval = std::numeric_limits<double>::quiet_NaN();
  int anynull;
  fits_read_col( fptr, TFLOAT, 1, 1, 1, naxes[0]*naxes[1]*naxes[2], &nullval, psfdata.get(), &anynull, &status );
  if ( naxes[0] != naxes[1] ) {
    logger->error( "read_psf: naxes[0] != naxes[1], but I'm going to assume that!" );
    status = 0;
    fits_close_file( fptr, &status );
    throw std::runtime_error( "read_psf: naxes[0] != naxes[1], but I'm going to assume that!" );
  }
  _psfwid = naxes[0];

  fits_close_file( fptr, &status );
}

// **********************************************************************
// This function isn't optimized; I could make more variables
// and iteratively multipy stuff up instead of using pow.
// However, in practice, this function is much faster than
// the filling of the psf stamp (where sincs are used).
// Using long double to be super paranoid about roundoff.  Probably excessive.

template<class T> std::shared_ptr<T[]> PSFExReader::get_oversampled_psf( double x0, double y0 ) const {
  std::unique_ptr<long double[]> ldbuffer = std::make_unique<long double[]>( _psfwid*_psfwid );
  std::shared_ptr<T[]> psfbuffer = std::move( std::make_unique<T[]>( _psfwid*_psfwid ) );
  for ( int i = 0 ; i < _psfwid*_psfwid ; ++i ) ldbuffer[i] = 0.;

  // I realy hope I'm handling the polynomial degree stuff right here.  I *think* I am, empirically,
  // and also based on Figure 1 of https://adsabs.harvard.edu/pdf/2011ASPC..442..435B
  // i and j are indexes into the x and y polynomial orders.
  // px and py are indexes into the psf buffer
  for ( int i = 0, polyoff = 0; i < poldeg1+1 ; ++i ) {
    // +1 because of C to FITS coordinate conversion
    double ypow = pow( ( y0+1 - polzero2 ) / polscal2, i );
    for ( int j = 0 ; j < poldeg1+1-i ; ++j, ++polyoff ) {
      double xpow = pow( ( x0+1 - polzero1 ) / polscal1, j );
      for ( int px = 0 ; px < _psfwid ; ++px ) {
        for ( int py = 0 ; py < _psfwid ; ++py ) {
          int spatialoff = px + py * _psfwid;
          ldbuffer[ spatialoff ] += psfdata[ polyoff * _psfwid * _psfwid + spatialoff ] * xpow * ypow;
        }
      }
    }
  }

  for ( int i = 0 ; i < _psfwid*_psfwid ; ++i ) psfbuffer[i] = ldbuffer[i];
  return psfbuffer;
}

template std::shared_ptr<float[]> PSFExReader::get_oversampled_psf<float>(double, double) const;
template std::shared_ptr<double[]> PSFExReader::get_oversampled_psf<double>(double, double) const;


// **********************************************************************

template<class T> std::shared_ptr<T[]> PSFExReader::psf_stamp( double x0, double y0, int offx, int offy,
                                                               int stampsize, bool normalize ) const
{
  // So... I don't know why I need to do std::move with std::make_unique<T[]> here.
  // I tried std::make_shared<T[]>, and it compiled as long as the standard was
  // at least C++17, but then I was getting a weird corrupted linked list error.
  // I don't understand.
  std::shared_ptr<T[]> stamp = std::move( std::make_unique<T[]>( stampsize*stampsize ) );
  fill_psf_stamp( stamp.get(), x0, y0, offx, offy, stampsize, normalize );
  return stamp;
}

template std::shared_ptr<float[]> PSFExReader::psf_stamp<float>(double,double,int,int,int,bool) const;
template std::shared_ptr<double[]> PSFExReader::psf_stamp<double>(double,double,int,int,int,bool) const;

template<class T> std::shared_ptr<T[]> PSFExReader::psf_stamp( double x0, double y0, int stampsize, bool normalize )
  const
{
  assert( stampsize % 2 != 0 );
  std::shared_ptr<T[]> stamp = std::move( std::make_unique<T[]>( stampsize*stampsize ) );
  fill_psf_stamp( stamp.get(), x0, y0, stampsize, normalize );
  return stamp;
}

template std::shared_ptr<float[]> PSFExReader::psf_stamp<float>(double,double,int,bool) const;
template std::shared_ptr<double[]> PSFExReader::psf_stamp<double>(double,double,int,bool) const;

template<class T,class P> std::shared_ptr<T[]> PSFExReader::psf_stamp( double x0, double y0, int stampsize,
                                                                       P* psfbuffer, bool normalize )
  const
{
  assert( stampsize % 2 != 0 );
  std::shared_ptr<T[]> stamp = std::move( std::make_unique<T[]>( stampsize*stampsize ) );
  fill_psf_stamp( stamp.get(), x0, y0, stampsize, psfbuffer, normalize );
  return stamp;
}

template std::shared_ptr<float[]> PSFExReader::psf_stamp<float,float>(double,double,int,float*,bool) const;
template std::shared_ptr<double[]> PSFExReader::psf_stamp<double,double>(double,double,int,double*,bool) const;
template std::shared_ptr<float[]> PSFExReader::psf_stamp<float,double>(double,double,int,double*,bool) const;
template std::shared_ptr<double[]> PSFExReader::psf_stamp<double,float>(double,double,int,float*,bool) const;

// **********************************************************************

template<class T> void PSFExReader::fill_psf_stamp( T* stamp, double x0, double y0, int stampsize, bool normalize )
  const
{
  auto psfbuffer = get_oversampled_psf<T>( x0, y0 );
  fill_psf_stamp( stamp, x0, y0, stampsize, psfbuffer.get(), normalize );
}

template void PSFExReader::fill_psf_stamp<float>(float*,double,double,int,bool) const;
template void PSFExReader::fill_psf_stamp<double>(double*,double,double,int,bool) const;

template<class T,class P> void PSFExReader::fill_psf_stamp( T* stamp, double x0, double y0, int stampsize,
                                                            P* psfbuffer, bool normalize )
  const
{
  assert( stampsize % 2 != 0 );

  int xc = (int)floor( x0 + 0.5 );
  int yc = (int)floor( y0 + 0.5 );
  // std::stringstream str("");
  // str << "PSFExReader::fill_psf_stamp: xc = " << xc << ", yc = " << yc;
  // logger->debug( str.str() );

  return fill_psf_stamp<T,P>( stamp, x0, y0, xc-stampsize/2, yc-stampsize/2, stampsize, psfbuffer, normalize );
}

template void PSFExReader::fill_psf_stamp<float,float>(float*,double,double,int,float*,bool) const;
template void PSFExReader::fill_psf_stamp<double,double>(double*,double,double,int,double*,bool) const;
template void PSFExReader::fill_psf_stamp<double,float>(double*,double,double,int,float*,bool) const;
template void PSFExReader::fill_psf_stamp<float,double>(float*,double,double,int,double*,bool) const;

template<class T> void PSFExReader::fill_psf_stamp( T* stamp, double x0, double y0,
                                                    int offx, int offy, int stampsize,
                                                    bool normalize )
  const
{
  auto psfbuffer = get_oversampled_psf<T>( x0, y0 );
  fill_psf_stamp( stamp, x0, y0, offx, offy, stampsize, psfbuffer.get(), normalize );
}

template void PSFExReader::fill_psf_stamp<float>(float*,double,double,int,int,int,bool) const;
template void PSFExReader::fill_psf_stamp<double>(double*,double,double,int,int,int,bool) const;

template<class T,class P> void PSFExReader::fill_psf_stamp( T* stamp, double x0, double y0,
                                                            int offx, int offy, int stampsize,
                                                            P* psfbuffer, bool normalize )
  const
{
  // std::stringstream str("");
  // str << "PSFExReader::fill_psf_stamp: offx = " << offx << ", offy = " << offy;
  // logger->debug( str.str() );

  const int sinclim = 4;  // 4 is the PSFEx standard
  // isx and isy are indexes into the stamp
  // iix and iiy are indexes into where on the image the stamp pixel would be
  // px and py are indexes into the psf

  for ( int i = 0 ; i < stampsize*stampsize ; ++i ) stamp[i] = 0.;

  long double tot = 0.;
  for ( int isx = 0, iix = offx ; isx < stampsize ; ++iix, ++isx ) {
    for ( int px = 0 ; px < _psfwid ; ++px ) {
      double xsincarg = ( -_psfwid / 2 + px ) - ( iix - x0 ) / _psfsamp;

      if ( xsincarg >= -sinclim && xsincarg <= sinclim ) {
        double xsincval = gsl_sf_sinc( xsincarg ) * gsl_sf_sinc( xsincarg/4. );

        long double subtot = 0.;
#pragma omp parallel for reduction(+:subtot)
        for ( int isy = 0 ; isy < stampsize ; ++isy ) {
          int iiy = isy + offy;
          for ( int py = 0 ; py < _psfwid ; ++py ) {
            double ysincarg = ( -_psfwid / 2 + py ) - ( iiy - y0 ) / _psfsamp;
            if ( ysincarg >= -sinclim && ysincarg <= sinclim ) {
              double val = ( psfbuffer[ px + py * _psfwid ] * xsincval
                             * gsl_sf_sinc( ysincarg ) * gsl_sf_sinc( ysincarg/4. ) );
              stamp[ isx + isy * stampsize ] += val;
              subtot += val;
            }
          }
        }
        tot += subtot;
      }
    }
  }

  // I was finding that the stamps weren't coming out normalized; their
  //   sum was off from 1.0 by up to 1 or 2% (in either direction),
  //   varying across the array.  I'm pretty sure I've got the
  //   polynomial terms right, and the sinc interpolation right, so I'm
  //   not sure what's up.  If there's a subtle bug with one of them,
  //   I'd like to find it, of course!  I *suspect*, however, that the
  //   real issue is that the sinc resampler is resampling from
  //   integbrated pixels to delta functions, rather than from
  //   integrated pixels to integrated pixels.  I would have to think
  //   harder about the math (or have somebody who understands the math
  //   better) to understand if that's really the case.
  // But, for now, explicitly make sure the stamp is normalized.  NOTE!
  //   This is not the right thing to do if the stamp is too small
  //   compared to the size of the PSF!
  if (normalize)
    for ( int i = 0 ; i < stampsize * stampsize ; ++i ) stamp[i] /= tot;
}

template void PSFExReader::fill_psf_stamp<float,float>(float*,double,double,int,int,int,float*,bool) const;
template void PSFExReader::fill_psf_stamp<double,double>(double*,double,double,int,int,int,double*,bool) const;
template void PSFExReader::fill_psf_stamp<float,double>(float*,double,double,int,int,int,double*,bool) const;
template void PSFExReader::fill_psf_stamp<double,float>(double*,double,double,int,int,int,float*,bool) const;

// **********************************************************************

template<class T> void PSFExReader::add_psf_to_image( T* image, int imagewid, int imagehei, int stampsize,
                                                     double x0, double  y0, double flux )
  const
{
  auto stamp = psf_stamp<T>( x0, y0, stampsize );
  for ( int i = 0 ; i < stampsize*stampsize ; ++i ) stamp[i] *= flux;
  add_stamp_to_image( image, imagewid, imagehei, stamp.get(), stampsize,
                      (int)floor(x0+0.5), (int)floor(y0+0.5) );
}

template void PSFExReader::add_psf_to_image<float>( float* image, int imagewid, int imagehei, int stampsize,
                                                    double x0, double y0, double flux ) const;
template void PSFExReader::add_psf_to_image<double>( double* image, int imagewid, int imagehei, int stampsize,
                                                     double x0, double y0, double flux ) const;

// **********************************************************************

template<class T> void PSFExReader::add_stamp_to_image( T* image, int imagewid, int imagehei,
                                                        T* stamp, int stampsize, int xc, int yc )
  const
{
  int offx = xc - stampsize/2;
  int offy = yc - stampsize/2;

  for ( int x = 0 ; x < stampsize ; ++x ) {
    int ix = x + offx;
    if ( ix < 0 || ix >=imagewid ) continue;
    for ( int y = 0 ; y < stampsize ; ++y ) {
      int iy = y + offy;
      if ( iy < 0 || iy >= imagehei ) continue;
      image[ iy * imagewid + ix ] += stamp[ y * stampsize + x ];
    }
  }
}

template void PSFExReader::add_stamp_to_image<float>( float* image, int imagewid, int imagehei,
                                                      float* stamp, int stampsize, int xc, int yc ) const;
template void PSFExReader::add_stamp_to_image<double>( double* image, int imagewid, int imagehei,
                                                       double* stamp, int stampsize, int xc, int yc ) const;
