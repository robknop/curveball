#include <fitsfile.hh>
#include <wcswrapper.hh>
#include <exception>
#include <sstream>
// #include <iostream>
#include <iomanip>
#include <cstdint>
#include <cstring>
#include <cstdio>
#include <sys/stat.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <gnuastro/fits.h>
#include <gnuastro/wcs.h>

// This is the largest size that gnuastro will read into memory before
//  doing memory mapping.  We want it not to do memory mapping, so this
//  should be bigger than anything we might load.
const size_t BIGASS = 1073741824;

// **********************************************************************
// **********************************************************************
// **********************************************************************

// **********************************************************************

FITSFile::FITSFile()
  : filename("")
{
  make_logger();
  initialize();
}

FITSFile::FITSFile( std::shared_ptr<spdlog::logger> logger )
  : filename("")
{
  if ( logger == nullptr ) make_logger();
  else this->logger = logger;
  initialize();
}

FITSFile::FITSFile( const std::string &filename, std::shared_ptr<spdlog::logger> logger )
{
  if ( logger == nullptr ) make_logger();
  else this->logger = logger;
  open_file( filename );
}

FITSFile::~FITSFile()
{
  // If I understand unique pointers correctly, this should destroy all of the members
  _hdus.clear();
}

void FITSFile::initialize() {
  filename = "";
  _hdus.clear();
}

void FITSFile::make_logger() {
  logger = spdlog::get( "FITSFile" );
  if ( logger == nullptr ) {
    logger = spdlog::stderr_color_mt( "FITSFile" );
    logger->set_level( spdlog::level::info );
  }
}

// **********************************************************************

void FITSFile::open_file( const std::string &infilename ) {
  initialize();
  filename = infilename;

  int nhdus = gal_fits_hdu_num( &(filename[0]) );
  _hdus.resize( nhdus );
  for ( int i = 0 ; i < nhdus ; ++i ) _hdus[i] = nullptr;
}

// **********************************************************************

bool FITSFile::is_simple_2d_image() {
  return ( simple_2d_image_hdu_num() >= 0 );
}

// **********************************************************************

int FITSFile::naxis_of_hdu( int n ) {
  std::stringstream str("");
  if ( _hdus[n] == nullptr ) {
    int status = 0;
    int naxis = -1;
    std::string hdustr = std::to_string( n );
    fitsfile *fptr = gal_fits_hdu_open( &(filename[0]), &(hdustr[0]), READONLY, 0 );
    fits_get_img_dim( fptr, &naxis, &status );
    if ( status != 0 ) {
      str.str("");
      str << "FITSFile:naxis_of_hdu : got status = " << status << " from fits_get_img_dim on HDU "
          << n << " of " << filename;
      logger->error( str.str() );
      fits_close_file( fptr, &status );
      throw std::runtime_error( str.str() );
    }
    fits_close_file( fptr, &status );
    return naxis;
  }
  else {
    if ( _hdus[n]->hdutype != FITSHDU::image_hdu ) return -1;
    return ((FITSImageWrapper*)(_hdus[n].get()))->_axlen.size();
  }
}

int FITSFile::simple_2d_image_hdu_num() {
  if ( ( _hdus.size() != 1 ) && ( _hdus.size() != 2 ) ) return -1;

  if ( ( _hdus.size() == 1 ) && ( hdu_is_type( 0, IMAGE_HDU ) ) ) {
    return ( ( naxis_of_hdu( 0 ) == 2 ) ? 0 : -1 );
  }

  if ( _hdus.size() == 2 ) {
    // For .fits.fz files, the first HDU is an image with naxis = 0
    if ( !hdu_is_type( 0, IMAGE_HDU ) ) return -1;
    if ( naxis_of_hdu( 0 ) != 0 ) return -1;

    char one[]{ '1', '\0' };
    fitsfile *fptr = gal_fits_hdu_open( &(filename[0]), one, READONLY, 0 );
    int status = 0;
    bool iscompressedimage = fits_is_compressed_image( fptr, &status );
    if ( status != 0 ) {
      std::stringstream str("");
      str << "FITSFile::simple_2d_image_hdu_num : got status = " << status << " from fits_is_compressed_image";
      logger->error( str.str() );
      fits_close_file( fptr, &status );
      throw std::runtime_error( str.str() );
    }
    fits_close_file( fptr, &status );
    if ( not iscompressedimage ) return -1;

    return ( ( naxis_of_hdu( 1 ) == 2 ) ? 1 : -1 );
  }

  return -1;
}

// **********************************************************************

bool FITSFile::hdu_is_type( int n, int hdutype ) {
  if ( _hdus[n] != nullptr ) {
    return ( _hdus[n]->hdutype == hdutype );
  }
  else {
    if ( filename.size() == 0 ) {
      logger->error( "FITSFile::hdu_is_type : null HDU pointer in FITSFile not associated with filename" );
      throw std::runtime_error( "FITSFile::hdu_is_type : null HDU pointer in FITSFile not associated with filename" );
    }
    std::string hdustr = std::to_string( n );
    int thishdutype = gal_fits_hdu_format( &(filename[0]), &(hdustr[0]) );
    return ( thishdutype == hdutype );
  }
}

// **********************************************************************

FITSHDU* FITSFile::hdu( size_t n ) {
  if ( _hdus[n] != nullptr ) return _hdus[n].get();

  if ( filename.size() == 0 ) {
    logger->error( "FITSFile::hdu : null HDU pointer in FITSFile not associated with filename" );
    throw std::runtime_error( "FITSFile::hdu : null HDU pointer in FITSFile not associated with filename" );
  }

  std::stringstream str("");
  str << "FITSFile::hdu : going to try to read hdu " << n << " from " << filename;
  logger->debug( str.str() );

  std::string hdustr = std::to_string( n );
  int hdutype = gal_fits_hdu_format( &(filename[0]), &(hdustr[0]) );

  if ( hdutype == FITSHDU::image_hdu ) {
    str.str("");
    fitsfile* fptr = gal_fits_hdu_open( &(filename[0]), &(hdustr[0]), READONLY, 0 );
    int type;
    size_t ndim;
    size_t *dims;
    gal_fits_img_info( fptr, &type, &ndim, &dims, nullptr, nullptr );
    free( dims );
    int status = 0;
    fits_close_file( fptr, &status );
    auto img = FITSImageWrapper::make_right_FITSImage_type( type, this, n, logger );
    img->read_image_data();
    if ( img->get_galdata()->wcs != nullptr ) {
      str.str("");
      str << "FITSFile::hdu : PROBLEM.  wcs is not nullptr after make_right_FITSImage_type";
      logger->error( str.str() );
    }
    img->set_wcs( std::move( WCSWrapper::read_from_file( filename, n, logger ) ) );
    _hdus[n] = std::shared_ptr<FITSHDU>( (FITSHDU*)img );
  }
  else if ( hdutype == FITSHDU::ascii_tbl ) {
    str.str("");
    str << "FITSFile::hdu : reading ascii tables isn't implemented (" << filename << " HDU " << n << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  else if ( hdutype == FITSHDU::binary_tbl ) {
    str.str("");
    str << "FITSFile::hdu : reading binary tables isn't implemented (" << filename << " HDU " << n << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  else {
    str.str("");
    str << "FITSFile::hdu : unknown HDU type " << hdutype << " (" << filename << " HDU " << n << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  _hdus[n]->hdutype = hdutype;
  return _hdus[n].get();
}

// **********************************************************************

void FITSFile::add_hdu( std::shared_ptr<FITSHDU> newhdu )
{
  _hdus.push_back( newhdu );
  _hdus.back()->set_hdunum( _hdus.size() - 1 );
  _hdus.back()->parentfile = this;
}

// **********************************************************************

void FITSFile::save_to( const std::string& outfile, bool overwrite )
{
  std::stringstream str("");
  int status = 0;

  // Because the FITSFile may not have actually read in some of the data (if this was a file on disk),
  //   then we don't have everything we need to save the image.  Perhaps we should read it here
  //   rather than throw an exception.
  for ( int i = 0 ; i < _hdus.size() ; ++i ) {
    if ( _hdus[i] == nullptr ) {
      str.str("");
      str << "FITSFile::save_to : HDU " << 0 << " hasn't been read or created; can't save this file.";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
  }

  struct stat statbuffer;
  if ( stat( outfile.c_str(), &statbuffer ) == 0 ) {
    if ( overwrite ) {
      str.str("");
      str << "FITSFile::save_to : " << outfile << " exists, deleting";
      logger->warn( str.str() );
      int err = std::remove( outfile.c_str() );
      if ( err != 0 ) {
        str.str("");
        str << "FITSFile::save_to : error " << err << " deleting " << outfile;
        logger->error( str.str() );
      throw std::runtime_error( str.str() );
      }
    }
    else {
      str.str("");
      str << "FITSFile::svae_to : " << outfile << " exists and overwrite is false";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
  }

  fitsfile *ofp = nullptr;
  fits_create_diskfile( &ofp, &(outfile[0]), &status );
  if ( status != 0 ) {
    str.str("");
    str << "FITSFile::save_to : status = " << status << " creating " << outfile;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  for ( int hdunum = 0 ; hdunum < _hdus.size() ; ++hdunum ) {
    FITSHDU* hdu = _hdus[hdunum].get();

    // Write the data
    if ( _hdus[hdunum]->hdutype == FITSHDU::image_hdu ) {
      FITSImageWrapper *img = (FITSImageWrapper*)(hdu);

      auto naxes = std::make_unique<long[]>( img->_axlen.size() );
      long long ndata = 1;
      for ( int i = 0 ; i < img->_axlen.size() ; ++i ) {
        naxes[i] = img->_axlen[i];
        ndata *= naxes[i];
      }
      int bitpix = gal_fits_type_to_bitpix( img->datatype );
      int cfitsiodatatype = gal_fits_type_to_datatype( img->datatype );
      fits_create_img( ofp, bitpix, img->_axlen.size(), naxes.get(), &status );
      if ( status != 0 ) {
        str.str("");
        str << "FITSFile::save_to : error in fits_create_img for HDU " << hdunum << " of " << outfile;
        logger->error( str.str() );
        fits_close_file( ofp, &status );
        throw std::runtime_error( str.str() );
      }
      void* actualimagedata = img->voidimagedata;
      std::shared_ptr<char[]> copieddata;
      if ( img->galdata->block != nullptr ) {
        if ( img->galdata->block->block != nullptr ) {
          logger->error( "FITSFile::save_to : I don't know how to write tiles of tiles!" );
          throw std::runtime_error( str.str() );
        }
        logger->warn( "Using copy_void_image_data to save image tile" );
        // We have to make a copy of the data, since the memory layout is not what
        // fits_write_pix is going to assume.
        copieddata = img->copy_void_image_data();
        actualimagedata = copieddata.get();
      }
      auto ones = std::make_unique<long[]>( img->_axlen.size() );
      for ( int i = 0 ; i < img->_axlen.size() ; ++i ) { ones[i] = 1; }
      fits_write_pix( ofp, cfitsiodatatype, ones.get(), ndata, actualimagedata, &status );
      if ( status != 0 ) {
        str.str("");
        str << "FITSFile::save_to : status = " << status << " trying to run fits_write_pix for HDU "
            << hdunum << " of " << outfile;
        logger->error( str.str() );
        fits_close_file( ofp, &status );
        throw std::runtime_error( str.str() );
      }
      copieddata = nullptr;  // Pathetic attempt to free memory as soon as possible
    }
    else {
      str.str("");
      str << "FITSFile::save_to : only image HDUs are currently supported, can't write HDU of type "
          << _hdus[hdunum]->hdutype;
      logger->error( str.str() );
      fits_close_file( ofp, &status );
      throw std::runtime_error( str.str() );
    }

    // Write the header.
    // Watch me blithely cast const char* to char*.
    for ( auto kw : hdu->_hdrkeywords ) {
      if ( hdu->hdrdtype(kw) == FITSHDU::hdr_string ) {
        fits_update_key( ofp, FITSHDU::hdr_string, &(kw[0]), (char*)(hdu->hdrsval(kw).c_str()),
                         (char*)(hdu->hdrkwcomment(kw)), &status );
      }
      else if ( hdu->hdrdtype(kw) == FITSHDU::hdr_bool ) {
        int val = hdu->hdrbval(kw) ? 1 : 0;
        fits_update_key( ofp, TLOGICAL, &(kw[0]), &val, (char*)(hdu->hdrkwcomment(kw)), &status );
      }
      else if ( hdu->hdrdtype(kw) == FITSHDU::hdr_int ) {
        int val = hdu->hdrival(kw);
        fits_update_key( ofp, TINT, &(kw[0]), &val, (char*)(hdu->hdrkwcomment(kw)), &status );
      }
      else if ( hdu->hdrdtype(kw) == FITSHDU::hdr_double ) {
        double val = hdu->hdrdval(kw);
        fits_update_key( ofp, TDOUBLE, &(kw[0]), &val, (char*)(hdu->hdrkwcomment(kw)), &status );
      }
      else {
        str.str("");
        str << "FITSFile::save_to : unknown header dtype " << hdu->hdrdtype(kw) << " for " << kw;
        logger->error( str.str() );
        throw std::runtime_error( str.str() );
      }
      if ( status != 0 ) {
        str.str("");
        str << "FITSFile::save_to : status = " << status << " from fits_update_key on " << kw;
        logger->error( str.str() );
        throw std::runtime_error( str.str() );
      }
    }
    for ( auto comment : hdu->_hdrcomments ) {
      fits_write_comment( ofp, &(comment[0]), &status );
      if ( status != 0 ) {
        str.str("");
        str << "FITSFile::save_to : status = " << status << " writing comment \"" << comment << "\" to HDU " << hdunum
            << " of " << outfile;
        logger->error( str.str() );
        throw std::runtime_error( str.str() );
      }
    }
    for ( auto history : hdu->_hdrhistory ) {
      fits_write_history( ofp, &(history[0]), &status );
      if ( status != 0 ) {
        str.str("");
        str << "FITSFile::save_to : status = " << status << " writing history \"" << history << "\" to HDU " << hdunum
            << " of " << outfile;
        logger->error( str.str() );
        throw std::runtime_error( str.str() );
      }
    }

    // Write the WCS
    if ( ( _hdus[hdunum]->hdutype == FITSHDU::image_hdu ) &&
         ( ((FITSImageWrapper*)(_hdus[hdunum].get()))->wcs() != nullptr ) ) {
      logger->warn( "Writing WCS isn't yet implemented." );
    }
  }
  fits_close_file( ofp, &status );
  if ( status != 0 ) {
    str.str("");
    str << "FITSFile::save_to : status = " << status << " trying to fits_close_file " << outfile;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
}

// **********************************************************************

template<class T> int FITSFile::append_image( int width, int height ) {
  logger->error( "FITSFile : this version of append_image is not implemented" );
  throw std::runtime_error( "FITSFile : this version of append_image is not implemented" );
}

template<class T> int FITSFile::append_image( gal_data_t* gdata ) {
  std::stringstream str("");
  if ( gdata->type != FITSImage<T>::image_datatype ) {
    str.str("");
    str << "FITSFile::append_image : tried to create fits file of type " << FITSImage<T>::image_datatype
        << " with gal_data_t type of " << gdata->type;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  int hdunum = _hdus.size();
  FITSImage<T>* img = new FITSImage<T>( logger );
  img->parentfile = this;
  img->set_hdunum( hdunum );
  img->galdata = std::unique_ptr<gal_data_t, gal_data_t_deleter>( gdata, gal_data_t_deleter() );
  img->voidimagedata = gdata->array;
  img->imagedata = (T*)(img->voidimagedata);
  img->_axlen.resize( gdata->ndim );
  for ( int i = 0 ; i < gdata->ndim ; ++i ) img->_axlen[i] = gdata->dsize[gdata->ndim - 1 -i];

  if ( gdata->wcs != nullptr ) {
    img->set_wcs( WCSWrapper::wrap( gdata->wcs, logger ) );
  }

  add_hdu( std::move( std::unique_ptr<FITSHDU>( (FITSHDU*)img ) ) );

  return hdunum;
}

template int FITSFile::append_image<float>( gal_data_t* gdata );
template int FITSFile::append_image<double>( gal_data_t* gdata );
template int FITSFile::append_image<int16_t>( gal_data_t* gdata );
template int FITSFile::append_image<uint16_t>( gal_data_t* gdata );
template int FITSFile::append_image<uint8_t>( gal_data_t* gdata );


// **********************************************************************
// **********************************************************************
// **********************************************************************
// FITSHDU

// **********************************************************************

const int FITSHDU::image_hdu = IMAGE_HDU;
const int FITSHDU::ascii_tbl = ASCII_TBL;
const int FITSHDU::binary_tbl = BINARY_TBL;

const int FITSHDU::type_uint8 = GAL_TYPE_UINT8;
const int FITSHDU::type_uint16 = GAL_TYPE_UINT16;
const int FITSHDU::type_int16 = GAL_TYPE_INT16;
const int FITSHDU::type_float = GAL_TYPE_FLOAT32;
const int FITSHDU::type_double = GAL_TYPE_FLOAT64;

const int FITSHDU::hdr_bool = TLOGICAL;
const int FITSHDU::hdr_string = TSTRING;
const int FITSHDU::hdr_int = TINT;
const int FITSHDU::hdr_double = TDOUBLE;

// **********************************************************************

FITSHDU::FITSHDU( FITSFile *parentfile, int hdunum, std::shared_ptr<spdlog::logger> logger )
  : parentfile(parentfile), logger(logger)
{
  set_hdunum( hdunum );
  read_header();
}


// **********************************************************************

FITSHDU::FITSHDU( std::shared_ptr<spdlog::logger> logger )
  : logger(logger)
{
  parentfile = nullptr;
  set_hdunum( -1 );
  clear_header();
}

// **********************************************************************

FITSHDU::~FITSHDU()
{
}

// **********************************************************************

void FITSHDU::clear_header()
{
  _hdrkeywords.clear();
  _hdrdtype.clear();
  _hdrkwcomments.clear();
  _hdrstringvalues.clear();
  _hdrboolvalues.clear();
  _hdrintvalues.clear();
  _hdrdoublevalues.clear();
  _hdrcomments.clear();
  _hdrhistory.clear();
}

// **********************************************************************
// Sadly, none of the CFITSIO or GNU Astronomy Library header routines
// really do what I want, that is, allow me to read the header in such
// a way that I could build a list from which I could later write another
// header.

void FITSHDU::read_header()
{
  int status = 0;
  std::stringstream str("");
  std::string ofname{ "" };

  if ( parentfile->filename.size() == 0 ) {
    logger->error( "FITSHDU::read_header : tried to read an HDU header from a FITSFile "
                   "not associated with a filename" );
    throw std::runtime_error( "FITSHDU::read_header : tried to read an HDU header from a FITSFile "
                              "not associated with a filename" );
  }

  clear_header();

  fitsfile* fptr = gal_fits_hdu_open( &(parentfile->filename[0]), &(_hdustr[0]), READONLY, 0 );
  if ( fptr == nullptr ) {
    str.str("");
    str << "FITSHDU::read_header : error getting hdu " << _hdunum << " from " << parentfile->filename;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  fitsfile *hdrptr = nullptr;
  bool mustclosehdrptr = false;

  if ( fits_is_compressed_image( fptr, &status ) ) {
    if ( status != 0 ) {
      str.str("");
      str << "FITSHDU::read_header : status = " << status << " from fits_is_compressed_image";
      logger->error( str.str() );
      fits_close_file( fptr, &status );
      throw std::runtime_error( str.str() );
    }
    // I'm irritated that cfitsio doesn't have something that lets me
    //   just read in the equivalent decompressed header.  All I've
    //   found is a routine that will copy the decompresed header to
    //   another file.  By reading cfitsio source code, I figured out
    //   that I could write it to memory (doubtless the mem:// was
    //   documented somewhere, but I didn't find it), but I still think
    //   we're going through decompressing the whole image, which is
    //   gratuitous for just reading the header.  Sigh.
    fits_create_file( &hdrptr, "mem://", &status );
    if ( status != 0 ) {
      str.str("");
      str << "FITSHDU::read_header : status = " << status << " after fits_create_file of temp header "
          << ofname;
      logger->error( str.str() );
      fits_close_file( fptr, &status );
      throw std::runtime_error( str.str() );
    }
    fits_img_decompress_header( fptr, hdrptr, &status );
    if ( status != 0 ) {
      str.str("");
      str << "FITSHDU::read_header : status = " << status << " after fits_img_decompress_header";
      logger->error( str.str() );
      fits_close_file( fptr, &status );
      throw std::runtime_error( str.str() );
    }
    mustclosehdrptr = true;
  }
  else {
    hdrptr = fptr;
  }

  // I don't think cfitsio has something that detects header keyword types, so I have to roll my own.

  int keysexist;
  int morekeys;
  fits_get_hdrspace( hdrptr, &keysexist, &morekeys, &status );
  if ( status != 0 ) {
    str.str("");
    str << "FITSHDR::read_header : status = " << status << " from fits_get_hdrspace";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  for ( int origdex = 1 ; origdex <= keysexist ; ++origdex ) {
    char keyword[81];
    char value[81];
    char comment[81];
    int length;
    fits_read_keyn( hdrptr, origdex, keyword, value, comment, &status );
    if ( status != 0 ) {
      str.str("");
      str << "FITSHDR::read_header : status = " << status << " from fits_read_keyn";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    if ( strcmp( keyword, "COMMENT" ) == 0 ) {
      _hdrcomments.push_back( std::string( comment ) );
    }
    else if ( strcmp( keyword, "HISTORY" ) == 0 ) {
      _hdrhistory.push_back( std::string( comment ) );
    }
    else {
      std::string kw( keyword );

      // Skip some that will be set automatically
      if ( ( kw == "SIMPLE" ) || ( kw == "BITPIX" ) || ( kw == "NEXTEND" ) || ( kw.substr( 0, 5 ) == "NAXIS" ) )
        continue;

      // str.str("");
      // str << "setting header kw \"" << kw << "\" with value \"" << value
      //     << "\" and comment \"" << comment << "\"";
      // logger->debug( str.str() );

      _hdrkeywords.push_back( kw );
      _hdrkwcomments[ kw ] = std::string( comment );

      if ( value[0] == '\'' ) {
        int dex = 1;
        for ( ; dex != '\0' ; ++dex ) {
          // I don't actually know if FITS allows for escaped quotes
          if ( ( value[dex] == '\'' ) && ( value[dex-1] != '\\' ) ) break;
        }
        _hdrdtype[ kw ] = FITSHDU::hdr_string;
        _hdrstringvalues[ kw ] = std::string( &(value[1]), dex-1 );
      }
      else {
        bool foundbool = false;
        bool isboolean = true;
        bool boolval = false;
        int dex = 0;
        for ( ; value[dex] != '\0' ; ++dex ) {
          if ( ( !foundbool ) && ( value[dex] == 'T' ) ) {
            foundbool = true;
            boolval = true;
          }
          else if ( ( !foundbool ) && ( value[dex] == 'F' ) ) {
            foundbool = true;
            boolval = false;
          }
          else if ( value[dex] != ' ' ) {
            isboolean = false;
            break;
          }
        }
        if ( isboolean ) {
          _hdrdtype[ kw ] = FITSHDU::hdr_bool;
          _hdrboolvalues[ kw ] = boolval;
        }
        else {
          std::string valstr( value );
          bool isdouble = ( ( valstr.find( '.' ) != std::string::npos ) ||
                            ( valstr.find( 'e' ) != std::string::npos ) ||
                            ( valstr.find( 'E' ) != std::string::npos ) );
          if ( isdouble ) {
            _hdrdtype[kw] = FITSHDU::hdr_double;
            _hdrdoublevalues[kw] = std::stod( valstr );
          }
          else {
            _hdrdtype[kw] = FITSHDU::hdr_int;
            try {
              _hdrintvalues[kw] = std::stol( valstr );
            }
            catch ( const std::exception &e ) {
              logger->error( "Bad things have happened." );
              throw e;
            }
          }
        }
      }
    }
  }

  fits_close_file( fptr, &status );
  if ( mustclosehdrptr ) fits_close_file( hdrptr, &status );
}

// **********************************************************************

const std::string FITSHDU::hdrsfmt( const std::string& kw ) const {
  std::stringstream str("");

  if ( _hdrdtype.at(kw) == FITSHDU::hdr_string ) {
    return _hdrstringvalues.at(kw);
  }
  else if ( _hdrdtype.at(kw) == FITSHDU::hdr_bool ) {
    str << std::setw(20) << ( _hdrboolvalues.at(kw) ? "T" : "F" );
  }
  else if ( _hdrdtype.at(kw) == FITSHDU::hdr_int ) {
    str << std::setw(20) << _hdrintvalues.at(kw);
  }
  else if ( _hdrdtype.at(kw) == FITSHDU::hdr_double ) {
    // C++ streams don't have a way to truncate trailing zeros from after decimal places.
    // It's very annoying.  Fortunately, sprintf does.  Still.  Gratuitous extra steps.
    char buffer[81];
    sprintf( buffer, "%20.12g", _hdrdoublevalues.at(kw) );
    str << buffer;
  }
  else {
    str.str("");
    str << "FITSHDU::hdrsfmt : unknown header type " << _hdrdtype.at(kw) << " for header " << kw;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  return str.str();
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

void gal_data_t_deleter::operator()( gal_data_t *g ) {
  // std::cerr << "Deleting a gal_data_t" << std::endl;
  gal_data_free(g);
}

// **********************************************************************

FITSImageWrapper::FITSImageWrapper( std::shared_ptr<spdlog::logger> logger )
  : FITSHDU( logger )
{
  init();
}

// **********************************************************************

FITSImageWrapper::FITSImageWrapper( FITSFile *parentfile, int hdunum, std::shared_ptr<spdlog::logger> logger)
  : FITSHDU( parentfile, hdunum, logger)
{
  init();
}

// **********************************************************************

void FITSImageWrapper::init()
{
  hdutype = FITSHDU::image_hdu;
  galdata = nullptr;
  voidimagedata = nullptr;
  _axlen.clear();
  _wcs = nullptr;
  datatype = 255;   // ...want something that's not real... not sure what to do
  datatypesize = 0;  // Likewise
}

// **********************************************************************

FITSImageWrapper::~FITSImageWrapper()
{
  // Make sure that gal_data_free won't free the wcs, as that pointer is owned
  // by the WCSWrapper
  galdata->wcs = nullptr;
}

// **********************************************************************

FITSImageWrapper* FITSImageWrapper::make_right_FITSImage_type( const int wantedtype, FITSFile *parentfile,
                                                               int hdunum, std::shared_ptr<spdlog::logger> logger )
{
  if ( wantedtype == FITSHDU::type_uint8 ) {
    return (FITSImageWrapper*)( new FITSImage<uint8_t>( parentfile, hdunum, logger ) );
    // return std::static_pointer_cast<FITSImageWrapper, FITSImage<uint8_t>>(
    //   std::shared_ptr<FITSImage<uint8_t>>( new FITSImage<uint8_t>( parentfile, hdunum, logger ) ) );
  }
  else if ( wantedtype == FITSHDU::type_uint16 ) {
    return (FITSImageWrapper*)( new FITSImage<uint16_t>( parentfile, hdunum, logger ) );
  }
  else if ( wantedtype == FITSHDU::type_int16 ) {
    return (FITSImageWrapper*)( new FITSImage<int16_t>( parentfile, hdunum, logger ) );
  }
  else if ( wantedtype == FITSHDU::type_float ) {
    return (FITSImageWrapper*)( new FITSImage<float>( parentfile, hdunum, logger ) );
  }
  else if ( wantedtype == FITSHDU::type_double ) {
    return (FITSImageWrapper*)( new FITSImage<double>( parentfile, hdunum, logger ) );
  }
  else {
    std::stringstream str("");
    str << "FITSImageWrapper::make_right_FITSImage_type : unknown data type " << wantedtype;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
}

// **********************************************************************

void FITSImageWrapper::read_image_data() {

  if ( typeid( *this ) == typeid( FITSImage<float> ) ) {
    ((FITSImage<float>*)this)->actually_read_image_data();
  }
  else if ( typeid( *this ) == typeid( FITSImage<double> ) ) {
    ((FITSImage<double>*)this)->actually_read_image_data();
  }
  else if ( typeid( *this ) == typeid( FITSImage<int16_t> ) ) {
    ((FITSImage<int16_t>*)this)->actually_read_image_data();
  }
  else if ( typeid( *this ) == typeid( FITSImage<uint16_t> ) ) {
    ((FITSImage<uint16_t>*)this)->actually_read_image_data();
  }
  else if ( typeid( *this ) == typeid( FITSImage<uint8_t> ) ) {
    ((FITSImage<uint8_t>*)this)->actually_read_image_data();
  }
  else {
    std::stringstream str("");
    str << "FITSImageWrapper::read_image_data : unknown T for FITSImage<T>";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
}

// **********************************************************************

void FITSImageWrapper::set_wcs( std::shared_ptr<WCSWrapper> wcs ) {
  if ( galdata->wcs != nullptr ) {
    std::stringstream str("");
    str << "galdata->wcs is not nullptr, there may be a memory leak!";
    logger->warn( str.str() );
    // ... shouldn't really be a worry, because if I've done things right
    // the only galdata->wcs that will ever get set will be here,
    // so the galdata->wcs will be wrapped by a WCSWrapper
  }
  _wcs = wcs;
  if ( _wcs != nullptr )
    galdata->wcs = _wcs->wcsprm();
  else
    galdata->wcs = nullptr;
}

// **********************************************************************

std::shared_ptr<char[]> FITSImageWrapper::copy_void_image_data()
{
  long totsize = 1;
  for ( auto l = _axlen.cbegin() ; l != _axlen.cend() ; ++l ) totsize *= *l;
  auto data = std::shared_ptr<char[]>( new char[ totsize * datatypesize ] );
  memcpy( data.get(), voidimagedata, totsize * datatypesize );
  return data;
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

template<class T> FITSImage<T>::FITSImage( FITSFile *parentfile, int hdunum, std::shared_ptr<spdlog::logger> logger )
  : FITSImageWrapper( parentfile, hdunum, logger )
{
  datatype = image_datatype;
  datatypesize = sizeof(T);
  imagedata = nullptr;
}

template<class T> FITSImage<T>::FITSImage( std::shared_ptr<spdlog::logger> logger )
  : FITSImageWrapper( logger )
{
  datatype = image_datatype;
  datatypesize = sizeof(T);
  imagedata = nullptr;
}

template<class T> FITSImage<T>::~FITSImage()
{
}

// **********************************************************************

template<class T> void FITSImage<T>::actually_read_image_data()
{
  std::stringstream str("");
  int status = 0;

  if ( parentfile->filename.size() == 0 ) {
   logger->error( "FITSImage::actually_read_image_data : tried to read image data for a "
                   "FITSFile not associated with a filename" );
    throw std::runtime_error( "FITSImage::read_image_data : tried to read image data for a "
                              "FITSFile not associated with a filename" );
  }

  voidimagedata = nullptr;
  _wcs = nullptr;
  _axlen.clear();

  fitsfile *fptr = gal_fits_hdu_open( &(parentfile->filename[0]), &(_hdustr[0]), READONLY, 0 );
  if ( fptr == nullptr ) {
    str.str("");
    str << "FITSImage::actually_read_image_data : Failed to open HDU " << _hdunum << " of " << parentfile->filename;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  // Get image dimensions
  // This is perverse.  It seems that dsize is *backwards*,
  // so that dsize[0] is the y-size and dsize[1] is the x-size.
  // (dsize[0] is NAXIS2 and dsize[1] is NAXIS1).
  int type;
  size_t ndim;
  size_t *dsize = nullptr;
  char *name = nullptr;
  char *unit = nullptr;
  gal_fits_img_info( fptr, &type, &ndim, &dsize, &name, &unit );
  _axlen.resize( ndim );
  for ( int i = 0 ; i < ndim ; ++i ) _axlen[i] = dsize[ndim - 1 - i];
  if ( name != nullptr ) free( name );
  if ( unit != nullptr ) free( unit );
  if ( dsize != nullptr ) free( dsize );

  if ( ndim > 0 ) {
    gal_data_t *tmp = gal_fits_img_read( &(parentfile->filename[0]), &(_hdustr[0]), BIGASS, 0 );
    galdata = std::unique_ptr<gal_data_t, gal_data_t_deleter>( tmp, gal_data_t_deleter() );
    if ( galdata->type != datatype ) {
      str.str("");
      str << "FITSImage::actually_read_image_data : Image data type is " << galdata->type
          << ", but I expected " << datatype;
      logger->error( str.str() );
      fits_close_file( fptr, &status );
      throw std::runtime_error( str.str() );
    }
    // I'm not convinced I'm using the shared pointer right here.
    // The array structure in the gnu astro library is a void*
    // I want shared_ptr to know how to delete it.
    // I have no idea how it was allocated, but I'm guessin that
    // telling C++ it's a byte array will do the right thing...?
    voidimagedata = galdata->array;
    imagedata = (T*)voidimagedata;
    set_wcs( WCSWrapper::wrap( galdata->wcs, logger ) );
  }
  fits_close_file( fptr, &status );
}

// **********************************************************************

template<class T> std::shared_ptr<FITSImage<T>> FITSImage<T>::create( size_t nx, size_t ny, const T* data,
                                                                      std::shared_ptr<spdlog::logger> logger ) {
  size_t dsize[2]{ nx, ny };
  auto gdata = gal_data_alloc( nullptr, image_datatype, 2, dsize, nullptr, 1, BIGASS, 0, nullptr, nullptr, nullptr );
  if ( data != nullptr ) {
    memcpy( gdata->array, data, nx*ny*sizeof(T) );
  }
  auto img = std::shared_ptr<FITSImage<T>>( new FITSImage<T>( logger ) );
  img->galdata = std::unique_ptr<gal_data_t,gal_data_t_deleter>( gdata, gal_data_t_deleter() );
  img->voidimagedata = gdata->array;
  img->imagedata = (T*)(img->voidimagedata);
  img->_axlen.resize(2);
  img->_axlen[0] = nx;
  img->_axlen[1] = ny;
  return img;
}

// **********************************************************************

template<class T> template<class U>
std::shared_ptr<FITSImage<U>> FITSImage<T>::subtile( int x0, int x1, int y0, int y1 )
{
  std::stringstream str("");

  if ( _axlen.size() != 2 ) {
    str.str("");
    str << "FITSImage::subtile : can only take subtile of 2d image, this one is " << _axlen.size() << "d";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  if ( ( x0 < 0 ) || ( x1 > _axlen[0] ) || ( y0 < 0 ) || ( y1 > _axlen[1] ) || ( x0 >= x1 ) || ( y0 >= y1 ) ) {
    str.str("");
    str << "FITSImage::subtile : invalid range [" << x0 << "," << x1 << "), [" << y0 << "," << y1 << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  auto tile = std::shared_ptr<FITSImage<U>>( new FITSImage<U>( logger ) );

  size_t dsize[2];
  // Remember that GAL dsize is backwards
  dsize[0] = y1-y0;
  dsize[1] = x1-x0;
  // What should "clear" be?  Can't find documentation.  Making it 0 because... dunno.
  auto tilegaldata = gal_data_alloc( &imagedata[ y0 * _axlen[0] + x0 ],image_datatype, 2, dsize,
                                     nullptr, 0, BIGASS, 0,
                                     nullptr, nullptr, nullptr );

  // Make sure the wcs is right
  tilegaldata->block = galdata.get();
  if ( _wcs != nullptr ) gal_wcs_on_tile( tilegaldata );

  // The dances we do to free what we must and not free what we musn't
  auto newtilegaldata = gal_data_copy_to_new_type( tilegaldata, tile->image_datatype );
  tilegaldata->array = nullptr;
  gal_data_free( tilegaldata );
  tilegaldata = newtilegaldata;
  str.str("");
  str << "After gal_data_copy_to_new_type, tilegaldata->block = " << tilegaldata->block;
  logger->debug( str.str() );

  // This wcs dancing is just to avoid a warning in set_wcs that doen't matter
  auto tmpwcs = tilegaldata->wcs;
  tilegaldata->wcs = nullptr;
  tile->galdata = std::unique_ptr<gal_data_t, gal_data_t_deleter>( tilegaldata, gal_data_t_deleter() );
  tile->set_wcs( WCSWrapper::wrap( tmpwcs, logger ) );
  tile->voidimagedata = tile->galdata->array;
  tile->imagedata = (U*)(tile->voidimagedata);
  tile->_axlen.resize(2);
  tile->_axlen[0] = x1-x0;
  tile->_axlen[1] = y1-y0;

  return tile;
}

// **********************************************************************

template<class T> std::shared_ptr<T[]> FITSImage<T>::copy_image_data()
{
  long totsize = 1;
  for ( auto l = _axlen.cbegin() ; l != _axlen.cend() ; ++l ) totsize *= *l;
  auto data = std::shared_ptr<T[]>( new T[totsize] );
  memcpy( data.get(), voidimagedata, sizeof(T) * totsize );
  return data;
}

// **********************************************************************

template<class T> template<class U> std::shared_ptr<FITSImage<U>> FITSImage<T>::clone() {
  std::shared_ptr<FITSImage<U>> img = std::shared_ptr<FITSImage<U>>( new FITSImage<U>( logger ) );
  // Copy header stuff; I'm pretty sure I get deep copies here
  img->_hdrkeywords = _hdrkeywords;
  img->_hdrdtype = _hdrdtype;
  img->_hdrkwcomments = _hdrkwcomments;
  img->_hdrstringvalues = _hdrstringvalues;
  img->_hdrboolvalues = _hdrboolvalues;
  img->_hdrintvalues = _hdrintvalues;
  img->_hdrdoublevalues = _hdrdoublevalues;
  img->_hdrcomments = _hdrcomments;
  img->_hdrhistory = _hdrhistory;

  img->parentfile = nullptr;
  img->_hdunum = -1;
  img->_hdustr = "invalid";

  img->_axlen = _axlen;

  size_t *dsize = new size_t[ _axlen.size() ];
  long totalsize = 1;
  for ( int i = 0 ; i < _axlen.size() ; ++i ) {
    dsize[ _axlen.size() - i - 1 ] = _axlen[i];
    totalsize*= _axlen[i];
  }
  auto galdata = gal_data_alloc( nullptr, datatype, _axlen.size(), dsize,
                                 nullptr, 1, BIGASS, 0, nullptr, nullptr, nullptr );
  delete[] dsize;
  // Double or maybe triple copy here, which is sad, but I think the way
  //  I've done shared pointers means that I have to do that since
  //  galdata will have the data but not know about the shared pointer,
  //  so I can't risk the shared pointer going out of scope.
  auto tmp = copy_image_data();
  memcpy( galdata->array, tmp.get(), sizeof(T) * totalsize );
  if ( image_datatype != img->image_datatype ) {
    galdata = gal_data_copy_to_new_type_free( galdata, img->image_datatype );
  }
  img->galdata = std::unique_ptr<gal_data_t,gal_data_t_deleter>( galdata, gal_data_t_deleter() );
  tmp = nullptr;
  img->voidimagedata = img->galdata->array;
  img->imagedata = (U*)(img->galdata->array);

  // Copy wcs
  if ( _wcs != nullptr ) {
    auto galwcs = gal_wcs_copy( _wcs->wcsprm() );
    img->set_wcs( WCSWrapper::wrap( galwcs, logger ) );
  }

  return img;
}

// **********************************************************************

// Force instantiation
template class FITSImage<float>;
template<> const uint8_t FITSImage<float>::image_datatype = GAL_TYPE_FLOAT32;

template class FITSImage<double>;
template<> const uint8_t FITSImage<double>::image_datatype = GAL_TYPE_FLOAT64;

template class FITSImage<int16_t>;
template<> const uint8_t FITSImage<int16_t>::image_datatype = GAL_TYPE_INT16;

template class FITSImage<uint16_t>;
template<> const uint8_t FITSImage<uint16_t>::image_datatype = GAL_TYPE_UINT16;

template class FITSImage<uint8_t>;
template<> const uint8_t FITSImage<uint8_t>::image_datatype = GAL_TYPE_UINT8;

template std::shared_ptr<FITSImage<float>> FITSImage<float>::subtile<float>(int,int,int,int);
template std::shared_ptr<FITSImage<double>> FITSImage<double>::subtile<double>(int,int,int,int);
template std::shared_ptr<FITSImage<uint16_t>> FITSImage<uint16_t>::subtile<uint16_t>(int,int,int,int);
template std::shared_ptr<FITSImage<int16_t>> FITSImage<int16_t>::subtile<int16_t>(int,int,int,int);
template std::shared_ptr<FITSImage<uint8_t>> FITSImage<uint8_t>::subtile<uint8_t>(int,int,int,int);
template std::shared_ptr<FITSImage<float>> FITSImage<double>::subtile<float>(int,int,int,int);
template std::shared_ptr<FITSImage<float>> FITSImage<uint16_t>::subtile<float>(int,int,int,int);
template std::shared_ptr<FITSImage<float>> FITSImage<int16_t>::subtile<float>(int,int,int,int);
template std::shared_ptr<FITSImage<float>> FITSImage<uint8_t>::subtile<float>(int,int,int,int);
template std::shared_ptr<FITSImage<double>> FITSImage<float>::subtile<double>(int,int,int,int);
template std::shared_ptr<FITSImage<double>> FITSImage<uint16_t>::subtile<double>(int,int,int,int);
template std::shared_ptr<FITSImage<double>> FITSImage<int16_t>::subtile<double>(int,int,int,int);
template std::shared_ptr<FITSImage<double>> FITSImage<uint8_t>::subtile<double>(int,int,int,int);
template std::shared_ptr<FITSImage<uint16_t>> FITSImage<uint8_t>::subtile<uint16_t>(int,int,int,int);
template std::shared_ptr<FITSImage<uint16_t>> FITSImage<int16_t>::subtile<uint16_t>(int,int,int,int);
template std::shared_ptr<FITSImage<int16_t>> FITSImage<uint8_t>::subtile<int16_t>(int,int,int,int);

template std::shared_ptr<FITSImage<float>> FITSImage<float>::clone<float>();
template std::shared_ptr<FITSImage<double>> FITSImage<double>::clone<double>();
template std::shared_ptr<FITSImage<int16_t>> FITSImage<int16_t>::clone<int16_t>();
template std::shared_ptr<FITSImage<uint16_t>> FITSImage<uint16_t>::clone<uint16_t>();
template std::shared_ptr<FITSImage<uint8_t>> FITSImage<uint8_t>::clone<uint8_t>();
template std::shared_ptr<FITSImage<float>> FITSImage<double>::clone<float>();
template std::shared_ptr<FITSImage<float>> FITSImage<int16_t>::clone<float>();
template std::shared_ptr<FITSImage<float>> FITSImage<uint16_t>::clone<float>();
template std::shared_ptr<FITSImage<float>> FITSImage<uint8_t>::clone<float>();
template std::shared_ptr<FITSImage<double>> FITSImage<float>::clone<double>();
template std::shared_ptr<FITSImage<double>> FITSImage<int16_t>::clone<double>();
template std::shared_ptr<FITSImage<double>> FITSImage<uint16_t>::clone<double>();
template std::shared_ptr<FITSImage<double>> FITSImage<uint8_t>::clone<double>();
template std::shared_ptr<FITSImage<uint16_t>> FITSImage<uint8_t>::clone<uint16_t>();
template std::shared_ptr<FITSImage<uint16_t>> FITSImage<int16_t>::clone<uint16_t>();
template std::shared_ptr<FITSImage<int16_t>> FITSImage<uint8_t>::clone<int16_t>();


// **********************************************************************
// **********************************************************************
// **********************************************************************

// template<class T> FITSTile<T>::FITSTile( FITSImage<T> *parentimage, std::shared_ptr<spdlog::logger> logger )
//   : FITSImage<T>( logger )
// {
//   this->parentimage = parentimage;
// }

// template<class T> FITSTile<T>::~FITSTile() {
//   // Make sure this isn't freed independently of the parent
//   if ( this->galdata != nullptr ) this->galdata->array = nullptr;
// }

// // **********************************************************************

// template<class T> std::shared_ptr<char[]> FITSTile<T>::copy_void_image_data()
// {
//   long totsize = 1;
//   for ( auto l = this->_axlen.cbegin() ; l != this->_axlen.cend() ; ++l ) totsize *= *l;
//   auto data = std::shared_ptr<char[]>( new char[ totsize * sizeof(T) ] );
//   for ( int j = 0 ; j < this->_axlen[1] ; ++j ) {
//     memcpy( data.get() + (j * this->_axlen[0] * sizeof(T)) ,
//             (char*)(this->voidimagedata) + (j * parentimage->_axlen[0] * sizeof(T)),
//             this->_axlen[0] * sizeof(T) );
//   }
//   return data;
// }

// template<class T> std::shared_ptr<T[]> FITSTile<T>::copy_image_data()
// {
//   long totsize = 1;
//   for ( auto l = this->_axlen.cbegin() ; l != this->_axlen.cend() ; ++l ) totsize *= *l;
//   auto data = std::shared_ptr<T[]>( new T[totsize] );
//   for ( int j = 0 ; j < this->_axlen[1] ; ++j ) {
//     memcpy( data.get() + (j*this->_axlen[0]),
//             this->imagedata + (j * parentimage->_axlen[0]),
//             this->_axlen[0] * sizeof(T) );
//   }
//   return data;
// }

// // **********************************************************************

// // Force instantiation
// template class FITSTile<float>;
// template class FITSTile<double>;
// template class FITSTile<int16_t>;
// template class FITSTile<uint16_t>;
// template class FITSTile<uint8_t>;
