# This makefile is a bit ugly
# It has hacks to incorporate libstumbler.so
#   from git submodule ../extern/stumbler,
#   as well as argparser.o from
#   the argparser submodule underneath
#   tests in the stumbler submodule.
# The argparser.o rule doesn't check
#   that argparser.hh hasn't changed.
# The libstumbler.so rule doesn't check
#   that the libstumbler.so inside
#   the submodule is up to date, it just
#   makes it if it's not there.

LIBRARY_MAJOR = 0
LIBRARY_MINOR = 1
LIBRARY_STEPPING = 0

objects = psfexreader.o wcswrapper.o fitsfile.o
mpiobjs = psffit.o
libdir = ../../lib
libraries = libcurveball

additionalobjects = argparser.o

HDF5_INCLUDE = -I/usr/include/hdf5/serial

# addedCFlags = -DNDEBUG -O3
addedCFlags = -g -DNDEBUG # -pg
MPICXX = mpicxx
CCFLAGS = -std=c++20 -fopenmp -I../include \
	-I../extern/stumbler/include -I../extern/stumbler/test/ \
	$(HDF5_INCLUDE) -fPIC -pthread $(addedCFlags)
LIBS = -L$(libdir) -lstumbler -L/usr/lib/x86_64-linux-gnu \
	-lcfitsio -lwcs -lgnuastro -lm -lfmt -lgsl -lfftw3 -lfftw3_omp -lgomp \
	-L/usr/lib/x86_64-linux-gnu/hdf5/serial -lhdf5_cpp -lhdf5 -pthread $(addedCFlags)

# Library targets

libinst = $(patsubst %,$(libdir)/%.so.$(LIBRARY_MAJOR).$(LIBRARY_MINOR).$(LIBRARY_STEPPING),$(libraries))
libstatic = $(patsubst %,%.a,$(libraries))

shared: sharedliblinks sharedlib
sharedlib: $(libinst)
sharedliblinks: sharedlib
	$(foreach libbase, $(libraries), \
	  ln -sf $(libbase).so.$(LIBRARY_MAJOR).$(LIBRARY_MINOR).$(LIBRARY_STEPPING) \
                $(libdir)/$(libbase).so.$(LIBRARY_MAJOR).$(LIBRARY_MINOR) ; \
	  ln -sf $(libbase).so.$(LIBRARY_MAJOR).$(LIBRARY_MINOR) $(libdir)/$(libbase).so.$(LIBRARY_MAJOR) ; \
	  ln -sf $(libbase).so.$(LIBRARY_MAJOR) $(libdir)/$(libbase).so \
	)

static: $(libstatic)

# Custom dependences

psfexreader.o: ../include/psfexreader.hh
wcswrapper.o: ../include/wcswrapper.hh
fitsfile.o: ../include/fitsfile.hh ../include/wcswrapper.hh
psffit.o: ../include/psfexreader.hh ../include/psffit.hh

# Standard rules

# %.a: $(objects) $(mpiobjs)
#	rm -f $@
#	ar cr $@ $^

$(libinst): %: $(objects) $(mpiobjs) $(additionalobjects)
	$(MPICXX) --shared -o $@ $(objects) $(mpiobjs) $(additionalobjects) $(LIBS)
	strip $@

$(libstatic): %: $(objects) $(mpiobjs) $(additionalobjects)
	rm -f $@
	ar cr $@ $(objects) $(mpiobjs) $(additionalobjects)

$(mpiobjs): %.o: %.cc
	$(MPICXX) -c $(CCFLAGS) -o $@ $<

$(objects): %.o: %.cc
	$(CXX) -c $(CCFLAGS) -o $@ $<


# Custom rules

argparser.o: ../extern/stumbler/test/argparser/argparser.cc
	g++ -c -o argparser.o $(CCFLAGS) ../extern/stumbler/test/argparser/argparser.cc

# Cleanup

clean:
	rm -f *.o *.a *.so *.so.*
	rm -f $(libdir)/*.so* $(libdir)/*.a
