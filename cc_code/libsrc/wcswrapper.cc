#include "wcswrapper.hh"
#include <sstream>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <gnuastro/wcs.h>

const size_t ONEGIG = 1 << 30;

// **********************************************************************

WCSWrapper::WCSWrapper( std::shared_ptr<spdlog::logger> inlogger )
{
  if ( inlogger == nullptr ) {
    make_logger();
  } else {
    logger = inlogger;
  }
}

WCSWrapper::~WCSWrapper() {
  if ( _wcs != nullptr ) gal_wcs_free( _wcs );
}

// **********************************************************************

void WCSWrapper::make_logger() {
  logger = spdlog::stderr_color_mt( "WCSWrapper" );
  logger->set_level( spdlog::level::info );
}

// **********************************************************************

std::shared_ptr<WCSWrapper> WCSWrapper::wrap( struct wcsprm* wcstowrap, std::shared_ptr<spdlog::logger> logger ) {
  if ( wcstowrap == nullptr ) return nullptr;
  std::shared_ptr<WCSWrapper> wcs = std::make_shared<WCSWrapper>( logger );
  wcs->_wcs = wcstowrap;
  return wcs;
}

// **********************************************************************

std::shared_ptr<WCSWrapper> WCSWrapper::read_from_file( std::string &filename, int hdunum,
                                                        std::shared_ptr<spdlog::logger> logger )
{
  std::stringstream str("");

  std::string hdustr = std::to_string(hdunum);
  if ( gal_fits_hdu_format( &(filename[0]), &(hdustr[0]) ) != IMAGE_HDU ) {
    str.str("");
    str << "WCSWrapper::read_from_file: FITS file hdu " << hdunum << " is not an image! (" << filename << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  int nwcs = 0;
  auto galwcs = gal_wcs_read( &(filename[0]), &(hdustr[0]), GAL_WCS_LINEAR_MATRIX_CD, 0, 0, &nwcs );
  if ( galwcs == nullptr ) {
    str.str("");
    str << "WCSWrapper::read_from_file: Failed to read WCS from " << filename;
    if ( nwcs == 0 ) {
      str << " as there are no wcses!";
      logger->debug( str.str() );
      return nullptr;
    }
    else {
      str << ", but there were " << nwcs << " wcses, so, hurm.";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
  }
  return wrap( galwcs, logger );
}

// **********************************************************************

std::pair<std::vector<double>,std::vector<double>> WCSWrapper::c_pix_to_world( const std::vector<double> &x,
                                                                               const std::vector<double> &y ) const
{
  std::stringstream str("");

  if ( x.size() != y.size() ) {
    str << "WCSWrapper::c_pix_to_world: length of x and y vectors must match!";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  size_t numcoords = x.size();
  gal_data_t* pixcoords = gal_data_alloc( nullptr, GAL_TYPE_FLOAT64, 1, &numcoords, _wcs, 0, ONEGIG, 0,
                                          nullptr, nullptr, nullptr );
  gal_list_data_add( &pixcoords,
                     gal_data_alloc( nullptr, GAL_TYPE_FLOAT64, 1, &numcoords, _wcs, 0, ONEGIG, 0,
                                     nullptr, nullptr, nullptr ) );
  for ( int i = 0 ; i < numcoords ; ++i ) {
    // +1.0 to convert from C to FITS coordinates
    ((double*)(pixcoords->array))[i] = x[i] + 1.0;
    ((double*)(pixcoords->next->array))[i] = y[i] + 1.0;
  }
  gal_data_t* worldcoords = gal_wcs_img_to_world( pixcoords, _wcs, 0 );

  std::vector<double> ra(numcoords);
  std::vector<double> dec(numcoords);
  for ( int i = 0 ; i < numcoords ; ++i ) {
    ra[i] = ((double*)(worldcoords->array))[i];
    dec[i] = ((double*)(worldcoords->next->array))[i];
  }

  gal_list_data_free( worldcoords );
  gal_list_data_free( pixcoords );

  return std::make_pair( ra, dec );
}

// **********************************************************************

std::pair<std::vector<double>,std::vector<double>> WCSWrapper::world_to_c_pix( const std::vector<double> &ra,
                                                                               const std::vector<double> &dec ) const
{
  std::stringstream str("");

  if ( ra.size() != dec.size() ) {
    str << "WCSWrapper::world_to_c_pix: length of x and y vectors must match!";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  size_t numcoords = ra.size();
  gal_data_t* worldcoords = gal_data_alloc( nullptr, GAL_TYPE_FLOAT64, 1, &numcoords, _wcs, 0, ONEGIG, 0,
                                            nullptr, nullptr, nullptr );
  gal_list_data_add( &worldcoords,
                     gal_data_alloc( nullptr, GAL_TYPE_FLOAT64, 1, &numcoords, _wcs, 0, ONEGIG, 0,
                                     nullptr, nullptr, nullptr ) );
  for ( int i = 0 ; i < numcoords ; ++i ) {
    ((double*)(worldcoords->array))[i] = ra[i];
    ((double*)(worldcoords->next->array))[i] = dec[i];
  }
  gal_data_t* pixcoords = gal_wcs_world_to_img( worldcoords, _wcs, 0 );

  std::vector<double> x(numcoords);
  std::vector<double> y(numcoords);
  for ( int i = 0 ; i < numcoords ; ++i ) {
    // -1 to convert fom FITS to C coordinates
    x[i] = ((double*)(pixcoords->array))[i] - 1.0;
    y[i] = ((double*)(pixcoords->next->array))[i] - 1.0;
  }

  // Make sure gal_list_data_free doesn't free the storage of the passed vectors!
  gal_list_data_free( pixcoords );
  gal_list_data_free( worldcoords );

  return std::make_pair( x, y );
}
