import sys
import os
import numpy
import matplotlib
import argparse
from matplotlib import pyplot
import pandas

sys.path.append( os.getcwd() )
from plot_readchain import readchain


def binsearch( sorted_array, value ):
    """Given sorted_array and value, return the maximum index in
    sorted_array such that sorted_array[index]>value
    """
    if sorted_array.ndim != 1:
        raise RuntimeError( "sorted_array must be 1d" )
    low = 0
    high = sorted_array.shape[0]
    while high - low > 1:
        dex = ( low + high ) // 2
        if sorted_array[dex] > value:
            high = dex
        else:
            low = dex
    if sorted_array[low] > value:
        return low
    else:
        return high

def meancov( vals ):
    params = vals.mean( axis=0 )
    sigs = vals.std( axis=0, ddof=0 )
    cov = numpy.cov( vals, rowvar=False, ddof=0 )
    corr = cov / sigs[numpy.newaxis, :] / sigs[:, numpy.newaxis]
    return params, sigs, cov, corr

def plot( df, columns, titles, plotlimits=None, nodisplay=False, nbins=20, filename=None,
          contour=True, greyscale=False, points=True, highlight_last=0 ):
    if ( nodisplay ) and ( filename is None ):
        raise RuntimeError( "Must either specify an output filename or not set nodisplay" )

    linestyle = ""
    titlesize = 20
    axistitlesize = 18
    labelsize = 10
    datamarkersize = 5
    stepmarkersize = 0.2
    bigmarkersize = 3.0
    bigmarkercolor = "royalblue"
    pointcolor = "royalblue" if ( not ( contour or greyscale ) ) else "lavender"
    contourcolor = "maroon"
    histcolor = "maroon"
    matplotlib.rc( 'mathtext', fontset='cm' )

    if greyscale and points:
        sys.stderr.write( "Warning: setting points to False since greyscale will completely cover them\n" )
        points = False

    fig = pyplot.figure( figsize=(10,10), tight_layout=True )

    n = len( columns )

    if plotlimits is None:
        plotlimits = [ None ] * len(columns)

    for i in reversed( range( len(columns) ) ):
        coli = df[columns[i]].values
        for j in range( i, len(columns) ):
            colj = df[columns[j]].values
            ax = fig.add_subplot( n, n, j*n + i + 1 )
            ax.tick_params( axis='both', labelsize=labelsize )


            if j == i:
                plotrange = ( plotlimits[i][0], plotlimits[i][1] ) if plotlimits[i] is not None else None
                vals, bins = numpy.histogram( coli, range=plotrange, bins=nbins )

                ax.step( bins[:-1], vals, where='pre', color=histcolor )
                if ( j == len(columns)-1 ):
                    title = titles[columns[i]] if i in titles else f'${columns[i]}$'
                    ax.set_xlabel( title, fontsize=axistitlesize )
                if ( i == 0 ):
                    ax.set_ylabel( '$N$', fontsize=axistitlesize )
                if plotlimits[i] is not None:
                    ax.set_xlim( plotlimits[i][0], plotlimits[i][1] )
                else:
                    plotlimits[i] = ax.get_xlim()

            else:
                plotrange = [ [ plotlimits[i][0], plotlimits[i][1] ],
                              [ plotlimits[j][0], plotlimits[j][1] ] ]
                ax.set_xlim( plotlimits[i][0], plotlimits[i][1] )
                ax.set_ylim( plotlimits[j][0], plotlimits[j][1] )
                if points:
                    ax.plot( coli, colj, marker='o', markersize=stepmarkersize, linestyle=linestyle,
                             color=pointcolor, markerfacecolor=pointcolor, zorder=1 )
                hist, xbins, ybins = numpy.histogram2d( coli, colj, range=plotrange, bins=nbins )
                xvals = ( xbins[:-1] + xbins[1:] ) / 2.
                yvals = ( ybins[:-1] + ybins[1:] ) / 2.
                if contour:
                    # Figure out confidence intervals
                    vals = numpy.flip( numpy.sort( hist.flatten().copy() ) )
                    cumulvals = numpy.cumsum( vals / vals.sum() )
                    quantiles = [ 0.9973, 0.9545, 0.6827 ]
                    contours = []
                    for q in quantiles:
                        dex = binsearch( cumulvals, q )
                        contours.append( vals[dex] )
                    ax.contour( xvals, yvals, hist.T, levels=contours, zorder=3, colors=contourcolor )
                if greyscale:
                    ax.imshow( numpy.flipud(hist.T), extent=( xbins[0], xbins[-1], ybins[0], ybins[-1] ),
                               interpolation="None", cmap="Greys", aspect="auto", zorder=2 )
                if highlight_last > 0:
                    ax.plot( coli[-highlight_last:], colj[-highlight_last:], marker='o',
                             markersize=bigmarkersize, linestyle="None",
                             color=bigmarkercolor, markerfacecolor=bigmarkercolor, zorder=4 )
                ax.tick_params( axis='both', labelsize=labelsize )
                if ( j == len(columns)-1 ):
                    title = titles[columns[i]] if i in titles else f'${columns[i]}$'
                    ax.set_xlabel( title, fontsize=axistitlesize )
                if ( i == 0 ):
                    title = titles[columns[j]] if j in titles else f'${columns[j]}$'
                    ax.set_ylabel( title, fontsize=axistitlesize )

    if not nodisplay:
        fig.show()
        pyplot.show()
    if filename is not None:
        fig.savefig( filename )

def main():
    parser = argparse.ArgumentParser( description="Chain corner plot",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( "filename", help="filename with chain (space separated, first row = title)" )
    parser.add_argument( "-s", "--startline", type=int, default=0,
                         help="Read only starting at startline in the file" );
    parser.add_argument( "--no-contours", action='store_true', help="Don't plot contours" )
    parser.add_argument( "-p", "--points", action='store_true', help="Plot points" );
    parser.add_argument( "-l", "--highlight-last", type=int, default=0,
                         help="Plot this many of the last points (bigger than -p)." )
    parser.add_argument( "-g", "--greyscale", action='store_true', help="Plot greyscale" )
    parser.add_argument( "-b", "--bins", type=int, default=20, help="Number of bins on each axis for greysacle" )
    parser.add_argument( "-n", "--colnums", type=int, nargs='+', default=None,
                         help="Just plot these columns (by number)" )
    parser.add_argument( "-c", "--cols", nargs='+', default=[],
                         help="Just plot these columns (by name) (supercedes --colnums)" )
    parser.add_argument( "--nodisplay", action='store_true', help="Don't display to screen" )
    parser.add_argument( "-o", "--outfile", default=None, help="Save plot to this file" )
    args = parser.parse_args()

    contour = not args.no_contours
    grey = args.greyscale
    points = args.points
    bins = args.bins

    # omgplotlimits = numpy.array( [ [ 0., 1 ],
    #                                [ 0., 1. ],
    #                                [ 0., 0.1 ] ] )
    omgplotlimits = None
    abplotlimits = None

    chains = readchain( args.filename, args.cols, args.startline, 1, 0 )

    columns = chains.columns
    titles = { x: str(x) for x in columns }

    means = chains.mean()
    sdevs = chains.std()
    print( "\nMeans:" )
    for col in columns:
        print( f'{str(col):6s} : {means[col]:13.6e} ± {sdevs[col]:10.3e}' )
    print( f"\nCovariance matrix:\n{chains.cov()}" )
    print( f"\nCorrelation matrix:\n{chains.corr()}" )

    plot( chains, columns, titles, nodisplay=args.nodisplay, filename=args.outfile, contour=contour,
          points=points, highlight_last=args.highlight_last, greyscale=grey, nbins=bins )


# ======================================================================
if __name__ == "__main__":
    main()
