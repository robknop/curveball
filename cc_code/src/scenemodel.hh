#include <fitsfile.hh>
#include <psfexreader.hh>
#include <complex>
#include <vector>
#include <list>
#include <string>
#include <spdlog/spdlog.h>
#include <fftw3.h>
#include <mpi.h>
// Have to include this becasue irritatingly, gal_warp_wcsalign_t
//   is an anonymous struct in warp.h, so I can't forward declare it
#include <gnuastro/warp.h>

// **********************************************************************
// Forward declarations
// struct wcsprm;

// **********************************************************************

struct fileinfo_t {
  std::string filename;
  std::string weightname;
  std::string maskname;
  std::string psfname;
  double zp;
  double initmag;
  double mjd;
  bool isref;
};
typedef struct fileinfo_t fileinfo_t;

// **********************************************************************

class ImageClip {
private:
  std::shared_ptr<spdlog::logger> logger;
  const fileinfo_t fileinfo;
  std::shared_ptr<FITSImage<float>> imagetile;
  std::shared_ptr<FITSImage<float>> weighttile;
  std::shared_ptr<FITSImage<uint16_t>> masktile;
  // std::shared_ptr<PSFExReader> psf;
  size_t offx, offy;
  double ctrpsfx, ctrpsfy;

  std::unique_ptr<std::complex<double>[]> ctrpsf;
  std::unique_ptr<std::complex<double>[]> ctrpsfft;
  std::unique_ptr<std::complex<double>[]> cornerpsf;
  std::unique_ptr<std::complex<double>[]> cornerpsfft;

  std::unique_ptr<std::complex<double>[]> warpedgalaxymodel;
  std::unique_ptr<std::complex<double>[]> convolvedgalaxymodel;
  std::unique_ptr<double[]> shiftedpsf;
  std::unique_ptr<double[]> model;
  std::unique_ptr<std::complex<double>[]> tmpft;
  std::unique_ptr<std::complex<double>[]> tmpinvft;
  fftw_plan wrpgal_to_tmpft_plan;
  fftw_plan tmpft_to_invtmpft_plan;

  struct wcsprm *modelsourcewcs;
  gal_warp_wcsalign_t warper;
  bool warpsinitialized = false;

  void warp_galaxy_model( double* galaxymodel );
  void convolve_warped_galaxy_model();
  void shift_psf( double shiftx, double shifty );

public:
  ImageClip( const fileinfo_t &fileinfo, double intira, double initdec, int patchsize,
             std::shared_ptr<spdlog::logger> logger );

  virtual ~ImageClip();

  ImageClip() = delete;
  ImageClip( ImageClip& ) = delete;
  ImageClip& operator=( ImageClip& ) = delete;

  void initialize_warps( struct wcsprm *modelsourcewcs );

  double lnL_contrib( double* galaxymodel, double snra, double sndec, double flux, double sky );

  std::shared_ptr<FITSImage<float>> get_imagetile() { return imagetile; }
  std::shared_ptr<FITSImage<float>> get_weighttile() { return weighttile; }
  const fileinfo_t& get_fileinfo() const { return fileinfo; }

  void dump_model( const std::string &fitsfilename, bool overwrite=false );
  void dump_resid( const std::string &fitsfilename, double sky=0., bool overwrite=false );
  void dump_tile( const std::string &fitsfilename, bool overwrite=false );
  void dump_weight( const std::string &fitsfilename, bool overwrite=false );
  void dump_mask( const std::string &fitsfilename, bool overwrite=false );

  /**
   * For debugging : write out a bunch of FITS files of steps and models and such
   *
   */
  void dump_debug( const std::string &fitsfilename, bool overwrite=false );

  static const double STDZP;
};

// **********************************************************************

class SceneModel {
private:
  bool initialized = false;
  bool havegone = false;
  MPI_Comm mpicomm = MPI_COMM_WORLD;
  int mpirank=0;
  int mpisize=1;
  const std::string &ltcvfile;
  const std::string &chainfile;
  const std::string &galmodel;
  double initra, initdec;
  double ramoveprior, decmoveprior;
  int patchsize, nsteps, burnin;
  size_t nparams;
  double stretchparam = 2.0;
  std::shared_ptr<spdlog::logger> logger;

  std::vector<fileinfo_t> fileinfo;
  std::vector<std::shared_ptr<ImageClip>> imageclips;
  int news;

  void read_file( const std::string &filename );
  void dump_fileinfo();
  void write_ltcv( const std::vector<std::vector<double>> &meanvar,
                   const std::vector<std::vector<double>> &covar,
                   const std::string &ltcvfile );
  void write_chain( const std::list<std::vector<double>> &chain, const std::string &chainfile );

  ImageClip *galmodelrefim;
  struct wcsprm *galmodelrefimwcs;

  /**
   * Parameters to pass to stumbler.
   *
   * The first patchsize*patchsize parameters are the galaxy model.
   * It's here like this so that I can memcpy directly from it in
   * order to wrap FITS images and do warps.
   *
   * The next two parameters are the RA and Dec of the SN
   *
   * The next nfiles-1 parameters are sky values for all the frames.
   * (Because of a degeneracy with the galaxy model, the first ref's
   * sky is assumed to be 0.)
   *
   * The remaining nnew parameters are the fluxes of the SN on each of
   * the new frames.  nnews will be equal to the number of elements
   * of imageclips that have fileinfo.isfref = false.
   *
   */
  std::vector<double> initparams;
  std::vector<double> initparamsig;
  std::vector<std::string> paramnames;

  std::vector<std::vector<double>> meanvar;
  std::vector<std::vector<double>> covar;

  /**
   * Calculate the thing that Stumbler will try to maximize.
   *
   * SIDE EFFECT : loads up the model tiles in the image clips
   *
   */
  double loglikelihood( const std::vector<double> &params );

public:
  /**
   * \brief Create a scene modeller
   *
   * @param filename a text file that has on each line:
   *                @code{new/ref mjd zp snmag filebase}
   *            where @code{new/ref} is either the string "new" or the
   *            string "ref" (in the latter case, no psf will be placed
   *            on the image when modeling), @code{mjd} is the MJD of
   *            the image, and @code{zp} is the zeropoint of the image,
   *            @code{snmag} is an initial guess at the magnitude of the
   *            supernova in this image, and @code{filebase} is the path
   *            of the image file with the .fits stripped off.  The code
   *            assumes that @code{<filebase>.fits},
   *            @code{<filebase>.weight.fits},
   *            @code{<filebase>mask.fits}, and @code{<filebase>.psf}
   *            all exit.  The mask file is expected to have a datatype
   *            of one of @code{uint8_t}, @code{int16_t}, or
   *            @code{uint16_t}, has has 0 for good pixels, !=0 for bad
   *            pixels.  The weight file is inverse variance for each
   *            pixel.  The psf file is the output of psfex run on a
   *            sextractor-produced catalog of the image.
   * @param ra the initial RA of the supernova
   * @param dec the initial dec of the supernova
   * @param patchsize the patch size in pixels to fit.  IMPLICIT ASSUMPTION : all the images
   *                  come from the same telescope!
   * @param steps the number of post-burn steps all the walkers should
   *              take.  (The number of walkers will be twice the number
   *              of parameters.)
   * @param burn the number of burn-in steps to take at the beginning
   * @param ltcvfile write the lightcurve to this file
   * @param chainfile write the chain in HDF5 format to this file, and the burn-in chain to burn_[chainfile]
   * @param galmodel write the galaxy model (NOT IMPLEMENTED)
   * @param a logger
   *
   */
  SceneModel( const std::string &filename, double ra, double dec, int patchsize, int steps, int burn,
              const std::string& ltcvfile, const std::string& chainfile,
              const std::string& galmodel, std::shared_ptr<spdlog::logger> logger=nullptr );

  virtual ~SceneModel() {}

  double get_stretchparam() { return stretchparam; }
  void set_stretchparam( double val ) { val = stretchparam; }

  /**
   * Returns whether or not the image at index n is a reference.
   *
   * Will return false also for an invalid image index;
   *
   */
  inline bool imageisref( size_t n ) {
    if ( n > imageclips.size() ) return false;
    return imageclips[n]->get_fileinfo().isref;
  }

  /**
   * Return the index into the parameters array for the galaxy model pixel at x,y.
   *
   * Returns -1 for invalid.
   *
   */
  inline int paramdex_gmodel( int x, int y ) {
    if ( x < 0 || y < 0 || x >= patchsize || y >= patchsize ) return -1;
    return y * patchsize + x;
  }
  /**
   * Return the index into the parameters array for sky level of image n.
   *
   * Returns -1 for invalid.  This will be the case for n outside the
   * range of image indexes, or for the first reference image.  (The sky level
   * of one image is degenerate with the galaxy model, so we choose the first
   * reference, in hopes that the references are already sky subtracted anyway.)
   *
   */
  inline int paramdex_sky( int imagen ) {
    if ( imagen < 0 || imagen >= imageclips.size() ) return -1;
    bool firstreffound = false;
    int skydex = patchsize*patchsize + 2;
    for ( int i = 0 ; i < imageclips.size() ; ++i ) {
      if ( (!firstreffound) && imageisref(i)) {
        firstreffound = true;
        if (i == imagen) return -1;
      }
      else {
        if ( i == imagen ) return skydex;
        ++skydex;
      }
    }
    return -1;
  }
  /**
   * Returns the index into the parameters array for the SN flux in image n;
   *
   * Returns -1 for invalid.  This will be the case for n outside the range of
   * iamge indexes, or for any reference image.
   *
   */
  inline int paramdex_flux( int imagen ) {
    if ( imagen < 0 || imagen >= imageclips.size() ) return -1;
    if ( imageisref( imagen ) ) return -1;
    int fluxdex = patchsize*patchsize + 2 + imageclips.size() - 1;
    for ( int i = 0 ; i < imageclips.size() ; ++i ) {
      if ( imagen == i ) return fluxdex;
      if ( !imageisref( i ) ) ++fluxdex;
    }
    return -1;
  }
  /**
   * Returns the index into the parmaters array for SN RA.
   *
   */
  inline int paramdex_ra() { return patchsize*patchsize; }
  /**
   * Returns the index into the parmeters array for SN Dec.
   *

   */
  inline int paramdex_dec() { return patchsize*patchsize + 1; }


  void dump_tiles( bool weight=false, bool mask=false );
  void dump_models( bool resid=true );

  /**
   * Don't use the default constructor, copy constructor, or operator=
   *
   */
  SceneModel() = delete;
  SceneModel( SceneModel& ) = delete;
  SceneModel& operator=( SceneModel& ) = delete;

  void initialize();
  void go();
};
