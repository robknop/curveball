import sys
import numpy
import h5py
import pandas

def readchain( filename, cols=[], startstride=0, stride=1, offset=0 ):
    h5file = h5py.File( filename )
    dset = h5file[ 'chain' ]
    nparam = dset.attrs[ 'nparams' ]
    paramnames = []
    for i in range(nparam):
        paramnames.append( dset.attrs[ f"param{i}" ][0] )
    parammap = { str(paramnames[i], encoding='utf-8') : i for i in range(len(paramnames)) }

    if len(cols) == 0:
        cols = paramnames

    vals = {}
    for col in cols:
        vals[ col ] = numpy.reshape( dset[ :, parammap[col] ] ,
                                     ( dset.shape[0] // stride, stride ) )[ startstride:, offset ]

    return pandas.DataFrame( vals )

def readallchains( filename, cols=[], startstride=0, stride=1 ):
    h5file = h5py.File( filename )
    dset = h5file[ 'chain' ]
    nparam = dset.attrs[ 'nparams' ]
    paramnames = []
    for i in range(nparam):
        paramnames.append( dset.attrs[ f"param{i}" ][0] )
    parammap = { str(paramnames[i], encoding='utf-8') : i for i in range(len(paramnames)) }

    if len(cols) == 0:
        cols = paramnames

    valses = []
    # Not doing this copy causes gigantic I/O explosion; this just uses lots of memory
    bigmess = numpy.copy( dset )
    for i in range(stride):
        vals = {}
        for col in cols:
            vals[ col ] = numpy.reshape( bigmess[ :, parammap[col] ] ,
                                         ( bigmess.shape[0] // stride, stride ) )[ startstride:, i ]
        valses.append( pandas.DataFrame( vals ) )
    return valses
