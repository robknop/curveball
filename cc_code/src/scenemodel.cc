#include "scenemodel.hh"
#include <fitsfile.hh>
#include <wcswrapper.hh>
#include <psfexreader.hh>
#include <argparser.hh>
#include <stumbler.hh>
#include <string>
#include <map>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <unistd.h>
#include <omp.h>
#include <mpi.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <fftw3.h>
// #include <gnuastro/warp.h>
//****
#if 0
#include <valgrind/callgrind.h>
#else
#define CALLGRIND_STOP_INSTRUMENTATION ""
#define CALLGRIND_START_INSTRUMENTATION ""
#endif
//****

const size_t BIGASS = 1073741824;
const double ImageClip::STDZP = 30.;

// **********************************************************************
// **********************************************************************
// **********************************************************************
// ImageClip methods

ImageClip::ImageClip( const fileinfo_t &fileinfo, double initra, double initdec, int patchsize,
                      std::shared_ptr<spdlog::logger> logger )
  : fileinfo(fileinfo), logger(logger)
{
  std::stringstream str("");
  str << "ImageClip: reading files for " << fileinfo.filename;
  logger->info( str.str() );

  // Read the FITS images (image, weight, mask)
  std::map<std::string,std::pair<std::string,std::shared_ptr<FITSImage<float>>*>> readlist{
    { ".fits", { fileinfo.filename, &imagetile } },
    { ".weight.fits", { fileinfo.weightname, &weighttile } },
  };
  std::vector<std::string> doorder = { ".fits", ".weight.fits" };
  std::vector<double> rav{ initra };
  std::vector<double> decv{ initdec };
  double x = -666.;
  double y = -666.;
  for ( auto i : doorder ) {
    str.str("");
    str << "ImageClip: ...reading " << readlist[i].first;
    logger->debug( str.str() );
    FITSFile f( readlist[i].first, logger );
    FITSImage<float> *img = f.get_image<float>();
    if ( i == ".fits" ) {
      if ( img->wcs() == nullptr ) {
        str.str("");
        str << "ImageClip: " << readlist[i].first << " doesn't have a WCS!";
        logger->error( str.str() );
        throw std::runtime_error( str.str() );
      }
      auto xy = img->wcs()->world_to_c_pix( rav, decv);
      x = xy.first[0];
      y = xy.second[0];
      if ( ( x < patchsize/2 + 1 ) || ( x > img->axlen(0) - patchsize/2 - 1 ) ||
           ( y < patchsize/2 + 1 ) || ( y > img->axlen(1) - patchsize/2 - 1 ) ) {
        str.str("");
        str << "ImageClip: file " << readlist[i].first << " has SN at " << x << ", " << y
            << " which is outside the image or too close to the edge.";
        logger->error( str.str() );
        throw std::runtime_error( str.str() );
      }
      offx = (size_t)floor(x + 0.5) - patchsize/2;
      offy = (size_t)floor(y + 0.5) - patchsize/2;
    }
    *(readlist[i].second) = img->subtile<float>( offx, offx+patchsize, offy, offy+patchsize );
  }
  // There's probably something really clever I could do with whatever C++ uses in place of
  // reflection in order to avoid repeating code, but, whatever.  I also have to be extra
  // verbose here, because while I'm assuming that the .fits and .weight.fits files are float
  // images (BITPIX=-32), I want masks to be able to be either uint8_t or uint16_t.
  {
    str.str("");
    str << "ImageClip: ...reading " << fileinfo.maskname;
    logger->debug( str.str() );
    FITSFile f( fileinfo.maskname, logger );
    int n = f.simple_2d_image_hdu_num();
    // I'm getting memory leak here; valgrind says it's inside the gal stuff that reads the wcs.
    // This memory leak does *not* show up in my tests in ../cctests, and I can't find it in the code,
    // so for now I'm going to punt, but I need to figure out what the hell is going on.  I guess
    // there's some chance it's in the gal, but in that case it ought to have shown in my my
    // tests.
    FITSImageWrapper* imgwrap = (FITSImageWrapper*)(f.hdu(n));
    if ( imgwrap->get_datatype() == FITSHDU::type_uint8 ) {
      FITSImage<uint8_t>* img = (FITSImage<uint8_t>*)imgwrap;
      masktile = img->subtile<uint16_t>( offx, offx+patchsize, offy, offy+patchsize );
    }
    else if ( imgwrap->get_datatype() == FITSHDU::type_uint16 ) {
      FITSImage<uint16_t>* img = (FITSImage<uint16_t>*)imgwrap;
      masktile = img->subtile<uint16_t>( offx, offx+patchsize, offy, offy+patchsize );
    }
    else if ( imgwrap->get_datatype() == FITSHDU::type_int16 ) {
      FITSImage<int16_t>* img = (FITSImage<int16_t>*)imgwrap;
      masktile = img->subtile<uint16_t>( offx, offx+patchsize, offy, offy+patchsize );
    }
    else {
      str.str("");
      str << "ImageClip : mask file has data type " << imgwrap->get_datatype()
          << ", but needs to be " << FITSHDU::type_uint8 << " (uint8_t), "
          << FITSHDU::type_uint16 << " (uint16_t)" << ", or "
          << FITSHDU::type_int16 << " (int16_t)" << std::endl;
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
  }

  // Scale the image to a zeropoint of STDZP
  for ( int j = 0 ; j < patchsize ; ++j ) {
    for ( int i = 0 ; i < patchsize ; ++i ) {
      imagetile->setpix( i, j , imagetile->pix( i, j ) * std::pow( 10., ( fileinfo.zp - STDZP ) / -2.5 ) );
      // ROB THINK ABOUT IF THIS IS RIGHT
      weighttile->setpix( i, j , weighttile->pix( i, j ) / std::pow( 10., ( fileinfo.zp - STDZP ) / -1.25 ) );
    }
  }

  //Make the centered PSF and the FFT of the centered PSF
  PSFExReader psfex( fileinfo.psfname, logger );
  // The -0.5 here is an artifact of the FITS standard placing x.0,y.0
  // at the center of the pixel x,y, whereas in a lot of ways x.5,y.5
  // being the center of the pixel would make more sense.  (In the
  // former case, the image covers the region in space from (-0.5,-0.5)
  // to (nx-0.5,ny-0.5), whereas in the latter case the image covers the
  // region in space from (0,0) to (nx,ny), which is obviously much
  // simpler.  .0-centered pixels makes sense if you're *point
  // sampling*, but that's not what an astronmical image is; it's an
  // integration of the distribution over the pixel area.)  Consider a
  // 31×31 patch.  We want the PSF to be centered in the middle of pixel
  // 15 here (So to the left are pixels 0-14, and to the right are
  // pixels 16-30, i.e. 15 pixels on each side.)  However, 31./2. =
  // 15.5, which would place the PSF at the upper-right corner of pixel
  // 15.
  ctrpsfx = (double)offx + (double)patchsize/2. - 0.5;
  ctrpsfy = (double)offy + (double)patchsize/2. - 0.5;
  auto tmppsf = psfex.psf_stamp<double>( ctrpsfx, ctrpsfy, offx, offy, patchsize );
  ctrpsf = std::make_unique<std::complex<double>[]>( patchsize * patchsize );
  for ( int i = 0 ; i < patchsize*patchsize ; ++i ) {
    ctrpsf[i].real( tmppsf[i] );
    ctrpsf[i].imag( 0. );
  }

  // For convolutions, we need the psf centered around 0,0, not the middle
  // ROB -- check what happens with an even patch size!  I think it screws it up,
  // shifting things by a half-pixel.  I should probably just enforce an odd
  // patch size.  (Also, a delta function not entirely contained within one pixel
  // will introduce blurring that shouldn't happen.  So, always make sure the psf
  // is centered in the middle of a pixel, i.e. odd patch size.)
  cornerpsf = std::move( std::make_unique<std::complex<double>[]>( patchsize*patchsize ) );
  for ( int srcj = 0 ; srcj < patchsize ; ++srcj ) {
    int dstj = srcj - patchsize/2;
    if ( dstj < 0 ) dstj = patchsize + dstj;
    for ( int srci = 0 ; srci < patchsize ; ++srci ) {
      int dsti = srci - patchsize/2;
      if ( dsti < 0 ) dsti = patchsize + dsti;
      cornerpsf[ dstj*patchsize + dsti ] = ctrpsf[ srcj*patchsize + srci ];
    }
  }

  ctrpsfft = std::make_unique<std::complex<double>[]>( patchsize*patchsize );
  fftw_plan plan = fftw_plan_dft_2d( patchsize, patchsize,
                                     reinterpret_cast<fftw_complex*>(ctrpsf.get()),
                                     reinterpret_cast<fftw_complex*>(ctrpsfft.get()),
                                     FFTW_FORWARD, FFTW_ESTIMATE );
  fftw_execute( plan );
  fftw_destroy_plan( plan );

  cornerpsfft = std::make_unique<std::complex<double>[]>( patchsize*patchsize );
  fftw_plan cornerplan = fftw_plan_dft_2d( patchsize, patchsize,
                                           reinterpret_cast<fftw_complex*>(cornerpsf.get()),
                                           reinterpret_cast<fftw_complex*>(cornerpsfft.get()),
                                           FFTW_FORWARD, FFTW_ESTIMATE );
  fftw_execute( cornerplan );
  fftw_destroy_plan( cornerplan );

  // Allocate workspace that'll be used in likelihood calculations

  warpedgalaxymodel = std::make_unique<std::complex<double>[]>( patchsize*patchsize );
  convolvedgalaxymodel = std::make_unique<std::complex<double>[]>( patchsize*patchsize );
  shiftedpsf = std::make_unique<double[]>( patchsize*patchsize );
  model = std::make_unique<double[]>( patchsize*patchsize );
  tmpft = std::make_unique<std::complex<double>[]>( patchsize*patchsize );
  tmpinvft = std::make_unique<std::complex<double>[]>( patchsize*patchsize );

  wrpgal_to_tmpft_plan = fftw_plan_dft_2d( patchsize, patchsize,
                                           reinterpret_cast<fftw_complex*>(warpedgalaxymodel.get()),
                                           reinterpret_cast<fftw_complex*>(tmpft.get()),
                                           FFTW_FORWARD, FFTW_ESTIMATE );
  tmpft_to_invtmpft_plan = fftw_plan_dft_2d( patchsize, patchsize,
                                             reinterpret_cast<fftw_complex*>(tmpft.get()),
                                             reinterpret_cast<fftw_complex*>(tmpinvft.get()),
                                             FFTW_BACKWARD, FFTW_ESTIMATE );
}

// **********************************************************************

void ImageClip::initialize_warps( struct wcsprm *modelsourcewcs ) {
  this->modelsourcewcs = modelsourcewcs;

  size_t dims[2];
  dims[0] = imagetile->axlen(1);
  dims[1] = imagetile->axlen(0);
  size_t npix = dims[0] * dims[1];
  warper = gal_warp_wcsalign_template();
  warper.coveredfrac = 0;    // Don't NaN pixels even if they aren't covered
  warper.edgesampling = 0;   // I need to figure out what this means
  warper.numthreads = 0;     // I believe this doesn't matter since I'm manually calling gal_warp_wcsalign_onpix
  warper.input = gal_data_alloc( nullptr, FITSHDU::type_double, 2, dims,
                                 modelsourcewcs, 0, BIGASS, 0, nullptr, nullptr, nullptr );
  warper.twcs = imagetile->wcs()->wcsprm();

  size_t *warpdsize = (size_t*)malloc( sizeof(size_t) * 2 );  // malloc since gal_data_free will free() it
  warpdsize[0] = imagetile->axlen(1);
  warpdsize[1] = imagetile->axlen(0);
  size_t two = 2;
  warper.widthinpix = gal_data_alloc( warpdsize, GAL_TYPE_SIZE_T, 1, &two, nullptr,
                                      1, -1, 0, nullptr, nullptr, nullptr );
  gal_warp_wcsalign_init( &warper );
  if ( warper.output->type != FITSHDU::type_double ) {
    std::stringstream str("");
    str << "gal_warp_wcsalign_init makde output type " << warper.output->type
        << ", but I need " << FITSHDU::type_double << " (double)";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  warpsinitialized = true;
}

// **********************************************************************

ImageClip::~ImageClip()
{
  fftw_destroy_plan( wrpgal_to_tmpft_plan );
  fftw_destroy_plan( tmpft_to_invtmpft_plan );

  if ( warpsinitialized ) {
    gal_data_free( warper.output );
    gal_data_free( warper.input );
    gal_data_free( warper.widthinpix );
    gal_warp_wcsalign_free( &warper );
  }
}

// **********************************************************************

void ImageClip::dump_model( const std::string &modelname, bool overwrite )
{
  size_t patchsize = imagetile->axlen(0);
  float* imgdata = new float[ patchsize*patchsize ];
  for ( int i = 0 ; i < patchsize*patchsize ; ++i ) imgdata[i] = model[i];
  auto f = FITSFile( logger );
  auto img = FITSImage<float>::create( imagetile->axlen(0), imagetile->axlen(1), imgdata, logger );
  f.add_hdu( img );
  f.save_to( modelname, overwrite );
  delete[] imgdata;
}

// **********************************************************************

void ImageClip::dump_tile( const std::string &tilename, bool overwrite )
{
  auto f = FITSFile( logger );
  auto img = imagetile->clone<float>();
  f.add_hdu( img );
  f.save_to( tilename, overwrite );
}

void ImageClip::dump_weight( const std::string &weightname, bool overwrite )
{
  auto f = FITSFile( logger );
  auto img = weighttile->clone<float>();
  f.add_hdu( img );
  f.save_to( weightname, overwrite );
}

void ImageClip::dump_mask( const std::string &maskname, bool overwrite )
{
  auto f = FITSFile( logger );
  auto img = masktile->clone<uint16_t>();
  f.add_hdu( img );
  f.save_to( maskname, overwrite );
}

// **********************************************************************

void ImageClip::dump_resid( const std::string &residname, double sky, bool overwrite )
{
  size_t patchsize = imagetile->axlen(0);
  float* imgdata = new float[ patchsize*patchsize ];
  for ( int i = 0 ; i < patchsize*patchsize ; ++i )
    imgdata[i] = imagetile->data()[i] - model[i] - sky;
  auto f = FITSFile( logger );
  auto img = FITSImage<float>::create( imagetile->axlen(0), imagetile->axlen(1), imgdata, logger );
  f.add_hdu( img );
  f.save_to( residname, overwrite );
  delete[] imgdata;
}

// **********************************************************************

void ImageClip::dump_debug( const std::string &fitsfilename, bool overwrite )
{
  std::stringstream str("");
  int npix = imagetile->axlen(0) * imagetile->axlen(1);

  auto slashdex = fitsfilename.find_last_of( "/" );
  std::string justfilename;
  if ( slashdex == std::string::npos )
    justfilename = fitsfilename;
  else
    justfilename = fitsfilename.substr( slashdex+1 );

  str.str("");
  str << "Dumping tiles for " << justfilename << " to current dir";
  logger->debug( str.str() );

  {
    auto f = FITSFile( logger );
    auto img = imagetile->clone<float>();
    f.add_hdu( img );
    f.save_to( fitsfilename + "_tile.fits", overwrite );
  }

  {
    auto f = FITSFile( logger );
    auto img = weighttile->clone<float>();
    f.add_hdu( img );
    f.save_to( fitsfilename + "_weighttile.fits", overwrite );
  }

  {
    auto f = FITSFile( logger );
    auto img = masktile->clone<uint16_t>();
    f.add_hdu( img );
    f.save_to( fitsfilename + "_masktile.fits", overwrite );
  }

  std::map<std::string,std::complex<double>*> complexes{
    { "_cornerpsf", cornerpsf.get() },
    { "_cornerpsfft", cornerpsfft.get() },
    { "_ctrpsf", ctrpsf.get() },
    { "_ctrpsfft", ctrpsfft.get() },
    { "_warpmodel", warpedgalaxymodel.get() },
    { "_convwarp", convolvedgalaxymodel.get() },
    { "_tmpft", tmpft.get() },
    { "_tmpinvft", tmpinvft.get() }
  };
  for ( auto which : complexes ) {
    auto realf = FITSFile( logger );
    auto imagf = FITSFile( logger );
    auto rimg = FITSImage<float>::create( imagetile->axlen(0), imagetile->axlen(1), nullptr, logger );
    auto iimg = FITSImage<float>::create( imagetile->axlen(0), imagetile->axlen(1), nullptr, logger );
    for ( int i = 0 ; i < npix ; ++i ) {
      rimg->data()[i] = which.second[i].real();
      iimg->data()[i] = which.second[i].imag();
    }
    realf.add_hdu( rimg );
    imagf.add_hdu( iimg );
    realf.save_to( fitsfilename + which.first + "_r.fits", overwrite );
    imagf.save_to( fitsfilename + which.first + "_i.fits", overwrite );
  }

  std::map<std::string,double*> reals{
    { "_shiftedpsf", shiftedpsf.get() },
    { "_model", model.get() }
  };
  for ( auto which : reals ) {
    auto f = FITSFile( logger );
    auto img = FITSImage<double>::create( imagetile->axlen(0), imagetile->axlen(1), which.second, logger );
    f.add_hdu( img );
    f.save_to( fitsfilename + which.first + ".fits", overwrite );
  }
}

// **********************************************************************

void ImageClip::warp_galaxy_model( double* galaxymodel )
{
  if ( ! warpsinitialized ) {
    logger->error( "ImageClip::warp_galaxy_model : warp not initialized" );
    throw std::runtime_error( "ImageClip::warp_galaxy_model : warp not initialized" );
  }
  size_t nx = imagetile->axlen(0);
  size_t ny = imagetile->axlen(1);
  size_t npix = nx * ny;
  // if ( modelsourcewcs == imagetile->wcs()->wcsprm() ) {
  if ( false ) {
    for ( int i = 0 ; i < npix ; ++i ) {
      warpedgalaxymodel[i].real( galaxymodel[i] );
      warpedgalaxymodel[i].imag( 0. );
    }
  }
  else {
    memcpy( warper.input->array, galaxymodel, npix*sizeof(double) );
    for ( int i = 0 ; i < npix ; ++i ) {
      gal_warp_wcsalign_onpix( &warper, i );
    }
    for ( int i = 0 ; i < npix ; ++i ) {
      warpedgalaxymodel[i].real( ((double*)(warper.output->array))[i] );
      warpedgalaxymodel[i].imag( 0. );
    }
  }
}

// **********************************************************************

void ImageClip::convolve_warped_galaxy_model()
{
  fftw_execute( wrpgal_to_tmpft_plan );
  size_t npix = imagetile->axlen(0) * imagetile->axlen(1);
  for ( int i = 0 ; i < npix ; ++i ) tmpft[i] *= cornerpsfft[i];
  fftw_execute( tmpft_to_invtmpft_plan );
  for ( int i = 0 ; i < npix ; ++i ) {
    convolvedgalaxymodel[i] = tmpinvft[i].real() / npix;
  }
}

// **********************************************************************

void ImageClip::shift_psf( double shiftx, double shifty )
{
  constexpr std::complex<double> imagnum{0., 1.};
  constexpr double twopi = 2.*std::acos(-1);
  const size_t nx = imagetile->axlen(0);
  const double dnx = (double)nx;     // I bet this is something the optimizer would do all by itself
  const size_t ny = imagetile->axlen(1);
  const double dny = (double)ny;
  for ( int j = 0 ; j < ny ; ++j ) {
    double jfac = ( j > ny/2 ) ? -(double)(ny-j) : (double) j;
    for ( int i = 0 ; i < nx ; ++i ) {
      double ifac = ( i > nx/2 ) ? -(double)(nx-i) : (double)i;
      std::complex<double> fac = ( std::exp( -twopi * imagnum * ifac * shiftx / dnx ) *
                                   std::exp( -twopi * imagnum * jfac * shifty / dny ) );
      tmpft[ j*nx + i ] = ctrpsfft[ j*nx + i ] * fac;
    }
  }
  fftw_execute( tmpft_to_invtmpft_plan );
  size_t npix = nx * ny;
  for ( int i = 0 ; i < npix ; ++i ) {
    shiftedpsf[i] = tmpinvft[i].real() / npix;
  }
}

// **********************************************************************

double ImageClip::lnL_contrib( double* galaxymodel, double snra, double sndec, double flux, double sky )
{
  warp_galaxy_model( galaxymodel );
  convolve_warped_galaxy_model();

  size_t npix = imagetile->axlen(0) * imagetile->axlen(1);
  if ( fileinfo.isref ) {
    for ( int i = 0 ; i < npix ; ++i )
      model[i] = convolvedgalaxymodel[i].real();
  }
  else {
    std::vector<double> rav{ snra };
    std::vector<double> decv{ sndec };
    auto xy = imagetile->wcs()->world_to_c_pix( rav, decv );
    double x = xy.first[0];
    double y = xy.second[0];
    shift_psf( x-ctrpsfx+offx, y-ctrpsfy+offy );
#ifndef NDEBUG
    {
      std::stringstream str("");
      str << "ImageClip::lnL_contrib : shifting " << fileinfo.filename << " psf by "
          << x-ctrpsfx+offx << " , " << y-ctrpsfy+offy
          << " ; flux is " << flux;
      logger->debug( str.str() );
    }
#endif
    for ( int i = 0 ; i < npix ; ++i ) {
      model[ i ] = convolvedgalaxymodel[ i ].real() + flux * shiftedpsf[ i ];
    }
  }

  // Assuming uncorrelated pixels,
  //   (2π)^(-n/2) * Prod( weight^something ) * exp( -1/2 * sum( weight * ( data - model )^2 )
  // (where I think something=+1/2).
  //
  // So log likelihood is:
  // -(n/2)*ln(2π) + something*Sum( weight ) - (1/2) * sum( weight * (data - model )^2 )
  //
  // Since the weights don't change, leave off the first two terms, as they're
  // constant each time, so won't matter in differences of ln likelihoods.
  //
  // Except for the image that was used as the original source for the
  // galaxy model, don't include the edge pixels in the liklihood
  // calculation because there are probably warping issues at the edge.
  // This may make the galaxy model stupid around the frame (though
  // hopefully there's enough overlap with non-aligned warps and psf
  // convolutions to make them not purely unconstrained).  I'm also
  // kinda worried about non-90° rotations, as that will mean corners of
  // the image won't have the galaxy model overlapping them.  Really I
  // should do a circular section, yes?  Yikes.

  int ij0 = ( modelsourcewcs == imagetile->wcs()->wcsprm() ) ? 0 : 1;
  int ij1 = ( modelsourcewcs == imagetile->wcs()->wcsprm() ) ? imagetile->axlen(0) : imagetile->axlen(0)-1;

  double lnL = 0;
  for ( int j = ij0 ; j < ij1 ; ++j ) {
    for ( int i = ij0 ; i < ij1 ; ++i ) {
      double val = ( imagetile->pix( i, j ) - model[ j*imagetile->axlen(0) + i ] ) - sky;
      lnL += -0.5 * weighttile->pix( i , j ) * val * val;
    }
  }

#ifndef NDEBUG
  {
    std::stringstream str("");
    str << "ImageClip::lnL_contrib, contrib is " << lnL << " for " << fileinfo.filename;
    logger->debug( str.str() );
  }
#endif

  return lnL;
}


// **********************************************************************
// **********************************************************************
// **********************************************************************

SceneModel::SceneModel( const std::string &filename, double ra, double dec, int patchsize, int steps, int burn,
                        const std::string& ltcvfile, const std::string& chainfile,
                        const std::string& galmodel, std::shared_ptr<spdlog::logger> logger )
  : initra(ra), initdec(dec), patchsize(patchsize), ltcvfile(ltcvfile), chainfile(chainfile), galmodel(galmodel),
    nsteps(steps), burnin(burn)
{
  if ( logger == nullptr ) {
    this->logger = spdlog::stderr_color_mt( "SceneModel" );
    this->logger->set_level( spdlog::level::info );
  }
  else {
    this->logger = logger;
  }
  read_file( filename );

  galmodelrefim = nullptr;
  for ( auto fi : fileinfo ) {
    auto clip = std::make_shared<ImageClip>( fi, initra, initdec, patchsize, logger );
    if ( ( galmodelrefim == nullptr ) && ( clip->get_fileinfo().isref ) ) {
      galmodelrefim = clip.get();
      galmodelrefimwcs = galmodelrefim->get_imagetile()->wcs()->wcsprm();
    }
    imageclips.push_back( std::move( clip ) );
  }
  if ( galmodelrefim == nullptr ) {
    logger->error( "SceneModel : no refernce images!" );
    throw std::runtime_error( "SceneModel : no reference images!" );
  }
  for ( auto clip : imageclips ) {
    clip->initialize_warps( galmodelrefimwcs );
  }

  // How far we'll allow ra and dec to move ( 1" )
  ramoveprior = 1./3600. / std::cos( initdec * 3.14159265359 / 180. );
  decmoveprior = 1./3600.;
}

// **********************************************************************

void SceneModel::dump_fileinfo() {
  std::stringstream str("");
  str << "FILES:" << std::endl;
  for ( auto i = fileinfo.cbegin() ; i != fileinfo.cend() ; ++i ) {
    str << "File:" << std::endl;
    str << "  fits   : " << i->filename << std::endl;
    str << "  weight : " << i->weightname << std::endl;
    str << "  mask   : " << i->maskname << std::endl;
    str << "  psf    : " << i->psfname << std::endl;
    str << "  mjd    : " << i->mjd << std::endl;
    str << "  zp     : " << i->zp << std::endl;
    str << "  initmag: " << i->initmag << std::endl;
    str << "  isref  : " << i->isref << std::endl;
  }
  logger->info( str.str() );
}

// **********************************************************************

void SceneModel::read_file( const std::string &filename ) {
  std::stringstream str("");
  std::ifstream ifp;
  ifp.open( filename );
  std::string line;
  int nfiles = 0;
  news = 0;
  for ( int n = 0 ; !ifp.eof() ; ++n ) {
    getline( ifp, line );
    if ( ifp.eof() ) break;
    if ( ifp.fail() ) {
      str.str("");
      str << "SceneModel::read_file failed reading line " << n << " of " << filename;
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    if ( line.size() == 0 ) continue;
    if ( line[0] == '#' ) continue;
    std::stringstream linereader( line );
    fileinfo_t onefile;
    std::string newref;
    std::string filebase;
    linereader >> newref >> onefile.mjd >> onefile.zp >> onefile.initmag >> filebase;
    if ( newref == "new" ) onefile.isref = false;
    else if ( newref == "ref" ) onefile.isref = true;
    else {
      str.str("");
      str << "SceneModel::read_file : \"" << newref << "\" is neither \"new\" nor \"ref\" on line "
          << n << " of " << filename;
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }

    if ( newref == "new" ) ++news;

    auto extensions = std::map<std::string,std::string*>{
        { ".fits", &onefile.filename },
        { ".weight.fits", &onefile.weightname },
        { ".mask.fits", &onefile.maskname },
        { ".psf", &onefile.psfname }
    };

    for ( auto i : extensions ) {
      std::string imgfilename = filebase + i.first;
      struct stat statbuffer;
      if ( stat( imgfilename.c_str(), &statbuffer ) != 0 ) {
        str.str("");
        str << "SceneModel::read_file : failed to find " << imgfilename;
        std::cerr << "str.str() is \"" << str.str() << "\"" << std::endl;
        std::cerr.flush();
        logger->error( str.str() );
        throw std::runtime_error( str.str() );
      }
      *(i.second) = imgfilename;
    }
    fileinfo.push_back( onefile );
    ++nfiles;
  }
  str.str( "" );
  str << "SceneModel::read_file read " << nfiles << " lines from " << filename;
  logger->info( str.str() );

  if ( news == nfiles ) {
    logger->error( "SceneModel::read_file : you have no refs!  I need one!" );
    throw std::runtime_error( "SceneModel::read_file : you have no refs!  I need one!" );
  }
}

// **********************************************************************

void SceneModel::write_ltcv( const std::vector<std::vector<double>> &meanvar,
                             const std::vector<std::vector<double>> &covar,
                             const std::string &ltcvfile )
{
  int npix = patchsize*patchsize;
  auto ofp = std::ofstream( ltcvfile );
  ofp << "# ltcv file for anonymous object\n";
  ofp << std::setprecision(6) << std::fixed;
  ofp << "# RA = " << meanvar[0][npix] << " +- " << sqrt(meanvar[1][npix])
      << " , Dec = " << meanvar[0][npix+1] << " +- " << sqrt(meanvar[1][npix+1])
      << std::endl;
  ofp << "# RA/Dec covar:\n";
  ofp << std::setprecision(4) << std::scientific;
  ofp << "#  " << std::setw(11) << covar[npix][npix]
      << " " << std::setw(11) << covar[npix][npix+1] << std::endl;
  ofp << "#  " << std::setw(11) << covar[npix+1][npix]
      << " " << std::setw(11) << covar[npix+1][npix+1] << std::endl;
  ofp << "#\n";
  ofp << "# " << news << " points ; flux covar matrix at end\n";
  ofp << "# mjd flux dflux sky dsky\n";
  for ( int i = 0 ; i < imageclips.size() ; ++i ) {
    int fluxdex = paramdex_flux( i );
    if ( fluxdex < 0 ) continue;
    int skydex = paramdex_sky( i );
    auto clip = imageclips[i];
    ofp << " " << std::fixed << std::setprecision(2) << std::setw(8) << clip->get_fileinfo().mjd;
    ofp << " " << std::scientific << std::setprecision(5) << std::setw(12) << meanvar[0][fluxdex]
        << " " << std::setprecision(3) << std::setw(10) << sqrt(meanvar[1][fluxdex])
        << "    " << std::setprecision(5) << std::setw(12) << ( skydex>=0 ? meanvar[0][skydex] : 0 )
        << " " << std::setprecision(3) << std::setw(10) << ( skydex>=0 ? sqrt(meanvar[1][skydex]) : 0 )
        << std::endl;
  }
  for ( int i = 0 ; i < imageclips.size() ; ++i ) {
    int idex = paramdex_flux(i);
    if ( idex < 0 ) continue;
    for ( int j = 0 ; j < imageclips.size() ; ++j ) {
      int jdex = paramdex_flux(j);
      if ( jdex < 0 ) continue;
      ofp << std::scientific << std::setprecision(5) << std::setw(12) << covar[idex][jdex] << std::endl;
    }
  }
  ofp.close();
}

// **********************************************************************

double SceneModel::loglikelihood( const std::vector<double> &params ) {
  size_t fluxdexen[imageclips.size()];
  size_t skydexen[imageclips.size()];
  // size_t skydex = patchsize*patchsize + 2;
  double ra = params[ patchsize*patchsize ];
  double dec = params[ patchsize*patchsize + 1 ];
  double lnL = 0.;

  double* galmodel = new double[ patchsize*patchsize ];
  // ...why don't I just pass params.data()?
  memcpy( galmodel, params.data(), sizeof(double) * patchsize*patchsize );

  // Prior : make sure ra and dec haven't slid by too much
  if ( ( fabs( ra - initra ) > ramoveprior ) || ( fabs( dec - initdec ) > decmoveprior ) ) {
    // Don't return early, because I use the side effects of this method
    //   when dumping tile images
    lnL += -1e30;
  }

#pragma omp parallel for reduction(+:lnL)
  for ( int i = 0 ; i < imageclips.size() ; ++i ) {
    double sky = 0.;
    int skydex = paramdex_sky( i );
    if ( skydex >= 0 ) sky = params[ skydex ];
    double flux = 0.;
    int fluxdex = paramdex_flux( i );
    if ( fluxdex >= 0 ) {
      flux = params[ fluxdex ];
      if ( flux < 0 ) lnL += -1e30;
    }
    lnL += imageclips[i]->lnL_contrib( galmodel, ra, dec, flux, sky );
  }

  delete[] galmodel;
  return lnL;
}

// **********************************************************************

void SceneModel::initialize()
{
  std::stringstream str("");
  mpicomm = MPI_COMM_WORLD;
  MPI_Comm_rank( mpicomm, &mpirank );
  MPI_Comm_size( mpicomm, &mpisize );
  str.str("");
  str << "MPI rank " << std::setw(3) << mpirank << " of " << std::setw(3) << mpisize;
  logger->info( str.str() );

  auto nthreads = omp_get_max_threads();
  str.str("");
  str << "omp_get_max_threads() returned " << nthreads;
  logger->info( str.str() );

  fftw_init_threads();
  fftw_plan_with_nthreads( nthreads );

  // dump_fileinfo();

  // Set initial values of the parameters (and the spread of the walkers around that)

  nparams = patchsize*patchsize + 2 + imageclips.size() - 1 + news;
  initparams.resize( nparams );
  initparamsig.resize( nparams );
  paramnames.resize( nparams );
  std::vector<bool> dexfound( nparams, false );

  // Galaxy model
  for ( int j = 0 ; j < patchsize ; ++j ) {
    for ( int i = 0 ; i < patchsize ; ++i ) {
      int dex = paramdex_gmodel( i, j );
      if ( dex < 0 ) {
        logger->error( "SceneModel::go : This really shouldn't happen.  No, really." );
        throw std::runtime_error( "SceneModel::go : This really shouldn't happen.  No, really." );
      }
      initparams[dex] = galmodelrefim->get_imagetile()->pix( i, j );
      initparamsig[dex] = galmodelrefim->get_weighttile()->pix( i, j );
      if ( initparamsig[dex] <= 0. ) initparamsig[dex] = fabs( initparams[dex] * 0.1 );
      else initparamsig[dex] = 1. / sqrt( initparamsig[dex] );
      str.str("");
      str << "pix" << std::setw(2) << std::setfill('0') << i
          << "_" << std::setw(2) << std::setfill('0') << j;
      paramnames[dex] = str.str();
      dexfound[dex] = true;
    }
  }

  // RA and Dec
  paramnames[ paramdex_ra()  ] = "ra";
  initparams[ paramdex_ra() ] = initra;
  initparamsig[ paramdex_ra() ] = 0.25/3600. / std::cos( initdec * 3.14159265359 / 180. );
  dexfound[ paramdex_ra() ] = true;
  paramnames[ paramdex_dec() ] = "dec";
  initparams[ paramdex_dec() ] = initdec;
  initparamsig[ paramdex_dec() ] = 0.25/3600.;
  dexfound[ paramdex_dec() ] = true;

  // Sky and flux
  int refdex = 0;
  int newdex = 0;
  for ( int i = 0 ; i < imageclips.size() ; ++i ) {
    auto clip = imageclips[i];
    auto img = clip->get_imagetile().get();
    int fluxdex = paramdex_flux( i );
    int skydex = paramdex_sky( i );
    if ( skydex >= 0 ) {
      initparams[skydex] = ( img->pix( 0, 0 ) + img->pix( 0, patchsize-1 )
                          + img->pix( patchsize-1, 0 ) + img->pix( patchsize-1, patchsize-1 ) ) / 4.;
      initparamsig[skydex] = clip->get_weighttile()->pix( 0, 0 );
      if ( initparamsig[skydex] < 0. ) initparamsig[skydex] = initparams[skydex] * 0.1;
      else initparamsig[skydex] = 1. / sqrt( initparamsig[skydex] );
      dexfound[ skydex ] = true;
    }
    if ( fluxdex >= 0 ) {
      initparams[fluxdex] = pow( 10 , ( clip->get_fileinfo().initmag - ImageClip::STDZP ) / -2.5 );
      initparamsig[fluxdex] = 0.05 * initparams[fluxdex];
      dexfound[ fluxdex ] = true;
    }
    if ( imageisref( i ) ) {
      if ( skydex >= 0 ) {
        str.str("");
        str << "refsky" << refdex;
        paramnames[ skydex ] = str.str();
      }
      if ( fluxdex >= 0 ) {
        logger->error( "This should never happen." );
        throw std::runtime_error( "This should never happen." );
      }
      ++refdex;
    } else {
      if ( skydex < 0 || fluxdex < 0 ) {
        logger->error( "This, too, should never happen." );
        throw std::runtime_error( "This, too, should never happen." );
      }
      str.str("");
      str << "newsky" << newdex;
      paramnames[ skydex ] = str.str();
      str.str("");
      str << "flux" << newdex;
      paramnames[ fluxdex ] = str.str();
      ++newdex;
    }
  }

  // Two sanity checks.  If the code isn't wrong, these will never error out.
  std::list<size_t> missing;
  for ( size_t i = 0 ; i < nparams ; ++i ) {
    if ( ! dexfound[i] ) {
      missing.push_back(i);
    }
  }
  if ( missing.size() > 0 ) {
    str.str("");
    str << "SceneModel::go : there's an error in parameter index calculations.  The following indexes weren't found: ";
    for ( auto i : missing ) str << i << " ";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  for ( auto name : paramnames ) {
    if ( name.size() == 0 ) {
      logger->error( "SceneModel::go : there's an error setting the parameter names." );
      throw std::runtime_error( "SceneModel::go : there's an error setting the parameter names." );
    }
  }

  // Load up meanvar with the initial parameters
  meanvar.resize(2);
  meanvar[0].resize( nparams );
  meanvar[1].resize( nparams );
  for ( int i = 0 ; i < nparams ; ++i ) {
    meanvar[0][i] = initparams[i];
    meanvar[1][i] = initparamsig[i] * initparamsig[i];
  }

  // Run a log likelihood to get models loaded in the image clips from initial values
  // auto t0 = std::chrono::high_resolution_clock::now();
  // double lnL = loglikelihood( initparams );
  // std::chrono::duration<double> dt = std::chrono::high_resolution_clock::now() - t0;
  // str.str("");
  // str << "...that took " << dt.count() << " seconds.";
  // logger->info( str.str() );

  initialized = true;
  logger->info( "SceneModel initialized" );
}

// **********************************************************************

void SceneModel::go()
{
  std::stringstream str("");

  if ( havegone ) {
    logger->error( "SceneModel::go : don't call this more than once." );
    throw std::runtime_error( "SceneModel::go : don't call this more than once." );
  }
  if ( !initialized ) {
    initialize();
  }

  int nwalkers = initparams.size() * 2;
  double stretchparam = 2.;
  auto lnLfunc = std::bind( &SceneModel::loglikelihood, this, std::placeholders::_1 );
  str.str("");
  str << "SceneModel::go : initializing Stumbler with " << nwalkers << " walkers.";
  logger->info( str.str() );

  std::string burnchainfile = "";
  if ( chainfile.size() == 0 ) {
    logger->warn( "No chain filename given, not writing full chain." );
  } else {
    str.str("");
    str << "burn_" << chainfile;
    burnchainfile = str.str();
    str.str("");
    str << "Writing chains to [burn_]" << chainfile;
    logger->info( str.str() );
  }

  int dumpevery = 1000;
  int seed = 42;
  auto stumbler = Stumbler( nwalkers, burnin, nsteps, stretchparam, initparams, initparamsig, paramnames,
                            lnLfunc, MPI_COMM_WORLD, chainfile, burnchainfile, dumpevery, seed, logger );
  if ( mpirank == 0 ) logger->info( "Stumbling about." );
  stumbler.go( 1 );

  if ( mpirank == 0 ) {
    logger->info( "Done stumbling." );

    stumbler.get_mean_var( meanvar );
    stumbler.get_covar( covar );

    str.str("");
    str << "Writing lightcurve file " << ltcvfile;
    logger->info( str.str() );
    write_ltcv( meanvar, covar, ltcvfile );

    if ( galmodel.size() != 0 ) {
      logger->error( "Dumping of galaxy model not implemented." );
    }
  }

  fftw_cleanup_threads();

  havegone = true;
}

// **********************************************************************

void SceneModel::dump_tiles( bool weight, bool mask )
{
  if ( mpirank != 0 ) return;
  int imagendigits = (int)floor( log( imageclips.size() ) / log( 10. ) ) + 1;

  logger->info( "Writing image tiles to tile_<n>_<ref|new>.fits" );
  for ( int i = 0 ; i < imageclips.size() ; ++i ) {
    std::stringstream name("");
    name << "tile_" << std::setw(imagendigits) << std::setfill('0') << i
         << (imageisref(i) ? "ref" : "new" ) << ".fits";
    imageclips[i]->dump_tile( name.str(), true );
  }

  if ( weight ) {
    logger->info( "Writing image weight tiles to weight_<n>_<ref|new>.fits" );
    for ( int i = 0 ; i < imageclips.size() ; ++i ) {
      std::stringstream name("");
      name << "weight_" << std::setw(imagendigits) << std::setfill('0') << i
           << (imageisref(i) ? "ref" : "new" ) << ".fits";
      imageclips[i]->dump_weight( name.str(), true );
    }
  }

  if ( mask ) {
    logger->info( "Writing image mask tiles to mask_<n>_<ref|new>.fits" );
    for ( int i = 0 ; i < imageclips.size() ; ++i ) {
      std::stringstream name("");
      name << "mask_" << std::setw(imagendigits) << std::setfill('0') << i
           << (imageisref(i) ? "ref" : "new" ) << ".fits";
      imageclips[i]->dump_mask( name.str(), true );
    }
  }
}

// **********************************************************************

void SceneModel::dump_models( bool resid )
{
  if ( mpirank != 0 ) return;
  int imagendigits = (int)floor( log( imageclips.size() ) / log( 10. ) ) + 1;

  // Use the side effect of loglikelihood that it fills in the model images in ImageClip
  loglikelihood( meanvar[0] );

  logger->info( "Writing model files to model_<n>_<ref|new>.fits" );
  for ( int i = 0 ; i < imageclips.size() ; ++i ) {
    std::stringstream name("");
    name << "model_" << std::setw(imagendigits) << std::setfill('0') << i
         << (imageisref(i) ? "ref" : "new" ) << ".fits";
    imageclips[i]->dump_model( name.str(), true );
  }

  if ( resid ) {
    logger->info( "Writing residual files to resid_<n>_<ref|new>.fits" );
    for ( int i = 0 ; i < imageclips.size() ; ++i ) {
      std::stringstream name("");
      name << "resid_" << std::setw(imagendigits) << std::setfill('0') << i
           << ( imageisref(i) ? "ref" : "new" ) << ".fits";
      int skydex = paramdex_sky( i );
      double sky = ( skydex >= 0 ) ? meanvar[0][skydex] : 0.;
      imageclips[i]->dump_resid( name.str(), sky, true );
    }
  }
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

int main( int argc, char *argv[] ) {
  //****
  CALLGRIND_STOP_INSTRUMENTATION;
  //****

  MPI_Init( &argc, &argv );

  std::stringstream str("");
  auto logger = spdlog::stderr_color_mt( "sm" );
  logger->set_level( spdlog::level::warn );

  auto argparser = argparser::ArgParser::get();
  auto arg_filename = argparser::Argument<std::string>( "", "", "filename", "", "Filename with lots of info" );
  auto arg_ltcvfile = argparser::Argument<std::string>( "-l", "--ltcvfile", "ltcvfile", "ltcv.dat",
                                                        "Name of output lightcurve file." );
  auto arg_chainfile = argparser::Argument<std::string>( "-c", "--chainfile", "chainfile", "",
                                                         "Write full chain to <chainfile>"
                                                         "and burn_<chainfile> (default: not written)"  );
  auto arg_galmodel = argparser::Argument<std::string>( "-g", "--galmodel", "galmodel", "",
                                                        "Output galaxy model filename (default: not written)" );
  auto arg_tiles = argparser::Argument<bool>( "-t", "--tiles", "tiles", false, "Dump FITS files of image tiles" );
  auto arg_weights = argparser::Argument<bool>( "-w", "--weights", "weights", false,
                                                "Dump FITS files of image tile weights (ignored if tiles is false)" );
  auto arg_masks = argparser::Argument<bool>( "-a", "--masks", "masks", false,
                                              "Dump FITS files of image tile masks (ignored if tiles is false)" );
  auto arg_models = argparser::Argument<bool>( "-m", "--models", "models", false, "Dump FITS files of models" );
  auto arg_resids = argparser::Argument<bool>( "-e", "--resids", "tiles", false,
                                               "Dump FITS files of residuals (ignored if models is false)" );
  auto arg_ra = argparser::Argument<double>( "-r", "--ra", "ra", 666., "RA of supernova" );
  auto arg_dec = argparser::Argument<double>( "-d", "--dec", "dec", 666., "Dec of supernova" );
  auto arg_patchsize = argparser::Argument<int>( "-p", "--patchsize", "patchsize", 0, "Patch size" );
  auto arg_steps = argparser::Argument<int>( "-s", "--steps", "steps", 200, "Number of steps to take after burn-in" );
  auto arg_burn = argparser::Argument<int>( "-b", "--burn", "burn", 100, "Number of burn-in steps" );
  auto arg_stretchparam = argparser::Argument<double>( "", "--stretchparam", "stretchparam", 2.0,
                                                       "Stretch move stretch parameter" );
  auto arg_info = argparser::Argument<bool>( "-v", "", "info", false, "Show info log" );
  auto arg_debug = argparser::Argument<bool>( "-vv", "", "debug", false, "Show debug log" );
  auto arg_help = argparser::Argument<bool>( "-h", "--help", "help", false, "Show help" );
  argparser->add_arg( arg_filename );
  argparser->add_arg( arg_ltcvfile );
  argparser->add_arg( arg_chainfile );
  argparser->add_arg( arg_galmodel );
  argparser->add_arg( arg_tiles );
  argparser->add_arg( arg_weights );
  argparser->add_arg( arg_masks );
  argparser->add_arg( arg_models );
  argparser->add_arg( arg_resids );
  argparser->add_arg( arg_ra );
  argparser->add_arg( arg_dec );
  argparser->add_arg( arg_patchsize );
  argparser->add_arg( arg_steps );
  argparser->add_arg( arg_burn );
  argparser->add_arg( arg_info );
  argparser->add_arg( arg_debug );
  argparser->add_arg( arg_help );
  argparser->parse( argc, argv );

  if ( arg_help.given ) {
    std::cout << argparser->help();
    return(0);
  }

  if ( arg_debug.given ) {
    logger->set_level( spdlog::level::debug );
  } else if ( arg_info.given ) {
    logger->set_level( spdlog::level::info );
  }

  if ( arg_filename.get_val().size() == 0 ) {
    logger->error( "Must specify a filename" );
    return(1);
  }
  if ( ( arg_ra.get_val() < 0 ) || ( arg_ra.get_val() >= 360 ) ||
       ( arg_dec.get_val() < -90 ) || ( arg_dec.get_val() > 90 ) ) {
    str.str("");
    str << "Invalid ra/dec " << arg_ra.get_val() << ", " << arg_dec.get_val();
    logger->error( str.str() );
    return(1);
  }
  if ( ( arg_patchsize.get_val() <= 5 ) || ( arg_patchsize.get_val() > 100 ) ) {
    str.str("");
    str << "Invalid or absurd patchsize: " << arg_patchsize.get_val();
    logger->error( str.str() );
    return(1);
  }
  if ( ( arg_patchsize.get_val() % 2 ) == 0 ) {
    // convolutions with the PSF in Fourier space will introduce shifts if
    // the patch size is not odd.
    logger->error( "Error, patchsize must be odd." );
    return(1);
  }

  auto sm = SceneModel( arg_filename.get_val(), arg_ra.get_val(), arg_dec.get_val(), arg_patchsize.get_val(),
                        arg_steps.get_val(), arg_burn.get_val(), arg_ltcvfile.get_val(), arg_chainfile.get_val(),
                        arg_galmodel.get_val(), logger );
  sm.set_stretchparam( arg_stretchparam.get_val() );

  //****
  CALLGRIND_START_INSTRUMENTATION;
  //****
  sm.go();
  //****
  CALLGRIND_STOP_INSTRUMENTATION;
  //****

  if ( arg_tiles.given ) sm.dump_tiles( arg_weights.given, arg_masks.given );
  if ( arg_models.given ) sm.dump_models( arg_resids.given );

  return(0);
}

