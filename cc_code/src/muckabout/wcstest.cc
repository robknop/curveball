// g++ -O3 -fopenmp -o wcstest wcstest.cc wcswrapper.cc -lcfitsio -lwcs -lgnuastro -lm -lfmt

#include "wcswrapper.hh"
#include <vector>
#include <iomanip>
#include <sstream>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <fitsio.h>

int main( int argc, char* argv[] ) {
  auto logger = spdlog::stderr_color_mt( "main" );
  logger->set_level( spdlog::level::debug );

  std::stringstream str("");
  std::ios oldstate(nullptr);

  std::string fname( "/data/curveball_dev/tmp/c4d_221110_043018_ori.58.fits.fz" );

  auto wcs = WCSWrapper( fname, logger );

  {
    std::vector<double> x{    0.,    0., 2047., 2047., 1024. };
    std::vector<double> y{    0., 4095.,    0., 4096., 2048. };
    auto radec = wcs.c_pix_to_world( x, y );
    std::vector<double>& ra = radec.first;
    std::vector<double>& dec = radec.second;

    str.str("");
    oldstate.copyfmt( str );
    str << "Pixel to world coordinates: " << std::endl;
    for ( int i = 0 ; i < x.size() ; ++i ) {
      str << "    " << std::fixed << std::setprecision(1)
          << std::setw(6) << x[i] << ", " << std::setw(6) << y[i] << " → "
          << std::fixed << std::setprecision(5)
          << std::setw(9) << ra[i] << ", " << std::setw(9) << dec[i];
      if ( i < x.size()-1 ) str << std::endl;
    }
    logger->info( str.str() );
    str.copyfmt( oldstate );
  }

  {
    std::vector<double> ra{ 7.9, 8.1, 8.29 };
    std::vector<double> dec{ -43.68, -43.75, -43.825 };
    auto xy = wcs.world_to_c_pix( ra, dec );
    std::vector<double>& x = xy.first;
    std::vector<double>& y = xy.second;

    str.str("");
    oldstate.copyfmt( str );
    str << "World to pixel coordinates: " << std::endl;
    for ( int i = 0 ; i < x.size() ; ++i ) {
      str << "    " << std::fixed << std::setprecision(5)
          << std::setw(9) << ra[i] << ", " << std::setw(9) << dec[i] << " → "
          << std::fixed << std::setprecision(1)
          << std::setw(6) << x[i] << ", " << std::setw(6) << y[i];
      if ( i < x.size()-1 ) str << std::endl;
    }
    logger->info( str.str() );
    str.copyfmt( oldstate );
  }
}
