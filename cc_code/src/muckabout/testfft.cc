#include <complex>
#include <fitsfile.hh>
#include <psfexreader.hh>
#include <fftw3.h>

int main( int argc, char *argv[] ) {
  FITSFile f( "../../cctest/fodder/c4d_221110_043018_ori.58.fits.fz" );
  PSFExReader psf( "../../cctest/fodder/c4d_221110_043018_ori.58.psf" );
  int x0 = 749;
  int x1 = 782;
  int y0 = 1262;
  int y1 = 1295;

  double shiftpsfx = 6.3;
  double shiftpsfy = -4.2;

  int nx = x1 - x0;
  int ny = y1 - y0 ;
  int npix = nx * ny;

  auto img = f.get_image<float>();
  auto tile = img->subtile<double>( x0, x1, y0, y1 );

  auto tilefile = FITSFile();
  tilefile.add_hdu( tile );
  tilefile.save_to( "tile.fits", true );

  auto psftile = psf.psf_stamp<double>( (x0+x1)/2, (y0+y1)/2, (x1-x0) );
  for ( int i = 0 ; i < npix ; ++i) psftile[i] *= 5000.;

  auto psfimg = FITSImage<double>::create( (x1-x0), (y1-y0), psftile.get() );
  auto psffile = FITSFile();
  psffile.add_hdu( psfimg );
  psffile.save_to( "psf.fits", true );

  auto ftrimg = tile->clone<float>();
  auto ftiimg = tile->clone<float>();
  auto invftrimg = tile->clone<float>();
  auto invftiimg = tile->clone<float>();

  auto origdata = new std::complex<double>[npix];
  auto ft = new std::complex<double>[npix];
  auto invft = new std::complex<double>[npix];

  auto psfdata = new std::complex<double>[npix];
  auto psfft = new std::complex<double>[npix];

  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      origdata[ j * nx + i ].real( tile->pix( i, j ) );
      origdata[ j * nx + i ].imag( 0. );
      psfdata[ j * nx + i ].real( psftile[ j * nx + i ] );
      psfdata[ j * nx + i ].imag( 0. );
    }
  }

  auto origimg = FITSImage<float>::create( (x1-x0), (y1-y0) );
  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      origimg->data()[ j*nx + i ] = origdata[ j*nx + i ].real();
    }
  }
  auto origimgfile = FITSFile();
  origimgfile.add_hdu( origimg );
  origimgfile.save_to( "origdata.fits", true );

  fftw_plan fwdplan = fftw_plan_dft_2d( nx, ny,
                                        reinterpret_cast<fftw_complex*>(origdata),
                                        reinterpret_cast<fftw_complex*>(ft),
                                        FFTW_FORWARD, FFTW_ESTIMATE );
  fftw_execute( fwdplan );
  fftw_destroy_plan( fwdplan );
  fftw_plan psfplan = fftw_plan_dft_2d( nx, ny,
                                        reinterpret_cast<fftw_complex*>(psfdata),
                                        reinterpret_cast<fftw_complex*>(psfft),
                                        FFTW_FORWARD, FFTW_ESTIMATE );
  fftw_execute( psfplan );
  fftw_destroy_plan( psfplan );
  // Add psf to tile
  for ( int j = 0 ; j < ny ; ++j ) {
    for (int i = 0 ; i < nx ; ++i ) {
      ft[ j*nx + i] += psfft[ j*nx + i ];
    }
  }
  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      ftrimg->data()[ j*nx + i ] = ft[ j*nx + i ].real();
      ftiimg->data()[ j*nx + i ] = ft[ j*nx + i ].imag();
    }
  }

  fftw_plan bkwdplan = fftw_plan_dft_2d( nx, ny,
                                         reinterpret_cast<fftw_complex*>(ft),
                                         reinterpret_cast<fftw_complex*>(invft),
                                         FFTW_BACKWARD, FFTW_ESTIMATE );
  fftw_execute( bkwdplan );
  fftw_destroy_plan( bkwdplan );

  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      invftrimg->data()[ j*nx + i ] = invft[ j*nx + i ].real() / npix;
      invftiimg->data()[ j*nx + i ] = invft[ j*nx + i ].imag() / npix;
    }
  }

  auto outftr = FITSFile();
  outftr.add_hdu( ftrimg );
  outftr.save_to( "ftr.fits", true );
  auto outfti = FITSFile();
  outfti.add_hdu( ftiimg );
  outfti.save_to( "fti.fits", true );
  auto outinvftr = FITSFile();
  outinvftr.add_hdu( invftrimg );
  outinvftr.save_to( "invftr.fits", true );
  auto outinvfti = FITSFile();
  outinvfti.add_hdu( invftiimg );
  outinvfti.save_to( "invfti.fits", true );

  delete[] invft;
  delete[] ft;
  delete[] origdata;
}
