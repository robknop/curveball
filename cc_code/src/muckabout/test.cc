#include <iostream>
#include <functional>


int main( int agrc, char* argv[] ) {
  size_t thing = 1 << 30;

  std::cout << thing << std::endl;
  
  int i = 5;
  int j = 7;
  std::reference_wrapper<int> ref(i);

  std::cout << "i=" << i << ", j=" << j << ", ref=" << ref.get() << std::endl;

  ref.get() = 9;

  std::cout << "i=" << i << ", j=" << j << ", ref=" << ref.get() << std::endl;

  ref = j;
  
  std::cout << "i=" << i << ", j=" << j << ", ref=" << ref.get() << std::endl;

  ref.get() = 11;
  
  std::cout << "i=" << i << ", j=" << j << ", ref=" << ref.get() << std::endl;

}
