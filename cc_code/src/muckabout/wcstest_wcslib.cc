// g++ -o wcstest_wcslib wcstest_wcslib.cc -lcfitsio -lwcs -lm -lfmt

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <ios>
#include <exception>
#include <unistd.h>
#include <fitsio.h>
#include <wcslib/wcshdr.h>
#include <spdlog/spdlog.h>

const auto spdlog_level = spdlog::level::debug;

int main( int argc, char* argv[] ) {
  std::stringstream str("");
  std::ios strstate( nullptr );
  std::string fname( "/data/curveball_dev/tmp/c4d_221110_043018_ori.58.fits.fz" );

  fitsfile *fptr;
  // WARNING : turns out that status is readwrite in the fits_open* routines!
  // If you don't initialize it, then sometimes they will fail!
  // This was not obvious to me from the documentation.
  // See : https://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/c_user/node28.html
  // I guess that's what I get for reading documentation on the routines
  // I want rather than exhaustively reading the entire document.
  int status = 0;

  spdlog::info( "Opening file" );
  fits_open_data( &fptr, fname.c_str(), READONLY, &status );
  str.str("");
  str << "File opened, status=" << status;
  spdlog::info( str.str() );
  if ( status != 0 ) {
    spdlog::error( "Repeated failures opening FITS file." );
    throw std::runtime_error( "Error opening FITS file." );
  }

  // Use this if you use fits_open_file instead of fits_open_data
  // spdlog::info( "It's a .fz file, so move to HDU 2" );
  // int hdutype;
  // fits_movabs_hdu( fptr, 2, &hdutype, &status );
  // str.str("");
  // str << "Moved to HDU 2; hdutype=" << hdutype << ", status=" << status;
  // spdlog::info( str.str() );

  spdlog::info( "Extracting header" );
  char *header;
  int nkeys;
  fits_hdr2str( fptr, 1, 0, 0, &header, &nkeys, &status );
  str.str("");
  str << "Header read, status=" << status << ", nkeys=" << nkeys;
  spdlog::info( str.str() );
  // std::cout << "\nHEADER:\n";
  // for ( int i = 0 ; i < nkeys ; ++i ) {
  //   std::string card( header + i*80, 80 );
  //   std::cout << card << std::endl;
  // }
  // std::cout << "\n";

  struct wcsprm *wcs;
  int nreject, nwcs;
  spdlog::info( "Parsing WCS out of header." );
  wcsbth( header, nkeys, WCSHDR_all, 2, WCSHDR_IMGHEAD, NULL, &nreject, &nwcs, &wcs );
  str.str("");
  str << "Parsed WCS; nreject=" << nreject << ", nwcs=" << nwcs;
  spdlog::info( str.str() );
  status = wcsset( wcs );
  str << "wcsset run, status=" << status;

  double pix[] = { 501., 701. };
  double sky[] = { 8.05316896, -43.77228301 };
  double imgcrd[] = { 0., 0. };
  double worldcrd[] = { 0., 0. };
  double θ, φ;
  int stats[] = { 0 };
  wcsp2s( wcs, 1, 2, pix, imgcrd, &φ, &θ, worldcrd, stats );
  str.str("");
  strstate.copyfmt( str );
  str << "Pixel " << std::fixed << std::setprecision(2) << pix[0] << ", " << pix[1]
      << " is world " << std::setprecision(8) << worldcrd[0] << ", " << worldcrd[1];
      // << " with imgcrd= " << imgcrd[0] << ", " << imgcrd[1]
      // << " and φ=" << φ << " and θ=" << θ;
  spdlog::info( str.str() );
  str.copyfmt( strstate );

  worldcrd[0] = 8.05316896;
  worldcrd[1] = -43.77228301;
  wcss2p( wcs, 1, 2, worldcrd, &φ, &θ, imgcrd, pix, stats );
  str.str("");
  strstate.copyfmt( str );
  str << "World " << std::fixed << std::setprecision(8) << worldcrd[0] << ", " << worldcrd[1]
      << " is pixel " << std::setprecision(2) << pix[0] << ", " << pix[1];
  spdlog::info( str.str() );
  str.copyfmt( strstate );

  spdlog::info( "Freeing memory and closing file" );
  wcsvfree( &nwcs, &wcs );
  fits_free_memory( header, &status );
  fits_close_file( fptr, &status );
}
