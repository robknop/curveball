// g++ -o testfitsread_cfitsio testfitsread_cfitsio.cc -lcfitsio -lm -lfmt

#include <string>
#include <sstream>
#include <iostream>
#include <iomanip>
#include <ios>
#include <exception>
#include <unistd.h>
#include <fitsio.h>
#include <spdlog/spdlog.h>

const auto spdlog_level = spdlog::level::debug;

int main( int argc, char* argv[] ) {
  std::stringstream str("");
  std::ios strstate( nullptr );
  std::string fname( "/data/curveball_dev/tmp/c4d_221110_043018_ori.58.fits.fz" );

  fitsfile *fptr;
  // WARNING : turns out that status is readwrite in the fits_open* routines!
  // If you don't initialize it, then sometimes they will fail!
  // This was not obvious to me from the documentation.
  // See : https://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/c_user/node28.html
  // I guess that's what I get for reading documentation on the routines
  // I want rather than exhaustively reading the entire document.
  int status = 0;

  spdlog::info( "Opening file" );
  fits_open_data( &fptr, fname.c_str(), READONLY, &status );
  str.str("");
  str << "File opened, status=" << status;
  spdlog::info( str.str() );
  if ( status != 0 ) {
    spdlog::error( "Repeated failures opening FITS file." );
    throw std::runtime_error( "Error opening FITS file." );
  }

  // Use this if you use fits_open_file instead of fits_open_data
  // spdlog::info( "It's a .fz file, so move to HDU 2" );
  // int hdutype;
  // fits_movabs_hdu( fptr, 2, &hdutype, &status );
  // str.str("");
  // str << "Moved to HDU 2; hdutype=" << hdutype << ", status=" << status;
  // spdlog::info( str.str() );

  int naxis;
  fits_get_img_dim( fptr, &naxis, &status );
  long* naxes = new long[ naxis ];
  fits_get_img_size( fptr, naxis, naxes, &status );
  str.str("");
  str << "Image is ";
  for ( int i = 0 ; i < naxis ; ++i ) {
    str << naxes[i];
    if ( i < naxis-1 ) str << " × ";
  }
  spdlog::info( str.str() );

  // spdlog::info( "Extracting header" );
  // char *header;
  // int nkeys;
  // fits_hdr2str( fptr, 1, 0, 0, &header, &nkeys, &status );
  // str.str("");
  // str << "Header read, status=" << status << ", nkeys=" << nkeys;
  // spdlog::info( str.str() );
  // std::cout << "\nHEADER:\n";
  // for ( int i = 0 ; i < nkeys ; ++i ) {
  //   std::string card( header + i*80, 80 );
  //   std::cout << card << std::endl;
  // }
  // std::cout << "\n";


  float val;
  int anynull;
  const int ncoords = 4;
  long pixcoords[] = { 128, 256,
                       256, 128,
                       1024, 333,
                       333, 1024 };
  for ( int i = 0 ; i < ncoords ; ++i ) {
    fits_read_pix( fptr, TFLOAT, pixcoords + 2*i, 1, 0, &val, &anynull, &status );
    str.str("");
    str << "Pixel " << pixcoords[2*i] << ", " << pixcoords[2*i+1]
        << " is " << val;
    spdlog::info( str.str() );
  }

  float* image =new float[ naxes[0]*naxes[1] ];
  long fpixel[] = { 1, 1 };
  long lpixel[] = { naxes[0], naxes[1] };
  // inc isn't described in the document.  A comment in the
  // source code says "increment to apply to each dimension".
  // Not sure exactly what this means; I have guesses.
  long inc[] = { 1, 1 };
  fits_read_subset( fptr, TFLOAT, fpixel, lpixel, inc, nullptr, image, &anynull, &status );
  str.str("");
  str << "inc[0] is " << inc[0] << "; inc[1] is " << inc[1];
  spdlog::info( str.str() );

  for ( int i = 0 ; i < ncoords ; ++i ) {
    str.str("");
    str << "float image at " << pixcoords[2*i] << ", " << pixcoords[2*i+1]
        << " is " << image[ ( pixcoords[2*i] - 1) + ( pixcoords[2*i+1] - 1) * naxes[0] ];
    spdlog::info( str.str() );
  }

  spdlog::info( "Freeing memory and closing file" );
  // fits_free_memory( header, &status );
  delete[] image;
  delete[] naxes;
  fits_close_file( fptr, &status );
}
