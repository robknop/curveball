#include <fitsfile.hh>
#include <psfexreader.hh>
#include <complex>
#include <sstream>
#include <omp.h>
#include <fftw3.h>
#include <spdlog/sinks/stdout_color_sinks.h>

int main( int argc, char *argv[] ) {
  auto logger = spdlog::stderr_color_mt( "testfft2" );
  logger->set_level( spdlog::level::debug );

  std::stringstream str("");

  auto nthreads = omp_get_max_threads();
  str.str("");
  str << "Initializing fftw to use " << nthreads << " threads.";
  logger->info( str.str() );
  fftw_init_threads();
  fftw_plan_with_nthreads( nthreads );

  
  PSFExReader psf( "../../cctest/fodder/c4d_221110_043018_ori.58.psf" );
  int x0 = 749;
  int x1 = 781;
  int y0 = 1262;
  int y1 = 1294;
  if ( ( y1-y0 ) != ( x1-x0) ) {
    logger->error( "(y1-y0) != (x1-x0)" );
    return(1);
  }
  int nx = x1 - x0;
  int ny = y1 - y0;
  int npix = nx * ny;
  // OK, here's where the ".0 is the center of the pixel" really bites us.
  // If the stamp is (say) 32x32, then the center should be on the corner
  // of pixels 15-16 in x and y, which would be 15.5, which is not 32/2.
  double xc = ( x0 + x1 ) / 2. - 0.5;
  double yc = ( y0 + y1 ) / 2. - 0.5;
  
  int stampsize = x1 - x0;
  
  double shiftpsfx = 3.3;
  double shiftpsfy = -2.2;
  
  auto psftile = psf.psf_stamp<double>( xc, yc, x0, y0, stampsize );
  for ( int i = 0 ; i < npix ; ++i) psftile[i] *= 5000.;

  auto psfimg = FITSImage<double>::create( nx, ny, psftile.get(), logger );
  auto psffile = FITSFile( logger );
  psffile.add_hdu( psfimg );
  psffile.save_to( "psf.fits", true );

  auto ftrimg = psfimg->clone<float>();
  auto ftiimg = psfimg->clone<float>();
  auto ftrshiftimg = psfimg->clone<float>();
  auto ftishiftimg = psfimg->clone<float>();
  auto invftrimg = psfimg->clone<float>();
  auto invftiimg = psfimg->clone<float>();
  
  auto origdata = new std::complex<double>[npix];
  auto ft = new std::complex<double>[npix];
  auto invft = new std::complex<double>[npix];

  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      origdata[ j * nx + i ].real( psfimg->pix( i, j ) );
      origdata[ j * nx + i ].imag( 0. );
    }
  }

  fftw_plan fwdplan = fftw_plan_dft_2d( nx, ny,
                                        reinterpret_cast<fftw_complex*>(origdata),
                                        reinterpret_cast<fftw_complex*>(ft),
                                        FFTW_FORWARD, FFTW_ESTIMATE );
  fftw_execute( fwdplan );
  fftw_destroy_plan( fwdplan );

  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      ftrimg->setpix( i, j, ft[ j*nx + i ].real() );
      ftiimg->setpix( i, j, ft[ j*nx + i ].imag() );
    }
  }
  auto outftr = FITSFile( logger );
  outftr.add_hdu( ftrimg );
  outftr.save_to( "ftr.fits", true );
  auto outfti = FITSFile( logger );
  outfti.add_hdu( ftiimg );
  outfti.save_to( "fti.fits", true );
                      
  // Shift psf

  str.str("");
  str << "Shifting by ( " << shiftpsfx << ", " << shiftpsfy << ")";
  logger->info( str.str() );
  constexpr std::complex<double> imagnum{0., 1.};
  constexpr double twopi = 2.*std::acos(-1);
  str.str("");
  str << "2π = " << twopi;
  logger->debug( str.str() );
  for ( int j = 0 ; j < ny ; ++j ) {
    double jfac = ( j > ny/2 ) ? -(double)(ny-j) : (double)j;
    for ( int i = 0 ; i < nx ; ++i ) {
      double ifac = ( i > nx/2 ) ? -(double)(nx-i) : (double)i;
      std::complex<double> fac = ( std::exp( -twopi * imagnum * ifac * shiftpsfx / (double)nx ) *
                                   std::exp( -twopi * imagnum * jfac * shiftpsfy / (double)ny ) );
      ft[ j*nx + i ] *= fac;
    }
  }

  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      ftrshiftimg->setpix( i, j, ft[ j*nx + i ].real() );
      ftishiftimg->setpix( i, j, ft[ j*nx + i ].imag() );
    }
  }
  auto outftrshift = FITSFile( logger );
  outftrshift.add_hdu( ftrshiftimg );
  outftrshift.save_to( "ftrshift.fits", true );
  auto outftishift = FITSFile( logger );
  outftishift.add_hdu( ftishiftimg );
  outftishift.save_to( "ftishift.fits", true );
  
  fftw_plan bkwdplan = fftw_plan_dft_2d( nx, ny,
                                         reinterpret_cast<fftw_complex*>(ft),
                                         reinterpret_cast<fftw_complex*>(invft),
                                         FFTW_BACKWARD, FFTW_ESTIMATE );
  fftw_execute( bkwdplan );
  fftw_destroy_plan( bkwdplan );

  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      invftrimg->setpix( i, j, invft[ j*nx + i ].real() / npix );
      invftiimg->setpix( i, j, invft[ j*nx + i ].imag() / npix );
    }
  }

  auto outinvftr = FITSFile( logger );
  outinvftr.add_hdu( invftrimg );
  outinvftr.save_to( "invftr.fits", true );
  auto outinvfti = FITSFile( logger );
  outinvfti.add_hdu( invftiimg );
  outinvfti.save_to( "invfti.fits", true );

  double totorigpsf = 0.;
  double totshiftpsf = 0.;
  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      totorigpsf += psfimg->pix( i, j );
      totshiftpsf += invftrimg->pix( i, j );
    }
  }
  str.str("");
  str << "Original sum: " << totorigpsf << " ; final sum: " << totshiftpsf
      << " (fractional difference " << ( (totshiftpsf-totorigpsf) / totorigpsf ) << ")";
  logger->info( str.str() );
        
  delete[] invft;
  delete[] ft;
  delete[] origdata;
  fftw_cleanup_threads();
}
