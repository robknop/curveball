#include <complex>
#include <iostream>
#include <fitsfile.hh>
#include <psfexreader.hh>
#include <fftw3.h>

int main( int argc, char *argv[] ) {
  FITSFile f( "../../cctest/fodder/c4d_221110_043018_ori.58.fits.fz" );
  int x0 = 749;
  int x1 = 782;
  int y0 = 1262;
  int y1 = 1295;

  int nx = x1 - x0;
  int ny = y1 - y0 ;
  int npix = nx * ny;

  if ( nx != ny ) {
    std::cerr << "nx must = ny" << std::endl;
    return(1);
  }
  
  auto img = f.get_image<float>();
  auto tile = img->subtile<double>( x0, x1, y0, y1 );
  
  auto tilefile = FITSFile();
  tilefile.add_hdu( tile );
  tilefile.save_to( "tile.fits", true );

  // Create a convolution kernel
  auto kernel = new double[ npix ];

  for ( int i = 0 ; i < npix ; ++i ) kernel[i] = 0.;
  if ( nx % 2  == 0 ) {
    kernel[ (ny/2) * nx + (nx/2) ] = 0.25;
    kernel[ (ny/2) * nx + (nx/2-1) ] = 0.25;
    kernel[ (ny/2-1) * nx + (nx/2) ] = 0.25;
    kernel[ (ny/2-1) * nx + (nx/2-1) ] = 0.25;
  } else {
    kernel[ ( ny / 2 ) * nx + nx / 2  ] = 1.;
  }

  // constexpr double pi = std::acos( -1 );
  // double sx = 2.;
  // double sy = 2.;
  // for ( int j = 0 ; j < ny ; ++j ) {
  //   double dj = ( j - ny/2. );
  //   for ( int i = 0 ; i < nx ; ++i ) {
  //     double di = ( i - nx/2. );
  //     kernel[ j*nx + i ] = 1/(2*pi*sx*sy) * std::exp( -(dj*dj)/(2*sy*sy) ) * std::exp( -(di*di)/(2*sx*sx) );
  //   }
  // }

  auto shiftkernel = new double[ npix ];
  for ( int srcj = 0 ; srcj < ny ; ++srcj ) {
    int dstj = srcj - ny/2;
    if ( dstj < 0 ) dstj = ny + dstj;
    for ( int srci = 0 ; srci < nx ; ++srci ) {
      int dsti = srci - ny/2;
      if ( dsti < 0 ) dsti = nx + dsti;
      shiftkernel[ dstj*nx + dsti ] = kernel[ srcj*nx + srci ];
    }
  }

  long double shtot = 0.;
  long double tot = 0.;
  for ( int i = 0 ; i < npix ; ++i ) {
    tot += kernel[i];
    shtot += shiftkernel[i];
  }
  std::cerr << "SUMS : kernel = " << tot << " , shiftkernel = " << shtot << std::endl;
  
  auto kernelimg = tile->clone<float>();
  for ( int i = 0 ; i < npix ; ++i ) kernelimg->data()[i] = shiftkernel[i];
  auto kernelfile = FITSFile();
  kernelfile.add_hdu( kernelimg );
  kernelfile.save_to( "kernel.fits", true );

  auto origdata = new std::complex<double>[npix];
  auto ft = new std::complex<double>[npix];
  auto origkerneldata = new std::complex<double>[npix];
  auto kernelft = new std::complex<double>[npix];
  auto invft = new std::complex<double>[npix];

  for ( int i = 0 ; i < npix ; ++i ) {
    origdata[i].real( tile->data()[i] );
    origdata[i].imag( 0. );
    origkerneldata[i].real( shiftkernel[i] );
    origkerneldata[i].imag( 0. );
  }
  
  fftw_plan fwdplan = fftw_plan_dft_2d( nx, ny,
                                        reinterpret_cast<fftw_complex*>(origdata),
                                        reinterpret_cast<fftw_complex*>(ft),
                                        FFTW_FORWARD, FFTW_ESTIMATE );
  fftw_execute( fwdplan );
  fftw_destroy_plan( fwdplan );

  fftw_plan kernelplan = fftw_plan_dft_2d( nx, ny,
                                           reinterpret_cast<fftw_complex*>(origkerneldata),
                                           reinterpret_cast<fftw_complex*>(kernelft),
                                           FFTW_FORWARD, FFTW_ESTIMATE );
  fftw_execute( kernelplan );
  fftw_destroy_plan( kernelplan );

  auto kernelftrimg = tile->clone<float>();
  auto kernelftiimg = tile->clone<float>();
  for ( int i = 0 ; i < npix ; ++i ) {
    kernelftrimg->data()[i] = kernelft[i].real();
    kernelftiimg->data()[i] = kernelft[i].imag();
  }
  auto kernelftrfile = FITSFile();
  kernelftrfile.add_hdu( kernelftrimg );
  kernelftrfile.save_to( "kernelftr.fits", true );
  auto kernelftifile = FITSFile();
  kernelftifile.add_hdu( kernelftiimg );
  kernelftifile.save_to( "kernelfti.fits", true );
  
  // Convolve
  for ( int i = 0 ; i < npix ; ++i ) {
    ft[ i ] *= kernelft[i];
  }

  // ****
  // for ( int i = 0 ; i < npix ; ++i ) {
  //   ft[i].real(1.);
  //   ft[i].imag(0.);
  // }
  // ****

  fftw_plan bkwdplan = fftw_plan_dft_2d( nx, ny,
                                         reinterpret_cast<fftw_complex*>(ft),
                                         reinterpret_cast<fftw_complex*>(invft),
                                         FFTW_BACKWARD, FFTW_ESTIMATE );
  fftw_execute( bkwdplan );
  fftw_destroy_plan( bkwdplan );

  auto invftrimg = tile->clone<float>();
  auto invftiimg = tile->clone<float>();
  for ( int j = 0 ; j < ny ; ++j ) {
    for ( int i = 0 ; i < nx ; ++i ) {
      invftrimg->data()[ j*nx + i ] = invft[ j*nx + i ].real() / npix;
      invftiimg->data()[ j*nx + i ] = invft[ j*nx + i ].imag() / npix;
    }
  }

  auto outinvftr = FITSFile();
  outinvftr.add_hdu( invftrimg );
  outinvftr.save_to( "invftr.fits", true );
  auto outinvfti = FITSFile();
  outinvfti.add_hdu( invftiimg );
  outinvfti.save_to( "invfti.fits", true );

  delete[] invft;
  delete[] kernelft;
  delete[] origkerneldata;
  delete[] ft;
  delete[] origdata;
  delete[] shiftkernel;
  delete[] kernel;
}
