#include <fitsfile.hh>
#include <wcswrapper.hh>
#include <string>
#include <sstream>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <gnuastro/data.h>
#include <gnuastro/warp.h>

int main( int argc, char* argv[] ) {
  auto logger = spdlog::stderr_color_mt( "main" );
  logger->set_level( spdlog::level::debug );

  std::stringstream str("");
  std::ios oldstate(nullptr);

  std::string file1 = "/data/curveball/ZTF/stack/2019-02-26/stack_ztf_20190226464468_000721_zg_io.111.fits";
  std::string file2 = "/data/curveball/ZTF/2018-08-22/ztf_20180822145093_001760_zg_io.042.fits";

  // We're going to warp 2 on to 1
  // These limits are on 1
  std::vector<double> x1tile{ 2991, 3030 };
  std::vector<double> y1tile{ 1107, 1146 };
  std::vector<size_t> ix1tile{ (size_t)(x1tile[0]), (size_t)(x1tile[1]) };
  std::vector<size_t> iy1tile{ (size_t)(y1tile[0]), (size_t)(y1tile[1]) };
  std::vector<double> addval{ 0., 100., 1000., 2345. };

  FITSFile fitsfile1( file1, logger );
  FITSFile fitsfile2( file2, logger );

  FITSImage<float> *fits1 = fitsfile1.get_image<float>();
  FITSImage<float> *fits2 = fitsfile2.get_image<float>();

  std::shared_ptr<FITSImage<float>> tile1 = fits1->subtile<float>( ix1tile[0], ix1tile[1], iy1tile[0], iy1tile[1] );
  FITSFile tile1out( logger );
  tile1out.add_hdu( tile1 );
  tile1out.save_to( "warp_test_tile1.fits", true );

  auto radec = fits1->wcs()->c_pix_to_world( x1tile, y1tile );
  auto coords2 = fits2->wcs()->world_to_c_pix( radec.first, radec.second );
  std::vector<double>& x2tile = coords2.first;
  std::vector<double>& y2tile = coords2.second;
  std::vector<int> ix2tile{ (int)floor(x2tile[0]+0.5), (int)floor(x2tile[1]+0.5) };
  std::vector<int> iy2tile{ (int)floor(y2tile[0]+0.5), (int)floor(y2tile[1]+0.5) };

  str.str("");
  str << "(" << x1tile[0] << ":" << x1tile[1] << ", " << y1tile[0] << ":" << y1tile[1] << ") goes to ("
      << x2tile[0] << ":" << x2tile[1] << ", " << y2tile[0] << ":" << y2tile[1];
  logger->info( str.str() );

  int offx = 10, offy = 10;
  std::shared_ptr<FITSImage<float>> tile2 = fits2->subtile<float>( ix2tile[0] + offx, ix2tile[1] + offx,
                                                                   iy2tile[0] + offy, iy2tile[1] + offy );
  FITSFile tile2out( logger );
  tile2out.add_hdu( tile2 );
  tile2out.save_to( "warp_test_tile2.fits", true );

  auto warper = gal_warp_wcsalign_template();
  warper.coveredfrac = 1;
  warper.edgesampling = 0;
  warper.numthreads = 0;
  // Gotta make a copy and convert to double
  warper.input = gal_data_copy_to_new_type( tile2->get_galdata(), GAL_TYPE_FLOAT64 );
  warper.twcs = tile1->wcs()->wcsprm();

  // I think this will get freed when the gal_struct_t I'm sticking it in gets freed
  size_t ndim = 2;
  size_t *dsize = (size_t*)malloc( sizeof(size_t) * 2 );
  // dsize is backwards
  dsize[1] = x1tile[1] - x1tile[0];
  dsize[0] = y1tile[1] - y1tile[0];
  warper.widthinpix = gal_data_alloc( dsize, GAL_TYPE_SIZE_T, 1, &ndim,
                                      nullptr, 1, -1, 0, nullptr, nullptr, nullptr );
  gal_warp_wcsalign_init( &warper );

  for ( int whichaddval = 0 ; whichaddval < addval.size() ; ++whichaddval ) {
#pragma omp parallel for
    for ( int i = 0 ; i < dsize[0]*dsize[1] ; ++i ) {
      ((double*)(warper.input->array))[i] += addval[whichaddval];
    }

#pragma omp parallel for
    for ( int i = 0 ; i < dsize[0]*dsize[1] ; ++i ) {
      gal_warp_wcsalign_onpix( &warper, i );
    }
    auto floatoutput = gal_data_copy_to_new_type( warper.output, GAL_TYPE_FLOAT32 );

    FITSFile ffile( logger );
    ffile.append_image<float>( floatoutput );
    std::stringstream ofname("");
    ofname << "warp_test_out_" << whichaddval << ".fits";
    ffile.save_to( ofname.str(), true );

  }
  gal_data_free( warper.output );
  gal_data_free( warper.input );
  gal_data_free( warper.widthinpix );
  gal_warp_wcsalign_free( &warper );
}




