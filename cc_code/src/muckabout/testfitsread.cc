// g++ -O3 -o testfitsread testfitsread.cc -lgnuastro -lcfitsio -lwcs -lm -lfmt

#include <memory>
#include <string>
#include <sstream>
#include <exception>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <gnuastro/fits.h>

const size_t ONEGIG = 1073741824;

int main( int argc, char* argv[] ) {
  auto logger = spdlog::stderr_color_mt( "main" );
  logger->set_level( spdlog::level::debug );

  std::stringstream str("");
  std::ios oldstate(nullptr);

  std::string fname( "/data/curveball_dev/tmp/c4d_221110_043018_ori.58.fits.fz" );

  // Irritating that GAL doesn't use const char* where it should
  // Because of that, I can't use fname.c_str()
  int nhdus = gal_fits_hdu_num( &(fname[0]) );
  if ( nhdus < 0 || nhdus > 2 ) {
    str.str("");
    str << "FITS file has " << nhdus << ", expected 1 or 2. (" << fname << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  int hdu = ( nhdus == 2 ) ? 1 : 0;
  str.str("");
  str << "Will read HDU " << hdu << " from " << fname;
  logger->info( str.str() );
  std::string hdustr = std::to_string(hdu);
  if ( gal_fits_hdu_format( &(fname[0]), &(hdustr[0]) ) != IMAGE_HDU ) {
    str.str("");
    str << "FITS file hdu " << hdu << " is not an image! (" << fname << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  
  fitsfile* fptr = gal_fits_hdu_open( &(fname[0]), &(hdustr[0]), READONLY, 0 );
  if ( fptr == nullptr ) {
    str.str("");
    str << "Error opening file " << fname;
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  // Get image dimensions
  // This is perverse.  It seems that dsize is *backwards*,
  // so that dsize[0] is the y-size and dsize[1] is the x-size.
  // (dsize[0] is NAXIS2 and dsize[1] is NAXIS1).
  int type;
  size_t ndim;
  size_t *dsize = nullptr;
  char *name = nullptr;
  char *unit = nullptr;
  gal_fits_img_info( fptr, &type, &ndim, &dsize, &name, &unit );
  if ( ndim != 2 ) {
    str.str("");
    str << "Got " << ndim << "-dimensional image, expected 2 (" << fname << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }

  // Print some information about the image
  str.str("");
  str << "ndim=" << ndim << ", ";
  for ( int i = 0 ; i < ndim ; ++i ) {
    str << dsize[i];
    if ( i < ndim-1 ) str << " × ";
  }
  str << ", unit=" << unit;
  logger->debug( str.str() );
  str.str("");
  str << "name=" << name;
  logger->debug( str.str() );

  gal_data_t *imgdata = gal_fits_img_read( &(fname[0]), &(hdustr[0]), ONEGIG, 0 );
  if ( imgdata->type != GAL_TYPE_FLOAT32 ) {
    str.str("");
    str << "Image data type is " << imgdata->type
        << ", but I'm going to assume GAL_TYPE_FLOAT32 (" << GAL_TYPE_FLOAT32 << ")";
    logger->error( str.str() );
    throw std::runtime_error( str.str() );
  }
  
  const int ncoords = 4;
  long pixcoords[] = { 128, 256,
                       256, 128,
                       1024, 333,
                       333, 1024 };
  for ( int i = 0 ; i < ncoords ; ++i ) {
    int ix = pixcoords[2*i];
    int iy = pixcoords[2*i+1];
    str.str("");
    str << "Pixel " << ix << ", " << iy << " is " << ((float*)(imgdata->array))[ iy*dsize[1] + ix ];
    logger->info( str.str() );
  }

  // Clean up

  // It's not clear to me from the gnu astro documentation how I'm supposed to
  // close a fits file opend with gal_fits_hdu_open.  I'm going to resort to using
  // the cfitsio function
  int status = 0;
  fits_close_file( fptr, &status );
  if ( status != 0 ) {
    str.str("");
    str << "Status " << status << " closing fptr";
    logger->error( str.str() );
  }

  if ( imgdata != nullptr ) { gal_data_free( imgdata ); imgdata = nullptr; }
  if ( dsize != nullptr ) { free( dsize ); dsize = nullptr; }
  if ( name != nullptr ) { free( name ); name = nullptr; }
  if ( unit != nullptr ) { free( unit ); unit = nullptr; }
}
