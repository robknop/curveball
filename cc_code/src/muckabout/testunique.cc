#include <iostream>
#include <memory>

void func( std::shared_ptr<float[]> it ) {
  std::cout <<  "In function; it has count " << it.use_count() << " and is =";
  for ( int i = 0 ; i < 6 ; ++i ) {
    std::cout << " " << it[i];
  }
  std::cout << std::endl;
}

int main( int argc, char* argv[] ) {
  std::shared_ptr<float[]> arr = std::move( std::make_unique<float[]>(6) );
  arr[0] = 4.;
  arr[1] = 8.;
  arr[2] = 15.;
  arr[3] = 16.;
  arr[4] = 23.;
  arr[5] = 42.;

  std::cout << "arr has count " << arr.use_count() << " and is =";
  for ( int i = 0 ; i < 6 ; ++i ) {
    std::cout << " " << arr[i];
  }
  std::cout << std::endl;

  func( arr );

  std::cout << "arr has count " << arr.use_count() << " and is =";
  for ( int i = 0 ; i < 6 ; ++i ) {
    std::cout << " " << arr[i];
  }
  std::cout << std::endl;

}
