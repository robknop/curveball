// g++ -O3 -fopenmp -o testpsfread testpsfread.cc psfexreader.cc -lcfitsio -lm -lfmt -lgsl

#include "psfexreader.hh"
#include <iomanip>
#include <sstream>
#include <chrono>
#include <sys/stat.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <fitsio.h>

int main( int argc, char* argv[] ) {
  auto logger = spdlog::stderr_color_mt( "main" );
  logger->set_level( spdlog::level::debug );

  std::stringstream str("");
  std::ios oldstate(nullptr);

  std::string fname( "../bin/junk.psf" );

  PSFExReader psfex( fname, logger );
  auto psfbuffer = psfex.get_oversampled_psf<float>( 1024, 2048 );

  struct stat statbuffer;
  int status = 0;

  logger->info( "Writing output fits file to psf.fits" );
  if ( stat( "psf.fits", &statbuffer ) == 0  ) {
    logger->warn( "psf.fits exists, deleting" );
    int err = std::remove( "psf.fits" );
    if ( err != 0 ) {
      str.str("");
      str << "Error " << err << " deleting psf.fits";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
  }

  fitsfile *ofits;

  fits_create_file( &ofits, "psf.fits", &status );
  long outpsffitssize[] = { psfex.psfwid(), psfex.psfwid() };
  long outpsffpixel[] = { 1, 1 };
  fits_create_img( ofits, FLOAT_IMG, 2, outpsffitssize, &status );
  fits_write_subset( ofits, TFLOAT, outpsffpixel, outpsffitssize, psfbuffer.get(), &status );
  fits_close_file( ofits, &status );
  ofits = nullptr;
  logger->info( "Wrote psf.fits" );

  logger->info( "Writing a stamp to stamp.fits" );
  if ( stat( "stamp.fits", &statbuffer ) == 0 ) {
    logger->warn( "stamp.fits exists, deleting" );
    int err = std::remove( "stamp.fits" );
    if ( err != 0 ) {
      str.str("");
      str << "Error " << err << " deleting stamp.fits";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
  }

  auto stamp = psfex.psf_stamp<float>( 1024, 2048, 25 );
  fits_create_file( &ofits, "stamp.fits", &status );
  long outstampfitssize[] = { 25, 25 };
    long outstampfpixel[] = { 1, 1 };
  fits_create_img( ofits, FLOAT_IMG, 2, outstampfitssize, &status );
  fits_write_subset( ofits, TFLOAT, outstampfpixel, outstampfitssize, stamp.get(), &status );
  fits_close_file( ofits, &status );
  ofits = nullptr;
  logger->info( "Wrote stamp.fits" );


  logger->info( "Writing many psfs to psfpalette.fits" );
  if ( stat( "psfpalette.fits", &statbuffer ) == 0 ) {
    logger->warn( "psfpalette.fits exists, deleting" );
    int err = std::remove( "psfpalette.fits" );
    if ( err != 0 ) {
      str.str("");
      str << "Error " << err << " deleting psfpalette.fits";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
  }

  auto t0 = std::chrono::high_resolution_clock::now();

  int imagewid = 2048;
  int imagehei = 4096;
  int stampwid = 25;
  int npsfs = 0;
  std::unique_ptr<float[]> image = std::make_unique<float[]>( imagewid*imagehei );
  for ( int i = 0 ; i < imagewid*imagehei ; ++i ) image[i] = 0.;
  for ( double x = stampwid ; x < imagewid - stampwid ; x += 1.273*stampwid ) {
    for ( double y = stampwid ; y < imagehei - stampwid ; y += 1.273*stampwid, ++npsfs ) {
      psfex.add_psf_to_image( image.get(), imagewid, imagehei, stampwid, x, y, 1. );
    }
  }

  std::chrono::duration<double> dur = std::chrono::high_resolution_clock::now() - t0;
  oldstate.copyfmt( str );
  str.str("");
  str << "Stamping " << npsfs << " PFS on to the image took "
      << std::fixed << std::setprecision(2) << dur.count() << " sec ("
      << std::scientific << ( dur.count() / npsfs ) << " per PSF).";
  logger->info( str.str() );
  str.copyfmt( oldstate );

  fits_create_file( &ofits, "psfpalette.fits", &status );
  long palettesize[] = { imagewid, imagehei };
  long palettefpixel[] = { 1, 1 };
  fits_create_img( ofits, FLOAT_IMG, 2, palettesize, &status );
  fits_write_subset( ofits, TFLOAT, palettefpixel, palettesize, image.get(), &status );
  fits_close_file( ofits, &status );
  ofits = nullptr;
  logger->info( "Wrote psfpalette.fits" );
}

