#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <H5Cpp.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <argparser.hh>

int main( int argc, char *argv[] )
{
  std::stringstream str("");
  auto logger = spdlog::stderr_color_mt( "main" );
  logger->set_level( spdlog::level::debug );
  
  auto argparser = argparser::ArgParser::get();
  auto arg_filename = argparser::Argument<std::string>( "", "", "filename", "", "Name out output file" );
  auto arg_create = argparser::Argument<bool>( "-c", "--create", "create", false, "Create the file" );
  auto arg_write = argparser::Argument<bool>( "-w", "--write", "write", false, "Write first few lines" );
  auto arg_append = argparser::Argument<bool>( "-a", "--append", "append", false, "Append to data" );
  auto arg_read = argparser::Argument<int>( "-r", "--read-cols", "readcols", 0, "Read these columns", 1, 5 );
  auto arg_nvals = argparser::Argument<int>( "-n", "--nvals", "nvals", 10, "Number of values per row" );
  auto arg_ndata = argparser::Argument<int>( "-d", "--ndata", "ndata", 25, "Number of rows" );
  auto arg_help = argparser::Argument<bool>( "-h", "--help", "help", false, "Show help" );
  argparser->add_arg( arg_filename );
  argparser->add_arg( arg_create );
  argparser->add_arg( arg_write );
  argparser->add_arg( arg_append );
  argparser->add_arg( arg_read );
  argparser->add_arg( arg_nvals );
  argparser->add_arg( arg_ndata );
  argparser->add_arg( arg_help );
  argparser->parse( argc, argv );

  if ( arg_help.given || !arg_filename.given ) {
    std::cout << "Usage: testhdf5 [OPTIONS] filename" << std::endl << std::endl;
    std::cout << argparser->help();
    return( 0 );
  }
  
  std::string filename = arg_filename.get_val();
  size_t nvals = arg_nvals.get_val();
  size_t ndata = arg_ndata.get_val();
  
  std::vector<double> data( ndata*nvals );

  data.resize( ndata );
  for ( int i = 0 ; i < ndata ; ++i ) {
    for ( int j = 0 ; j < nvals ; ++j ) {
      data[ i * nvals + j ] = pow(j, 2) + (double)i / 1000.;
    }
  }

  if ( arg_create.given ) {
    logger->info( "Creating dataset" );
    hid_t h5file = H5Fcreate( filename.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT );
    hsize_t dims[2]{ 0, nvals };
    hsize_t maxdims[2]{ H5S_UNLIMITED, nvals };
    hid_t space = H5Screate_simple( 2, dims, maxdims );
    hid_t plist = H5Pcreate( H5P_DATASET_CREATE );
    H5Pset_layout( plist, H5D_CHUNKED );
    hsize_t chunk_dims[2]{ 5, nvals };
    H5Pset_chunk( plist, 2, chunk_dims );
    hid_t dset = H5Dcreate( h5file, "data", H5T_NATIVE_DOUBLE, space, H5P_DEFAULT, plist, H5P_DEFAULT );
    H5Dclose( dset );
    H5Pclose( plist );
    H5Sclose( space );
    H5Fclose( h5file );
  }

  if ( arg_write.given ) {
    logger->info( "Writing initial dataset" );
    hid_t h5file = H5Fopen( filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
    hid_t dset = H5Dopen2( h5file, "data", H5P_DEFAULT );
    hid_t space = H5Dget_space( dset );
    hsize_t ndims = H5Sget_simple_extent_ndims( space );
    if ( ndims != 2 ) {
      str.str("");
      str << "dataspace has " << ndims << " dimensions, expected 2.";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    hsize_t dims[2];
    hsize_t maxdims[2];
    H5Sget_simple_extent_dims( space, dims, maxdims );
    if ( ( dims[1] != nvals ) || ( maxdims[1] != nvals ) ) {
      str.str("");
      str << "dims is [" << dims[0] << ", " << dims[1]
          << "], maxdims is [" << maxdims[0] << ", maxdims[1]"
          << "], but the second dimension should be nvals=" << nvals;
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    if ( maxdims[0] != H5S_UNLIMITED ) {
      str.str("");
      str << "maxdims[0] is " << maxdims[0] << " but I expected "
          << H5S_UNLIMITED << " (H5S_UNLIMITED)";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    if ( dims[0] != ndata ) {
      H5Sclose( space );
      dims[0] = ndata;
      H5Dset_extent( dset, dims );
      space = H5Dget_space( dset );
      H5Sget_simple_extent_dims( space, dims, maxdims );
      str.str("");
      str << "After extent setting, dims=[" << dims[0] << ", " << dims[1]
          << "], maxdims=[" << maxdims[0] << ", " << maxdims[1];
      logger->debug( str.str() );
    }

    // These should already be set, but I'm putting this here just to be explicit
    // for my later reading notes.
    dims[0] = ndata;
    dims[1] = nvals;

    // Select the region in the file dataspace
    hsize_t start[2]{ 0, 0 };
    H5Sselect_hyperslab( space, H5S_SELECT_SET, start, nullptr, dims, nullptr );
    
    // Need to create an H5 dataspace to indicate the size
    // of the memory buffer
    hid_t memspace = H5Screate_simple( ndims, dims, nullptr );
    H5Dwrite( dset, H5T_NATIVE_DOUBLE, memspace, space, H5P_DEFAULT, data.data() );

    H5Sclose( memspace );
    H5Sclose( space );
    H5Dclose( dset );
    H5Fclose( h5file );
  }
      
  if ( arg_append.given ) {
    logger->info( "Appending to dataset" );
    hid_t h5file = H5Fopen( filename.c_str(), H5F_ACC_RDWR, H5P_DEFAULT );
    hid_t dset = H5Dopen2( h5file, "data", H5P_DEFAULT );
    hid_t space = H5Dget_space( dset );
    hsize_t ndims = H5Sget_simple_extent_ndims( space );

    if ( ndims != 2 ) {
      str.str("");
      str << "dataspace has " << ndims << " dimensions, expected 2.";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    hsize_t dims[2];
    hsize_t maxdims[2];
    H5Sget_simple_extent_dims( space, dims, maxdims );
    if ( ( dims[1] != nvals ) || ( maxdims[1] != nvals ) ) {
      str.str("");
      str << "dims is [" << dims[0] << ", " << dims[1]
          << "], maxdims is [" << maxdims[0] << ", maxdims[1]"
          << "], but the second dimension should be nvals=" << nvals;
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    if ( maxdims[0] != H5S_UNLIMITED ) {
      str.str("");
      str << "maxdims[0] is " << maxdims[0] << " but I expected "
          << H5S_UNLIMITED << " (H5S_UNLIMITED)";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }

    str.str("");
    str << "Current dataset has dims=[" << dims[0] << ", " << dims[1] << "]";
    logger->debug( str.str() );

    hsize_t newdims[2]{ dims[0] + ndata, nvals };
    H5Sclose( space );
    H5Dset_extent( dset, newdims );
    space = H5Dget_space( dset );
    H5Sget_simple_extent_dims( space, newdims, maxdims );
    if ( newdims[0] != dims[0] + ndata ) {
      str.str("");
      str << "After extending space, newdims[0]=" << newdims[0]
          << ", but expected " << dims[0] + ndata;
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    hsize_t addedsize[2]{ newdims[0]-dims[0], nvals };
    str.str("");
    str << "Extending dataset from " << dims[0] << " to " << newdims[0] << " rows, adding a block of size ["
        << addedsize[0] << ", " << addedsize[1] << "]";
    logger->info( str.str() );
    hsize_t slabstart[2]{ dims[0], 0 };
    H5Sselect_hyperslab( space, H5S_SELECT_SET, slabstart, nullptr, addedsize, nullptr );

    // Update the buffer
    for ( int i = 0 ; i < ndata ; ++i ) {
      for ( int j = 0 ; j < nvals ; ++j ) {
        data[ i * nvals + j ] = pow(j, 2) + (double)( i + dims[0] ) / 1000.;
      }
    }
    
    hid_t memspace = H5Screate_simple( ndims, addedsize, nullptr );
    H5Dwrite( dset, H5T_NATIVE_DOUBLE, memspace, space, H5P_DEFAULT, data.data() );

    H5Sclose( memspace );
    H5Sclose( space );
    H5Dclose( dset );
    H5Fclose( h5file );
  }

  if ( arg_read.given ) {
    auto cols = arg_read.get_vals();
    str.str( "" );
    str << "Reading columns ";
    for ( auto c : cols ) str << c << " ";
    logger->info( str.str() );

    hid_t h5file = H5Fopen( filename.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT );
    hid_t dset = H5Dopen2( h5file, "data", H5P_DEFAULT );
    hid_t space = H5Dget_space( dset );
    hsize_t ndims = H5Sget_simple_extent_ndims( space );
    if ( ndims != 2 ) {
      str.str("");
      str << "dataspace has " << ndims << " dimensions, expected 2.";
      logger->error( str.str() );
      throw std::runtime_error( str.str() );
    }
    hsize_t dims[2];
    hsize_t maxdims[2];
    H5Sget_simple_extent_dims( space, dims, maxdims );
    for ( auto c : cols ) {
      if ( c >= dims[1] ) {
        str.str("");
        str << "Can't read column " << c << ", there are only " << dims[1] << " columns.";
        logger->error( str.str() );
        throw std::runtime_error( str.str() );
      }
    }

    std::vector<double> readdata( dims[0] );
    hsize_t memdims[2]{ dims[0], 1 };
    hid_t memspace = H5Screate_simple( ndims, memdims, nullptr );
    hsize_t filestart[2]{ 0, 0 };
    for ( auto c : cols ) {
      filestart[1] = c;
      H5Sselect_hyperslab( space, H5S_SELECT_SET, filestart, nullptr, memdims, nullptr );
      H5Dread( dset, H5T_NATIVE_DOUBLE, memspace, space, H5P_DEFAULT, readdata.data() );
      str.str("");
      str << "Column " << c << ": ";
      for ( auto val : readdata ) {
        str << val << " ";
      }
      logger->info( str.str() );
    }

    H5Sclose( memspace );
    H5Sclose( space );
    H5Dclose( dset );
    H5Fclose( h5file );
  }
}
