import sys
import re
import argparse
import matplotlib
from matplotlib import pyplot

def plot( t, flux, dflux ):
    linestype = ""
    titlesize = 20
    axistitlesize = 18
    labelsize = 10
    marker = 'o'
    pointsize = 5
    pointcolor = "royalblue"
    matplotlib.rc( 'mathtext', fontset='cm' )

    fig = pyplot.figure( figsize=(10,6), tight_layout=True )
    ax = fig.add_subplot( 1, 1, 1 )
    ax.tick_params( axis='both', labelsize=labelsize )

    ax.errorbar( t, flux, dflux, linestyle="", marker=marker, color=pointcolor, markerfacecolor=pointcolor )
    ax.set_ylabel( r"$\mathrm{flux}\ (zp=30)$" )
    ax.set_xlabel( r"$\mathrm{MJD}$" )
    
    pyplot.show()
    
def main():
    parser = argparse.ArgumentParser( description="Plot light curve from scenemodel",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( "filename", help="filename output from scenemodel" )
    args = parser.parse_args()

    npoints = -1
    npointsre = re.compile( '^\# *([0-9]+) +points' )
    with open( args.filename ) as ifp:

        while npoints < 0:
            line = ifp.readline()
            if line == "":
                raise RuntimeError( "Failed to find a line with '# n points'" )
            match = npointsre.search( line )
            if match is not None:
                npoints = int( match.group(1) )

        t = []
        flux = []
        dflux = []
        while len(t) < npoints:
            line = ifp.readline()
            if ( line.strip()[0] ) == '#':
                continue
            if line == "":
                raise RuntimeError( f"Only found {len(t)} points, expected {npoints}" )
            try:
                t0, flux0, dflux0, *junk = line.split()
            except ValueError as e:
                nl = "\n"
                raise RuntimeError( f"Error at line \"{line.rstrip(nl)}\": "
                                    f"only found {len(t)} points, expected {npoints}" )
            t.append( float(t0) )
            flux.append( float(flux0) )
            dflux.append( float(dflux0) )


    plot( t, flux, dflux )

# ======================================================================

if __name__ == "__main__":
    main()

