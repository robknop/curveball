import sys
import os
import numpy
import numpy.fft
import pandas
import argparse
import matplotlib
from matplotlib import pyplot

sys.path.append( os.getcwd() )
from plot_readchain import readchain, readallchains

# Stolen (and modified) from https://emcee.readthedocs.io/en/stable/tutorials/autocorr/
def autocor( data, norm=True ):
    n = 1
    while n < len(data):
        n = n << 1

    f = numpy.fft.fft( data, n=2 * n)
    acf = numpy.fft.ifft(f * numpy.conjugate(f))[: len(data) ].real
    acf /= 4 * n

    if norm:
        acf /= acf[0]

    return acf

# Stolen from https://emcee.readthedocs.io/en/stable/tutorials/autocorr/
def auto_window(taus, c):
    m = numpy.arange(len(taus)) < c * taus
    # I think we want a ~m here ; it wasn't in that URL
    if numpy.any( ~m ):
        return numpy.argmin(m)
    return len(taus) - 1


def main():
    parser = argparse.ArgumentParser( description="Plot autocorrelation of MCMC chain",
                                      formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument( 'filename', help="Filename with chain" )
    parser.add_argument( '--skip', type=int, default=0, help="Skip this many initial strides" )
    parser.add_argument( '-s', '--stride', type=int, default=1,
                         help="Plot every stride points (skip stride-1 in between)" )
    parser.add_argument( '-o', '--offset', type=int, default=0,
                         help="Offset the first point by this much" )
    parser.add_argument( '-c', '--cols', nargs='+', default=[],
                         help="Plot these columns (by name) (supercedes --colnums) (default: all)" )
    parser.add_argument( '-t', '--plot-t-vs-n', default=False, action='store_true',
                         help="Plot autocorrelation time estimate as a function of chain length" )
    # parser.add_argument( '-l', '--trim-last', default=0, type=int,
    #                      help="Trim this many lines from end of file (for partial completed writes)" )
    args = parser.parse_args()

    if args.plot_t_vs_n:
        dfs = readallchains( args.filename, args.cols, args.skip, args.stride )
        df = dfs[args.offset]
    else:
        df = readchain( args.filename, args.cols, args.skip, args.stride, args.offset )

    fig = pyplot.figure( figsize=( 18./len(df.columns), 12), tight_layout=True )

    for dex, col in enumerate( df.columns ):
        tmp = df[col].values - df[col].values.mean()
        ac = autocor( tmp )
        # ac2 = numpy.correlate( tmp, tmp[:len(tmp)//2] )
        ax = fig.add_subplot( len(df.columns), 1, dex+1 )
        ax.plot( numpy.arange( len(ac) ), ac, color='blue' )
        # ax.plot( numpy.arange( len(ac2) ), ac2/ac2[0], color='green' )
        ax.set_xlabel( col )
        xmin, xmax = ax.get_xlim()
        ax.plot( [xmin, xmax], [0, 0], linestyle='--', color='red' )

    fig.show()
    pyplot.show()

    if not args.plot_t_vs_n: return

    # Based on https://emcee.readthedocs.io/en/stable/tutorials/autocorr/
    N = numpy.exp( numpy.linspace( numpy.log(100), numpy.log( len(df) ), 10 ) ).astype( int )
    for dex, col in enumerate( df.columns ):
        acbar = numpy.zeros( len(dfs[0]) )
        for tmpdf in dfs:
            tmp = tmpdf[col].values - tmpdf[col].values.mean()
            acbar += autocor( tmp )
        acbar /= len( dfs )

        fig = pyplot.figure( figsize=( 8, 6 ), tight_layout=True )
        ax = fig.add_subplot( 1, 1, 1 )
        ax.set_title( f"Autocor for {col}" )
        ax.plot( numpy.arange( len(acbar) ), acbar, color='blue' )
        xmin, xmax = ax.get_xlim()
        ax.plot( [xmin, xmax], [0, 0], linestyle='--', color='red' )
        fig.show()

        y = []
        for n in N:
            taus = 2.0 * numpy.cumsum( acbar[:n] ) - 1.
            y.append( taus[ auto_window( taus, 5.0 ) ] )

        print( f"Autocorrelation time for {col}: {y[-1]}" )

        fig = pyplot.figure( figsize=( 6, 6 ), tight_layout=True )
        ax = fig.add_subplot( 1, 1, 1 )
        ax.set_xscale( 'log' )
        ax.set_yscale( 'log' )
        ax.set_xlabel( "N" )
        ax.set_ylabel( "t_{ac}" )
        ax.set_title( col )
        ax.plot( N, y, marker='o' )
        fig.show()

    pyplot.show()


# ======================================================================

if __name__ == "__main__":
    main()
