// g++ -g crossconvsub.cc -o crossconvsub -lCCfits -lcfitsio -lgsl

#include <iostream>
#include <fstream>
#include <typeinfo>
#include <cxxopts.hpp>
#include <CCfits/CCfits>
#include <cmath>
#include <gsl/gsl_sf_trig.h>

// **********************************************************************
// **********************************************************************
// **********************************************************************

class CrossConvSub {

private:

  const cxxopts::ParseResult& opts;
  bool verbose;
  string newfile;
  string reffile;
  string newpsffile;
  string refpsffile;

  // C++ has evolved a lot since I used to use it all the time.
  // Figure out what the implications of shared_ptr are
  // My god C++ is complicated compared to Python, or even Java
  struct fitsfile {
    unsigned int xsize;
    unsigned int ysize;
    std::shared_ptr<CCfits::FITS> fileptr = NULL;
    CCfits::PHDU* hdu = NULL;
    std::valarray<float> data;
    std::valarray<float> convdata;
  };
  fitsfile newfits;
  fitsfile reffits;

  double x, y;
  
  int newpsfwid;
  std::valarray<float> newpsf;
  int refpsfwid;
  std::valarray<float> refpsf;

  std::valarray<float> sub;
  
  // ****************************************
  
  string psfname( const string& optname, const string& fitsname )
  {
    string psf;
    if ( this->opts.count(optname) == 1 ) {
      psf = this->opts[optname].as<std::string>();
    }
    else {
      int fitsdex = fitsname.find( ".fits" );
      if ( fitsdex < 0 ) {
        std::cerr << "Error parsing " << fitsname << " for *.fits" << std::endl;
        std::exit(1);
      }
      psf = fitsname.substr( 0, fitsdex ) + ".psf";
    }
    return psf;
  }

  // ****************************************

  void readfits( string& filename, fitsfile& fits )
  {
    try {
      fits.fileptr = std::make_shared<CCfits::FITS>( filename, CCfits::Read , true );
    }
    catch ( CCfits::FITS::CantOpen except ) {
      std::cerr << "Can't open FITS file " << filename << std::endl;
      std::exit(1);
    }
    fits.hdu = &(fits.fileptr->pHDU());
    fits.hdu->readAllKeys();
    if ( fits.hdu->axes() != 2 ) {
      std::cerr << filename << " isn't 2-dimensional." << std::endl;
      std::exit(1);
    }
    fits.xsize = fits.hdu->axis(0);
    fits.ysize = fits.hdu->axis(1);
    if ( fits.hdu->bitpix() != -32 ) {
      std::cerr << filename << " isn't 32-bit floats, and I don't know how to cope." << std::endl;
    }
    fits.hdu->read( fits.data );

    // std::cerr << "Image is " << fits.xsize << " × " << fits.ysize << std::endl;
    // std::cerr << "FITS pixel 101, 201 = data[200*axis0 + 100] = " << fits.data[ 200*fits.xsize + 100 ] << std::endl;
  }

  // ****************************************

  void genpsf( string& filename, std::valarray<float>& psf, int& psfwid, float x, float y ) {
    std::cerr << "Reading psf file " << filename << std::endl;
    std::shared_ptr<CCfits::FITS> fileptr = NULL;
    try {
      fileptr = std::make_shared<CCfits::FITS>( filename, CCfits::Read, true );
    }
    catch( CCfits::FITS::CantOpen except ) {
      std::cerr << "Can't open PSF file " << filename << std::endl;
      std::exit(1);
    }
    auto& hdu = fileptr->extension(1);
    hdu.readAllKeys();

    int l;
    string s1, s2;
    if ( hdu.keyWord("POLNAXIS").value(l) != 2 ) {
      std::cerr << "In " << filename << ", unexpected value fo POLNAXIS = " << l << std::endl;
      std::exit(1);
    }
    if ( hdu.keyWord("POLNGRP").value(l) != 1 ) {
      std::cerr << "In " << filename << ", unexpected value fo POLNGRP = " << l << std::endl;
      std::exit(1);
    }
    hdu.keyWord("POLNAME1").value(s1);
    hdu.keyWord("POLNAME2").value(s2);
    if ( ( s1 != "X_IMAGE" ) || ( s2 != "Y_IMAGE" ) ) {
      std::cerr << "In " << filename << ", unexpected value for POLNAME1 = " << s1
                << " and/or POLNAME2 = " << s2 << std::endl;
      std::exit(1);
    }

    double x0, xsc, y0, ysc, psfsamp;
    int psfxlen, psfylen, psfnparam, psforder;
    // The -=1 is to deal with FITS irritatingly being 1-offset instead of 0-offset
    hdu.keyWord("POLZERO1").value(x0); x0 -= 1;
    hdu.keyWord("POLSCAL1").value(xsc);
    hdu.keyWord("POLZERO2").value(y0); y0 -= 1;
    hdu.keyWord("POLSCAL2").value(ysc);
    hdu.keyWord("PSF_SAMP").value(psfsamp);
    hdu.keyWord("POLDEG1").value(psforder);
    hdu.keyWord("PSFAXIS1").value(psfxlen);
    hdu.keyWord("PSFAXIS2").value(psfylen);
    hdu.keyWord("PSFAXIS3").value(psfnparam);
    
    int nparam = 0;
    for ( int i = 0 ; i <= psforder ; ++i ) nparam += psforder + 1 - i;
    if ( nparam != psfnparam ) {
      std::cerr << "In " << filename << ", nparam = " << nparam << " and psfnparam = " << psfnparam << std::endl;
      std::exit(1);
    }
    
    std::vector< std::valarray<float> > psfdatawrapper;
    hdu.column("PSF_MASK").readArrays( psfdatawrapper, 1, 1 );
    std::valarray<float >& psfdata = psfdatawrapper[0];

    int stampwid = (int)( floor( psfsamp * psfxlen ) + 0.5 );
    if ( ( stampwid % 2 ) == 0 ) stampwid -= 1;
    std::cerr << "psfxlen = " << psfxlen << ", psfsamp = " << psfsamp << ", stampwid = " << stampwid << std::endl;

    auto psfbase = std::valarray<double>( 0., psfxlen*psfylen );
    for ( int yo = 0, off=0 ; yo <= psforder ; yo++ ) {
      double ypow = pow( (y-y0)/ysc, yo );
      for ( int xo = 0, off=0 ; xo <= psforder-yo ; xo++, off+=psfxlen*psfylen ) {
        double xpow = pow( (x-x0)/xsc, xo );
        for ( int i = 0 ; i < psfxlen ; ++i ) {
          for ( int j = 0 ; j < psfylen ; ++j ) {
            psfbase[ j*psfxlen + i ] += psfdata[ off + j*psfxlen + i ] * xpow * ypow;
          }
        }
      }
    }

    std::valarray<double> psfimg( 0., stampwid*stampwid );
    double psftot = 0.;
    double xoff = x - floor( x + 0.5 ) + ( stampwid / 2 );
    double yoff = y - floor( y + 0.5 ) + ( stampwid / 2 );
    std::cerr << "x,y = " << x << ", " << y << " ; stampwid = " << stampwid
              << "; xoff,yoff = " << xoff << ", " << yoff << std::endl;
    // ROB!  Do openmp or openacc here?
    for ( int j = 0 ; j < stampwid ; ++j ) {
      for ( int i = 0 ; i < stampwid ; ++i ) {
        for ( int pj = -psfylen/2 ; pj <= psfylen/2 ; ++pj ) {
          double ysincval;
          double ysincarg = pj - ( j - yoff ) / psfsamp;
          if ( ( ysincarg > 4. ) || ( ysincarg < -4. ) )
            ysincval = 0;
          else
            ysincval = gsl_sf_sinc( ysincarg ) * gsl_sf_sinc( ysincarg/4. );
          for ( int pi = -psfxlen/2 ; pi <= psfxlen/2 ; ++pi ) {
            double xsincarg = pi - ( i - xoff ) / psfsamp;
            double xsincval;
            if ( ( xsincarg > 4. ) || ( xsincarg < -4. ) )
              xsincval = 0;
            else
              xsincval = gsl_sf_sinc( xsincarg ) * gsl_sf_sinc( xsincarg/4. );
            double val = xsincval * ysincval * psfbase[ (pj+psfylen/2)*psfxlen + (pi+psfxlen/2) ];
            psfimg[ j*stampwid + i ] += val;
            psftot += val;
          }
        }
      }
    }
    // Normalize and convert from double to float
    psf.resize( psfimg.size() );
    for ( int i = 0 ; i < psfimg.size() ; ++i )
      psf[i] = psfimg[i] / psftot;
    psfwid = stampwid;
  }

  // **********************************************************************

  void convolve( const std::valarray<float>& image, int imagexsize, int imageysize,
                 const std::valarray<float>& kernel, int kernelxsize, int kernelysize,
                 const std::valarray<float>& convimage )
  {
    if ( ( ( kernelxsize % 2) == 0 ) || ( ( kernelysize % 2 ) == 0 ) ) {
      throw std::length_error( "kernel[xy]size must be odd" );
    }

    convimage.resize( image.size() );
    for ( int i = 0 ; i < imagexsize ; ++i ) {
      int offx0 = -kernelxsize/2;
      if ( i + offx0 < 0 ) offx0 = -i;
      int offx1 = kernelxsize/2;
      if ( i + offx1 >= imagexsize ) offx1 = imagexsize-1-i;

      for ( int j = 0 ; j < imageysize ; ++j ) {
        double norm = 0.;
        double val = 0.;
        int offy0 = -kernelysize/2;
        if ( j + offy0 < 0 ) offy0 = -j;
        int offy1 = kernelysize/2;
        if ( j + offy1 >= imageysize ) offy1 = imageysize-1-j;

        for ( int offx = offx0 ; offx <= offx1 ; ++offx ) {
          int ki = offx + kernelxsize/2;
          for ( int offy = offy0 ; offy <= offy1 ; ++offy ) {
            int kj = offy + kernelysize/2;
            val += image[ (j+offy)*imagexsize + (i+offx) ] * kernel[ kj*kernexsize + ki ];
            norm += kernel[ kj*kernsize + ki ];
          }
        }
        convimage[ j*imagexsize + i ] = val / norm;
      }
    }
  }
  
public:

  // ****************************************

  CrossConvSub( const cxxopts::ParseResult& inopts ) :
    opts(inopts)
  {
    if ( ( opts.count("new") != 1 ) || ( opts.count("ref") != 1 ) || (opts.count("sub") != 1 ) ) {
      std::cerr << "Error, must specify one each of new, ref, sub." << std::endl;
      std::exit(1);
    }
      
    verbose = opts["verbose"].as<bool>();
    newfile = opts["new"].as<std::string>();
    reffile = opts["ref"].as<std::string>();

    if ( ( opts.count("newpsf") > 1 ) || ( opts.count("refpsf") ) ) {
      std::cerr << "Don't give multiple newpsf or refpsf" << std::endl;
      std::exit(1);
    }
    newpsffile = this->psfname( "newpsf", newfile );
    refpsffile = this->psfname( "refpsf", reffile );
  }

  virtual ~CrossConvSub()
  {
  }
  
  void go()
  {
    if ( verbose ) std::cerr << "Reading FITS files..." << std::endl;
    this->readfitsfiles();
    if ( verbose ) std::cerr << "Generating PSF convolution kernels..." << std::endl;
    this->genpsfkernels();
    if ( verbose ) std::cerr << "Convolving..." << std::endl;
    this->convolveimages();
    if ( verbose ) std::cerr << "Subtracting..." << std::endl;
    this->subtract();
    if ( verbose ) std::cerr << "Writing subtraction..." << std::endl;
    this->writesub();
  }

  void readfitsfiles()
  {
    this->readfits( newfile, newfits );
    this->readfits( reffile, reffits );
    if ( ( reffits.xsize != newfits.xsize ) || ( reffits.ysize != newfits.ysize ) ) {
      std::cerr << "Ref is " << reffits.xsize << "×" << reffits.ysize
                << " , New is " << newfits.xsize << "×" << newfits.ysize
                << "; they need to match." << std::endl;
      std::exit(1);
    }

    x = opts["x"].as<float>();
    if ( x < -1e30 ) x = newfits.xsize / 2.;
    y = opts["y"].as<float>();
    if ( y < -1e30 ) y = newfits.ysize / 2.;
  }

  void genpsfkernels()
  {
    this->genpsf( newpsffile, newpsf, newpsfwid, x, y );
    this->genpsf( refpsffile, refpsf, refpsfwid, x ,y );
  }

  void convolveimages()
  {
    this->convolve( newfits.data, refpsf, newfits.convdata );
    this->convolve( reffits.data, newpsf, reffits.convdata );
  }

  void subtract()
  {
    double norm = opts["refmult"].as<double>();
    sub.resize( newfits.data.size() );
    for ( int i = 0 ; i < newfits.data.size() ; ++i )
      sub[i] = newfits.convdata[i] - norm * reffits.convdata[i];
  }

  void writesub()
  {
    throw std::runtime_error( "Writing FITS files not yet implemented" );
  }

};

// **********************************************************************
// **********************************************************************
// **********************************************************************

int main( int argc, char *argv[] )
{
  cxxopts::Options options( "crossconvsub", "Cross-convolution FITS image subtraction" );
  options.add_options()
    ( "v,verbose", "Show debug info" )
    ( "n,new", "Search/Science/New FITS Image", cxxopts::value<std::string>())
    ( "r,ref", "Template/Reference/Ref FITS Image", cxxopts::value<std::string>())
    ( "s,sub", "Output Difference/Subtraction/Sub FITS Image", cxxopts::value<std::string>())
    ( "newpsf", "New PSF file (default: new-.fits+.psf)", cxxopts::value<std::string>())
    ( "refpsf", "Ref PSF file (default: new-.fits+.psf)", cxxopts::value<std::string>())
    ( "x", "x-Position (FITS coords) on image you care about (default: center)",
      cxxopts::value<float>()->default_value("-1e31") )
    ( "y", "y-Position (FITS coords) on image you care about (default: center)",
      cxxopts::value<float>()->default_value("-1e31") )
    ( "m,refmult", "Multiply ref by this value to flux-match",
      cxxopts::value<double>()->default_value("1.") )
    ( "h,help", "Print help" )
    ;
  auto opts = options.parse( argc, argv );

  if ( opts.count("help") ) {
    std::cerr << options.help() << std::endl << std::endl;
    std::cerr << "Assumes that new and ref are aligned, background-subtracted, " << std::endl;
    std::cerr << "and scaled to each other." << std::endl;
    std::exit(0);
  }

  auto Subber = CrossConvSub( opts );
  Subber.go();
}
