#include <fitsfile.hh>
#include <memory>
#include <string>
#include <iostream>
#include <unistd.h>

int main( int argc, char* argv[] ) {
  std::string filename( "fodder/c4d_221110_043018_ori.58.fits" );

  std::shared_ptr<FITSFile> f;

  for ( int i = 0 ; i < 10 ; ++i ) {
    sleep(2);
    f.release();
    f = FITSFile::open( filename );
    auto hdu = f->hdu(0);
    std::cerr << "Made " << i+1 << " so far..." << std::endl;
  }
}


