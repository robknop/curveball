#include "testbase.hh"
#include <wcswrapper.hh>
#include <memory>
#include <unordered_map>
#include <vector>
#include <string>
#include <sstream>
#include <iomanip>
#include <cmath>

class TestWCSWrapper : public TestBase {
private:
  std::string filename;
  int hdunum;

  bool test_pix_to_world( WCSWrapper& wcs ) {
    const double precision = 0.1/3600.;
    std::vector<double> x{    0.,    0., 2047., 2047., 1024. };
    std::vector<double> y{    0., 4095.,    0., 4096., 2048. };
    std::vector<double> expectedra{ 7.88120, 8.29523, 7.88095, 8.29588, 8.08838 };
    std::vector<double> expecteddec{ -43.67752, -43.67697, -43.82671, -43.82612, -43.75213 };

    auto radec = wcs.c_pix_to_world( x, y );
    std::vector<double>& ra = radec.first;
    std::vector<double>& dec = radec.second;

    bool retval = true;
    for ( int i = 0 ; i < ra.size() ; ++i ) {
      std::stringstream str("");
      str << "test_pix_to_world: Pixel " << std::fixed << std::setprecision(1) << x[i] << ", " << y[i]
          << " translated to RA " << std::setprecision(6) << ra[i] << ", DEC " << dec[i]
          << " (expected " << expectedra[i] << ", " << expecteddec[i] << ")";
      if ( ( fabs( ra[i] - expectedra[i] ) > precision ) ||
           ( fabs( dec[i] - expecteddec[i] ) > precision ) ) {
        logger->error( str.str() );
        retval = false;
      }
      else {
        logger->debug( str.str() );
      }
    }
    return retval;
  }

  bool test_world_to_pix( WCSWrapper& wcs ) {
    const double precision = 0.02;
    std::vector<double> ra{ 7.9, 8.1, 8.29 };
    std::vector<double> dec{ -43.68, -43.75, -43.825 };
    std::vector<double> expectedx = { 33.73, 995.02, 2031.37 };
    std::vector<double> expectedy = { 185.94, 2162.83, 4037.98 };
    auto xy = wcs.world_to_c_pix( ra, dec );
    std::vector<double>& x = xy.first;
    std::vector<double>& y = xy.second;

    bool retval = true;
    for ( int i = 0 ; i < x.size() ; ++i ) {
      std::stringstream str("");
      str << "test_world_to_pix: World " << std::fixed << std::setprecision(6) << ra[i] << ", " << dec[i]
          << " translated to (" << std::setprecision(2) << x[i] << ", " << y[i]
          << " (expected " << expectedx[i] << ", " << expectedy[i] << ")";
      if ( ( fabs( x[i] - expectedx[i] ) > precision ) ||
           ( fabs( y[i] - expectedy[i] ) > precision ) ) {
        logger->error( str.str() );
        retval = false;
      } else {
        logger->debug( str.str() );
      }
    }
    return retval;
  }

  bool test_wrap() {
    logger->error( "test_wrap not implemented" );
    return false;
  }

public:
  TestWCSWrapper( int argc, char* argv[] )
    : TestBase( argc, argv )
  {
    filename = "fodder/c4d_221110_043018_ori.58.fits.fz";
    hdunum = 1;
  }

  int run_tests() {
    std::stringstream str("");

    std::shared_ptr<WCSWrapper> wcs;
    try {
      wcs = WCSWrapper::read_from_file( filename, hdunum, funclogger );
    } catch( std::exception e ) {
      str.str("");
      str << "Failed to read WCS from " << filename;
      logger->error( str.str() );
      return 1;
    }

    std::unordered_map<std::string,std::function<bool()>> tests{
      { "test_pix_to_world", std::bind(&TestWCSWrapper::test_pix_to_world, this, std::ref(*wcs)) },
      { "test_world_to_pix", std::bind(&TestWCSWrapper::test_world_to_pix, this, std::ref(*wcs)) },
      { "test_wrap", std::bind(&TestWCSWrapper::test_wrap, this) },
    };
    std::vector<std::string> testnames{
      "test_pix_to_world",
      "test_world_to_pix",
      "test_wrap"
    };

    int retval = actually_run_tests( testnames, tests );
    return retval;
  }
};

// **********************************************************************

int main( int argc, char* argv[] ) {
  auto tester = TestWCSWrapper( argc, argv );
  exit( tester.run_tests() );
}
