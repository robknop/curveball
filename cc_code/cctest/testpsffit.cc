#include "testbase.hh"
#include <utility>
#include <iostream>
#include <iomanip>
#include <random>
#include <cmath>
#include <fitsio.h>
#include <psffit.hh>
#include <psfexreader.hh>

class TestPSFFit : public TestBase{
private:
  std::string psffilename;
  std::shared_ptr<argparser::Argument<bool>> arg_dumpfits;
  std::shared_ptr<argparser::Argument<bool>> arg_dumpchains;
  std::shared_ptr<argparser::Argument<bool>> arg_nostumbler;

  const double x = 50.25;
  const double y = 1700.66;
  const int stampsize = 25;
  const double flux = 2000.;
  const double x0 = 49.;
  const double σx0 = 1.;
  const double y0 = 1702.;
  const double σy0 = 1.;
  const double flux0 = 1500.;
  const double σflux0 = 20;
  const int clip0x = 38;
  const int clip0y = 1688;
  const double skynoise = 1.;

  const int steps = 200;
  const int burnsteps = 200;
  const int nwalkers = 20;

  unsigned long int stamp_seed = 23;
  unsigned long int stumbler_seed = 42;

  const std::vector<double> expected_param_abs{ 1., 0.01, 0.01 };
  const std::vector<double> expected_covar_abs{ 0.1, 0.001, 0.001,
                                                0.001, 1e-4, 1e-6,
                                                0.001, 1e-6, 1e-4 };
  const std::vector<double> expected_param_fixpos_abs{ 1., 1e-6, 1e-6 };
  const std::vector<double> expected_covar_fixpos_abs{ 0.1, 1e-8, 1e-8,
                                                       1e-8, 1e-8, 1e-8,
                                                       1e-8, 1e-8, 1e-8 };


  const std::vector<double> stumbler_expected_param{ 1992., 50.26, 1700.64 };
  const std::vector<double> stumbler_expected_covar{ 2315.36, -0.10662, 0.13188,
                                                    -0.10662, 0.004539, 0.0005240,
                                                     0.13188, 0.0005240, 0.0059956 };

  const std::vector<double> stumbler_expected_param_fixpos{ 1995., 50.25, 1700.66 };
  const std::vector<double> stumbler_expected_covar_fixpos{ 4425.75, 0., 0.,
                                                            0., 0., 0.,
                                                            0., 0., 0. };


  // Maybe worry about the fact that the off-diagonal LM covariances are
  //   so much smaller than the stumbler ones.  One of the two must not
  //   be right!  Looking at the stumbler code, I don't think it's wrong
  //   but I'd be shocked if the GSL were wrong, so I must be missing something.
  // ...if I run for 2000 burn-in steps and 2000 measurement steps, the
  //   off-diagonal cvariances change, all getting smaller in absolute
  //   value.  I think this is a case of the
  //   uncertainty-on-the-uncertainty.  These covariances are small, so
  //   they're not going to be known to high relative precision.

  const std::vector<double> lm_expected_param{ 1995., 50.26, 1700.66 };
  const std::vector<double> lm_expected_covar{ 2216.14, -0.0070551, 0.0036748,
                                              -0.0070551, 0.0046804, 9.73857e-5,
                                               0.0036748, 9.73857e-5, 0.0060237 };

  const std::vector<double> lm_expected_param_fixpos{ 1996., 50.25, 1700.66 };
  const std::vector<double> lm_expected_covar_fixpos{ 2216.14, 0., 0.,
                                                      0., 0., 0.,
                                                      0., 0., 0. };

  // **********************************************************************

  std::pair<std::shared_ptr<double[]> , std::shared_ptr<double[]>> make_stamp( PSFExReader &psf,
                                                                               double flux, double x, double y,
                                                                               unsigned long int seed )
  {
    auto stamp = psf.psf_stamp<double>( x, y, clip0x, clip0y, stampsize );
    auto weight = std::make_shared<double[]>( stampsize*stampsize );

    if ( seed == 0 ) {
      seed = std::random_device()();
      // std::cerr << "seed= " << seed << std::endl;
    }
    std::mt19937 rng( seed );

    for ( int i = 0 ; i < stampsize * stampsize ; ++i ) {
      stamp[i] *= flux;

      if ( stamp[i] <= 0 )
        weight[i] = 1. / ( skynoise * skynoise );
      else
        weight[i] = 1. / ( skynoise*skynoise + stamp[i] );

      // Noise up the stamp

      auto sig = std::sqrt( 1 / weight[i] );
      std::normal_distribution<double> normal( 0., sig );
      stamp[i] += normal( rng );
    }

    return std::make_pair( stamp, weight );
  }

  // **********************************************************************

  void dump_to_fits( double *array, const std::string& filename )
  {
    std::remove( filename.c_str() );
    fitsfile *fits;
    int fitsstatus = 0;
    long naxes[2] = { stampsize, stampsize };
    long firstpix[2] = { 1, 1 };
    fits_create_file(  &fits, filename.c_str(), &fitsstatus );
    if ( fitsstatus ) {
      std::stringstream str("");
      str << "Error creating FITS file " << filename << ", status = " << fitsstatus;
      logger->error( str.str() );
    }
    else {
      fits_create_img( fits, DOUBLE_IMG, 2, naxes, &fitsstatus );
      fits_write_pix( fits, TDOUBLE, firstpix, stampsize*stampsize, array, &fitsstatus );
      fits_close_file( fits, &fitsstatus );
    }
  }

  // **********************************************************************

  bool checkparam( const std::vector<double> &param, const std::vector<std::vector<double>> &covar,
                   const std::vector<double> &expectedparam, const std::vector<double> &expectedcovar,
                   const std::vector<double> &paramabs, const std::vector<double> &covarabs )
  {
    bool ok = true;
    std::stringstream str("");
    int sz = expectedparam.size();
    for ( int i = 0 ; i < sz ; ++i ) {
      if ( !isclose( expectedparam[i], param[i], paramabs[i] ) ) {
        str.str("");
        str << "param[ " << i << "] = " << param[i] << " is more than "
            << paramabs[i] << " away from " << expectedparam[i];
        logger->error( str.str() );
        ok = false;
      }
      for ( int j = 0 ; j < sz ; ++j ) {
        if ( !isclose( expectedcovar[j*sz + i], covar[i][j], covarabs[j*sz + i] ) ) {
          str.str("");
          str << "covar[" << i << "][" << j << "] = " << covar[i][j]
              << " is more than " << covarabs[j*sz + i ] << " away from " << expectedcovar[j*sz + i];
          logger->error( str.str() );
          ok = false;
        }
      }
    }
    return ok;
  }

  bool checkparam( const double *param, const double *covar,
                   const std::vector<double> &expectedparam, const std::vector<double> &expectedcovar,
                   const std::vector<double> &paramabs, const std::vector<double> &covarabs )
  {
    bool ok = true;
    std::stringstream str("");
    int sz = expectedparam.size();
    for ( int i = 0 ; i < sz ; ++i ) {
      if ( !isclose( expectedparam[i], param[i], paramabs[i] ) ) {
        str.str("");
        str << "param[ " << i << "] = " << param[i] << " is more than "
            << paramabs[i] << " away from " << expectedparam[i];
        logger->error( str.str() );
        ok = false;
      }
      for ( int j = 0 ; j < sz ; ++j ) {
        if ( !isclose( expectedcovar[j*sz + i], covar[j*sz + i], covarabs[j*sz + i] ) ) {
          str.str("");
          str << "covar[" << i << "][" << j << "] = " << covar[j*sz + i]
              << " is more than " << covarabs[j*sz + i] << " away from " << expectedcovar[j*sz + i];
          logger->error( str.str() );
          ok = false;
        }
      }
    }
    return ok;
  }

  // **********************************************************************

  bool test_psffit_stumbler() {
    auto psf = PSFExReader( psffilename, funclogger );
    auto stwt = make_stamp( psf, flux, x, y, stamp_seed );
    auto stamp = stwt.first;
    auto weight = stwt.second;

    if ( arg_dumpfits->given ) {
      dump_to_fits( stamp.get(), "test_psffit_stumbler_stamp.fits" );
      dump_to_fits( weight.get(), "test_psffit_stumbler_weight.fits" );
    }

    std::vector<double> initparam{ flux0, x0, y0 };
    std::vector<double> initsig{ σflux0, σx0, σy0 };
    auto fitter = PSFFitter( psffilename, stamp.get(), weight.get(), stampsize, clip0x, clip0y );

    std::vector<double> param;
    std::vector<std::vector<double>> covar;
    std::string filebase = ( arg_dumpchains->given ? "test_psffit" : "" );
    fitter.run_stumbler_psf_fit( param, covar, initparam, initsig, stumbler_seed, filebase,
                                 false, nwalkers, burnsteps, steps );

    auto chisq = fitter.stumbler_reduced_chisq();

    std::cout << std::fixed;
    std::cout << "flux: " << std::setprecision(0) << param[0] << " ± " << std::sqrt( covar[0][0] )
              << "\nx: " << std::setprecision(3) << param[1] << " ± " << std::sqrt( covar[1][1] )
              << "\ny: " << param[2] << " ± " << std::sqrt( covar[2][2] )
              << "\nχ²/ν = " << chisq << std::endl;

    if ( arg_dumpfits->given ) {
      auto model = psf.psf_stamp<double>( param[1], param[2], clip0x, clip0y, stampsize, true );
      auto resid = std::make_shared<double[]>( stampsize*stampsize );
      auto reducedresid = std::make_shared<double[]>( stampsize*stampsize );
      for ( int i = 0 ; i < stampsize * stampsize ; ++i ) {
        model[i] *= param[0];
        resid[i] = stamp[i] - model[i];
        reducedresid[i] = resid[i] * std::sqrt( weight[i] );
      }
      dump_to_fits( model.get(), "test_psffit_stumbler_model.fits" );
      dump_to_fits( resid.get(), "test_psffit_stumbler_resid.fits" );
      dump_to_fits( reducedresid.get(), "test_psffit_stumbler_reduced_resid.fits" );
    }

    return checkparam( param, covar, stumbler_expected_param, stumbler_expected_covar,
                       expected_param_abs, expected_covar_abs );
  }

  // **********************************************************************

  bool test_psffit_repeated_stumbler() {
    auto psf = PSFExReader( psffilename, funclogger );

    const int ntrials = 11;

    double totalflux=0., totalflux2=0., totalx=0., totalx2=0., totaly=0., totaly2=0.;
    double totaldflux=0., totaldflux2=0., totaldx=0., totaldx2=0., totaldy=0., totaldy2=0.;
    double totalchisq=0., totalchisq2=0.;

    for (int trial = 0 ; trial < ntrials ; ++trial ) {
      auto stwt = make_stamp( psf, flux, x, y, 0 );
      auto stamp = stwt.first;
      auto weight = stwt.second;

      std::vector<double> initparam{ flux0, x0, y0 };
      std::vector<double> initsig{ σflux0, σx0, σy0 };
      auto fitter = PSFFitter( psffilename, stamp.get(), weight.get(), stampsize, clip0x, clip0y );

      std::vector<double> param;
      std::vector<std::vector<double>> covar;
      fitter.run_stumbler_psf_fit( param, covar, initparam, initsig, stumbler_seed, "",
                                   false, nwalkers, burnsteps, steps );

      auto chisq = fitter.stumbler_reduced_chisq();

      totalflux += param[0];
      totalflux2 += param[0] * param[0];
      totalx += param[1];
      totalx2 += param[1] * param[1];
      totaly += param[2];
      totaly2 += param[2] * param[2];
      totaldflux += std::sqrt( covar[0][0] );
      totaldflux2 += covar[0][0];
      totaldx += std::sqrt( covar[1][1] );
      totaldx2 += covar[1][1];
      totaldy += std::sqrt( covar[2][2] );
      totaldy2 += covar[2][2];

      totalchisq += chisq;
      totalchisq2 += chisq * chisq;
    }

    totalflux /= ntrials;
    totalflux2 = std::sqrt( totalflux2 / ntrials - totalflux * totalflux );
    totalx /= ntrials;
    totalx2 = std::sqrt( totalx2 / ntrials - totalx * totalx );
    totaly /= ntrials;
    totaly2 = std::sqrt( totaly2 / ntrials - totaly * totaly );
    totaldflux /= ntrials;
    totaldflux2 = std::sqrt( totaldflux2 / ntrials - totaldflux * totaldflux );
    totaldx /= ntrials;
    totaldx2 = std::sqrt( totaldx2 / ntrials - totaldx * totaldx );
    totaldy /= ntrials;
    totaldy2 = std::sqrt( totaldy2 / ntrials - totaldy * totaldy );
    totalchisq /= ntrials;
    totalchisq2 = std::sqrt( totalchisq2 / ntrials - totalchisq * totalchisq );

    std::cout << std::fixed;
    std::cout << "flux: " << std::setprecision(0) << totalflux << " (σ " << totalflux2
              << ") ± " << totaldflux << " (σ " << totaldflux2
              << ", reduced " << totaldflux / std::sqrt(ntrials) << ")\n"
              << "x: " << std::setprecision(3) << totalx << " (σ " << totalx2
              << ") ± " << totaldx << " (σ " << totaldx2
              << ", reduced " << totaldx / std::sqrt(ntrials) << ")\n"
              << "y: " << totaly << " (σ " << totaly2
              << ") ± " << totaldy << " (σ " << totaldy2
              << ", reduced " << totaldy / std::sqrt(ntrials) << ")\n"
              << "chisq/ν: " << totalchisq << " (σ " << totalchisq2 << ")\n";

    return true;
  }

  // **********************************************************************

  bool test_psffit_stumbler_func() {
    auto psf = PSFExReader( psffilename, funclogger );
    auto stwt = make_stamp( psf, flux, x, y, stamp_seed );
    auto stamp = stwt.first;
    auto weight = stwt.second;

    if ( arg_dumpfits->given ) {
      dump_to_fits( stamp.get(), "test_psffit_func_stamp.fits" );
      dump_to_fits( weight.get(), "test_psffit_func_weight.fits" );
    }

    double param[3];
    double covar[3*3];
    std::string filebase = ( arg_dumpchains->given ? "test_psffit_func" : "" );
    fit_psf_stumbler( psffilename.c_str(), stamp.get(), weight.get(), stampsize, clip0x, clip0y,
                      flux0, x0, y0, σflux0, σx0, σy0,
                      false, steps, burnsteps, nwalkers, param, covar, stumbler_seed, filebase.c_str() );

    std::cout << std::fixed;
    std::cout << "flux: " << std::setprecision(0) << param[0] << " ± " << std::sqrt( covar[3*0 + 0] )
              << "\nx: " << std::setprecision(3) << param[1] << " ± " << std::sqrt( covar[3*1 + 1] )
              << "\ny: " << param[2] << " ± " << std::sqrt( covar[3*2 + 2] )
              << std::endl;

    return checkparam( param, covar, stumbler_expected_param, stumbler_expected_covar,
                       expected_param_abs, expected_covar_abs );
  }

  // **********************************************************************

  bool test_psffit_stumbler_func_fixpos() {
    auto psf = PSFExReader( psffilename, funclogger );
    auto stwt = make_stamp( psf, flux, x, y, stamp_seed );
    auto stamp = stwt.first;
    auto weight = stwt.second;

    if ( arg_dumpfits->given ) {
      dump_to_fits( stamp.get(), "test_psffit_func_stamp.fits" );
      dump_to_fits( weight.get(), "test_psffit_func_weight.fits" );
    }

    double param[3];
    double covar[3*3];
    std::string filebase = ( arg_dumpchains->given ? "test_psffit_func" : "" );
    fit_psf_stumbler( psffilename.c_str(), stamp.get(), weight.get(), stampsize, clip0x, clip0y,
                      flux0, x, y, σflux0, 0., 0.,
                      true, steps, burnsteps, nwalkers, param, covar, stumbler_seed, filebase.c_str() );

    std::cout << std::fixed;
    std::cout << "flux: " << std::setprecision(0) << param[0] << " ± " << std::sqrt( covar[3*0 + 0] )
              << "\nx: " << std::setprecision(3) << param[1] << " ± " << std::sqrt( covar[3*1 + 1] )
              << "\ny: " << param[2] << " ± " << std::sqrt( covar[3*2 + 2] )
              << std::endl;

    return checkparam( param, covar, stumbler_expected_param_fixpos, stumbler_expected_covar_fixpos,
                       expected_param_fixpos_abs, expected_covar_fixpos_abs );
  }

  // **********************************************************************

  bool test_psffit_lm() {
    auto psf = PSFExReader( psffilename, funclogger );
    auto stwt = make_stamp( psf, flux, x, y, stamp_seed );
    auto stamp = stwt.first;
    auto weight = stwt.second;

    std::vector<double> initparam{ flux0, x0, y0 };
    auto fitter = PSFFitter( psffilename, stamp.get(), weight.get(), stampsize, clip0x, clip0y );

    std::vector<double> param;
    std::vector<std::vector<double>> covar;
    fitter.run_lm_psf_fit( param, covar, initparam, false );

    std::cout << std::fixed;
    std::cout << "flux: " << std::setprecision(0) << param[0] << " ± " << std::sqrt( covar[0][0] )
              << "\nx: " << std::setprecision(3) << param[1] << " ± " << std::sqrt( covar[1][1] )
              << "\ny: " << param[2] << " ± " << std::sqrt( covar[2][2] )
              << std::endl;

    return checkparam( param, covar, lm_expected_param, lm_expected_covar,
                       expected_param_abs, expected_covar_abs );
  }

  // **********************************************************************

  bool test_psffit_lm_func() {
    auto psf = PSFExReader( psffilename, funclogger );
    auto stwt = make_stamp( psf, flux, x, y, stamp_seed );
    auto stamp = stwt.first;
    auto weight = stwt.second;

    double param[3];
    double covar[3*3];
    fit_psf_lm( psffilename.c_str(), stamp.get(), weight.get(), stampsize, clip0x, clip0y,
                flux0, x0, y0, false, param, covar );

    std::cout << std::fixed;
    std::cout << "flux: " << std::setprecision(0) << param[0] << " ± " << std::sqrt( covar[3*0 + 0] )
              << "\nx: " << std::setprecision(3) << param[1] << " ± " << std::sqrt( covar[3*1 + 1] )
              << "\ny: " << param[2] << " ± " << std::sqrt( covar[3*2 + 2] )
              << std::endl;

    return checkparam( param, covar, lm_expected_param, lm_expected_covar,
                       expected_param_abs, expected_covar_abs );
  }

  // **********************************************************************

  bool test_psffit_lm_func_fixpos() {
    auto psf = PSFExReader( psffilename, funclogger );
    auto stwt = make_stamp( psf, flux, x, y, stamp_seed );
    auto stamp = stwt.first;
    auto weight = stwt.second;

    double param[3];
    double covar[3*3];
    fit_psf_lm( psffilename.c_str(), stamp.get(), weight.get(), stampsize, clip0x, clip0y,
                flux0, x, y, true, param, covar );

    std::cout << std::fixed;
    std::cout << "flux: " << std::setprecision(0) << param[0] << " ± " << std::sqrt( covar[3*0 + 0] )
              << "\nx: " << std::setprecision(3) << param[1] << " ± " << std::sqrt( covar[3*1 + 1] )
              << "\ny: " << param[2] << " ± " << std::sqrt( covar[3*2 + 2] )
              << std::endl;

    return checkparam( param, covar, lm_expected_param_fixpos, lm_expected_covar_fixpos,
                       expected_param_fixpos_abs, expected_covar_fixpos_abs );
  }

  // **********************************************************************

  bool test_psffit_repeated_lm() {
    auto psf = PSFExReader( psffilename, funclogger );

    double totalflux=0., totalflux2=0., totalx=0., totalx2=0., totaly=0., totaly2=0.;
    double totaldflux=0., totaldflux2=0., totaldx=0., totaldx2=0., totaldy=0., totaldy2=0.;

    const int ntrials = 21;

    for ( int i = 0 ; i < ntrials ; ++i ) {
      auto stwt = make_stamp( psf, flux, x, y, 0 );
      auto stamp = stwt.first;
      auto weight = stwt.second;

      std::vector<double> initparam{ flux0, x0, y0 };
      auto fitter = PSFFitter( psffilename, stamp.get(), weight.get(), stampsize, clip0x, clip0y );

      std::vector<double> param;
      std::vector<std::vector<double>> covar;
      fitter.run_lm_psf_fit( param, covar, initparam, false );

      totalflux += param[0];
      totalflux2 += param[0] * param[0];
      totalx += param[1];
      totalx2 += param[1] * param[1];
      totaly += param[2];
      totaly2 += param[2] * param[2];

      totaldflux += std::sqrt( covar[0][0] );
      totaldflux2 += covar[0][0];
      totaldx += std::sqrt( covar[1][1] );
      totaldx2 += covar[1][1];
      totaldy += std::sqrt( covar[2][2] );
      totaldy2 += covar[2][2];
    }

    totalflux /= ntrials;
    totalflux2 = sqrt( totalflux2 / ntrials - totalflux * totalflux );
    totalx /= ntrials;
    totalx2 = sqrt( totalx2 / ntrials - totalx * totalx );
    totaly /= ntrials;
    totaly2 = sqrt( totaly2 / ntrials - totaly * totaly );

    totaldflux /= ntrials;
    totaldflux2 = sqrt( totaldflux2 / ntrials - totaldflux * totaldflux );
    totaldx /= ntrials;
    totaldx2 = sqrt( totaldx2 / ntrials - totaldx * totaldx );
    totaldy /= ntrials;
    totaldy2 = sqrt( totaldy2 / ntrials - totaldy * totaldy );

    std::cout << std::fixed;
    std::cout << "flux: " << std::setprecision(0) << totalflux << " (σ " << totalflux2
              << ") ± " << totaldflux << " (σ " << totaldflux2
              << ", reduced " << totaldflux / std::sqrt(ntrials) << ")\n"
              << "x: " << std::setprecision(3) << totalx << " (σ " << totalx2
              << ") ± " << totaldx << " (σ " << totaldx2
              << ", reduced " << totaldx / std::sqrt(ntrials) << ")\n"
              << "y: " << totaly << " (σ " << totaly2
              << ") ± " << totaldy << " (σ " << totaldy2
              << ", reduced " << totaldy / std::sqrt(ntrials) << ")\n";

    return true;
  }

  // **********************************************************************

public:

  TestPSFFit( int argc, char* argv[] )
    : TestBase( argc, argv, true )
  {
    arg_nostumbler = std::make_shared<argparser::Argument<bool>>( "-n", "--nostumbler", "nostumbler", false,
                                                                  "Don't run stumbler tests, just LM" );
    arg_dumpfits = std::make_shared<argparser::Argument<bool>>( "-d", "--dumpfits", "dumpfits", false,
                                                                "Dump psf stamps, fit, residual to fits files." );
    arg_dumpchains = std::make_shared<argparser::Argument<bool>>( "-c", "--dumpchains", "dumpchains", false,
                                                                  "Dump mcmc chains to test_*.dat." );
    auto arg_randmake = argparser::Argument<bool>( "-r", "--random-make-seed", "randdata", false,
                                                   "Use system entropy for random seed for fake data generation." );
    auto arg_randstum = argparser::Argument<bool>( "-s", "--random-stumbler-seed", "randstum",
                                                   false, "Use system entropy for stumbler seed." );
    psffilename = "fodder/testpsfexreader.psf";
    argparser->add_arg( std::ref(*arg_dumpfits) );
    argparser->add_arg( std::ref(*arg_dumpchains) );
    argparser->add_arg( std::ref(*arg_nostumbler) );
    argparser->add_arg( arg_randmake );
    argparser->add_arg( arg_randstum );
    parse_args( argc, argv );

    if ( arg_randstum.given ) {
      stumbler_seed = std::random_device()();
    }
    if ( arg_randmake.given ) {
      stamp_seed = std::random_device()();
    }
  }

  // **********************************************************************

  int run_tests() {
    std::stringstream str("");

    str.str("");
    str << "Reading " << psffilename;
    logger->debug( str.str() );

    std::unordered_map<std::string,std::function<bool()>> tests{
      { "test_psffit_stumbler", std::bind(&TestPSFFit::test_psffit_stumbler, this ) },
      { "test_psffit_stumbler_func", std::bind(&TestPSFFit::test_psffit_stumbler_func, this ) },
      { "test_psffit_stumbler_func_fixpos", std::bind(&TestPSFFit::test_psffit_stumbler_func_fixpos, this ) },
      { "test_psffit_repeated_stumbler", std::bind(&TestPSFFit::test_psffit_repeated_stumbler, this ) },
      { "test_psffit_lm", std::bind(&TestPSFFit::test_psffit_lm, this ) },
      { "test_psffit_lm_func", std::bind(&TestPSFFit::test_psffit_lm_func, this ) },
      { "test_psffit_lm_func_fixpos", std::bind(&TestPSFFit::test_psffit_lm_func_fixpos, this ) },
      { "test_psffit_repeated_lm", std::bind(&TestPSFFit::test_psffit_repeated_lm, this ) },
    };
    std::vector<std::string> testnames;
    if ( ! arg_nostumbler->given ) {
      testnames.push_back( "test_psffit_stumbler" );
      testnames.push_back( "test_psffit_stumbler_func" );
      testnames.push_back( "test_psffit_stumbler_func_fixpos" );
      // testnames.push_back( "test_psffit_repeated_stumbler" );
    };
    testnames.push_back( "test_psffit_lm" );
    testnames.push_back( "test_psffit_lm_func" );
    testnames.push_back( "test_psffit_lm_func_fixpos" );
    // testnames.push_back( "test_psffit_repeated_lm" );

    int retval = actually_run_tests( testnames, tests );
    return retval;
  }
};


// **********************************************************************

int main( int argc, char* argv[] ) {
  MPI_Init( &argc, &argv );
  auto tester = TestPSFFit( argc, argv );
  exit( tester.run_tests() );
}
