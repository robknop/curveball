#include "testbase.hh"
#include <fitsfile.hh>
#include <wcswrapper.hh>
#include <vector>
#include <unordered_map>
#include <iomanip>
#include <cstring>

class TestFITSFile : public TestBase {
private:
  std::string filename, fzfilename;

  // **********************************************************************

  void dump_header( FITSHDU *hdu ) {
    std::stringstream str("");
    str << "Header keywords:" << std::endl;
    for ( int i = 0 ; i < hdu->nhdrkeywords() ; ++i ) {
      std::string kw = hdu->hdrkw(i);
      const char* comment = hdu->hdrkwcomment(kw);
      str << "  " << std::setw(8) << hdu->hdrkw(i) << " : " << std::setw(20) << hdu->hdrsfmt(kw);
      if ( comment != nullptr ) str << " / " << comment;
      str << std::endl;
    }
    logger->debug( str.str() );

    str.str("");
    str << "Header comments:" << std::endl;
    for ( int i = 0 ; i < hdu->nhdrcomments() ; ++i ) str << "    \"" << hdu->hdrcomment(i) << "\"" << std::endl;
    logger->debug( str.str() );

    str.str("");
    str << "Header history:" << std::endl;
    for ( int i = 0 ; i < hdu->nhdrhistory() ; ++i ) str << "    \"" << hdu->hdrhistory(i) << "\"" << std::endl;
    logger->debug( str.str() );
  }

  // **********************************************************************

  bool test_create() {
    float* data = new float[ 32*32 ];
    for ( int j = 0 ; j < 32 ; ++j ) {
      for ( int i = 0 ; i < 32 ; ++i ) {
        data[ j*32 + i ] = i*32 + j;
      }
    }
    auto img = FITSImage<float>::create( 32, 32, data, funclogger );

    bool ok = true;
    for ( int j = 0 ; j < 32 ; ++j ) {
      for ( int i = 0 ; i < 32 ; ++i ) {
        if ( img->pix( i, j ) != i*32 + j ) ok = false;
      }
    }

    img = FITSImage<float>::create( 32, 32, nullptr, funclogger );
    for ( int j = 0 ; j < 32 ; ++j ) {
      for ( int i = 0 ; i < 32 ; ++i ) {
        if ( img->pix( i, j ) != 0. ) ok = false;
      }
    }

    delete[] data;
    return ok;
  }

  // **********************************************************************

  bool test_read_image( FITSFile &ff, int whichhdu ) {
    std::stringstream str("");

    auto hdu1 = ff.hdu( whichhdu );

    dump_header( hdu1 );

    // auto iw = (FITSImageWrapper*)hdu1;
    // str << "axlen = { " << iw->axlen(0) << ", " << iw->axlen(1) << "}; galdata->dsize = {"
    //     << iw->galdata->dsize[0] << ", " << iw->galdata->dsize[1] << "}";
    // logger->debug( str.str() );

    return ( hdu1 != nullptr );
  }

  // **********************************************************************

  bool test_hdr_kw( FITSFile &ff, int whichhdu ) {
    std::stringstream str("");

    auto hdu1 = ff.hdu( whichhdu );

    int nhdrrecords = hdu1->nhdrkeywords() + hdu1->nhdrcomments() + hdu1->nhdrhistory();
    if (  nhdrrecords != 240 ) {
      str.str("");
      str << "test_hdr_kw : found " << nhdrrecords << " hdr keywords, expected 240.";
      logger->error( str.str() );
      return false;
    }

    if ( hdu1->hdrkw( 8 ) != "OBSERVAT" ) {
      str << "test_hdr_kw : hdrkw[8] = " << hdu1->hdrkw(8) << ", expected \"OBSERVAT\"";
      logger->error( str.str() );
      return false;
    }

    return true;
  }

  // **********************************************************************

  bool test_hdr_value( FITSFile &ff, int whichhdu ) {
    std::stringstream str("");

    auto hdu = ff.hdu( whichhdu );

    if ( hdu->hdrsval( "PROPID" ) != "2022A-724693" ) {
      str.str("");
      str << "test_hdr_value : header \"PROPID\" has value \"" << hdu->hdrsval("PROPID")
          << "\", expected \"2022A-724693\"";
      logger->error( str.str() );
      return false;
    }

    if ( strcmp( hdu->hdrkwcomment("SEEING"), "Median FWHM of seeing in Arcsec / Goldstein" ) != 0 ) {
      str.str("");
      str << "test_hdr_value : header \"SEEING\" has comment \"" << hdu->hdrkwcomment("SEEING")
          << "\", which is not what was expected";
      logger->error( str.str() );
      return false;
    }

    return true;
  }

  // **********************************************************************

  bool test_simpleimage_hdu( FITSFile &fzff, FITSFile &ff ) {
    std::stringstream str("");
    bool retval = true;

    // Uncommenting this will, at least as I've laid the tests out as of this writing,
    // cause the next test to fail.
    // FITSHDU* ffhdu0 = ff.hdu( 0 );
    // FITSHDU* fzffhdu0 = fzff.hdu( 0 );
    // FITSHDU* fzffhdu1 = fzff.hdu( 1 );
    // str.str("");
    // str << "ffhdu0 type = " << ffhdu0->hdutype
    //     << ", fzffhdu0 type = " << fzffhdu0->hdutype
    //     << ", fzffhdu1 type = " << fzffhdu1->hdutype;
    // logger->debug( str.str() );

    if ( ! fzff.is_simple_2d_image() ) {
      logger->error( "test_simpleimage_hdu : .fits.fz file isn't simple" );
      retval = false;
    }
    if ( ! ff.is_simple_2d_image() ) {
      logger->error( "test_simpleimage_hdu : .fits file isn't simple" );
      retval = false;
    }
    if ( fzff.simple_2d_image_hdu_num() != 1 ) {
      logger->error( "test_simpleimage_hdu : image HDU of .fits.fz isn't 1" );
      retval = false;
    }
    if ( ff.simple_2d_image_hdu_num() != 0 ) {
      logger->error( "test_simpleimage_hdu : image HDU of .fits isn't 0" );
      retval = false;
    }
    return retval;
  }

  // **********************************************************************

  bool test_all_hdus_are_still_nullptr( FITSFile &ff ) {
    bool ok = true;
    for ( int i = 0 ; i < ff.nhdus() ; ++i ) {
      if ( ff.just_give_me_the_hdu_array_element_dammit( i ) != nullptr ) ok = false;
    }
    return ok;
  }

  // **********************************************************************

  bool test_image_pixels( FITSFile &ff ) {
    const int ncoords = 4;
    double x[] = { 128, 256, 1024, 333 };
    double y[] = { 256, 128, 333, 1024 };
    double expected_values[] = { 1.26689, 3.82977, 12.3305, 17.6313 };
    double precision = 1e-4;

    FITSImage<float> *img = ff.get_image<float>();
    bool ok = true;
    for ( int i = 0 ; i < ncoords ; ++i ) {
      if ( fabs( img->pix( x[i], y[i] ) - expected_values[i] ) > precision ) {
        std::stringstream str("");
        str << "test_image_pixels: ("
            << std::setw(4) << x[i] << ", " << std::setw(4) << y[i] << ") = "
            << std::fixed << std::setprecision(4) << img->pix( x[i], y[i] )
            << ", but expected " << expected_values[i];
        logger->error( str.str() );
        ok = false;
      }
    }
    return ok;
  }

  // **********************************************************************

  bool test_save_to( FITSFile &ff ) {
    std::stringstream str("");
    bool ok = true;

    std::string outname( "fodder/testfitsfile_test_save_to.fits" );
    ff.save_to( outname, true );

    // Read it back and make sure a few things are right.

    FITSFile writtenfile( outname, funclogger );
    if ( writtenfile.hdu(0)->hdrsval( "SEQID" ) != "ELAIS-DDF-SEQ-A" ) {
      str.str("");
      str << "test_save_to: written file\'s SEQID header is " << writtenfile.hdu(0)->hdrsval("SEQID")
          << ", expected \"ELAIS-DDF-SEQ-A\"";
      logger->error( str.str() );
      ok = false;
    }

    if ( !test_image_pixels( writtenfile ) ) {
      ok = false;
    }

    return ok;
  }

  // **********************************************************************

  bool test_wcs( FITSFile &ff ) {
    std::stringstream str("");
    bool ok = true;
    FITSImage<float> *img = ff.get_image<float>();

    // I copied this stuff from testwcswrapper.cc .... think

    double precision = 0.1/3600.;
    std::vector<double> x{    0.,    0., 2047., 2047., 1024. };
    std::vector<double> y{    0., 4095.,    0., 4096., 2048. };
    std::vector<double> expectedra{ 7.88120, 8.29523, 7.88095, 8.29588, 8.08838 };
    std::vector<double> expecteddec{ -43.67752, -43.67697, -43.82671, -43.82612, -43.75213 };

    auto radec = img->wcs()->c_pix_to_world( x, y );
    std::vector<double>& calcra = radec.first;
    std::vector<double>& calcdec = radec.second;

    for ( int i = 0 ; i < calcra.size() ; ++i ) {
      std::stringstream str("");
      str << "test_wcs: Pixel " << std::fixed << std::setprecision(1) << x[i] << ", " << y[i]
          << " translated to RA " << std::setprecision(6) << calcra[i] << ", DEC " << calcdec[i]
          << " (expected " << expectedra[i] << ", " << expecteddec[i] << ")";
      if ( ( fabs( calcra[i] - expectedra[i] ) > precision ) ||
           ( fabs( calcdec[i] - expecteddec[i] ) > precision ) ) {
        logger->error( str.str() );
        ok = false;
      }
      else {
        logger->debug( str.str() );
      }
    }

    precision = 0.02;
    std::vector<double> ra{ 7.9, 8.1, 8.29 };
    std::vector<double> dec{ -43.68, -43.75, -43.825 };
    std::vector<double> expectedx = { 33.73, 995.02, 2031.37 };
    std::vector<double> expectedy = { 185.94, 2162.83, 4037.98 };
    auto xy = img->wcs()->world_to_c_pix( ra, dec );
    std::vector<double>& calcx = xy.first;
    std::vector<double>& calcy = xy.second;

    for ( int i = 0 ; i < calcx.size() ; ++i ) {
      std::stringstream str("");
      str << "test_wcs: World " << std::fixed << std::setprecision(6) << ra[i] << ", " << dec[i]
          << " translated to (" << std::setprecision(2) << calcx[i] << ", " << calcy[i]
          << " (expected " << expectedx[i] << ", " << expectedy[i] << ")";
      if ( ( fabs( calcx[i] - expectedx[i] ) > precision ) ||
           ( fabs( calcy[i] - expectedy[i] ) > precision ) ) {
        logger->error( str.str() );
        ok = false;
      } else {
        logger->debug( str.str() );
      }
    }

    return ok;
  }

  // **********************************************************************

  bool test_copy_image_data( FITSFile &ff ) {
    FITSImage<float> *img = ff.get_image<float>();

    std::shared_ptr<float[]> data = img->copy_image_data();

    bool ok = true;
    for ( int i = 0 ; i < img->axlen(0) ; i += 100 )  {
      for ( int j = 0 ; j < img->axlen(1) ; j += 100 ) {
        if ( img->pix( i, j ) != data[ j * img->axlen(0) + i ] ) ok = false;
      }
    }

    // I should probably also put in a copy of copy_void_image_data
    // That routine will have been run in copy_save_to, so it's tested there.

    return ok;
  }


  // **********************************************************************

  bool test_add_hdu() {
    logger->error( "test_add_hdu not implemented" );
    return false;
  }

  // **********************************************************************

  bool test_subtile( FITSFile &ff ) {
    std::stringstream str("");
    std::ios oldstate( nullptr );
    bool ok = true;
    FITSImage<float> *img = ff.get_image<float>();

    int x0 = 1351;
    int x1 = 1432;
    int y0 = 1212;
    int y1 = 1313;
    // Subtile of same type will get hit in test_clone
    auto tile = img->subtile<double>( x0, x1, y0, y1 );

    std::vector<int> x{ 43 };
    std::vector<int> y{ 27 };
    std::vector<float> expected{ 46.3437 };
    float precision = 1e-4;

    for ( int i = 0 ; i < x.size() ; ++i ) {
      if ( fabs( tile->pix( x[i], y[i] ) - expected[i] ) > precision ) {
        str.str("");
        str << "test_subtile : pix(" << x[i] << ", " << y[i] << ") = "
            << tile->pix( x[i], y[i] ) << ", expected " << expected[i];
        logger->error( str.str() );
        ok = false;
      }
    }

    // Make sure Tile WCS is right
    std::vector<double> pixx{ 29, 48, 0, 80 };
    std::vector<double> pixy{ 48, 29, 0, 100 };
    std::vector<double> fullpixx( pixx.size() );
    std::vector<double> fullpixy( pixy.size() );
    for ( int i = 0 ; i < pixx.size() ; ++i ) {
      fullpixx[i] = pixx[i] + x0;
      fullpixy[i] = pixy[i] + y0;
    }
    double wcsprecision = 1e-6;
    auto world = tile->wcs()->c_pix_to_world( pixx, pixy );
    auto fullworld = img->wcs()->c_pix_to_world( fullpixx, fullpixy );
    for ( int i = 0 ; i < pixx.size() ; ++i ) {
      if ( ( fabs(world.first[i] - fullworld.first[i]) > wcsprecision) ||
           ( fabs(world.second[i] - fullworld.second[i]) > wcsprecision ) ) {
        str.str("");
        oldstate.copyfmt( str );
        str << "test_subtile : wcs mismatch, pixel (" << std::fixed << std::setprecision(2)
            << pixx[i] << ", " << pixy[i] << ") expected ra/dec (" << std::setprecision(6)
            << fullworld.first[i] << ", " << fullworld.second[i] << ") but got ("
            << world.first[i] << ", " << world.second[i] << ")" << std::endl;
        logger->error( str.str() );
        str.copyfmt( oldstate );
        ok = false;
      }
    }

    auto backtofull = img->wcs()->world_to_c_pix( fullworld.first, fullworld.second );
    auto backtotile = tile->wcs()->world_to_c_pix( fullworld.first, fullworld.second );
    double backprecision = 1e-2;
    for ( int i = 0 ; i < backtofull.first.size() ; ++i ) {
      if ( ( fabs( backtofull.first[i] - ( backtotile.first[i] + x0 ) ) > backprecision ) ||
           ( fabs( backtofull.second[i] - ( backtotile.second[i] + y0 ) ) > backprecision ) ) {
        str.str("");
        oldstate.copyfmt( str );
        str << "test_subtile : wcs mismatch, world (" << std::fixed << std::setprecision(6)
            << fullworld.first[0] << ", " << fullworld.second[0] << ") on full image was ("
            << std::setprecision(2) << backtofull.first[i] << ", " << backtofull.second[i]
            << ") and on tile was (" << backtotile.first[i] << ", " << backtotile.second[i]
            << ") with offset (" << x0 << ", " << y0 << ")";
        logger->error( str.str() );
        str.copyfmt( oldstate );
        ok = false;
      }
    }

    return ok;
  }

  // **********************************************************************

  bool test_clone( FITSFile &ff ) {
    std::stringstream str("");
    std::ios oldstate( nullptr );
    bool ok = true;
    FITSImage<float> *img = ff.get_image<float>();

    int x0 = 1351;
    int x1 = 1432;
    int y0 = 1212;
    int y1 = 1313;
    auto tile = img->subtile<float>( x0, x1, y0, y1 );
    auto clone = tile->clone<double>();
    tile = nullptr;

    std::vector<int> x{ 43 };
    std::vector<int> y{ 27 };
    std::vector<float> expected{ 46.3437 };
    float precision = 1e-4;

    // Make sure that if I futz a tile pxiel, it doesn't futz the original image pixel
    clone->data()[ (y[0]+1) * clone->axlen(0) + (x[0]+1) ] = -666.;
    if ( img->pix( x0+x[0]+1, y0+y[0]+1 ) == -666. ) {
      str.str("");
      str << "test_clone: modifying the clone' spixel modified the original image's pixel";
      logger->error( str.str() );
      ok = false;
    }

    // The rest of this was copied from test_subtile... should
    //   move it out to its own function

    for ( int i = 0 ; i < x.size() ; ++i ) {
      if ( fabs( clone->pix( x[i], y[i] ) - expected[i] ) > precision ) {
        str.str("");
        str << "test_clone : pix(" << x[i] << ", " << y[i] << ") = "
            << clone->pix( x[i], y[i] ) << ", expected " << expected[i];
        logger->error( str.str() );
        ok = false;
      }
    }

    // Make sure Tile WCS is right
    std::vector<double> pixx{ 29, 48, 0, 80 };
    std::vector<double> pixy{ 48, 29, 0, 100 };
    std::vector<double> fullpixx( pixx.size() );
    std::vector<double> fullpixy( pixy.size() );
    for ( int i = 0 ; i < pixx.size() ; ++i ) {
      fullpixx[i] = pixx[i] + x0;
      fullpixy[i] = pixy[i] + y0;
    }
    double wcsprecision = 1e-6;
    auto world = clone->wcs()->c_pix_to_world( pixx, pixy );
    auto fullworld = img->wcs()->c_pix_to_world( fullpixx, fullpixy );
    for ( int i = 0 ; i < pixx.size() ; ++i ) {
      if ( ( fabs(world.first[i] - fullworld.first[i]) > wcsprecision) ||
           ( fabs(world.second[i] - fullworld.second[i]) > wcsprecision ) ) {
        str.str("");
        oldstate.copyfmt( str );
        str << "test_clone : wcs mismatch, pixel (" << std::fixed << std::setprecision(2)
            << pixx[i] << ", " << pixy[i] << ") expected ra/dec (" << std::setprecision(6)
            << fullworld.first[i] << ", " << fullworld.second[i] << ") but got ("
            << world.first[i] << ", " << world.second[i] << ")" << std::endl;
        logger->error( str.str() );
        str.copyfmt( oldstate );
        ok = false;
      }
    }

    return ok;
  }


  // **********************************************************************

public:
  TestFITSFile( int argc, char* argv[] )
    : TestBase( argc, argv )
  {
    fzfilename = "fodder/c4d_221110_043018_ori.58.fits.fz";
    filename = "fodder/c4d_221110_043018_ori.58.fits";
  }

  int run_tests() {
    std::stringstream str("");

    str.str("");
    str << "Reading " << filename;
    logger->debug( str.str() );
    FITSFile fzff( fzfilename, funclogger );
    FITSFile ff( filename, funclogger );

    std::unordered_map<std::string,std::function<bool()>> tests{
      { "test_create", std::bind( &TestFITSFile::test_create, this ) },
      { "test_simpleimage_hdu", std::bind( &TestFITSFile::test_simpleimage_hdu, this, std::ref(fzff), std::ref(ff) ) },
      { "test_all_hdus_are_still_nullptr", std::bind( &TestFITSFile::test_all_hdus_are_still_nullptr,
                                                      this, std::ref(fzff) ) },
      { "test_read_image", std::bind( &TestFITSFile::test_read_image, this, std::ref(fzff), 1 ) },
      { "test_hdr_kw", std::bind( &TestFITSFile::test_hdr_kw, this, std::ref(fzff), 1 ) },
      { "test_hdr_value", std::bind( &TestFITSFile::test_hdr_value, this, std::ref(fzff), 1 ) },
      { "test_image_pixels", std::bind( &TestFITSFile::test_image_pixels, this, std::ref(ff) ) },
      { "test_image_pixels_fz", std::bind( &TestFITSFile::test_image_pixels, this, std::ref(fzff) ) },
      { "test_save_to", std::bind( &TestFITSFile::test_save_to, this, std::ref(ff) ) },
      { "test_wcs", std::bind( &TestFITSFile::test_wcs, this, std::ref(fzff) ) },
      { "test_copy_image_data", std::bind( &TestFITSFile::test_copy_image_data, this, std::ref(fzff) ) },
      { "test_add_hdu", std::bind( &TestFITSFile::test_add_hdu, this ) },
      { "test_subtile", std::bind( &TestFITSFile::test_subtile, this, std::ref(fzff) ) },
      { "test_clone", std::bind( &TestFITSFile::test_clone, this, std::ref(fzff) ) },
      };
    std::vector<std::string> testnames{
      "test_create",
      "test_simpleimage_hdu",
      "test_all_hdus_are_still_nullptr",
      "test_read_image",
      "test_simpleimage_hdu",          // Test again to see if it works after image is read
      "test_hdr_kw",
      "test_hdr_value",
      "test_image_pixels",
      "test_image_pixels_fz",
      "test_save_to",
      "test_wcs",
      "test_copy_image_data",
      "test_add_hdu",
      "test_subtile",
      "test_clone",
    };

    int retval = actually_run_tests( testnames, tests );
    return retval;
  }
};

// **********************************************************************

int main( int argc, char* argv[] ) {
  auto tester = TestFITSFile( argc, argv );
  return( tester.run_tests() );
}

