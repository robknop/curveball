#include "testbase.hh"
#include <psfexreader.hh>
#include <fitsfile.hh>
#include <unordered_map>
#include <vector>
#include <sstream>
#include <iomanip>
#include <cmath>

class TestPSFExReader : public TestBase {
private:
  std::string psffilename;
  std::shared_ptr<argparser::Argument<bool>> arg_dumpfits;

  bool test_psf_stamp( PSFExReader &psf ) {
    const double x = 50.25;
    const double y = 1700.66;
    const int stampsize = 25;
    auto stamp = psf.psf_stamp<double>( x, y, stampsize );

    bool retval = true;

    long double tot = 0.;
    for ( int i = 0 ; i < stampsize*stampsize ; ++i ) tot += stamp[i];
    if ( ( tot < 0.999999 ) || ( tot > 1.000001 ) ) {
      std::stringstream str("");
      str << "test_psf_stamp: sum of psf stamp is not 1, it's " << std::fixed << std::setprecision(7) << tot;
      logger->error( str.str() );
      retval = false;
    }

    std::vector<double> spotcheckx = { 5, 10, 24, 10, 10 };
    std::vector<double> spotchecky = { 10, 10, 10, 5, 24 };
    std::vector<double> expectedvalues = { 0.00056987, 0.01135416, 7.717e-5, 0.00168584, -9.2215e-6  };
    double precision = 1e-8;

    for ( int i = 0 ; i < spotcheckx.size() ; ++i ) {
      double val = stamp[ spotchecky[i]*stampsize + spotcheckx[i] ];
      std::stringstream str("");
      str << "test_psf_stamp: At (" << std::fixed << std::setprecision(1)
          << std::setw(4) << spotcheckx[i] << ", " << std::setw(4) << spotchecky[i]
          << ") stamp=" << std::defaultfloat << std::setprecision(10) << val
          << " (Expected " << expectedvalues[i] << ")";
      if (  fabs( val - expectedvalues[i] ) > precision ) {
        logger->error( str.str() );
        retval = false;
      } else {
        logger->debug( str.str() );
      }
    }

    if ( arg_dumpfits->given ) {
      // I may be playing a naughty game with shared pointers here
      auto img = FITSImage<double>::create( stampsize, stampsize, stamp.get(), funclogger );
      FITSFile f( funclogger );
      f.add_hdu( img );
      f.save_to( "psf.fits", true );
    }

    return retval;
  }

  bool test_psf_offset_stamp( PSFExReader &psf ) {
    bool retval = true;
    const double x = 50.25;
    const double y = 1700.66;
    const int stampsize = 32;
    const int offx = 31;
    const int offy = 1688;
    auto stamp = psf.psf_stamp<double>( x, y, offx, offy, stampsize );

    std::vector<double> spotcheckx = { 12, 17, 31, 17, 17 };
    std::vector<double> spotchecky = { 11, 11, 11, 6, 25 };
    std::vector<double> expectedvalues = { 0.00056987, 0.01135416, 7.717e-5, 0.00168584, -9.2215e-6  };
    // Values are a bit off since the shfited PSF has shifted a bit of
    // its flux off of the edge of the image -- but also I've got a
    // larger image! -- so the normalization's going to affect things
    // slightly differently.
    double absprecision = 1e-4;
    double relprecision = 1e-3;

    for ( int i = 0 ; i < spotcheckx.size() ; ++i ) {
      double val = stamp[ spotchecky[i]*stampsize + spotcheckx[i] ];
      std::stringstream str("");
      str << "test_psf_offset_stamp: At (" << std::fixed << std::setprecision(1)
          << std::setw(4) << spotcheckx[i] << ", " << std::setw(4) << spotchecky[i]
          << ") stamp=" << std::defaultfloat << std::setprecision(10) << val
          << " (Expected " << expectedvalues[i] << ")";
      if ( ( fabs( val - expectedvalues[i] ) > absprecision ) ||
           ( fabs( ( val - expectedvalues[i] ) / expectedvalues[i] ) > relprecision ) ) {
        logger->error( str.str() );
        retval = false;
      } else {
        logger->debug( str.str() );
      }
    }

    if ( arg_dumpfits->given ) {
      // I may be playing a naughty game with shared pointers here
      auto img = FITSImage<double>::create( stampsize, stampsize, stamp.get(), funclogger );
      FITSFile f( funclogger );
      f.add_hdu( img );
      f.save_to( "offset_psf.fits", true );
    }

    return retval;
  }

  bool test_add_psf_to_image( PSFExReader &psf ) {
    logger->warn( "test_add_psf_to_image not implemented" );
    return true;
  }

  bool test_add_stamp_to_image( PSFExReader &psf ) {
    logger->warn( "test_add_stamp_to_image not implemented" );
    return true;
  }


public:
  TestPSFExReader( int argc, char* argv[] )
    : TestBase( argc, argv, true )
  {
    arg_dumpfits = std::make_shared<argparser::Argument<bool>>( "-d", "--dumpfits", "dumpfits", false,
                                                                "Dump psf stamps to psf.fits and offset_psf.fits" );
    psffilename = "fodder/testpsfexreader.psf";
    argparser->add_arg( std::ref(*arg_dumpfits) );
    parse_args( argc, argv );
  }

  int run_tests() {
    std::stringstream str("");

    str.str("");
    str << "Reading " << psffilename;
    logger->debug( str.str() );
    auto psf = PSFExReader( psffilename, funclogger );

    std::unordered_map<std::string,std::function<bool()>> tests{
      { "test_psf_stamp", std::bind(&TestPSFExReader::test_psf_stamp, this, std::ref(psf) ) },
      { "test_psf_offset_stamp", std::bind(&TestPSFExReader::test_psf_offset_stamp, this, std::ref(psf) ) },
      { "test_add_psf_to_image", std::bind(&TestPSFExReader::test_add_psf_to_image, this, std::ref(psf) ) },
      { "test_add_stamp_to_image", std::bind(&TestPSFExReader::test_add_stamp_to_image, this, std::ref(psf) ) },
    };
    std::vector<std::string> testnames{
      "test_psf_stamp",
      "test_psf_offset_stamp",
      "test_add_psf_to_image",
      "test_add_stamp_to_image"
    };

    int retval = actually_run_tests( testnames, tests );
    return retval;
  }
};

// **********************************************************************

int main( int argc, char* argv[] ) {
  auto tester = TestPSFExReader( argc, argv );
  exit( tester.run_tests() );
}



