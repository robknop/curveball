#include <argparser.hh>
#include <vector>
#include <unordered_map>
#include <sstream>
#include <iostream>
#include <cmath>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

class TestBase {
protected:
  std::shared_ptr<spdlog::logger> logger, funclogger;
  argparser::ArgParser *argparser;
  std::shared_ptr<argparser::Argument<bool>> arg_info;
  std::shared_ptr<argparser::Argument<bool>> arg_debug;
  std::shared_ptr<argparser::Argument<bool>> arg_help;

  void parse_args( int argc, char* argv[] ) {
    argparser->parse( argc, argv );

    auto mylevel = spdlog::level::info;
    auto funclevel = spdlog::level::warn;

    if (arg_debug->given) {
      mylevel = spdlog::level::debug;
      funclevel = spdlog::level::debug;
    } else if ( arg_info->given) {
      funclevel = spdlog::level::info;
    }

    logger = spdlog::stderr_color_mt( "test" );
    logger->set_level( mylevel );
    funclogger = spdlog::stderr_color_mt( "func" );
    funclogger->set_level( funclevel );

    if ( arg_help->given ) {
      std::cerr << argparser->help();
      std::exit(0);
    }
  }

  bool isclose( double val1, double val2, double abs=0., double rel=0. ) {
    if ( ( abs <= 0 )  && ( rel <= 0 ) )
      throw std::runtime_error( "Must have at least one of abs or rel >0" );

    bool ok = true;
    if ( ( abs > 0 ) && ( fabs( val1 - val2 ) > abs ) ) ok = false;
    if ( ( rel > 0 ) && ( fabs( (val1 - val2) / val1 ) > rel ) ) ok =false;

    return ok;
  }
  
  TestBase( int argc, char* argv[], bool manuallyparseargs=false ) {
    argparser = argparser::ArgParser::get();
    arg_info = std::make_shared<argparser::Argument<bool>>( "-v", "", "info", false,
                                                            "Show info messages (default: just error & warning)" );
    arg_debug = std::make_shared<argparser::Argument<bool>>( "-vv", "", "debug", false, "Show debug messages" );
    arg_help = std::make_shared<argparser::Argument<bool>>( "-h", "--help", "help", false, "Show help." );
    argparser->add_arg( std::ref(*arg_info) );
    argparser->add_arg( std::ref(*arg_debug) );
    argparser->add_arg( std::ref(*arg_help) );
    if ( !manuallyparseargs ) parse_args( argc, argv );
    // TODO : get help here
  }

  int actually_run_tests( std::vector<std::string>& testnames,
                          std::unordered_map<std::string,std::function<bool()>>& tests ) {
    std::stringstream str("");
    int retval = 0;
    for ( auto testname: testnames ) {
      str.str("");
      // str << "Running test " << testname;
      // logger->debug( str.str() );
      auto test = tests[testname];
      str.str("");
      str << testname << " : ";
      bool result = false;
      try {
        result = test();
        str << (result ? "passed" : "failed" );
      }
      catch ( std::exception &ex ) {
        str << "failed; exception: " << ex.what();
        retval = 1 ;
      }
      catch ( ... ) {
        str << "failed; some kind of exception";
        retval = 1;
      }
      if ( !result ) {
        logger->error( str.str() );
        retval = 1;
      }
      else {
        logger->info( str.str() );
      }
    }
    return retval;
  }
};
