#include "testbase.hh"
#include <vector>
#include <map>
#include <mpi.h>
#include <runner.hh>

// AS it exists as of this writing, the Makefile will only test this
// running without mpirun, i.e. running in a single thread.

class TestRunner : public TestBase {
private:
  std::vector<runner::RunnerDouble> results;

public:
  TestRunner( int argc, char* argv[] )
    : TestBase( argc, argv )
  {
  }

  void square_collector( const runner::RunnerDouble &result, int i )
  {
    results[i].set( result.get() );
  }

  std::unique_ptr<runner::RunnerDouble> square_worker( const runner::RunnerDouble &param )
  {
    return std::make_unique<runner::RunnerDouble>( param.get() * param.get() );
  }


  bool square_numbers( int n )
  {
    std::stringstream str("");

    std::vector<runner::RunnerDouble> params(n);
    results.resize( n );

    for ( int i = 0 ; i < n ; ++i ) {
      params[i].set( (double)i );
    }

    auto runner = runner::Runner<runner::RunnerDouble,runner::RunnerDouble>(
      MPI_COMM_WORLD,
      std::bind( &TestRunner::square_collector, this, std::placeholders::_1, std::placeholders::_2 ),
      std::bind( &TestRunner::square_worker, this, std::placeholders::_1 ),
      funclogger );
    runner.go( params );

    bool ok = true;
    for ( int i = 0 ; i < n ; ++i ) {
      if ( fabs( results[i].get() - params[i].get()*params[i].get() ) > 1e-8 ) {
        str.str("");
        str << params[i].get() << " squared is not " << results[i].get();
        logger->error( str.str() );
        ok = false;
      }
    }

    return ok;
  }

  int run_tests()
  {
    std::unordered_map<std::string,std::function<bool()>> tests{
      { "test_runner", std::bind( &TestRunner::square_numbers, this, 10 ) }
    };
    std::vector<std::string> testnames{
      "test_runner"
    };

    int retval = actually_run_tests( testnames, tests );
    return retval;
  }
};

// **********************************************************************

int main( int argc, char* argv[] ) {
  MPI_Init( &argc, &argv );
  auto tester = TestRunner( argc, argv );
  return( tester.run_tests() );
}
