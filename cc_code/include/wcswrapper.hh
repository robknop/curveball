#ifndef INCLUDE_WSCWRAPPER_HH
#define INCLUDE_WCSWRAPPER_HH 1

#include <memory>
#include <vector>
#include <string>
#include <spdlog/spdlog.h>

// Forward delcarations of things declared in gnuastro/wcs.h and stuff included there
struct wcsprm;

/**
 * \brief A wrapper around the gnuastro wcs functions.
 *
 * Here to avoid having to clutter my code with the overly complex data structures
 * for converting between pixel and woord coordinates, and to encapsulate the FITS
 * to C coordinate conversions.
 *
 * Only handles FITS files with a single 2d image (could be .fz'ed).
 */
class WCSWrapper {
public:

  WCSWrapper( std::shared_ptr<spdlog::logger> logger=nullptr );
  WCSWrapper( std::string &filename, int hdunum, std::shared_ptr<spdlog::logger> logger=nullptr );
  virtual ~WCSWrapper();

  WCSWrapper(const WCSWrapper&) = delete;
  WCSWrapper& operator=(const WCSWrapper&) = delete;

  /**
   * Wrap an existing GAL wcsprm structure.
   *
   * The returned WCSWrapper object takes ownership of the wcsprm object, and will
   * free it in its destuctor, so don't free it manually!
   */
  static std::shared_ptr<WCSWrapper> wrap( struct wcsprm* wcstowrap, std::shared_ptr<spdlog::logger> logger=nullptr );

  /**
   * Read a WCS from the header of a FITS Image.
   *
   */
  static std::shared_ptr<WCSWrapper> read_from_file( std::string &filename, int hdunum,
                                                     std::shared_ptr<spdlog::logger> logger=nullptr );

  std::pair<std::vector<double>,std::vector<double>> c_pix_to_world( const std::vector<double> &x,
                                                                     const std::vector<double> &y ) const;
  std::pair<std::vector<double>,std::vector<double>> world_to_c_pix( const std::vector<double> &ra,
                                                                     const std::vector<double> &dec ) const;

  struct wcsprm* wcsprm() { return _wcs; }

private:
  struct wcsprm* _wcs = nullptr;

  std::shared_ptr<spdlog::logger> logger;
  void make_logger();

};

#endif
