#ifndef INCLUDE_FITSFILE_HH
#define INCLUDE_FITSFILE_HH 1

#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <unordered_map>
#include <spdlog/spdlog.h>

// Forward declaration of stuff in GAL, so that I don't have to include
// GAL headers here, thereby cluttering the namesapce of antyhing that
// includes this header.
struct gal_data_t;
typedef struct gal_data_t gal_data_t;

// Forward declarations
class FITSFile;
template<class T> class FITSImage;
// template<class T> class FITSTile;
class WCSWrapper;

// **********************************************************************

/**
 * Encapsulates a single HDU within a FITSFile
 *
 */
class FITSHDU {
  friend class FITSFile;

protected:
  std::shared_ptr<spdlog::logger> logger;

  std::vector<std::string> _hdrkeywords;
  std::unordered_map<std::string,int> _hdrdtype;
  std::unordered_map<std::string,std::string> _hdrkwcomments;
  std::unordered_map<std::string,std::string> _hdrstringvalues;
  std::unordered_map<std::string,bool> _hdrboolvalues;
  std::unordered_map<std::string,long> _hdrintvalues;
  std::unordered_map<std::string,double> _hdrdoublevalues;
  std::vector<std::string> _hdrcomments;
  std::vector<std::string> _hdrhistory;

  FITSFile *parentfile;
  int _hdunum;
  std::string _hdustr;

  FITSHDU( FITSFile *parentfile, int hdunum, std::shared_ptr<spdlog::logger> logger );
  FITSHDU( std::shared_ptr<spdlog::logger> logger );

  void set_hdunum( int hdunum ) {
    _hdunum = hdunum;
    _hdustr = std::to_string( _hdunum );
  }

  void read_header();
  void clear_header();

public:
  // HDU types (corresponding to uppercase types in cfitsio). Actual
  // definitions aren't here so I can avoid including in this header
  // file things like fitsio.h and other C headers that would make my
  // life complicated.  (The goal is that users of this class won't have
  // to pollute their namespace by including those things.)
  static const int image_hdu;
  static const int ascii_tbl;
  static const int binary_tbl;

  // Image data types (corresponding to GAL_TYPE_* from gnu astro library)
  static const int type_uint8;
  static const int type_uint16;
  static const int type_int16;
  static const int type_float;
  static const int type_double;

  // Header data types
  static const int hdr_bool;
  static const int hdr_string;
  static const int hdr_int;
  static const int hdr_double;

  /**
   * The CFITSIO hdutype.  One of IMAGE_HDU, ASCII_TBL, BINARY_TBL (also FITSHDU::image_hdu etc.).
   *
   * Constants are in the <fitsio.h> header, but the same constants
   * in lower case are static const members of FITSHDU
   *
   */
  int hdutype;

  FITSHDU( const FITSHDU& ) = delete;
  FITSHDU() = delete;
  FITSHDU& operator=( const FITSHDU& ) = delete;

  int nhdrkeywords() const { return _hdrkeywords.size(); }
  int nhdrcomments() const { return _hdrcomments.size(); }
  int nhdrhistory() const { return _hdrhistory.size(); }

  const std::string& hdrkw( int n ) const { return _hdrkeywords.at(n); }
  int hdrdtype( const std::string& kw ) const { return _hdrdtype.at(kw); }
  const std::string& hdrsval( const std::string& kw ) const { return _hdrstringvalues.at(kw); }
  bool hdrbval( const std::string& kw ) const { return _hdrboolvalues.at(kw); }
  int hdrival( const std::string& kw ) const { return _hdrintvalues.at(kw); }
  double hdrdval( const std::string& kw ) const { return _hdrdoublevalues.at(kw); }
  const std::string hdrsfmt( const std::string& kw ) const;
  const char* hdrkwcomment( const std::string& kw ) const {
    auto i = _hdrkwcomments.find( kw );
    if ( i == _hdrkwcomments.end() ) return nullptr;
    else return i->second.c_str();
  }

  const std::string& hdrcomment( int n ) const { return _hdrcomments.at(n); }
  const std::string& hdrhistory( int n ) const { return _hdrhistory.at(n); }

  virtual ~FITSHDU();
};

// **********************************************************************

/**
 * \brief A wrapper around a CFITSIO fits file
 *
 * This class name is hazardous, because there's a FITSfile (notice
 * lowercase second f) defined in fitsio.h
 */
class FITSFile
{
  friend class FITSHDU;
  template<class T> friend class FITSImage;

private:
  std::string filename;   // Has to be non-constant since cfitsio routines don't know about const

protected:
  std::shared_ptr<spdlog::logger> logger;

  void initialize();
  void make_logger();

  std::vector<std::shared_ptr<FITSHDU>> _hdus;

  bool hdu_is_type( int n, int hdutype );

  /**
   * \brief utility routine used by simple_2d_image_hdu_num
   *
   */
  int naxis_of_hdu( int n );

public:
  FITSFile( FITSFile& ) = delete;
  FITSFile& operator=( FITSFile& ) = delete;

  FITSFile();
  FITSFile( std::shared_ptr<spdlog::logger> logger );
  FITSFile( const std::string &filename, std::shared_ptr<spdlog::logger> logger = nullptr );

  virtual ~FITSFile();

  /**
   * \brief Open the filename for reading (*not* writing)
   *
   */
  void open_file( const std::string &filename );

  /**
   * \brief Is the file a single-HDU (sorta) 2d image?
   *
   * A "simple 2d image" is a .fits or .fits.fz of a single two-dimensional image.
   * That will be either a single HDU that's an image, or two HDUs, where the
   * first HDU is that .fz header thingy, and the second HDU is a compressed
   * image.
   */
  bool is_simple_2d_image();

  /**
   * \brief Return HDU (0 or 1) of the image, or -1 if it's not a simple 2d image.
   *
   * (See is_simple_2d_image).
   */
  int simple_2d_image_hdu_num();

  /**
   * \brief get the FITS image if this is a simple image
   */
  template<class T> FITSImage<T>* get_image() {
    int num = simple_2d_image_hdu_num();
    if ( ( num != 0 ) && ( num != 1 ) )
      throw std::runtime_error( "FITSFile::get_image : not a simple 2d image!" );
    // return std::static_pointer_cast<FITSImage<T>, FITSHDU>( hdu(num) );
    return (FITSImage<T>*)( hdu(num) );
  }

  bool hdu_is_image( int n ) { return hdu_is_type( n, FITSHDU::image_hdu ); }
  bool hdu_is_asciitbl( int n ) { return hdu_is_type( n, FITSHDU::ascii_tbl ); }
  bool hdu_is_binarytbl( int n ) { return hdu_is_type( n, FITSHDU::binary_tbl ); }
  size_t nhdus() const { return _hdus.size(); }

  /**
   * \brief Return the FITSHDU object for the given HDU, counting from 0.
   *
   * Loads the header and data for the indicated HDU into memory.  Caches it,
   * so that subsequent calls will return the same FITSHDU*.
   *
   * If the requested HDU is actually an image, the real type will be
   * FITSImage*, which derives from FITSHDU*, and it should be safe to
   * cast the pointer to FITSImage
   *
   */
  FITSHDU* hdu( size_t n );
  // This next one is just for my tests
  FITSHDU* just_give_me_the_hdu_array_element_dammit( size_t n ) const {
    return _hdus[n].get();
  }

  /**
   * \brief Create a new image HDU from a gal_data_t structure and add it to the FITSFile.
   *
   * Takes ownership of the gal_data_t structure and everything inside
   * it.  Returns the hdunum of the created image
   *
   */
  template<class T> int append_image( gal_data_t *gdata );

  template<class T> int append_image( int width, int height );




  /**
   * \brief Add a new hdu to the FITSFile (just in memory!)
   *
   */
  void add_hdu( std::shared_ptr<FITSHDU> newhdu );

  /**
   * \brief Write out a new FITS file.
   *
   */
  void save_to( const std::string& outfile, bool overwrite=false );
};

// **********************************************************************

// Need this to use smart pointers to gal_data_t

class gal_data_t_deleter {
public:
  void operator()( gal_data_t* g );
};

/**
 * \brief A not-to-be-used-by-the-end-user-probably wrapper for doing some dynamic typing.
 *
 * NOTE: this uses the GNU Astronomy Library to actually read the FITS
 * file.  Something VERY SCARY in that library is that the "array"
 * member of the gal_data_t structure is defined with the "restrict"
 * keyword (which, I *think*, will translate to __restrict__ in g++).
 * This means that we're only supposed to access that array using that
 * one pointer.  But, already, the GAL documentation itself tells you to
 * do things that violate this.  In particular, look at the Tesselation
 * library.  To create a tile, you set the array pointer of the
 * gal_data_t to the element of the array where the first element of the
 * tile lives.  Is this not another array pointer pointing to the same
 * data that the array pointer of the parent block points at?  And, yet,
 * the restict keyword tells me that you're not supposed to do that.
 * So, either, I don't fully understand "restrict", or the GAL
 * developers are lucky that they haven't yet run into a compiler
 * optimization that breaks their tests.
 *
 * I'm going to go ahead and have other pointers point to the array
 * member of the gal_data_t structure and stick my head in the sand and
 * hope that it's OK.  To be safe, we should never directly use the
 * "array" member of that structure directly, to avoid any compiler
 * optimizations that might break things.  At least, I hope that makes
 * it safe.
 *
 * Restrict should only be on things like function arguments or local
 * variables, not on the definition of a data type that you expect lots
 * of people to use.  I really think the GAL has done it wrong by
 * defining gal_data_t this way.
 *
 */
class FITSImageWrapper :
  public FITSHDU
{
  friend class FITSFile;
  template<class T> friend class FITSImage;
  friend class FITSHDU;

  // I don't completely understand C++ protected.  I need to access
  //   _axlen in FITSTile::pix, but I need to access the _axlen of
  //   the FITSTile's "parentimage" member.  It seems by the C++ rules,
  //   I can't actually do that.  (To get to the other protected stuff,
  //   I had to go through this, but this->parentimage->_axlen didn't work.
  // So, I friend FITSTile here, even though I think I ought not to have to.
  // template<class T> friend class FITSTile;

protected:
  std::unique_ptr<gal_data_t, gal_data_t_deleter> galdata = nullptr;
  /**
   * This must always point at galdata->array
   */
  void* voidimagedata;
  /**
   * This is acopy of galdata->dsize
   */
  std::vector<int> _axlen;

  uint8_t datatype;
  size_t datatypesize;

  // Constructors should only be used by derived classes and by friends
  // The only right place ot make a FITSImageWrapper or FITSHeader is
  // inside FITSHDU or FITSFile
  FITSImageWrapper( FITSFile *parentfile, int hdunum, std::shared_ptr<spdlog::logger> logger );
  FITSImageWrapper( std::shared_ptr<spdlog::logger> logger );
  void init();

  /**
   * \brief should ONLY be called by FITSFile::hdu
   *
   * ...at least until I better organize the code.
   */
  void read_image_data();

  /**
   * Returns a FITSImage<T>*, where T is based on wanted type, cast to derived class FITSImageWrapper*.
   * Also adds it to the parent fitsfile's hdu vector, which owns the pointer, so don't free the return ponter.
   *
   */
  static FITSImageWrapper* make_right_FITSImage_type( const int wantedtype, FITSFile *parentfile,
                                                      int hdunum, std::shared_ptr<spdlog::logger> logger );
  std::shared_ptr<WCSWrapper> _wcs = nullptr;

public:
  FITSImageWrapper() = delete;
  FITSImageWrapper( FITSImageWrapper& ) = delete;
  FITSImageWrapper& operator=( FITSImageWrapper& ) = delete;

  WCSWrapper* wcs() { return _wcs.get(); }
  void set_wcs( std::shared_ptr<WCSWrapper> wcs );

  size_t axlen(size_t i) { return _axlen[i]; }
  virtual std::shared_ptr<char[]> copy_void_image_data();
  gal_data_t* get_galdata() { return galdata.get(); }

  uint8_t get_datatype() const { return datatype; }

  // This isn't legal
  // template<class U> virtual std::shared_ptr<FITSImage<U>> subtile( int x0, int x1, int y0, int y1 ) = 0;

  virtual ~FITSImageWrapper();
};

// **********************************************************************

/**
 * \brief An Image HDU
 *
 */
template<class T> class FITSImage :
  public FITSImageWrapper
{
  friend class FITSFile;
  friend class FITSImageWrapper;
  template<class U> friend class FITSImage;   // ...omg

private:
  void actually_read_image_data();

protected:
  FITSImage( FITSFile *parentfile, int hdunum, std::shared_ptr<spdlog::logger> logger );
  FITSImage( std::shared_ptr<spdlog::logger> logger );

  static const uint8_t image_datatype;
  T* imagedata;

public:
  FITSImage() = delete;
  FITSImage( FITSImage& ) = delete;
  FITSImage& operator=( FITSImage& ) = delete;

  /**
   * \brief Makes a clone of the FITS image that is detached from any file.
   *
   * Useful e.g. for cloning tiles if you want to dispose of the owning image.
   */
  template<class U> std::shared_ptr<FITSImage<U>> clone();

  virtual ~FITSImage();

  /**
   * \brief Create a new fitsimage that's a copy of a data array.
   *
   * @param nx Width of the image
   * @param ny Height of the image
   * @param data A pointer to memory of size nx*ny*sizeof(T)
   * @param logger
   *
   * Returns a FITSImage<T> that has a copy of the data array.  (Does
   * *not* just wrap @code{data}.)
   *
   */
  static std::shared_ptr<FITSImage<T>> create( size_t nx, size_t ny, const T* data=nullptr,
                                               std::shared_ptr<spdlog::logger> logger=nullptr );

  // inline int width() const { return _axlen[0]; }
  // inline int height() const { return _axlen[1]; }

  inline T pix( int x, int y ) const {
    // -DNDEBUG didn't seem to be getting rid of these...
    // assert( imagedata != nullptr );
    // assert( _axlen.size() == 2 );
    // assert( x < _axlen[0] );
    // assert( y < _axlen[1] );
    return imagedata[ y * _axlen[0] + x ];
  }
  inline T pix( const std::vector<size_t> &coords ) const {
    // -DNDEBUG didn't seem to be getting rid of these...
    // assert( imagedata != nullptr );
    // assert( _axlen.size() == coords.size() );
    // assert( coords.back() < _axlen.back() );
    int dex = coords.back();
    for ( int axis = coords.size() - 2 ; axis >= 0 ; --axis ) {
      assert( coords[ dex ] < _axlen[ dex ] );
      dex *= _axlen[ dex ];
      dex += coords[ dex ];
    }
    return imagedata[ dex ];
  }

  inline void setpix( int x, int y, T val ) {
    // -DNDEBUG didn't seem to be getting rid of these...
    // assert( imagedata != nullptr );
    // assert( _axlen.size() == 2 );
    // assert( x <= _axlen[0] );
    // assert( y <= _axlen[1] );
    imagedata[ y * _axlen[0] + x ] = val;
  }

  inline void setpix( const std::vector<size_t> &coords, T val ) {
    // -DNDEBUG didn't seem to be getting rid of these...
    // assert( imagedata != nullptr );
    // assert( _axlen.size() == coords.size() );
    // assert( coords.back() < _axlen.back() );
    int dex = coords.back();
    for ( int axis = coords.size() - 2 ; axis >=0 ; --axis ) {
      // assert( coords[ dex ] < _axlen[ dex ] );
      dex *= _axlen[ dex ];
      dex += coords[ dex ];
    }
    imagedata[ dex ] = val;
  }

  T* data() { return imagedata; }

  virtual std::shared_ptr<T[]> copy_image_data();

  /**
   * \brief Return a new FITSImage that has a copy of pixels [x0,x1) and [y0,y1) from this image.
   *
   */
  template<class U> std::shared_ptr<FITSImage<U>> subtile( int x0, int x1, int y0, int y1 );
};

#endif

