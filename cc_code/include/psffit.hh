#include <memory>
#include <string>
#include <vector>
#include <stumbler.hh>
#include <psfexreader.hh>
#include <gsl/gsl_vector_double.h>

int lmfuncwrapper( const gsl_vector *params, void *vself, gsl_vector *f );

class PSFFitter {
private:
  std::string &psffilename;
  const double *clip, *weight;
  int clipsize, clip0x, clip0y;
  bool fix_position = false;
  double fixx0, fixy0;

  std::unique_ptr<Stumbler<double>> stumbler;

  PSFExReader psfex;

  double lnL( const std::vector<double> &param, const MPI_Comm &comm ) const;

  int lmfunc( const gsl_vector *params, gsl_vector *f );

public:
  std::vector<std::string> paramnames{ "flux", "x", "y" };
  std::vector<double> param;
  std::vector<double> var;
  std::vector<std::vector<double>> covar;

  PSFFitter( std::string &psffilename, const double *clip, const double *weight,
             int clipsize, int clip0x, int clip0y );

  void run_stumbler_psf_fit( std::vector<double> &param, std::vector<std::vector<double>> &covar,
                             const std::vector<double> &initparam, const std::vector<double> &initsigma,
                             unsigned long int stumblerseed=0, std::string filebase="",
                             bool fix_position=false, int nwalkers=20, int burnsteps=200, int steps=200 );

  double stumbler_reduced_chisq() const;

  void run_lm_psf_fit( std::vector<double> &param, std::vector<std::vector<double>> &covar,
                       const std::vector<double> &initparam, bool fix_position=false );


  friend int lmfuncwrapper( const gsl_vector *params, void *vself, gsl_vector *f );
};

extern "C"
{
  void fit_psf_stumbler( const char *psffilename, const double *clip, const double *weight,
                         int clipsize, int clip0x, int clip0y,
                         double initflux, double initx, double inity, double sigflux, double sigx, double sigy,
                         int fixpos, int steps, int burnsteps, int nwalkers, double *param, double *covar,
                         unsigned long int stumblerseed=0, const char *filebase="" );

  void fit_psf_lm( const char *psffilename, const double *clip, const double *weight,
                   int clipsize, int clip0x, int clip0y,
                   double initflux, double initx, double inity,
                   int fixpos, double *param, double *covar );
}


