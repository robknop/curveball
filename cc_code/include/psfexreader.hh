#ifndef INCLUDE_PSFEXREADER_HH
#define INCLUDE_PSFEXREADER_HH 1

#include <string>
#include <functional>
#include <memory>
#include <array>
#include <spdlog/spdlog.h>

/**
 * \brief A class to use .psf files from astromatic's psfex
 */
class PSFExReader {
public:
  PSFExReader();
  PSFExReader( std::shared_ptr<spdlog::logger> logger );
  PSFExReader( const std::string &filepath );
  PSFExReader( const std::string &filepath, std::shared_ptr<spdlog::logger> logger );
  virtual ~PSFExReader();

  /**
   * \brief Reads the psfex .psf file; necessary before doing anything else.
   */
  void read_psf( const std::string &filepath );

  /**
   * \brief Return...something that PSFEx makes and that I don't fully understand.
   *
   * (x0, y0) : C coordinates of the position on the image where you want the PSF.
   */
  template<class T> std::shared_ptr<T[]> get_oversampled_psf( double x0, double y0 ) const;

  /**
   * \brief Return a stamp of the PSF at a given point on the image the PSF applies to.
   *
   * See fill_psf_stamp for definition of the inputs.
   *
   * Returns the stamp image.  It is stampsize*stampsize in size, in
   * standard FITS (i.e. y-major) order.
   */
  template<class T> std::shared_ptr<T[]> psf_stamp( double x0, double y0, int stampsize, bool normalize=true ) const;

  /**
   * \brief Return a stamp of the PSF at a given point on the image the PSF applies to.
   *
   * x0, y0 -- position on the original image (c coordinates?) of the PSF
   * offx, offy -- c-pixel of the lower left of where the stamp is on the image
   * stampsize -- size of square image to return
   */
  template<class T> std::shared_ptr<T[]> psf_stamp( double x0, double y0, int offx, int offy,
                                                    int stampsize, bool normalize=true ) const;

  /**
   * \brief Return a stamp of the PSF at a given point on the image the PSF applies to.
   *
   * See fill_psf_stamp for definition of the inputs.
   *
   * Returns the stamp image.  It is stampsize*stampsize in size, in
   * standard FITS (i.e. y-major) order.
   */
  template<class T,class P> std::shared_ptr<T[]> psf_stamp( double x0, double y0, int stampsize,
                                                            P* psfbuffer, bool normalize=true ) const;


  /**
   * \brief Replace a pre-allocated stamp image with the PSF at a given point on its associated full image
   *
   * stamp -- allocated stampsize*stampsize array
   * x0, y0 -- position of the psf on the overall image (c pixels)
   * offx, offy -- offset of the lower-left corner of the stamp on the full image
   * stampsize -- size of square stamp
   * normalize -- make sure the thing sums to one.  Think about this with offsetting!
   */
  template<class T> void fill_psf_stamp( T* stamp, double x0, double y0, int offx, int offy,
                                         int stampsize, bool normalize=true ) const;

  /**
   * \brief Replace a pre-allocated stamp image with the PSF at a given point on its associated full image
   *
   * stamp -- allocated stampsize*stampsize array
   * x0, y0 -- position of the psf on the overall image (c pixels)
   * offx, offy -- offset of the lower-left corner of the stamp on the full image
   * stampsize -- size of square stamp
   * psfbuffer -- something returned by get_oversampled_psf
   * normalize -- make sure the thing sums to one.  Think about this with offsetting!
   */
  template<class T,class P> void fill_psf_stamp( T* stamp, double x0, double y0, int offx, int offy,
                                                 int stampsize, P* psfbuffer, bool normalize=true ) const;

  /**
   * \brief Replace a pre-allocated stamp image with the PSF at a given point on its associated full image
   *
   * See the other fill_psf_stamp for definition of the inputs.
   */
  template<class T> void fill_psf_stamp( T* stamp, double x0, double y0, int stampsize, bool normalize=true ) const;

  /**
   * \brief Repalce a pre-allocated stamp image with the PSF at a given point on its associate full image
   *
   * stamp : the stamp image (must be allocated to stampsize*stampsize elements)
   * x0, y0 : the position (C coordinates) on the full image where the psf was evaluated
   * stampsize : size of square image.  Must be odd!
   * psfbuffer: something returned by get_oversampled_psf
   * normalize: ensure that the stamp image sums to 1.0
   */
  template<class T,class P> void fill_psf_stamp( T* stamp, double x0, double y0, int stampsize,
                                                 P* psfbuffer, bool normalize=true ) const;

  /**
   * \brief Add a PSF to an image.
   *
   * image: a pointer to the image data
   * imagewid, imagehei : x and y size of image
   * stampsize : size of the postage stamp
   * x0, y0 : position of psf (C coordinates)
   * flux : flux of the psf
   **/
  template<class T> void add_psf_to_image( T* image, int imagewid, int imagehei, int stampsize,
                                           double x0, double y0, double flux ) const;

  /**
   * \brief Add a PSF postage stamp
   *
   * image : a pointer to the image data
   * imagewid, imagehei : x and y size of image
   * stamp : pointer to stamp data (e.g. returned from get_psf_stamp)
   * stampsize : size of stamp image
   * xc, yc: position on image to add the stamp (center pixel of stamp will be added here)
   */
  template<class T> void add_stamp_to_image( T* image, int imagewid, int imagehei,
                                             T* stamp, int stampsizem, int xc, int yc ) const;


  int psfwid() const { return _psfwid; }
  int psfsamp() const { return _psfsamp; }

private:
  bool initialized = false;
  std::shared_ptr<spdlog::logger> logger = nullptr;

  std::shared_ptr<float[]> psfdata = nullptr;
  int _psfwid = 0;
  double polzero1, polzero2, polscal1, polscal2, _psfsamp, poldeg1;

  void make_logger();
};

#endif
