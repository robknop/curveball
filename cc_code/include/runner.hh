#ifndef INCLUDE_RUNNER_HH
#define INCLUDE_RUNNER_HH 1

#include <mpi.h>
#include <vector>
#include <set>
#include <queue>
#include <functional>
#include <memory>
#include <sstream>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

namespace runner {

  /**
   * \brief Subclass this to make data that's passed or returned from a worker or a collector
   *
   */
  class RunnerDatum {
  public:
    RunnerDatum() {}
    virtual ~RunnerDatum() {}

    /**
     * \brief The number of bytes needed to serialize the state of this object.
     *
     */
    virtual size_t buffer_size() const = 0;
    /**
     * \brief Fill the buffer with the bytes needed to serialize this object.
     *
     * The buffer will have already been allocated and will have the
     * size returned by buffer_size.
     *
     */
    virtual void serialize( void* buf ) const = 0;
    /**
     * \brief Set the object's state from the buffer.
     *
     * The buffer will have been produced by a previous call to the
     * object's serialize method.  n is the number of bytes that are in
     * the buffer.  (This allows for objects that don't have a static
     * buffer size, even though that's probably the most common use
     * case.)  There is no need to free the buffer (hence const).
     *
     */
    virtual void deserialize( const void* buf, size_t n ) = 0;
  };

  /**
   * \brief An example RunnerDatum that holds a single double.
   *
   */
  class RunnerDouble : public RunnerDatum {
  private:
    double _val;
  public:
    RunnerDouble() : RunnerDatum(), _val(0.) {}
    RunnerDouble( double val ) : RunnerDatum(), _val(val) {}
    RunnerDouble( char *buf, size_t n ) : RunnerDatum() { memcpy( &_val, buf, sizeof(double) ); }
    virtual ~RunnerDouble() {}

    void set( double val ) { val = _val; }
    double get() const { return _val; }
    size_t buffer_size() const { return sizeof(double); };
    void serialize( void* buf ) const { memcpy( buf, &_val, sizeof(double) ); }
    void deserialize( const void* buf, size_t n ) { memcpy( &_val, buf, sizeof(double) ); }
    // (Callously ignoring whether or not 8 is the size we get...)
  };

  /**
   * \brief A class to oversee running of multiple MPI threads, managed by a controller thread that collects results.
   *
   * Overview: there is a controller process, which launches in parallel
   * worker processes.  Each worker process takes parameters and returns
   * results.  The controller process launches workers and collects the
   * results.  It will work even single-threaded, and in that case will
   * just fall back to calling the workers serially.
   *
   * To use this:
   *
   * 1. The most annoying part, so that you can pass arbitrary parameters
   *    to each worker and so that arbitrary parmeters can be passed
   *    through MPI.  (Python's mpi4py handles this by using pickle before
   *    calling MPI communcation functions; this here is like a manual
   *    pickle.)  You need to subclass RunnerDatum twice; once for the
   *    parameters passed to a worker, and once for the results returned
   *    from the worker.  In the templates below, these two subclasses are
   *    called Param and Result, but they can be the same class if
   *    desired.  (See, for example, ../cctest/testrunner.cc, which uses
   *    RunnerDouble for both Param and Result.)  They must define the
   *    virtual methods in RunnerData.  See RunnerDouble for an example
   *    (or just use it if want you want to pass and/or receive is a
   *    single double.)
   *
   * 2. Write a worker function that a reference that takes a const
   *    reference to a Param and returns a unique pointer to a Result.
   *
   * 3. Write a collector function that takes a const reference to a
   *    Result and an integer.  The Result is what was returned by the
   *    worker process indexed by the integer.  It can do whatever it
   *    wants with the Result (even nothing), but this is generally where
   *    you would collect together the results performed by all the worker
   *    processes.
   *
   * 4. Make a vector of Param, one element in the vector for each worker
   *    process that you want to run.
   *
   * 5. Instantiate a Runner, passing it an MPI communicator (probably
   *    MPI_COMM_WORLD, but something else if you know what you're doing),
   *    the collector function, and the worker function.  (Pass it
   *    MPI_COMM_NULL if you want to have it run serially.)
   *
   * 6. Call the go method of your Runner instance, passing it the vector
   *    of Param you made.
   *
   * 7. Profit
   *
   */
  template<class Param, class Result> class Runner {
  public:
    /**
     * \brief Create a runner that will execute a worker and return the result to a collector.
     *
     * \param comm An MPI communicator, such as MPI_COMM_WORLD.  Pass
     * MPI_COMM_NULL to force it into serial mode (equivalent to running
     * without mpirun.)
     *
     * \param collector The collector function that will be called in
     * the controller thread (MPI rank 0) once for each result returned
     * by a worker.
     *
     * \param worker The worker function that will be called once in one
     * of the worker threads (MPI rank other than 0, unless the whole
     * thing is running serially) once for each parameter passed to the
     * Runner's go method.  Both Param and Result must be subclasses of
     * RunnerDatum.
     *
     * \param logger A spdlog logger object.  If not given (or if null),
     * it will try to get the "Runner" spdlog object, or make that
     * object if it's not found in spdlog's registry.
     *
     */
    Runner( const MPI_Comm &comm,
            std::function<void(const Result &result, int i)> collector,
            std::function<std::unique_ptr<Result>(const Param &param)> worker,
            std::shared_ptr<spdlog::logger> logger=nullptr );

    /**
     * \brief Execute the worker and collector once for each parameter in a vector.
     *
     * \param params The Param objects on which to run workers.  Param
     * must be a subclass of RunnerDatum.
     *
     * \param dieonerror Should the whole process thrown an exception if
     * the worker throwns an exception?  By default, just sets an
     * internal success state to false if the worker throws an
     * exception.  (NOT FULLY IMPLEMENTED -- right now if there's an
     * exception in the worker, it will just throw, and will be caught
     * somewhere you've set up to catch exceptions, but only in that one
     * rank, and the whole thing will become a gigantic mess.  It's
     * probably better not to catch exceptions from the workers right
     * now and just let the whole program die.)
     *
     *
     * \param In the controller thread (MPI rank 0), log at INFO level
     * the completion of workers at this interval.
     *
     * \return True if all worker threads completed, false if one or
     * more worker threads threw an exception.
     */
    bool go( const std::vector<Param> &params, bool dieonerror=false, int reportevery=10 );

  private:
    const MPI_Comm comm;
    int mpisize, mpirank;

    std::shared_ptr<spdlog::logger> logger;

    const int mpi_tag_hello = 1;
    const int mpi_tag_response = 2;
    const int mpi_tag_dothings = 3;
    const int mpi_tag_die = 4;

    std::vector<short int> success;
    std::function<void( const Result &result, int i)> collector;
    std::function<std::unique_ptr<Result>( const Param &param)> worker;

    void justdoit( const std::vector<Param> &params, bool dieonerror, int reportevery );
    void controllerthread( const std::vector<Param> &params, bool dieonerror, int reportevery );
    void workerthread();
  };


  // **********************************************************************
  // **********************************************************************
  // **********************************************************************
  // Stick the instantiation in the header so that it will work with any type

  template<class Param, class Result>
  Runner<Param,Result>::Runner(  const MPI_Comm &comm,
                                 std::function<void(const Result&,int)> collector,
                                 std::function<std::unique_ptr<Result>(const Param&)> worker,
                                 std::shared_ptr<spdlog::logger> logger )
    : comm(comm),
      collector(collector),
      worker(worker),
      logger(logger)
  {
    if ( comm == MPI_COMM_NULL ) {
      mpisize = 1;
      mpirank = 0;
    }
    else {
      MPI_Comm_rank( comm, &mpirank );
      MPI_Comm_size( comm, &mpisize );
    }

    if ( logger == nullptr ) {
      logger = spdlog::get( "Runner" );
      if ( logger == nullptr ) {
        logger = spdlog::stderr_color_mt( "Runner" );
        logger->set_level( spdlog::level::info );
      }
    }
  }

  // **********************************************************************(

  template<class Param, class Result>
  bool Runner<Param,Result>::go( const std::vector<Param> &params, bool dieonerror, int reportevery )
  {
    if ( mpirank == 0 ) {
      success.resize( params.size() );
      if ( mpisize == 1 ) justdoit( params, dieonerror, reportevery );
      else                controllerthread( params, dieonerror, reportevery );
    } else {
      workerthread();
    }
    // spdlog::debug( "Starting success broadcast..." );
    // if ( mpisize > 1 ) MPI_Bcast( success.data(), success.size(), MPI_SHORT, 0, comm );
    // spdlog::debug( "...done with success broadcast." );

    int didisucceed;
    if ( mpirank == 0 ) {
      for ( auto s=success.begin() ; s!=success.end() ; ++s )
        if (!(*s)) {
          didisucceed = 0;
          break;
        }
    }
    // spdlog::debug( "Starting success broadcast..." );
    MPI_Bcast( &didisucceed, 1, MPI_INT, 0, comm );
    // spdlog::debug( "...done with success broadcast." );
    return ( didisucceed > 0 ) ? true : false;
  }

  // **********************************************************************

  template<class Param,class Result>
  void Runner<Param,Result>::justdoit( const std::vector<Param> &params, bool dieonerror, int reportevery ) {
    std::stringstream str("");

    for ( int i = 0 ; i < success.size() ; ++i ) success[i] = 0;

    str.str("");
    str << "Starting jobs; have " << params.size() << " to do.";
    logger->info( str.str() );
    for ( int i = 0 ; i < params.size() ; ++i ) {
      if ( i % reportevery == 0 ) {
        str.str("");
        str << "Completed " << i << " of " << params.size() << " jobs.";
        logger->info( str.str() );
      }

      try {
        auto result = worker( params[i] );
        collector( *result, i );
        success[i] = 1;
      }
      catch( std::exception &ex ) {
        str.str("");
        str << "Exception on job " << i << ": " << ex.what();
        logger->error( str.str() );
        if ( dieonerror ) throw ex;
      }
    }
  }

// **********************************************************************

  template<class Param, class Result>
  void Runner<Param,Result>::controllerthread( const std::vector<Param> &params, bool dieonerror, int reportevery ) {
    std::stringstream str("");
    std::queue<int> avail;
    std::set<int> checkedin;
    std::vector<char> parambuf;
    std::vector<char> resultbuf;
    std::vector<bool> did( params.size(), false );
    Result res( 0. );
    int ncheckedin = 0;
    int sentdex = 0;
    int ndone = 0;

    MPI_Status status;

    for ( int i = 0 ; i < success.size() ; ++i ) success[i] = 0;
    checkedin.clear();

    logger->debug( "Waiting for all workers to check in" );
    while ( ncheckedin < mpisize-1 ) {
      int n;
      MPI_Recv( &n, 1, MPI_INT, MPI_ANY_SOURCE, mpi_tag_hello, comm, &status );
      if ( checkedin.find( status.MPI_SOURCE ) != checkedin.end() ) {
        str.str( "" );
        str << "Worker " << status.MPI_SOURCE << " checked in more than once!";
        throw std::runtime_error( str.str() );
      }
      checkedin.insert( status.MPI_SOURCE );
      ncheckedin += 1;
      avail.push( status.MPI_SOURCE );
    }

    str.str("");
    str << "All nodes checked in, starting " << params.size() << " jobs.";
    logger->debug( str.str() );

    while ( ndone < params.size() ) {
      while ( ( avail.size() > 0 ) && ( sentdex < params.size() ) ) {
        int rank = avail.front();
        avail.pop();
        int n = params[sentdex].buffer_size();
        if ( parambuf.size() < n ) parambuf.resize( n );
        params[sentdex].serialize( parambuf.data() );
        // str.str("");
        // str << "Sending job " << sentdex << " to rank " << rank;
        // logger->debug( str.str() );
        MPI_Send( &n, 1, MPI_INT, rank, mpi_tag_dothings, comm );
        MPI_Send( parambuf.data(), n, MPI_BYTE, rank, sentdex, comm );
        sentdex += 1;
      }

      int n;
      MPI_Recv( &n, 1, MPI_INT, MPI_ANY_SOURCE, mpi_tag_response, comm, &status );
      if ( resultbuf.size() < n ) resultbuf.resize( n );
      MPI_Recv( resultbuf.data(), n, MPI_BYTE, status.MPI_SOURCE, MPI_ANY_TAG, comm, &status );
      if ( did[status.MPI_TAG] ) {
        str.str("");
        str << "Got a result for index " << status.MPI_TAG << " more than once!";
        throw std::runtime_error( str.str() );
      }
      // str.str("");
      // str << "Got result for job " << status.MPI_TAG << " from rank " << status.MPI_SOURCE;
      // logger->debug( str.str() );
      res.deserialize( resultbuf.data(), n );
      avail.push( status.MPI_SOURCE );
      collector( res, status.MPI_TAG );
      did[status.MPI_TAG] = true;
      success[status.MPI_TAG] = 1;                // ROB think about returning failure!
      ndone += 1;
      if ( ( ndone == params.size() ) || ( ndone % reportevery == 0 ) ) {
        str.str("");
        str << "Completed " << ndone << " of " << params.size() << " jobs.";
        logger->info( str.str() );
      }
    }

    if ( avail.size() != mpisize-1 ) {
      str.str("");
      str << "After all jobs done, avail.size()=" << avail.size()
          << ", mpisize-1 = " << mpisize-1;
      throw std::runtime_error( str.str() );
    }

    while ( avail.size() > 0 ) {
      int rank = avail.front();
      avail.pop();
      str.str("");
      str << "Telling rank " << rank << " to die.";
      logger->debug( str.str() );
      int n = 0;
      MPI_Send( &n, 1, MPI_INT, rank, mpi_tag_die, comm );
    }

    // Should check that all did are true, though success will also be false in that case

    logger->debug( "Controller finished." );
  }

  // **********************************************************************

  template<class Param, class Result>
  void Runner<Param,Result>::workerthread() {
    int n = 0;
    int dex;
    bool done = false;
    MPI_Status status;
    std::vector<char> parambuf, resbuf;
    Param param;

    // Check in with the controller
    MPI_Send( &n, 1, MPI_INT, 0, mpi_tag_hello, comm );

    // Wait to be told what to do
    while ( ! done ) {
      MPI_Recv( &n, 1, MPI_INT, 0, MPI_ANY_TAG, comm, &status );
      if ( status.MPI_TAG == mpi_tag_die ) done = true;
      else {
        // Do things!
        if ( parambuf.size() < n ) parambuf.resize(n);
        MPI_Recv( parambuf.data(), n, MPI_BYTE, 0, MPI_ANY_TAG, comm, &status );
        param.deserialize( parambuf.data(), n );
        dex = status.MPI_TAG;
        std::unique_ptr<Result> res = worker( param );
        n = res->buffer_size();
        if ( resbuf.size() < n ) resbuf.resize(n);
        res->serialize( resbuf.data() );
        MPI_Send( &n, 1, MPI_INT, 0, mpi_tag_response, comm );
        MPI_Send( resbuf.data(), n, MPI_BYTE, 0, dex, comm );
      }
    }

    std::stringstream str("");
    str << "Goodby from " << mpirank;
    spdlog::debug( str.str() );
  }


};  // end of namespace

#endif
