#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
import pathlib

from exposuresource import ExposureSource
import config
import util

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.DEBUG )

    config.Config.init( '/home/curveball/curveballconfig.yaml' )
    
    ztf = ExposureSource.get( "ZTF" )
    images = ztf.find_images(188.509, 26.15, starttime='2020-01-11T08:52', endtime='2020-01-11T08:53')

    # This is quite an ugly hack
    for ccd in range(1,17):
        for quadrant in range(1,5):
            images.at[0, 'ccdid'] = ccd
            images.at[0, 'qid'] = quadrant
            # logger.info( f'Gonna download ccdid={images.iloc[0]["ccdid"]}, quadrant={images.iloc[0]["qid"]}' )
            ztf.download_image( images, 0, filedir='/incoming', logger=logger )

# ======================================================================

if __name__ == "__main__":
    main()
