import sys
import os
import math

sys.path.insert( 0, "/ztfquery" )
sys.path.insert( 0, "/ztfdr" )
import ztfdr.release
DR1_PATH = "/ztfcosmodr/dr1"

import config
import db

config.Config.init( "/home/curveball/curveballconfig.yaml" )
curdb = db.DB.get()

# This is an ugly-quick-hack graft of other stuff I already wrote,
#  so it's flatulent

with open( "bts.lis" ) as ifp:
    sne = {}
    for line in ifp:
        if ( len(line) == 0 ) or ( line[0] == '#' ):
            continue
        arr = line.split()
        snname = arr[0]
        hostn = int(arr[1])
        hostra = float(arr[4])
        hostdec = float(arr[5])
        snra = float(arr[4])
        sndec = float(arr[5])
        snt0 = float(arr[6]) - 2400000.5
        snz = float(arr[7])
        hostz = float(arr[8])

        if not snname in sne:
            sne[snname] = {}
        sne[snname][hostn] = { 'ra': snra, 'dec': sndec, 't0': snt0, 'snz': snz, 'hostz': hostz }

    ztfdr1 = ztfdr.release.ZTFDataRelease.from_directory( DR1_PATH )

    for sn, snhosts in sne.items():
        # print( f"Checking {sn}" )
        if sn in ztfdr1.lcdata.index.levels[0]:
            print( sn )
