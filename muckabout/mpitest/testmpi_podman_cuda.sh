#!/usr/bin/env bash
#SBATCH -J testmpi
#SBATCH -t 00:05:00
#SBATCH --ntasks=4
#SBATCH --ntasks-per-node=4
#SBATCH -o testmpi_podman_cuda.out
#SBATCH -e testmpi_podman_cuda.out
#SBATCH -A m2218_g
#SBATCH --qos=debug
#SBATCH --exclusive
#SBATCH --constraint=gpu
#SBATCH --gpus-per-node=4

dockerimage=docker.io/rknop/curveball:mpich-cuda
#sourcedir=/global/homes/r/raknop/curveball/muckabout/mpitest
sourcedir=$PWD

echo "Hello world from " `hostname`
echo "SLURM_JOB_NODELIST = " $SLURM_JOB_NODELIST
echo "SLURM_CLUSTER_NAME = " $SLURM_CLUSTER_NAME
echo "SLURM_JOB_CPUS_PER_NODE = " $SLURM_JOB_CPUS_PER_NODE
echo "SLURM_JOB_NAME = " $SLURM_JOB_NAME
echo "SLURM_JOB_QOS = " $SLURM_JOB_QOS
echo "SLURM_JOB_ACCOUNT = " $SLURM_JOB_ACCOUNT
echo "SLURM_JOB_ID = " $SLURM_JOB_ID

pwd

# date
# echo "Testing straight-up srun python <args>: "
# module load python/3.9-anaconda-2021.11
# srun python -m mpi4py cudainfo.py

date
echo "Testing podman-hpc run --gpu <mount-opts> <image> mpirun -n 4 python <args>"
podman-hpc run --gpu \
    --mount type=bind,source=$sourcedir,target=/testmpi \
    $dockerimage \
    mpirun -n 5 python -m mpi4py /testmpi/cudainfo.py

# date
# echo "Testing srun podman-hpc run --cuda-mpi <opts> <image> python <args>"
# srun podman-hpc run --cuda-mpi --gpu \
#     --mount type=bind,source=$sourcedir,target=/testmpi \
#     $dockerimage \
#     python -m mpi4py /testmpi/cudainfo.py

# date
# echo "Testing srun podman-hpc shared-run --cuda-mpi <opts> <image> python <args>"
# srun podman-hpc shared-run --cuda-mpi --gpu \
#     --mount type=bind,source=$sourcedir,target=/testmpi \
#     $dockerimage \
#     python -m mpi4py /testmpi/cudainfo.py


date

echo "Goodbye, world."
