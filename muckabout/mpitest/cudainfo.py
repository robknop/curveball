import sys
import io
import socket
import pycuda
import pycuda.driver
import argparse
import logging
from mpi4py import MPI


def main():
    parser = argparse.ArgumentParser( "CUDA test" )
    parser.add_argument( "-a", "--attributes", action='store_true', help="Show attributes" )
    args = parser.parse_args()

    rank = MPI.COMM_WORLD.Get_rank()
    size = MPI.COMM_WORLD.Get_size()

    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    formatter = logging.Formatter( f'[%(asctime)s - {rank:02d} - %(levelname)s] - %(message)s',
                                   datefmt='%Y-%m-%d %H:%M:%S' )
    logout.setFormatter( formatter )
    logger.setLevel( logging.INFO )

    pycuda.driver.init()

    strio = io.StringIO()

    strio.write( f"Rank {rank} of {size} on {socket.gethostname()}\n" )
    
    strio.write( f"pycuda cuda version: {pycuda.driver.get_version()}, " )
    drver = pycuda.driver.get_driver_version()
    drvermajor = drver // 1000
    drverminor = drver - 1000 * drvermajor
    strio.write( f"driver cuda version: {drvermajor}.{drverminor}\n" )

    ndevices = pycuda.driver.Device.count()
    strio.write( f"There are {ndevices} devices.\n" )
    device = pycuda.driver.Device( rank % ndevices )

    strio.write( f"PyCuda device : {device.name()}\n" )
    strio.write( f"   Bus ID : {device.pci_bus_id()}\n" )
    strio.write( f"   compute capability: {device.compute_capability()}\n" )
    strio.write( f"   total memory: {device.total_memory()/1024./1024./1024.:.2f} GiB\n" )
    if args.attributes:
        attrdict = device.get_attributes()
        strio.write( f"   Attributes: \n" )
        for attr, val in attrdict.items():
            strio.write( f"      {attr} = {val}\n" )

    logger.info( strio.getvalue() )
    

# ======================================================================

if __name__ == "__main__":
    main()
