#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import math
import astropy.units as units
from astropy.io import fits
from astropy.wcs import WCS
from astropy.coordinates import SkyCoord

def main():
    base = 'ztf_20200111369965_001669_zr_io'
    print( "{" )
    for ccd in range(1,17):
        for quadrant in range(1,5):
            tag = f'{ccd:02d}{quadrant:1d}'
            with fits.open( f'/incoming/{base}.{tag}.fits' ) as ifp:
                wcs = WCS(ifp[0].header)
                telrad = ifp[0].header['TELRAD']
                teldecd = ifp[0].header['TELDECD']
                nx = ifp[0].header['NAXIS1']
                ny = ifp[0].header['NAXIS2']
            ctr = wcs.pixel_to_world( math.floor(nx/2), math.floor(ny/2) )
            cosdec = math.cos( (teldecd + ctr.dec.to(units.deg).value)/2. * math.pi / 180. )
            ll = wcs.pixel_to_world( 0, 0 )
            ul = wcs.pixel_to_world( 0, ny-1 )
            ur = wcs.pixel_to_world( nx-1, ny-1 )
            lr = wcs.pixel_to_world( nx-1, 0 )

            print( f"  '{tag}' : {{ " )
            ra = ctr.ra.to(units.deg).value
            dec = ctr.dec.to(units.deg).value
            dra = ( ra - telrad ) * cosdec
            ddec = dec - teldecd
            print( f"              'raoff': {dra}," )
            print( f"              'decoff': {ddec}," )
            for i, coord in enumerate( [ll, ul, ur, lr] ):
                ra = coord.ra.to(units.deg).value
                dec = coord.dec.to(units.deg).value
                dra = ( ra - telrad ) * cosdec
                ddec = dec - teldecd
                print( f"              'ra{i}off': {dra}," )
                print( f"              'dec{i}off': {ddec}," )
            print( "          }," )
    print( "}" )
            
            

# ======================================================================

if __name__ == "__main__":
    main()
