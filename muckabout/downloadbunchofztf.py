#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import logging
import pathlib

from exposuresource import ExposureSource
import config

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.DEBUG )

    config.Config.init( '/home/curveball/curveballconfig.yaml' )
    
    ztf = ExposureSource.get( "ZTF" )
    images = ztf.find_images( 185.5432, 26.9946, logger=logger )

    outdir = pathlib.Path( "/incoming" )
    for i in range(len(images)):
        logger.info( f'Downloading {i+1} of {len(images)}...' )
        ztf.download_image( images, i, filedir=outdir, logger=logger )

# ======================================================================

if __name__ == "__main__":
    main()
