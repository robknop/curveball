#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import math
import matplotlib
from matplotlib import pyplot

import config
from exposuresource import ExposureSource

def main():
    config.Config.init( '/home/curveball/curveballconfig.yaml' )
    ztf = ExposureSource.get( 'ZTF' )

    fig = pyplot.figure( figsize=(9, 9), tight_layout=True )
    ax = fig.add_subplot( 1, 1, 1 )
    ax.invert_xaxis()
    
    wid = None
    arrowlen = None
    for chip in ztf.chips:
        ras = [ chip.ra0off, chip.ra1off, chip.ra2off, chip.ra3off, chip.ra0off ]
        decs = [ chip.dec0off, chip.dec1off, chip.dec2off, chip.dec3off, chip.dec0off ]
        if arrowlen is None:
            wid = max(decs) - min(decs)
            arrowlen = 0.25 * ( max(decs) - min(decs) )
        ax.plot( ras, decs, color='b' )
        ax.text( max(ras) - wid*0.1, max(decs) - wid*0.1, chip.chiptag, ha="left", va="top" )

        rot90 = math.fabs( chip.ra1off - chip.ra0off ) > math.fabs( chip.dec1off - chip.dec0off )
        raplus = (chip.ra1off > chip.ra0off) if rot90 else (chip.ra2off > chip.ra1off)
        decplus = (chip.dec2off > chip.dec1off) if rot90 else (chip.dec1off > chip.dec0off)
        rasgn = 1 if raplus else -1
        decsgn = 1 if decplus else -1

        ax.arrow( chip.raoff, chip.decoff, arrowlen*rasgn, 0, width=0.01*wid,
                  head_width=0.1*wid, head_length=0.1*wid, length_includes_head=True )
        ax.arrow( chip.raoff, chip.decoff, 0, arrowlen*decsgn, width=0.01*wid,
                  head_width=0.1*wid, head_length=0.1*wid, length_includes_head=True )
        if rot90:
            ax.text( chip.raoff + rasgn*arrowlen, chip.decoff, '+x',
                     ha='center', va='top' if rasgn>0 else 'bottom' )
            ax.text( chip.raoff, chip.decoff + decsgn*arrowlen, '+y',
                     va='center', ha='right' if decsgn<0 else 'left' )
        else:
            ax.text( chip.raoff + rasgn*arrowlen, chip.decoff, '+x',
                     va='center', ha='right' if rasgn>0 else 'left' )
            ax.text( chip.raoff, chip.decoff + decsgn*arrowlen, '+y',
                     ha='center', va='top' if decsgn<0 else 'bottom' )
            
    fig.show()
    fig.savefig( "ZTFfocalplane.svg" )

# ======================================================================

if __name__ == "__main__":
    main()
