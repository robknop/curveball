#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import logging

import numpy
import matplotlib
from matplotlib import pyplot

sys.path.insert( 0, "/ztfquery" )
sys.path.insert( 0, "/ztfdr" )
import ztfdr.release
DR1_PATH = "/ztfcosmodr/dr1"

import db
import config

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.INFO )

    config.Config.init( "/home/curveball/curveballconfig.yaml", logger=logger )    
    
    ztfdr1 = ztfdr.release.ZTFDataRelease.from_directory( DR1_PATH )

    obj = 'ZTF18aaxdrjn'
    dr1band = 'p48g'
    band = 'ZTF_g'

    ztfdata = ztfdr1.lcdata.loc[obj]
    ztfdata = ztfdata[ ztfdata['band'] == dr1band ]

    objobj = db.Object.get_by_name( obj )
    ltcv = db.ApPhot.get_for_obj_and_band( objobj, band )

    fig = pyplot.figure( figsize=(8,6), tight_layout=True )
    ax = fig.add_subplot( 1, 1, 1 )
    ax.set_title( f'{obj} {band}' )
    ax.set_xlabel( "t-t_0" )
    ax.set_ylabel( "flux-ish (10¯⁶ Jy)" )

    t = [ l.mjd - objobj.t0 for l in ltcv ]
    flux = [ l.flux * 1e6 for l in ltcv ]
    dflux = [ l.dflux for l in ltcv ]

    ax.errorbar( t, flux, yerr=dflux, marker='o', linestyle="", color="red" )

    ztft = ztfdata['jd'].values - 2400000.5 - objobj.t0
    ztfflux = ztfdata['flux'].values * 10**( (ztfdata['zp']-8.9) / -2.5 ) * 1e6
    ztfdflux = ztfdata['flux_err'].values * 10**( (ztfdata['zp']-8.9) / -2.5 )

    ax.errorbar( ztft, ztfflux, yerr=ztfdflux, marker='o', linestyle="", color="blue",
                 markerfacecolor='none', markeredgewidth=1 )

    fig.savefig( "it.svg" )
    

# ======================================================================

if __name__ == "__main__":
    main()
