#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import math

import numpy
from scipy.integrate import dblquad
from astropy.io import fits

import psfexreader
import processing

nx = 3080
ny = 3072

wid = 21

x0 = nx/2.
sigx0 = 2.
sigxx = 0.5 / nx
sigxy = 0.

y0 = ny/2.
sigy0 = 3.
sigyx = -0.5 / ny
sigyy = 0.

theta0 = 0.
thetax = 0.
thetay = math.pi / 4. / nx

noiselevel = 0.00001

def psf( yr, xr, sigx, sigy, theta ):
    xrot =  xr * math.cos(theta) + yr * math.sin(theta)
    yrot = -xr * math.sin(theta) + yr * math.cos(theta)
    return 1/(2*math.pi*sigx*sigy) * math.exp( -( xrot**2/(2.*sigx**2) + yrot**2/(2.*sigy**2) ) )

def psfpixel( x, y, xi, yi ):
    sigx = sigx0 + (x-x0) * sigxx + (y-y0) * sigxy
    sigy = sigy0 + (x-x0) * sigyx + (y-y0) * sigyy
    theta = theta0 + (x-x0) * thetax + (y-y0) * thetay
    
    res = dblquad( psf, xi-x-0.5, xi-x+0.5, yi-y-0.5, yi-y+0.5, args=( sigx, sigy, theta ) )
    return res[0]

def main():
    nx = 3072
    ny = 3080
    img = numpy.zeros( (ny, nx) )
    spacing = 302.6
    x0 = ( nx - ( spacing * ( nx // spacing ) ) ) / 2.
    y0 = ( ny - ( spacing * ( ny // spacing ) ) ) / 2.
    xpos = numpy.arange( x0, nx, spacing )
    ypos = numpy.arange( y0, ny, spacing )

    for xc in xpos:
        xi0 = int( math.floor(xc)+0.5 )
        sys.stderr.write( f"x = {xc}\n" )
        for yc in ypos:
            yi0 = int( math.floor(yc)+0.5 )
            for xi in range(xi0 - (wid//2), xi0 + wid//2):
                for yi in range(yi0 - (wid//2), yi0 + wid//2):
                    img[yi, xi] = psfpixel( xc, yc, xi, yi )

    img += numpy.random.normal( 0., noiselevel, img.shape )
                    
    hdu = fits.PrimaryHDU( data=img )
    hdu.writeto( "junk.fits", overwrite=True )
    hdu = fits.PrimaryHDU( data=numpy.zeros_like( img, dtype=numpy.uint8 ) )
    hdu.writeto( "junk.mask.fits", overwrite=True )
    img[:, :] = 1./(noiselevel**2)
    hdu = fits.PrimaryHDU( data=img )
    hdu.writeto( "junk.weight.fits", overwrite=True )

    processing.cat_psf_sky( None, "junk.fits", "junk.weight.fits", "junk.mask.fits", nodb=True, skipsky=True )

    psf = psfexreader.PSFExReader( "junk.psf" )
    palette = psf.get_psf_palette( spacing=spacing, x0=x0, y0=y0, xsize=nx, ysize=ny )
    hdu = fits.PrimaryHDU( data=palette )
    hdu.writeto( "psfpalette.fits", overwrite=True )
    
# ======================================================================

if __name__ == "__main__":
    main()
