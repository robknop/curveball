#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os

import db
import config
from exposuresource import ExposureSource

def main():
    config.Config.init( "/home/curveball/curveballconfig.yaml" )    
    ztf = ExposureSource.get("ZTF")

    nones = []
    with db.DB.get() as curdb:
        allimgs = curdb.db.query(db.Image.basename).all()
    for thing in allimgs:
        basename = thing[0]
        lp = ztf.local_path( f'{basename}.fits' )
        if lp is None:
            nones.append( f'{basename}.fits' )
    nl = "\n"
    print( f'{len(nones)} nones:{nl}{nl.join(nones)}' )
            

# ======================================================================

if __name__ == "__main__":
    main()
