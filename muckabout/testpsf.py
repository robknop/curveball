raise RuntimeError( "Run bin/psfexreader.py instead" )

#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import math
import logging

import numpy

from astropy.io import fits

def main():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    logout.setFormatter( logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s' ) )
    logger.setLevel( logging.DEBUG )

    imagefile = "junk.fits"
    psffile = "junk.psf"

    nx = 10
    ny = 10
    
    with fits.open( psffile, memmap=False ) as psf:
        if ( psf[1].header['POLNAXIS'] != 2 ):
            raise ValueError( f'Unexpected value of POLNAXIS = {psf[1].header["POLNAXIS"]}' )
        if ( psf[1].header['POLNGRP'] != 1 ):
            raise ValueError( f'Unexpected value of POLNGRP = {psf[1].header["POLNGRP"]}' )
        if ( psf[1].header['POLNAME1'] != "X_IMAGE" ) or ( psf[1].header['POLNAME2'] != "Y_IMAGE" ):
            raise ValueError( f'Unexpected values of POLNAME1 ("{psf[1].header["POLNAME1"]}") '
                              f'and/or POLNAME2 ("{psf[1].header["POLNAME2"]}")' )
        # The -1 is to deal with FITS 1-offset BS
        x0 = float( psf[1].header['POLZERO2'] ) - 1
        xsc = float( psf[1].header['POLSCAL2'] )
        y0 = float( psf[1].header['POLZERO1'] ) - 1
        ysc = float( psf[1].header['POLSCAL1'] )

        psfsamp = float( psf[1].header['PSF_SAMP'] )

        psforder = int( psf[1].header['POLDEG1'] )
        nparam = ( psforder - numpy.arange( 0, psforder+1, dtype=int ) + 1 ).sum()
        if psf[1].data[0][0].shape[0] != nparam:
            raise ValueError( f'Unexpected shape {psf[1].data[0][0].shape} of psf basis for order={psforder}; '
                              f'expected ({nparam}, :, :)' )

        psfdata = psf[1].data[0][0]

    psfwid = psfdata.shape[1]
    if ( psfwid % 2 ) == 0:
        raise ValueError( f'Even psf width {psfwid} surprises me.' )
    if psfdata.shape[2] != psfwid:
        raise ValueError( f'Non-square psf ( {psfwid} × {psfdata.shape[2]} ) surprises me.' )
    psfoff = int( math.floor( psfwid/2 ) + 0.5 )

    psfdex1d = numpy.arange( -(psfwid//2), psfwid//2+1, dtype=int )
    psfdex = numpy.meshgrid( psfdex1d, psfdex1d )
    
    stampwid = int( math.floor( psfsamp * psfwid ) + 0.5 )
    if ( stampwid % 2 ) == 0:
        logger.warning( f'Stampwid came out even ({stampwid}), subtracting ' )
        stampwid -= 1

    with fits.open( imagefile, mammap=False ) as image:
        psfpalette = numpy.zeros_like( image[0].data, dtype=float )
        xsize = int( image[0].header["NAXIS2"] )
        ysize = int( image[0].header["NAXIS1"] )
    xvals = numpy.arange( 0+stampwid, xsize-stampwid, (xsize-2*stampwid)/nx )
    yvals = numpy.arange( 0+stampwid, ysize-stampwid, (ysize-2*stampwid)/ny )

    logger.info( f'PSF x positions: {xvals}' )
    logger.info( f'PSF y positions: {yvals}' )
    
    for x in xvals:
        for y in yvals:
            xc = int( math.floor(x + 0.5) )
            yc = int( math.floor(y + 0.5) )

            psfbase = numpy.zeros_like( psfdata[0,:,:] )
            off = 0
            # THINK ABOUT THE ORDER
            #  I think psfex does it in the order 1, x, x², y, yx, y²
            #  But x and y are backwards in numpy compared to FITS,
            #    so the outer loop is x here
            #  Did I do this right?
            for i in range( psforder+1 ):
                for j in range( psforder+1-i ) :
                    psfbase += psfdata[off] * ( (x - x0) / xsc )**i * ( (y - y0) / ysc )**j
                    off += 1
            
            # Be clever and find a way to avoid for loop here?  (Uf-da)
            for xi in numpy.arange( xc - stampwid//2, xc + stampwid//2, dtype=int ):
                for yi in numpy.arange( yc - stampwid//2 , yc + stampwid//2, dtype=int ):
                    xsincarg = psfdex1d - (xi-x) / psfsamp
                    xsincvals = numpy.sinc( xsincarg ) * numpy.sinc( xsincarg/4. )
                    xsincvals[ ( xsincarg > 4 ) | ( xsincarg < -4 ) ] = 0
                    ysincarg = psfdex1d - (yi-y) / psfsamp
                    ysincvals = numpy.sinc( ysincarg ) * numpy.sinc( ysincarg/4. )
                    ysincvals[ ( ysincarg > 4 ) | ( ysincarg < -4 ) ] = 0
                    # import pdb; pdb.set_trace()
                    psfpalette[ xi, yi ] = ( xsincvals[:, numpy.newaxis] * ysincvals[numpy.newaxis, :] * psfbase ).sum()

    psfpalettehdu = fits.PrimaryHDU( data=psfpalette )
    psfpalettehdu.writeto( "psfpalette.fits" )
    
# ======================================================================

if __name__ == "__main__":
    main()
