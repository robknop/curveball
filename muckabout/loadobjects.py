import sys
import os
import math

import config
import db

config.Config.init( "/home/curveball/curveballconfig.yaml" )
curdb = db.DB.get()

with open( "bts.lis" ) as ifp:
    sne = {}
    for line in ifp:
        if ( len(line) == 0 ) or ( line[0] == '#' ):
            continue
        arr = line.split()
        snname = arr[0]
        hostn = int(arr[1])
        hostra = float(arr[4])
        hostdec = float(arr[5])
        snra = float(arr[4])
        sndec = float(arr[5])
        snt0 = float(arr[6]) - 2400000.5
        snz = float(arr[7])
        hostz = float(arr[8])

        print( f"{snname:>16s} RA={snra:.5f} Dec={sndec:.5f} t0={snt0:.2f} sn_z={snz:.3f} hostz={hostz:.4f}" )
        if not snname in sne:
            sne[snname] = {}
        sne[snname][hostn] = { 'ra': snra, 'dec': sndec, 't0': snt0, 'snz': snz, 'hostz': hostz }

    for sn, snhosts in sne.items():
        obj = db.Object.get_by_name( sn, curdb=curdb )
        if obj is not None:
            sys.stderr.write( f"WARNING: {sn} already in the database, not loading it." )
        else:
            for hostn, data in snhosts.items():
                if math.fabs( data['snz'] - data['hostz'] ) > 0.02:
                    sys.stderr.write( f"WARNING: host {hostn} for {sn} has z={data['hostz']:.3f}, compared to "
                                      f"sn_z={data['snz']:.3f}.  Not loading" )
                else:
                    sys.stderr.write( f"Loading {sn} using z of host {hostn}\n" )
                    obj = db.Object( name=sn, z=data['hostz'], ra=data['ra'], dec=data['dec'], t0=data['t0'] )
                    curdb.db.add( obj )
                    curdb.db.commit()
                    break
    
