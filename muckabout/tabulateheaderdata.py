#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import os
import pathlib

from astropy.io import fits

def main():
    filenames = []
    jds = []
    zps = []
    seeings = []
    filters = []
    exptimes = []

    for filename in sys.argv[1:]:
        with fits.open( filename ) as ifp:
            hdr = ifp[0].header
            filenames.append( filename )
            jds.append( hdr['OBSJD'] )
            zps.append( hdr['MAGZP'] )
            seeings.append( hdr['SEEING'] )
            filters.append( hdr['FILTER'] )
            exptimes.append( hdr['EXPOSURE'] )

    print( f'#{"filename":41s} {"jd":10s} {"zp":4s} {"seeing":6s} {"filter":6s} exptime' )
    for filename, jd, zp, seeing, filter, exptime in zip(filenames, jds, zps, seeings, filters, exptimes):
        print( f'{os.path.basename(filename):42s} {jd:10.2f} {zp:4.2f} {seeing:6.2f} {filter:6s} {exptime:4.1f}' )
    

# ======================================================================

if __name__ == "__main__":
    main()
        
