#!/bin/sh

command=""
environment=$(env)
while IFS='=' read -r name val; do
    if [[ $name == OMPI_* || $name == PMIX_* || $name == SLURM_JOB_ID || $name == SLURM_ARRAY_* ]]; then
        fixval=`echo $val | perl -pe 's/"/\\\\"/g'`
        command+="SINGULARITYENV_$name=\"$fixval\" "
    fi
done <<< "$environment"

# echo "OMPI env vars: " ${command[@]}
command+="singularity exec --cleanenv $@"
# echo "Running: " ${command[@]}
eval ${command[@]}
