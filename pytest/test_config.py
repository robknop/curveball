import sys
import os
import pathlib
import pytest

_rundir = pathlib.Path(__file__).parent
if not str(_rundir.parent) in sys.path:
    sys.path.insert(0, str(_rundir.parent / "bin" ) )
import config

# A note about pytest: Things aren't completely sandboxed.  When I call
# config.Config.get(), it selts Config._default, and that carries over
# from one test to the next even if the call wasn't in a fixture with
# class scope.  (The tests below are ordered with this in mind.)

class TestConfig:

    # Need this so it doesn't interfere with other tests
    # that use the cfg fixture from confetest.py
    @pytest.fixture(scope='class', autouse=True)
    def save_config_state( self ):
        self.defaultdefault = config.Config._default_default
        self.default = config.Config._default
        self.configs = config.Config._configs
        config.Config._default = None
        config.Config._configs = {}

        yield True

        config.Config._default_default = self.defaultdefault
        config.Config._default = self.default
        config.Config._configs = self.configs


    def test_no_default( self ):
        assert config.Config._default == None

    @pytest.mark.skipif( os.getenv("CURVEBALL_CONFIG") is None, reason="Set CURVEBALL_CONFIG to test this" )
    def test_default_default( self ):
        cfgobj = config.Config.get()
        assert cfgobj._path == pathlib.Path( f'/curveball/{os.getenv("CURVEBALL_CONFIG")}.yaml' )

    def test_set_default( self ):
        cfgobj = config.Config.get( _rundir / 'test.yaml', setdefault=True )
        assert config.Config._default == f'{ (_rundir / "test.yaml").resolve() }'

    @pytest.fixture(scope='class')
    def cfgobj( self ):
        return config.Config.get( _rundir / 'test.yaml', setdefault=True )

    def test_preload(self, cfgobj):
        assert cfgobj.value('preload1dict1.preload1_1val1') == '2_1val1'
        assert cfgobj.value('preload1dict1.preload1_1val2') == '1_1val2'
        assert cfgobj.value('preload1dict1.preload2_1val2') == 'main1val2'
        assert cfgobj.value('preload1dict1.preload2_1val3') == '2_1val3'
        assert cfgobj.value('preload1dict1.main1val3') == 'main1val3'
        assert cfgobj.value('preload1dict1.augment1val1') == 'a1_1val1'

        assert cfgobj.value('preload1dict2') == '2scalar1'

        assert cfgobj.value('preload1list1') == [ 'main1' ]
        assert cfgobj.value('preload1list2') == [ '1_2val0', '1_2val1', 'a1_2val0' ]

        assert cfgobj.value('preload1scalar1') == '2scalar1'
        assert cfgobj.value('preload1scalar2') == '1scalar2'
        assert cfgobj.value('preload1scalar3') == 'main3'
        assert cfgobj.value('preload2scalar2') == '2scalar2'

    def test_main(self, cfgobj):
        assert cfgobj.value('maindict.mainval1') == 'val1'
        assert cfgobj.value('maindict.mainval2') == 'val2'

        assert cfgobj.value('mainlist1') == ['main1', 'main2', 'main3', 'aug1', 'aug2']
        assert cfgobj.value('mainlist2') == ['override1', 'override2']

        assert cfgobj.value('mainscalar1') == 'aug1'
        assert cfgobj.value('mainscalar2') == 'override2'
        assert cfgobj.value('mainscalar3') == 'override2'

        assert cfgobj.value('mainnull') is None
        with pytest.raises( ValueError ):
            print( f"config value notdefined is {cfgobj.value('notdefined')}" )

    def test_override(self, cfgobj):
        assert cfgobj.value( 'override1list1.0' ) == '1_1override1'
        assert cfgobj.value( 'override1list1.1' ) == '1_1override2'
        assert cfgobj.value( 'override1list2' ) == [ '2_2override1', '2_2override2' ]

    def test_fieldsep( self, cfgobj ):
        fields, isleaf, curfield, ifield = cfgobj._fieldsep( 'nest.nest1.0.nest1a' )
        assert isleaf == False
        assert curfield == 'nest'
        assert fields == ['nest', 'nest1', '0', 'nest1a' ]
        assert ifield is None
        fields, isleaf, curfield, ifield = cfgobj._fieldsep( '0.test' )
        assert isleaf == False
        assert ifield == 0
        fields, isleaf, curfield, ifield = cfgobj._fieldsep( 'mainlist2' )
        assert isleaf
        fields, isleaf, curfield, ifield = cfgobj._fieldsep( 'mainscalar1' )
        assert isleaf


    def test_nest(self, cfgobj):
        assert cfgobj.value( 'nest' ) ==  { 'nest1': [ { 'nest1a': { 'val': 'foo' } }, 42 ],
                                         'nest2': { 'val': 'bar' } }
        assert cfgobj.value( 'nest.nest1.0.nest1a.val' ) == 'foo'

    def test_set(self, cfgobj):
        with pytest.raises( TypeError ):
            cfgobj.set_value( 'settest.list.notanumber', 'kitten', appendlists=True )
        with pytest.raises( TypeError ):
            cfgobj.set_value( 'settest.0', 'puppy' )
        with pytest.raises( TypeError ):
            cfgobj.set_value( 'settest.0.subset', 'bunny' )
        with pytest.raises( TypeError ):
            cfgobj.set_value( 'settest.dict.0', 'iguana' )
        with pytest.raises( TypeError ):
            cfgobj.set_value( 'settest.dict.2.something', 'tarantula' )

        cfgobj.set_value( 'settest.list.0', 'mouse', appendlists=True )
        assert cfgobj.value('settest.list.2') == 'mouse'
        cfgobj.set_value( 'settest.list.5', 'mongoose' )
        assert cfgobj.value('settest.list') == [ 'mongoose' ]

        cfgobj.set_value( 'settest.dict.newkey', 'newval' )
        assert cfgobj.value( 'settest.dict' ) == { 'key1': 'val1',
                                                'key2': 'val2',
                                                'newkey': 'newval' }
        assert cfgobj.value( 'settest.dict.newkey' ) == 'newval'

        cfgobj.set_value( 'settest.dict2', 'scalar' )
        assert cfgobj.value('settest.dict2') == 'scalar'

        cfgobj.set_value( 'settest.scalar', 'notathing' )
        assert cfgobj.value('settest.scalar') == 'notathing'

        cfgobj.set_value( 'settest.scalar.thing1', 'thing1' )
        cfgobj.set_value( 'settest.scalar.thing2', 'thing2' )
        assert cfgobj.value('settest.scalar') == { 'thing1': 'thing1', 'thing2': 'thing2' }

        cfgobj.set_value( 'settest.scalar2.0.key', "that wasn't a scalar" )
        assert cfgobj.value('settest.scalar2') == [ { "key": "that wasn't a scalar" } ]

        cfgobj.set_value( 'totallynewvalue.one', 'one' )
        cfgobj.set_value( 'totallynewvalue.two', 'two' )
        assert cfgobj.value('totallynewvalue') == { 'one': 'one', 'two': 'two' }

    def test_clone( self, cfgobj ):
        newconfig = config.Config.clone( _rundir / 'test.yaml' )
        newconfig.set_value( 'clonetest2', 'manuallyset' )
        assert cfgobj.value('clonetest1') == 'orig'
        assert cfgobj.value('clonetest2') == 'orig'
        assert newconfig.value('clonetest1') == 'orig'
        assert newconfig.value('clonetest2') == 'manuallyset'

