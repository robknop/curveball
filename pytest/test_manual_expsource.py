import math
import sys
import pathlib
import pytest

import numpy
import numpy.random

import astropy.wcs
from astropy.io import fits

import db
import exposuresource
import processing
import astrometry

_rundir = pathlib.Path(__file__).parent
if not str(_rundir.parent) in sys.path:
    sys.path.insert( 0, str(_rundir.parent / "bin") )

from manual_image_fixtures import make_manual_image, load_manual_image, manual_expsource
from base_test_an_exposure_source import BaseExposureSourceTest

class TestManualExpsource(BaseExposureSourceTest):
    exposuresourcename = 'manual_expsource'
    exposuresourcesecondary = None

    basic_topdirname = 'manual_expsource'
    basic_expname = 'manual_20210415_081533.fits'
    basic_imagename = 'manual_20210415_081533.00.fits'
    basic_weightname = 'manual_20210415_081533.00.weight.fits'
    basic_maskname = 'manual_20210415_081533.00.mask.fits'
    basic_archivesubdir = pathlib.Path( 'manual_expsource/2021-04-15' )
    basic_relpath = pathlib.Path( 'manual_expsource/2021-04-15/manual_20210415_081533.00.fits' )
    basic_exposure_basename = 'manual_20210415_081533'
    basic_image_basename = 'manual_20210415_081533.00'
    basic_chiptag = '00'
    basic_yyyymmdd = '2021-04-15'
    basic_isstack = False

    basicabs = { 'pixscale': 1e-4 }
    basicstotest = { 'pixscale': 0.37,
                     'clipsize': 51,
                     'badmask': 0xffff,
                     'ra_degrees_keyword': 'TELRAD',
                     'dec_degrees_keyword': 'TELDECD',
                     'saturate_keyword': 'SATURATE',
                     'readnoise_keyword': 'READNOI',
                     'darkcurrent_keyword': 'DARKCUR',
                     'exptime_keyword': 'EXPTIME'
                     }
    basickeykeywords = { 'FILTER','TELRAD','TELDECD','GAIN','SATURATE','MAGZP','MAGZPUNC' }

    hdr_gain = pytest.approx( 2.5, abs=0.01)
    hdr_readnoise = 0.
    hdr_darkcurrent = 0.
    hdr_exptime = pytest.approx( 60.0, abs=0.01 )
    hdr_expsource_filter = 'open'
    hdr_filter = 'open'
    hdr_guess_seeing = pytest.approx( 1.3, abs=1e-1 )

    approxexposure1mjd = 59319.34413

    cat_seeing = pytest.approx( 1.3, abs=0.1 )
    cat_skysig = pytest.approx( 14., abs=1. )
    cat_medsky = pytest.approx( 500, abs=4. )    #Hurm
    cat_minobjs = 18
    cat_maxgoodobjs = 22
    cat_minmeansnr = 50

    photocalib_magzp = pytest.approx( 27.00, abs=0.01 )
    photocalib_magzpunc = pytest.approx( 0.01, abs=0.01 )
    photocalib_lmtmg = pytest.approx( 20.4, abs=0.2 )

    loaded_expgallong = pytest.approx( 281.56, abs=1e-2 )
    loaded_expgallat = pytest.approx( -16.05, abs=1e-2 )
    loaded_expra = pytest.approx( 127.68, abs=1e-5 )
    loaded_expdec = pytest.approx( -67.30, abs=1e-5 )
    loaded_band_filtercode = 'open'
    loaded_band_name = 'open'
    loaded_ra = pytest.approx( 127.68, abs=1e-3 )
    loaded_dec = pytest.approx( -67.30, abs=1e-3 )
    loaded_magzp = photocalib_magzp

    y0 = 786
    y1 = 909
    x0 = 785
    x1 = 832
    origimg_mean = pytest.approx( 506.01782, abs=1e-3 )
    origimg_std = pytest.approx( 68.1200, abs=1e-2 )
    origimg_weightmean = pytest.approx( 0.0049747, abs=1e-5 )
    origimg_weightstd = pytest.approx( 0.000284, abs=1e-5 )
    origimg_masksum = 500

    check_no_lmt_mg = False
    origimg_header_checks = { 'MAGZP': pytest.approx( 27.00, abs=0.01 ),
                              'SATURATE': pytest.approx( 50000., abs=10. ),
                              'MJD': pytest.approx( 59319.34413, abs=1e-5 ),
                              'GAIN': 2.5,
                              'SEEING': 1.3,
                              'FILTER': 'open'
                             }


    # ======================================================================

    @pytest.fixture(scope='class')
    def expsource( self, manual_expsource ):
        return exposuresource.ExposureSource.get( 'manual_expsource', None )

    @pytest.fixture(scope='class')
    def blob_and_dex( self, expsource ):
        return ( None, None )

    @pytest.fixture(scope='class')
    def imagepath( self, database, manual_expsource ):
        impath = make_manual_image( database, manual_expsource, seed=42 )
        yield impath
        basename = impath.name[:-4]
        for suffix in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
            ( impath.parent / f"{basename}{suffix}" ).unlink( missing_ok=True )
            manual_expsource.delete_file_on_archive( f"{basename}{suffix}" )

    @pytest.fixture(scope='class')
    def loaded_imagepath( self, database, manual_expsource, imagepath ):
        loadedimgobj = load_manual_image( database, manual_expsource, imagepath )
        yield loadedimgobj

        basename = imagepath.name[:-5]
        for suffix in [ '.cat', '.psf', '.psf.xml', 'mask.fits', '.cat', '.psf', '.psf.xml' ]:
            ( imagepath.parent / f"{basename}{suffix}" ).unlink( missing_ok=True )
            manual_expsource.delete_file_on_archive( f"{basename}{suffix}" )

        with db.DB.get() as dbo:
            # SQLAlchemy is such a mystery.  I don't know why this didn't work.
            # dbo.db.merge( loadedimageobj )
            # dbo.db.delete( loadedimageobj )
            # dbo.db.commit()
            con = dbo.db.connection().engine.raw_connection()
            cursor = con.cursor()
            cursor.execute( "DELETE FROM image WHERE basename=%(basename)s", { 'basename': basename } )
            cursor.close()
            con.commit()
            
    @pytest.fixture(scope='class')
    def ensure_image_local( self, database, manual_expsource ):
        impath = make_manual_image( database, manual_expsource, seed=42 )
        yield impath
        for suffix in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
            ( impath.parent / f"{basename}{suffix}" ).unlink( missing_ok=True )
            manual_expsource.delete_file_on_archive( f"{basename}{suffix}" )


    @pytest.mark.xfail( reason="Not defined for manual_expsource" )
    def test_find_images( self, expsource, blob_and_dex ):
        assert False

    @pytest.mark.xfail( reason="Not defined for manual_expsource" )
    def test_load_image2_no_photocalib( self, database, expsource, loaded_imagepath2_nophotocalib ):
        assert False

    @pytest.mark.xfail( reason="Not defined for manual_expsource" )
    def test_photocalib_indb( self, expsource, database, loaded_imagepath2_nophotocalib, photocalib2_indb ):
        assert False

    @pytest.mark.xfail( reason="Built-in assumptions break for manual_expsource" )
    def test_ensure_sourcefiles_local( self ):
        assert False
