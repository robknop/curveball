import sys
import pathlib
import pytest

_rundir = pathlib.Path(__file__).parent
if not str(_rundir.parent) in sys.path:
    sys.path.insert( 0, str(_rundir.parent / "bin") )

from base_test_an_exposure_source import BaseExposureSourceTest

class TestZTF(BaseExposureSourceTest):
    exposuresourcename = 'ZTF'
    exposuresourcesecondary = None

    # Many many many constants that are what test compare things to

    # These give the parameters of a search we'll do with find_images It
    # will llok for images containing the ran and dec, in mjd range
    # findstart to findend We expect to find an image in there whose mjd
    # is approxexposuremjd (to with 1e-4), whose filter is foundfilter,
    # and whose exposuretime is foundexptime.  The total number of
    # images found should be between minnumfound and maxnumfound.
    findra = 153.17869
    finddec = 39.37392
    approxexposure1mjd = 58561.24413
    approxexposure2mjd = 58558.18191
    approxexposure3mjd = 58559.25969
    approxexposure4mjd = 58559.30766
    findstart = approxexposure1mjd - 5
    findend = approxexposure1mjd + 5
    found1filter = 'zg'
    found1exptime = 30
    found2filter = 'zr'
    found2exptime = 30
    minnumfound = 1
    maxnumfound = 15

    # Must we make a weight image, or does one come directly from the exposuresource?
    must_make_weight = True

    # Basic keyword and value testing for this exposuresource
    basicabs = { 'pixscale': 1e-4 }
    basicstotest = { 'pixscale': 1.013,
                     'clipsize': 51,
                     'badmask': 0x07e1,
                     'ra_degrees_keyword': 'TELRAD',
                     'dec_degrees_keyword': 'TELDECD',
                     'saturate_keyword': 'SATURATE',
                     'readnoise_keyword': 'READNOI',
                     'darkcurrent_keyword': 'DARKCUR',
                     'exptime_keyword': 'EXPOSURE'
                     }
    basickeykeywords = { 'ORIGIN', 'OBSERVER', 'INSTRUME', 'IMGTYPE', 'FILTERID', 'FILTER', 'FILTPOS',
                         'RA', 'DEC', 'OBSERVAT', 'TELESCOPE', 'PIXSCALX', 'PIXSCALY', 'GAIN',
                         'SATURATE', 'DBFIELD', 'MAGZP', 'MAGZPUNC' }

    # Test filename and path resolution for this exposuresource
    basic_topdirname = 'ZTF'
    basic_expname = 'ztf_20181116491134_000789_zg_io.fits'
    basic_imagename = 'ztf_20181116491134_000789_zg_io.011.fits'
    basic_maskname = 'ztf_20181116491134_000789_zg_io.011.mask.fits'
    basic_weightname = 'ztf_20181116491134_000789_zg_io.011.weight.fits'
    basic_archivesubdir = pathlib.Path( 'ZTF/2018-11-16' )
    basic_relpath = pathlib.Path( 'ZTF/2018-11-16/ztf_20181116491134_000789_zg_io.011.fits' )
    basic_exposure_basename = 'ztf_20181116491134_000789_zg_io'
    basic_image_basename = 'ztf_20181116491134_000789_zg_io.011'
    basic_chiptag = '011'
    basic_yyyymmdd = '2018-11-16'
    basic_isstack = False

    # Check the header in the image *as downloaded* (so not after
    # curveball has loaded it).  Set check_no_lmt_mg to True if "LMT_MG"
    # is not in the original header.  Set the dictionary to a series of
    # keywords and values expected to be in the header.
    check_no_lmt_mg = True
    origimg_header_checks = { 'MAGZP': pytest.approx( 26.327, abs=0.001 ),
                              'FILTER': 'ZTF_g',
                              'OBSMJD': pytest.approx( 58561.24413, abs=1e-5 ),
                              'TELRAD': pytest.approx( 154.478, abs=1e-3 ),
                              'TELDECD': pytest.approx( 36.95, abs=1e-3 ),
                              'EXPTIME': pytest.approx( 30. )
                              }

    # A region on the image that we'll check to make sure the downloaded
    # image values are plausible.  Look at the mean and standard
    # deviation of the image and weight files in the region, and the sum
    # of the whole mask file.
    x0 = 1000
    x1 = 1100
    y0 = 1000
    y1 = 1100
    origimg_mean = pytest.approx( 2236.60, abs=1e-2 )
    origimg_std = pytest.approx( 20.14, abs=1e-2 )
    origimg_weightmean = pytest.approx( 0.002747, abs=1e-6 )
    origimg_weightstd = pytest.approx( 2.4e-5, abs=1e-6 )
    origimg_masksum = 11887

    # These will be checks on the various calls to exposuresource that
    # extract header information.
    hdr_gain = pytest.approx( 6.2, abs=0.1 )
    hdr_readnoise = pytest.approx( 10.0, abs=0.1 )
    hdr_darkcurrent = pytest.approx( 1.0, abs=0.1 )
    hdr_exptime = pytest.approx( 30.0, abs=0.1 )
    hdr_expsource_filter = 'ZTF_g'
    hdr_filter = 'ZTF_g'
    hdr_guess_seeing = pytest.approx( 2.09, abs=0.01 )

    # These are the values expected for the seeing, skysig, and medsky
    # after sextractor and psfex is run on the image.  minobjs is a
    # lower bound on the number of objects that should be found in the
    # image, and minmeansnr is a lower bound on the *average* s/n of all
    # the objects found in the image.
    cat_seeing = pytest.approx( 2.082, abs=0.003 )
    cat_skysig = pytest.approx( 21.5, abs=0.2 )
    cat_medsky = pytest.approx( 2238, abs=1 )
    cat_minobjs = 500
    cat_maxgoodobjs = 100000
    cat_minmeansnr = 45.

    # After photocalib, what should the magnitude zeropoint,
    # uncertainty, and limiting magnitudes be?  (These will
    # be pulled from the post-photocalib image header.)
    photocalib_magzp = pytest.approx( 26.327, abs=0.001 )
    photocalib_magzpunc = pytest.approx( 5e-5, abs=1e-5 )    #This value is absurd
    photocalib_lmtmg = pytest.approx( 19.8, abs=0.1 )

    # Once the image is loaded into the database, check some fields of
    # the image and exposure tables.
    loaded_expgallat = pytest.approx( 56.2675, abs=1e-4 )
    loaded_expgallong = pytest.approx( 186.253, abs=1e-3 )
    loaded_expra = pytest.approx( 154.478, abs=1e-3 )
    loaded_expdec = pytest.approx( 36.95, abs=1e-3 )
    loaded_band_filtercode = 'zg'
    loaded_band_name = 'ZTF_g'
    loaded_ra = pytest.approx( 152.722, abs=1e-3 )
    loaded_dec = pytest.approx( 39.377, abs=1e-3 )
    loaded_magzp = pytest.approx( 26.327, abs=0.001 )

    loaded2_magzp = pytest.approx( 26.195, abs=0.001 )
    loaded2_seeing = pytest.approx( 2.817, abs=0.001 )
    loaded2_medsky = pytest.approx( 478.9, abs=0.1 )
    loaded2_lmtmg = pytest.approx( 20.339, abs=0.001 )
    
    # Below should be tests specific to this exposure source
    
    # There's something perverse about hardcoding the chip offsets here
    # that are already hardcoded in the ztf standard loading thingy.  Is
    # this even worth testing?  I suppose so, because if I edit that
    # file by accident, I won't have edited this one the same way.
    def test_chips( self, expsource ):
        raoffsets = {
            '011' :  [ 3.635, 3.60401, 2.75362, 2.7782 ],
            '012' :  [ 2.77756, 2.75304, 1.90237, 1.92041 ],
            '013' :  [ 2.76427, 2.74018, 1.89266, 1.9101 ],
            '014' :  [ 3.61869, 3.58795, 2.74082, 2.76475 ],
            '021' :  [ 1.79194, 1.77782, 0.926854, 0.934429 ],
            '022' :  [ 0.934108, 0.92649, 0.0754188, 0.0764791 ],
            '023' :  [ 0.930343, 0.922852, 0.0750287, 0.075851 ],
            '024' :  [ 1.78506, 1.77098, 0.92314, 0.93063 ],
            '031' :  [ -0.0460581, -0.0469337, -0.897921, -0.903623 ],
            '032' :  [ -0.903902, -0.898225, -1.74911, -1.76133 ],
            '033' :  [ -0.901904, -0.896283, -1.74402, -1.75627 ],
            '034' :  [ -0.0471624, -0.0482614, -0.896017, -0.901592 ],
            '041' :  [ -1.89191, -1.87707, -2.72765, -2.74903 ],
            '042' :  [ -2.74973, -2.72827, -3.5786, -3.60649 ],
            '043' :  [ -2.73943, -2.71843, -3.5656, -3.59316 ],
            '044' :  [ -1.88471, -1.87033, -2.71778, -2.73874 ],
            '051' :  [ 3.6712, 3.64009, 2.7823, 2.80635 ],
            '052' :  [ 2.80569, 2.78173, 1.9236, 1.94061 ],
            '053' :  [ 2.79357, 2.77001, 1.91547, 1.93185 ],
            '054' :  [ 3.65546, 3.62469, 2.77052, 2.79402 ],
            '061' :  [ 1.8083, 1.79309, 0.935065, 0.943002 ],
            '062' :  [ 0.942697, 0.93472, 0.0764005, 0.0773198 ],
            '063' :  [ 0.938646, 0.930794, 0.0760603, 0.0767168 ],
            '064' :  [ 1.8008, 1.78572, 0.931135, 0.938968 ],
            '071' :  [ -0.0480242, -0.0462265, -0.904488, -0.913473 ],
            '072' :  [ -0.913832, -0.904859, -1.76305, -1.77914 ],
            '073' :  [ -0.908646, -0.899901, -1.75443, -1.77049 ],
            '074' :  [ -0.0463888, -0.0448727, -0.899506, -0.908302 ],
            '081' :  [ -1.91217, -1.89421, -2.75205, -2.77702 ],
            '082' :  [ -2.77773, -2.75274, -3.61022, -3.64216 ],
            '083' :  [ -2.76431, -2.73992, -3.5937, -3.62545 ],
            '084' :  [ -1.90226, -1.88498, -2.73927, -2.76383 ],
            '091' :  [ 3.7073, 3.67484, 2.80911, 2.83393 ],
            '092' :  [ 2.83328, 2.80849, 1.94251, 1.95966 ],
            '093' :  [ 2.82084, 2.79657, 1.93449, 1.951 ],
            '094' :  [ 3.69107, 3.65903, 2.79717, 2.82153 ],
            '101' :  [ 1.82671, 1.8106, 0.944269, 0.952627 ],
            '102' :  [ 0.95229, 0.943889, 0.0774134, 0.078068 ],
            '103' :  [ 0.94809, 0.939841, 0.0773949, 0.077734 ],
            '104' :  [ 1.81851, 1.8025, 0.940114, 0.948271 ],
            '111' :  [ -0.0452611, -0.0444687, -0.91088, -0.919295 ],
            '112' :  [ -0.919571, -0.911213, -1.77756, -1.79364 ],
            '113' :  [ -0.915222, -0.907044, -1.76937, -1.78537 ],
            '114' :  [ -0.0446728, -0.0442693, -0.906681, -0.914918 ],
            '121' :  [ -1.92636, -1.90767, -2.77366, -2.80011 ],
            '122' :  [ -2.80079, -2.7744, -3.63999, -3.67412 ],
            '123' :  [ -2.78654, -2.76066, -3.6224, -3.65604 ],
            '124' :  [ -1.91604, -1.89799, -2.76008, -2.78597 ],
            '131' :  [ 3.74916, 3.71345, 2.83878, 2.86622 ],
            '132' :  [ 2.86545, 2.83803, 1.96296, 1.98209 ],
            '133' :  [ 2.85108, 2.8242, 1.95347, 1.97199 ],
            '134' :  [ 3.73034, 3.69516, 2.82497, 2.85171 ],
            '141' :  [ 1.85152, 1.83181, 0.956462, 0.967756 ],
            '142' :  [ 0.967332, 0.956066, 0.0805535, 0.0834768 ],
            '143' :  [ 0.960432, 0.949416, 0.0783974, 0.0809784 ],
            '144' :  [ 1.84022, 1.8207, 0.949804, 0.960852 ],
            '151' :  [ -0.0466679, -0.044186, -0.919752, -0.930624 ],
            '152' :  [ -0.930888, -0.920134, -1.79541, -1.81473 ],
            '153' :  [ -0.924332, -0.913704, -1.78459, -1.80371 ],
            '154' :  [ -0.0443744, -0.0420903, -0.913311, -0.923964 ],
            '161' :  [ -1.94577, -1.92719, -2.80228, -2.8291 ],
            '162' :  [ -2.82983, -2.80292, -3.67759, -3.71286 ],
            '163' :  [ -2.81575, -2.78935, -3.65963, -3.69443 ],
            '164' :  [ -1.936, -1.91813, -2.78865, -2.81511 ],
        }
        decoffsets = {
            '011' :  [ -2.0821, -2.94725, -2.91885, -2.05323 ],
            '012' :  [ -2.05324, -2.91882, -2.89724, -2.03154 ],
            '013' :  [ -2.91916, -3.78486, -3.76331, -2.89749 ],
            '014' :  [ -2.94749, -3.81272, -3.78485, -2.91906 ],
            '021' :  [ -2.03461, -2.9005, -2.88926, -2.02307 ],
            '022' :  [ -2.023, -2.8892, -2.88473, -2.01873 ],
            '023' :  [ -2.88945, -3.75571, -3.75107, -2.88505 ],
            '024' :  [ -2.9007, -3.7666, -3.75571, -2.8895 ],
            '031' :  [ -2.01897, -2.88498, -2.88718, -2.02102 ],
            '032' :  [ -2.02105, -2.88721, -2.89614, -2.03034 ],
            '033' :  [ -2.88745, -3.7536, -3.76232, -2.89643 ],
            '034' :  [ -2.88528, -3.75131, -3.75364, -2.88744 ],
            '041' :  [ -2.03395, -2.89971, -2.91778, -2.05219 ],
            '042' :  [ -2.05217, -2.91782, -2.94268, -2.07749 ],
            '043' :  [ -2.91807, -3.7839, -3.80832, -2.94298 ],
            '044' :  [ -2.90001, -3.76579, -3.78384, -2.91809 ],
            '051' :  [ -0.169973, -1.0353, -1.00751, -0.141727 ],
            '052' :  [ -0.141577, -1.00749, -0.986878, -0.121096 ],
            '053' :  [ -1.0078, -1.87361, -1.85307, -0.987253 ],
            '054' :  [ -1.03555, -1.90082, -1.87358, -1.00777 ],
            '061' :  [ -0.121739, -0.987571, -0.975741, -0.109622 ],
            '062' :  [ -0.109652, -0.975772, -0.971228, -0.1052 ],
            '063' :  [ -0.976036, -1.84221, -1.83751, -0.971486 ],
            '064' :  [ -0.987872, -1.8537, -1.84223, -0.976049 ],
            '071' :  [ -0.100624, -0.966613, -0.971806, -0.105643 ],
            '072' :  [ -0.105643, -0.971784, -0.984187, -0.118329 ],
            '073' :  [ -0.972097, -1.83825, -1.85025, -0.984477 ],
            '074' :  [ -0.966926, -1.8329, -1.83822, -0.972094 ],
            '081' :  [ -0.119376, -0.985038, -1.00615, -0.140625 ],
            '082' :  [ -0.140546, -1.00618, -1.03448, -0.169528 ],
            '083' :  [ -1.00646, -1.87213, -1.89985, -1.0348 ],
            '084' :  [ -0.985323, -1.85107, -1.8721, -1.00645 ],
            '091' :  [ 1.74045, 0.875325, 0.903608, 1.76929 ],
            '092' :  [ 1.76929, 0.903662, 0.924219, 1.78993 ],
            '093' :  [ 0.90336, 0.0375735, 0.0581984, 0.924004 ],
            '094' :  [ 0.87501, 0.00974563, 0.037574, 0.903376 ],
            '101' :  [ 1.7958, 0.930003, 0.942204, 1.80836 ],
            '102' :  [ 1.80836, 0.942184, 0.946694, 1.81275 ],
            '103' :  [ 0.941929, 0.0757738, 0.0804499, 0.946441 ],
            '104' :  [ 0.929692, 0.0638452, 0.075741, 0.94195 ],
            '111' :  [ 1.81406, 0.948051, 0.94382, 1.80995 ],
            '112' :  [ 1.80995, 0.943834, 0.931969, 1.79769 ],
            '113' :  [ 0.943582, 0.0774554, 0.0659001, 0.931714 ],
            '114' :  [ 0.947786, 0.0817716, 0.0774101, 0.943554 ],
            '121' :  [ 1.79659, 0.930819, 0.909024, 1.77459 ],
            '122' :  [ 1.7746, 0.908943, 0.879442, 1.7445 ],
            '123' :  [ 0.908603, 0.0429537, 0.0140133, 0.87913 ],
            '124' :  [ 0.930589, 0.0648153, 0.0430325, 0.908671 ],
            '131' :  [ 3.65193, 2.78683, 2.81755, 3.68316 ],
            '132' :  [ 3.68318, 2.81756, 2.84008, 3.70581 ],
            '133' :  [ 2.81726, 1.95162, 1.97399, 2.8398 ],
            '134' :  [ 2.78659, 1.92151, 1.95168, 2.81726 ],
            '141' :  [ 3.7046, 2.83875, 2.85382, 3.72007 ],
            '142' :  [ 3.72016, 2.85382, 2.86085, 3.72691 ],
            '143' :  [ 2.85354, 1.98751, 1.9946, 2.8605 ],
            '144' :  [ 2.83846, 1.97282, 1.98756, 2.85354 ],
            '151' :  [ 3.72844, 2.86224, 2.856, 3.72226 ],
            '152' :  [ 3.72226, 2.85605, 2.84155, 3.7074 ],
            '153' :  [ 2.85571, 1.9896, 1.97547, 2.84125 ],
            '154' :  [ 2.86202, 1.99601, 1.98967, 2.85572 ],
            '161' :  [ 3.7079, 2.84214, 2.82036, 3.68605 ],
            '162' :  [ 3.686, 2.82043, 2.79043, 3.65562 ],
            '163' :  [ 2.82008, 1.95444, 1.9251, 2.79017 ],
            '164' :  [ 2.84188, 1.97609, 1.95454, 2.82009 ],
        }

        for chip in expsource.chips:
            assert chip.isgood
            assert chip.ra0off == pytest.approx( raoffsets[ chip.chiptag ][0], abs=1e-4 )
            assert chip.ra1off == pytest.approx( raoffsets[ chip.chiptag ][1], abs=1e-4 )
            assert chip.ra2off == pytest.approx( raoffsets[ chip.chiptag ][2], abs=1e-4 )
            assert chip.ra3off == pytest.approx( raoffsets[ chip.chiptag ][3], abs=1e-4 )
            assert chip.dec0off == pytest.approx( decoffsets[ chip.chiptag ][0], abs=1e-4 )
            assert chip.dec1off == pytest.approx( decoffsets[ chip.chiptag ][1], abs=1e-4 )
            assert chip.dec2off == pytest.approx( decoffsets[ chip.chiptag ][2], abs=1e-4 )
            assert chip.dec3off == pytest.approx( decoffsets[ chip.chiptag ][3], abs=1e-4 )
