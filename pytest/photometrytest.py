import pytest

import db
import processing
import astrometry

from manual_image_fixtures import manual_expsource, make_manual_image, load_manual_image

class PhotometryTest:
    @pytest.fixture(scope='class')
    def loaded_manual_image( self, database, manual_expsource ):
        imagepath = make_manual_image( database, manual_expsource )
        basename = imagepath.name[:-5]
        wtpath = manual_expsource.local_path( f"{basename}.weight.fits" )
        mkpath = manual_expsource.local_path( f"{basename}.mask.fits" )
        loadedimgobj = load_manual_image( database, manual_expsource, imagepath )

        astrometry.save_image_header_to_db_wcs( manual_expsource, basename, curdb=database )
        seeing, skysig, medsky = processing.cat_psf_sky( manual_expsource, imagepath, wtpath, mkpath,
                                                         nodb=False, curdb=database, skipsky=True )

        yield loadedimgobj

        for suffix in [ '.cat', '.psf', '.psf.xml', 'mask.fits', '.cat', '.psf', '.psf.xml' ]:
            ( imagepath.parent / f"{basename}{suffix}" ).unlink( missing_ok=True )
            manual_expsource.delete_file_on_archive( f"{basename}{suffix}" )

        with db.DB.get() as dbo:
            # dbo.db.merge( loadedimgobj )
            # dbo.db.delete( loadedimgobj )
            # That gave a mysterious error.  I hate sqlalchemy.
            # So, use SQL, like God intended.  But, first, we have
            # to fight with SQL alchemy to get the raw postgres
            # connection.
            con = dbo.db.connection().engine.raw_connection()
            cursor = con.cursor()
            cursor.execute( "DELETE FROM image WHERE basename=%(basename)s", { 'basename': basename } )
            cursor.close()
            con.commit()

