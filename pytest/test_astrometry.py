import pytest

import numpy

import astrometry

class TestAstrometry:
    def test_download_gaia_stars( self ):
        ra = 128.
        dec = -40.
        dra = 0.1
        ddec = 0.1
        buffer = 0.2
        res = astrometry.download_gaia_stars( 128., -40., 0.1, 0.1, buffer=buffer )

        dracd = dra / numpy.cos( dec * numpy.pi / 180. )
        assert numpy.all( res.X_WORLD >= ( ra - (1+buffer) * (dracd/2.) ) )
        assert numpy.all( res.X_WORLD <= ( ra + (1+buffer) * (dracd/2.) ) )
        assert numpy.all( res.Y_WORLD >= ( dec - (1+buffer) * (ddec/2.) ) )
        assert numpy.all( res.Y_WORLD < ( dec + (1+buffer) * (ddec/2.) ) )
        assert numpy.max( res.MAG ) > 21.
        assert len(res) > 750
        
        # TODO test wcs version instead of ra/dec version
        # TODO test limmag, forcenew, regs, etc.
