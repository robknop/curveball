import math
import sys
import pathlib
import pytest

import numpy
import numpy.random

import astropy.wcs
from astropy.io import fits

import db
import photometry
import apphot

from photometrytest import PhotometryTest
from manual_image_fixtures import manual_expsource, load_manual_image

class TestApphot(PhotometryTest):
    @pytest.fixture(scope='class')
    def photor( self, manual_expsource, loaded_manual_image ):
        return apphot.ApPhot( 'apphot1', manual_expsource, loaded_manual_image )

    @pytest.fixture(scope='class')
    def got_photor( self, manual_expsource, loaded_manual_image ):
        # This one is subtly different from the previous.
        # If you just make apphot.ApPhot, it defaults to no aperture
        #   correction.  This one defaults to aperture correction in a
        #   radius of 5× the main radius.
        return photometry.Photomotor.get( 'apphot1', manual_expsource, loaded_manual_image )

    def test_single_norecenter( self, photor ):
        # photutils coords (backwards from numpy)
        origx = 794
        origy = 824
        phot, newx, newy, covar, info = photor.measure( origx, origy, noapercor=True )
        assert phot == pytest.approx( 7986, abs=1. )
        assert newx == origx
        assert newy == origy
        assert numpy.sqrt( covar[0][0] ) == pytest.approx( 101, abs=1. )
        for i in range( 3 ):
            for j in range( 3 ):
                if ( i != 0 ) and ( j != 0 ):
                    assert covar[i][j] == 0.
        assert info['radius'] == pytest.approx( 3.33, abs=0.01 )


    def test_multiple_norecenter( self, photor ):
        origx = [ 794, 899, 1988 ]
        origy = [ 824, 792, 363 ]
        phot,  newx, newy, covar, info = photor.measure( origx, origy, noapercor=True )
        assert phot == pytest.approx( numpy.array( [ 7985., 22635., 24392. ] ), abs=1. )
        for i, dphot in enumerate( [ 101., 127., 129. ] ):
            assert numpy.sqrt( covar[ i, 0, 0] ) == pytest.approx( dphot, abs=1. )
            for j in range( 3 ):
                for k in range( 3 ):
                    if ( j != 0 ) and ( k != 0 ):
                        assert covar[ i, j, k ] == 0.
        assert ( newx == numpy.array( origx ) ).all()
        assert ( newy == numpy.array( origy ) ).all()
        assert info['radius'] == pytest.approx( numpy.array( [ 3.33, 3.33, 3.33 ] ), abs=0.01 )


    def test_single_recenter( self, photor ):
        origx = 795
        origy = 824
        phot, newx, newy, covar, info = photor.measure( origx, origy, noapercor=True,
                                                        recenter=True, recentroiditerations=5 )
        assert phot == pytest.approx( 8056, abs=1. )
        assert newx == pytest.approx( 793.6, abs=0.1 )
        assert newy == pytest.approx( 824.1, abs=0.1 )
        expected_covar = numpy.array( [ [ 1.020e+04, 0., 0. ],
                                        [ 0., 1.666, 3.100e-2 ],
                                        [ 0., 3.100e-2, 1.774 ] ] )
        abs_covar = numpy.array( [ [ 1., 1e-8, 1e-8 ],
                                   [ 1e-8, 0.01, 0.001 ],
                                   [ 1e-8, 0.001, 0.01 ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) < abs_covar )
        assert info['radius'] == pytest.approx( 3.33, abs=0.01 )

    def test_multiple_recenter( self, photor ):
        origx = [ 793, 900, 1988 ]
        origy = [ 824, 793, 365 ]
        phot, newx, newy, covar, info = photor.measure( origx, origy, noapercor=True,
                                                        recenter=True, recentroiditerations=5 )
        # I'm distressed that the first aperture isn't returning the same thing as in
        #   test_single_recenter; think about this
        assert phot == pytest.approx( numpy.array( [ 8053., 23061., 24394. ] ), abs=1. )
        assert newx == pytest.approx( numpy.array( [ 793.6, 898.8, 1988.0 ] ), abs=0.1 )
        assert newy == pytest.approx( numpy.array( [ 824.1, 792.6, 363.0 ] ), abs=0.1 )
        expected_covar = numpy.array( [ [ [ 1.0199e+04, 0., 0. ],
                                          [ 0., 1.666, 3.100e-02 ],
                                          [ 0., 3.100e-02, 1.774 ] ],
                                        [ [ 1.6203e+04, 0., 0. ],
                                          [ 0., 1.843, -2.097e-02 ],
                                          [ 0., -2.097e-02, 1.653 ] ],
                                        [ [ 1.6735e+04, 0., 0. ],
                                          [ 0., 1.766, 3.79e-03 ],
                                          [ 0., 3.79e-03, 1.752 ] ] ] )
        abs_covar = numpy.array( [ [ 1., 1e-8, 1e-8 ],
                                   [ 1e-8, 0.01, 0.0001 ],
                                   [ 1e-8, 0.0001, 0.01 ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) <= abs_covar[numpy.newaxis, :, :] )
        assert info['radius'] == pytest.approx( numpy.array( [ 3.33, 3.33, 3.33 ] ), abs=0.01 )

    def test_recenter_with_apercor( self, photor ):
        abs_covar = numpy.array( [ [ 1., 1e-8, 1e-8 ],
                                   [ 1e-8, 0.01, 0.0001 ],
                                   [ 1e-8, 0.0001, 0.01 ] ] )

        origx = [ 793, 900, 1988 ]
        origy = [ 824, 793, 365 ]
        phot, newx, newy, covar, info = photor.measure( origx, origy, recenter=True, recentroiditerations=5 )
        assert phot == pytest.approx( numpy.array( [ 8828., 25304., 26790. ] ), abs=1. )
        assert newx == pytest.approx( numpy.array( [ 793.6, 898.8, 1988.0 ] ), abs=0.1 )
        assert newy == pytest.approx( numpy.array( [ 824.1, 792.6, 363.0 ] ), abs=0.1 )
        expected_covar = numpy.array( [ [ [ 1.2256e+04, 0., 0. ],
                                          [ 0., 1.666, 3.100e-02 ],
                                          [ 0., 3.100e-02, 1.774 ] ],
                                        [ [ 1.9506e+04, 0., 0., ],
                                          [ 0., 1.843, -2.097e-02 ],
                                          [ 0., -2.097e-02, 1.653 ] ],
                                        [ [ 2.0185e+04, 0., 0. ],
                                          [ 0., 1.766, 3.79e-03 ],
                                          [ 0., 3.79e-03, 1.752 ] ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) <= abs_covar[numpy.newaxis, :, :] )
        assert info['radius'] == pytest.approx( numpy.array( [ 3.33, 3.33, 3.33 ] ), abs=0.01 )
        assert info['apercor'] == pytest.approx( numpy.array( [ -0.100, -0.101, -0.102 ] ), abs=0.001 )
        assert ( info['dapercor'] == numpy.array( [ 0., 0., 0. ] ) ).all()

        # Test with a single value
        phot, newx, newy, covar, info = photor.measure( origx[0], origy[0], recenter=True, recentroiditerations=5 )
        assert phot == pytest.approx( 8828., abs=1. )
        assert newx == pytest.approx( 793.6, abs=0.1 )
        assert newy == pytest.approx( 824.1, abs=0.1 )
        expected_covar = numpy.array( [ [ 1.2256e+04, 0., 0. ],
                                        [ 0., 1.666, 3.100e-02 ],
                                        [ 0., 3.100e-02, 1.774 ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar) <= abs_covar[numpy.newaxis, :, :] )
        assert info['radius'] == pytest.approx( 3.33, abs=0.01 )
        assert info['apercor'] == pytest.approx( -0.100, abs=0.001 )
        assert info['dapercor'] == 0.

        phot, newx, newy, covar, info = photor.measure( origx, origy, recenter=True, recentroiditerations=5 )
        assert phot == pytest.approx( numpy.array( [ 8828., 25304., 26790. ] ), abs=1. )
        assert newx == pytest.approx( numpy.array( [ 793.6, 898.8, 1988.0 ] ), abs=0.1 )
        assert newy == pytest.approx( numpy.array( [ 824.1, 792.6, 363.0 ] ), abs=0.1 )
        expected_covar = numpy.array( [ [ [ 1.2256e+04, 0., 0. ],
                                          [ 0., 1.666, 3.100e-02 ],
                                          [ 0., 3.100e-02, 1.774 ] ],
                                        [ [ 1.9506e+04, 0., 0., ],
                                          [ 0., 1.843, -2.097e-02 ],
                                          [ 0., -2.097e-02, 1.653 ] ],
                                        [ [ 2.0185e+04, 0., 0. ],
                                          [ 0., 1.766, 3.79e-03 ],
                                          [ 0., 3.79e-03, 1.752 ] ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) <= abs_covar[numpy.newaxis, :, :] )
        assert info['radius'] == pytest.approx( numpy.array( [ 3.33, 3.33, 3.33 ] ), abs=0.01 )
        assert info['apercor'] == pytest.approx( numpy.array( [ -0.100, -0.101, -0.102] ), abs=0.001 )
        assert ( info['dapercor'] == numpy.array( [0., 0., 0. ] ) ).all()

        phot, newx, newy, covar, info = photor.measure( origx[0], origy[0], recenter=True, recentroiditerations=5 )
        assert phot == pytest.approx( 8828., abs=1. )
        assert newx == pytest.approx( 793.6, abs=0.1 )
        assert newy == pytest.approx( 824.1, abs=0.1 )
        expected_covar = numpy.array( [ [ 1.2256e+04, 0., 0. ],
                                        [ 0., 1.666, 3.100e-02 ],
                                        [ 0., 3.100e-02, 1.774 ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar) <= abs_covar[numpy.newaxis, :, :] )
        assert info['radius'] == pytest.approx( 3.33, abs=0.01 )
        assert info['apercor'] == pytest.approx( -0.100, abs=0.001 )
        assert info['dapercor'] == 0.


    def test_multiple_recenter_with_apercor_using_photometry_get( self, got_photor ):
        abs_covar = numpy.array( [ [ 1., 1e-8, 1e-8 ],
                                   [ 1e-8, 0.01, 0.0001 ],
                                   [ 1e-8, 0.0001, 0.01 ] ] )

        origx = [ 793, 900, 1988 ]
        origy = [ 824, 793, 365 ]
        phot, newx, newy, covar, info = got_photor.measure( origx, origy, recenter=True )
        assert phot == pytest.approx( numpy.array( [ 8828., 25304., 26791. ] ), abs=1. )
        assert newx == pytest.approx( numpy.array( [ 793.6, 898.8, 1988.0 ] ), abs=0.1 )
        assert newy == pytest.approx( numpy.array( [ 824.1, 792.6, 363.0 ] ), abs=0.1 )
        expected_covar = numpy.array( [ [ [ 1.2256e+04, 0., 0. ],
                                          [ 0., 1.666, 3.100e-02 ],
                                          [ 0., 3.100e-02, 1.774 ] ],
                                        [ [ 1.9506e+04, 0., 0., ],
                                          [ 0., 1.843, -2.097e-02 ],
                                          [ 0., -2.097e-02, 1.653 ] ],
                                        [ [ 2.0185e+04, 0., 0. ],
                                          [ 0., 1.766, 3.79e-03 ],
                                          [ 0., 3.79e-03, 1.752 ] ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) <= abs_covar[numpy.newaxis, :, :] )
        assert info['radius'] == pytest.approx( numpy.array( [ 3.33, 3.33, 3.33 ] ), abs=0.01 )
        assert info['apercor'] == pytest.approx( numpy.array( [ -0.100, -0.101, -0.102 ] ), abs=0.001 )
        assert ( info['dapercor'] == numpy.array( [ 0., 0., 0. ] ) ).all()

        # Also want to test when we send a single position
        phot, newx, newy, covar, info = got_photor.measure( origx[0], origy[0], recenter=True )
        assert phot == pytest.approx( 8828., abs=1. )
        assert newx == pytest.approx( 793.6, abs=0.1 )
        assert newy == pytest.approx( 824.1, abs=0.1 )
        expected_covar = numpy.array( [ [ 1.2256e+04, 0., 0. ],
                                        [ 0., 1.666, 3.100e-02 ],
                                        [ 0., 3.100e-02, 1.774 ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) <= abs_covar )
        assert info['radius'] == pytest.approx( 3.33, abs=0.01 )
        assert info['apercor'] == pytest.approx( -0.100, abs=0.001 )
        assert info['dapercor'] == 0.

        phot, newx, newy, covar, info = got_photor.measure( origx, origy, recenter=True )
        assert phot == pytest.approx( numpy.array( [ 8828., 25304., 26791. ] ), abs=1. )
        assert newx == pytest.approx( numpy.array( [ 793.6, 898.8, 1988.0 ] ), abs=0.1 )
        assert newy == pytest.approx( numpy.array( [ 824.1, 792.6, 363.0 ] ), abs=0.1 )
        expected_covar = numpy.array( [ [ [ 1.2256e+04, 0., 0. ],
                                          [ 0., 1.666, 3.100e-02 ],
                                          [ 0., 3.100e-02, 1.774 ] ],
                                        [ [ 1.9506e+04, 0., 0., ],
                                          [ 0., 1.843, -2.097e-02 ],
                                          [ 0., -2.097e-02, 1.653 ] ],
                                        [ [ 2.0185e+04, 0., 0. ],
                                          [ 0., 1.766, 3.79e-03 ],
                                          [ 0., 3.79e-03, 1.752 ] ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) <= abs_covar[numpy.newaxis, :, :] )
        assert info['radius'] == pytest.approx( numpy.array( [ 3.33, 3.33, 3.33 ] ), abs=0.01 )
        assert info['apercor'] == pytest.approx( numpy.array( [ -0.100, -0.101, -0.102 ] ), abs=0.001 )
        assert ( info['dapercor'] == numpy.array( [ 0., 0., 0. ] ) ).all()

        phot, newx, newy, covar, info = got_photor.measure( origx[1], origy[1], recenter=True )
        assert phot == pytest.approx( 25304., abs=1. )
        assert newx == pytest.approx( 898.8, abs=0.1 )
        assert newy == pytest.approx( 792.6, abs=0.1 )
        expected_covar = numpy.array( [ [ 1.9506e+04, 0., 0., ],
                                        [ 0., 1.843, -2.097e-02 ],
                                        [ 0., -2.097e-02, 1.653 ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) <= abs_covar )
        assert info['radius'] == pytest.approx( 3.33, abs=0.01 )
        assert info['apercor'] == pytest.approx( -0.101, abs=0.001 )
        assert info['dapercor'] == 0.

    def test_apercor( self, photor ):
        origx = 795
        origy = 824

        # Sanity check: for gaussians, the aperture correction from
        # 1FWHM to ∞ is about -0.07 magnitudes.  I'm getting something
        # bigger here because the saved seeing is 3.33 not 3.5 pixels
        # (whereas I constructed it with FWHM=3.5 pixels)

        # Test aperture correction from stars identified on the image,
        #   which isn't the method that actually gets used

        ac, dac = photor.apercor( origx, origy, infrad=[2,3,5,10], recentroiditerations=0, method='stars' )
        # These numbers are ugly, which is why I moved away from the 'stars' method...
        assert ac == pytest.approx( numpy.array( [-0.100, -0.101, -0.100, -0.084] ), abs=0.001 )
        assert dac == pytest.approx( numpy.array( [0.005, 0.006, 0.009, 0.017 ] ), abs=0.001 )

        # Results are the same here because the sextractor positions on my fake
        #   stars are all very good....
        ac, dac = photor.apercor( origx, origy, infrad=[2,3,5,10], recentroiditerations=5, method='stars' )
        assert ac == pytest.approx( numpy.array( [-0.100, -0.101, -0.100, -0.084] ), abs=0.001 )
        assert dac == pytest.approx( numpy.array( [0.005, 0.006, 0.009, 0.017] ), abs=0.001 )

        ac, dac = photor.apercor( origx, origy, infrad=[2,3,5,10], recentroiditerations=5,
                                  radius=2., radiusunit='pixel', method='stars' )
        assert ac == pytest.approx( numpy.array( [-0.554, -0.586, -0.586, -0.579] ), abs=0.001 )
        assert dac == pytest.approx( numpy.array( [0.004, 0.005, 0.006, 0.011] ), abs=0.001 )

        # Do one to really test if it's getting it right on a Gaussian
        ac, dac = photor.apercor( origx, origy, infrad=[2,3,5,10], recentroiditerations=5,
                                  radius=3.5, radiusunit='pixel', method='stars' )
        assert ac == pytest.approx( numpy.array( [-0.07, -0.07, -0.07, -0.07] ), abs=0.01 )


        # Now test the method actually used

        ac, dac = photor.apercor( origx, origy )
        assert ( len(ac) == 1 ) and ( ac[0] == pytest.approx( -0.101, abs=0.001 ) )

        ac, dac = photor.apercor( origx, origy, radius=2 )
        assert ( len(ac) == 1 ) and ( ac[0] == pytest.approx( -0.591, abs=0.001 ) )

        ac, dac = photor.apercor( origx, origy, radius=3.5 )
        # I'm sad that this isn't -0.070
        assert ( len(ac) ==1 ) and ( ac[0] == pytest.approx( -0.073, abs=0.001 ) )
