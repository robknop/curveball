import numbers
import pytest

import astropy.io.votable
import astropy.wcs
from astropy.io import fits

import db

from fixtures_ztf import ztf, ztf_downloaded_image, ztf_astromcalib

class TestZtfAstrometry:

    def test_astrom( self, database, ztf, ztf_downloaded_image, ztf_astromcalib ):
        basename = ztf_downloaded_image.basename
        impath = ztf.local_path( f"{basename}.fits" )
        with fits.open( impath  ) as hdu:
            imagehdr = hdu[0].header

            # Make sure that the WCS in the database matches the WCS in the header
            
            wcsdb = db.WCS.get_for_image( ztf_downloaded_image )
            for k, v in wcsdb.header:
                if k in [ 'HISTORY', 'COMMENT' ]:
                    continue
                if isinstance( v, numbers.Number ):
                    assert v == pytest.approx( imagehdr[k], rel=1e-4 )
                else:
                    assert v == imagehdr[k]

            # Make sure the scamp xml looks good

            scampxml = astropy.io.votable.parse( impath.parent / f"{basename}.scamp.xml" )
            scampstat = scampxml.get_table_by_index( 1 )
            nmatch = scampstat.array["AstromNDets_Reference"][0]
            sig0 = scampstat.array["AstromSigma_Reference"][0][0]
            sig1 = scampstat.array["AstromSigma_Reference"][0][1]
            assert nmatch > 300
            assert sig0 < 0.1
            assert sig1 < 0.1

            # Check a couple of coordintes
            x = [ 2016.46,  534.53 ]
            y = [ 2379.27, 1026.92 ]
            ra = [ 153.47964, 153.99196 ]
            dec = [  38.84303, 39.24304 ]

            wcs = astropy.wcs.WCS( imagehdr )
            sc = wcs.pixel_to_world( x, y )
            assert sc[0].ra.value == pytest.approx( ra[0], abs=1e-4 )
            assert sc[1].ra.value == pytest.approx( ra[1], abs=1e-4 )
            assert sc[0].dec.value == pytest.approx( dec[0], abs=1e-4 )
            assert sc[1].dec.value == pytest.approx( dec[1], abs=1e-4 )
                        

