import sys
import pathlib
import pytest

_rundir = pathlib.Path(__file__).parent
if not str(_rundir.parent) in sys.path:
    sys.path.insert( 0, str(_rundir.parent / "bin") )
import db
    
from base_test_an_exposure_source import BaseExposureSourceTest

class TestPTF(BaseExposureSourceTest):
    exposuresourcename = 'PTF'
    exposuresourcesecondary = None

    findra = 195.15858
    finddec = 28.05672
    approxexposuremjd = 55734.22418
    findstart = approxexposuremjd - 5
    findend = approxexposuremjd + 5
    foundfilter = 'R'
    foundexptime = 60
    minnumfound = 1
    maxnumfound = 20

    must_make_weight = True

    basicabs = { 'pixscale': 1e-4 }
    basicstotest = { 'pixscale': 1.01,
                     'clipsize': 51,
                     'badmask': 0x07e1,
                     'ra_degrees_keyword': 'TELRA',
                     'dec_degrees_keyword': 'TELDEC',
                     'saturate_keyword': 'SATURVAL',
                     'readnoise_keyword': 'READNOI',
                     'darkcurrent_keyword': 'DARKCUR',
                     'exptime_keyword': 'AEXPTIME'
                     }
    basickeykeywords = { 'ORIGIN', 'OBSERVER', 'INSTRUME', 'IMGTYP',
                        'FILTERID', 'FILTER', 'FILTERSL', 'OBJRAD', 'OBJDECD', 'TELESCOPE',
                        'PIXSCALE', 'GAIN', 'SATURVAL', 'DBFIELD', 'IMAGEZPT', 'COLORTRM', 'ZPTSIGMA' }

    basic_topdirname = 'PTF'
    basic_expname = 'ptf_20110621_043914_pr.fits'
    basic_imagename = 'ptf_20110621_043914_pr.08.fits'
    basic_maskname = 'ptf_20110621_043914_pr.08.mask.fits'
    basic_weightname = 'ptf_20110621_043914_pr.08.weight.fits'
    basic_archivesubdir = pathlib.Path( 'PTF/2011-06-21/' )
    basic_relpath = pathlib.Path( 'PTF/2011-06-21/ptf_20110621_043914_pr.08.fits' )
    basic_exposure_basename = 'ptf_20110621_043914_pr'
    basic_image_basename = 'ptf_20110621_043914_pr.08'
    basic_chiptag = '08'
    basic_yyyymmdd = '2011-06-21'
    basic_isstack = False

    check_no_lmt_mg = True
    origimg_header_checks = { 'MAGZPT': pytest.approx( 23.448, abs=0.001 ),
                              'FILTER': 'R',
                              'OBSMJD': pytest.approx( 55734.22418, abs=1e-5 ),
                              'TELRA': pytest.approx( 195.6198, abs=1e-4 ),
                              'TELDEC': pytest.approx( 28.2595, abs=1e-4 ),
                              'EXPTIME': pytest.approx( 60. )
                              }

    x0 = 1000
    x1 = 1100
    y0 = 1000
    y1 = 1100
    origimg_mean = pytest.approx( 765.83, abs=1e-2 )
    origimg_std = pytest.approx( 30.50, abs=1e-2 )
    origimg_weightmean = pytest.approx( 0.001763, abs=1e-6 )
    origimg_weightstd = pytest.approx( 6.2e-5, abs=1e-6 )
    origimg_masksum = 109852

    hdr_gain = pytest.approx( 1.4, abs=0.1 )
    hdr_readnoise = pytest.approx( 5.9, abs=0.1 )
    hdr_darkcurrent = pytest.approx( 0.1, abs=0.1 )
    hdr_exptime = pytest.approx( 60.0, abs=0.1 )
    hdr_expsource_filter = 'PTF_R'
    hdr_filter = 'R'
    hdr_guess_seeing = pytest.approx( 1.69, abs=0.01 )

    cat_seeing = pytest.approx( 1.74, abs=0.01 )
    cat_skysig = pytest.approx( 20.3, abs=0.2 )
    cat_medsky = pytest.approx( 743, abs=1 )
    cat_minobjs = 500
    cat_maxgoodobjs = 10000
    cat_minmeansnr = 30.

    photocalib_magzp = pytest.approx( 23.429, abs=0.001 )
    photocalib_magzpunc = pytest.approx( 0.029, abs=1e-3 )
    photocalib_lmtmg = pytest.approx( 17.2, abs=0.1 )
    
    loaded_expgallat = pytest.approx( 87.3069, abs=1e-4 )
    loaded_expgallong = pytest.approx( 58.4058, abs=1e-3 )
    loaded_expra = pytest.approx( 195.620, abs=1e-3 )
    loaded_expdec = pytest.approx( 28.260, abs=1e-3 )
    loaded_band_filtercode = 'pr'
    loaded_band_name = 'PTF_R'
    loaded_ra = pytest.approx( 195.144, abs=1e-3 )
    loaded_dec = pytest.approx( 27.712, abs=1e-3 )
    loaded_magzp = pytest.approx( 23.429, abs=0.001 )

    @pytest.mark.xfail( reason='Chips have fake values in database' )
    def test_chips( self, expsource ):
        assert False

    # This may effectively have already been run in the ensure files local test
    def test_source_url( self, expsource, blob_and_dex, loaded_imagepath ):
        row = blob_and_dex[0].iloc[blob_and_dex[1]]
        # loaded_imagepath is probably named poorly, because it's really a db.Image object
        assert ( f"https://irsa.ipac.caltech.edu/ibe/data/ptf/images/level1/{row['pfilename']}" ==
                 expsource.source_url( loaded_imagepath ) )
        assert ( f"https://irsa.ipac.caltech.edu/ibe/data/ptf/images/level1/{row['afilename1']}" ==
                 expsource.source_url( f"{loaded_imagepath.basename}.mask.fits" ) )
