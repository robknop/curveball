import sys
import pytest
import math

import numpy
import numpy.random

import db
import exposuresource
import processing
import astrometry

@pytest.fixture(scope='session')
def manual_expsource( database ):
    return exposuresource.ExposureSource.get( 'manual_expsource', None )

def make_manual_image( database, manual_expsource, seed=42 ):
    rng = numpy.random.default_rng( seed )

    fwhm = 3.5
    wid = int( 5 * fwhm )
    sigma = fwhm / ( 2 * math.sqrt( 2 * math.log( 2 ) ) )
    nstars = 20

    data = numpy.zeros( ( 1024, 2048 ), dtype=numpy.float32 )
    psf = numpy.empty( ( wid, wid ) )
    for i in range(nstars):
        x0 = rng.random() * 1024
        y0 = rng.random() * 2048
        amplitude = rng.random() * 30000 / ( 2. * numpy.pi * sigma**2 )

        xvals = numpy.arange( int( x0 - wid/2 ), int( x0 - wid/2 ) + wid, dtype=int )
        yvals = numpy.arange( int( y0 - wid/2 ), int( y0 - wid/2 ) + wid, dtype=int )
        psf[:,:] = amplitude * numpy.exp( -( ( xvals[:,numpy.newaxis] - x0 )**2 + 
                                             ( yvals[numpy.newaxis,:] - y0 )**2 )
                                          / ( 2 * sigma**2 ) )

        xlowoff = 0
        xhighoff = wid
        ylowoff = 0
        yhighoff = wid
        xmin = xvals[0]
        if ( xmin < 0 ):
            xlowoff = 0 - xmin
            xmin = 0
        xmax = xvals[0] + wid
        if ( xmax > 1024 ):
            xhighoff = xmax - 1024
            xmax = 1024
        ymin = yvals[0]
        if ( ymin < 0 ):
            ylowoff = 0 - ymin
            ymin = 0
        ymax = yvals[0] + wid
        if ( ymax > 2048 ):
            yhighoff = ymax - 2048
            ymax = 2048

        data[ xmin:xmax, ymin:ymax ] += psf[ xlowoff:xhighoff, ylowoff:yhighoff ]

    # Add shot noise to data
    data += rng.standard_normal( data.shape ) * ( numpy.sqrt(2.5*data) )
    # Add sky to data
    skylevel = 500.
    skynoise = math.sqrt( skylevel / 2.5 )
    data += skylevel + skynoise * rng.standard_normal( data.shape )
    # All noise
    weightdata = 1. / ( data / 2.5 )

    # Throw in a handful of bad pixels
    xvals = rng.integers( 0, 1024, 500 )
    yvals = rng.integers( 0, 2048, 500 )
    data[ xvals, yvals ] = 65536.
    weightdata[ xvals, yvals ] = 0.
    maskdata = numpy.zeros_like( data, dtype=numpy.int16 )
    maskdata[ xvals, yvals ] = 1
    
    basename = manual_expsource.create_image( data, weightdata=weightdata, maskdata=maskdata,
                                              y=2021, m=4, d=15, h=8, minute=15, s=33,
                                              ra=127.68, dec=-67.3, ignore_if_already_in_db=True )
    impath = manual_expsource.local_path( f"{basename}.fits" )

    return impath

def load_manual_image( database, manual_expsource, imagepath, seed=42 ):
    impath = imagepath
    basename = manual_expsource.image_basename( impath.name )

    imgobj = db.Image.get_by_basename( basename, curdb=database )
    assert imgobj is None

    loadedimgobj = manual_expsource.create_or_load_image( impath, curdb=database )
    assert loadedimgobj is not None
    # My manual images are by default photo calibed
    loadedimgobj.isphotom = True
    database.db.commit()

    return loadedimgobj
