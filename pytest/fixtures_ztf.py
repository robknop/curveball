import pathlib
import shutil
import pytest

import pandas

import db
import exposuresource
import processing
import astrometry

# ============================================================
# ============================================================
# Fixtures for something that requires a ztf image

@pytest.fixture(scope="session")
def ztf( database, archive ):
    return exposuresource.ExposureSource.get( 'ZTF', None )

@pytest.fixture(scope="module")
def ztf_downloaded_image( ztf, database ):
    # OK, this is ugly and perhaps a bad idea.  I'm
    #  putting in what I know the blob will be for
    #  ztf had I done the search, even though I don't do the
    #  search.  It's a bad idea, because blob is suppsoed
    #  to be opaque, yet here's a test fixture looking at
    #  the inside of it.  I do this so I don't have to
    #  put in a (potentially slow) search here, as that
    #  functionality is tested in test_ztf.py
    blob = pandas.DataFrame( [
        { 'in_row_id': 1,
          'in_ra': 153.1787,
          'in_dec': 39.3739,
          'ra': 153.638064,
          'dec': 39.085662,
          'infobits': 0,
          'field': 712,
          'ccdid': 8,
          'qid': 3,
          'rcid': 30,
          'fid': 1,
          'filtercode': 'zg',
          'pid': 805307663015,
          'nid': 805,
          'expid': 80530766,
          'itid': 1,
          'imgtype': 'object',
          'imgtypecode': 'o',
          'obsdate': '2019-03-17 07:23:02.685+00',
          'obsjd': 2458559.807662,
          'exptime': 30,
          'filefracday': 20190317307639,
          'seeing': 3.1378,
          'airmass': 1.019,
          'moonillf': 0.807088,
          'moonesb': 0,
          'maglimit': 19.550699,
          'crpix1': 1536.5,
          'crpix2': 1540.5,
          'crval1': 153.63809,
          'crval2': 39.085667,
          'cd11': -0.000281,
          'cd12': 0.000014,
          'cd21': -0.000014,
          'cd22': -0.000281,
          'ra1': 154.169877,
          'dec1': 39.538514,
          'ra2': 153.051271,
          'dec2': 39.495333,
          'ra3': 153.11345,
          'dec3': 38.630769,
          'ra4': 154.218148,
          'dec4': 38.673035,
          'ipac_pub_date': '2019-12-11 00:00:00+00',
          'ipac_gid': 1,
          'obsmjd': 58559.307662,
         } ] )
    
    fname = ztf.blob_image_name( blob, 0 )
    writtenfiles = ztf.download_blob_image( blob, 0 )
    imagefile = None
    maskfile = None
    for f in writtenfiles:
        if f.name == fname:
            imagefile = f
        else:
            maskfile = f 
    # We have to make weights for ztf files
    weightpath = processing.make_weight( ztf, imagefile )
    writtenfiles.append( weightpath )

    # Measure stuff for loading
    basename = ztf.image_basename( imagefile.name )
    seeing, skysig, medsky = processing.cat_psf_sky( ztf, imagefile, weightpath, maskfile, nodb=True )
    ztf.photocalib( imagefile )

    # Load image into database
    imgobj = None
    imgobj = db.Image.get_by_basename( basename, curdb=database )
    assert imgobj is None
    loadedimgobj = ztf.create_or_load_image( imagefile, curdb=database )
    assert loadedimgobj is not None
    loadedimgobj.isphotom = True
    database.db.commit()

    # Reload for anality
    imgobj = db.Image.get_by_basename( ztf.image_basename( imagefile.name ), curdb=database )

    yield imgobj

    database.db.delete( imgobj.exposure )
    database.db.delete( imgobj )
    database.db.commit()
        
    for suffix in [ '.cat', '.psf', '.psf.xml' ]:
        ( imagefile.parent / f'{basename}{suffix}' ).unlink( missing_ok=True )

    for f in writtenfiles:
        f.unlink( missing_ok=True )

@pytest.fixture(scope="module")
def ztf_astromcalib( database, ztf, ztf_downloaded_image ):
    ztf.ensure_image_local( ztf_downloaded_image.basename, curdb=database )
    impath = ztf.local_path( f"{ztf_downloaded_image.basename}.fits" )
    assert impath is not None
    cat = ztf.local_path( f"{ztf_downloaded_image.basename}.cat" )
    assert cat is not None

    assert astrometry.astrom_calib( impath, cat, ztf, savedb=True, curdb=database )

    yield True

    for suffix in [ ".gaia.cat", ".scamp.xml", ".astrom.cat" ]:
        path = ztf.local_path( f"{ztf_downloaded_image.basename}{suffix}" )
        path.unlink( missing_ok=True )

@pytest.fixture(scope='module')
def ztf_downloaded_image_cat_psf_sky_in_db( database, ztf, ztf_downloaded_image, ztf_astromcalib ):
    basename = ztf_downloaded_image.basename
    imagefile = ztf.local_path( f'{basename}.fits' )
    weightfile = ztf.local_path( f'{basename}.weight.fits' )
    maskfile = ztf.local_path( f'{basename}.mask.fits' )
    seeing, skysig, medsky = processing.cat_psf_sky( ztf, imagefile, weightfile, maskfile, nodb=False )

    yield True

    # TODO  : cleanup
    
    # import pdb; pdb.set_trace()
    # pass


# ======================================================================
# A different set of images, this time not downloaded from the actual
# ZTF server but just copied from the fodder subdirectory

@pytest.fixture(scope='module')
def ztf18aaxdrjn( database ):
    obj = db.Object( ra=185.006866199841,
                     dec=56.1588815903165,
                     t0=58279.2,
                     z=0.0339,
                     name='ZTF18aaxdrjn' )
    database.db.add( obj )
    database.db.commit()

    yield obj

    database.db.delete( obj )
    database.db.commit()
    
@pytest.fixture(scope='module')
def ztf_image( cfg, database, ztf ):
    imgbase = 'ztf_20180528256620_000789_zr_io.092'
    for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
        source = pathlib.Path( 'fodder' ) / f"{imgbase}{ext}"
        dest = pathlib.Path( cfg.value('datadirs')[0] ) / ztf.relpath( f"{imgbase}{ext}" )
        dest.parent.mkdir( parents=True, exist_ok=True )
        shutil.copy2( source, dest )

    band = db.Band.get_by_name( 'ZTF_r', ztf, curdb=database )

    img = db.Image( exposure_id=None,
                    band_id=band.id,
                    chip_id=None,
                    ra=184.629610942558,
                    dec=56.3098166415756,
                    ra1=185.463,
                    dec1=56.7131,
                    ra2=185.351,
                    dec2=55.8494,
                    ra3=183.814,
                    dec3=55.9013,
                    ra4=183.892,
                    dec4=56.7659,
                    skysig=10.6461,
                    medsky=766.87035,
                    seeing=1.87912,
                    magzp=26.1914,
                    limiting_mag=20.5696,
                    isstack=False,
                    isastrom=True,
                    isphotom=True,
                    isskysub=False,
                    hascat=True,
                    basename=imgbase )
    database.db.add( img )
    database.db.commit()

    astrometry.save_image_header_to_db_wcs( ztf, img.basename, curdb=database )

    yield img

    database.db.delete( img )
    database.db.commit()

    for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
        lpath = ztf.local_path( f'{imgbase}{ext}' )
        if lpath is not None:
            lpath.unlink()

@pytest.fixture(scope='module')
def ztf_image_on_archive( ztf, ztf_image ):
    for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
        ztf.upload_file_to_archive( f"{ztf_image.basename}{ext}" )

    yield ztf_image

    for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
        ztf.delete_file_on_archive( f"{ztf_image.basename}{ext}" )
            
@pytest.fixture(scope='module')
def ztf_ref( cfg, database, ztf, ztf18aaxdrjn ):
    refbase = 'stack_ztf_20210417285637_000789_zr_io.092'
    for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
        source = pathlib.Path( 'fodder' ) / f"{refbase}{ext}"
        dest = pathlib.Path( cfg.value('datadirs')[0] ) / ztf.relpath( f"{refbase}{ext}" )
        dest.parent.mkdir( parents=True, exist_ok=True )
        shutil.copy2( source, dest )

    band = db.Band.get_by_name( 'ZTF_r', ztf, curdb=database )

    ref = db.Image( exposure_id=None,
                    band_id=band.id,
                    chip_id=None,
                    ra=185.161028873304,
                    dec=56.2508626876234,
                    ra1=185.93,
                    dec1=55.8154,
                    ra2=185.947,
                    dec2=56.6812,
                    ra3=184.375,
                    dec3=56.6812,
                    ra4=184.393,
                    dec4=55.8154,
                    skysig=1.86991,
                    medsky=-1.32677,
                    seeing=1.70418,
                    magzp=26.282,
                    limiting_mag=22.6548,
                    isstack=True,
                    hasfakes=False,
                    isastrom=True,
                    isphotom=True,
                    isskysub=True,
                    hascat=True,
                    basename=refbase )
    database.db.add( ref )
    
    astrometry.save_image_header_to_db_wcs( ztf, ref.basename, curdb=database )

    objref = db.ObjectReference( object_id=ztf18aaxdrjn.id, image_id=ref.id )
    database.db.add( objref )
    database.db.commit()

    yield ref

    database.db.delete( objref )
    database.db.delete( ref )
    database.db.commit()

    for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
        lpath = ztf.local_path( f'{refbase}{ext}' )
        if lpath is not None:
            lpath.unlink()
    

@pytest.fixture(scope='module')
def ztf_ref_on_archive( ztf, ztf_ref ):
    for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
        ztf.upload_file_to_archive( f"{ztf_ref.basename}{ext}" )

    yield ztf_ref

    for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
        ztf.delete_file_on_archive( f"{ztf_ref.basename}{ext}" )
            
            
