import sys
import pathlib
import math
import numpy
from mpi4py import MPI
import pytest
from astropy.io import fits

_rundir = pathlib.Path(__file__).parent
if not str(_rundir.parent) in sys.path:
    sys.path.insert( 0, str(_rundir.parent / "bin") )
import db
import config
import exposuresource
import processing

# For your exposure source, create a test_<exposuresource>.py file with
# a class that derives from this class.  Then, se t a whole bunch of
# contsants in that class.  See test_ztf.py for an example and for some
# documentation on what you have to set.

class BaseExposureSourceTest:

    # ======================================================================
    # Fixtures

    @pytest.fixture(scope='class')
    def expsource( self, database ):
        return exposuresource.ExposureSource.get( self.exposuresourcename, secondary=self.exposuresourcesecondary )

    # Fixture for finding images.  Make it class-scope a fixture because
    # it's slow and we don't want have to redo it.

    @pytest.fixture(scope='class')
    def blob_and_dexen( self, expsource ):
        comm = MPI.COMM_WORLD
        rank = MPI.COMM_WORLD.Get_rank()
        if rank == 0:
            blob = expsource.find_images( self.findra, self.finddec, starttime=self.findstart, endtime=self.findend )
            dex1 = -1
            dex2 = -1
            dex3 = -1
            dex4 = -1
            for i in range( len(blob) ) :
                mjd = expsource.blob_image_mjd( blob, i )
                if math.fabs( mjd - self.approxexposure1mjd ) < 0.0001:
                    dex1 = i
                elif math.fabs( mjd - self.approxexposure2mjd ) < 0.0001:
                    dex2 = i
                elif math.fabs( mjd - self.approxexposure3mjd ) < 0.0001:
                    dex3 = i
                elif math.fabs( mjd - self.approxexposure4mjd ) < 0.0001:
                    dex4 = i
        else:
            blob = None
            dex1 = None
            dex2 = None
            dex3 = None
            dex4 = None
        blob = comm.bcast( blob, root=0 )
        dex1 = comm.bcast( dex1, root=0 )
        dex2 = comm.bcast( dex2, root=0 )
        dex3 = comm.bcast( dex3, root=0 )
        return ( blob, dex1, dex2, dex3, dex4 )

    def actually_download( self, expsource, blob, dex ):
        comm = MPI.COMM_WORLD
        rank = MPI.COMM_WORLD.Get_rank()
        if rank == 0:
            fname = expsource.blob_image_name( blob, dex )
            writtenfiles = expsource.download_blob_image( blob, dex )

            imagefile = None
            for f in writtenfiles:
                if f.name == fname:
                    imagefile = f
                    break
            if self.must_make_weight:
                weightpath = processing.make_weight( expsource, imagefile )
                writtenfiles.append( weightpath )
        else:
            imagefile = None
            writtenfiles = None
        imagefile = comm.bcast( imagefile, root=0 )
        writtenfiles = comm.bcast( writtenfiles, root=0 )

        return rank, imagefile, writtenfiles


    # This fixture will find an image on the EXPSOURCE server (from a specific
    # hardcoded search) and download it to the local image store.  It does
    # not load it into the database.  It also downloads and wrangles the
    # mask file, and creates the weight file.  It returns the path to the
    # image.

    @pytest.fixture(scope='class')
    def imagepath( self, expsource, blob_and_dexen ):
        blob, dex, junk2, junk3, junk4 = blob_and_dexen
        rank, imagefile, writtenfiles = self.actually_download( expsource, blob, dex )
        yield imagefile
        if rank == 0:
            for f in writtenfiles: f.unlink( missing_ok=True )

    @pytest.fixture(scope='class')
    def imagepath2( self, expsource, blob_and_dexen ):
        blob, junk1, dex, junk3, junk4 = blob_and_dexen
        rank, imagefile, writtenfiles = self.actually_download( expsource, blob, dex )
        yield imagefile
        if rank == 0:
            for f in writtenfiles: f.unlink( missing_ok=True )

    @pytest.fixture(scope='class')
    def imagepath3( self, expsource, blob_and_dexen ):
        blob, junk1, junk2, dex, junk4 = blob_and_dexen
        rank, imagefile, writtenfiles = self.actually_download( expsource, blob, dex )
        yield imagefile
        if rank == 0:
            for f in writtenfiles: f.unlink( missing_ok=True )

    @pytest.fixture(scope='class')
    def imagepath4( self, expsource, blob_and_dexen ):
        blob, junk1, junk2, junk3, dex = blob_and_dexen
        rank, imagefile, writtenfiles = self.actually_download( expsource, blob, dex )
        yield imagefile
        if rank == 0:
            for f in writtenfiles: f.unlink( missing_ok=True )

    # This fixture runs imagepath and then builds a .cat and .psf file from
    # from that image.  (This also sets the SEEING, SKYSIG, and MEDSKY
    # header keywords.)

    @pytest.fixture(scope='class')
    def cat_psf_sky( self, expsource, imagepath ):
        basename = expsource.image_basename( imagepath.name )
        maskpath = imagepath.parent / f'{basename}.mask.fits'
        weightpath = imagepath.parent / f'{basename}.weight.fits'
        seeing, skysig, medsky = processing.cat_psf_sky( expsource, imagepath, weightpath, maskpath, nodb=True )

        yield seeing, skysig, medsky

        for suffix in [ '.cat', '.psf', '.psf.xml' ]:
            ( imagepath.parent / f'{basename}{suffix}' ).unlink( missing_ok=True )


    @pytest.fixture( scope='class' )
    def cat_psf_sky2( self, expsource, imagepath2 ):
        basename = expsource.image_basename( imagepath2.name )
        maskpath = imagepath2.parent / f'{basename}.mask.fits'
        weightpath = imagepath2.parent / f'{basename}.weight.fits'
        seeing, skysig, medsky = processing.cat_psf_sky( expsource, imagepath2, weightpath, maskpath, nodb=True )

        yield seeing, skysig, medsky

        for suffix in [ '.cat', '.psf', '.psf.xml' ]:
            ( imagepath.parent / f'{basename}{suffix}' ).unlink( missing_ok=True )

    # This one does photocalib just in the header, not in the db
    @pytest.fixture(scope='class')
    def photocalib( self, expsource, imagepath, cat_psf_sky ):
        return expsource.photocalib( imagepath )

    # This fixture runs the EXPSOURCE photocalib routine, but marks the database
    # as having a catalog; it also uploads the .cat and .xml file to the
    # archive, and updates the image database seeing, skysig, and medsky values.
    #
    # This fixture is identical to the previous except for the nodb argument to cat_psf_sky
    # and the delete_file_on_archive call
    @pytest.fixture(scope='class')
    def cat_psf_sky_indb( self, expsource, imagepath, loaded_imagepath ):
        basename = expsource.image_basename( imagepath.name )
        maskpath = imagepath.parent / f'{basename}.mask.fits'
        weightpath = imagepath.parent / f'{basename}.weight.fits'
        seeing, skysig, medsky = processing.cat_psf_sky( expsource, imagepath, weightpath, maskpath, nodb=False )

        yield seeing, skysig, medsky

        for suffix in [ '.cat', '.psf', '.psf.xml' ]:
            ( imagepath.parent / f'{basename}{suffix}' ).unlink( missing_ok=True )
            expsource.delete_file_on_archive( f"{basename}{suffix}" )

    # This fixture, after running imagepath and photocalib, loads the image
    # into the database.
    @pytest.fixture( scope='class' )
    def loaded_imagepath( self, expsource, imagepath, photocalib ):
        # image should not exist yet
        dbo = db.DB.get()
        basename = expsource.image_basename( imagepath.name )
        imgobj = db.Image.get_by_basename( basename, curdb=dbo )
        assert imgobj is None

        loadedimgobj = expsource.create_or_load_image( imagepath, curdb=dbo )
        assert loadedimgobj is not None
        # Since I photocalibed before loading, I need to tell the database that
        loadedimgobj.isphotom = True
        dbo.db.commit()

        imgobj = db.Image.get_by_basename( basename, curdb=dbo )

        yield imgobj

        dbo.db.delete( imgobj.exposure )
        dbo.db.delete( imgobj )
        dbo.db.commit()
        # TODO -- update the connector so it doesn't thrown an exception if the
        #   file is missing (and thus it can't be deleted )
        for suffix in [ ".fits", ".weight.fits", ".mask.fits" ]:
            try:
                expsource.delete_file_on_archive( f"{basename}{suffix}" )
            except Exception as e:
                if str(e)[0:16] == "Failed to delete":
                    pass
                else:
                    raise e

    @pytest.fixture( scope='class' )
    def loaded_imagepath2_nophotocalib( self, expsource, imagepath2 ):
        dbo = db.DB.get()
        basename = expsource.image_basename( imagepath2.name )
        imgobj = db.Image.get_by_basename( basename, curdb=dbo )
        assert imgobj is None

        loadedimgobj = expsource.create_or_load_image( imagepath2, curdb=dbo )
        assert loadedimgobj is not None
        yield loadedimgobj

        dbo.db.delete( loadedimgobj.exposure )
        dbo.db.delete( loadedimgobj )
        dbo.db.commit()
        for suffix in [ '.fits', '.weight.fits', '.mask.fits' ]:
            try:
                expsource.delete_file_on_archive( f'{basename}{suffix}' )
            except Exception as e:
                if str(e)[0:16] == "Failed to delete":
                    pass
                else:
                    raise e

    @pytest.fixture( scope='class' )
    def cat_psf_sky2_indb( self, expsource, imagepath2 ):
        basename = expsource.image_basename( imagepath2.name )
        maskpath = imagepath2.parent / f'{basename}.mask.fits'
        weightpath = imagepath2.parent / f'{basename}.weight.fits'
        seeing, skysig, medsky = processing.cat_psf_sky( expsource, imagepath2, weightpath, maskpath, nodb=False )

        yield seeing, skysig, medsky

        for suffix in [ '.cat', '.psf', '.psf.xml' ]:
            ( imagepath2.parent / f'{basename}{suffix}' ).unlink( missing_ok=True )
            expsource.delete_file_on_archive( f'{basename}{suffix}' )

    @pytest.fixture( scope='class' )
    def photocalib2_indb( self, expsource, imagepath2, cat_psf_sky2_indb ):
        return expsource.photocalib( imagepath2, savedb=True )

    # This fixture deletes the image, mask, and weight files from the local store
    @pytest.fixture( scope='class' )
    def delete_imagepath( self, expsource, imagepath ):
        basename = expsource.image_basename( imagepath.name )
        maskpath = imagepath.parent / f'{basename}.mask.fits'
        weightpath = imagepath.parent / f'{basename}.weight.fits'

        for f in [ imagepath, maskpath, weightpath ]:
            f.unlink( missing_ok=True )

        return True

    # This fixture deletes the .cat, .psf, and .psf.xml files from the local store
    @pytest.fixture( scope='class' )
    def delete_cat_psf( self, expsource, imagepath ):
        basename = expsource.image_basename( imagepath.name )
        for suffix in [ ".cat", ".psf", ".psf.xml" ]:
            ( imagepath.parent / f"{basename}{suffix}" ).unlink( missing_ok=True )
        return True

    # This fixture makes sure that the .cat, .psf, and .psf.xml files are in
    # the local store.  It will probably fail if the cat_psf_sky_indb
    # fixture hasn't already been run, but I don't want to make that a
    # prerequisite of this fixture since part of the purpose of this fixture
    # is to make sure we can *download* from the archive.  You probably want
    # to run the delete_cat_psf fixture before this one.
    @pytest.fixture( scope='class' )
    def ensure_cat_psf_local( self, expsource, imagepath ):
        basename = expsource.image_basename( imagepath.name )
        for suffix in [ ".cat", ".psf", ".psf.xml" ]:
            p = imagepath.parent / f"{basename}{suffix}"
            if not p.is_file():
                expsource.get_file_from_archive( p )
        return True

    # This fixture makes sure that the image, mask, and weight files are in
    # the local store.  Since it does have loaded_imagepath as a
    # prerequisite fixture, we know that the image is loaded into the
    # database.  If you run the delete_imagepath fixture before this one,
    # then this one will test pulling down from the archive.
    @pytest.fixture( scope='class' )
    def ensure_image_local( self, expsource, imagepath, loaded_imagepath ):
        basename = expsource.image_basename( imagepath.name )
        expsource.ensure_sourcefiles_local( basename )
        return True

    @pytest.fixture( scope='class' )
    def ensure_image_local_after_delete( self, expsource, imagepath, loaded_imagepath, delete_imagepath ):
        basename = expsource.image_basename( imagepath.name )
        expsource.ensure_sourcefiles_local( basename )
        return True
        
    
    # End of fixtures
    # ======================================================================


    def test_exposuresources_exist( self, database ):
        assert True
        them = database.db.query( db.ExposureSource ).all()
        assert len(them) > 0

    def test_basics( self, expsource ):

        for attribute, value in self.basicstotest.items():
            if attribute in self.basicabs:
                assert getattr( expsource, attribute ) == pytest.approx( value, abs=self.basicabs[attribute] )
            else:
                assert getattr( expsource, attribute ) == value

        kkw = set( expsource.key_keywords.split(',') )
        assert kkw == self.basickeykeywords

        assert expsource.topdirname ==  self.basic_topdirname
        assert expsource.archivesubdir( self.basic_imagename ) == self.basic_archivesubdir
        assert expsource.relpath( self.basic_imagename ) == self.basic_relpath
        assert expsource.exposure_basename( self.basic_expname ) == self.basic_exposure_basename
        assert expsource.image_basename( self.basic_imagename ) == self.basic_image_basename
        assert expsource.chiptag( self.basic_imagename ) == self.basic_chiptag
        assert expsource.yyyymmdd( self.basic_imagename ) == self.basic_yyyymmdd
        assert expsource.isstack( self.basic_imagename ) == self.basic_isstack
        assert expsource.filetype( self.basic_imagename ) == 'image'
        assert expsource.filetype( self.basic_maskname ) == 'mask'
        assert expsource.filetype( self.basic_weightname ) == 'weight'

    def test_find_images( self, expsource, blob_and_dexen ):
        blob, dex1, dex2, dex3, dex4 = blob_and_dexen

        assert ( len(blob) > self.minnumfound ) and ( len(blob) < self.maxnumfound )
        assert dex1 >= 0, f"Couldn't find image with mjd {self.approxexposure1mjd}"
        assert dex2 >= 0, f"Couldn't find image with mjd {self.approxexposure2mjd}"
        assert dex3 >= 0, f"Couldn't find image with mjd {self.approxexposure3mjd}"
        assert dex4 >= 0, f"Couldn't find image with mjd {self.approxexposure4mjd}"
        assert expsource.blob_image_filter( blob, dex1 ) == self.found1filter
        assert expsource.blob_image_exptime( blob, dex1 ) == self.found1exptime
        assert expsource.blob_image_filter( blob, dex2 ) == self.found2filter
        assert expsource.blob_image_exptime( blob, dex2 ) == self.found2exptime

    def test_download_image_and_make_weight( self, expsource, imagepath ):
        assert imagepath is not None

        basename = expsource.image_basename( imagepath.name )
        maskname = f'{basename}.mask.fits'
        maskpath = expsource.local_path( maskname )
        weightname = f'{basename}.weight.fits'
        weightpath = expsource.local_path( weightname )

        with fits.open( imagepath, memmap=False ) as img:

            if self.check_no_lmt_mg:
                with pytest.raises( KeyError ):
                    val = img[0].header['LMT_MG']

            for key, val in self.origimg_header_checks.items():
                assert img[0].header[key] == val

            assert img[0].data[self.x0:self.x1, self.y0:self.y1].mean() == self.origimg_mean
            assert img[0].data[self.x0:self.x1, self.y0:self.y1].std() == self.origimg_std

        with fits.open( maskpath, memmap=False ) as msk:
            assert msk[0].data.min() == 0
            assert msk[0].data.max() == 1
            assert msk[0].data.sum() == self.origimg_masksum

        with fits.open( weightpath, memmap=False ) as wgt:
            assert wgt[0].data[self.x0:self.x1, self.y0:self.y1].mean() == self.origimg_weightmean
            assert wgt[0].data[self.x0:self.x1, self.y0:self.y1].std() == self.origimg_weightstd

    def test_various_header_info( self, expsource, imagepath ):
        assert expsource.gain( imagepath ) == self.hdr_gain
        assert expsource.readnoise( imagepath ) == self.hdr_readnoise
        assert expsource.darkcurrent( imagepath ) == self.hdr_darkcurrent
        assert expsource.exptime( imagepath ) == self.hdr_exptime
        assert expsource.filter( imagepath ) == self.hdr_expsource_filter
        assert expsource.mjd( imagepath ) == pytest.approx( self.approxexposure1mjd, abs=1e-4 )
        assert expsource.guess_seeing( imagepath ) == self.hdr_guess_seeing

    @pytest.mark.xfail( reason="I need to figure out the whole up/down left/right thing" )
    def test_north_up_east_left( self, expsource, imagepath ):
        with fits.open( imagepath, memmap=False ) as img:
            data = img[0].data
        rotdata = expsource.north_up_east_left( data )
        assert data[:100,:100].mean() == pytest.approx( rotdata[:100,-100:].mean() )

    def test_cat_psf_sky( self, expsource, imagepath, cat_psf_sky ):
        basename = expsource.image_basename( imagepath.name )
        seeing, skysig, medsky = cat_psf_sky
        assert seeing == self.cat_seeing
        assert skysig == self.cat_skysig
        assert medsky == self.cat_medsky

        assert ( imagepath.parent / f"{basename}.psf" ).is_file()
        catpath = imagepath.parent / f"{basename}.cat"
        assert catpath.is_file()

        with fits.open( catpath, memmap="False" ) as catfp:
            cat = catfp[2].data
        cat = cat[ cat['IMAFLAGS_ISO'] == 0 ]

        # I don't completely trust sextractor to give me exactly the
        # same set of objects every time, so checking that the catalog
        # is right is hard.  For now, just do some basic tests.
        assert len(cat) > self.cat_minobjs
        assert len(cat) < self.cat_maxgoodobjs
        assert ( cat['FLUX_APER'] / cat['FLUXERR_APER'] ).mean() > self.cat_minmeansnr

        # Also check the side effects to the header
        with fits.open( imagepath, memmap="False" ) as ifp:
            assert ifp[0].header['SEEING'] == pytest.approx( seeing, abs=0.001 )
            assert ifp[0].header['SKYSIG'] == pytest.approx( skysig, abs=0.01 )
            assert ifp[0].header['MEDSKY'] == pytest.approx( medsky, abs=0.01 )

    def test_photocalib( self, expsource, imagepath, cat_psf_sky, photocalib ):
        # Photocalib for EXPSOURCE is currently pathetic
        magzp = photocalib
        with fits.open( imagepath, memmap=False ) as img:
            assert img[0].header['MAGZP'] == self.photocalib_magzp
            assert img[0].header['MAGZPERR'] == self.photocalib_magzpunc
            assert img[0].header['LMT_MG'] == self.photocalib_lmtmg


    def test_create_or_load_image( self, expsource, loaded_imagepath ):
        imgobj = loaded_imagepath
        assert imgobj.exposure.mjd == pytest.approx( self.approxexposure1mjd, abs=1e-4 )
        assert imgobj.exposure.gallat == self.loaded_expgallat
        assert imgobj.exposure.gallong == self.loaded_expgallong
        assert imgobj.exposure.ra == self.loaded_expra
        assert imgobj.exposure.dec == self.loaded_expdec
        assert imgobj.exposure.band.filtercode == self.loaded_band_filtercode
        assert imgobj.exposure.band.name == self.loaded_band_name

        assert imgobj.isskysub == expsource._dbinfo.skysub
        assert imgobj.isastrom == False
        assert imgobj.isphotom == True
        assert imgobj.hascat == False
        assert imgobj.ra == self.loaded_ra
        assert imgobj.dec == self.loaded_dec
        assert imgobj.band.name == self.loaded_band_name
        assert imgobj.magzp == self.loaded_magzp
        assert imgobj.seeing == self.cat_seeing
        assert imgobj.skysig == self.cat_skysig

    # Much of this test is redundant with test_cat_psf_sky; common
    # functionality should be extracted to a common function
    #
    # Note that one effect of the cat_psf_sky_indb fixture is uploading
    # the .cat, .psf, and .psfxml files to the archive.  This means that
    # this test also effectively tests upload_file_to_archive
    # (as, ideally, an exeption will be raised if that fails).
    def test_indb_cat_psf_sky( self, expsource, imagepath, cat_psf_sky_indb ):
        basename = expsource.image_basename( imagepath.name )

        with db.DB.get() as dbo:
            imgobj = db.Image.get_by_basename( basename, curdb=dbo )
            assert imgobj.hascat == True

            seeing, skysig, medsky = cat_psf_sky_indb
            assert seeing == self.cat_seeing
            assert skysig == self.cat_skysig
            assert medsky == self.cat_medsky

            assert ( imagepath.parent / f"{basename}.psf" ).is_file()
            catpath = imagepath.parent / f"{basename}.cat"
            assert catpath.is_file()

            with fits.open( catpath, memmap="False" ) as catfp:
                cat = catfp[2].data
            cat = cat[ cat['IMAFLAGS_ISO'] == 0 ]

            # I don't completely trust sextractor to give me exactly the
            # same set of objects every time, so checking that the catalog
            # is right is hard.  For now, just do some basic tests.
            assert len(cat) > self.cat_minobjs
            assert ( cat['FLUX_APER'] / cat['FLUXERR_APER'] ).mean() > self.cat_minmeansnr

            # Also check the side effects to the header
            with fits.open( imagepath, memmap="False" ) as ifp:
                assert ifp[0].header['SEEING'] == pytest.approx( seeing, abs=0.005 )
                assert ifp[0].header['SKYSIG'] == pytest.approx( skysig, abs=0.05 )
                assert ifp[0].header['MEDSKY'] == pytest.approx( medsky, abs=0.05 )

    @pytest.mark.mpi(min_size=3)
    def test_parallel_cat_psf_sky( self, expsource, imagepath3, imagepath4 ):
        basename3 = expsource.image_basename( imagepath3.name )
        maskpath3 = imagepath3.parent / f'{basename3}.mask.fits'
        weightpath3 = imagepath3.parent / f'{basename3}.weight.fits'
        basename4 = expsource.image_basename( imagepath4.name )
        maskpath4 = imagepath4.parent / f'{basename4}.mask.fits'
        weightpath4 = imagepath4.parent / f'{basename4}.weight.fits'

        seeings, skysigs, medskys = processing.parallel_cat_psf_sky( expsource,
                                                                     [imagepath3, imagepath4],
                                                                     [weightpath3, weightpath4],
                                                                     [maskpath3, maskpath4],
                                                                     nodb=True )
        assert False, f"seeings={seeings}, skysigs={skysigs}, medskys={medskys}"

    def test_ensure_image_local( self, expsource, imagepath, ensure_image_local ):
        expsource.ensure_image_local( expsource.image_basename( imagepath.name ) )

    # I just do this so that I know that test_get_file_from_archive really
    # must pull from the archive.  The fixtures here are starting to get
    # a bit out of hand....
    def test_make_sure_delete_cat_psf_worked( self, expsource, imagepath, cat_psf_sky_indb, delete_cat_psf ):
        basename = expsource.image_basename( imagepath.name )
        for suffix in [ ".cat", ".psf", ".psf.xml" ]:
            assert expsource.local_path( f'{basename}{suffix}' ) is None

    def test_get_file_from_archive( self, expsource, imagepath, cat_psf_sky_indb,
                                    delete_cat_psf, ensure_cat_psf_local ):
        basename = expsource.image_basename( imagepath.name )
        for suffix in [ ".cat", ".psf", ".psf.xml" ]:
            p = expsource.local_path( f'{basename}{suffix}' )
            assert p is not None
            assert p.is_file()

    # Another "make sure the delete fixture worked"
    def test_make_sure_delete_imagepath_worked( self, expsource, imagepath, delete_imagepath ):
        basename = expsource.image_basename( imagepath.name )
        for suffix in [ ".fits", ".weight.fits", ".mask.fits" ]:
            assert expsource.local_path( f'{basename}{suffix}' ) is None

    def test_ensure_sourcefiles_local( self, expsource, imagepath, loaded_imagepath, ensure_image_local_after_delete ):
        basename = expsource.image_basename( imagepath.name )

        maskname = f'{basename}.mask.fits'
        maskpath = expsource.local_path( maskname )
        weightname = f'{basename}.weight.fits'
        weightpath = expsource.local_path( weightname )

        with fits.open( imagepath, memmap=False ) as img:
            assert img[0].header['FILTER'] == self.hdr_filter
            assert img[0].header['MAGZP'] ==self.loaded_magzp
            assert img[0].header['SEEING'] == self.cat_seeing
            assert img[0].header['SKYSIG'] == self.cat_skysig
            assert img[0].data[self.x0:self.x1, self.y0:self.y1].mean() == self.origimg_mean
            assert img[0].data[self.x0:self.x1, self.y0:self.y1].std() == self.origimg_std

        with fits.open( maskpath, memmap=False ) as msk:
            assert msk[0].data.min() == 0
            assert msk[0].data.max() == 1
            assert msk[0].data.sum() == self.origimg_masksum

        with fits.open( weightpath, memmap=False ) as wgt:
            assert wgt[0].data[self.x0:self.x1, self.y0:self.y1].mean() == self.origimg_weightmean
            assert wgt[0].data[self.x0:self.x1, self.y0:self.y1].std() == self.origimg_weightstd

    def test_load_image2_no_photocalib( self, database, expsource, loaded_imagepath2_nophotocalib ):
        imgobj = loaded_imagepath2_nophotocalib
        assert imgobj is not None
        assert not imgobj.isphotom

    def test_photocalib_indb( self, expsource, database, loaded_imagepath2_nophotocalib, photocalib2_indb ):
        imgobj = db.Image.get_by_basename( loaded_imagepath2_nophotocalib.basename, curdb=database )
        assert imgobj.isphotom
        assert imgobj.magzp == self.loaded2_magzp
        assert imgobj.limiting_mag == self.loaded2_lmtmg
        assert imgobj.seeing == self.loaded2_seeing
        assert imgobj.medsky == self.loaded2_medsky
        with fits.open( expsource.local_path_from_basename( imgobj.basename ) ) as hdu:
            assert hdu[0].header['MAGZP'] == self.loaded2_magzp
            assert hdu[0].header['LMT_MG'] == self.loaded2_lmtmg
            assert hdu[0].header['MEDSKY'] == self.loaded2_medsky
            assert hdu[0].header['SEEING'] == self.loaded2_seeing

    # Let this be tested as a side effect of test_expsource_make_reference
    # @pytest.mark.skip( 'Test not implemented' )
    # def test_create_or_load_stack( self, expsource ):
    #     pass

    # Going to have a separate test_expsource_make_reference
    # @pytest.mark.skip( 'Test not implemented' )
    # def test_secure_references( self, expsource ):
    #     pass


