import math
import sys
import pathlib
import pytest

import numpy
import numpy.random

import astropy.wcs
from astropy.io import fits

import db
import photometry
import psfphot_psfex

from photometrytest import PhotometryTest
from manual_image_fixtures import manual_expsource, load_manual_image

class TestPsfphotPSFEx(PhotometryTest):
    @pytest.fixture(scope='class')
    def photor( self, manual_expsource, loaded_manual_image ):
        return photometry.Photomotor.get( 'psfphot2', manual_expsource, loaded_manual_image )

    def test_single_norecenter( self, photor ):
        origx = 794
        origy = 824
        phot, newx, newy, covar, info = photor.measure( origx, origy )
        assert phot == pytest.approx( 8499, abs=1. )
        assert newx == origx
        assert newy == origy
        assert numpy.sqrt( covar[0][0] ) == pytest.approx( 98.5, abs=0.5 )
        for i in range( 3 ):
            for j in range( 3 ):
                if ( i != 0 ) and ( j != 0 ):
                    assert covar[i][j] == 0.

    def test_multiple_norecenter( self, photor ):
        origx = [ 794, 899, 1988 ]
        origy = [ 824, 792, 363 ]
        phot,  newx, newy, covar, info = photor.measure( origx, origy )
        assert phot == pytest.approx( numpy.array( [ 8499., 23405., 26836. ] ), abs=1. )
        for i, dphot in enumerate( [ 98.5, 128.5, 134.2 ] ):
            assert numpy.sqrt( covar[ i, 0, 0] ) == pytest.approx( dphot, abs=0.5 )
            for j in range( 3 ):
                for k in range( 3 ):
                    if ( j != 0 ) and ( k != 0 ):
                        assert covar[ i, j, k ] == 0.
        assert ( newx == numpy.array( origx ) ).all()
        assert ( newy == numpy.array( origy ) ).all()

    def test_single_recenter( self, photor ):
        origx = 795
        origy = 824
        phot, newx, newy, covar, info = photor.measure( origx, origy, recenter=True )
        assert phot == pytest.approx( 8768., abs=1. )
        assert newx == pytest.approx( 793.6, abs=0.1 )
        assert newy == pytest.approx( 824.1, abs=0.1 )
        expected_covar = numpy.array( [ [  9.869e+03, -3.588e-03, -1.239e-03 ],
                                        [ -3.588e-03,  4.825e-04, -5.994e-06 ],
                                        [ -1.240e-03, -5.994e-06, 4.599e-04 ] ] )
        abs_covar = numpy.array( [ [ 0.01e+03, 0.01e-03, 0.01e-03 ],
                                   [ 0.01e-03, 0.01e-04, 0.01e-06 ],
                                   [ 0.01e-03, 0.01e-06, 0.01e-04 ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) < abs_covar )

    def test_multiple_recenter( self, photor ):
        origx = [ 793, 900, 1988 ]
        origy = [ 824, 793, 365 ]
        phot, newx, newy, covar, info = photor.measure( origx, origy, recenter=True )
        # I'm distressed that the first aperture isn't returning the same thing as in
        #   test_single_recenter; think about this
        assert phot == pytest.approx( numpy.array( [ 8767., 25296., 26923. ] ), abs=1. )
        assert newx == pytest.approx( numpy.array( [ 793.6, 898.9, 1988.0 ] ), abs=0.1 )
        assert newy == pytest.approx( numpy.array( [ 824.1, 792.6, 363.0 ] ), abs=0.1 )
        expected_covar = numpy.array( [[[ 9.864e+03, -7.874e-03, -1.119e-03],
                                        [-7.874e-03,  4.825e-04, -6.001e-06],
                                        [-1.119e-03, -6.001e-06,  4.598e-04]],

                                       [[17.421e+03, -2.550e-03, -3.819e-03],
                                        [-2.550e-03,  8.933e-05, -1.240e-06],
                                        [-3.819e-03, -1.240e-06,  8.727e-05]],

                                       [[18.140e+03, -1.274e-03, -4.285e-03],
                                        [-1.274e-03,  7.527e-05,  3.429e-07],
                                        [-4.285e-03,  3.429e-07,  7.647e-05]]] )
        abs_covar = numpy.array( [ [ 1., 1e-5, 1e-5 ],
                                   [ 1e-5, 1e-6, 1e-8 ],
                                   [ 1e-5, 1e-8, 1e-6 ] ] )
        assert numpy.all( numpy.fabs( covar - expected_covar ) <= abs_covar[numpy.newaxis, :, :] )
