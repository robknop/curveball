import pytest
import sys

_it = 1

@pytest.fixture(scope='module')
def fixture1():
    global _it
    val = _it
    _it += 1
    return str(val)

@pytest.fixture(scope='module')
def fixture2( fixture1 ):
    return f"{fixture1}_a"

class TestPytest:
    def test_one( self, fixture1 ):
        assert fixture1 == "1"

    def test_two( self, fixture1, fixture2 ):
        assert fixture1 == "1"
        assert fixture2 == "1_a"
    
