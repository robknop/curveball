# ABOUT THIS FIXTURE (well, these fixtures)
#
# This does not create a database from whole cloth.  Because curveball
# is written assuming Postgres, before you can even run tests, you must
# have a functioning postgres server out there with a database already
# created (and that has the Q3C extension on it).  The database will be
# completely wiped out and recreated as part of the test fixture setup.
# Set up the config file curveball_testdatabase.yaml in this directory
# to match that database.
#
# You must also already have created the data directory that's listed as
# "datadirs" in curveball_testdatabase.yaml (as seen inside the docker
# image where you're running); the tests expect that directory to start
# empty (well, it can have subdirectories, but no files).
#
# There is also a cfg fixture that is a config to go with the test database.

import sys
import os
import time
import pathlib
import logging
import subprocess
from mpi4py import MPI
import pytest
import pandas
import psycopg2.sql
import psycopg2.extras

_rundir = pathlib.Path(__file__).parent
if not str(_rundir.parent) in sys.path:
    sys.path.insert( 0, str(_rundir.parent / "bin") )
import config
import db
import exposuresource
from archive import Archive

def getconfigenv():
    return "pytest/curveball_testdatabase"

def getconfig():
    return config.Config.get( "./curveball_testdatabase.yaml" )

def scorched_earth( cfg ):
    dbo = db.DB.get( cfg=cfg )
    con = dbo.db.connection().engine.raw_connection()
    cursor = con.cursor( cursor_factory=psycopg2.extras.RealDictCursor )
    cursor.execute( "SELECT * FROM information_schema.tables WHERE table_schema='public'" )
    rows = cursor.fetchall()
    for row in rows:
        cursor.execute( psycopg2.sql.SQL( "DROP TABLE {table} CASCADE" )
                        .format( table=psycopg2.sql.Identifier( row['table_name'] ) ) )
    cursor.close()
    con.commit()

@pytest.fixture(scope="session", autouse=True)
def cfg():
    return getconfig()

# Have to think about MPI in this fixture because it
#  will get called by the MPI test in
#  base_test_an_exposure_source.py
# ...but it doesn't work.  When I try to
# run this under mpi, the database never
# gets created.  Dunno.
@pytest.fixture(scope="session")
def database( cfg ):
    if MPI.COMM_WORLD.Get_rank() == 0:
        scorched_earth( cfg )
        env = os.environ.copy()
        env[ 'CURVEBALL_CONFIG' ] = getconfigenv()
        subprocess.run( [ 'alembic', 'upgrade', 'head' ], cwd='..', env=env )
        subprocess.run( [ 'python3', 'standard_defines.py' ], cwd='..', env=env )
    # Make sure all ranks are synced; don't want a non-0
    #  rank trying to read tables before they exist.
    MPI.COMM_WORLD.Barrier()
    dbo = db.DB.get( cfg=cfg )
    # return dbo
    yield dbo
    dbo.close()
    if MPI.COMM_WORLD.Get_rank() == 0:
        scorched_earth( cfg )
    MPI.COMM_WORLD.Barrier()

# A little bit ugly.  I put in the autouse
#  so that the archive_singlet would really
#  be None when this is run, since some
#  of the other tests use an archive.
@pytest.fixture(scope='session', autouse=True)
def archive( cfg ):
    assert exposuresource.ExposureSource.archive_singlet is None
    arch = Archive( archive_url=cfg.value( 'archive.uploadurl' ),
                    path_base=cfg.value( 'archive.prefix' ),
                    token=cfg.value( 'archive.token' ),
                    verify_cert=cfg.value( 'archive.verify_cert' ),
                    local_read_dir=cfg.value( 'archive.read_dir' ),
                    local_write_dir=cfg.value( 'archive.write_dir' ),
                    track_uploads=True )
    exposuresource.ExposureSource.archive_singlet = arch
    yield arch
    for upload in arch.all_uploads:
        arch.delete( upload, okifmissing=True )



# ======================================================================
# Mostly, you don't see the logging when running tests, unless you give
# -s.  Lots of my routines default to the "main" logger, so put this in
# here to get debug output with pytest -s.

@pytest.fixture(autouse=True,scope='session')
def gratuitous_default_logger():
    logger = logging.getLogger("main")
    logout = logging.StreamHandler( sys.stderr )
    logger.addHandler( logout )
    formatter = logging.Formatter( f'[%(asctime)s - %(levelname)s] - %(message)s',  datefmt='%Y-%m-%D %H:%M:%S' )
    logout.setFormatter( formatter )
    logger.setLevel( logging.DEBUG )
    return logger

