import pathlib
import pytest

import numpy
import numpy.random
from astropy.io import fits

import psffit
import psfexreader

rundir = pathlib.Path( __file__ ).parent
fodder = rundir.parent / "cc_code/cctest/fodder"

class TestPSFFit:
    x = 50.25
    y = 1700.66
    stampsize = 25
    flux = 2000
    x0 = 45.
    σx0 = 1.
    y0 = 1703.66
    σy0 = 1.
    flux0 = 300.
    σflux0 = 20.
    clip0x = 38
    clip0y = 1688
    skynoise = 1

    steps = 200
    burnsteps = 200
    nwalkers = 20

    expected_mcmc_param = [ 1910., 50.14, 1700.76 ]
    expected_mcmc_covar = numpy.array( [ [ 2.134e+03, 5.502e-01, 1.520e-02 ],
                                         [ 5.502e-01, 4.779e-03, 2.803e-04 ],
                                         [ 1.520e-02, 2.803e-04, 5.569e-03 ] ] )

    expected_lm_param = [ 1920., 50.14, 1700.75 ]
    expected_lm_covar = numpy.array( [ [ 2.215e+03, 9.658e-02, -7.726e-02 ],
                                       [ 9.658e-02, 5.039e-03, 9.892e-05 ],
                                       [ -7.726e-02, 9.892e-05, 6.518e-03 ] ] )

    expected_lm_fixed_flux = 1919.62
    expected_lm_fixed_var = 2216.14

    # If you're worried about the covariance off-diagonals in the lm
    # case being smaller in the lm case than in the mcmc case, see the
    # comment in cc_code/cctest/testpsffit.cc

    @pytest.fixture( scope="class" )
    def psffile( self ):
        return str( fodder / "testpsfexreader.psf" )

    @pytest.fixture( scope="class" )
    def psf( self, psffile ):
        return psfexreader.PSFExReader( psffile )

    @pytest.fixture( scope="class" )
    def stamp_and_weight( self, psf ):
        clip = psf.getclip( TestPSFFit.x, TestPSFFit.y, 1,
                            clipx0=TestPSFFit.clip0x, clipy0=TestPSFFit.clip0y, clipwid=TestPSFFit.stampsize )
        assert clip.shape[0] == clip.shape[1]
        assert clip.shape[0] == TestPSFFit.stampsize

        # Noise it up
        rng = numpy.random.default_rng( 23 )
        clip *= TestPSFFit.flux
        clipwt = numpy.full_like( clip, TestPSFFit.skynoise ** 2 )
        clipwt[ clip > 0 ] += clip[ clip > 0 ]
        clip += rng.normal( 0, numpy.sqrt(clipwt), clip.shape )
        clipwt = 1. / clipwt

        return ( clip, clipwt )

    def test_psffit_mcmc( self, psffile, stamp_and_weight, psf ):
        clip, clipwt = stamp_and_weight
        param, covar = psffit.psffit( clip, clipwt, psffile, TestPSFFit.clip0x, TestPSFFit.clip0y,
                                      TestPSFFit.flux0, TestPSFFit.x0, TestPSFFit.y0,
                                      fixpos=False, method='mcmc',
                                      sigflux=TestPSFFit.σflux0, sigx=TestPSFFit.σx0, sigy=TestPSFFit.σy0,
                                      steps=TestPSFFit.steps, burnsteps=TestPSFFit.burnsteps,
                                      nwalkers=TestPSFFit.nwalkers, stumblerseed=23, filebase="" )

        fit = psf.getclip( param[1], param[2], param[0],
                           clipx0=TestPSFFit.clip0x, clipy0=TestPSFFit.clip0y, clipwid=TestPSFFit.stampsize )
        resid = clip - fit
        chisq = ( ( resid**2 * clipwt ) / ( clip.shape[0] * clip.shape[1] - 3 ) ).sum()

        # fits.writeto( "psffit_mcmc_data.fits", clip, overwrite=True )
        # fits.writeto( "psffit_mcmc_weight.fits", clipwt, overwrite=True )
        # fits.writeto( "psffit_mcmc_fit.fits", fit, overwrite=True )
        # fits.writeto( "psffit_mcmc_resid.fits", resid, overwrite=True )

        assert param[0] == pytest.approx( TestPSFFit.expected_mcmc_param[0], abs=1. )
        assert param[1] == pytest.approx( TestPSFFit.expected_mcmc_param[1], abs=0.01 )
        assert param[2] == pytest.approx( TestPSFFit.expected_mcmc_param[2], abs=0.01 )
        assert ( numpy.fabs( ( covar - TestPSFFit.expected_mcmc_covar )
                             / TestPSFFit.expected_mcmc_covar ) < 0.01 ).all()
        assert ( chisq > 0.9 ) and ( chisq < 1.1 )

    def test_psffit_lm( self, psffile, stamp_and_weight, psf ):
        clip, clipwt = stamp_and_weight
        param, covar = psffit.psffit( clip, clipwt, psffile, TestPSFFit.clip0x, TestPSFFit.clip0y,
                                      TestPSFFit.flux0, TestPSFFit.x0, TestPSFFit.y0,
                                      fixpos=False, method='lm' )

        fit = psf.getclip( param[1], param[2], param[0],
                           clipx0=TestPSFFit.clip0x, clipy0=TestPSFFit.clip0y, clipwid=TestPSFFit.stampsize )
        resid = clip - fit
        chisq = ( ( resid**2 * clipwt ) / ( clip.shape[0] * clip.shape[1] - 3 ) ).sum()

        # fits.writeto( "psffit_lm_data.fits", clip, overwrite=True )
        # fits.writeto( "psffit_lm_weight.fits", clipwt, overwrite=True )
        # fits.writeto( "psffit_lm_fit.fits", fit, overwrite=True )
        # fits.writeto( "psffit_lm_resid.fits", resid, overwrite=True )

        assert param[0] == pytest.approx( TestPSFFit.expected_lm_param[0], abs=1. )
        assert param[1] == pytest.approx( TestPSFFit.expected_lm_param[1], abs=0.01 )
        assert param[2] == pytest.approx( TestPSFFit.expected_lm_param[2], abs=0.01 )
        assert ( numpy.fabs( ( covar - TestPSFFit.expected_lm_covar )
                             / TestPSFFit.expected_lm_covar ) < 0.01 ).all()
        assert ( chisq > 0.9 ) and ( chisq < 1.1 )

    def test_psffit_lm_fixpos( self, psffile, stamp_and_weight, psf ):
        clip, clipwt = stamp_and_weight
        param, covar = psffit.psffit( clip, clipwt, psffile, TestPSFFit.clip0x, TestPSFFit.clip0y,
                                      TestPSFFit.flux0, TestPSFFit.x, TestPSFFit.y,
                                      fixpos=True, method='lm' )

        fit = psf.getclip( param[1], param[2], param[0],
                           clipx0=TestPSFFit.clip0x, clipy0=TestPSFFit.clip0y, clipwid=TestPSFFit.stampsize )
        resid = clip - fit
        chisq = ( ( resid**2 * clipwt ) / ( clip.shape[0] * clip.shape[1] - 3 ) ).sum()

        # fits.writeto( "psffit_lm_fixpos_data.fits", clip, overwrite=True )
        # fits.writeto( "psffit_lm_fixpos_weight.fits", clipwt, overwrite=True )
        # fits.writeto( "psffit_lm_fixpos_fit.fits", fit, overwrite=True )
        # fits.writeto( "psffit_lm_fixpos_resid.fits", resid, overwrite=True )

        assert param[0] == pytest.approx( TestPSFFit.expected_lm_fixed_flux, abs=1. )
        assert param[1] == pytest.approx( TestPSFFit.x, abs=1e-8 )
        assert param[2] == pytest.approx( TestPSFFit.y, abs=1e-8 )
        assert covar[0][0] == pytest.approx( TestPSFFit.expected_lm_fixed_var, abs=1. )
        for i in range(3):
            for j in range(3):
                if ( i == 0 ) and ( j == 0 ): continue
                assert covar[i][j] == pytest.approx( 0., 1e-8 )

