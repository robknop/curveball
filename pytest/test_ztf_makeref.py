import os
import pytest

from astropy.io import fits

import db
import make_reference

from fixtures_ztf import ztf

class TestZtfMakeref:
    @pytest.fixture(scope='class')
    def ztf18aarwxum( self, database, ztf ):
        obj = db.Object( t0=58693.26, z=0.0846, ra=267.37147, dec=58.56938, name='ZTF18aarwxum' )
        database.db.add( obj )
        database.db.commit()
        yield obj
        database.db.delete( obj )
        database.db.commit()

    # This one takes several minutes to run.
    # (Could be longer if ztf server is logjammed.)
    @pytest.fixture(scope='class')
    def ztf18aarwxum_ref( self, database, ztf, ztf18aarwxum ):
        stackobj, objrefobj = make_reference.make_reference( ztf, 'ZTF_g', obj=ztf18aarwxum, curdb=database )
        yield stackobj

        # THIS IS SCARY
        # Clean up images that were pulled down as part of building the stack.
        # The problem is if that there were some pre-existing images for some
        # reason, they will be deleted here too even though we don't want them to.
        # But, it will be the end of the session, so, whatevs.

        mems = ( database.db.query( db.Image )
                 .join( db.StackMember, db.StackMember.member_id==db.Image.id )
                 .filter( db.StackMember.image_id==stackobj.id ) ).all()

        for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml',
                     '.astrom.cat', '.gaia.cat', '.scamp.xml']:
            bases = [ stackobj.basename ]
            bases.extend( [ mem.basename for mem in mems ] )
            for base in bases:
                ztf.delete_file_on_archive( f'{base}{ext}' )
                lpath = ztf.local_path( f'{base}{ext}' )
                if lpath is not None:
                    lpath.unlink()

        database.db.delete( stackobj )
        # I was getting foreign key conflict errors when I tried
        #  to delete the images below, I'm really not sure why.
        #  Going to try comitting this delete so that it will
        #  cascade to StackMember to see if that eliminates
        #  the conflicts... and, it worked.  Insert usual
        #  comment about ORMs making database stuff harder.
        database.db.commit()
        database.db.delete( objrefobj )
        for m in mems:
            database.db.delete( m )
        database.db.commit()

    @pytest.mark.skipif( os.getenv("OMG_SKIP_SLOW") is not None, reason="OMG_SKIP_SLOW is set" )
    def test_makeref( self, database, ztf, ztf18aarwxum, ztf18aarwxum_ref ):
        expectedmembers = set( [
            'ztf_20190610347801_000797_zg_io.151',
            'ztf_20190606330347_000797_zg_io.151',
            'ztf_20190612328426_000797_zg_io.151',
            'ztf_20190606348553_000797_zg_io.151',
            'ztf_20190606366609_000797_zg_io.151',
            'ztf_20190613361736_000797_zg_io.151',
            'ztf_20190609322951_000797_zg_io.151',
            'ztf_20190613362639_000797_zg_io.151',
            'ztf_20190609343009_000797_zg_io.151',
            'ztf_20190613386771_000797_zg_io.151',
            'ztf_20190610346910_000797_zg_io.151',
        ] )
        
        assert ztf18aarwxum_ref.seeing == pytest.approx( 1.88, abs=0.02 )
        assert ztf18aarwxum_ref.magzp == pytest.approx( 26.20, abs=0.01 )
        assert ztf18aarwxum_ref.isstack
        assert ztf18aarwxum_ref.isphotom
        assert ztf18aarwxum_ref.isastrom
        assert ztf18aarwxum_ref.isskysub
        q = ( database.db.query( db.Image )
              .join( db.StackMember, db.StackMember.member_id==db.Image.id )
              .filter( db.StackMember.image_id==ztf18aarwxum_ref.id ) )
        members = set( [ i.basename for i in q.all() ] )
        assert members == expectedmembers

        with fits.open( ztf.local_path( f'{ztf18aarwxum_ref.basename}.fits' ) ) as hdu:
            assert hdu[0].data[1520:1560, 1780:1850].mean() == pytest.approx( -0.14, abs=0.02 )
            assert hdu[0].data[1520:1560, 1780:1850].std() == pytest.approx( 1.4, abs=0.1 )
        
