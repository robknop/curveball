import random
import pytest
import hashlib
import pathlib
import shutil

from astropy.io import fits
import numpy

from fixtures_ztf import ztf, ztf_image

import exposuresource

class TestArchive:

    def test_upload_download( self, ztf, ztf_image, archive ):
        relpath = ztf.relpath( f'{ztf_image.basename}.fits' )
        assert archive.get_info( relpath ) is None

        # Make sure the file does exist locally
        localfile = ztf.get_file_from_archive( f'{ztf_image.basename}.fits', verifymd5=False )
        assert localfile.is_file()
        md5 = hashlib.md5()
        with open( localfile, "rb" ) as ifp:
            md5.update( ifp.read() )
        
        # Upload
        assert ztf.upload_file_to_archive( f'{ztf_image.basename}.fits', overwrite=False )
        info = archive.get_info( relpath )
        assert info['md5sum'] == md5.hexdigest()
        
        # Delete local
        localfile.unlink()
        with pytest.raises( FileNotFoundError ):
            f = fits.open( localfile )
            f.close()

        # Download
        localfile = ztf.get_file_from_archive( f'{ztf_image.basename}.fits' )
        newmd5 = hashlib.md5()
        with open( localfile, "rb" ) as ifp:
            newmd5.update( ifp.read() )
        assert newmd5.hexdigest() == md5.hexdigest()

        # Delete on server
        assert ztf.delete_file_on_archive( f"{ztf_image.basename}.fits" )
        assert archive.get_info( relpath ) is None
