import math
import pytest
import pathlib
import shutil
import pandas
import sqlalchemy
import sqlalchemy.orm

from astropy.io import fits

import db
import make_reference
import processing
import subtract
import buildltcv
import recenter

from fixtures_ztf import ztf
# from fixtures_ztf_subtract_buildltcv import ztf18aarwxum, ztf18aarwxum_ref, ztf18aarwxum_images
# from fixtures_ztf_subtract_buildltcv import ztf18aarwxum_subtract_image0, ztf18aarwxum_ltcv

class TestZtfSubtract:

    @pytest.fixture(scope='class')
    def ztf18aarwxum( self, database, ztf ):
        obj = db.Object( t0=58693.26, z=0.0846, ra=267.37147, dec=58.56938, name='ZTF18aarwxum' )
        database.db.add( obj )
        database.db.commit()
        yield obj
        database.db.delete( obj )
        database.db.commit()

    # Hack a pre-built reference into the database to make this test run faster
    # than if we actually called make_reference.  (See test_ztf_makeref for a test of that.)
    @pytest.fixture(scope='class')
    def ztf18aarwxum_ref( self, cfg, database, ztf, ztf18aarwxum ):
        base = 'stack_ztf_20190606330347_000797_zg_io.151'
        for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
            source = pathlib.Path( 'fodder' ) / f"{base}{ext}"
            dest = pathlib.Path( cfg.value( 'datadirs' )[0] ) / ztf.relpath( f"{base}{ext}" )
            dest.parent.mkdir( parents=True, exist_ok=True )
            shutil.copy2( source, dest )

        band = db.Band.get_by_name( 'ZTF_g', ztf, curdb=database )
            
        ref = db.Image( exposure_id=None,
                        band_id=band.id,
                        chip_id=None,
                        ra=268.010498436266,
                        dec=58.2295122963904,
                        ra1=268.821,
                        dec1=57.7937,
                        ra2=268.841,
                        dec2=58.6598,
                        ra3=267.18,
                        dec3=58.6598,
                        ra4=267.2,
                        dec4=57.7937,
                        skysig=1.20127,
                        medsky=-0.8904,
                        seeing=1.87593,
                        magzp=26.1967,
                        limiting_mag=22.9457,
                        isstack=True,
                        hasfakes=False,
                        isastrom=True,
                        isphotom=True,
                        isskysub=True,
                        hascat=True,
                        basename=base
                       )
        database.db.add( ref )
        database.db.commit()

        obref = db.ObjectReference( ra=ztf18aarwxum.ra,
                                    dec=ztf18aarwxum.dec,
                                    object_id=ztf18aarwxum.id,
                                    image_id=ref.id,
                                    donotuse=False )
        database.db.add( obref )
        database.db.commit()
        
        yield ref

        database.db.delete( obref )
        database.db.delete( ref )
        database.db.commit()
        for ext in [ '.fits', '.weight.fits', '.mask.fits', '.cat', '.psf', '.psf.xml' ]:
            # This hack didn't save the reference to the archive
            lpath = ztf.local_path( f'{base}{ext}' )
            if lpath is not None:
                lpath.unlink()

    @pytest.fixture(scope='class')
    def ztf18aarwxum_images( self, database, ztf, ztf18aarwxum ):
        # Like in ztf_imageobj, reverse engineering what we'd
        # get as an opaque data structure from find_images
        # so that we don't have to run that
        blob = pandas.DataFrame( [
            {
                'in_row_id': 1,
                'in_ra': 267.3715,
                'in_dec': 58.5694,
                'ra': 268.020218,
                'dec': 58.236669,
                'infobits': 0,
                'field': 797,
                'ccdid': 15,
                'qid': 1,
                'rcid': 56,
                'fid': 1,
                'filtercode': 'zg',
                'pid': 939243995615,
                'nid': 939,
                'expid': 93924399,
                'itid': 1,
                'imgtype': 'object',
                'imgtypecode': 'o',
                'obsdate': '2019-07-29 05:51:21.286+00',
                'obsjd': 2458693.743993,
                'exptime': 30,
                'filefracday': 20190729243993,
                'seeing': 1.77264,
                'airmass': 1.082,
                'moonillf': -0.113528,
                'moonesb': 0,
                'maglimit': 21.0154,
                'crpix1': 1536.5,
                'crpix2': 1540.5,
                'crval1': 268.02022,
                'crval2': 58.236659,
                'cd11': -0.000281,
                'cd12': 0.000003,
                'cd21': -0.000003,
                'cd22': -0.000281,
                'ra1': 268.840382,
                'dec1': 58.672392,
                'ra2': 267.17939,
                'dec2': 58.661942,
                'ra3': 267.2195,
                'dec3': 57.79563,
                'ra4': 268.840918,
                'dec4': 57.806051,
                'ipac_pub_date': '2021-03-31 00:00:00+00',
                'ipac_gid': 2,
                'obsmjd': 58693.243993,
            },
            {
                'in_row_id': 1,
                'in_ra': 267.3715,
                'in_dec': 58.5694,
                'ra': 267.155215,
                'dec': 58.689159,
                'infobits': 0,
                'field': 826,
                'ccdid': 4,
                'qid': 3,
                'rcid': 14,
                'fid': 1,
                'filtercode': 'zg',
                'pid': 958200631415,
                'nid': 958,
                'expid': 95820063,
                'itid': 1,
                'imgtype': 'object',
                'imgtypecode': 'o',
                'obsdate': '2019-08-17 04:48:55.378+00',
                'obsjd': 2458712.700637,
                'exptime': 30,
                'filefracday': 20190817200602,
                'seeing': 1.98736,
                'airmass': 1.145,
                'moonillf': -0.974348,
                'moonesb': 0,
                'maglimit': 20.1563,
                'crpix1': 1536.5,
                'crpix2': 1540.5,
                'crval1': 267.1553,
                'crval2': 58.689201,
                'cd11': -0.00028,
                'cd12': 0.000025,
                'cd21': -0.000025,
                'cd22': -0.00028,
                'ra1': 267.919239,
                'dec1': 59.156903,
                'ra2': 266.242987,
                'dec2': 59.07876,
                'ra3': 266.411892,
                'dec3': 58.217207,
                'ra4': 268.047105,
                'dec4': 58.293198,
                'ipac_pub_date': '2021-03-31 00:00:00+00',
                'ipac_gid': 2,
                'obsmjd': 58712.200637
            }
        ] )

        allwritten = []
        imagefiles = []
        imgobjs = []
        for dex in range( len( blob ) ):
            fname = ztf.blob_image_name( blob, dex )
            writtenfiles = ztf.download_blob_image( blob, dex )
            imagefile = None
            maskfile = None
            for f in writtenfiles:
                if f.name == fname:
                    imagefile = f
                else:
                    maskfile = f
            weightpath = processing.make_weight( ztf, imagefile )
            writtenfiles.append( weightpath )
            allwritten.extend( writtenfiles )
            imagefiles.append( imagefile )

            basename = ztf.image_basename( imagefile.name )
            seeing, skysig, medsky = processing.cat_psf_sky( ztf, imagefile, weightpath, maskfile, nodb=True )
            ztf.photocalib( imagefile )

            imgobj = None
            imgobj = db.Image.get_by_basename( basename, curdb=database )
            assert imgobj is None
            loadedimgobj = ztf.create_or_load_image( imagefile, curdb=database )
            assert loadedimgobj is not None
            loadedimgobj.isphotom = True
            database.db.commit()
            imgobj = db.Image.get_by_basename( ztf.image_basename( imagefile.name ), curdb=database )
            imgobjs.append( imgobj )

        yield imgobjs

        for i in imgobjs:
            database.db.delete( i.exposure )
            database.db.delete( i )
        database.db.commit()

        for i in imagefiles:
            basename = ztf.image_basename( i.name )
            for suffix in [ '.cat', '.psf', '.psf.xml' ]:
                ( i.parent / f'{basename}{suffix}' ).unlink( missing_ok=True )
                ztf.delete_file_on_archive( f'{basename}{suffix}' )

        for f in allwritten:
            f.unlink( missing_ok=True )

    @pytest.fixture(scope='class')
    def ztf18aarwxum_subtract_image0( self, database, ztf, ztf18aarwxum, ztf18aarwxum_ref, ztf18aarwxum_images):
        stor = subtract.Subtractor.get( 'alard1', ztf, ztf18aarwxum_images[0], ztf18aarwxum_ref, curdb=database )
        stor.subtract()
        yield stor

        for which in [ 'remapref', 'remaprefweight', 'remaprefmask', 'remaprefcat', 'sub', 'subweight', 'submask' ]:
            p = stor.sub_filepath( which )
            p.unlink( missing_ok=True )
            ztf.delete_file_on_archive( p.name )


    @pytest.fixture(scope='class')
    def ztf18aarwxum_centering_ltcv( self, database, ztf, ztf18aarwxum, ztf18aarwxum_images ):
        builder = buildltcv.LtcvBuilder( ztf, ztf18aarwxum, 'ZTF_g', subalg='alard1', photmethod='apphot1',
                                         subtags='kaglorky', newsubtags=True,
                                         phottags=['gazorniplotz','centering'], tagphotmethoddefault=False,
                                         tagphotdefault=False, newphottags=True,
                                         recenter=True )
        builder()
        yield builder

        # Cleanup is harder here since I didn't save the subtraction objects, and
        #   because I didn't save which subtraction objects actually had to
        #   be created (and thus weren't cleaned up by another fixture), so overdo it.
        # Note that I'm *not* deleting things from the database, which I probably should
        iids = [ i.id for i in ztf18aarwxum_images ]
        stors = database.db.query( db.Subtraction ).filter( db.Subtraction.image_id.in_( iids ) )

        for im in ztf18aarwxum_images:
            for ext in [ '.astrom.cat', '.gaia.cat', '.scamp.xml' ]:
                p = ztf.local_path( f'{im.basename}{ext}' )
                if p is not None:
                    p.unlink( missing_ok=True )
                    ztf.delete_file_on_archive( p.name )
        for stor in stors:
            for ext in [ '.remapref.cat', '.remapref.fits', '.remapref.mask.fits', '.remapref.weight.fits',
                         '.sub.fits', '.sub.mask.fits',  '.sub.weight.fits' ]:
                p = ztf.local_path( f'{stor.image.basename}.{stor.id}{ext}' )
                if p is not None:
                    p.unlink( missing_ok=True )
                    ztf.delete_file_on_archive( p.name )

    @pytest.fixture(scope='class')
    def ztf18aarwxum_recenter( self, database, ztf, ztf18aarwxum, ztf18aarwxum_centering_ltcv ):
        oldra = ztf18aarwxum.ra
        olddec = ztf18aarwxum.dec
        meanra, meandec = recenter.recenter( ztf18aarwxum, update=True, curdb=database )
        return oldra, olddec, meanra, meandec
                    
    @pytest.fixture(scope='class')
    def ztf18aarwxum_ltcv( self, database, ztf, ztf18aarwxum, ztf18aarwxum_images, ztf18aarwxum_centering_ltcv ):
        builder = buildltcv.LtcvBuilder( ztf, ztf18aarwxum, 'ZTF_g', subalg='alard1', photmethod='apphot1',
                                         subtags='kaglorky', newsubtags=True,
                                         phottags=['gazorniplotz'], newphottags=True )
        builder()
        yield builder

        # Cleanup is harder here since I didn't save the subtraction objects, and
        #   because I didn't save which subtraction objects actually had to
        #   be created (and thus weren't cleaned up by another fixture), so overdo it.
        # Note that I'm *not* deleting things from the database, which I probably should
        iids = [ i.id for i in ztf18aarwxum_images ]
        stors = database.db.query( db.Subtraction ).filter( db.Subtraction.image_id.in_( iids ) )

        for im in ztf18aarwxum_images:
            for ext in [ '.astrom.cat', '.gaia.cat', '.scamp.xml' ]:
                p = ztf.local_path( f'{im.basename}{ext}' )
                if p is not None:
                    p.unlink( missing_ok=True )
                    ztf.delete_file_on_archive( p.name )
        for stor in stors:
            for ext in [ '.remapref.cat', '.remapref.fits', '.remapref.mask.fits', '.remapref.weight.fits',
                         '.sub.fits', '.sub.mask.fits',  '.sub.weight.fits' ]:
                p = ztf.local_path( f'{stor.image.basename}.{stor.id}{ext}' )
                if p is not None:
                    p.unlink( missing_ok=True )
                    ztf.delete_file_on_archive( p.name )


    # ======================================================================
    # ======================================================================
    # ======================================================================
        
    def test_subtract( self, database, ztf, ztf18aarwxum_subtract_image0 ):
        stor = ztf18aarwxum_subtract_image0

        newimg = ztf.local_path( f'{ztf18aarwxum_subtract_image0.image.basename}.fits' )
        newwt = ztf.local_path( f'{ztf18aarwxum_subtract_image0.image.basename}.weight.fits' )
        remaprefimg = stor.sub_filepath( "remapref" )
        remaprefwt = stor.sub_filepath( "remaprefweight" )
        subimg = stor.sub_filepath( "sub" )
        subwt = stor.sub_filepath( "subweight" )

        with fits.open(newimg) as img, fits.open(remaprefimg) as remapref, fits.open(subimg) as sub:
            imgdata = img[0].data
            refdata = remapref[0].data
            subdata = sub[0].data
        with fits.open(newwt) as iw, fits.open(remaprefwt) as rw, fits.open(subwt) as sw:
            imgweight = iw[0].data
            refweight = rw[0].data
            subweight = sw[0].data

        # Check one small patch on the image, and by induction,
        # it means the whole image is perfect
        x0 = 949
        y0 = 1879
        xmin = 947
        xmax = 951
        ymin = 1877
        ymax = 1881
        expectedvals = {
            'new': { 'data': imgdata,
                     'weight': imgweight,
                     'ctr': 315.482,
                     'mean': 157.748,
                     'sum': 2523.979,
                     'std': 59.646,
                     'err': 21.475
                     },
            'ref' : { 'data': refdata,
                      'weight': refweight,
                      'ctr': 219.447,
                      'mean': 63.724,
                      'sum': 1019.808,
                      'std': 65.380,
                      'err': 5.583
                      },
            'sub' : { 'data': subdata,
                      'weight': subweight,
                      'ctr': 29.291,
                      'mean': 2.922,
                      'sum': 46.748,
                      'std': 12.766,
                      'err': 22.201,
                     }
        }
        for img in [ 'new', 'ref', 'sub' ]:
            assert ( expectedvals[img]['data'][x0,y0]
                     == pytest.approx( expectedvals[img]['ctr'], abs=0.1 ) )
            assert ( expectedvals[img]['data'][xmin:xmax,ymin:ymax].mean()
                     == pytest.approx( expectedvals[img]['mean'], abs=0.1 ) )
            assert ( expectedvals[img]['data'][xmin:xmax,ymin:ymax].sum()
                     == pytest.approx( expectedvals[img]['sum'], abs=0.1 ) )
            assert ( expectedvals[img]['data'][xmin:xmax,ymin:ymax].std()
                     == pytest.approx( expectedvals[img]['std'], abs=0.1 ) )
            assert ( math.sqrt( ( 1. / expectedvals[img]['weight'][xmin:xmax,ymin:ymax] ).sum() )
                     == pytest.approx( expectedvals[img]['err'], abs=0.1 ) )
        
    def test_get_existing_subtraction( self, database, ztf, ztf18aarwxum_subtract_image0,
                                       ztf18aarwxum_ref, ztf18aarwxum_images ):
        stor = subtract.Subtractor.get( 'alard1', ztf, ztf18aarwxum_images[0], ztf18aarwxum_ref, curdb=database )
        existingsub = stor._get_existing_subtraction( curdb=database )
        assert stor.subobj is not None
        

    def test_recentering_buildltcv( self, database, ztf, ztf18aarwxum_images, ztf18aarwxum_centering_ltcv ):
        imgids = [ i.id for i in ztf18aarwxum_images ]
        expectedphots = [
            { 'mjd': pytest.approx( 58693.24399, abs=1e-4 ),
              'flux': pytest.approx( 778.17, abs=0.1 ),
              'dflux': pytest.approx( 25.26, abs=0.1 ),
              'mag': pytest.approx( 18.804, abs=0.01 ),
              'dmag': pytest.approx( 0.035, abs=0.01 ),
              'refnorm': pytest.approx( 1.188, abs=0.01 ),
              'imagex': pytest.approx( 2723.73, abs=0.1 ),
              'imagey': pytest.approx( 336.77, abs=0.1 )
             },
            { 'mjd': pytest.approx( 58712.20064, abs=1e-4 ),
              'flux': pytest.approx( 307.86, abs=0.1 ),
              'dflux': pytest.approx( 39.58, abs=0.1 ),
              'mag': pytest.approx( 19.865, abs=0.01 ),
              'dmag': pytest.approx( 0.140, abs=0.01 ),
              'refnorm': pytest.approx( 1.176, abs=0.01 ),
              'imagex': pytest.approx( 1173.74, abs=0.1 ),
              'imagey': pytest.approx( 1998.48, abs=0.1 )
             }
        ]

        gz_phots = list( ( database.db.query( db.Photometry )
                           .filter( db.Photometry.image_id.in_( imgids ) )
                           .join( db.PhotometryVersiontag, db.PhotometryVersiontag.photometry_id==db.Photometry.id )
                           .join( db.VersionTag,
                                  sqlalchemy.and_( db.PhotometryVersiontag.versiontag_id==db.VersionTag.id,
                                                   db.VersionTag.name=='gazorniplotz' ) )
                           .order_by( db.Photometry.mjd ) ).all() )

        c_phots = list( ( database.db.query( db.Photometry )
                          .filter( db.Photometry.image_id.in_( imgids ) )
                          .join( db.PhotometryVersiontag, db.PhotometryVersiontag.photometry_id==db.Photometry.id )
                          .join( db.VersionTag,
                                 sqlalchemy.and_( db.PhotometryVersiontag.versiontag_id==db.VersionTag.id,
                                                  db.VersionTag.name=='centering' ) )
                          .order_by( db.Photometry.mjd ) ).all() )

        d_phots = list( ( database.db.query( db.Photometry )
                          .filter( db.Photometry.image_id.in_( imgids ) )
                          .join( db.PhotometryVersiontag, db.PhotometryVersiontag.photometry_id==db.Photometry.id )
                          .join( db.VersionTag,
                                 sqlalchemy.and_( db.PhotometryVersiontag.versiontag_id==db.VersionTag.id,
                                                  db.VersionTag.name=='default' ) )
                          .order_by( db.Photometry.mjd ) ).all() )

        assert len( d_phots ) == 0
        for gzp, cp in zip( gz_phots, c_phots ):
            assert gzp.mjd == cp.mjd
            assert gzp.flux == cp.flux
            assert gzp.dflux == cp.dflux

        for phot, expected in zip( c_phots, expectedphots ):
            for key, val in expected.items():
                assert getattr(phot, key) == val

    def test_recenter_nosave( self, database, ztf18aarwxum ):
        oldra = ztf18aarwxum.ra
        olddec = ztf18aarwxum.dec
        meanra, meandec = recenter.recenter( ztf18aarwxum )
        assert ztf18aarwxum.ra == oldra
        assert ztf18aarwxum.dec == olddec
        newobj = db.Object.get( ztf18aarwxum.id, curdb=database )
        assert newobj.ra == oldra
        assert newobj.dec == olddec
        assert newobj.ra != meanra
        assert newobj.dec != meandec
                
    def test_recenter( self, ztf18aarwxum, ztf18aarwxum_recenter ):
        oldra, olddec, meanra, meandec = ztf18aarwxum_recenter

        assert ztf18aarwxum.ra == pytest.approx( meanra, abs=1e-7 )
        assert ztf18aarwxum.dec == pytest.approx( meandec, abs=1e-7 )
        assert oldra == pytest.approx( 267.37147, abs=1e-5 )
        assert olddec == pytest.approx( 58.56938, abs=1e-5 )
        assert meanra == pytest.approx( 267.3714313, abs=1e-6 )
        assert meandec == pytest.approx( 58.5693583, abs=1e-6 )

                
    def test_buildltcv( self, database, ztf, ztf18aarwxum_images, ztf18aarwxum_ltcv ):
        imgids = [ i.id for i in ztf18aarwxum_images ]
        # These are a bit different from the ones in
        # test_recentering_buildltcv because the photometry
        # position was forced this time.  As expected,
        # the higher S/N detection moved less.
        expectedphots = [
            { 'mjd': pytest.approx( 58693.24399, abs=1e-4 ),
              'flux': pytest.approx( 778.11, abs=0.1 ),
              'dflux': pytest.approx( 25.31, abs=0.1 ),
              'mag': pytest.approx( 18.804, abs=0.01 ),
              'dmag': pytest.approx( 0.035, abs=0.01 ),
              'refnorm': pytest.approx( 1.188, abs=0.01 ),
              'imagex': pytest.approx( 2723.71, abs=0.1 ),
              'imagey': pytest.approx( 336.75, abs=0.1 )
             },
            { 'mjd': pytest.approx( 58712.20064, abs=1e-4 ),
              'flux': pytest.approx( 289.96, abs=0.1 ),
              'dflux': pytest.approx( 39.94, abs=0.1 ),
              'mag': pytest.approx( 19.931, abs=0.01 ),
              'dmag': pytest.approx( 0.150, abs=0.01 ),
              'refnorm': pytest.approx( 1.176, abs=0.01 ),
              'imagex': pytest.approx( 1174.05, abs=0.1 ),
              'imagey': pytest.approx( 1998.89, abs=0.1 )
             }
        ]
            
        gz_phots = list( ( database.db.query( db.Photometry )
                           .filter( db.Photometry.image_id.in_( imgids ) )
                           .join( db.PhotometryVersiontag, db.PhotometryVersiontag.photometry_id==db.Photometry.id )
                           .join( db.VersionTag,
                                  sqlalchemy.and_( db.PhotometryVersiontag.versiontag_id==db.VersionTag.id,
                                                   db.VersionTag.name=='gazorniplotz' ) )
                           .order_by( db.Photometry.mjd ) ).all() )

        d_phots = list( ( database.db.query( db.Photometry )
                          .filter( db.Photometry.image_id.in_( imgids ) )
                          .join( db.PhotometryVersiontag, db.PhotometryVersiontag.photometry_id==db.Photometry.id )
                          .join( db.VersionTag,
                                 sqlalchemy.and_( db.PhotometryVersiontag.versiontag_id==db.VersionTag.id,
                                                  db.VersionTag.name=='default' ) )
                          .order_by( db.Photometry.mjd ) ).all() )

        for gzp, dp in zip( gz_phots, d_phots ):
            assert gzp.mjd == dp.mjd
            assert gzp.flux == dp.flux
            assert gzp.dflux == dp.dflux

        for phot, expected in zip( d_phots, expectedphots ):
            for key, val in expected.items():
                assert getattr(phot, key) == val
