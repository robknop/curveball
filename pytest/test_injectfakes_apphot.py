import sys
import math
import pathlib
import shutil
import pytest

import numpy.random

import db
import astrometry
import subtract
from injectfakes import FakeInjector

from fixtures_ztf import *

# TODO -- more
# ...check that the "injectfakes" subtraction tag exists
#    and that the subtraction is created and tagged
#    only with that (def. not latest ord efault)
# ...check that fakeinjector cleans up even when
#    it errors out

class TestInjectFakes:

    def test_injectfakes( self, database, ztf, ztf_image_on_archive, ztf_ref_on_archive ):
        img = ztf_image_on_archive
        ref = ztf_ref_on_archive
        rng = numpy.random.default_rng( 42 )
        try:
            faker = FakeInjector( ztf, img, ref, subalg='alard1', curdb=database )
            df = faker.repeatedtrials( 9, npsfs=25, minmag=17, maxmag=20, photalg='apphot1',
                                       fluxdistrib='flat', noisy=True, rng=rng )
            ngood, chisq, chisqmargin, meanreducedresid, meanfractionalresid = faker.repeatedtrialsstats( df )

            # Expect chisq/dof to be close to one.
            # Putting in what I know I get for this seed
            # as a regression test.
            assert chisq == pytest.approx( 0.875, abs=0.02 )

            # expect meanreducedresid to be 0±1/sqrt(ngood)
            # For the number of psfs here, that would be 0.2.
            # I know for the random seed I have here I get -0.26,
            # so I'm putting this in as a regresion test.
            assert meanreducedresid == pytest.approx( -0.265, abs=0.01 )

            # Expect fractional resid to be small
            assert meanfractionalresid == pytest.approx( 0.00, abs=0.02 )

        finally:
            # I know that FakeInjector will have performed a subtraction, so I have to get rid of that
            # to fully clean up
            with db.DB.get() as dbo:
                stor = subtract.Subtractor.get( 'alard1', ztf, img, ref, curdb=dbo )
                subobj = stor.get_or_make_subtraction_object( only_load=True, curdb=dbo )
                if subobj is None:
                    # assert False, "Couldn't find subobj that should exist"
                    sys.stderr.write( "test_injectfakes: cleanup didn't find subobj, test must have failed" )
                else:
                    for f in stor.all_archived_filenames():
                        ztf.delete_file_on_archive( f )
                        p = ztf.local_path( f )
                        if p is not None:
                            p.unlink( missing_ok=True )
                    dbo.db.delete( subobj )
                    dbo.db.commit()
        
