import db
import config
import define_ztf_exposuresource
import define_decam_exposuresource
import define_ptf_exposuresource
import sqlalchemy as sa

def main():
    config.Config.init()

    with db.DB.get() as dbo:
        define_ztf_exposuresource.main( dbo )
        define_decam_exposuresource.define_dbentry( dbo )
        define_ptf_exposuresource.main( dbo )
        
        

# ======================================================================

if __name__ == "__main__":
    main()
