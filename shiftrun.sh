#!/bin/sh

estring=""
environment=$(env)
while IFS='=' read -r name val; do
    if [[ $name == OMPI_* || $name == PMIX_* || $name == SLURM_JOB_ID ]]; then
        fixval=`echo $val | perl -pe 's/"/\\\\"/g'`
        estring+=" -e $name=\"$fixval\" "
    fi
done <<< "$environment"

command="shifter --clearenv ${estring} $@"
echo "Running: " ${command[@]}
eval ${command[@]}
