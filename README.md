# Curveball

A Ball of software for making light Curves.

This software is designed to pull down images from a source somewhere on the Internet (e.g. the NOIR Lab archive, image archives for various surveys, etc) and do the processing necessary to build difference image lightcurves.  All the images it knows about are tracked in a database.  Images from the exposure source will only be added to curveball's database when you ask it to do that.  Once it knows about an image, it will pull it (plus a weight and a mask) down as needed from somewhere.

* If the exposure source has preprocessed images (i.e. overscan, bias, flat, etc. corrected), then it will get the images from the original exposure source.  It may modify them a bit after they arrive.  In particular, if the database says that the image has been astrometrically or photometrically calibrated, the image header will be updated with the saved information.  Also, the mask image will be wrangled from the standard used on the exposure source to the standard that curveball expects.

* Otherwise, ... well it's not implemented yet.

When curveball makes new files (e.g. image stacks, photometry catalogs), it will archive those images to a location on NERSC in addition to saving them to the local data archive.  (__Important note__: if you use `https://curveball-upload.lbl.gov/upload` as `archive.uploadurl` in your config file, then we're all sharing the same archive!  To avoid creating a disaster, make _sure_ that you've got `archive.prefix` set to something that's specific to you!)

This design (i.e. getting files from the exposure source as needed, use of the NERSC archive) is intended to make it easy to move the software from one system to another.  Ideally, just install the software (inside a Docker or similar image), set up a config file for your local system, and it will then grab (and store) images as needed from/to the NERSC archive and/or the exposure source.

## Getting Started

### Setting yourself up to run curveball on an existing database

#### Prerequisites

You need the following:

1. Access to a machine where the curveball docker image exists.  On cori and perlmutter, you can use the image `rknop/curveball:latest`.

1. A checkout of the curveball repository.  Go to somewhere where you can write files and run
   ````bash
   git clone git@gitlab.com:robknop/curveball.git
   ````
   or
   ````bash
   git clone https://gitlab.com/robknop/curveball.git
   ````
   you might need a gitlab account for this, and you might need to have an ssh key linked to this account.  (Investigation needed.)  If you're going to be developing curveball, you definitely want to use the first form of this clone command.  This command will create a subdirectory `curveball` in your current directory.

1. The right branch checked out: by default, the `master` branch is what you get from the git clone command.  However, you might want to be working on another branch; talk to somebody who knows what they're doing to find out if you do.  For instance, you might want ot be on the "ZTF" branch; get to that branch with the command `git checkout ztf`.  (You can get back to the master branch with `git checkout master`.)

1. A place where you can store a lot of files.  You can share this place with other people who run curveball *if* the directory permissions are set up right (this takes some doing!), and if you're sure that you're not going to collide (both be writing the same file at the same time).  Alternatively, you can have your own private space, which will lead to some data duplication, but is safer.  On cori and perlmutter, you want this to be on the appropriate scratch filesystem.  Make yourself a subdirectory "curveball" underneath this directory.

1. A username and password for the database.

1. A username and password for the appropriate section of the curveball webap.  This username and password is *not* the same as the databadse username/password.

1. A config file.  Look at `curveballconfig.yaml` or `curveballconfig_ztf.yaml`.  You _will_ need to customize this for yourself.  In particular, make sure the following:
    * `database.database` must be the right database for curveball that you're working with.  (The host must also be correct, but as of this writing, all known instances of curveball use the same database host.)
    * `database.username` is your _database_ (not webap) username.
    * `datadirs` is a array with a single entry; for simplicity, leave it as `/data/curveball`.  (This will be an alias for the place you identfied above as a place to store lots of files.)
    * `archive.localdir` is the same as the one entry in `datadirs`.
    * `archive.prefix` is correct for your database.  *This is very important.*  Do not run anything until you're very sure you've got this right.

1. The environmentvariable `CURVEBALL_CONFIG` needs to be set to the name of the `.yaml` file (without the `.yaml`... so, if you made `curveball_mine.yaml`, then the value is `curveball_mine`.  Every time you get a new terminal, you need to run:
   ````bash
   export CURVEBALL_CONFIG=[value]
   ````

   You might want to put that command in your `.bashrc` file so you don't have to remember to do this every time.

1. A secrets file.  This is part of the config file that's not stored with the rest, so that things that shouldn't be public won't get committed to gitlab.  Create this file as follows (talk to Rob to find out the right token value):
    * `cd ~`
    * `mkdir secrets`
    * `chmod go-rwx secrets`
    * Now edit the file `curveball.yaml` and give it the contents:
      ````yaml
      database:
        password: [your database password]

      archive:
        token: [right token value]
      ````

#### Make a convenience file for launching into the docker image

(This is for running on NERSC perlmutter; running elsewhere will have a different command.)  When running on the command line, you can't just run curveball stuff; first, you must launch into the docker image.  On perlmutter, you accomplish this with the `shifter` command.  To make this easy on yourself, in your curveball directory make a file `shifter.sh` (or some such) with contents: ````bash #!/bin/bash

shifter --image=rknop/curveball:latest --volume="[path]:/home/curveball" --volume="[path]:/data" --volume="[path]:/secrets" -e TERM=xterm /bin/bash
````

The first `[path]` needs to be replaced with the full path to where you checked out curveball.  You can figure this out with `pwd`.  The second `[path]` needs to be the secrets directory you made above.  The third `[path]` needs to be the scratch directory you identified where you can write lots of files.

Thereafter, to launch into the docker image so that you can run curveball, you can run
````
bash shifter.sh
cd /curveball
````

Inside this docker image, the directory `/curveball` will be at your curveball checkout, and `/data/curveball` will be where all images are written (assuming you've used the suggested value in the config file).

On perlmutter, for instance, you'll want to do this in an interactive session.  From a perlmutter login node, run
````bash
  salloc --qos=interactive -t 04:00:00 --constraint=cpu --account=[account]
````
where `[account]` is the nersc group account the time from this session should be charged to.  If you need to run something that will last more than a couple of hours, you want to do it via batch job, for which I haven't written instructions yet.

#### Creating a kernel to run with jupyter

If you want to run curveball stuff within jupyter, you have to define a kernel that uses the curveball docker image.  First, make sure your jupyter kernels directory exists (it probably does), and the cd into it (notice the . before local below!):
````bash
cd ~
mkdir -p .local/share/jupyter/kernels
cd .local/share/jupyter/kernels
````

Now, make a subdirectory for the curveball kernel.  If you'll be working with more than one database, you'll probably need different subdirectories for each database.
````bash
mkdir curveball
cd curveball
````

Finally, edit a file `kernel.json` and give it contents
````json
{
  "argv": [
    "shifter",
    "--image=rknop/curveball",
    "--volume=[path]:/curveball;[path]:/data;[path]:/secrets",
    "python",
    "-m",
    "ipykernel_launcher",
    "-f",
    "{connection_file}"
  ],
  "display_name": "curveball",
  "language": "python"
}
````

At this point, the kernel should be defined, and it should show up if you make a new notebook on jupyter.

### Setting up a new curveball database

To be written.

## Lightcurve Building Overview

* Define an object
* Build a template for that object in each band for your ExposureSource
* Identify and pull down the images that will be used for the lightcurve
* Build the lightcurve allowing recentering
* Update the object position
* Build the lightcurve a second time with the new object position

There are of course details.

Run `python buildltcv.py --help | less` for more details about all of this.

# Further Notes

The notes below are mostly for those of us who are developing curveball.

## "Style Guide"

Do _not_ use a source cleanup tool like black.  Those totally screw up your source code (and everybody else's to, if you do it to the whole code base).  Style guides are _guidelines_.  Running code to implement them is like taking a sledgehammer to a light nail.  It will properly drive the nail in, but it will also do all kinds of collateral damage to the stuff around the nail.

Don't feel the need to religiously adhere to PEP-8, _unless_ you view the "A Foolish Consistency is the Hobgoblin of Little Minds" as the pre-eminent and most important part of PEP-8.  Religious PEP-8 adherence makes your source code standards compliant... and harder to read.  (Whenever I read style guides, I find myself bristling in the same way that Beethoven did when reminded about parallel fifths being forbidden.  You are probably having the same reaction to this paragraph.)  Instead, look at PEP-20.

__Follow PEP-20.  Make your code readable.__ For me, that means doing things that some style guides say I'm not supposed to do, in particular:

* I tend to put a space after/before opening/closing parentheses.  So, instead of calling `object.method(arg)`, I call `object.method( arg )`.  I don't always do this, but I find often it makes the code far more readable.  Others disagree, and some style guides say never do this.

* I separate methods with comment separators like a line of =.  I find that this makes it much easier to separate out one method I want to find.

Three rules I would ask that you adhere to:

* Never use TABs to indent!  Always use spaces.  Make sure your editor is configured properly to do this.  If you edit existing code, most of them nowadays are smart enough to see that your python has space-based and not tab-based indenting.  Python will _break_ if you try to mix spaces and TABs.  (Personally, I think the "never use TABs" rule should apply to _all_ source code, and all things written with a monospace font.  They are important for non-monospace stuff such as you do in a word processor, but cause disaster beyond any benefit they give in things like source code.  They [violate the fundamental assumption of monospace text](https://dorigo.co/2022/06/tabs-spaces/), which is one character = one character on the screen.  I know there are lots of people who disagree with me... but they are wrong.  In any event, with Python, it's not just style, because indentation has semantic meaning; as such, you must be consistent, so for purposes of this code, to make things keep working we all have to do the same thing.)

* Try to keep your lines shorter than 120 characters.  The style guides all say 80 characters, which I find to be far too restrictive.  We're enough decades past VT-100 terminals now that I don't think that's necessary any more.  Too wide makes things hard to read, but too narrow means lines are too often broken between multiple lines, also harms readability.  120 is to me a good compromise.  When I open terminals and editors, I always make them 120 characters wide; that way, I know that the code I've written will fit within my terminal.  (One exception: when writing docstrings, there I format it to 80 characters or less.  120 character wide lines seem to be quite readable for code, but are too wide for paragraphs.)

* Avoid use of \ at the end of a line to break lines.  I find this to be a trap.  It's very easy to end up with a space after the \ that screws this up.  When you need to break a line into multiple lines, but things in parentheses.  Python doesn't need the \ at the end of a line to recognize that the line is continued if it's inside parentheses.  This makes things way more readable than \ at the end of lines.

## Doing stuff in interactive python

You'll need to make sure a config has been read in.  If you've mounted the curveball directory at `/home/curveball`, you can load the default config with:

    import config
    config.Config.init( "/home/curveball/curveballconfig.yaml" )

If you want to use a different config, set the environment variable `CURVEBALL_CONFIG` to the name of the `.yaml` file in `/home/curveball`, only ''without'' the `.yaml`.  (So, if you want to use `/home/curveball/dev.yaml`, do `export CURVEBALL_CONFIG=dev`.)

## Finding and securing local images

Within python (in a script, or writing your own code), call the `local_path(filename)` method of an ExposureSource object.  That will return a `pathlib.Path` object for the image (or other file) in the local archive if found, or `None` if not found.

For images that are taken directly from the exposure source, call the `ensure_sourcefiles_local` method of an ExposureSource object.  That will pull down the image, mask, and weight images.  The image must already be known in the database before this will work.

TODO: securing local image of NERSC archive things.

## Initializing a database

You must already have the database created at a place you can access it, and you must have done (as postgres root)

    CREATE EXTENSION q3c;

in that database.  Make sure that the .yaml config file you're using is
properly configured for the database. Then run:

export CURVEBALL_CONFIG=<yaml file>

If the database is empty, you should then just be able to run (inside the container, with /home/curveball as your CWD):

     alembic upgrade head

After that, run

    python ./standard_defines.py

in /home/curveball to add the Alard/Luptton subtraction algorithm and everything necessary for the ZTF exposure source.

## Add an object to the database

    python bin/define_object.py <name> <ra> <dec> <t0>

May also add -z, --classification, --confidence, --othernames; see
--help for more information.

These go into the "object" table in the database.

## List what images we have in the database for an object

    python bin/list_db_images.py [-b <band>] <object>

where <object> is the object name.  This will list image names, time
relative to the object's peak, band, seeing, zeropoint, and limiting
mag.

## Pull down and load into the database all raw images for a given supernova

    python bin/get_images_for_sn.py -e <expsource> -o <object>

where <object> is the object name in the database, and <expsource> is
the string that is passed to ExposureSource.get(); at the moment, it
knows "ZTF" and "PTF", but I don't know if PTF is actually working.  Run
with --help for more information.  (You can change some things from
default like the time range of images to get; you can also specify an
ra/dec/peak time instead of an object name.

This script does everything in the "Finding images" section below, so
you don't need to worry about doing everything in there for the SN
images you want to grab.

## Finding images

There isn't a command line utility.  However, you can instantiate an
exposure source (expsource in the example below) with 
exposuresource.ExposureSource.get(...) and call:

    images = expsource.find_images( ra, dec, starttime=..., endtime=... )
    
That will return a Pandas DataFrame with a bunch of information.  This
is intended to be opaque; running len() on it tells you the number of
images you get, but the actual fields are exposure source-specific.  You
can call

    imagename = expsource.blob_image_path( images, i )

to get the image path relative to where images are stored (datadirs in
config) that this image would have if it were in the database and
local.  There's not a good way right now to pull out other information
about the image (like exposure time, filter, etc.).

If you want to download one of the images you just found, you can do it
with

   expsource.download_blob_image( images, index )

where `images` is what was returned by `find_images`, and `index` is the
index into that opaque blob of the image you want.  That will download
it to a subdirectory underneath the first directory you have configured
in `datadirs` in your yaml config file.  (This is where the pipeline
will want to find the image.)  However, it will *not* load the image
into the database.  You can load the downloaded image into the database
with

  expsource.create_or_load_image( expsource.blob_image_path( images, index ) )


## Preprocess images (i.e. flatfield etc.)

Can't be done at the moment.  What I've worked with so far are archives
of reduced images.

## Create a template for an object

Run

   python make_reference.py --help

## Build lightcurve

    python buildltcv.py --help

This loads up photometry into the database.  It will only work on images
already in the database, so before doing this, you probably need to run
`get_images_for_sn.py`.

