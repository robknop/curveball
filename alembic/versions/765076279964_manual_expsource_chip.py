"""manual_expsource_chip

Revision ID: 765076279964
Revises: ead1eca74353
Create Date: 2023-04-20 17:13:51.647807

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '765076279964'
down_revision = 'ead1eca74353'
branch_labels = None
depends_on = None


def upgrade() -> None:
    conn = op.get_bind().connection
    cursor = conn.cursor()
    cursor.execute( "SELECT id,pixscale FROM exposuresource WHERE name='manual_expsource' AND secondary IS NULL" )
    res = cursor.fetchone()
    expid = res[0]
    deltax = res[1] / 3600. * 1024
    deltay = res[1] / 3600. * 512
    op.execute( f"INSERT INTO camerachip(exposuresource_id,raoff,decoff,ra0off,dec0off,ra1off,dec1off,"
                f"ra2off,dec2off,ra3off,dec3off,isgood,chiptag) "
                f"VALUES ({expid},0.,0.,{deltax},{-deltay},{deltax},{deltay},{-deltax},{deltay},{-deltax},{-deltay},"
                f"true,'00')" )

def downgrade() -> None:
    conn = op.get_bind().connection
    cursor = conn.cursor()
    cursor.execute( "SELECT id FROM exposuresource WHERE name='manual_expsource' AND secondary IS NULL" )
    res = cursor.fetchone()
    expid = res[0]
    op.execute( f"DELETE FROM camerachip WHERE exposuresource_id={expid}" )
