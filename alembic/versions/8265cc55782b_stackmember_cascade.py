"""stackmember_cascade

Revision ID: 8265cc55782b
Revises: e1a4510e2aa8
Create Date: 2023-04-24 20:15:43.936547

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8265cc55782b'
down_revision = 'e1a4510e2aa8'
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('stackmember_image_id_fkey', 'stackmember', type_='foreignkey')
    op.create_foreign_key(None, 'stackmember', 'image', ['image_id'], ['id'], ondelete='CASCADE')
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'stackmember', type_='foreignkey')
    op.create_foreign_key('stackmember_image_id_fkey', 'stackmember', 'image', ['image_id'], ['id'], ondelete='RESTRICT')
    # ### end Alembic commands ###
