"""manual_expsource

Revision ID: ead1eca74353
Revises: 81cacb52cb5e
Create Date: 2023-04-20 16:13:14.871647

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ead1eca74353'
down_revision = '81cacb52cb5e'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute( "INSERT INTO exposuresource(sourcetype,nx,ny,orientation,pixscale,preprocessed,skysub,name,secondary) "
                "VALUES(0,2048,1024,0,0.37,false,false,'manual_expsource',NULL)" )
    op.execute( "INSERT INTO band(closeststd,name,filtercode,description,sortdex,exposuresource_id) "
                "SELECT NULL,'open','open','no filter',1000,id "
                "  FROM exposuresource WHERE name='manual_expsource' AND secondary IS NULL" )

def downgrade() -> None:
    conn = op.get_bind()
    cmd = conn.execute( "SELECT id FROM exposuresource WHERE name='manual_expsource' AND secondary IS NULL" )
    res = cmd.fetchone()
    expid = res[0]
    op.execute( f"DELETE FROM band WHERE name='open' AND exposuresource_id={expid}" )
    op.execute( f"DELETE FROM exposuresource WHERE id={expid}" )
