"""prep_subalgorithm_fix

Revision ID: 9590e7049773
Revises: 765076279964
Create Date: 2023-04-21 21:24:18.121340

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '9590e7049773'
down_revision = '765076279964'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute( "UPDATE subtraction SET subalg_id=NULL" )


def downgrade() -> None:
    op.execute( "UPDATE subtraction SET subalg_id="
                "( SELECT id FROM sub_algorithm WHERE name='Alard/Lupton - Hotpants Ver 1' )" )
