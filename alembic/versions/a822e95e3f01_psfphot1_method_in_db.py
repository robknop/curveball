"""psfphot1_method_in_db

Revision ID: a822e95e3f01
Revises: 46d114e046cd
Create Date: 2023-05-17 18:49:44.479862

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'a822e95e3f01'
down_revision = '46d114e046cd'
branch_labels = None
depends_on = None


def upgrade() -> None:
    op.execute( "INSERT INTO photmethod(name,description,isaper,ispsffit,isscenemodel) "
                "VALUES ('psfphot1','PSF Photometry 4x oversampling',false,true,false)" ) # Emily added


def downgrade() -> None:
    op.execute( "DELETE FROM photmethod WHERE name='psfphot1'" )
