import sys
import os
import pathlib

from logging.config import fileConfig

from sqlalchemy import engine_from_config
from sqlalchemy import pool

from alembic import context

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)

import db
import config as curveball_config
configfile = pathlib.Path( "/curveball" ) / ( os.getenv( "CURVEBALL_CONFIG" ) + ".yaml" )
curveball_config.Config.init( configfile )

def run_migrations_offline() -> None:
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """

    dbcon = db.DB.get()
    context.configure(
        connection=dbcon.db.connection(),
        target_metadata=db.Base.metadata,
        literal_binds=True,
        dialetc_opts={"paramstyle": "named"},
    )
    with context.begin_transaction():
        context.run_migrations()
    dbcon.db.commit()
    dbcon.close()


def run_migrations_online() -> None:
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    dbcon = db.DB.get()
    context.configure(
        connection=dbcon.db.connection(),
        target_metadata=db.Base.metadata
    )
    with context.begin_transaction():
        context.run_migrations()
    dbcon.db.commit()
    dbcon.close()

if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
